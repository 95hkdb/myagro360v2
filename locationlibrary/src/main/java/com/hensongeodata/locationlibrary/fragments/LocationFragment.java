package com.hensongeodata.locationlibrary.fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.hensongeodata.locationlibrary.MyApplication;
import com.hensongeodata.locationlibrary.R;
import com.hensongeodata.locationlibrary.controllers.BroadcastCall;
import com.hensongeodata.locationlibrary.models.Loc;
import com.hensongeodata.locationlibrary.services.LocationService;


/**
 * Created by user1 on 10/14/2017.
 */

 public class LocationFragment extends Fragment {
    View rootView;
    private MyApplication myApplication;
    private Spinner mSpinner;
    private String placeholder="Choose your location";
    private ArrayList<String> mFields;

    private Double mLatitude=-1.0;
    private Double mLongitude=-1.0;
    private String name;
    private Double accuracy=-1.0;

    private boolean isShowingGPSDialog=false;
    private static final int MY_PERMISSIONS_ACCESS_FINE_LOCATION = 121;
    private static int ACTIVATE_GPS=100;
    private int action;

    private IntentFilter filter;
    private LocationReceiver locationReceiver;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(locationReceiver==null) {
            locationReceiver = new LocationReceiver();
            filter = new IntentFilter(BroadcastCall.LOC_UPDATE);
            getActivity().registerReceiver(locationReceiver, filter);
        }

        if(action==ACTIVATE_GPS){
            checkGpsStatus();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.spinner, container, false);
        myApplication=new MyApplication();
        mFields=new ArrayList<>();

          mSpinner = rootView.findViewById(R.id.sp);
        return rootView;
    }

    public void setFields(ArrayList<String>fields ){
        mFields=new ArrayList<>();
        for(int a=0;a<fields.size();a++){
            mFields.add(fields.get(a));
        }
        mFields.add(placeholder);

        setSpinner(mSpinner,mFields,placeholder);
    }

    public Loc getLocation(){
        if(mLatitude==-1.0){

        }
        else if(mLongitude==-1.0){

        }
        else if(accuracy==-1.0){

        }
        else if(TextUtils.isEmpty(name)||name==null||name.trim().equalsIgnoreCase(placeholder)){

        }

        return new Loc(mLatitude,mLongitude,accuracy,name);
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public Double getLongitude() {
        return mLongitude;
    }

    public String getName() {
        return name;
    }

    public Double getAccuracy() {
        return accuracy;
    }


    private void setmLatitude(Double mLatitude) {
        this.mLatitude = mLatitude;
    }

    private void setmLongitude(Double mLongitude) {
        this.mLongitude = mLongitude;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

    private void setSpinner(Spinner spinner, List<String> list, String prompt){
        if(list!=null&&list.size()>2) {
            final int listsize = list.size() - 1;
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list) {
                @Override
                public int getCount() {
                    return (listsize); // Truncate the list
                }
            };

            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapter);
            spinner.setSelection(listsize);
            spinner.setPrompt(prompt);
            spinner.setFocusable(true);
        }
    }

    public void pickLocationPermission() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(getActivity(), "We need your geo location in order to for you to continue", Toast.LENGTH_SHORT).show();
            }
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},  MY_PERMISSIONS_ACCESS_FINE_LOCATION );
        } else {
            checkGpsStatus();
        }
    }

    public void checkGpsStatus() {

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        boolean GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!GpsStatus&&!isShowingGPSDialog){
            isShowingGPSDialog=true;
            new AlertDialog.Builder(getActivity())
                    .setTitle("GPS Activation")
                    .setMessage("Please go to your phone settings and turn on Location.")
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            isShowingGPSDialog=false;
                            settingsGPS();
                        }
                    })
                    .show();
        }
        else {
            getActivity().startService(new Intent(getActivity(), LocationService.class));
            // doUpdate();
            action=-1;
            Toast.makeText(getActivity(),"GPS activated",Toast.LENGTH_SHORT).show();
        }
    }

    private void settingsGPS(){
        Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
        action=ACTIVATE_GPS;
    }

    private class LocationReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            setmLatitude(intent.getExtras().getDouble("latitude"));
            setmLongitude(intent.getExtras().getDouble("longitude"));
            setAccuracy(intent.getExtras().getDouble("accuracy"));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PERMISSIONS_ACCESS_FINE_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                getActivity().finish();
            }
            checkGpsStatus();
        }
    }

    private void displayToast(String message){
        Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(locationReceiver!=null)
            getActivity().unregisterReceiver(locationReceiver);
    }
}