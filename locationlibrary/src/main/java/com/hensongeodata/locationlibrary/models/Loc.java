package com.hensongeodata.locationlibrary.models;

/**
 * Created by user1 on 11/16/2017.
 */

public class Loc {
    private Double latitude;
    private Double longitude;
    private Double accuracy;
    private String name;


    public Loc(){}

    public Loc(Double latitude, Double longitude, Double accuracy, String name) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.accuracy = accuracy;
        this.name = name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
