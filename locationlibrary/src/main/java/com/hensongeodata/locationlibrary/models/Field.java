package com.hensongeodata.locationlibrary.models;

/**
 * Created by user1 on 11/16/2017.
 */

public class Field {
    private String name;

    public Field(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
