package com.hensongeodata.locationlibrary.services;


/**
 * Created by brasaeed on 03/06/2017.


public class LocsCast extends Service implements LocationListener {

    private static String TAG=LocsCast.class.getSimpleName();
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private double fusedLatitude = 0.0;
    private  double fusedLongitude = 0.0;
    private  int id=0;

    Runnable runnable;
    Handler handler;
    private boolean updateLoc=false;

    NetworkRequest networkRequest;

    @Override
    public void onCreate() {
        super.onCreate();
        handler=new Handler();
        networkRequest=new NetworkRequest(this);
        init();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (Utilities.isOnlineReceiver(this)) {
            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            }
            Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            if (hour == 19) {
                stopSelf();
            }
        }

        return START_STICKY;
    }



    private void init(){

        Log.d(TAG,"checkPlayService "+checkPlayServices());
        if (checkPlayServices()) {
            startFusedLocation();
            registerRequestUpdate(this);
            allowUpdate();
        }
        else {
            new AlertDialog.Builder(this)
                    .setTitle("Google Play Service Error")
                    .setMessage("Banikudi requires an up to date Google Play Service." +
                            "Please update the Google Play Service installed on your phone and try again.")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                           stopSelf();
                        }
                    })
                    .show();

        }

    }


    // check if google play services is installed on the device
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(),
                        "This device is supported. Please download google play services", Toast.LENGTH_LONG)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();

                stopSelf();
            }
            return false;
        }
        return true;
    }


    public void startFusedLocation() {
        Log.d(TAG,"mGoogleApiClient "+mGoogleApiClient);
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnectionSuspended(int cause) {
                            Log.d(TAG," suspended "+cause);
                        }

                        @Override
                        public void onConnected(Bundle connectionHint) {
                            Log.d(TAG," connected "+connectionHint);
                        }
                    }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {

                        @Override
                        public void onConnectionFailed(ConnectionResult result) {
                            Log.d(TAG," failed "+result);
                        }
                    }).build();
            mGoogleApiClient.connect();
        } else {
            mGoogleApiClient.connect();
        }
    }


    public void stopFusedLocation() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    public void registerRequestUpdate(final LocationListener listener) {
        Log.d(TAG,"rru started");
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(2000); // every  2 seconds

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, listener);
                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (!isGoogleApiClientConnected()) {
                        mGoogleApiClient.connect();
                    }
                    registerRequestUpdate(listener);
                }
            }
        }, 5000);
    }

    public boolean isGoogleApiClientConnected() {
        return mGoogleApiClient != null && mGoogleApiClient.isConnected();
    }

    @Override
    public void onLocationChanged(Location location) {
        setFusedLatitude(location.getLatitude());
        setFusedLongitude(location.getLongitude());
        Log.d(TAG,"locationChanged");
          if(updateLoc) {
              Log.d(TAG,"locsCast "+getFusedLongitude()+" "+getFusedLatitude()+" "+location.getAccuracy());
                networkRequest.sendLocsCast(getFusedLongitude(), getFusedLatitude(),location.getAccuracy());
            updateLoc=false;
            allowUpdate();
        }
 }

    private void allowUpdate(){
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateLoc=true;
            }
        },10);
    }

    public void setFusedLatitude(double lat) {
        fusedLatitude = lat;
    }

    public void setFusedLongitude(double lon) {
        fusedLongitude = lon;
    }

    public double getFusedLatitude() {
        return fusedLatitude;
    }

    public double getFusedLongitude() {
        return fusedLongitude;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
 */