package com.hensongeodata.locationlibrary.controllers;

import android.content.Context;
import android.content.Intent;
import android.location.Location;


/**
 * Created by user1 on 8/2/2017.
 */

public class BroadcastCall {
    public static String LOC_UPDATE = "android.intent.action.locsmman_transaction";

    public static void publishLocation(Context context,Location location){
        Intent intent = new Intent(LOC_UPDATE);
        intent.setAction(LOC_UPDATE);
            intent.putExtra("location",true);
            intent.putExtra("latitude", location.getLatitude());
            intent.putExtra("longitude",location.getLongitude());
            intent.putExtra("accuracy",location.getAccuracy());

            context.sendBroadcast(intent);
    }

}
