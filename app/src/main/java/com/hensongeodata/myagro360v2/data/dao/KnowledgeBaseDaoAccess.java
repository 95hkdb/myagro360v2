package com.hensongeodata.myagro360v2.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.hensongeodata.myagro360v2.model.KnowledgeBase;

import java.util.List;

@Dao
public interface KnowledgeBaseDaoAccess {

    @Insert
    long insert(KnowledgeBase knowledgeBase);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSingleKnowledge(KnowledgeBase knowledgeBase);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateKnowledge(KnowledgeBase knowledgeBase);

    @Insert
    void insertKnowledgeBaseList(List<KnowledgeBase> knowledgeBases);

    @Query("SELECT * FROM knowledgeBase WHERE id = :id")
    KnowledgeBase fetchKnowledgeBaseById(long id);

    @Query("SELECT * FROM knowledgeBase WHERE title LIKE  :name LIMIT 1")
//    @Query("SELECT * FROM knowledgeBase WHERE title LIKE '%' || :name || '%' LIMIT 1")
    KnowledgeBase searchPestInKB(String name);

    @Query("SELECT * FROM knowledgeBase")
    LiveData<List<KnowledgeBase>> fetchKnowledgeBase();

    @Query("SELECT *  FROM knowledgeBase")
    List<KnowledgeBase>fetchKnowledgeBaseList();

    @Query("SELECT COUNT(*) FROM knowledgeBase")
    int knowledgeBaseCount();

    @Query("DELETE FROM knowledgeBase")
    void deleteKB();

}
