package com.hensongeodata.myagro360v2.data;

import android.content.Context;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.hensongeodata.myagro360v2.data.dao.CropDaoAccess;
import com.hensongeodata.myagro360v2.data.dao.FarmActivityDaoAccess;
import com.hensongeodata.myagro360v2.data.dao.FarmDaoAccess;
import com.hensongeodata.myagro360v2.data.dao.KnowledgeBaseDaoAccess;
import com.hensongeodata.myagro360v2.data.dao.MapDaoAccess;
import com.hensongeodata.myagro360v2.data.dao.MemberDaoAccess;
import com.hensongeodata.myagro360v2.data.dao.NotificationDaoAccess;
import com.hensongeodata.myagro360v2.data.dao.POISDaoAccess;
import com.hensongeodata.myagro360v2.data.dao.PlotDaoAccess;
import com.hensongeodata.myagro360v2.data.dao.ScanDaoAccess;
import com.hensongeodata.myagro360v2.data.dao.ScoutDaoAccess;
import com.hensongeodata.myagro360v2.model.KnowledgeBase;
import com.hensongeodata.myagro360v2.version2_0.Model.AppNotification;
import com.hensongeodata.myagro360v2.version2_0.Model.Crop;
import com.hensongeodata.myagro360v2.version2_0.Model.Farm;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmActivity;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.version2_0.Model.Member;
import com.hensongeodata.myagro360v2.version2_0.Model.POISModel;
import com.hensongeodata.myagro360v2.version2_0.Model.ScanModel;
import com.hensongeodata.myagro360v2.version2_0.Model.ScoutModel;

@Database(entities = {MapModel.class, FarmPlot.class, POISModel.class, Crop.class, FarmActivity.class,
        Member.class, Farm.class, AppNotification.class, ScanModel.class, ScoutModel.class, KnowledgeBase.class}, version = 47, exportSchema = false)

public abstract class MapDatabase extends RoomDatabase {

      private static final String TAG = MapDatabase.class.getSimpleName();
      private static final Object LOCK = new Object();
      private static final String DATABASE_NAME = "map_db";
      private static  MapDatabase sInstance;

      public static MapDatabase getInstance(Context context){
            if (sInstance == null){
                  synchronized (LOCK){
                        Log.d(TAG, "Creating a new database instance");
                        sInstance = Room.databaseBuilder(context.getApplicationContext(),
                                MapDatabase.class, MapDatabase.DATABASE_NAME)
//                        .allowMainThreadQueries()
                                .fallbackToDestructiveMigration()
                                .build();
                  }
            }
//
            Log.d(TAG, "Getting the Location database instance");

            return sInstance;
      }

      public abstract MapDaoAccess daoAccess();

      public abstract PlotDaoAccess plotDaoAccess();

      public abstract POISDaoAccess poisDaoAccess();

      public abstract CropDaoAccess cropDaoAccess();

      public abstract FarmActivityDaoAccess farmActivityDaoAccess();

      public abstract MemberDaoAccess memberDaoAccess();

      public abstract FarmDaoAccess farmDaoAccess();

      public abstract NotificationDaoAccess notificationDaoAccess();

      public abstract ScanDaoAccess scanDaoAccess();

      public abstract ScoutDaoAccess scoutDaoAccess();

      public abstract KnowledgeBaseDaoAccess knowledgeBaseDaoAccess();

}
