package com.hensongeodata.myagro360v2.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.hensongeodata.myagro360v2.version2_0.Model.ScanModel;

import java.util.List;

@Dao
public interface ScanDaoAccess {

    @Insert
    long insert(ScanModel scanModel);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSingleScout(ScanModel scanModel);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateScout(ScanModel scanModel);

    @Insert
    void insertScoutList(List<ScanModel> scanModels);

    @Query("SELECT * FROM scans WHERE id = :id")
    ScanModel fetchScanById(long id);

    @Query("SELECT * FROM scans WHERE org_id = :orgId ORDER BY date_created DESC")
    LiveData<List<ScanModel>> fetchAllScouts(String orgId);

    @Query("SELECT * FROM scans WHERE scout_id = :scout_id ORDER BY date_created DESC")
    LiveData<List<ScanModel>> fetchScansBByScoutID(String scout_id);

    @Query("SELECT * FROM scans WHERE org_id = :orgId ORDER BY id ASC")
    List<ScanModel> fetchAllScanList(String orgId);

    @Query("SELECT *  FROM scans WHERE org_id = :orgId  ORDER BY id DESC")
    LiveData<List<ScanModel>> fetchAllScansWithPlot(String orgId);

    @Query("SELECT * FROM scans WHERE item_id = :itemId")
    ScanModel fetchScoutByItemId(String itemId);

    @Query("SELECT COUNT(*) FROM scans WHERE org_id =:orgId AND pest_found = 1 ")
    int pestDetectedCount(String orgId);

    @Query("SELECT COUNT(*) FROM scans WHERE org_id =:orgId")
    int scoutCount(String orgId);

    @Query("SELECT COUNT(*) FROM scans WHERE scout_id =:scout_id AND org_id =:orgId")
    int scansCountByBatchId(String orgId, long scout_id);

    @Query("SELECT * FROM scans WHERE scout_id =:scout_id")
    List<ScanModel> scansByScoutId(long scout_id);

    @Query("SELECT COUNT(*) FROM scans WHERE scout_id =:scout_id AND pest_found = 1 AND org_id =:orgId")
    int pestDetectedCountByBatchId(String orgId, long scout_id);

    @Query("SELECT COUNT(*) FROM scans WHERE org_id =:orgId")
    int scanCount(String orgId);

    @Query("DELETE FROM scans WHERE scout_id =:scout_id")
    int deleteByScoutId(long scout_id);

    @Query("DELETE FROM scans WHERE id =:id")
    int deleteById(long id);

    @Query("DELETE FROM scans WHERE org_id =:orgId")
    int deleteScans(String orgId);

}
