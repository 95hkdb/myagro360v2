package com.hensongeodata.myagro360v2.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.hensongeodata.myagro360v2.model.MapModel;

import java.util.List;

@Dao
public interface MapDaoAccess {

    @Insert
    long insert(MapModel mapModel);

    @Query("SELECT * FROM maps WHERE id = :mapId")
    MapModel fetchMapById(int mapId);

    @Query("SELECT * FROM maps WHERE id = :mapId")
    MapModel fetchMapById(String mapId);

    @Query("SELECT COUNT(*) FROM maps")
    int countMaps();

    @Query("SELECT COUNT(*) FROM maps WHERE user_id = :userId")
    int countUserMaps(String userId);

    @Query("SELECT COUNT(*) FROM maps WHERE org_id = :orgId")
    int mapsByOrgId(String orgId);

    @Query("SELECT SUM(size_in_acres) FROM maps WHERE  org_id=:orgId")
    int mapSizeSum( String orgId);

//    @Query("SELECT SUM(size_in_acres) FROM maps WHERE user_id = :userId AND org_id=:orgId")
//    int mapSizeSum(String userId, String orgId);

    @Query("SELECT * FROM maps WHERE user_id = :userId AND org_id=:orgId ORDER BY map_id DESC")
    LiveData<List<MapModel>> fetchAllMaps(String userId, String orgId);

    @Query("SELECT * FROM maps WHERE user_id = :userId AND org_id=:orgId ORDER BY map_id DESC")
    LiveData<List<MapModel>> fetchAllMapsByRecent(String userId, String orgId);

    @Query("SELECT  * FROM maps WHERE org_id=:orgId AND user_id =:userId  ORDER BY size_in_m DESC ")
    LiveData<List<MapModel>> fetchAllMapsBySizeDesc(String userId, String orgId);

    @Query("SELECT  * FROM maps WHERE org_id=:orgId AND user_id =:userId  ORDER BY size_in_m ASC ")
    LiveData<List<MapModel>> fetchAllMapsBySizeAsc(String userId, String orgId);

    @Query("SELECT * FROM maps WHERE user_id = :userId AND org_id=:orgId ORDER BY map_id DESC")
    List<MapModel> fetchAllMapsList(String userId, String orgId);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateMap(MapModel mapModel);

    @Query("UPDATE maps SET synced = 1 WHERE id = :id")
    void updateSyncedMapByMapId(String id);

    @Query("SELECT * from maps  WHERE name = :name LIMIT 1")
    MapModel mapNameExists(String name);

    @Query("SELECT * FROM maps WHERE synced = 0")
    List<MapModel> fetchNotSyncedMaps();

    @Query("SELECT * FROM maps WHERE id = :mapId")
    MapModel fetchNotSyncedMap(String mapId);

    @Query("DELETE FROM maps WHERE synced = 1")
    int deleteSyncedMaps();

    @Query("DELETE FROM maps WHERE id = :mapId")
    int deleteMapById(String mapId);



}
