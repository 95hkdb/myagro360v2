package com.hensongeodata.myagro360v2.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.hensongeodata.myagro360v2.version2_0.Model.Member;

import java.util.List;

@Dao
public interface MemberDaoAccess {

    @Insert
    long insert(Member member);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSingleMember(Member member);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateMember(Member member);

    @Insert
    void insertMemberList(List<Member> members);

    @Query("SELECT * FROM members WHERE member_id = :memberId")
    List<Member> fetchMemberByMemberId(long memberId);

    @Query("SELECT * FROM members WHERE member_id = :memberId")
    Member fetchMemberById(Long memberId);

    @Query("SELECT * FROM members WHERE org_id = :orgId")
    LiveData<List<Member>> fetchAllMembers(String orgId);

    @Query("SELECT COUNT(first_name) FROM members")
    int memberCount();

    @Query("DELETE FROM members")
    void deleteMembers();

}
