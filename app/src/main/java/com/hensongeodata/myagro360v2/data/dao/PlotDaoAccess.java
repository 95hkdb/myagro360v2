package com.hensongeodata.myagro360v2.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;

import java.util.List;

@Dao
public interface PlotDaoAccess {

    @Insert
    long insert(FarmPlot farmPlot);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSinglePlot(FarmPlot farmPlot);

    @Insert
    void insertMultiplePlots(List<FarmPlot> farmPlotList);

    @Query("SELECT * FROM plots WHERE id = :plotId LIMIT 1")
    FarmPlot fetchPlotById(String plotId);

    @Query("SELECT * FROM plots WHERE plot_id = :plotId LIMIT 1")
    FarmPlot fetchPlotByPlotId(long plotId);

    @Query("SELECT * FROM plots WHERE map_id = :mapId ")
   List<FarmPlot>  fetchPlotsByMapId(String mapId);

    @Query("SELECT * FROM plots WHERE user_id =:userId AND org_id =:orgId  ORDER BY plot_id DESC ")
    LiveData<List<FarmPlot>> fetchAllPlotsDesc(String orgId, String userId);

      @Query("SELECT * FROM plots WHERE  org_id =:orgId  ORDER BY plot_id DESC ")
      LiveData<List<FarmPlot>> fetchAllPlotsDesc( String orgId);

      @Query("SELECT  * FROM plots WHERE org_id=:orgId AND user_id = :userId   ORDER BY plot_id DESC ")
      LiveData<List<FarmPlot>> fetchAllPlots(String orgId, String userId);

      @Query("SELECT  * FROM plots WHERE org_id=:orgId AND user_id =:userId  ORDER BY size_in_acres DESC ")
      LiveData<List<FarmPlot>> fetchAllPlotsBySizeDesc(String orgId, String userId);

      @Query("SELECT  * FROM plots WHERE org_id=:orgId AND user_id =:userId  ORDER BY size_in_acres ASC ")
      LiveData<List<FarmPlot>> fetchAllPlotsBySizeAsc(String orgId, String userId);

      @Query("SELECT  * FROM plots WHERE org_id=:orgId AND user_id =:userId  ORDER BY name DESC ")
      LiveData<List<FarmPlot>> fetchAllPlotsByNameDESC(String orgId, String userId);

      @Query("SELECT  * FROM plots WHERE org_id=:orgId AND user_id =:userId  ORDER BY name ASC ")
      LiveData<List<FarmPlot>> fetchAllPlotsByNameAsc(String orgId, String userId);

      @Query("SELECT  DISTINCT * FROM plots WHERE org_id=:orgId AND user_id =:userId  ORDER BY plot_id DESC ")
      LiveData<List<FarmPlot>> fetchDistinctPlots(String orgId, String userId);

      @Query("SELECT  * FROM plots WHERE org_id=:orgId AND crop_id=:cropId  ORDER BY name ASC ")
      LiveData<List<FarmPlot>> fetchAllPlotsByCropId(String orgId, String cropId);

      @Query("SELECT  * FROM plots WHERE org_id=:orgId AND user_id=:userId  ORDER BY name ASC ")
      LiveData<List<FarmPlot>> fetchAllPlotsByUserId(String orgId, String userId);

    @Query("SELECT * FROM plots WHERE synced = 0 ")
    List<FarmPlot> fetchAllUnSyncedPlots();

      @Query("SELECT * FROM plots WHERE synced = 0 AND org_id =:orgId")
      List<FarmPlot> fetchUnSyncedPlots(String orgId);

      @Query("SELECT * FROM plots WHERE id =:id")
      FarmPlot plotExist(String id);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updatePlot(FarmPlot farmPlot);

    @Query("UPDATE plots SET synced = 1 WHERE plot_id = :id")
    void updateSyncStatusById(long id);

    @Query("DELETE FROM plots WHERE plot_id = :id")
    int deletePlotById(long id);

    @Query("SELECT COUNT(*) FROM plots")
    int plotCount();


      @Query("SELECT COUNT(name) FROM plots WHERE user_id = :userId")
      int plotCount(String userId);

      @Query("SELECT SUM(size_in_acres) FROM plots WHERE org_id=:orgId")
      long plotSizeSum( String orgId);

      @Query("SELECT COUNT(*) FROM plots WHERE org_id = :orgId")
      int plotCountByOrgId(String orgId);

      @Query("DELETE FROM plots WHERE synced = 1")
      int deleteSyncedPlots();

      @Query("DELETE FROM plots")
      int deletePlots();

}
