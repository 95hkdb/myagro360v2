package com.hensongeodata.myagro360v2.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.hensongeodata.myagro360v2.version2_0.Model.POISModel;

import java.util.List;

@Dao
public interface POISDaoAccess {

    @Insert
    long insert(POISModel poisModel);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSinglePOIS(POISModel poisModel);

    @Insert
    void insertMultiplePOIS(List<POISModel> poisModels);


    @Query("SELECT * FROM pois WHERE map_id = :mapId")
    List<POISModel> fetchPOISByMapId(String mapId);

    @Query("SELECT * FROM pois WHERE map_id = :mapId")
    POISModel fetchOnePOISByMapId(String mapId);

    @Query("DELETE FROM pois")
    int deletePOIS();

}
