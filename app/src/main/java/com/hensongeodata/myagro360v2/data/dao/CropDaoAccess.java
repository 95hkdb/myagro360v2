package com.hensongeodata.myagro360v2.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.hensongeodata.myagro360v2.version2_0.Model.Crop;

import java.util.List;

@Dao
public interface CropDaoAccess {

    @Insert
    long insert(Crop crop);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSingleCrop(Crop crop);

    @Insert
    void insertMultiplePOIS(List<Crop> crops);

    @Query("SELECT * FROM crops")
    LiveData<List<Crop>> fetchAllCrops();

    @Query("SELECT * FROM crops WHERE id = :cropId")
    List<Crop> fetchCropsById(String cropId);

    @Query("SELECT * FROM crops WHERE crop_id = :cropId")
    Crop fetchCropsByCropId(long cropId);

    @Query("SELECT * FROM crops WHERE crop_id = :cropId")
    Crop fetchCropByCropId(String cropId);

    @Query("SELECT COUNT(*) FROM crops")
    int getCropsCount();

    @Query("DELETE  FROM crops")
    void deleteCrops();
}
