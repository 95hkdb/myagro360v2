package com.hensongeodata.myagro360v2.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.hensongeodata.myagro360v2.version2_0.Model.AppNotification;

import java.util.List;

@Dao
public interface NotificationDaoAccess {

    @Insert
    long insert(AppNotification appNotification);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNotification(AppNotification appNotification);

    @Query("SELECT * FROM app_notifications ORDER BY date_created DESC")
    LiveData<List<AppNotification>> fetchAllNotifications();

    @Query("SELECT COUNT(*) FROM  app_notifications WHERE read = 1")
    int notificationsReadCount();

    @Query("SELECT COUNT(*) FROM  app_notifications WHERE read = 0")
    int notificationsUnReadCount();

    @Query("UPDATE app_notifications SET read = 1 WHERE id = :id")
    void setReadNotification(long id);

    @Query("DELETE  FROM app_notifications")
    void deleteNotifications();

}
