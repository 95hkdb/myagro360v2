package com.hensongeodata.myagro360v2.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.hensongeodata.myagro360v2.api.response.Farm;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmActivity;

import java.util.List;

@Dao
public interface FarmActivityDaoAccess {

    @Insert
    long insert(FarmActivity farmActivity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSingleFarmActivity(FarmActivity farmActivity);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(FarmActivity farmActivity);

    @Insert
    void insertMultipleActivities(List<FarmActivity> farmActivities);

    @Query("SELECT * FROM activities WHERE farm_activity_id = :id")
    List<Farm> fetchFarmActivitiesById(String id);

    @Query("SELECT * FROM activities WHERE farm_activity_id = :id")
    FarmActivity fetchActivityById(String id);

    @Query("SELECT * FROM activities WHERE farm_activity_id = :farmActivityId")
    List<Farm> fetchFarmActivitiesByFarmActivityId(long farmActivityId);

//    @Query("SELECT * FROM activities WHERE  = :farmActivityId")
//    List<Farm> fetchFarmActivitiesByFarmActivityId(long farmActivityId);

    @Query("SELECT * FROM activities WHERE farm_activity_id = :farmActivityId LIMIT 1")
    FarmActivity fetchActivityByFarmActivityId(long farmActivityId);

    @Query("SELECT * FROM activities WHERE user_id = :userId AND org_id = :orgId ORDER BY farm_activity_id DESC")
    LiveData<List<FarmActivity>> fetchAllActivities(String userId, String orgId);

    @Query("SELECT * FROM activities WHERE completed = 1 AND user_id = :userId AND org_id = :orgId ORDER BY farm_activity_id DESC")
    LiveData<List<FarmActivity>> fetchActivitiesByCompleted(String userId, String orgId);

    @Query("SELECT * FROM activities WHERE completed = 0 AND user_id = :userId AND org_id = :orgId ORDER BY farm_activity_id DESC")
    LiveData<List<FarmActivity>> fetchActivitiesByPending(String userId, String orgId);

    @Query("SELECT * FROM activities WHERE id = :activityId AND user_id = :userId AND org_id = :orgId ORDER BY farm_activity_id DESC")
    LiveData<List<FarmActivity>> fetchActivitiesByActivityType(String userId, String orgId, String activityId);

    @Query("SELECT * FROM activities WHERE primary_plot_id = :primaryPlotId AND user_id = :userId AND org_id = :orgId ORDER BY farm_activity_id DESC")
    LiveData<List<FarmActivity>> fetchActivitiesByPlot(String userId, String orgId, long primaryPlotId);

    @Query("SELECT * FROM activities WHERE farm_id = :farmId AND user_id = :userId AND org_id = :orgId ORDER BY farm_activity_id DESC")
    LiveData<List<FarmActivity>> fetchActivitiesByFarm(String userId, String orgId, String farmId);

    @Query("SELECT * FROM activities WHERE start_date BETWEEN :from AND :end  AND user_id = :userId AND org_id = :orgId ORDER BY farm_activity_id DESC")
    LiveData<List<FarmActivity>> fetchActivitiesByStartDate(String userId, String orgId, long from, long end);

    @Query("SELECT * FROM activities WHERE  user_id = :userId AND org_id = :orgId ORDER BY farm_activity_id DESC")
    LiveData<List<FarmActivity>> fetchActivitiesByRecent(String userId, String orgId);

    @Query("SELECT * FROM activities WHERE end_date BETWEEN :from AND :end  AND user_id = :userId AND org_id = :orgId ORDER BY farm_activity_id DESC")
    LiveData<List<FarmActivity>> fetchActivitiesByEndDate(String userId, String orgId, long from, long end);

    @Query("SELECT * FROM activities WHERE id =:activityId AND user_id = :userId AND org_id = :orgId ORDER BY farm_activity_id DESC")
    LiveData<List<FarmActivity>> fetchActivitiesByPending(String userId, String orgId, String activityId);

    @Query("SELECT * FROM activities WHERE user_id = :userId AND org_id = :orgId AND plot_id = :plotId ORDER  BY farm_activity_id DESC")
    List<FarmActivity> fetchActivitiesByPlotId(String userId, String orgId, String plotId);

    @Query("SELECT * FROM activities WHERE user_id = :userId AND org_id = :orgId AND plot_id = :primaryPlotId ORDER  BY farm_activity_id DESC")
    List<FarmActivity> fetchActivitiesByPrimaryPlotId(String userId, String orgId, long primaryPlotId);

    @Query("SELECT * FROM activities WHERE user_id = :userId AND org_id = :orgId ORDER BY farm_activity_id DESC")
    List<FarmActivity> fetchAllActivityList(String userId, String orgId);

    @Query("SELECT * FROM activities WHERE user_id = :userId AND org_id = :orgId AND synced = 0")
    List<FarmActivity> fetchNotSyncedActivities(String userId, String orgId);

    @Query("UPDATE activities SET completed = 1 WHERE farm_activity_id = :farmActivityId ")
    void markActivityAsCompleted(long farmActivityId);

    @Query("UPDATE activities SET completed = 0 WHERE farm_activity_id = :farmActivityId ")
    void markActivityAsPending(long farmActivityId);

    @Query("SELECT * FROM activities WHERE farm_id= :farmId")
    List<FarmActivity> fetchFarmActivitiesByFarmId(String farmId);

    @Query("SELECT * FROM activities WHERE user_id = :userId AND org_id = :orgId ORDER BY created_at DESC LIMIT 4")
    List<FarmActivity> fourRecentActivities(String userId, String orgId);

    @Query("SELECT COUNT(*) FROM activities WHERE plot_id=:potId ")
    int farmActivitiesByPlotIdCount(String potId);

    @Query("SELECT COUNT(*) FROM activities WHERE user_id =:userId AND org_id =:orgId ")
    int farmActivitiesCount(String userId, String orgId);

    @Query("SELECT COUNT(DISTINCT(plot_id)) FROM activities WHERE farm_id = :value AND user_id = :userId AND org_id = :orgId ")
    int fetchActivitiesPlotCount(String userId, String orgId, String value);

    @Query("SELECT COUNT(DISTINCT(plot_id)) FROM activities WHERE farm_id = :value AND user_id = :userId AND org_id = :orgId ")
    int fetchActivitiesByFarmPlotCount(String userId, String orgId, String value);

    @Query("SELECT COUNT(DISTINCT(plot_id)) FROM activities WHERE primary_plot_id = :value AND user_id = :userId AND org_id = :orgId ")
    int fetchActivitiesByPlotPlotCount(String userId, String orgId, long value);

    @Query("SELECT COUNT(DISTINCT(plot_id)) FROM activities WHERE completed = :value AND user_id = :userId AND org_id = :orgId ")
    int fetchActivitiesByCompletedPlotCount(String userId, String orgId, int value);

    @Query("SELECT COUNT(DISTINCT(plot_id)) FROM activities WHERE id = :value AND user_id = :userId AND org_id = :orgId ")
    int fetchActivitiesByActivityPlotCount(String userId, String orgId, String value);

    @Query("SELECT COUNT(DISTINCT(primary_plot_id)) FROM activities WHERE user_id = :userId AND org_id = :orgId ")
    int fetchActivitiesPlotCount(String userId, String orgId);

    @Query("DELETE FROM activities WHERE farm_activity_id=:id ")
    int deleteFarmActivity( long id);

    @Query("DELETE FROM activities")
      void deleteFarmActivities();
}
