package com.hensongeodata.myagro360v2.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.hensongeodata.myagro360v2.version2_0.Model.Farm;

import java.util.List;

@Dao
public interface FarmDaoAccess {

    @Insert
    long insert(Farm farm);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSingleFarm(Farm farm);

    @Insert
    void insertMultipleFarms(List<Farm> farms);

    @Query("SELECT * FROM farms WHERE user_id =:userId AND org_id = :orgId")
    List<Farm> fetchAllFarms(String userId, String orgId);

    @Query("SELECT * FROM farms WHERE user_id =:userId AND org_id = :orgId")
    LiveData<List<Farm>> fetchAllFarmsLiveData(String userId, String orgId);

    @Query("SELECT * FROM farms WHERE farm_id = :farmId")
    List<Farm> fetchFarmsByFarmId(String farmId);

    @Query("SELECT * FROM farms WHERE id = :farmId")
    Farm fetchFarmByFarmId(String farmId);

    @Query("DELETE FROM farms ")
    void deleteFarms();

    @Query("DELETE FROM farms WHERE user_id =:userId AND org_id =:orgId")
    void deleteFarmsByUser(String userId, String orgId);

    @Query("SELECT COUNT(*) FROM farms WHERE org_id =:orgId")
    int getFarmsCount(String orgId);

    @Query("SELECT * FROM farms WHERE synced = 0 AND user_id= :userId AND org_id= :orgId")
    List<Farm> fetchNotSyncedFarms(String userId, String orgId);

}
