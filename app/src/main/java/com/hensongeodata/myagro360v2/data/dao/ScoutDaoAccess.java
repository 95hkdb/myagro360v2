package com.hensongeodata.myagro360v2.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.hensongeodata.myagro360v2.version2_0.Model.ScoutModel;

import java.util.List;

@Dao
public interface ScoutDaoAccess {

    @Insert
    long insert(ScoutModel scoutModel);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSingleScout(ScoutModel scoutModel);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateScout(ScoutModel scoutModel);

    @Insert
    void insertScoutList(List<ScoutModel> scoutModels);

    @Query("SELECT * FROM scouts WHERE id = :id")
    ScoutModel fetchScoutById(long id);

    @Query("SELECT * FROM scouts WHERE org_id = :orgId ORDER BY id DESC")
    LiveData<List<ScoutModel>> fetchAllScouts(String orgId);

    @Query("SELECT * FROM scouts WHERE org_id = :orgId ORDER BY id DESC")
    List<ScoutModel> fetchAllScoutList(String orgId);

    @Query("SELECT *  FROM scouts WHERE org_id = :orgId  ORDER BY id DESC")
    LiveData<List<ScoutModel>> fetchAllScansWithPlot(String orgId);

    @Query("SELECT COUNT(*) FROM scouts WHERE org_id =:orgId")
    int pestDetectedCount(String orgId);

    @Query("SELECT COUNT(*) FROM scouts WHERE org_id =:orgId ")
    int scoutCount(String orgId);

    @Query("SELECT COUNT(*) FROM scouts WHERE org_id =:orgId AND user_id =:userId")
    int scoutCountyUser(String orgId, String userId);

    @Query("SELECT COUNT(*) FROM scouts WHERE batch_id =:batchId AND org_id =:orgId")
    int scoutCountByBatchId(String orgId, String batchId);

    @Query("DELETE FROM  scouts WHERE id = :scoutId")
    int deleteByScoutId(long scoutId);

    @Query("DELETE FROM  scouts WHERE org_id = :orgId")
    int deleteScouts(String orgId);

}
