package com.hensongeodata.myagro360v2.data.viewModels;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.model.KnowledgeBase;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Model.AppNotification;
import com.hensongeodata.myagro360v2.version2_0.Model.Crop;
import com.hensongeodata.myagro360v2.version2_0.Model.Farm;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmActivity;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;
import com.hensongeodata.myagro360v2.version2_0.Model.Member;
import com.hensongeodata.myagro360v2.version2_0.Model.ScanModel;
import com.hensongeodata.myagro360v2.version2_0.Model.ScoutModel;

import java.util.List;

public class MainViewModel extends AndroidViewModel {

      private static final String TAG = MainViewModel.class.getSimpleName();
      private LiveData<List<MapModel>> maps;
      private LiveData<List<MapModel>> mapsByRecent;
      private LiveData<List<MapModel>> mapsBySizeHighToLow;
      private LiveData<List<MapModel>> mapsBySizeLowToHigh;
      private LiveData<List<FarmPlot>> plots;
      private LiveData<List<FarmPlot>> plotsByDescOrder;
      private LiveData<List<FarmPlot>> plotsBySizeDesc;
      private LiveData<List<FarmPlot>> plotsBySizeAsc;
      private LiveData<List<FarmPlot>> plotsByNameDesc;
      private LiveData<List<FarmPlot>> plotsByNameAsc;
      private LiveData<List<FarmPlot>> distinctPlots;
      private LiveData<List<FarmPlot>> plotsByCrop;
      private LiveData<List<FarmPlot>> plotsByUser;
      private LiveData<List<Crop>> crops;
      private LiveData<List<FarmActivity>> farmActivities;
      private LiveData<List<FarmActivity>> farmActivitiesByCompleted;
      private LiveData<List<FarmActivity>> farmActivitiesByPending;
      private LiveData<List<FarmActivity>> farmActivitiesByActivityTpe;
      private LiveData<List<FarmActivity>> farmActivitiesByPlot;
      private LiveData<List<FarmActivity>> farmActivitiesByFarm;
      private LiveData<List<FarmActivity>> farmActivitiesByStartDate;
      private LiveData<List<FarmActivity>> farmActivitiesByRecent;
      private LiveData<List<FarmActivity>> plotFarmActivities;
      private LiveData<List<Member>> members;
      private LiveData<List<Farm>> farms;
      private LiveData<List<AppNotification>> appNotifications;
      private LiveData<List<ScanModel>> scoutModelList;
      private LiveData<List<ScanModel>> scansByScoutID;
      private LiveData<List<ScanModel>> plotScoutModelList;
      private LiveData<List<ScoutModel>> scoutModels;
      private LiveData<List<KnowledgeBase>> knowledgeBaseList;

      // Create a LiveData with a String
      private MutableLiveData<String> currentName;
      private String userId;
//      private MutableLiveData<String> userId;

      private String cropId;
      private String activityId;
      private String plotId;
      private String farmId;
      private String startDate;
      private String endDate;;

            public MainViewModel(@NonNull Application application) {
                  super(application);


            MapDatabase mapDatabase = MapDatabase.getInstance(this.getApplication());

            if (SharePrefManager.isSecondaryUserAvailable(this.getApplication())) {
                  userId = SharePrefManager.getSecondaryUserPref(this.getApplication());
            } else {
                  userId = SharePrefManager.getInstance(this.getApplication()).getUserDetails().get(0);
            }

            String orgId = SharePrefManager.getInstance(this.getApplication()).getOrganisationDetails().get(0);
            cropId = SharePrefManager.getCropSortPreference(this.getApplication());
            activityId = SharePrefManager.getActivityTypeSortPreference(this.getApplication());
            if (SharePrefManager.getSortParameterPreference(this.getApplication(), SharePrefManager.SORT_BY_PLOT_TYPE_ACTIVITY).isEmpty()) {
                  plotId = "0";
            } else {
                  plotId = SharePrefManager.getSortParameterPreference(this.getApplication(), SharePrefManager.SORT_BY_PLOT_TYPE_ACTIVITY);
            }

            farmId = SharePrefManager.getSortParameterPreference(this.getApplication(), SharePrefManager.SORT_BY_FARM_TYPE_ACTIVITY);

            if (SharePrefManager.getSortParameterPreference(this.getApplication(), SharePrefManager.SORT_BY_FROM_DATE_TYPE_ACTIVITY).isEmpty()) {
                  startDate = "0";
            } else {
                  startDate = SharePrefManager.getSortParameterPreference(this.getApplication(), SharePrefManager.SORT_BY_FROM_DATE_TYPE_ACTIVITY);
            }

            if (SharePrefManager.getSortParameterPreference(this.getApplication(), SharePrefManager.SORT_BY_END_DATE_TYPE_ACTIVITY).isEmpty()) {
                  endDate = "0";
            } else {
                  endDate = SharePrefManager.getSortParameterPreference(this.getApplication(), SharePrefManager.SORT_BY_END_DATE_TYPE_ACTIVITY);
            }

            Log.i(TAG, "MainViewModel:  Plot Id " + plotId);

            maps = mapDatabase.daoAccess().fetchAllMaps(String.valueOf(userId), orgId);
            mapsByRecent = mapDatabase.daoAccess().fetchAllMapsByRecent(String.valueOf(userId), orgId);
            mapsBySizeHighToLow = mapDatabase.daoAccess().fetchAllMapsBySizeDesc(String.valueOf(userId), orgId);
            mapsBySizeLowToHigh = mapDatabase.daoAccess().fetchAllMapsBySizeAsc(String.valueOf(userId), orgId);

            plots = mapDatabase.plotDaoAccess().fetchAllPlots(orgId, String.valueOf(userId));
            plotsByDescOrder = mapDatabase.plotDaoAccess().fetchAllPlotsDesc(orgId, String.valueOf(userId));
            plotsBySizeDesc = mapDatabase.plotDaoAccess().fetchAllPlotsBySizeDesc(orgId, String.valueOf(userId));
            plotsBySizeAsc = mapDatabase.plotDaoAccess().fetchAllPlotsBySizeAsc(orgId, String.valueOf(userId));
            plotsByNameDesc = mapDatabase.plotDaoAccess().fetchAllPlotsByNameDESC(orgId, String.valueOf(userId));
            plotsByNameAsc = mapDatabase.plotDaoAccess().fetchAllPlotsByNameAsc(orgId, String.valueOf(userId));
            distinctPlots = mapDatabase.plotDaoAccess().fetchDistinctPlots(orgId, String.valueOf(userId));
            plotsByCrop = mapDatabase.plotDaoAccess().fetchAllPlotsByCropId(orgId, cropId);
            plotsByUser = mapDatabase.plotDaoAccess().fetchAllPlotsByUserId(orgId, userId);

            crops = mapDatabase.cropDaoAccess().fetchAllCrops();
            farmActivities = mapDatabase.farmActivityDaoAccess().fetchAllActivities(String.valueOf(userId), orgId);
            farmActivitiesByCompleted = mapDatabase.farmActivityDaoAccess().fetchActivitiesByCompleted(String.valueOf(userId), orgId);
            farmActivitiesByPending = mapDatabase.farmActivityDaoAccess().fetchActivitiesByPending(String.valueOf(userId), orgId);
            farmActivitiesByActivityTpe = mapDatabase.farmActivityDaoAccess().fetchActivitiesByActivityType(String.valueOf(userId), orgId, activityId);
            farmActivitiesByPlot = mapDatabase.farmActivityDaoAccess().fetchActivitiesByPlot(String.valueOf(userId), orgId, Long.valueOf(plotId));
            farmActivitiesByFarm = mapDatabase.farmActivityDaoAccess().fetchActivitiesByFarm(String.valueOf(userId), orgId, farmId);
            farmActivitiesByStartDate = mapDatabase.farmActivityDaoAccess().fetchActivitiesByStartDate(String.valueOf(userId), orgId, Long.valueOf(startDate), Long.valueOf(endDate));
            farmActivitiesByRecent = mapDatabase.farmActivityDaoAccess().fetchActivitiesByRecent(String.valueOf(userId), orgId);
            members = mapDatabase.memberDaoAccess().fetchAllMembers(orgId);
            farms = mapDatabase.farmDaoAccess().fetchAllFarmsLiveData(String.valueOf(userId), orgId);
            appNotifications = mapDatabase.notificationDaoAccess().fetchAllNotifications();
            scoutModelList = mapDatabase.scanDaoAccess().fetchAllScouts(orgId);
            plotScoutModelList = mapDatabase.scanDaoAccess().fetchAllScansWithPlot(orgId);
            scoutModels = mapDatabase.scoutDaoAccess().fetchAllScouts(orgId);
            knowledgeBaseList = mapDatabase.knowledgeBaseDaoAccess().fetchKnowledgeBase();
      }

      public LiveData<List<MapModel>> getMaps() {
            return maps;
      }

      public LiveData<List<MapModel>> getMapsByRecent() {
            return mapsByRecent;
      }

      public void setMapsByRecent(LiveData<List<MapModel>> mapsByRecent) {
            this.mapsByRecent = mapsByRecent;
      }

      public LiveData<List<MapModel>> getMapsBySizeHighToLow() {
            return mapsBySizeHighToLow;
      }

      public void setMapsBySizeHighToLow(LiveData<List<MapModel>> mapsBySizeHighToLow) {
            this.mapsBySizeHighToLow = mapsBySizeHighToLow;
      }

      public LiveData<List<MapModel>> getMapsBySizeLowToHigh() {
            return mapsBySizeLowToHigh;
      }

      public void setMapsBySizeLowToHigh(LiveData<List<MapModel>> mapsBySizeLowToHigh) {
            this.mapsBySizeLowToHigh = mapsBySizeLowToHigh;
      }

      public LiveData<List<FarmPlot>> getPlots() {
            return plots;
      }

      public LiveData<List<FarmPlot>> getPlotsBySizeDesc() {
            return plotsBySizeDesc;
      }

      public LiveData<List<FarmPlot>> getPlotsBySizeAsc() {
            return plotsBySizeAsc;
      }

      public LiveData<List<FarmPlot>> getPlotsByNameDesc() {
            return plotsByNameDesc;
      }

      public LiveData<List<FarmPlot>> getPlotsByNameAsc() {
            return plotsByNameAsc;
      }

      public LiveData<List<FarmPlot>> getDistinctPlots() {
            return distinctPlots;
      }

      public void setCropId(String crop_id) {
            cropId = crop_id;
      }

      public LiveData<List<FarmPlot>> getPlotsByCrop() {
            return plotsByCrop;
      }

      public void setPlotsByCrop(LiveData<List<FarmPlot>> plotsByCrop) {
            this.plotsByCrop = plotsByCrop;
      }

      public LiveData<List<FarmPlot>> getPlotsByUser() {
            return plotsByUser;
      }

      public void setPlotsByUser(LiveData<List<FarmPlot>> plotsByUser) {
            this.plotsByUser = plotsByUser;
      }

      public LiveData<List<FarmPlot>> getPlotsByDescOrder() {
            return plotsByDescOrder;
      }

      public LiveData<List<Crop>> getCrops() {
            return crops;
      }

      public LiveData<List<FarmActivity>> getFarmActivities() {
            return farmActivities;
      }

      public LiveData<List<FarmActivity>> getFarmActivitiesByCompleted() {
            return farmActivitiesByCompleted;
      }

      public LiveData<List<FarmActivity>> getFarmActivitiesByPending() {
            return farmActivitiesByPending;
      }

      public LiveData<List<FarmActivity>> getFarmActivitiesByActivityTpe() {
            return farmActivitiesByActivityTpe;
      }

      public void setFarmActivitiesByActivityTpe(LiveData<List<FarmActivity>> farmActivitiesByActivityTpe) {
            this.farmActivitiesByActivityTpe = farmActivitiesByActivityTpe;
      }

      public LiveData<List<FarmActivity>> getFarmActivitiesByPlot() {
            return farmActivitiesByPlot;
      }

      public void setFarmActivitiesByPlot(LiveData<List<FarmActivity>> farmActivitiesByPlot) {
            this.farmActivitiesByPlot = farmActivitiesByPlot;
      }

      public LiveData<List<FarmActivity>> getFarmActivitiesByFarm() {
            return farmActivitiesByFarm;
      }

      public void setFarmActivitiesByFarm(LiveData<List<FarmActivity>> farmActivitiesByFarm) {
            this.farmActivitiesByFarm = farmActivitiesByFarm;
      }

      public LiveData<List<FarmActivity>> getFarmActivitiesByStartDate() {
            return farmActivitiesByStartDate;
      }

      public void setFarmActivitiesByStartDate(LiveData<List<FarmActivity>> farmActivitiesByStartDate) {
            this.farmActivitiesByStartDate = farmActivitiesByStartDate;
      }

      public LiveData<List<FarmActivity>> getFarmActivitiesByRecent() {
            return farmActivitiesByRecent;
      }

      public void setFarmActivitiesByRecent(LiveData<List<FarmActivity>> farmActivitiesByRecent) {
            this.farmActivitiesByRecent = farmActivitiesByRecent;
      }

      public LiveData<List<Member>> getMembers() {
            return members;
      }

      public LiveData<List<Farm>> getFarms() {
            return farms;
      }

      public LiveData<List<AppNotification>> getAppNotifications() {
            return appNotifications;
      }

      public void setAppNotifications(LiveData<List<AppNotification>> appNotifications) {
            this.appNotifications = appNotifications;
      }

      public LiveData<List<ScanModel>> getScoutModelList() {
            return scoutModelList;
      }

      public void setScoutModelList(LiveData<List<ScanModel>> scoutModelList) {
            this.scoutModelList = scoutModelList;
      }

      public LiveData<List<ScanModel>> getPlotScoutModelList() {
            return plotScoutModelList;
      }

      public void setPlotScoutModelList(LiveData<List<ScanModel>> plotScoutModelList) {
            this.plotScoutModelList = plotScoutModelList;
      }

      public LiveData<List<ScoutModel>> getScoutModels() {
            return scoutModels;
      }

      public void setScoutModels(LiveData<List<ScoutModel>> scoutModels) {
            this.scoutModels = scoutModels;
      }

      public LiveData<List<KnowledgeBase>> getKnowledgeBaseList() {
            return knowledgeBaseList;
      }

      public void setKnowledgeBaseList(LiveData<List<KnowledgeBase>> knowledgeBaseList) {
            this.knowledgeBaseList = knowledgeBaseList;
      }

      public String getUserId() {
            return userId;
      }

      public void setUserId(String userId) {
            this.userId = userId;
      }

      public MutableLiveData<String> getCurrentName() {
            if (currentName == null) {
                  currentName = new MutableLiveData<String>();
            }
            return currentName;
      }

      public void setCurrentName(MutableLiveData<String> currentName) {
            this.currentName = currentName;
      }
}
