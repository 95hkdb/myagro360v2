package com.hensongeodata.myagro360v2.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Update;

import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;

@Dao
public abstract class TimestampDataDao {

      @Insert(onConflict = OnConflictStrategy.REPLACE)
      abstract long insert(FarmPlot farmPlot);

      void insertWithTimeStamp(FarmPlot farmPlot){
//            farmPlot.setUpdatedAt(System.currentTimeMillis());
//            farmPlot.setCreatedAt(System.currentTimeMillis());
            insert(farmPlot);
      }

      @Update
      abstract long update(FarmPlot farmPlot);

      void updateWithTimeStamp(FarmPlot farmPlot){
//            farmPlot.setUpdatedAt(System.currentTimeMillis());
            update(farmPlot);
      }

}
