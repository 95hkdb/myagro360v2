package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.model.*;

/**
 * Created by user1 on 10/4/2017.
 */

public class Settings extends BaseActivity {
    private static String TAG=Settings.class.getSimpleName();
    private AppCompatAutoCompleteTextView acWorkArea;
    private TextInputEditText etWorkArea;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private EditText etAccuracy;
    private Switch swPreview;
    private Switch swArchive;
    private Spinner spLang;

    SettingsReceiver receiver;
    IntentFilter filter;

    String selectedDistrict;
    public static int CHOOSE_DISTRICT=100;
    public static int CHOOSE_TOWN=300;
    public static int UPLOAD_PENDING=200;
    private int action=-1;
    private String langCache;

    ArrayAdapter<String>adapter;
    ArrayList<Transaction>transactionPending;
    View bgPending;
    ImageButton ibPending;
    TextView tvPending;
    int initialPending;
    int uploadingID=-1;//save the id of transaction being uploaded to delete transaction later

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
          toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Settings");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close();
            }
        });

        receiver=new SettingsReceiver();
        filter=new IntentFilter(BroadcastCall.SETTINGS_UPDATE);
        registerReceiver(receiver,filter);


          acWorkArea = findViewById(R.id.ac_workarea);
        acWorkArea.setText(user.getWorkarea());
        setTownAutocomplete();
        //acWorkArea.addTextChangedListener(textWatcher);

          etWorkArea = findViewById(R.id.et_workarea);
        etWorkArea.setText(user.getWorkarea());
        etWorkArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(Settings.this,Town.class),CHOOSE_TOWN);
            }
        });

        findViewById(R.id.ib_workarea).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(Settings.this,District.class),CHOOSE_DISTRICT);
            }
        });


          swPreview = findViewById(R.id.sw_review);
        swPreview.setChecked(settings.isPreviewForm());
        swPreview.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                settings.setPreviewForm(b);
            }
        });

          swArchive = findViewById(R.id.sw_archive);
        swArchive.setChecked(settings.isArchive_mode());
        swArchive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                settings.setArchive_mode(b);
            }
        });

          etAccuracy = findViewById(R.id.et_accuracy);
        etAccuracy.setText(""+settings.getAccuracy_max());
        etAccuracy.addTextChangedListener(twAccuracy);

//        langCache=settings.getLang();
//        spLang= (Spinner) findViewById(R.id.spinner);
//        List<String> languages=new ArrayList<>();
//
//        languages.add(langCache.toUpperCase());
//        if(langCache.equalsIgnoreCase(SettingsModel.LAN_ENG))
//                languages.add(SettingsModel.LAN_FR.toUpperCase());
//        else if(langCache.equalsIgnoreCase(SettingsModel.LAN_FR))
//                languages.add(SettingsModel.LAN_ENG.toUpperCase());
//
//        setSpinner(spLang,languages);
//        spLang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                if(i==0)
//                    settings.setLang(spLang.getItemAtPosition(i).toString().toLowerCase());
//                else if(i==1)
//                    settings.setLang(spLang.getItemAtPosition(i).toString().toLowerCase());
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

    }

//    private void setSpinner(Spinner spinner, List<String>list){
//        if(list!=null) {
//           // final int listsize = list.size() - 1;
//            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list) {
////                @Override
////                public int getCount() {
////                    return (listsize); // Truncate the list
////                }
//            };
//
//            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//            spinner.setAdapter(dataAdapter);
//            spinner.setPrompt("Change Language");
//            spinner.setFocusable(true);
//            spinner.setSelection(list.indexOf((new SettingsModel(this)).getLang()));
//            spinner.setEnabled(true);
//        }
//    }
    private void setTownAutocomplete(){
          ArrayList<com.hensongeodata.myagro360v2.model.Town> towns = databaseHelper.getAllItems(DatabaseHelper.TOWN_TBL, "-1");
        List<String> list=new ArrayList<>();

        for(int a=0; a<towns.size();a++){
            String townName=towns.get(a).getName().toLowerCase();
              townName = townName.substring(0, 1).toUpperCase() + townName.substring(1);
            list.add(townName);
        }
        acWorkArea.setAdapter( new ArrayAdapter<String>
                (Settings.this,android.R.layout.select_dialog_item, list));
    }

    private TextWatcher twAccuracy=new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            String value=String.valueOf(charSequence).trim();
            if(!value.isEmpty()&&!value.equalsIgnoreCase(""))
                settings.setAccuracy_max(Integer.parseInt(value));
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private boolean validateAccuracy(){
        String accuracy=etAccuracy.getText().toString().trim();
        if(accuracy.trim().isEmpty()||accuracy.trim().equalsIgnoreCase("null")||Integer.parseInt(accuracy)<=0){
            Toast.makeText(this,"Value set for accuracy is not valid.",Toast.LENGTH_SHORT).show();
            etAccuracy.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== Activity.RESULT_OK) {
            if (requestCode == CHOOSE_DISTRICT){
               // etWorkArea.setText(myState.getString(Keys.SP_STR_DISTRICT));
                acWorkArea.setText(user.getWorkarea());
                setTownAutocomplete();
            }
            else if(requestCode==CHOOSE_TOWN){
                etWorkArea.setText(user.getWorkarea());
            }
        }
    }

    private void close(){
        if(validateAccuracy())
            finish();
    }

    class SettingsReceiver extends BroadcastReceiver {
        private String TAG=SettingsReceiver.class.getSimpleName();
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       // getMenuInflater().inflate(R.menu.settings_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        etWorkArea.setText(user.getWorkarea());
        acWorkArea.setText(user.getWorkarea());
        setTownAutocomplete();
    }

    @Override
    public void onBackPressed() {
        if(validateAccuracy())
                super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
            super.onDestroy();
            databaseHelper.close();
            unregisterReceiver(receiver);
    }
}
