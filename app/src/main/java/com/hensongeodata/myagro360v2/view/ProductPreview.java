package com.hensongeodata.myagro360v2.view;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ProductAdapterRV;
import com.hensongeodata.myagro360v2.model.Product;
import com.squareup.picasso.Picasso;

public class ProductPreview extends BaseActivity {
Product product;
TextView tvPrice;
TextView tvQuantity;
ImageButton ibAdd,ibSub;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_preview);
        product= ProductAdapterRV.productSelected;
        if(product!=null) {
           Product productCached = databaseHelper.getProduct(product.id);
            Log.d("ProductPreview","cachedProduct: "+(productCached==null?"null":""+productCached.status));
           if(productCached!=null&&productCached.status==Product.STATUS_CART)
               product.quantity=productCached.quantity;
           else
               product.quantity=1;
        }
        if(product==null){
            MyApplication.showToast(this,resolveString(R.string.loading), Gravity.CENTER);
            finish();
            return;
        }

          tvPrice = findViewById(R.id.tv_price);
          tvQuantity = findViewById(R.id.tv_quantity);

        ((TextView)findViewById(R.id.tv_caption)).setText(product.category);
        ((TextView)findViewById(R.id.tv_name)).setText(product.name);
        ((TextView)findViewById(R.id.tv_description)).setText(product.description);

        ImageButton ibBack=findViewById(R.id.ib_back);
        ibBack.setImageResource(R.drawable.ic_close_dark);
        ibBack.setVisibility(View.VISIBLE);
        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvPrice.setText("GHc "+product.price);

        Picasso.get().load(
                (product.image))
                .resize(800, 800)
                .centerCrop()
                .placeholder(R.drawable.blurred_image)
                .into((ImageView)findViewById(R.id.iv_image));


        ((TextView)findViewById(R.id.tv_username)).setText("Supplier: "+product.owner_name);

          findViewById(R.id.btn_cart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToCart();
                finish();
            }
        });


        ibSub=findViewById(R.id.ib_minus);
                ibSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(product.quantity>1)
                    product.quantity=product.quantity-1;
                else
                    MyApplication.showToast(ProductPreview.this,"Minimum order quantity reached.",Gravity.CENTER);

                updateQuantity();
            }
        });
        ibAdd=findViewById(R.id.ib_add);
        ibAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(product.quantity<product.max)
                    product.quantity=product.quantity+1;
                else
                    MyApplication.showToast(ProductPreview.this,"Maximum quantity in stock reached.",Gravity.CENTER);

                updateQuantity();
            }
        });
        /*StepperTouch stepperTouch = (StepperTouch) findViewById(R.id.stepper_touch);
        stepperTouch.stepper.setMin(1);
        stepperTouch.stepper.setValue(product.quantity);
        stepperTouch.stepper.setMax(100);
        stepperTouch.stepper.addStepCallback(new OnStepCallback() {
            @Override
            public void onStep(int value, boolean positive) {
                product.quantity=value;
                updateQuantity();
                //Toast.makeText(getApplicationContext(), value + "", Toast.LENGTH_SHORT).show();
            }
        });
*/
        updateQuantity();
    }

    void updateQuantity(){
        tvQuantity.setText("Qty "+product.quantity);
        long priceSum=product.price*product.quantity;
        tvPrice.setText("Ghc "+priceSum);
    }

    private void addToCart(){
        product.status=Product.STATUS_CART;
        databaseHelper.addProduct(product);
        MyApplication.showToast(this,product.name+ " added to cart",Gravity.CENTER);
    }
}
