package com.hensongeodata.myagro360v2.view;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;

import java.util.ArrayList;

import com.hensongeodata.myagro360v2.adapter.HistoryAdapterRV;
import com.hensongeodata.myagro360v2.model.Archive;

/**
 * Created by user1 on 1/25/2018.
 */

public class HistoryList extends SearchableList {
    private static String TAG=HistoryList.class.getSimpleName();
    private HistoryAdapterRV adapter;
    //private ImageButton ibDelete;
    //private Button btnSubmit;
    //private int pendingArchiveSize=0;

    //private ArchiveReceiver receiver;
    //private IntentFilter filter;
   // private ArrayList<Object> adapterArchiveList;
    //private HashMap<String,Boolean> topAction;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fetchHistory();//load archives into list view
        tvCaption.setText("History");
        etSearch.addTextChangedListener(new MyWatcher(HISTORY,adapter));

        bgAction.setVisibility(View.GONE);
    }

    private void fetchHistory(){
        showProgress("Loading History");
        ArrayList<Archive> historyList=databaseHelper.getHistory();

        if(historyList==null||historyList.size()==0){
            recyclerView.setVisibility(View.GONE);
            setTvPlaceholder("No history available");
        }
        else {
            tvPlaceholder.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter = new HistoryAdapterRV(this, historyList);
            recyclerView.setAdapter(adapter);
        }


        hideProgress();

    }


    @Override
    public void onResume() {
        super.onResume();
       // fetchHistory();
        //adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
