package com.hensongeodata.myagro360v2.view;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.fragment.AudioFragment;
import com.hensongeodata.myagro360v2.fragment.LearnFragment;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;

/**
 * Created by user1 on 7/25/2018.
 */

public class LearnContent extends AppCompatActivity {
    private static String TAG= LearnContent.class.getSimpleName();
    int index;
    ViewPager viewPager;
    ViewPagerAdapter adapter;
    private ImageButton ibPrev;
    private ImageButton ibNext;
    private TextView tvPage;
    private TextView tvCaption;
    AudioFragment fragment;
    private View vAudio;
    private boolean audioOn=false;
    private boolean hasAudio=false;
    private boolean mpPrepared=false;
    private String title;
    private String audioUrl;
    // private JcPlayerView audioPlayer;
    MediaPlayer mPlayer;
    private SeekBar seekBar;
    //private LineBarVisualizer lineBarVisualizer;
    private ImageButton ibAudio;
    private Handler handlerSeek;
    private Runnable runnableSeek;
    private ImageView ivAssistant;
    private ImageButton ibReplay;
    private AVLoadingIndicatorView avAssistant;
    private static int ASSIST_LOADING=100;
    private static int ASSIST_ACTIVE=200;
    private static int ASSIST_REPLAY=300;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
          super.onCreate(savedInstanceState);
          setContentView(R.layout.kb_content_home);

          init();

//        plusInit();

    }

      private void init() {

            index=getIntent().getIntExtra("kb_id",0);
//            title = LearnHomeFragment.knowledgeBases.get(index).getTitle();
            title= LibraryActivity.knowledgeBases.get(index).getTitle();

            tvCaption=findViewById(R.id.tv_caption);
            vAudio = findViewById(R.id.v_audio);
            tvPage = findViewById(R.id.tv_page);
            viewPager = findViewById(R.id.viewpager_kb);

            ivAssistant = findViewById(R.id.iv_assistant);
            ibReplay = findViewById(R.id.ib_replay);
            avAssistant = findViewById(R.id.av_assistant);
            ibAudio = findViewById(R.id.ib_audio);

            tvCaption.setText(title);
            adapter = new ViewPagerAdapter(getSupportFragmentManager());
            viewPager.setAdapter(adapter);
            viewPager.setCurrentItem(index);

            findViewById(R.id.ib_close).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        finish();
                  }
            });

            tvPage.setText("Topic " + (index + 1) + " of " + LibraryActivity.knowledgeBases.size());

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                  @Override
                  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                  }

                  @Override
                  public void onPageSelected(int position) {
                        title = LibraryActivity.knowledgeBases.get(position).getTitle();
                        tvCaption.setText(title);
                        tvPage.setText("Topic " + (position + 1) + " of " + LibraryActivity.knowledgeBases.size());

                        audioUrl = LibraryActivity.knowledgeBases.get(position).getAudio();
                        hasAudio = audioUrl != null && !audioUrl.trim().isEmpty();

                        if(hasAudio) {
                              audioOn=false;
                              toggleAudio();
                        } else
                              toggleAudio();

                  }

                  @Override
                  public void onPageScrollStateChanged(int state) {

                  }
            });

            findViewById(R.id.ib_back).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        if(viewPager.getCurrentItem()>0)
                              viewPager.setCurrentItem(viewPager.getCurrentItem()-1);

                  }
            });

            findViewById(R.id.ib_next).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        if ((viewPager.getCurrentItem() + 1) < LibraryActivity.knowledgeBases.size())
                              viewPager.setCurrentItem(viewPager.getCurrentItem()+1);

                  }
            });

            ibReplay.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        audioOn=false;
                        toggleAudio();
                  }
            });

            audioUrl = LibraryActivity.knowledgeBases.get(index).getAudio();
            hasAudio = audioUrl != null && !audioUrl.trim().isEmpty();

            seekBar=findViewById(R.id.seekbar);
//            handlerSeek=new Handler();
//            runnableSeek=new Runnable() {
//                  @Override
//                  public void run() {
//                        seekBar.setProgress(mPlayer.getCurrentPosition());
//                        handlerSeek.postDelayed(this, 5);
//                  }
//            };

            addAudioFragment();

            ibAudio.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        toggleAudio();
                  }
            });
            toggleAudio();

      }

    private void addAudioFragment(){
//        FragmentManager fm = getSupportFragmentManager();
//        fragment = new AudioFragment();
//        fm.beginTransaction().add(vAudio.getId(), fragment).commit();

        //   audioPlayer= (JcPlayerView) findViewById(R.id.player_audio);
        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                switchAssistantDisplay(ASSIST_REPLAY);
            }
        });
        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mPlayer=mediaPlayer;
                mpPrepared=true;
                startAudio();
            }
        });

//        if(hasAudio)
//            playAudio(title,audioUrl);
    }

    private void playAudio(String title,String url){
        switchAssistantDisplay(ASSIST_LOADING);

        (new Handler()).post(new Runnable() {
            @Override
            public void run() {

                //   if(url==null)
                //      audioUrl="http://www.noiseaddicts.com/samples_1w72b820/2558.mp3";
                try{

                    mPlayer.setDataSource(audioUrl);
                    mPlayer.prepareAsync();
                }catch (IOException e){
                    // Catch the exception
                    e.printStackTrace();
                }catch (IllegalArgumentException e){
                    e.printStackTrace();
                }catch (SecurityException e){
                    e.printStackTrace();
                }catch (IllegalStateException e){
                    e.printStackTrace();
                }

            }
        });
    }

    private void startAudio(){
        switchAssistantDisplay(ASSIST_ACTIVE);
        mPlayer.seekTo(0);
        mPlayer.start();
          seekBar.setMax(mPlayer.getDuration());
             handlerSeek.postDelayed(runnableSeek, 5);
        mPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            public void onBufferingUpdate(MediaPlayer mp, int percent)
            {
                double ratio = percent / 100.0;
                int bufferingLevel = (int)(mp.getDuration() * ratio);

                seekBar.setSecondaryProgress(bufferingLevel);
            }

        });

    }

    private void toggleAudio(){
        if(audioOn||!hasAudio){
            vAudio.setVisibility(View.GONE);
            ibAudio.setImageResource(R.drawable.baseline_voice_over_off);

            if(mPlayer!=null) {
                mPlayer.stop();
                mPlayer.reset();
                mpPrepared=false;
            }
        }
        else {
            vAudio.setVisibility(View.VISIBLE);
            ibAudio.setImageResource(R.drawable.baseline_record_voice);

            if(mPlayer!=null&&mpPrepared) {
                mPlayer.stop();
                mPlayer.reset();
                mpPrepared=false;
            }

            playAudio(title,audioUrl);
        }

        audioOn=!audioOn;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mPlayer!=null) {
            mPlayer.stop();
            mPlayer.release();
        }
    }

    private void switchAssistantDisplay(int mode){
        ivAssistant.setVisibility(View.GONE);
        ibReplay.setVisibility(View.GONE);
        avAssistant.setVisibility(View.GONE);
        String msg="Audio assistant";

        if(mode==ASSIST_LOADING) {
            avAssistant.setVisibility(View.VISIBLE);
            msg="Preparing audio";
        }
        else if(mode==ASSIST_ACTIVE) {
            ivAssistant.setVisibility(View.VISIBLE);
            msg="Playing audio";
        }
        else if(mode==ASSIST_REPLAY) {
            ibReplay.setVisibility(View.VISIBLE);
            msg="Audio ended";
        }

          Toast.makeText(LearnContent.this, msg, Toast.LENGTH_SHORT).show();
    }

    public static class ViewPagerAdapter extends FragmentStatePagerAdapter {

          private static String TAG= ViewPagerAdapter.class.getSimpleName();
        private LearnFragment learnFragment;

          public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            learnFragment=new LearnFragment();
        }

        @Override
        public int getCount() {
              return LibraryActivity.knowledgeBases.size();
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment=learnFragment.newInstance(position);
            return fragment;
        }

    }


}
