package com.hensongeodata.myagro360v2.view;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.Interface.OnImageReady;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.controller.Utility;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.SettingsModel;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

import id.zelory.compressor.Compressor;
import io.fotoapparat.Fotoapparat;
import io.fotoapparat.configuration.UpdateConfiguration;
import io.fotoapparat.error.CameraErrorListener;
import io.fotoapparat.exception.camera.CameraException;
import io.fotoapparat.parameter.ScaleType;
import io.fotoapparat.preview.Frame;
import io.fotoapparat.preview.FrameProcessor;
import io.fotoapparat.result.PhotoResult;

import static io.fotoapparat.selector.FlashSelectorsKt.autoFlash;
import static io.fotoapparat.selector.FlashSelectorsKt.off;
import static io.fotoapparat.selector.FlashSelectorsKt.torch;
import static io.fotoapparat.selector.LensPositionSelectorsKt.back;

/**
 * Created by user1 on 5/15/2018.
 */

public class CameraPhoto extends MyCamera {
    private static String TAG=CameraPhoto.class.getSimpleName();
    private Image image;
    private Handler handler;
    private Runnable snapRunnable;
    private TextView tvStop;
    private TextView tvMaxShot;
    private ImageView ivPreview;
    private View vTop;
    private View vPreview;
    public static int MAX_SHOT=10;
    private Bitmap mBitmap;
    private SettingsModel settingsModel;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_photo);
        settingsModel=new SettingsModel(this);
          cameraView = findViewById(R.id.camera_view);
          ibFlash = findViewById(R.id.ib_flash);
        ibFlash.setImageResource(R.drawable.ic_flash_auto);

        vCapture =findViewById(R.id.btn_snap);
        vCaptureInner=findViewById(R.id.snap_inner);
        vCaptureInner.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/10,1.0f,0.0f));

        vCaptureOutter=findViewById(R.id.snap_outter);
        vCaptureEffect=findViewById(R.id.snap_effect);

          tvFlash = findViewById(R.id.tv_flash);

          tvSnap = findViewById(R.id.tv_snap);
          tvAction = findViewById(R.id.tv_action);

          cameraView = findViewById(R.id.camera_view);
        fotoapparat= createFotoapparatImage();
          tvStop = findViewById(R.id.tv_stop);
        tvStop.setText("End");
          ibStop = findViewById(R.id.ib_stop);
        ibStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopCamera();
            }
        });


        updateFlash(FLASH_AUTO);
        updateFotoapparat(FLASH_AUTO);

        vCapture.setOnClickListener(snapClickListener);
        tvSnap.setText("Tap to take photo");

        handler=new Handler();
        snapRunnable=new Runnable() {
            @Override
            public void run() {
                vCapture.setOnClickListener(snapClickListener);
                vCaptureInner.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/12,1.0f,0.0f));
                vCapture.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/10,1.2f,1.0f));
                vCaptureEffect.setVisibility(View.GONE);
            }
        };

        vTop=findViewById(R.id.v_top);
          ivPreview = findViewById(R.id.iv_preview);
          tvMaxShot = findViewById(R.id.tv_max);

        tvMaxShot.setText("Shots remaining: "+(MAX_SHOT-MyApplication.imageMultiArrayList.get(index).getImages().size()));
        MyApplication.imageMultiArrayList.get(index).setOnImageReady(new OnImageReady() {
            @Override
            public void onReady() {
                Log.d(TAG,"onReady");
                int size=MyApplication.imageMultiArrayList.get(index).getImages().size();
                int mSize=size-1;
                if(mSize>=0){
                    final Uri uri=MyApplication.imageMultiArrayList.get(index).getImages().get(mSize).getUri();
                    Log.d(TAG,"uri: "+uri);
                    (new Handler()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            processShot(uri);
                        }
                    },3000);
                   // vPreview.setVisibility(View.VISIBLE);
                }

            }
        });

        vPreview=findViewById(R.id.v_preview);
        (findViewById(R.id.ib_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //processShot();

        (findViewById(R.id.btn_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        (findViewById(R.id.btn_repeat)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vPreview.setVisibility(View.GONE);
            }
        });
    }

    View.OnClickListener snapClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            snapEffectImage();
            snap();
        }
    };

    protected void startCamera(){
        if (Utility.checkPermission(this)) {
            fotoapparat.start();
        }
        else {
            Toast.makeText(this,"Camera permission required to continue",Toast.LENGTH_SHORT).show();
        }
    }

    protected void stopCamera(){
        if (Utility.checkPermission(this)) {
            fotoapparat.stop();
        }
        else {
            Toast.makeText(this,"Camera permission required to continue",Toast.LENGTH_SHORT).show();
        }

        finish();
    }

    protected void updateFlash(String flash){
        tvFlash.setText(flash);
        if(flash.equalsIgnoreCase(FLASH_AUTO))
            ibFlash.setOnClickListener(new FlashListener(FLASH_ON));
        else if(flash.equalsIgnoreCase(FLASH_ON))
            ibFlash.setOnClickListener(new FlashListener(FLASH_OFF));
        else if(flash.equalsIgnoreCase(FLASH_OFF))
            ibFlash.setOnClickListener(new FlashListener(FLASH_AUTO));
    }

    protected void updateFotoapparat(String flash) {
        Log.d(TAG,"fotoapparat: "+fotoapparat+" flash: "+flash);
        if (fotoapparat != null&&flash!=null) {
            fotoapparat.updateConfiguration(
                    flashConfiguration(flash)
            );
        }
    }

    protected UpdateConfiguration flashConfiguration(String flash){
        Log.d(TAG,"config flash: "+flash);
        if(flash.equalsIgnoreCase(FLASH_AUTO))
            return UpdateConfiguration.builder()
                    .flash(
                            autoFlash()
                    )
                    .build();
        else if(flash.equalsIgnoreCase(FLASH_ON))
            return UpdateConfiguration.builder()
                    .flash(
                            torch()
                    )
                    .build();
        else
            return UpdateConfiguration.builder()
                    .flash(
                            off()
                    )
                    .build();
    }

    class FlashListener implements View.OnClickListener{
        String flash;
        FlashListener(String flash){
            this.flash=flash;
        }
        @Override
        public void onClick(View view) {
            if(flash.equalsIgnoreCase(FLASH_AUTO)){
                ibFlash.setImageResource(R.drawable.ic_flash_auto);
            }
            else if(flash.equalsIgnoreCase(FLASH_ON)){
                ibFlash.setImageResource(R.drawable.ic_flash_on);
            }
            else if(flash.equalsIgnoreCase(FLASH_OFF)){
                ibFlash.setImageResource(R.drawable.ic_flash_off);
            }

            updateFotoapparat(flash);
            updateFlash(flash);
        }
    }

    private void snapEffectImage(){
        vCapture.setOnClickListener(null);
        vCaptureEffect.setVisibility(View.VISIBLE);
        vCaptureInner.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/12,0.0f,1.0f));
        vCapture.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/10,1.0f,1.2f));
        handler.postDelayed(snapRunnable,DURATION_SNAP/2);
    }

    private Fotoapparat createFotoapparatImage() {
        return Fotoapparat
                .with(this)
                .into(cameraView)
                .previewScaleType(ScaleType.CenterCrop)
                .lensPosition(back())
                .flash(autoFlash())
                .frameProcessor(new SampleFrameProcessor())
                .cameraErrorCallback(new CameraErrorListener() {
                    @Override
                    public void onError(@NotNull CameraException e) {
                        Toast.makeText(CameraPhoto.this, e.toString(), Toast.LENGTH_LONG).show();
                    }
                })
                .build();
    }

    private class SampleFrameProcessor implements FrameProcessor {
        @Override
        public void process(@NotNull Frame frame) {
            // Perform frame processing, if needed
        }
    }


    private void snap() {
        Log.d(TAG,"snap");
        PhotoResult photoResult = fotoapparat.takePicture();
        image=new Image();
        image.setId(index+" Image "+MyApplication.imageMultiArrayList.get(index).getImages().size());

        String fileName=image.getId()+(new CreateForm(this)).getCurrentTime();
       // settingsModel.setFile_counter(settingsModel.getFile_counter()+1);

         fileName=fileName.replaceAll("\\s","");
         fileName=fileName.replaceAll(":","");
         fileName=fileName.trim();
       // Log.d(TAG,"fileName: "+fileName);

        File file=new File(getExternalFilesDir("photos"), fileName+".jpg");
        photoResult.saveToFile(file);
        photoResult.saveToFile(file);
        image.setUri(Uri.fromFile(file));

        File compressedImageFile=null;

        MyApplication.imageMultiArrayList.get(index).addImage(image);

        //processShot(Uri.fromFile(file));
    }

    private void processShot(Uri uri){
        int mIndex=MyApplication.imageMultiArrayList.get(index).getImages().size();
        Image savedImage=MyApplication.imageMultiArrayList.get(index).getImages().get(mIndex-1);
       if(savedImage!=null&&savedImage.getUri()!=null) {
        //compress and update image at index-1
           File file = new File(savedImage.getUri().getPath());
           MyApplication.imageMultiArrayList.get(index).getImages().set(mIndex - 1, compress(file));
       }
        if(mIndex<MAX_SHOT){

            tvMaxShot.setText("Shots remaining: "+(MAX_SHOT-mIndex));
            updatePreviewImage(uri);

            vPreview.setVisibility(View.VISIBLE);
        }
        else {
            (new Handler()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(CameraPhoto.this,"Image limit ("+MAX_SHOT+") reached.",Toast.LENGTH_SHORT).show();
                    finish();
                }
            },500);
        }
    }

    private Image compress(File file){
        Image image=new Image();
        File compressedImageFile=null;

        try {
            if(file!=null) {
                compressedImageFile = new Compressor(this).compressToFile(file);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(compressedImageFile!=null){
            image.setUri(Uri.fromFile(compressedImageFile));
        }
        else
            image.setUri(Uri.fromFile(file));

        return image;
    }

    private void updatePreviewImage(final Uri uri){
        Log.d(TAG,"2 uri: "+uri+" path: "+uri.getPath());
                ivPreview.setImageBitmap(myApplication.bitmapCompressLarge(
                        (uri.getPath())));

    }

    private void endTopAnimation(){
        vTop.startAnimation(LocsmmanEngine.ScaleOnce(400,1.1f,1.0f));
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"onStart camera");
        startCamera();
    }

    @Override
    protected void onStop() {
        super.onStop();
       stopCamera();
    }

}
