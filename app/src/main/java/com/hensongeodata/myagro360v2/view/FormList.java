package com.hensongeodata.myagro360v2.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.view.View;

import java.util.ArrayList;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.FormAdapterRV;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.model.Form;
import com.hensongeodata.myagro360v2.model.Organization;

/**
 * Created by user1 on 1/25/2018.
 */

public class FormList extends SearchableList {
    private static String TAG=FormList.class.getSimpleName();
    private static String ACTION_SEARCH="action_search";
    private static String ACTION_NORMAL="action_normal";

    public static String ACTION_SELECT="item selection action";
    public static String ACTION_DOWNLOAD="download action";

    private FormAdapterRV adapter;

    private Organization organization;
    FormListReceiver receiver;
    IntentFilter filter;

    String org_id;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        receiver=new FormListReceiver();
        filter=new IntentFilter(BroadcastCall.FORM_LIST);
        registerReceiver(receiver,filter);

        org_id=getIntent().getExtras().getString("org_id");

        if(org_id==null)
            org_id=settings.getOrg_id();

        organization=databaseHelper.getOrganization(org_id);

        if(org_id!=null&&!org_id.trim().equalsIgnoreCase("")) {
            tvCaption.setText(organization.getName());
            fetchForms(org_id);
        }
        else {
            finish();
            return;
        }
        //recyclerView.setAdapter(adapter);
        etSearch.addTextChangedListener(new MyWatcher(FORM,adapter));

        fabAction.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_update));
        fabAction.setVisibility(View.VISIBLE);
        fabAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(myApplication.hasNetworkConnection(FormList.this)) {
                    showProgress("Updating Forms...");
                    networkRequest.fetchForms(BroadcastCall.FORM_LIST);
                }
                else
                    myApplication.showInternetError(FormList.this);
            }
        });
    }

    private void fetchForms(String org_id){
        showProgress("Loading Forms");
        ArrayList<Form> forms=databaseHelper.getForms(org_id);
        hideProgress();

        if(forms==null||forms.size()<1){
            recyclerView.setVisibility(View.GONE);
            setTvPlaceholder("No Forms available");
        }
        else {
            tvPlaceholder.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter = new FormAdapterRV(this, forms);
            recyclerView.setAdapter(adapter);
        }
        // return organizations;
    }

    class FormListReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {


            String action=intent.getExtras().getString("action");
            if(action!=null) {
                if (action.equalsIgnoreCase(ACTION_SELECT)) {
                    createForm.setShouldRefreshForm(true);//allow form refresh on main
                    //a Form was selected
                    String formID=intent.getExtras().getString("formID");
                    settings.setForm_id(formID);

                    Form form=databaseHelper.getForm(formID);
                    settings.setOrg_id(form.getOrganization().getId());
                    settings.setTransaction_id(null);//NB transaction_id is only used when previewing from transaction

                    //Intent i=new Intent(FormList.this,MainActivity.class);
                    //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    //startActivity(i);
                    hideProgress();
                    finish();
                }
                else if(action.equalsIgnoreCase(ACTION_DOWNLOAD)) {
                    //JoinCommunityResponse from download request
                    fetchForms(org_id);//reload organization list since it's been updated
                    hideProgress();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
        hideProgress();
    }
}
