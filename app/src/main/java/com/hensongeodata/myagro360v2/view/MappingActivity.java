package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.Interface.HistoryTabListener;
import com.hensongeodata.myagro360v2.Interface.Refresh;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.adapter.HistoryScanTabsRV;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.fragment.HistoryMappingFragAutomated;
import com.hensongeodata.myagro360v2.fragment.HistoryScanFragment;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.model.SharePrefManager;

import java.util.ArrayList;
import java.util.HashMap;

import static android.graphics.Color.TRANSPARENT;


public class MappingActivity extends BaseActivity implements HistoryTabListener,Refresh {
    private static String TAG= MappingActivity.class.getSimpleName();

    protected RecyclerView recyclerView;
    protected HistoryScanTabsRV adapter;
    protected LocsmmanEngine locsmmanEngine;
    protected TextView tvCaption;

    public ImageButton ibTopLeft;
    protected ImageButton ibTopRight;
    private Context context;
    private TextView tvPlaceholder;
    ArrayList<String> headers;
    ArrayList<HashMap<String,String>>items;
    private int mode;
    private String form_id;
    private boolean refresh=false;
    private Button btnAdd;
    public static Refresh refreshListener;
    public static String mScout_id;
    public static int SCOUT=100;
    public static int FORM=200;
    public static boolean submitted=false;

      //    Form for mapping
      Dialog myDialog;
      Button btnNext;
      EditText textLabel;
      EditText textComment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_scan);
        locsmmanEngine=new LocsmmanEngine(this);
        mode=getIntent().getIntExtra("mode",-1);
        form_id=getIntent().getStringExtra("form_id");

        refreshListener=this;
          myDialog = new Dialog(this);

          ibTopLeft = findViewById(R.id.ib_back);
          ibTopRight = findViewById(R.id.ib_action);
        ibTopRight.setImageResource(R.drawable.ic_close_dark);
        ibTopRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

          tvPlaceholder = findViewById(R.id.tv_placeholder);
        btnAdd=findViewById(R.id.btn_add);

        (findViewById(R.id.btn_add)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"add click "+mode);
                fillFormMap();
                  if (mode == ChatbotAdapterRV.MODE_MAP) {
                    openMapping();
                        fillFormMap();
                  }
                else if(mode==ChatbotAdapterRV.MODE_SCOUT)
                    openScouting();
            }
        });

        ((TextView)findViewById(R.id.tv_caption))
                .setText(mode==ChatbotAdapterRV.MODE_SCOUT?resolveString(R.string.scout_history)
                        : resolveString(R.string.mapping_history));

          recyclerView = findViewById(R.id.recyclerview_horizontal);
          recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));

        init();
    }

    private void init(){
        items=new ArrayList<>();

        if(form_id!=null)
            headers=databaseHelper.getScoutIDList(form_id,mode);
        else
            headers=databaseHelper.getScoutIDList(mode);

        if(headers!=null)
            Log.d(TAG,"headers size: "+headers.size());

        if(headers!=null&&headers.size()>0){
            int index=0;
            int tag= headers.size();
            for(String header:headers){
                HashMap<String, String >map=new HashMap<>();
                map.put("value",header);
                map.put("id",""+tag);
                map.put("focus", ""+(index==0? 1:0));
                items.add(map);

                index++;
                tag--;
            }
            addFragment(headers.get(0),true);
            tvPlaceholder.setVisibility(View.GONE);
        }
        else{
            tvPlaceholder.setVisibility(View.VISIBLE);
        }


        Log.d(TAG,"item size: "+items.size());
        adapter=new HistoryScanTabsRV(this, items, this, new Refresh() {
            @Override
            public void reload(String scout_id) {
                startActivity(getIntent());
                finish();
            }

            @Override
            public void moreClicked(String title, String sectionId) {

            }

            @Override
            public void shareButtonClicked(String section_id) {

            }

            @Override
            public void onSyncButtonClicked() {

            }


        });
        adapter.setMode(mode);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"onResume");
        if(submitted){
            refresh();
            submitted=false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       Log.d(TAG,"onActivityResult");
        if(resultCode== Activity.RESULT_OK){
            Log.d(TAG,"onActivityResult ok. requestcode: "+requestCode);
            if(requestCode==SCOUT){
                startActivity(getIntent());
                finish();
            }else if(requestCode==FORM){
                refresh();
            }
        }
    }

    private void refresh(){
        if(MainActivityAutomated.mapModel!=null&&MainActivityAutomated.mapModel.id!=null) {
            databaseHelper.removeScoutListItem(MainActivityAutomated.mapModel.id, ChatbotAdapterRV.MODE_MAP);
            MainActivityAutomated.mapModel=new MapModel();
            startActivity(getIntent());
            finish();
        }
        else {
            mScout_id = null;
            init();
        }
    }

      private void fillFormMap() {
            myDialog.setContentView(R.layout.form_map);
            btnNext = myDialog.findViewById(R.id.btn_map_dialog);
            textComment = myDialog.findViewById(R.id.textComment);
            textLabel = myDialog.findViewById(R.id.textMapLabel);

            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.green(TRANSPARENT)));
            myDialog.show();
//        myDialog.setCanceledOnTouchOutside(false);
      }

      public void dialogForm(View view) {

            String label = textLabel.getText().toString();
            String comment = textComment.getText().toString();
            if (label.isEmpty())
                  textLabel.setError("Field required!");
            else {
                  if (comment.isEmpty())
                        comment = "";
                  SharePrefManager.getInstance(getApplicationContext()).formMapping(label, comment);
                  myDialog.dismiss();

                  openMapping();
            }
      }

    private void openMapping(){
        Log.d(TAG,"openMapping");
        Intent mapping=new Intent(this,ScoutAutomated.class);
        mapping.putExtra("mode",ChatbotAdapterRV.MODE_MAP);
        startActivityForResult(mapping,SCOUT);
        refresh=true;
    }

    private void openScouting(){
        Intent scouting=new Intent(this,ScoutAutomated.class);
        scouting.putExtra("mode",ChatbotAdapterRV.MODE_SCOUT);
        startActivityForResult(scouting,SCOUT);
        refresh=true;
    }

    @Override
    public void onSelected(String item) {
        addFragment(item,false);
    }

    private void addFragment(String section_id, boolean firstTime){
        Bundle bundle = new Bundle();
        bundle.putString("section_id", section_id);
        FragmentManager fm = getSupportFragmentManager();

        if(mode==ChatbotAdapterRV.MODE_SCOUT) {
            HistoryScanFragment fragment = new HistoryScanFragment();
            fragment.setArguments(bundle);

            if (firstTime)
                fm.beginTransaction().add(R.id.v_container, fragment).commit();
            else
                fm.beginTransaction().replace(R.id.v_container, fragment).commit();
        }
        else {
            HistoryMappingFragAutomated fragment=new HistoryMappingFragAutomated();
            fragment.setArguments(bundle);
            if(firstTime)
                fm.beginTransaction().add(R.id.v_container,fragment).commit();
            else
                fm.beginTransaction().replace(R.id.v_container,fragment).commit();
        }
        }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.shutdownLive();
    }

    @Override
    public void reload(String scout_id) {
        Log.d(TAG,"reload scout_id: "+scout_id);
       mScout_id=scout_id;
    }

    @Override
    public void moreClicked(String title, String sectionId) {

    }

    @Override
    public void shareButtonClicked(String section_id) {

    }

    @Override
    public void onSyncButtonClicked() {

    }

}
