package com.hensongeodata.myagro360v2.view;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.R;


/**
 * Created by user1 on 9/11/2017.
 */

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
          TextView tvAppName = findViewById(R.id.tv_app_name);
        tvAppName.setText(getResources().getString(R.string.app_name));

          TextView tvVersion = findViewById(R.id.tv_version);
        tvVersion.setText(getResources().getString(R.string.version));

          TextView tvCopyright = findViewById(R.id.tv_copyright);
        tvCopyright.setText(getResources().getString(R.string.copyright));

          TextView tvReserved = findViewById(R.id.tv_reserved);
        tvReserved.setText(getResources().getString(R.string.reserved));


        (findViewById(R.id.ib_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
