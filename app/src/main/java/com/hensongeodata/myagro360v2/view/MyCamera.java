package com.hensongeodata.myagro360v2.view;

import android.hardware.Camera;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.CameraPermissionsDelegate;

import io.fotoapparat.Fotoapparat;
import io.fotoapparat.view.CameraView;

/**
 * Created by user1 on 5/15/2018.
 */

public class MyCamera extends BaseActivity {
    private static String TAG=MyCamera.class.getSimpleName();
    public static boolean autoStart=false;
    public static int ACTION_PHOTO=100;
    public static int ACTION_VIDEO=200;
    public static int ACTION_SCAN=300;
    public static int action;
    protected int index;//index of Image/Video global list to update after capturing
    private LinearLayout vControls;
    protected ImageButton ibFlash;
    protected ImageButton ibStop;
    protected View vCapture;
    protected View vCaptureOutter;
    protected View vCaptureInner;
    protected View vCaptureEffect;
    protected ImageView ivRecording;
    protected View vAction;

    protected TextView tvFlash;
    protected TextView tvSnap;
    protected TextView tvAction;

    protected static String FLASH_ON;
    protected static String FLASH_OFF;
    protected static String FLASH_AUTO;
    private String flash;//=FLASH_AUTO;

    protected FrameLayout cameraViewVideo;
    protected CameraView cameraView;
    protected Fotoapparat fotoapparat;
    protected Camera camera;

    protected boolean hasCameraPermission;
    protected MyApplication myApplication;

    public static long DURATION_SNAP=2000;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FLASH_ON=resolveString(R.string.on);
        FLASH_OFF=resolveString(R.string.off);
        FLASH_AUTO=resolveString(R.string.auto);

        if(getIntent().getExtras()!=null)
                index=getIntent().getExtras().getInt("index");

          cameraViewVideo = findViewById(R.id.camera_preview);

        hasCameraPermission = (new CameraPermissionsDelegate(this)).hasCameraPermission();

        myApplication=new MyApplication();

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }



}
