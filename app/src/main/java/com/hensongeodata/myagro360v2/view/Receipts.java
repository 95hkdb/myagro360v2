package com.hensongeodata.myagro360v2.view;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.PaymentHistoryAdapterRV;
import com.hensongeodata.myagro360v2.model.PaymentReceipt;

import java.util.ArrayList;

public class Receipts extends SearchableList {
PaymentHistoryAdapterRV adapter;
ArrayList<PaymentReceipt> receipts;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fetchReceipts();//load archives into list view
        tvCaption.setText(resolveString(R.string.receipts));
        etSearch.addTextChangedListener(new MyWatcher(RECEIPTS,adapter));

        bgAction.setVisibility(View.GONE);

    }

    void fetchReceipts(){
        showProgress(resolveString(R.string.loading));
        receipts=databaseHelper.getReceipts(this);

        if(receipts==null||receipts.size()==0){
            recyclerView.setVisibility(View.GONE);
            setTvPlaceholder(resolveString(R.string.nothing_to_display));
        }
        else {
            tvPlaceholder.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter = new PaymentHistoryAdapterRV(this, receipts);
            recyclerView.setAdapter(adapter);
        }


        hideProgress();
    }
}
