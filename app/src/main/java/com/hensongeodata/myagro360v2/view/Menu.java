package com.hensongeodata.myagro360v2.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.model.Product;
import com.hensongeodata.myagro360v2.model.SettingsModel;

import java.util.ArrayList;

/**
 * Created by user1 on 7/20/2018.
 */

public class Menu extends BaseActivity{

    View vShop,vScan,vScout,vMapping, vForms,vChat,vLearn, vShare;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        (new SettingsModel(this)).setArchive_mode(false);

        setContentView(R.layout.menu);
        vShop=findViewById(R.id.v_shop_);
        setupView(vShop,R.drawable.ic_cart,resolveString(R.string.shop));

        vMapping=findViewById(R.id.v_mapping);
        setupView(vMapping,R.drawable.ic_map,resolveString(R.string.mapping));

        vScout=findViewById(R.id.v_scout);
//        setupView(vScout,R.drawable.ic_scout,resolveString(R.string.));

        vForms =findViewById(R.id.v_forms_);
        setupView(vForms,R.drawable.ic_form_white,resolveString(R.string.forms_go_to));

        vChat =findViewById(R.id.v_chat_);
        setupView(vChat,R.drawable.ic_chat,resolveString(R.string.chat));


        vShare =findViewById(R.id.v_share_);
        setupView(vShare,R.drawable.ic_share,resolveString(R.string.share_location));

        vScan =findViewById(R.id.v_scan);
        setupView(vScan,R.drawable.ic_scan_white,resolveString(R.string.scan));

        vLearn =findViewById(R.id.v_learn);
        setupView(vLearn,R.drawable.ic_knowledge,resolveString(R.string.faw_knowledge));


        vShop.setOnClickListener(
                new MyListener(new Intent(this,BrowseToBuy.class),false));

   /*   vShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onAddMapButtonClicked(View view) {
                Toast.makeText(Menu.this,resolveString(R.string.coming_soon),Toast.LENGTH_SHORT).show();
            }
        });*/
        alternateColor(vShop);

        //alternateColor(vChat);
        vChat.setOnClickListener
                (new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openWesync();
                    }
                });
        alternateColor(vChat);

        Intent mapping=new Intent(this, MappingActivity.class);
        mapping.putExtra("mode",ChatbotAdapterRV.MODE_MAP);
        vMapping.setOnClickListener(
                new MyListener(mapping,false));
        alternateColor(vMapping);


        Intent scouting=new Intent(this, MappingActivity.class);
        scouting.putExtra("mode",ChatbotAdapterRV.MODE_SCOUT);
        vScout.setOnClickListener(
                new MyListener(scouting,false));
        alternateColor(vScout);


        alternateColor(vForms);
        vForms.setOnClickListener
                (new MyListener(new Intent(this,MainActivity.class),true));

        alternateColor(vShare);
        vShare.setOnClickListener
                (new MyListener(new Intent(this,ShareLocation.class),false));


        vScan.setOnClickListener(
                new MyListener(new Intent(this, ScanActivity.class),false));
        alternateColor(vScan);

        vLearn.setOnClickListener(
                new MyListener(new Intent(this, LibraryActivity.class),false));
        alternateColor(vLearn);

  /*      alternateColor(vChat);
        vChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onAddMapButtonClicked(View view) {
                Toast.makeText(Menu.this,resolveString(R.string.coming_soon),Toast.LENGTH_SHORT).show();
            }
        });*/

        (findViewById(R.id.ib_user_)).setOnClickListener(
                new MyListener(new Intent(this,ProfileUpdateActivity.class),false));
        hideSystemUI();

        /*findViewById(R.id.ib_language).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onAddMapButtonClicked(View view) {
                showLanguageOptions(view);
            }
        });*/
        findViewById(R.id.ib_language).setVisibility(View.GONE);

    }

    private void showLanguageOptions(View v){
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if(menuItem.getItemId()==R.id.action_french){
                }
                else if(menuItem.getItemId()==R.id.action_swahili){

                }
                else if(menuItem.getItemId()==R.id.action_english){

                }
                return true;
            }
        });
        popup.inflate(R.menu.language);
        popup.show();
    }

    private void alternateColor(View v){
        v.setBackgroundColor(getResources().getColor(R.color.colorAccent));
    }

    private void setupView(View baseView,int iconSrc,String tag){
        ((ImageView)baseView.findViewById(R.id.iv_icon)).setImageResource(iconSrc);
        ((TextView)baseView.findViewById(R.id.tv_tag)).setText(tag);
    }

    class MyListener implements View.OnClickListener{
        Intent i;
        boolean finish=false;
        public MyListener(Intent i, boolean finish){
            this.i=i;
            this.finish=finish;
        }
        @Override
        public void onClick(View view) {
            //user.setChatroomActive_id(null);
            if(finish)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            startActivity(i);
            if(finish)
                finish();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    void openWesync(){
        String packageName="com.hensongeodata.wesync";
        Intent intent = getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent == null) {
            // Bring user to the market or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            //intent.setData(Uri.parse("market://details?id=" + packageName));
            intent.setData(Uri.parse("https://1drv.ms/u/s!AqpaVLehV_otfY6BsreT57ifXTg"));

            startActivity(intent);
            return;
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction("com.hensongeodata");
        intent.putExtra("email",user.getEmail());
        intent.putExtra("password",user.getPassword());

        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateShopCounter();
    }

    void updateShopCounter(){
        ArrayList<Product> prods=databaseHelper.getProducts(Product.STATUS_CART);
        View vCount=vShop.findViewById(R.id.v_count);
        TextView tvCount=vCount.findViewById(R.id.tv_count);
        if(prods!=null&&prods.size()>0){
            int cart_size=prods.size();
            if(cart_size<100)
                tvCount.setText(""+cart_size);
            else
                tvCount.setText("99+");

            vCount.setVisibility(View.VISIBLE);

        }
        else {
            vCount.setVisibility(View.GONE);
        }
    }

}
