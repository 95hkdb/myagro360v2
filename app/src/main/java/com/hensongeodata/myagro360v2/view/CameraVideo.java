package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaMetadataRetriever;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.CameraPreview;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.controller.Utility;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by user1 on 5/17/2018.
 */

public class CameraVideo extends MyCamera {

    private static final String TAG =CameraVideo.class.getSimpleName() ;
    private Camera mCamera;
    private MediaRecorder mMediaRecorder;
    private CameraPreview mPreview;
    //private Handler handler;
    //private Runnable runnable;
    private Handler handlerTimer;
    private Runnable runnableTimer;
    private static long SECOND=1000;
    private static long RECORD_TIME=SECOND*60;
    private static long TIME_ELASPED=0;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    private File file;
   // private boolean mediaPrepared=false;
    private boolean isRecording=false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_video);
          cameraViewVideo = findViewById(R.id.camera_preview);
          ibFlash = findViewById(R.id.ib_flash);
        ibFlash.setImageResource(R.drawable.ic_flash_auto);

        vCapture =findViewById(R.id.btn_snap);
        vCaptureInner=findViewById(R.id.snap_inner);
        vCaptureInner.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/10,1.0f,0.0f));

        vCaptureOutter=findViewById(R.id.snap_outter);
        vCaptureEffect=findViewById(R.id.snap_effect);

          tvFlash = findViewById(R.id.tv_flash);

          tvSnap = findViewById(R.id.tv_snap);
          tvAction = findViewById(R.id.tv_action);

        mCamera = getCameraInstance();
        mCamera.setDisplayOrientation(90);
        if(mCamera==null){
            Toast.makeText(this,"Can't access camera.",Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        mPreview = new CameraPreview(this, mCamera);
        cameraViewVideo.addView(mPreview);

        setTimeLapse();

        handlerTimer=new Handler();
        runnableTimer=new Runnable() {
            @Override
            public void run() {
                TIME_ELASPED++;
                setTimeLapse();

                Log.d(TAG,"timelapse: "+TIME_ELASPED+" record_time: "+RECORD_TIME
                        +" "+(TIME_ELASPED<RECORD_TIME));
                if(TIME_ELASPED<RECORD_TIME/SECOND)
                    handlerTimer.postDelayed(runnableTimer,SECOND);
                else {
                    //vCapture.onTouchEvent(MotionEvent.ACTION_DOWN);
                    stopRecording();
                    close();
                }
            }
        };

        tvSnap.setText("Tap to record");
 //       mediaPrepared=prepareVideoRecorder();
        vCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isRecording){
                    stopRecording();
                    close();
                }
                else if(!startRecording()){
                    stopRecording();
                }
            }
        });
//        vCapture.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_DOWN: {
//                        startRecording();
//                        break;
//                    }
//                    case MotionEvent.ACTION_UP: {
//                        stopRecording();
//                        close();
//                    }
//                }
//                return true;
//            }
//        });
    }

    private boolean startRecording(){
        snapEffectStart();
        Log.d(TAG,"started recording");
        if(prepareVideoRecorder()) {
            mMediaRecorder.start();
            handlerTimer.postDelayed(runnableTimer,1000);
            isRecording=true;
            tvSnap.setText("Tap to stop");
            return  true;
        }
        else {
            Toast.makeText(this,"Media failed to prepare.",Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void stopRecording(){
        Log.d(TAG,"stopped recording");
        snapEffectEnd();
        if(mMediaRecorder!=null) {
            mMediaRecorder.stop();
        }
        releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();
        // release the camera immediately on pause event
        isRecording=false;
        TIME_ELASPED=0;
        tvSnap.setText("Tap to record");
    }

    private void setTimeLapse(){
        long timeRemaining=(RECORD_TIME/SECOND)-TIME_ELASPED;
        tvAction.setText(timeRemaining+" s");
    }


//    private void initHandler(){
//        handler=new Handler();
//        handler.postDelayed(runnable,RECORD_TIME);
//    }

    protected void snapEffectStart(){
        vCaptureInner.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/12,0.0f,1.0f));
        vCapture.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/10,1.0f,1.2f));
    }
    protected void snapEffectEnd(){
        vCaptureInner.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/12,1.0f,0.0f));
        vCapture.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/10,1.2f,1.0f));
    }

    private boolean prepareVideoRecorder(){
        Log.d(TAG,"prepare recorder");
        if(!Utility.checkPermission(this)){
            Toast.makeText(this,"Permission required to continue.",Toast.LENGTH_SHORT).show();
            return false;
        }

        mMediaRecorder = new MediaRecorder();

        // Step 1: Unlock and set camera to MediaRecorder
        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);

        // Step 2: Set sources
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));

        // Step 4: Set output file
        Log.d(TAG,"file uri");
        file=getOutputMediaFile(MEDIA_TYPE_VIDEO);
        Log.d(TAG,"set uri");
        mMediaRecorder.setOutputFile(file.toString());

        // Step 5: Set the preview output
        mMediaRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());

        // Step 6: Prepare configured MediaRecorder
        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    private void releaseMediaRecorder(){
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();   // clear recorder configuration
            mMediaRecorder.release(); // release the recorder object
            mMediaRecorder = null;
            mCamera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }

    private void setOrientation(Camera camera){
      //  Camera.getCameraInfo();
    }

    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d(TAG, "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
//            if (Build.VERSION.SDK_INT >= 14)
//                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
//            else
                mediaMetadataRetriever.setDataSource(videoPath);

            bitmap = mediaMetadataRetriever.getFrameAtTime(10, MediaMetadataRetriever.OPTION_CLOSEST);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            Log.d(TAG,"b4 open camera");
            c = Camera.open(); // attempt to get a Camera instance
            Log.d(TAG,"after open camera");
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
            Log.d(TAG,"getCamera exception:"+e);
        }
        Log.d(TAG,"camera: "+c);
        return c; // returns null if camera is unavailable
    }

    @Override
    protected void onPause() {
        super.onPause();
       stopRecording();
    }



    private void close(){
        Intent i = new Intent();
        if(file!=null&&file.getPath()!=null)
            i.putExtra("file_path", file.getPath());
        else
            i.putExtra("file_path", "");

        setResult(Activity.RESULT_OK, i);

        finish();
    }

}
