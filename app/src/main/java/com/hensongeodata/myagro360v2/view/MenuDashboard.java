package com.hensongeodata.myagro360v2.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.HttpHandler;
import com.hensongeodata.myagro360v2.structurenew.bottomfragment.FarmHomeFragment;
import com.hensongeodata.myagro360v2.structurenew.bottomfragment.ForumHomeFragment;
import com.hensongeodata.myagro360v2.structurenew.bottomfragment.HomeFragment;
import com.hensongeodata.myagro360v2.structurenew.bottomfragment.ShopHomeFragment;
import com.hensongeodata.myagro360v2.helper.CircleTransform;
import com.hensongeodata.myagro360v2.model.KnowledgeBase;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MenuDashboard extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

      private static String TAG = MenuDashboard.class.getSimpleName();
      public static List<KnowledgeBase> knowledgeBases;

      BottomNavigationView navigationView;
      HttpHandler httpHandler;
      DrawerLayout drawerLayout;
      NavigationView navView;
      MyApplication myApplication;
      private Handler handler;
      private Runnable runnable;
      private boolean allowExit = false;

      final String appPackageName = "air.jp.go.jica.shep"; // getPackageName() from Context or Activity object

      //    toolbar
      private CircleImageView orgLogo, navProfile;
      private TextView username, orgName;

      private static final String FRAGMENT_OTHER = "other_fragments" ;
      private static final String FRAGMENT_HOME = "home_fragment" ;

      private BottomNavigationView.OnNavigationItemSelectedListener myNavigationItemListener
              = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                  switch (item.getItemId()) {
                        case R.id.navigation_home:
                              orgName.setText(SharePrefManager.getInstance(getApplicationContext()).getOrganisationDetails().get(1));
                              viewFragment(new HomeFragment(), FRAGMENT_HOME);
                              return true;
                        case R.id.navigation_plots:
                              orgName.setText("Forum");
                              viewFragment(new ForumHomeFragment(), FRAGMENT_OTHER);
                              return true;
                        case R.id.navigation_farm_activity:
//                    Intent scouting = new Intent(getApplicationContext(), MappingActivity.class);
//                    scouting.putExtra("mode", ChatbotAdapterRV.MODE_SCOUT);
//                    startActivity(scouting);
                              orgName.setText("Farm Activity");
                              viewFragment(new FarmHomeFragment(), FRAGMENT_OTHER);
                              return true;
                        case R.id.learn_navigation:
//                    Intent mapping = new Intent(getApplicationContext(), MappingActivity.class);
//                    mapping.putExtra("mode", ChatbotAdapterRV.MODE_MAP);
//                    startActivity(mapping);
                              orgName.setText("Shop");
                              viewFragment(new ShopHomeFragment(), FRAGMENT_OTHER);
                              return true;
                  }
                  return false;
            }
      };

      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.menu_navigation_bar);

            init();
      }

      private void init() {

            myApplication = new MyApplication();

            navView = findViewById(R.id.navigation_view);
            navigationView = findViewById(R.id.navigation);
            drawerLayout = findViewById(R.id.menu_drawer_layout);
            orgLogo = findViewById(R.id.org_logo);
            navProfile = findViewById(R.id.nav_profile);
            orgName = findViewById(R.id.toolbar_title);

            navView.setNavigationItemSelectedListener(this);
            navigationView.setOnNavigationItemSelectedListener(myNavigationItemListener);

            View headerView = navView.getHeaderView(0);
            username = headerView.findViewById(R.id.nav_username);

            orgName.setText(SharePrefManager.getInstance(getApplicationContext()).getOrganisationDetails().get(1));
            String logo_url = SharePrefManager.getInstance(getApplicationContext()).getUserDetails().get(5);
            Log.e(TAG, "init: "+ logo_url );
            username.setText(""+SharePrefManager.getInstance(getApplicationContext()).getUserDetails().get(2) + " "
                    + SharePrefManager.getInstance(getApplicationContext()).getUserDetails().get(1));

            if (logo_url != null) {
                  Picasso.get()
                          .load(logo_url)
                          .resize(400, 400)
                          .transform(new CircleTransform())
                          .onlyScaleDown()
                          .into(orgLogo);
            } else
                  orgLogo.setImageDrawable(getResources().getDrawable(R.drawable.ic_supervisor));

            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout,
                    R.string.open_drawer, R.string.close_drawer);
            drawerLayout.setDrawerListener(toggle);
            toggle.syncState();
            handler = new Handler();
            runnable = new Runnable() {
                  @Override
                  public void run() {
                        allowExit = false;
                  }
            };

            openDrawer();

            viewFragment(new HomeFragment(), FRAGMENT_HOME);

      }

//      private void checkUpdate() {
//            networkRequest = new NetworkRequest(MenuDashboard.this);
//            networkRequest.recentPost();
//
//            String url = "";
//            String title = "";
//            String date = "";
//            if ((url = SharePrefManager.getInstance(MenuDashboard.this).getPostInitial().get(0)) != null)
//                  url = SharePrefManager.getInstance(this).getPostInitial().get(0);
//            else
//                  url = "";
//
//            if ((title = SharePrefManager.getInstance(this).getPostContent().getTitle()) != null)
//                  title = SharePrefManager.getInstance(this).getPostContent().getTitle();
//            else
//                  title = "";
//
//
//            String new_date = "";
//            if ((date = SharePrefManager.getInstance(this).getPostContent().getDate()) != null) {
//                  date = SharePrefManager.getInstance(this).getPostContent().getDate();
//                  DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:ss a");
//                  Date mydate = new Date();
//                  try {
//                        mydate = df.parse(date);
////            Log.e(TAG, "Date is " + mydate);
//                  } catch (ParseException e) {
//                        e.printStackTrace();
//                  }
//                  DateFormat df1 = new SimpleDateFormat("E MMMM dd,yyyy hh:mm a");
//                  new_date = df1.format(mydate);
//            }
//
//
//            String creator = SharePrefManager.getInstance(this).getPostInitial().get(1);
//            String content = SharePrefManager.getInstance(this).getPostContent().getContent();
////        if (url.isEmpty())
////            postImage.setVisibility(View.GONE);
////
////        if (title.isEmpty())
////            checkFalse = true;
////
////        postCreator.setText(creator + " : ");
////        postTitle.setText(title);
////        postMain.setText(content);
////
////        postDate.setText(new_date);
//
//            checkFalse = title.isEmpty();
//    }

      public String checkTime(){

            //Get the time of day
            Date date = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int hour = cal.get(Calendar.HOUR_OF_DAY);

            Log.e(TAG, "checkTime: " + String.valueOf(hour));


            //Set greeting
            String greeting = null;
            if(hour >=6 && hour<12){
                  greeting = "Good Morning";
            } else if(hour>= 12 && hour < 17){
                  greeting = "Good Afternoon";
            } else if(hour >= 17 && hour < 21){
                  greeting = "Good Evening";
            } else if(hour >= 21 && hour < 24){
                  greeting = "Good Night";
            }

            return greeting;

      }

      private void openMain() {
//        showMore.setOnClickListener(new MyListener(new Intent(this, MainActivity.class), false));
      }

      private void openDrawer() {
            orgLogo.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        if (!drawerLayout.isDrawerOpen(GravityCompat.START))
                              drawerLayout.openDrawer(Gravity.START);
                        //Toast.makeText(MenuDashboard.this, "Menu Selected ", Toast.LENGTH_LONG).show();
                  }
            });
      }

      private void openGame(){
            try {
                  startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                  startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
      }

      private void viewFragment(Fragment fragment, String name){
            final FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_container_new, fragment);
            // know how many fragments there are in the stack
            final int count = fragmentManager.getBackStackEntryCount();
            // 2. If the fragment is **not** "home type", save it to the stack
            if (name.equals(FRAGMENT_OTHER)){
                  fragmentTransaction.addToBackStack(name);
            }
            // commit !
            fragmentTransaction.commit();

            // 3. After the commit, if the fragment is not an "home type" the back stack is changed, triggering the
            // OnBackStackChanged callback
            fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                  @Override
                  public void onBackStackChanged() {
                        // If the stack decreases it means I clicked the back button
                        if (fragmentManager.getBackStackEntryCount() <= count){
                              // pop all the fragment and remove the listener
                              fragmentManager.popBackStack(FRAGMENT_OTHER, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                              fragmentManager.removeOnBackStackChangedListener(this);
                              // set the home button selected
                              navigationView.getMenu().getItem(0).setChecked(true);
                        }
                  }
            });
      }

      @Override
      public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            int id = menuItem.getItemId();

            switch (id) {
                  case R.id.menu_asime:
                        viewFragment(new ShopHomeFragment(), FRAGMENT_OTHER);
                        navigationView.setSelectedItemId(R.id.learn_navigation);
                        break;

                  case R.id.menu_activity:
                        viewFragment(new FarmHomeFragment(), FRAGMENT_OTHER);
                        navigationView.setSelectedItemId(R.id.navigation_farm_activity);
                        break;

                  case R.id.menu_access_form:
                        startActivity(new Intent(getApplicationContext(), AddForm.class));
                        break;

                  case R.id.menu_game:
                        openGame();
                        break;

            case R.id.menu_saved_form:
                startActivity(new Intent(getApplicationContext(), ArchiveList.class));
//                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                break;

                  case R.id.menu_share_location:
                        startActivity(new Intent(getApplicationContext(), ShareLocation.class));
                        break;

                  case R.id.menu_guide:
                        if (myApplication.hasNetworkConnection(this))
                              startActivity(new Intent(this, GuideVideo.class));
                        else
                              myApplication.showInternetError(this);
                        break;

                  case R.id.menu_share_app:
//                startActivity(new Intent(getApplicationContext(), ShareLocation.class));
                        shareApp();
                        break;
                  case R.id.menu_feedback:
                        startActivity(new Intent(getApplicationContext(), Feedback.class));
                        break;
                  case R.id.menu_settings:
                        //Toast.makeText(this, "Draft Selected", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), Settings.class));
                        break;
                  case R.id.menu_logout:
                        //Toast.makeText(this, "Draft Selected", Toast.LENGTH_SHORT).show();
                        exit(MenuDashboard.this);
                        break;
                  case R.id.menu_about:
                        // Toast.makeText(this, "Email Selected", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), About.class));
                        break;

            }
            drawerLayout.closeDrawer(GravityCompat.START);
            return true;
      }

      @Override
      public void onBackPressed() {

            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                  drawerLayout.closeDrawer(GravityCompat.START);
//                  getSupportFragmentManager().popBackStack();
            }
            if (navigationView.getMenu().getItem(0).isChecked()) {
                  if (allowExit)
                        super.onBackPressed();
                  else {
                        allowExit = true;
                        Toast.makeText(this, "Tap back button again to exit.", Toast.LENGTH_SHORT).show();
                        handler.postDelayed(runnable, 5000);//change allowExit to false after 5 secs
                  }
            }
            else
                  getSupportFragmentManager().popBackStack();
      }

}
