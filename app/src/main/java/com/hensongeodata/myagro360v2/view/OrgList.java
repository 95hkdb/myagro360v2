package com.hensongeodata.myagro360v2.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.OrgAdapterRV;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.model.Organization;

/**
 * Created by user1 on 1/25/2018.
 */

public class OrgList extends SearchableList {
    private static String TAG=OrgList.class.getSimpleName();
    public static String ACTION_SELECT="item selection action";
    public static String ACTION_DOWNLOAD="download action";
    private OrgAdapterRV adapter;
    private IntentFilter filter;
    private OrgListReceiver receiver;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        filter=new IntentFilter(BroadcastCall.ORGLIST);
        receiver=new OrgListReceiver();
        registerReceiver(receiver,filter);


        fetchOrgs();

        tvCaption.setText("Organizations");

        etSearch.addTextChangedListener(new MyWatcher(ORGANIZATION,adapter));


        fabAction.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_update));
        fabAction.setVisibility(View.VISIBLE);
        fabAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
           //Log.d(TAG,"inside fab click");
                downloadOrgs();
            }
        });


        ArrayList<Organization> organizations=databaseHelper.getOrganizations();

        if(organizations!=null){
            if(organizations.size()==0)
                    fabAction.performClick();
        }
    }

    private void downloadOrgs(){
        if(myApplication.hasNetworkConnection(OrgList.this)){
            showProgress("Updating Forms...");
            networkRequest.fetchForms(BroadcastCall.ORGLIST);
        }
        else
            myApplication.showInternetError(OrgList.this);
    }

    private void fetchOrgs(){
        showProgress("Loading Organizations");
        ArrayList<Organization> organizations=databaseHelper.getOrganizations();
        Log.d(TAG,"orgList size: "+organizations.size());
        hideProgress();

        if(organizations==null||organizations.size()<1){
            recyclerView.setVisibility(View.GONE);
                setTvPlaceholder("No Organizations available");
        }
        else {
            tvPlaceholder.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter = new OrgAdapterRV(this, organizations);
            recyclerView.setAdapter(adapter);
        }
   // return organizations;
}


    class OrgListReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            String action=intent.getExtras().getString("action");
            if(action!=null) {
                if (action.equalsIgnoreCase(ACTION_SELECT)) {
                    //an organization was selected
                    String orgID = intent.getExtras().getString("orgID");
                    Intent i = new Intent(OrgList.this, FormList.class);
                    i.putExtra("org_id", orgID);
                    startActivity(i);
                    finish();
                }
                else if(action.equalsIgnoreCase(ACTION_DOWNLOAD)) {
                    //JoinCommunityResponse from download request
                    fetchOrgs();//reload organization list since it's been updated
                    hideProgress();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
        hideProgress();
    }
}
