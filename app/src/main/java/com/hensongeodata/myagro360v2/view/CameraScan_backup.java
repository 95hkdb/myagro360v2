//package com.faw.myagro360.view;
//
//import android.content.Context;
//import android.net.Uri;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Vibrator;
//import android.support.annotation.Nullable;
//import android.util.Log;
//import android.view.View;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.faw.myagro360.Interface.PercentageListener;
//import com.faw.myagro360.MyApplication;
//import com.faw.myagro360.R;
//import com.faw.myagro360.api.request.LocsmmanProAPI;
//import com.faw.myagro360.controller.CreateForm;
//import com.faw.myagro360.controller.LocsmmanEngine;
//import com.faw.myagro360.controller.NetworkRequest;
//import com.faw.myagro360.controller.ProgressAnimation;
//import com.faw.myagro360.controller.RetrofitHelper;
//import com.faw.myagro360.controller.Utility;
//import com.faw.myagro360.model.Image;
//import com.faw.myagro360.model.Keys;
//import com.faw.myagro360.model.ScanFAW;
//import com.faw.myagro360.model.SettingsModel;
//import com.faw.myagro360.model.User;
//
//import org.jetbrains.annotations.NotNull;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.File;
//import java.io.IOException;
//
//import io.fotoapparat.Fotoapparat;
//import io.fotoapparat.configuration.UpdateConfiguration;
//import io.fotoapparat.error.CameraErrorListener;
//import io.fotoapparat.exception.camera.CameraException;
//import io.fotoapparat.parameter.ScaleType;
//import io.fotoapparat.preview.Frame;
//import io.fotoapparat.preview.FrameProcessor;
//import io.fotoapparat.result.PhotoResult;
//import io.fotoapparat.view.CameraView;
//import okhttp3.MultipartBody;
//import okhttp3.ResponseBody;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//import retrofit2.Retrofit;
//import retrofit2.converter.gson.GsonConverterFactory;
//
//import static com.faw.myagro360.controller.RetrofitHelper.createPartFromString;
//import static io.fotoapparat.selector.FlashSelectorsKt.off;
//import static io.fotoapparat.selector.FlashSelectorsKt.torch;
//import static io.fotoapparat.selector.LensPositionSelectorsKt.back;
//
///**
// * Created by user1 on 5/15/2018.
// */
//
//public class CameraScan_backup extends MyCamera {
//    private static String TAG=CameraScan_backup.class.getSimpleName();
//    private Image image;
//    private Handler handler;
//    private Runnable snapRunnable;
//    private Handler handlerProcessShot;
//    private Runnable runnableProcessShot;
//    private TextView tvStop;
//    private ImageView ivPreview;
//    private View vPreview;
//    private TextView tvPercentage;
//    private TextView tvScanning;
//    private ProgressBar vProgress;
//    public static int MAX_SCAN=2;
//    private static int SCAN_INDEX =0;
//    public static int FAW_DETECTION_TRUE=1;
//    public static int FAW_DETECTION_FALSE=0;
//    private ScanFAW scanFAW;
//    private SettingsModel settingsModel;
//    private int MAX_WIDTH=-1;
//    private int CURRENT_WIDTH=0;
//    private int animationIndex=0;
//    private boolean auto_progress=true;
//    private boolean scan_started=false;
//    private Vibrator vibrator;
//    private Call<ResponseBody> call;
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.camera_scan);
//        scanFAW= MyApplication.scanFAWList.get(index);
//        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
//
//        SCAN_INDEX =0;
//        settingsModel=new SettingsModel(this);
//        cameraView= (CameraView) findViewById(R.id.camera_view);
//        ibFlash= (ImageButton) findViewById(R.id.ib_flash);
//        ibFlash.setImageResource(R.drawable.ic_flash_off);
//
//        cameraView.post(new Runnable() {
//            @Override
//            public void run() {
//                MAX_WIDTH=cameraView.getWidth();
//            }
//        });
//
//        vCapture =findViewById(R.id.btn_snap);
//        vCaptureInner=findViewById(R.id.snap_inner);
//        vCaptureInner.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/10,1.0f,0.0f));
//
//        vCaptureOutter=findViewById(R.id.snap_outter);
//        vCaptureEffect=findViewById(R.id.snap_effect);
//
//        tvFlash= (TextView) findViewById(R.id.tv_flash);
//
//        tvSnap= (TextView) findViewById(R.id.tv_snap);
//        tvAction= (TextView) findViewById(R.id.tv_action);
//
//        cameraView= (CameraView) findViewById(R.id.camera_view);
//        fotoapparat= createFotoapparatImage();
//        tvStop= (TextView) findViewById(R.id.tv_stop);
//        tvStop.setText("End");
//        ibStop= (ImageButton) findViewById(R.id.ib_stop);
//        ibStop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onAddMapButtonClicked(View view) {
//                stopCamera();
//            }
//        });
//
//
//        updateFlash(FLASH_OFF);
//        updateFotoapparat(FLASH_OFF);
//
//        vCapture.setOnClickListener(snapClickListener);
//        tvSnap.setText("Tap to scan");
//
//        handler=new Handler();
//        snapRunnable=new Runnable() {
//            @Override
//            public void run() {
//                vCapture.setOnClickListener(snapClickListener);
//                vCaptureInner.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/12,1.0f,0.0f));
//                vCapture.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/10,1.2f,1.0f));
//                vCaptureEffect.setVisibility(View.GONE);
//            }
//        };
//
//        (findViewById(R.id.ib_back)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onAddMapButtonClicked(View view) {
//                finish();
//            }
//        });
//
//        handlerProcessShot=new Handler();
//
//        vProgress= (ProgressBar) findViewById(R.id.v_progress);
//        vProgress.setProgress(0);
//        tvPercentage= (TextView) findViewById(R.id.tv_percentage);
//        tvScanning= (TextView) findViewById(R.id.tv_tag);
//
//
//        (findViewById(R.id.ib_close)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onAddMapButtonClicked(View view) {
//                finish();
//            }
//        });
//
//    }
//
//    View.OnClickListener snapClickListener=new View.OnClickListener() {
//        @Override
//        public void onAddMapButtonClicked(View view) {
//            if(!scan_started) {
//                snapEffectImage();
//                animateProgress();
//                snap();
//            scan_started=true;
//            }
//            else {
//                finish();
//            }
//
//        }
//    };
//
//    protected void startCamera(){
//        if (Utility.checkPermission(this))
//            fotoapparat.start();
//        else
//            Toast.makeText(this,"Camera permission required to continue",Toast.LENGTH_SHORT).show();
//    }
//
//    protected void stopCamera(){
//        if (Utility.checkPermission(this))
//            fotoapparat.stop();
//        else
//            Toast.makeText(this,"Camera permission required to continue",Toast.LENGTH_SHORT).show();
//
//        finish();
//    }
//
//    protected void updateFlash(String flash){
//        tvFlash.setText(flash);
////        if(flash.equalsIgnoreCase(FLASH_AUTO))
////            ibFlash.setOnClickListener(new FlashListener(FLASH_ON));
//         if(flash.equalsIgnoreCase(FLASH_ON))
//            ibFlash.setOnClickListener(new FlashListener(FLASH_OFF));
//        else if(flash.equalsIgnoreCase(FLASH_OFF))
//            ibFlash.setOnClickListener(new FlashListener(FLASH_ON));
//    }
//
//    protected void updateFotoapparat(String flash) {
//        Log.d(TAG,"fotoapparat: "+fotoapparat+" flash: "+flash);
//        if (fotoapparat != null&&flash!=null) {
//            fotoapparat.updateConfiguration(
//                    flashConfiguration(flash)
//            );
//        }
//    }
//
//    protected UpdateConfiguration flashConfiguration(String flash){
//        Log.d(TAG,"config flash: "+flash);
//       if(flash.equalsIgnoreCase(FLASH_ON))
//            return UpdateConfiguration.builder()
//                    .flash(
//                            torch()
//                    )
//                    .build();
//        else
//            return UpdateConfiguration.builder()
//                    .flash(
//                            off()
//                    )
//                    .build();
//    }
//
//    class FlashListener implements View.OnClickListener{
//        String flash;
//        FlashListener(String flash){
//            this.flash=flash;
//        }
//        @Override
//        public void onAddMapButtonClicked(View view) {
//            if(flash.equalsIgnoreCase(FLASH_ON)){
//                ibFlash.setImageResource(R.drawable.ic_flash_on);
//            }
//            else if(flash.equalsIgnoreCase(FLASH_OFF)){
//                ibFlash.setImageResource(R.drawable.ic_flash_off);
//            }
//
//            updateFotoapparat(flash);
//            updateFlash(flash);
//        }
//    }
//
//    private void snapEffectImage(){
//        ((TextView)(findViewById(R.id.tv_scan))).setText("Stop");
//        tvSnap.setText("Tap to cancel");
//        //vCapture.setOnClickListener(null);
//        vCaptureInner.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/12,0.0f,1.0f));
//        vCapture.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/10,1.0f,1.2f));
//    }
//
//    private Fotoapparat createFotoapparatImage() {
//        return Fotoapparat
//                .with(this)
//                .into(cameraView)
//                .previewScaleType(ScaleType.CenterCrop)
//                .lensPosition(back())
//                .flash(off())
//                .frameProcessor(new SampleFrameProcessor())
//                .cameraErrorCallback(new CameraErrorListener() {
//                    @Override
//                    public void onError(@NotNull CameraException e) {
//                        Toast.makeText(CameraScan_backup.this, e.toString(), Toast.LENGTH_LONG).show();
//                    }
//                })
//                .build();
//    }
//
//    private class SampleFrameProcessor implements FrameProcessor {
//        @Override
//        public void process(@NotNull Frame frame) {
//            // Perform frame processing, if needed
//        }
//    }
//
//    private void snap() {
//        SCAN_INDEX++;
//        PhotoResult photoResult = fotoapparat.takePicture();
//        image=new Image();
//        image.setId(index+" Image "+ SCAN_INDEX);
//
//        String fileName=validate(image.getId()+(new CreateForm(this)).getCurrentTime());
//
//        File file=new File(getExternalFilesDir("photos"), fileName+".jpg");
//        photoResult.saveToFile(file);
//
//        image.setUri(Uri.fromFile(file));
//
//        runnableProcessShot=new Runnable() {
//            @Override
//            public void run() {
//                cancel(handlerProcessShot,runnableProcessShot);
//                    processShot(image);
//            }
//        };
//
//        handlerProcessShot.postDelayed(runnableProcessShot,5000);
//
//    }
//
//    private void animateProgress(){
//        ProgressAnimation anim = new ProgressAnimation(vProgress, 0, 100);
//        anim.setDuration(60000);
//        anim.setPercentageListener(new PercentageListener() {
//            @Override
//            public void onUpdate(int percentage) {
//                tvPercentage.setText(percentage+" %");
//                if(percentage>=100)
//                    end(scanFAW);
//            }
//        });
//
//        vProgress.startAnimation(anim);
//        tvScanning.setText("Scanning...");
//
//    }
//    private String validate(String fileName){
//        fileName=fileName.replaceAll("\\s","");
//        fileName=fileName.replaceAll(":","");
//        fileName=fileName.trim();
//
//        return fileName;
//    }
//
//    private void processShot(Image image){
//        MultipartBody.Part file = RetrofitHelper.prepareFilePart(this,"image",image.getUri().getPath());
//        sendFile(file);
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        Log.d(TAG,"onStart camera");
//        startCamera();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//       stopCamera();
//    }
//
//    public void sendFile(MultipartBody.Part file){
//        Log.d(TAG,"inside sendFile "+ SCAN_INDEX);
//        Retrofit.Builder builder=new Retrofit.Builder()
//                .baseUrl(Keys.APPS_BASE)
//                .addConverterFactory(GsonConverterFactory.create());
//
//        Retrofit retrofit=builder.build();
//
//        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);
//
//        String gps;
//        if(MyApplication.poi!=null)
//            gps=MyApplication.poi.getLat()+","+MyApplication.poi.getLon();
//        else{
//            finish();
//            return;
//        }
//
//        Log.d(TAG,"gps: "+gps);
//         call= service.scanFAW(createPartFromString(gps),
//                createPartFromString((new User(this)).getId()!=null?
//                        (new User(this)).getId():""), file);
//
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//
//                ResponseBody responseBody=response.body();
//                String message=null;
//                try {
//                    if(responseBody!=null)
//                        message=responseBody.string();
//
//                    Log.d(TAG,"JoinCommunityResponse: "+message+" body: "+responseBody
//                            +" code: "+response.code()+" error: "+response.errorBody());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    processResult(NetworkRequest.STATUS_FAIL,message);
//                }
//
//                Log.d(TAG,"date: "+message);
//                if(message==null||message.trim().equalsIgnoreCase("null"))
//                    processResult(NetworkRequest.STATUS_FAIL,message);
//                else
//                    processResult(NetworkRequest.STATUS_SUCCESS,message);
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                String message=t.getMessage();
//                Log.d(TAG,"throwable: "+t);
//                processResult(NetworkRequest.STATUS_FAIL,message);
//            }
//        });
//    }
//
//    private void processResult(int status,String response){
//        if(status==NetworkRequest.STATUS_SUCCESS){
//            try {
//                JSONObject jsonObject=new JSONObject(response);
//
//                if(jsonObject.has("success")){
//                    int success=jsonObject.getInt("success");
//                    Log.d(TAG,"success: "+success);
//                    if(success==1){
//                        scanFAW.setMessage(jsonObject.optString("date"));
//                        scanFAW.setResponse(jsonObject.optInt("detected"));
//                        scanFAW.setImage(image);
//
//                        if(scanFAW.getResponse()==FAW_DETECTION_TRUE) {
//                            vibrator.vibrate(1000);
//                            scanFAW.setHeader("Fall Armyworm detected");
//                        }
//                        else
//                            scanFAW.setHeader("Fall Armyworm not detected");
//                    }
//                }
//
//                Log.d(TAG,"date: "+scanFAW.getMessage());
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//
//        end(scanFAW);
//    }
//
//    private void end(ScanFAW scanFAW){
//       // MyApplication.scanFAWList.set(index,scanFAW);
//
//        if(scanFAW.getResponse()==FAW_DETECTION_TRUE|| SCAN_INDEX >=MAX_SCAN)
//            finish();
//        else
//            snap();
//    }
//
//    private void cancel(Handler handler,Runnable runnable){
//        if(handler!=null&&runnable!=null)
//            handler.removeCallbacks(runnable);
//    }
//
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        MyApplication.scanFAWList.set(index,scanFAW);
//        //if(call!=null)
//            //call.cancel();
//    }
//}
