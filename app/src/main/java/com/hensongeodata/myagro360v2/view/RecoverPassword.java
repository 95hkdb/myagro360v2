package com.hensongeodata.myagro360v2.view;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.User;

/**
 * Created by user1 on 10/31/2017.
 */

public class RecoverPassword extends BaseActivity {
    Toolbar toolbar;
    ActionBar actionBar;
    Button btnRecover;
    TextInputEditText etEmail;
    String email;
    IntentFilter filter;
    RecoverPasswordBroadcastReceiver receiver;
    User user;
    MyApplication myApplication;
    NetworkRequest networkRequest;
    ProgressDialog progressDialog;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recover_pwd);
          toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(resolveString(R.string.recover_password));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        receiver=new RecoverPasswordBroadcastReceiver();
        filter=new IntentFilter(BroadcastCall.RECOVER);
        registerReceiver(receiver,filter);

        user=new User(this);
        myApplication=new MyApplication();
        networkRequest=new NetworkRequest(this);
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage(resolveString(R.string.progress_recover));
        progressDialog.setCancelable(false);


          etEmail = findViewById(R.id.et_email);
        (findViewById(R.id.btn_recover)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email =etEmail.getText().toString().trim();
                if(!isEmailValid(email)){
                    etEmail.setError(resolveString(R.string.error_field_required));
                    etEmail.requestFocus();
                    return;
                }

                doRecover(email);
            }
        });
    }

    private void doRecover(String email){
        if(myApplication.hasNetworkConnection(this)){
                progressDialog.show();
            networkRequest.recover(email);
        }
        else {
            if(progressDialog.isShowing())
                progressDialog.hide();

            myApplication.showInternetError(this);
        }
    }


    private boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    class RecoverPasswordBroadcastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            if(progressDialog.isShowing())
                progressDialog.hide();

            String message=intent.getExtras().getString("date");
            if(message!=null){
                if(message.equalsIgnoreCase("true")){
                    Toast.makeText(RecoverPassword.this,"Success. Please check your email for a password recovery link", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(RecoverPassword.this,SigninSignup.class));
                    finish();
                }
                else if(message.equalsIgnoreCase("false")){
                    myApplication.showMessage(RecoverPassword.this,"Sorry, There's no user with email '"+email+"' in on our platform");
                }
                else {
                    myApplication.showMessage(RecoverPassword.this,"Sorry. Account recovery failed. Try again.");
                }
            }
            else {
                myApplication.showMessage(RecoverPassword.this,"Something went wrong. Try again.");
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
