package com.hensongeodata.myagro360v2.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;

/**
 * Created by user1 on 4/10/2017.
 */

public class ResetPassword extends AppCompatActivity {
    private static String TAG="ResetPassword";
    private EditText etEmail, etPasswordOld,etPassword, etPasswordConfirm;
    private Button btnReset;
    private ProgressDialog progressDialog;
    private String email,passwordOld,password, passwordConfirm;

    Toolbar toolbar;
    ActionBar actionBar;

    private MyApplication myApplication;
    private NetworkRequest networkRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_pwd);
          toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Reset Password");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        progressDialog=new ProgressDialog(ResetPassword.this);
        progressDialog.setMessage("Updating password. Please wait");

        myApplication=new MyApplication();
        networkRequest=new NetworkRequest(this);

          WebView webView = findViewById(R.id.webview);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);

        webView.loadUrl("http://myaccounts.hensongeodata.com");
      /*  webView.setWebViewClient(new WebViewClient() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.getUrl().toString());
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                maploading.clearAnimation();
                maploading.setVisibility(View.GONE);
                mapRetry.setVisibility(View.GONE);
                webView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                mapRetry.setVisibility(View.VISIBLE);
                webView.setVisibility(View.GONE);
                maploading.setVisibility(View.GONE);
                maploading.setAnimation(myApplication.blinkAnim());
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                mapRetry.setVisibility(View.VISIBLE);
                webView.setVisibility(View.GONE);
                maploading.setVisibility(View.GONE);
                maploading.setAnimation(myApplication.blinkAnim());
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                mapRetry.setVisibility(View.VISIBLE);
                webView.setVisibility(View.GONE);
                maploading.setVisibility(View.GONE);
                maploading.setAnimation(myApplication.blinkAnim());
            }
        });*/

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
