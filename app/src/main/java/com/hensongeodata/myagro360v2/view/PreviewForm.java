package com.hensongeodata.myagro360v2.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONException;

import java.util.ArrayList;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.PreviewAdapterRV;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.fragment.BSFragment;
import com.hensongeodata.myagro360v2.model.Section;
import com.hensongeodata.myagro360v2.service.UploadFilesService;

/**
 * Created by user1 on 2/8/2018.
 */

public class PreviewForm extends BaseActivity {
    private static String TAG=PreviewForm.class.getSimpleName();
    public static int SOURCE_FORM=1000;
    public static int SOURCE_ARCHIVE=2000;
    public static int SOURCE_HISTORY=3000;

    private static int MODE_DATA=200;
    private static int MODE_FILE=202;
    private static int submission_mode;

    private int source;
    private String transaction_id;
    private PreviewAdapterRV adapter;
    private RecyclerView recyclerView;
    private Button btnSubmit;
    private boolean archive=false;

    private IntentFilter filter;
    private PreviewReceiver receiver;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        filter=new IntentFilter(BroadcastCall.TRANSACTION_UPDATE);
        receiver=new PreviewReceiver();
        registerReceiver(receiver,filter);

          recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        source=getIntent().getExtras().getInt("source");
        transaction_id=getIntent().getExtras().getString("transaction_id");
        Log.d(TAG,"transaction_id: "+transaction_id);
            adapter=new PreviewAdapterRV(this,formatPreviewList(databaseHelper.buildPreview(transaction_id)));

        if(adapter!=null)
            recyclerView.setAdapter(adapter);

        findViewById(R.id.btn_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             if(source==SOURCE_ARCHIVE) {
                 settings.setTransaction_id(transaction_id);
                 createForm.setShouldRefreshForm(true);

                 Intent i=new Intent(PreviewForm.this,MainActivity.class);
                 i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                 startActivity(i);
             }
                finish();
            }
        });


        findViewById(R.id.ib_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


          btnSubmit = findViewById(R.id.btn_submit);

        if(source==SOURCE_HISTORY) {
            btnSubmit.setText("Go back");
            findViewById(R.id.btn_edit).setVisibility(View.GONE);
        }
        else
            btnSubmit.setText(((source==SOURCE_FORM)&&settings.isArchive_mode())?"Save":"Submit");

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(source==SOURCE_HISTORY)
                        finish();
                    else
                        submit();
            }
        });
    }

    private ArrayList formatPreviewList(ArrayList rawList){
       ArrayList formattedList=new ArrayList();
        Section section=new Section();
        section.setTitle("");
        section.setDesc("");

        formattedList.add(section);//this section is added to create some space above the preview itemsVisible

        if(rawList!=null) {
            for (int a = 0; a < rawList.size(); a++) {
                formattedList.add(rawList.get(a));

                if(a+1==rawList.size()) {
                    formattedList.add(section);
                    formattedList.add(section);//add section again to create some space beneath preview itemsVisible
                }
            }
        }
        return formattedList;
    }

    private void submit(){
            if(settings.isArchive_mode()&&source==SOURCE_FORM) {
                createForm.setShouldRefreshForm(true);
                archive=true;
                updateGroupPOI();
                finish();
            }
            else {
                if(myApplication.hasNetworkConnection(this)){
                    showProgress("Submitting data. Please wait");
                    try {
                        // TODO: 6/5/2018 modify buildJSON form_id parameter
                        submission_mode=MODE_DATA;
                        if(source==SOURCE_FORM)
                            networkRequest.sendTransaction(createForm.buildJSON(this,null,true));
                        else
                            networkRequest.sendTransaction(databaseHelper.buildJSON(transaction_id,true));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                    myApplication.showInternetError(this);

            }

        //remove previously archived instance of this form
        String cachedTransaction_id=settings.getTransaction_id();//uncache transaction_id
        if(cachedTransaction_id!=null&&!cachedTransaction_id.isEmpty()) {
            databaseHelper.removeArchive(cachedTransaction_id);
            settings.setTransaction_id(null);
        }
    }


    class PreviewReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int status=intent.getExtras().getInt("status");
            String msg=intent.getExtras().getString("date");
            if(status== NetworkRequest.STATUS_SUCCESS){
                if(submission_mode==MODE_DATA){
                    submission_mode=MODE_FILE;
                    UploadFilesService.progressDialog=getProgress();
                    startService(new Intent(PreviewForm.this, UploadFilesService.class));
                    //hideProgress();
                }
                else if(submission_mode==MODE_FILE) {
                    uploadComplete();
                    Toast.makeText(context,"Files submitted successfully",Toast.LENGTH_SHORT).show();
                    hideProgress();
                }
                else {
                    myApplication.showMessage(context,"Submission mode not specified.");
                    hideProgress();
                }
            }
            else if(status==NetworkRequest.STATUS_FAIL){
                if(submission_mode==MODE_DATA) {
                    if(msg!=null)
                        myApplication.showMessage(context,msg);
                    else
                        myApplication.showMessage(context,"Data submission failed.");

                }
                else if(submission_mode==MODE_FILE)
                    myApplication.showMessage(context,"Data submission incomplete.");

                hideProgress();
            }
        }
    }


    private void uploadComplete(){
        Log.d(TAG,"upload complete");
        hideProgress();
        if(source==SOURCE_ARCHIVE)
            databaseHelper.removeArchive(transaction_id);

        createForm.setShouldRefreshForm(true);
        updateGroupPOI();
        finish();
    }

    private void updateGroupPOI(){
          if (MyApplication.poiGroup != null && MyApplication.poiGroup.isSelect()) {
            if(MyApplication.poiGroup.getName()!=null&&!MyApplication.poiGroup.getName().equalsIgnoreCase(BSFragment.DEAFAULT_GROUP_NAME))
                    databaseHelper.updatePOIGroup(MyApplication.poiGroup);

        }

          if (!MyApplication.poiGroup.isSelect()) {
           // databaseHelper.selectPOIGroup(myApplication.poiGroup);
            //myApplication.setDefaultPoiGroup(databaseHelper);
            myApplication.selectPOIGroup(this,databaseHelper);
            Log.d(TAG,"not isSelect");
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(source==SOURCE_FORM&&!archive){
            //since form was temporarily archived, and 'archive' button wasn't pressed we have to remove that archive
            databaseHelper.removeArchive(transaction_id);
        }

        unregisterReceiver(receiver);
    }
}
