package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.Interface.NetworkResponse;
import com.hensongeodata.myagro360v2.Interface.TaskListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.fragment.BSFragmentSimple;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.model.Pauline;
import com.hensongeodata.myagro360v2.model.SettingsModel;

import java.util.ArrayList;

/**
 * Created by user1 on 7/16/2018.
 */

public class Scout extends Chatbot implements TaskListener,NetworkResponse {
    private static String TAG=Scout.class.getSimpleName();
    private BottomSheetBehavior mBottomSheetBehavior;
    private  View bottomSheet;
    private View btnBSHandle;
    private boolean bsOpened=false;
    private BSFragmentSimple bsFragmentSimple;

    Pauline pauline;
    ChatbotAdapterRV adapter;
    ArrayList<InstructionScout> itemsVisible;
    //ArrayList<InstructionScout> cachedMappingItems;
    LocsmmanEngine locsmmanEngine;
    Runnable runnable;
    Handler handler;
    int index=0;
    int action=0; //number of times an action e.g taking location coordinates has been performed
    public static int OPEN_SCANNER=100;
    private long DELAY_TIME=2000; //delay time is 10 seconds
    public static String scannning_id;
    private TextToSpeech tts;
    private int mode;
    private String form_id;
    private boolean isFrench=false;
    private ArrayList<String> strPOIS;
    private ArrayList<InstructionScout> items;
    private String mapping_id="";

    private final int SPEED_FAST=400;
    private final int SPEED_SLOW=500;
    private int speed=SPEED_SLOW;
    private boolean speedShowing=false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSystemUI();
        mode=getIntent().getIntExtra("mode",-1);
        form_id=getIntent().getStringExtra("form_id");
        strPOIS=new ArrayList<>();
        items=new ArrayList<>();
      //  cachedMappingItems=new ArrayList<>();
        MyApplication.modeScout=mode;
        isFrench=settings.getLang().equalsIgnoreCase(SettingsModel.LAN_FR);
        tts=MyApplication.tts;
        String caption="Pest assistant";
        if(mode==ChatbotAdapterRV.MODE_SCOUT)
            caption=isFrench?"Assistant de dépistage des parasites":"Pest scouting assistant";
        else if(mode==ChatbotAdapterRV.MODE_MAP)
                caption=isFrench?"Assistant de cartographie des ravageurs":"Pest mapping assistant";

        tvCaption.setText(caption);
        locsmmanEngine = new LocsmmanEngine(this);
        pauline = new Pauline(this);
        pauline.setTaskListener(this);

        //if(mode==ChatbotAdapterRV.MODE_SCOUT)
        MyApplication.initLive(this,pauline.getTaskListener());
        MyApplication.refreshScanService(this);

        showButton();//hides all buttons

        itemsVisible = new ArrayList<>();

        if(mode==ChatbotAdapterRV.MODE_SCOUT)
                itemsVisible = pauline.getChatScout();
        else if(mode==ChatbotAdapterRV.MODE_MAP)
                itemsVisible=pauline.getChatMap();

        adapter = new ChatbotAdapterRV(this);
        adapter.setMode(mode);
        recyclerView.setAdapter(adapter);

        handler=new Handler();

        runnable=new Runnable() {
            @Override
            public void run() {
           processItem();
            }
        };

        handler.postDelayed(runnable,1000);

        ibTopRight.setImageResource(R.drawable.ic_close_dark);
        ibTopRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        toggleSpeaker();

          FragmentManager fm = getSupportFragmentManager();
        bsFragmentSimple= (BSFragmentSimple) fm.findFragmentById(R.id.frag_bsfragment);
        if(mode==ChatbotAdapterRV.MODE_MAP)
            initBottomSheet(true);
        else
            initBottomSheet(false);
    }

    private void processItem(){
        if(shouldProcess()) {
            Log.d(TAG, "process item");
            InstructionScout instruction = null;
            if (index < itemsVisible.size())
                instruction = itemsVisible.get(index);
            else
                end();

            if (instruction != null) {
                adapter.addItem(instruction);
                scroll(adapter.getItemCount() - 1);

                if(instruction.getAction()==InstructionScout.ACTION_SCAN&&mode==ChatbotAdapterRV.MODE_MAP)
                    speak(isFrench? resolveString(R.string.ai_map_attached_fr):resolveString(R.string.ai_map_attached));
                else if (instruction.getMessage() != null) speak(instruction.getMessage());
            }
            index++;

            if (instruction != null) {
                if (instruction.getAction() == InstructionScout.ACTION_TASK) {
                    speedShowing=false;
                    //show ok button
                    showButton(btnOk.getId());

                    btnOk.setText(isFrench? resolveString(R.string.yes_fr): resolveString(R.string.yes_));
                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(mode==ChatbotAdapterRV.MODE_SCOUT)
                                    openScanner();
                            else if(mode==ChatbotAdapterRV.MODE_MAP)
                                    snapshotLocation();
                        }
                    });

                    if(mode==ChatbotAdapterRV.MODE_MAP)
                        showButton(btnOk.getId(),btnPositive.getId());

                    btnPositive.setText(isFrench?resolveString(R.string.ai_map_done_fr):resolveString(R.string.ai_map_done));
                    btnPositive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                if(mode==ChatbotAdapterRV.MODE_MAP)
                                    submitMapping();
                                else
                                    finish();
                        }
                    });

                    stopHandler();
                }
                else {
                    if(!speedShowing) {
                        showButton(btnOk.getId());
                        speedShowing=true;
                    }
                    btnOk.setText(speed==SPEED_SLOW? resolveString(R.string.speed_up):resolveString(R.string.slow_down));
                    btnOk.setOnClickListener(speedListener);
                    handler.postDelayed(runnable, DELAY_TIME);
                    //showButton();//empty argument means hide all buttons
                }
            }
        }
    }

    private void openScanner(){
        Intent i=new Intent(Scout.this,CameraScanSimple.class);
        InstructionScout instructionScout=null;

        if(index<itemsVisible.size())
              instructionScout=itemsVisible.get(index);

        if(instructionScout!=null&&instructionScout.getId()!=null) {
            i.putExtra("item_id",instructionScout.getId());
            i.putExtra("map_id",instructionScout.getScout_id());
            i.putExtra("point_number",instructionScout.getPoint_number());
            i.putExtra("plant_number",instructionScout.getPlant_number());
            stopHandler();
            startActivityForResult(i, OPEN_SCANNER);
        }
    }

      //    for mapping
      private void snapshotLocation(){
            POI poi=bsFragmentSimple.getPoi();
            if(poi!=null) {
                  String accuracy=poi.getAccuracy();
                  if(accuracy!=null&&!accuracy.trim().equalsIgnoreCase("null")
                          &&!accuracy.trim().isEmpty()) {

                        if(Integer.parseInt(accuracy)<=settings.getAccuracy_max()) {
                              String message = poi.getLat() + "#" + poi.getLon() + "#" + poi.getAccuracy();
                              strPOIS.add(message);
                              Log.d(TAG, "date: " + message);
                              InstructionScout instructionScout = null;
                              if (itemsVisible.size() > adapter.getItemCount())
                                    instructionScout = itemsVisible.get(adapter.getItemCount());

                              Log.d(TAG,"form_id: "+form_id+" instructionScount: "+instructionScout);
                              if (instructionScout != null) {
                                    if(form_id!=null)
                                          instructionScout.setScout_id(instructionScout.getScout_id()+"_"+form_id);

                                    mapping_id=instructionScout.getScout_id();
                                    instructionScout.setMessage(message);
                                    itemsVisible.set(adapter.getItemCount(), instructionScout);

                                    instructionScout.setStatus(InstructionScout.STATE_UNPROCCESSED);

                                    action++;
                                    Log.d(TAG,"before add scoutItem");
                                    databaseHelper.addScoutItem(instructionScout, mode);
                                    items.add(instructionScout);
                              }
                              // MyApplication.refreshScanService(this);
                              handler.postDelayed(runnable, 1000);
                        } else {

                              MyApplication.showToast(this,
                                      "Ensure accuracy is below "+(settings.getAccuracy_max()+1)
                                              +" before you continue",Gravity.CENTER);
                              btnBSHandle.performClick();
                        }
                  } else {
                        btnBSHandle.performClick();
                  }
            } else
                  btnBSHandle.performClick();
      }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== Activity.RESULT_OK){
            if(requestCode==OPEN_SCANNER){
                int success=data.getIntExtra("success",0);
                Log.d(TAG,"success: "+success);
                if(success==1) {
                    if(MyApplication.itemsLivePosition!=null)
                        MyApplication.itemsLivePosition.add(adapter.getItemCount());

                    handler.postDelayed(runnable, 1000);
                }
                MyApplication.refreshScanService(this);
            }
        }
        else {}
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void refresh(){
        MainActivityAutomated.mapModel=new MapModel();
        MainActivityAutomated.pois=new ArrayList<>();
    }

    public void end(){
       showButton(btnNegative.getId());
       btnNegative.setText(isFrench?resolveString(R.string.ai_scout_done_fr):resolveString(R.string.ai_scout_done));
       btnNegative.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               finish();
           }
       });
    }

    private void stopHandler(){
        Log.d(TAG,"removing handler...");
        if(handler!=null) handler.removeCallbacks(runnable);
    }

    @Override
    public void onTaskAccepted(String id) {
        //id is the position of the task in the list
    }

    @Override
    public void onTaskStarted(String id) {

    }

    @Override
    public void onTaskStopped(String id, boolean complete) {
       Log.d(TAG,"task complete "+complete);
        if(complete){
            InstructionScout instructionScout=databaseHelper.getInstructScout(id,mode);
            Log.d(TAG,"instruction: "+instructionScout+" id "+instructionScout.getId());
            if(instructionScout!=null){
                    int index=getIndex(itemsVisible,instructionScout);
                    if(index>=0)
                        adapter.updateItem(index,instructionScout);
            }
        }
    }

    @Override
    public void onTaskCancelled(String id) {

    }

    @Override
    protected void onDestroy() {
        if(MyApplication.tts!=null){
            MyApplication.tts.stop();
        }
        super.onDestroy();
        stopHandler();
        MyApplication.shutdownLive();
    }

    private boolean shouldProcess(){
        if(speed==SPEED_SLOW&&MyApplication.tts!=null&&MyApplication.ttsAvailable&&speakEnabled&&MyApplication.tts.isSpeaking()){
            handler.postDelayed(runnable, 1000);
            Log.d(TAG,"speaking...");
            return false;
        }

        return true;
    }

    private void initBottomSheet(boolean init){

        bottomSheet = findViewById(R.id.bottom_sheet);

        if(!init) bottomSheet.setVisibility(View.GONE);

        btnBSHandle=bottomSheet.findViewById(R.id.bg_bottomsheet_handle);
        btnBSHandle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int bsState=mBottomSheetBehavior.getState();
                if(bsState==BottomSheetBehavior.STATE_COLLAPSED){
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                else if(bsState==BottomSheetBehavior.STATE_EXPANDED) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });

        //  View bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setPeekHeight(70);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                //  Toast.makeText(getActivity(),"bs callback",Toast.LENGTH_SHORT).show();
                if(newState==BottomSheetBehavior.STATE_COLLAPSED) {
                   // ibBSHandle.setImageResource(R.drawable.ic_arrow_up);
                    bsOpened=false;
                }
                else if (newState==BottomSheetBehavior.STATE_EXPANDED) {
                //    ibBSHandle.setImageResource(R.drawable.ic_arrow_down);
                    bsOpened=true;
                }
                //bsOpened=!bsOpened;
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        bottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickLocationPermission();
                //Toast.makeText(ScanModel.this,"Refreshing location updates...",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void speak(CharSequence text){
        if (MyApplication.ttsAvailable&&speakEnabled&&Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                tts.speak(text,TextToSpeech.QUEUE_FLUSH,null,"id1");
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    public static int getIndex(ArrayList<InstructionScout> items,InstructionScout instructionScout){
        int index=-1;
        if(MyApplication.itemsLivePosition!=null&&MyApplication.itemsLivePosition.size()>0){
            index=MyApplication.itemsLivePosition.get(0);
            MyApplication.itemsLivePosition.remove(0);
        }
        else {
            index=(new MyApplication()).containsInstructScout(items, instructionScout);
        }

        return index;
    }

    private void showPop(String caption){

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.map_save_menu);

        // set the custom dialog components - text, image and button
          TextView tvCaption = dialog.findViewById(R.id.tv_caption);
        tvCaption.setText(caption);
        dialog.findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            submitMapping();
            dialog.cancel();
            }
        });

//        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//                dialog.cancel();
//            }
//        });

        dialog.show();
    }

    private void submitMapping(){
//        Log.d(TAG,"submitMapping action: "+action);
//            if (mode == ChatbotAdapterRV.MODE_MAP && strPOIS!=null&&strPOIS.size() > 0&&action>=3) {
//               //action is the number of pois saved to db;
//                if(myApplication.hasNetworkConnection(this)) {
//                    try {
//                        showProgress("Saving your farm. Please wait...");
//                        networkRequest.sendFAWMappingBulk(createForm.buildJSON_Mapping(
//                                createForm.buildPOI_Mapping(strPOIS)),
//                                mapping_id,this);
//                    } catch (JSONException e) {
//                        hideProgress();
//                        showPop("Couldn't process your farm way points. Try again. " +
//                                "If error persists, kindly restart the mapping procedure");
//                        e.printStackTrace();
//                    }
//                }
//                else {
//                    hideProgress();
//                    showPop("You don't have internet connectivity now. " +
//                            "You can activate your internet and try again, or attempt save later");
//                }
//            } else {
//                finish();
//            }
        }

    @Override
    public void onRespond(Object object) {

        Log.d(TAG,"object: "+object);
        if(object!=null) {
            if(object instanceof String&&((String) object).equalsIgnoreCase("error")){
                showPop("Connection to server failed. Try again.");
                hideProgress();
                return;
            }

            if(object instanceof String&&((String) object).equalsIgnoreCase("1")) {
                int index = 0;
                for (InstructionScout instructionScout : items) {
                    instructionScout.setStatus(InstructionScout.STATE_PROCCESSED);
                    items.set(index, instructionScout);
                    index++;
                }
                databaseHelper.addScoutItems(items, ChatbotAdapterRV.MODE_MAP);
                hideProgress();
                MyApplication.showToast(Scout.this,
                        "MapModel saved successfully",
                        Gravity.CENTER);
                finish();
            }
            else {
                hideProgress();
                showPop("Couldn't save farm. Try again.");
            }
        }
        else {
            hideProgress();
            showPop("Couldn't connect to server. Try again.");
        }
    }


    View.OnClickListener speedListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switchSpeed();
            ((Button)view).setText(speed==SPEED_SLOW? resolveString(R.string.speed_up): resolveString(R.string.slow_down));
        }
    };

    void switchSpeed(){
        if(speed==SPEED_SLOW) {
            speed = SPEED_FAST;
            DELAY_TIME=200;
            if(speakEnabled)
                toggleSpeaker();
        }
        else if(speed==SPEED_FAST) {
            speed = SPEED_SLOW;
            DELAY_TIME=3000;
            if(!speakEnabled)
                toggleSpeaker();
        }
    }
}
