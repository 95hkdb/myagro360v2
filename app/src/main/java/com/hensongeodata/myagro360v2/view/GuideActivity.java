package com.hensongeodata.myagro360v2.view;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.Button;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.fragment.GuideFragment;

public class GuideActivity extends BaseActivity {

    private static String TAG=GuideActivity.class.getSimpleName();
    private Button btnPrevious;
    private Button btnNext;
    GuideFragmentAdapter adapter;
    ViewPager viewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guide);
        //myApplication.buildGuide();

        adapter = new GuideFragmentAdapter(getSupportFragmentManager());

          viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(10);


          btnPrevious = findViewById(R.id.btn_previous);

          btnNext = findViewById(R.id.btn_next);
        btnNext.setOnClickListener(nextListener);

        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewPager.getCurrentItem()!=-1)
                    viewPager.setCurrentItem(viewPager.getCurrentItem()-1);//scroll to previous page
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //updateNavigation();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void next(){
        if((viewPager.getCurrentItem()+1)<adapter.getCount())
            viewPager.setCurrentItem(viewPager.getCurrentItem()+1);//scroll to next page
        else
            finish();
    }

    View.OnClickListener nextListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            next();
        }
    };


    public class GuideFragmentAdapter extends FragmentStatePagerAdapter {
        private String TAG= GuideFragmentAdapter.class.getSimpleName();
        private GuideFragment guideFragment;
        public GuideFragmentAdapter(FragmentManager fm) {
            super(fm);
            guideFragment =new GuideFragment();
        }

        public GuideFragment getFragment(){
            return guideFragment;
        }

        @Override
        public Parcelable saveState() {
            return super.saveState();
        }

        @Override
        public int getCount() {
            if(MyApplication.guides==null)
                return 0;
            else
                return MyApplication.guides.size();
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment= guideFragment.newInstance(position);
            return fragment;
        }
    }

}
