package com.hensongeodata.myagro360v2.view;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.receiver.PushReceiver;

/**
 * Created by user1 on 4/9/2018.
 */

public class DashboardFAW extends BaseActivity {

    WebView webView;
    ProgressBar progressBar;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_faw);

        if(!myApplication.hasSession(this)){
            Intent intent=new Intent(this, SigninSignup.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            networkRequest.close();
            finish();
            return;
        }


          progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
          webView = findViewById(R.id.webview);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);

        webView.loadUrl(Keys.APPS_FAW);
        webView.setWebViewClient(new WebViewClient() {


            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
            }

            @TargetApi(Build.VERSION_CODES.LOLLIPOP)


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.getUrl().toString());
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                         }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);

            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
            }
        });


        findViewById(R.id.v_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getIntent()!=null&&getIntent().getExtras()!=null){
                    if(getIntent().getExtras().getInt("source")== PushReceiver.SOURCE_PUSHY){
                        startActivity(new Intent(DashboardFAW.this,MainActivity.class));
                    }
                }

                finish();
            }
        });
    }
}
