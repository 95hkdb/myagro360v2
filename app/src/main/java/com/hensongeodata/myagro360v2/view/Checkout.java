package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.Interface.CheckoutListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.CheckoutAdapterRV;
import com.hensongeodata.myagro360v2.model.Product;

import java.util.List;

public class Checkout extends BaseActivity implements CheckoutListener {
private RecyclerView recyclerView;
private CheckoutAdapterRV adapter;
public static List<Product> productList;
private static TextView tvSubtotal;
private static TextView tvTax;
private static TextView tvTotal;
private static long tax =0;
private static long total=0;
private static long mSubtotal=0;
public static String TAG=Checkout.class.getSimpleName();
private boolean wentToPayment=false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout);
        ((TextView)(findViewById(R.id.tv_caption))).setText(resolveString(R.string.checkout));
        recyclerView=findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        tvSubtotal=findViewById(R.id.tv_subtotal);
        tvTax=findViewById(R.id.tv_tax);
        tvTotal=findViewById(R.id.tv_total);

        ImageButton ibBack=findViewById(R.id.ib_back);
        ibBack.setImageResource(R.drawable.ic_close_dark);
        ibBack.setVisibility(View.VISIBLE);
        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.btn_checkout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wentToPayment=true;
                startActivity(new Intent(Checkout.this,PaymentActivity.class));
            }
        });

     }

    @Override
    public void onResume() {
        super.onResume();
        showCart();
        calculateSubtotal();
    }

    void noItemToDisplay(){
         MyApplication.showToast(this,resolveString(R.string.nothing_to_display), Gravity.CENTER);
        if(wentToPayment){
            Intent i=new Intent();
            i.putExtra("response",1);
            setResult(Activity.RESULT_OK,i);
        }
        finish();
         return;
    }

    private void showCart(){
        productList=databaseHelper.getProducts(Product.STATUS_CART);
        adapter=new CheckoutAdapterRV(this,productList,this);
        recyclerView.setAdapter(adapter);
    }

    private void calculateSubtotal(){
        mSubtotal=0;
        for(Product product:productList){
            mSubtotal+=product.price*product.quantity;
        }

        updateCalculations();
    }


    private long updateCalculations(){
        total=mSubtotal+ tax;
        tvSubtotal.setText("GHc "+mSubtotal);
        tvTotal.setText("GHc "+total);
        tvTax.setText("GHc "+ tax);
        Log.d(TAG,"updateSubTotal: "+mSubtotal);
        if(mSubtotal==0)
            noItemToDisplay();

        return mSubtotal;
    }

    @Override
    public void add(int atIndex) {
        Product product=productList.get(atIndex);
        if(product.quantity<product.max)
                product.quantity+=1;
        else
            MyApplication.showToast(Checkout.this,"Maximum quantity in stock reached.",Gravity.CENTER);

        productList.set(atIndex,product);
        calculateSubtotal();
    }

    @Override
    public void subtract(int atIndex) {
        Product product=productList.get(atIndex);
        if(product.quantity>1)
                product.quantity-=1;
        else
            MyApplication.showToast(Checkout.this,"Minimum order quantity reached.",Gravity.CENTER);


        productList.set(atIndex,product);
        calculateSubtotal();
    }

    @Override
    public void remove(int atIndex) {
        if(databaseHelper.removeProduct(productList.get(atIndex))) {
            productList.remove(atIndex);
            calculateSubtotal();
            adapter.notifyDataSetChanged();
        }
    }
}
