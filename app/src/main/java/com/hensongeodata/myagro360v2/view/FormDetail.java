package com.hensongeodata.myagro360v2.view;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Form;

/**
 * Created by user1 on 1/30/2018.
 */

public class FormDetail extends BaseActivity {
    private static String TAG=FormDetail.class.getSimpleName();
    private TextView tvTitle;
    private TextView tvDescription;
    private TextView tvOrganization;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private Form form;

    private String form_id;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_details);

          toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Form Details");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

          tvTitle = findViewById(R.id.tv_title);
          tvDescription = findViewById(R.id.tv_description);
          tvOrganization = findViewById(R.id.tv_organization);

        form_id=getIntent().getExtras().getString("form_id");
        form=databaseHelper.getForm(form_id);
       // Log.d(TAG,"form_id: "+form_id+" form: "+form);
        if(form!=null){
            tvTitle.setText(form.getTitle());
            tvDescription.setText(form.getDescription());
            tvOrganization.setText(form.getOrganization().getName());
        }
    }
}
