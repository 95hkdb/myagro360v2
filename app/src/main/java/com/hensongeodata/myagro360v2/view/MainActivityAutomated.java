package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.adapter.IntroAdapterRV;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.MyViewpager;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.fragment.BSFragment;
import com.hensongeodata.myagro360v2.fragment.FormFragmentAutomated;
import com.hensongeodata.myagro360v2.model.Form;
import com.hensongeodata.myagro360v2.model.Intro;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.model.SettingsModel;
import com.hensongeodata.myagro360v2.service.UploadFilesService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivityAutomated extends BaseActivity {

    private static String TAG= MainActivityAutomated.class.getSimpleName();
    //private Handler handler;
    private static int MODE_DATA=200;
    private static int MODE_FILE=202;
    private static int MODE_IDLE=203;
    private static int submission_mode;
    private FloatingActionButton fabNext;
    private FloatingActionButton fabBack;
    private ActionBar actionBar;
    private BottomSheetBehavior mBottomSheetBehavior;
    private ImageButton ibBSHandle;
    private  View bottomSheet;
    private View btnBSHandle;
    private boolean bsOpened=false;
    private View navHeader;
    private MainActivityReceiver receiver;
    private IntentFilter filter;

    private Form form;
    FormFragmentAdapter adapter;
    MyViewpager viewPager;
    String field_id; //for testing purposes
    String transaction_id;//most recent transaction_id in play
    CollapsingToolbarLayout collapsingToolbarLayout;

    private NestedScrollView nsv_info;
    private RecyclerView recyclerView;
    private IntroAdapterRV introAdapterRV;
    private Handler handler;
    private Runnable runnable;
    private boolean allowExit=false;
    public static ArrayList<POI> pois;
    public static MapModel mapModel;
    private boolean  shouldClose=false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_automated);
          Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar=getSupportActionBar();

        if(MainActivityAutomated.mapModel!=null)
                Log.d(TAG,"oncreate map.id: "+MainActivityAutomated.mapModel.id+" map.type: "+MainActivityAutomated.mapModel.type);

          nsv_info = findViewById(R.id.nsv_info);
          recyclerView = findViewById(R.id.recyclerview);

        handler =new Handler();
        runnable=new Runnable() {
            @Override
            public void run() {
                allowExit=false;
            }
        };

        filter=new IntentFilter(BroadcastCall.TRANSACTION_UPDATE);
        receiver=new MainActivityReceiver();
        registerReceiver(receiver,filter);

          collapsingToolbarLayout = findViewById(R.id.collapsingToolbarLayout);

         //networkRequest.fetchForms(null);//no broadcast JoinCommunityResponse registered hence null
        // networkRequest.fetchForm("tLSxRB",false);
        // networkRequest.fetchForm("AdlUZ6",false);
       //  networkRequest.fetchForm("NvkbnF",false);

        LayoutInflater mInflater = LayoutInflater.from(this);
        View actionBarCustom = mInflater.inflate(R.layout.actionbar_custom, null);
        actionBar.setCustomView(actionBarCustom);

        initBottomSheet();

          fabBack = findViewById(R.id.fab_back);
          fabNext = findViewById(R.id.fab_submit);
        adapter = new FormFragmentAdapter(getSupportFragmentManager());

          viewPager = findViewById(R.id.viewpager);
        viewPager.setPagingEnabled(false);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(10);


        fabNext.setOnClickListener(nextListener);
        fabNext.setVisibility(View.GONE);

        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewPager.getCurrentItem()!=-1)
                    viewPager.setCurrentItem(viewPager.getCurrentItem()-1);//scroll to previous page
            }
        });

        updateFabIcon();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                updateFabIcon();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    public static JSONArray getPOIList(){
        JSONArray jsonArray=new JSONArray();
        try {
        if(pois!=null){
            for (POI poi : pois) {
                JSONObject jsonObject = new JSONObject();
                if(poi!=null) {
                    jsonObject.put("latitude", poi.getLat());
                    jsonObject.put("longitude", poi.getLon());
                    jsonObject.put("accuracy", poi.getAccuracy());
                }
                jsonArray.put(jsonObject);
            }

        }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }


    public static JSONObject getMappingDetails(){
        JSONObject jsonObject=new JSONObject();
        try {
            if(mapModel!=null){
                jsonObject.put("name",mapModel.name);
                jsonObject.put("type",mapModel.type);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static void clearMappingReference(){
        mapModel=new MapModel();
        pois=new ArrayList<>();
    }

    public static void removeMap(Context context){
        Log.d(TAG,"remove map mapModel: "+mapModel+" pois: "+pois);
        Log.d(TAG,"remove map mapModel.id: "+mapModel.id+" pois.size: "+pois.size());
        if(mapModel!=null&& mapModel.id!=null &&pois!=null&&pois.size()>0){
            (new DatabaseHelper(context)).removeScoutListItem(mapModel.id, ChatbotAdapterRV.MODE_MAP);
        }

        //clearMappingReference();
    }

    public void next(int sectionIndex){

        FormFragmentAutomated formFragment= (FormFragmentAutomated) adapter.instantiateItem(viewPager,sectionIndex);
        if(formFragment.validate()){ //particular form in focus is validated
            if((sectionIndex+1)>=adapter.getCount()){

                try {

                   //if(MyApplication.isEmptyPOI(myApplication.getInstantPOI(this,true)))
                      // return;

                        transaction_id=databaseHelper.saveArchive(createForm.buildJSON(MainActivityAutomated.this,null,false));

                    if(settings.isArchive_mode()){
                        archiveForm(transaction_id);
                    }
                    else if(!settings.isArchive_mode()){
                        submitForm(transaction_id);
                    }
                    else {
                        archiveForm(transaction_id);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgress();
                    Toast.makeText(MainActivityAutomated.this, "Form failed to save.", Toast.LENGTH_SHORT).show();
                }
            }
            else {
                viewPager.setCurrentItem(sectionIndex+1);//scroll to next page
            }
        }

    }

    public void next(){

        FormFragmentAutomated formFragment= (FormFragmentAutomated) adapter.instantiateItem(viewPager,viewPager.getCurrentItem());
        if(formFragment.validate()){ //particular form in focus is validated
            if((viewPager.getCurrentItem()+1)>=adapter.getCount()){
                try {
                     // if(MyApplication.isEmptyPOI(myApplication.getInstantPOI(this,true)))
                      //  return;

                        transaction_id=databaseHelper.saveArchive(createForm.buildJSON(MainActivityAutomated.this,null,false));

                    if(settings.isArchive_mode()){
                        archiveForm(transaction_id);
                    }
                    else if(!settings.isArchive_mode()){
                        submitForm(transaction_id);
                    }
                    else {
                        archiveForm(transaction_id);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgress();
                    Toast.makeText(MainActivityAutomated.this, "Form failed to save.", Toast.LENGTH_SHORT).show();
                }
            }
            else {
                viewPager.setCurrentItem(viewPager.getCurrentItem()+1);//scroll to next page
            }
        }

    }

    View.OnClickListener nextListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            next();
        }
    };

    private void switchView(){
        if(settings.getForm_id()!=null||settings.getOrg_id()!=null
                ||settings.getTransaction_id()!=null)
            showMain();
        else
            showIntro();

    }

    private void showMain(){
        Log.d(TAG,"show Main");
        viewPager.setVisibility(View.VISIBLE);
        bottomSheet.setVisibility(View.VISIBLE);
        nsv_info.setVisibility(View.GONE);
        updateFabIcon();
    }

    private void showIntro(){
        Log.d(TAG,"show Intro");
        nsv_info.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.GONE);
        fabBack.setVisibility(View.GONE);
        fabNext.setVisibility(View.GONE);
        bottomSheet.setVisibility(View.GONE);

        if(settings.getFirstTime()) {
            Intro intro = new Intro();
            intro.setCaption(getResources().getString(R.string.intro_caption));
            intro.setMessage(getResources().getString(R.string.intro_message));

            ArrayList<Object> introItems = new ArrayList<>();
            introItems.add(intro);
            introItems.add(null);

            introAdapterRV = new IntroAdapterRV(this, introItems);
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            recyclerView.setAdapter(introAdapterRV);
        }
        else {
            nsv_info.findViewById(R.id.v_guide).setVisibility(View.GONE);
            View v=nsv_info.findViewById(R.id.v_menu_mini);

            v.setVisibility(View.VISIBLE);
            ((TextView)v.findViewById(R.id.tv_action)).setText("Add details");
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showMenu();
                }
            });
        }
        settings.setFirstTime(false);
    }

    private void refreshForm(){
        Log.d(TAG,"refreshing: ");
        if(settings.getTransaction_id()==null&&settings.getForm_id()!=null) {
            createForm.sections = databaseHelper.displayForm(settings.getForm_id());
        }
        else if(settings.getTransaction_id()!=null){
            createForm.sections = databaseHelper.displayForm();
            myApplication.selectPOIGroup(this,databaseHelper);//create a temp default group
        }
        adapter = new FormFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        form=databaseHelper.getForm(settings.getForm_id());
        Log.d(TAG,"form: "+form);
      //  if(form!=null)
      //      Log.d(TAG,"form title: "+form.getTitle());

        if(form!=null&&form.getTitle()!=null&&!form.getTitle().isEmpty()) {
            if(form.getId()!=null&&form.getId().equalsIgnoreCase("3628"))
                FixedActivity.scannerActive=true;

            collapsingToolbarLayout.setTitle(form.getTitle());
        }
        else {
            collapsingToolbarLayout.setTitle(getResources().getString(R.string.app_name));
        }

        switchView();//switch between intro view and form view

        createForm.setShouldRefreshForm(false);
        submission_mode=MODE_DATA;

       // Log.d(TAG,"pic form: "+databaseHelper.getFormByCode("tLSxRB"));
       // Log.d(TAG,"vid form: "+databaseHelper.getFormByCode("AdlUZ6"));
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"shouldClose: "+shouldClose);
        if(shouldClose){
            MappingActivity.submitted=true;
            Intent i=new Intent();
            setResult(Activity.RESULT_OK,i);
            finish();
        }
        else {
            switchView();//switch between intro view and form view

            Log.d(TAG, "refresh: " + createForm.isShouldRefreshForm()
                    + " form id: " + (new SettingsModel(this)).getForm_id());
            if (createForm.isShouldRefreshForm()) {
                (new Handler()).post(new Runnable() {
                    @Override
                    public void run() {
                        refreshForm();//basically reloads form
                    }
                });
            }
            updateFabIcon();
            BroadcastCall.publishLocationUpdate(this, NetworkRequest.STATUS_SUCCESS, BSFragment.ACTION_DIMENSION);
        }
        }

    public class FormFragmentAdapter extends FragmentStatePagerAdapter {
        private String TAG= FormFragmentAdapter.class.getSimpleName();
        private FormFragmentAutomated formFragment;
        public FormFragmentAdapter(FragmentManager fm) {
            super(fm);
            formFragment =new FormFragmentAutomated();
        }

        public Fragment getFragment(){
            return formFragment;
        }

        @Override
        public Parcelable saveState() {
            return super.saveState();
        }

        @Override
        public int getCount() {
            return createForm.sections.size();
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment= formFragment.newInstance(position);
            return fragment;
        }
    }

    private void updateFabIcon(){

        if(adapter.getCount()==0)
            fabNext.setVisibility(View.GONE);
        else {
            if(findViewById(R.id.btn_next)!=null) {
                if (viewPager.getCurrentItem() + 1 == adapter.getCount() || viewPager.getCurrentItem() == adapter.getCount()) {
                    if (settings.isPreviewForm())
                        ((Button) findViewById(R.id.btn_next)).setText("Preview");
                    else if (settings.isArchive_mode())
                        ((Button) findViewById(R.id.btn_next)).setText("Save Form");
                    else
                        ((Button) findViewById(R.id.btn_next)).setText("Submit Form");
                } else
                    ((Button) findViewById(R.id.btn_next)).setText("Next page");

                  findViewById(R.id.btn_next).setOnClickListener(nextListener);
            }
            if (adapter.getCount() != 1) {
                if (viewPager.getCurrentItem() != 0)
                    fabBack.setVisibility(View.VISIBLE);
                else
                    fabBack.setVisibility(View.GONE);
            } else
                fabBack.setVisibility(View.GONE);
        }
        }

    public String getBtnText(int sectionIndex){

        if(adapter.getCount()!=0){

                if (sectionIndex+1 ==createForm.sections.size()) {
                    if (settings.isPreviewForm())
                        return "Preview";
                    else if (settings.isArchive_mode())
                        return "Save Form";
                    else
                        return "Submit Form";
                } else
                    return "Next page";
        }

        return null;
    }

    private void submitForm(String transaction_id){
        if(triggerAddMapping()){
            return;
        }
        if(settings.isPreviewForm()) {
            archiveForm(transaction_id);
        }
        else {
            if(myApplication.hasNetworkConnection(this)) {
                showProgress("Submitting Form. Please wait...");
                try {
                    submission_mode=MODE_DATA;
                    networkRequest.sendTransaction(createForm.buildJSON(MainActivityAutomated.this,null,true));
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgress();
                    Toast.makeText(this, "Form submission failed", Toast.LENGTH_SHORT).show();
                }
            }
            else {
                myApplication.showInternetError(this);
            }

        }
    }

    private void archiveForm(String transaction_id){
        if(triggerAddMapping()){
           return;
        }
        showProgress("Saving Form...");
        //form archived temporarily and transaction id used in preview mode
        if(transaction_id!=null) {
            hideProgress();
            Toast.makeText(MainActivityAutomated.this, "Form saved successfully.", Toast.LENGTH_SHORT).show();

            if(settings.isPreviewForm())
                showPreview(PreviewForm.SOURCE_FORM,transaction_id);
            else
                refreshForm();
        }
        else {
            hideProgress();
            Toast.makeText(MainActivityAutomated.this, "Form failed to save.", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean triggerAddMapping(){

        return false;
    }

    private boolean hasPOI(){
        return true;
    //return (myApplication.poiGroup.getPoiList()!=null&&myApplication.poiGroup.getPoiList().size()>0);
}

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent();
        setResult(Activity.RESULT_CANCELED,i);
        finish();
    }

    private void initBottomSheet(){

        bottomSheet = findViewById(R.id.bottom_sheet);
        btnBSHandle=bottomSheet.findViewById(R.id.bg_bottomsheet_handle);
          ibBSHandle = bottomSheet.findViewById(R.id.ib_bottomsheet_handle);
        ibBSHandle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getActivity(),"handle click",Toast.LENGTH_SHORT).show();
                int bsState=mBottomSheetBehavior.getState();
                if(bsState==BottomSheetBehavior.STATE_COLLAPSED){
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                else if(bsState==BottomSheetBehavior.STATE_EXPANDED) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });

        btnBSHandle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ibBSHandle.performClick();
            }
        });

        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setPeekHeight(100);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                //  Toast.makeText(getActivity(),"bs callback",Toast.LENGTH_SHORT).show();
                if(newState==BottomSheetBehavior.STATE_COLLAPSED) {
                    ibBSHandle.setImageResource(R.drawable.ic_arrow_up);
                    bsOpened=false;
                }
                else if (newState==BottomSheetBehavior.STATE_EXPANDED) {
                    ibBSHandle.setImageResource(R.drawable.ic_arrow_down);
                    bsOpened=true;
                }
                //bsOpened=!bsOpened;
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        bottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickLocationPermission();
                Toast.makeText(MainActivityAutomated.this,"Refreshing location updates...",Toast.LENGTH_SHORT).show();
            }
        });
    }

    class MainActivityReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int status=intent.getExtras().getInt("status");
            String msg=intent.getExtras().getString("date");
            Log.d(TAG,"status: "+status+" msg: "+msg+" submission_mode:"+submission_mode);
            if(status== NetworkRequest.STATUS_SUCCESS){
                if(submission_mode==MODE_DATA){
                    submission_mode=MODE_FILE;
                    UploadFilesService.progressDialog=getProgress();
                    startService(new Intent(MainActivityAutomated.this, UploadFilesService.class));
                   // hideProgress();
                }
                else if(submission_mode==MODE_FILE) {
                    uploadComplete();
                    Toast.makeText(context,"Data submitted successfully",Toast.LENGTH_SHORT).show();
                    hideProgress();
                    if(MappingActivity.refreshListener!=null)
                         MappingActivity.refreshListener.reload(mapModel.id);

                    shouldClose=true;
                }
                else {
                    if(msg!=null)
                        myApplication.showMessage(context,msg);
                    else
                        myApplication.showMessage(context,"Submission mode not specified.");

                    hideProgress();
                    if(MappingActivity.refreshListener!=null)
                        MappingActivity.refreshListener.reload(mapModel.id);

                    shouldClose=true;
                }
            }
            else if(status==NetworkRequest.STATUS_FAIL){
                if(submission_mode==MODE_DATA) {
                    if(msg!=null)
                        myApplication.showMessage(context,msg);
                    else
                        myApplication.showMessage(context,
                                "There is a problem with your internet connection. Try again");

                }
                else if(submission_mode==MODE_FILE)
                    myApplication.showMessage(context,"Data submission incomplete.");

                hideProgress();
            }
        }
    }

    private void uploadComplete(){
        createForm.setShouldRefreshForm(true);
        settings.setTransaction_id(null);
        refreshForm();
        hideProgress();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds itemsVisible to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_automated, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_info) {
            Intent i=new Intent(this,FormDetail.class);
            i.putExtra("form_id",settings.getForm_id());
            startActivity(i);
            return true;
        }
        else if (id == R.id.action_add) {
            showMenu();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.online=false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
        if(handler!=null)
            handler.removeCallbacks(runnable);
    }

    private void registerReceiver(){
        registerReceiver(receiver,filter);
    }


    void showMenu(){
        final Dialog dialog=new Dialog(MainActivityAutomated.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.menu_mainactivity_automated);

        dialog.findViewById(R.id.tv_form_code).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    startActivity(new Intent(MainActivityAutomated.this,AddForm.class));
                    dialog.cancel();
            }
        });

        dialog.findViewById(R.id.tv_form_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    startActivity(new Intent(MainActivityAutomated.this,OrgList.class));
                    dialog.cancel();
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

    }

}