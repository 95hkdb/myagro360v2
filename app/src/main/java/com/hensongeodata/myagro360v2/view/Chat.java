package com.hensongeodata.myagro360v2.view;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.hensongeodata.myagro360v2.Interface.JoinCommunityListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.MessagingAdapterRV;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.controller.TouchImageView;
import com.hensongeodata.myagro360v2.model.Author;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.Message;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.receiver.PushReceiver;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.messages.MessageInput;

import org.nibor.autolink.LinkExtractor;
import org.nibor.autolink.LinkSpan;
import org.nibor.autolink.LinkType;
import org.nibor.autolink.Span;
import org.owasp.encoder.Encode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.Locale;
import java.util.UUID;

import id.zelory.compressor.Compressor;

public class Chat extends BaseActivity   implements MessageInput.AttachmentsListener, MessageInput.InputListener, JoinCommunityListener {
 private static String TAG=Chat.class.getSimpleName();
    MessageInput messageInput;
    MessagingAdapterRV adapter;
    ArrayList<Message> messages;
    static Author mAuthor;
    MessageReceiver receiver;
    IntentFilter filter;
    private RecyclerView recyclerView;
    Message uMessage;
    private Dialog dialogImage;
    private Dialog dialogGroupSwitch;

    private Image image;
    com.esafirm.imagepicker.model.Image image_lib;
    private MessageInput imgMessageInput;
    private static int REQUEST_CAMERA=100;
    private static int SELECT_FILE=200;
    String imageFilePath;
    private TouchImageView mImageView;
    private String group_id;
    private int source;
    public static String SOURCE="source";
    public static int SOURCE_SHARE=200;
    private Message xternalMessage;
    private Uri uri;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat);
        MyApplication.in_chat=true;
        cancelChatNotification();

        processExternalAppShare();

        receiver=new MessageReceiver();
        filter=new IntentFilter(BroadcastCall.MESSAGE);
        registerReceiver(receiver,filter);

      source=getIntent().getIntExtra(SOURCE,0);

      if(source==SOURCE_SHARE&&MyApplication.xternalMessage!=null){
          xternalMessage=MyApplication.xternalMessage;
          group_id=xternalMessage.getGroup_id();
      }

          TextView tvCaption = findViewById(R.id.tv_caption);
        Log.d(TAG,"active chatroom: "+user.getChatroomActive_id());
        if(user.getChatroomActive_id()!=null)
            tvCaption.setText(getChatroomName(this,Integer.parseInt(user.getChatroomActive_id())));
        else
            tvCaption.setText(resolveString(R.string.community_title));

        ImageButton ibBack=findViewById(R.id.ib_back);
        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close();
            }
        });
        ibBack.setImageResource(R.drawable.ic_back);
        ibBack.setVisibility(View.VISIBLE);
        ImageButton ibAction=findViewById(R.id.ib_action);
        ibAction.setImageResource(R.drawable.ic_group);
        ibAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            showGroupSwitchDialog();
            }
        });
        ibAction.setVisibility(View.VISIBLE);

        mAuthor=getAuthor(this);

        initImageDialog();
        initGroupSwitchDialog();

        messages=new ArrayList<>();

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setStackFromEnd(true);
          recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(linearLayoutManager);

        adapter=new MessagingAdapterRV(this,messages,user.getId());
        recyclerView.setAdapter(adapter);

        //firstVisibleInListview = linearLayoutManager.findFirstVisibleItemPosition();

        messageInput=findViewById(R.id.input);
        messageInput.setInputListener(this);

        messageInput.setAttachmentsListener(this);

        if(user.getChatroomActive_id()!=null&&!user.getChatroomActive_id().trim().isEmpty())
            loadMessages();
        else
            joinCommunity();


    }


    @Override
    public void onAddAttachments() {
        selectImage();
    }

    @Override
    public boolean onSubmit(CharSequence input) {
        Message mMessage=new Message();
        mMessage.setText(input.toString().trim());
        pushMessage(mMessage);

        return false;
    }

    private void pushMessage(Message message){
        message.setCreatedAt(new Date());
        message.setStatus(Message.STATUS_PENDING);
        message.setId( UUID.randomUUID().toString().replaceAll("-", "").toUpperCase());
        message.setAuthor(mAuthor);
        message.setGroup_id(user.getChatroomActive_id());
        message.setSeen(Message.SEEN);
        databaseHelper.addMessage(message);
        BroadcastCall.publishMessage(Chat.this,message.getId(), NetworkRequest.STATUS_SUCCESS);
        messageInput.getInputEditText().setText(null);

    }

    @Override
    public void JoinCommunityResponse(String response, int status) {
        hideProgress();
        Log.d(TAG,"response: "+response);
        if(response!=null&&response.contains("joinedcommunity")){
            user.setChatroomActive_id(getChatroom_id(Chat.this,resolveString(R.string.community_title)));
            MyApplication.showToast(Chat.this,resolveString(R.string.success),Gravity.CENTER);
            loadMessages();
        }
        else {
            shutdown(resolveString(R.string.no_response));
        }
    }
    private void shutdown(String date){
        MyApplication.showToast(this,resolveString(R.string.no_response), Gravity.CENTER);
        finish();
    }

    private void joinCommunity(){
        if(myApplication.hasNetworkConnection(this)){
            showProgress(resolveString(R.string.join_community));
            networkRequest.joinCommunity(user,this);
        }
        else{
            shutdown(resolveString(R.string.no_internet_message));
        }

    }

    private class MessageReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
                String message_id=intent.getStringExtra("msg_id");
            Log.d(TAG,"message_id: "+message_id);
                if(message_id!=null){
                    uMessage=databaseHelper.getMessage(message_id);
                    uMessage.setCreatedAt(new Date());

                    if(uMessage!=null) {
                       // messages.add(uMessage);
                        adapter.addMessage(uMessage);
                        adapter.notifyDataSetChanged();

                        recyclerView.post(new Runnable() {
                            @Override
                            public void run() {
                                if(adapter.getItemCount()>0)
                                        recyclerView.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                recyclerView.smoothScrollToPosition(adapter.getItemCount()-1);
                                            }
                                        });
                            }
                        });
                    }
                }

                Log.d(TAG,"adapter count: "+adapter.getItemCount());
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        MyApplication.in_chat=true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.in_chat=false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(receiver!=null)
            unregisterReceiver(receiver);

    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        //this.menu = menu;
        getMenuInflater().inflate(R.menu.community, menu);
        //onSelectionChanged(0);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_switch_group:
                    showGroupSwitchDialog();
                break;
//            case R.id.action_copy:
//                messagesAdapter.copySelectedMessagesText(this, getMessageStringFormatter(), true);
//                AppUtils.showToast(this, R.string.copied_message, true);
//                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
            super.onBackPressed();
            close();
    }

    private void loadMessages(){
            new Handler().postDelayed(new Runnable() { //imitation of internet connection
                @Override
                public void run() {

                    ArrayList<Message> messages = databaseHelper.getMessages(Chat.this);
                    adapter.addToEnd(messages,true);
                    if(xternalMessage!=null){
//                            if(xternalMessage.getImage()==null)
//                                    adapter.addMessage(xternalMessage);
//                        else {
                                image=xternalMessage.getImage();
                                if(image!=null) {
                                    image.setImage_url(image.getImage_url());
                                    image.setUri(Uri.parse(image.getImage_url()));
                                    xternalMessage.setImage(image);

                                    Log.d(TAG, "loadmessages path: " + image.getImage_url());
                                    showImageDialog(image, xternalMessage != null ? xternalMessage.getText().trim() : xternalMessage.getText());
                                }
                                else{
                                    if(messageInput!=null&&messageInput.getInputEditText()!=null)
                                    messageInput.getInputEditText().setText(xternalMessage.getText());
                                }
                               // }
                }

                    MyApplication.xternalMessage=null;
                }
            }, 100);
    }

    private void initImageDialog(){

        dialogImage = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialogImage.setContentView(R.layout.chat_image_attachment);

          mImageView = dialogImage.findViewById(R.id.iv_image);
        dialogImage.findViewById(R.id.ib_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogImage.cancel();
            }
        });

        imgMessageInput=dialogImage.findViewById(R.id.input_attachment);
        //imgMessageInput.getButton().setVisibility(View.GONE);
        imgMessageInput.setInputListener(new MessageInput.InputListener() {
            @Override
            public boolean onSubmit(CharSequence input) {
                Message mMessage=new Message();
                mMessage.setText(input.toString().trim());
                mMessage.setImage(image);
                pushMessage(mMessage);

                imgMessageInput.getInputEditText().setText(null);
                dialogImage.cancel();
                return false;
            }
        });

        (dialogImage.findViewById(R.id.fab_send)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message mMessage=new Message();
                mMessage.setText(imgMessageInput.getInputEditText().getText().toString().trim());
                mMessage.setImage(image);
                pushMessage(mMessage);

                imgMessageInput.getInputEditText().setText(null);
                dialogImage.cancel();
            }
        });

    }

    private void initGroupSwitchDialog(){

        dialogGroupSwitch = new Dialog(this);
        dialogGroupSwitch.setContentView(R.layout.groups);

        dialogGroupSwitch.findViewById(R.id.tv_community).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user.setChatroomActive_id(getChatroom_id(Chat.this,resolveString(R.string.community_title)));
                dialogGroupSwitch.cancel();
                startActivity(getIntent());
                finish();
            }
        });

        String profession_id=(new User(this)).getProfession_id();
        Log.d(TAG,"profession: "+profession_id);

        if(profession_id!=null&&!profession_id.trim().isEmpty()) {
            final int pro_id=Integer.parseInt(profession_id);

            ((TextView) dialogGroupSwitch.findViewById(R.id.tv_other)).setText(getChatroomName(this,pro_id ));
            dialogGroupSwitch.findViewById(R.id.tv_other).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    user.setChatroomActive_id(String.valueOf(pro_id));
                    dialogGroupSwitch.cancel();
                    startActivity(getIntent());
                    finish();
                }
            });
        }
        else {
            dialogGroupSwitch.findViewById(R.id.tv_other).setVisibility(View.GONE);
        }
    }

    private void showGroupSwitchDialog(){
        dialogGroupSwitch.show();
    }

    private void showImageDialog(Image image){
        imgMessageInput.getInputEditText().setText(null);
        Log.d(TAG,"preview url: "+image.getUri());
        //mImageView.setImage(ImageSource.uri(fixPath(image.getImage_url())));
        Picasso.get().load(fixPath(image.getImage_url())).placeholder(R.drawable.blurred_image).resize(1000,1000)
                .centerInside().into(mImageView);
        dialogImage.show();
    }

    private void showImageDialog(Image image,String textPlaceholder){
        imgMessageInput.getInputEditText().setText(""+textPlaceholder);
        Log.d(TAG,"preview url: "+image.getImage_url()+" uri: "+image.getUri());
        //mImageView.setImage(ImageSource.uri(fixPath(image.getImage_url())));
        Picasso.get().load(fixPath(image.getImage_url())).placeholder(R.drawable.blurred_image).resize(800,800)
                .centerInside().into(mImageView);
        dialogImage.show();
    }

    private String fixPath(String path){
        if(path!=null){
            if(path.length()>1){
                if(path.charAt(0)=='/'||path.charAt(0)!='f'){
                    path="file://"+path;
                }
            }
        }

            return path;
    }


    private void selectImage() {
        ImagePicker.create(this)
                .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .toolbarArrowColor(Color.WHITE) // Toolbar 'up' arrow color
                .single() // single mode
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .theme(R.style.AppTheme) // must inherit ef_BaseTheme. please refer to sample
                .enableLog(false) // disabling log // custom image loader, must be serializeable
                .start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
             onSelectImage(ImagePicker.getFirstImageOrNull(data));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void onSelectImage(com.esafirm.imagepicker.model.Image image_lib) {

        String compressedPath=compressImage(image_lib.getPath());
        //String compressedPath=image_lib.getPath();
        Log.d(TAG,"path: "+compressedPath);
        image=new Image();
        image.setImage_url(compressedPath);
        image.setUri(Uri.parse(compressedPath));
        showImageDialog(image);
    }

    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();
        return image;
    }

    protected static File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Locsmman Pro");

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");
    }

    public static String getPath( Context context, Uri uri ) {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "Not found";
        }
        return result;
    }

    private String compressImage(String path){
        String mPath = null;
        if(path!=null) {
            File file = new File(path);
            File compressedImageFile = null;
            try {
                if (file != null)
                    compressedImageFile = new Compressor(this).compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "compressed file: " + compressedImageFile);

            if (compressedImageFile != null) {
                mPath = compressedImageFile.getPath();
            }

            if(mPath==null||mPath.trim().isEmpty())
                mPath=path;
        }

        return mPath;
    }

    public static Author getAuthor(Context context){
        User user=new User(context);
        Author author=new Author();
        String fullname="";
        if(user.getFirstTime()!=null)
                fullname=user.getFirstname()+" ";
        if(user.getLastname()!=null)
                fullname=fullname+""+user.getLastname();

        if(fullname==null||fullname.trim().equalsIgnoreCase("null")||fullname.trim().isEmpty())
            fullname=user.getPhone();
        if(fullname==null||fullname.trim().equalsIgnoreCase("null")||fullname.trim().isEmpty())
            fullname=user.getEmail();
        if(fullname==null||fullname.trim().equalsIgnoreCase("null")||fullname.trim().isEmpty())
            fullname="Unknown";

        author.setName(fullname);
        author.setId(user.getId());
        author.setAvatar("");

        return author;
    }

    public static String getChatroom_id(Context context,String chatroom){
        int room_id;
//        Log.d(TAG,"chatroom: "+chatroom);
//        if(chatroom!=null){
//            if(chatroom.equalsIgnoreCase(context.getResources().getString(R.string.community_title)))
//                room_id=5347;
//            else if(chatroom.equalsIgnoreCase(context.getResources().getString(R.string.extension_officer)))
//                room_id=5348;
//            else if(chatroom.equalsIgnoreCase(context.getResources().getString(R.string.input_dealer)))
//                room_id=3;
//            else if(chatroom.equalsIgnoreCase(context.getResources().getString(R.string.entomologist)))
//                room_id=5349;
//            else if(chatroom.equalsIgnoreCase(context.getResources().getString(R.string.farmer)))
//                room_id=5350;
//           else
//               room_id=5347;
//        }
//        else
            room_id=5347;

     //   Log.d(TAG,"room_id: "+room_id);
        return String.valueOf(room_id);
    }

    public static String getChatroomName(Context context,int chatroom_id){
        String room_name=context.getResources().getString(R.string.coming_soon);
        Log.d(TAG,"room_id: "+chatroom_id);
//        if(chatroom_id==5347)
//            room_name=context.getResources().getString(R.string.community_title);
//        else if(chatroom_id==5348)
//            room_name=context.getResources().getString(R.string.extension_officer);
//        else if(chatroom_id==5351)
//            room_name=context.getResources().getString(R.string.input_dealer);
//        else if(chatroom_id==5349)
//            room_name=context.getResources().getString(R.string.entomologist);
//        else if(chatroom_id==5350)
//            room_name=context.getResources().getString(R.string.farmer);
//        else if(chatroom_id==-1)
//            room_name=context.getResources().getString(R.string.surveyor);

        Log.d(TAG,"room_name: "+room_name);
        return room_name;
    }

    public static int getActiveRoom_id(Context context){
        int active_default=Integer.parseInt(getChatroom_id(context,context.getResources().getString(R.string.community_title)));

        String active=(new User(context)).getChatroomActive_id();
        if(active!=null&&!active.trim().isEmpty())
            active_default=Integer.parseInt(active);

        return active_default;
    }

    void processExternalAppShare(){
        Intent intent = getIntent();
        if(intent!=null) {
            String action = intent.getAction();
            String type = intent.getType();
            Log.d(TAG,"type: "+type);
            if (Intent.ACTION_SEND.equals(action) && type != null) {
                if ("text/plain".equals(type)) {
                    handleSendText(intent); // Handle text being sent
                } else if (type.startsWith("image/")) {
                    handleSendImage(intent); // Handle single image being sent
                }
            }
        }
    }


    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            // Update UI to reflect text being shared
            finish();
            BaseActivity.sharetoChat(Chat.this,null,sharedText,true);
            return;
        }
    }

    void handleSendImage(Intent intent) {
          Uri imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        Log.d(TAG,"imageUri: "+imageUri);
        if (imageUri != null) {
            Image image=new Image();
            image.setUri(imageUri);
            image.setImage_url(getPath(imageUri));
            Log.d(TAG,"url: "+getPath(imageUri));
            finish();
            BaseActivity.sharetoChat(Chat.this,image,"",true);
            return;
        }
    }


    private String getPath(Uri uri){
        InputStream input = null;
        OutputStream output = null;
        File file = null;
        try {
            final String fileName=CameraScan.validate((new CreateForm(this)).getCurrentTime());

                 file= new File(getExternalFilesDir("photos"), fileName+".jpg");

                output = new FileOutputStream(file);

                byte[] buffer = new byte[4 * 1024]; // or other buffer size
                int read;
                input =
                        getContentResolver().openInputStream(uri);
                while ((read = input.read(buffer)) != -1) {
                    output.write(buffer, 0, read);
                }

                output.flush();
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if(input!=null&&output!=null) {
                    try {
                        output.close();
                        input.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            if(file!=null)
                return file.getPath();
            else
                return null;
        }

    private void close(){
        if(getIntent()!=null) {
            int source = getIntent().getIntExtra("source", 0);
            if (source == PushReceiver.SOURCE_PUSHY) {
                startActivity(new Intent(Chat.this, Menu.class));
            }
        }

        finish();
    }

    private void cancelChatNotification(){
        NotificationManager nm = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        nm.cancel(10);
    }

    public static String formatString(String input){
       // String input = "wow http://test.com such linked";
        if(input!=null&&!input.trim().isEmpty()) {
            LinkExtractor linkExtractor = LinkExtractor.builder()
                    .linkTypes(EnumSet.of(LinkType.URL,LinkType.WWW,LinkType.EMAIL)) // limit to URLs
                    .build();
            Iterable<Span> spans = linkExtractor.extractSpans(input);

            StringBuilder sb = new StringBuilder();
            for (Span span : spans) {
                String text = input.substring(span.getBeginIndex(), span.getEndIndex());
                if (span instanceof LinkSpan) {
                    // span is a URL
                    sb.append("<a href=\"");
                    sb.append(Encode.forHtmlAttribute(text));
                    sb.append("\">");
                    sb.append(Encode.forHtml(text));
                    sb.append("</a>");
                } else {
                    // span is plain text before/after link
                    sb.append(Encode.forHtml(text));
                }
            }

            return sb.toString();
        }
        else
            return input;
    }

}
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                int currentFirstVisible = linearLayoutManager.findFirstVisibleItemPosition();
////
////                if(currentFirstVisible > firstVisibleInListview)
////                    Log.i("RecyclerView scrolled: ", "scroll up!");
////                else
////                    Log.i("RecyclerView scrolled: ", "scroll down!");
//
//                firstVisibleInListview = currentFirstVisible;
//                if(firstVisibleInListview<5){
//                    loadMessages();
//                }
//                Log.d(TAG,"firstVisible: "+firstVisibleInListview);
//            }
//        });

// messagesList=findViewById(R.id.messagesList);
// messagesList.setAdapter(adapter);

//        adapter.setOnMessageClickListener(new MessagesListAdapter.OnMessageClickListener<Message>() {
//            @Override
//            public void onMessageClick(Message date) {
//                Log.d(TAG,"id: "+date.getId());
//            }
//        });

//
//        adapter.setLoadMoreListener(this);
//        adapter.enableSelectionMode(this);