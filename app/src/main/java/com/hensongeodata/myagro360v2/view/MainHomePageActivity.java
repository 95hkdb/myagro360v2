package com.hensongeodata.myagro360v2.view;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.helper.Constants;
import com.hensongeodata.myagro360v2.helper.Tools;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Broadcast.MyBroadcastReceiver;
import com.hensongeodata.myagro360v2.version2_0.Events.AEAItemClickedEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.CommentPageBackEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.DBSyncingComplete;
import com.hensongeodata.myagro360v2.version2_0.Events.DashboardItemClickedEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.KBDownloadCompleted;
import com.hensongeodata.myagro360v2.version2_0.Events.MapSortButtonClicked;
import com.hensongeodata.myagro360v2.version2_0.Events.MapSortClosed;
import com.hensongeodata.myagro360v2.version2_0.Events.NewAlertEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.PushedToServerEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.ScanCompletedEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.ScanDeletedEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.ScoutButtonClickedEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.ThreadCreatedEvent;
import com.hensongeodata.myagro360v2.version2_0.Fragment.AEAFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.AlertFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.AppNotificationFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.AttachMapDialogFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.ChooseActivityDialogFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.ChooseMapDialogFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.ChoosePlotDialogFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.DashboardFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.EditPlotDialogFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.FarmActivitiesSortFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.FarmActivityFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.ForumFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.FullMapDialogFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.LearnKnowledgeBaseFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.MainHomeFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.Map.MapSortFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.Map.MappingFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.Plots.AddPlotDialogFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.Plots.PlotActivitiesFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.Plots.PlotsFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.Plots.PlotsSortFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.PlotsDialogFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.ScanFragment;
import com.hensongeodata.myagro360v2.version2_0.Fragment.ScoutingFragment;
import com.hensongeodata.myagro360v2.version2_0.LoadUserActivity;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmActivity;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;
import com.hensongeodata.myagro360v2.version2_0.Model.Member;
import com.hensongeodata.myagro360v2.version2_0.Model.ScanModel;
import com.hensongeodata.myagro360v2.version2_0.PlotDetailDialogFragment;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;
import com.hensongeodata.myagro360v2.version2_0.sync.DBSyncTask;
import com.hensongeodata.myagro360v2.version2_0.sync.crops.CropSyncUtil;
import com.hensongeodata.myagro360v2.version2_0.sync.farm.FarmSyncUtil;
import com.hensongeodata.myagro360v2.version2_0.sync.farmActivity.FarmActivitySyncTask;
import com.hensongeodata.myagro360v2.version2_0.sync.map.MapSyncUtil;
import com.hensongeodata.myagro360v2.version2_0.sync.plots.PlotSyncUtil;
import com.rollbar.android.Rollbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


public class MainHomePageActivity extends BaseActivity implements
        FarmActivityFragment.FarmHomeFragmentListener,
        ChooseActivityDialogFragment.OnChooseActivityDialogFragmentListener,
        AlertFragment.OnFragmentInteractionListener,
        MainHomeFragment.MenuItemClickListener,
        NavigationView.OnNavigationItemSelectedListener ,
        MappingFragment.OnAddMapButtonListener,
        PlotsFragment.OnPlotsFragmentListener,
        ChooseMapDialogFragment.OnChooseMapFragmentListener,
        AddPlotDialogFragment.OnAddDialogFragmentListener,
        PlotsSortFragment.OnPlotSortFragmentListener,
        FarmActivitiesSortFragment.OnFarmActivitiesSortFragmentListener,
        ChoosePlotDialogFragment.OnAddFarmActivityDialogFragmentListener,
        PlotActivitiesFragment.OnPlotActivitiesFragmentListener ,
        EditPlotDialogFragment.OnEditDialogFragmentListener,
        ChoosePlotDialogFragment.OnChoosePlotDialogFragmentListener,
        ScoutingFragment.ScoutingFragmentListener, ScanFragment.OnScanFragmentListener{

    private static final String TAG = MainHomePageActivity.class.getSimpleName();
    private Context mContext;
    private String userId;
    private Fragment fragment;
    private Toolbar toolbar;
    private BottomNavigationView navView;
    private DrawerLayout drawer;
    private int selectedItemId;
    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private View bottom_sheet;
    private MyApplication myApplication;
    private RadioButton typeRadioButton;
    private String mapType;
    private String cropType;
    Button yesButton;
    private DatabaseHelper databaseHelper;
    private MapDatabase mapDatabase;
    private MainViewModel mMainViewModel;
    private String mappingId;
    private double mapSize;
    private int mapCount;
    private int plotsCount;
    private TextView appBarTitle;
    private View parent_view;
    private long mPlotId;
    private ChooseMapDialogFragment chooseMapDialogFragment;
    private ChoosePlotDialogFragment choosePlotDialogFragment;
    private ChooseActivityDialogFragment chooseActivityDialogFragment;
    private NavigationView nav_view;
    private AppCompatSpinner plots_spinner;
    private HashMap<Integer, String> mapSpinnerHashMap;
    private Handler handler;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    loadHomeFragment();
                    return true;

                case R.id.navigation_plots:
                    loadPlotsFragment();
                    mMainViewModel = null;
                    return true;

                case R.id.navigation_farm_activity:
                    loadFarmAcctivitiesFragment();
//                    checkSelected();
                    changeAppBarTitle("Activities");
                    return true;
//
                case R.id.navigation_alert:
                    fragment = new AppNotificationFragment();
                    loadFragment(fragment);
//                    checkSelected();
                    changeAppBarTitle("Alerts");
                    return true;

                case R.id.navigation_learn:
                    fragment = new LearnKnowledgeBaseFragment();
                    loadFragment(fragment);
//                    checkSelected();
                    changeAppBarTitle("Learn");
//                    uncheckAllSideBarMenuItems();
                    return true;

            }
            return false;
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_home_page);

        Rollbar.init(this);

        handler = new Handler();
        mapDatabase = MapDatabase.getInstance(this);
        mMainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        myApplication = new MyApplication();
        mContext = this;

        if (SharePrefManager.isSecondaryUserAvailable(this)){
            userId = SharePrefManager.getSecondaryUserPref(this);
        }else {
            userId = SharePrefManager.getInstance(this).getUserDetails().get(0);
        }

        choosePlotDialogFragment = new ChoosePlotDialogFragment();

        parent_view = findViewById(android.R.id.content);
        appBarTitle = findViewById(R.id.app_bar_title);
        navView = findViewById(R.id.nav_view);
        drawer = findViewById(R.id.drawer_layout);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        loadHomeFragment();

        getMapMainViewModel();
        initToolbar();
        initComponent();
        initNavigationMenu();
        fetchMap();
        getPlotsMainViewModel();

        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "onCreate:  " + mapDatabase.daoAccess().countMaps());;
            }
        });

        EventBus.getDefault().post(new NewAlertEvent());

        bottom_sheet = findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);

        if (getIntent() != null ){
            Intent intent = getIntent();
            String destination = intent.getStringExtra("destination");

            if (intent.getStringExtra("origination" ) != null){
                String origination = intent.getStringExtra("origination");
                String mappingId = intent.getStringExtra("map_id");

                if (origination.equals(Constants.MAPPING_FRAGMENT)){
                    loadPlotsFragment();
                    showAddNewPlotDialog(mappingId, 0);
                }
            }

            if ( destination != null){
                switch(destination){
                    case "map":
                        loadFragment(new MappingFragment());
                        unCheckSelected();
                }
            }
        }

        if(myApplication.hasNetworkConnection(this)){
            DBSyncTask.SyncAllMembers(this);
        }

        MapSyncUtil.initialize(this);
        FarmSyncUtil.initialize(this);
        PlotSyncUtil.initialize(this);
        CropSyncUtil.initialize(this);
//        demoAlarm(this);

        Log.i(TAG, "Random map" + mapCount);

    }

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.setTitle("HOME");
        toolbar.setTitleTextColor(    getResources().getColor(R.color.black));
        toolbar.setPadding(15,0,0,0);
    }

    private void initComponent() {
        Tools.setSystemBarLight(this);
    }

    private void loadHomeFragment() {
        fragment = new MainHomeFragment();
        loadFragment(fragment);
        checkSelected();
        changeAppBarTitle(getResources().getString(R.string.app_name));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.grey_60));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
//            Toast.makeText(mContext
//           , item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
        transaction.commit();
    }


    private void initNavigationMenu(){
        nav_view = findViewById(R.id.drawer_nav_view);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawer.setDrawerListener(toggle);
        toggle.syncState();

        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                unCheckAllItems();

                switch(menuItem.getItemId()){

                    case R.id.nav_aea:
                        Fragment  aea_fragment = new AEAFragment();
                        loadFragment( aea_fragment);
                        changeAppBarTitle("Agent Portal");
                        break;

                    case R.id.nav_dashboard:
                        loadDashboard();
                        changeAppBarTitle("Dashboard");
                        break;

                    case R.id.nav_mapping:
                        loadMapFragment();
                        changeAppBarTitle("Mapping");
                        break;

                    case R.id.nav_scout:
                        Fragment  scout_fragment = ScoutingFragment.newInstance(1);
                        loadFragment( scout_fragment);
                        changeAppBarTitle("Scouts");
                        break;

                    case R.id.nav_scan:
                        Fragment  scanFragment = ScanFragment.newInstance(1);
                        loadFragment( scanFragment);
                        changeAppBarTitle("Scans");
                        break;

                      case R.id.nav_forum:
                            Fragment  forumFragment = new ForumFragment();
                            loadFragment( forumFragment);
                            changeAppBarTitle("Forum");
                            break;

                    case R.id.nav_forms:
                        startActivity( new Intent(mContext, AddForm.class));
                        break;

                    case R.id.nav_settings:
                        startActivity( new Intent(mContext, Settings.class));
                        break;

//                    case R.id.nav_shop:
//                        startActivity( new Intent(getApplicationContext(), ShopActivity.class));
//                        break;

                    case R.id.nav_logout:
                        logout();
                        break;

                    case R.id.nav_about: startActivity( new Intent(mContext, About.class));

                }

                drawer.closeDrawers();
                return true;
            }
        });
    }

    private void loadMapFragment() {
        fragment = new MappingFragment();
        loadFragment(fragment);
        unCheckSelected();
        changeAppBarTitle("Mapping");
    }

    private void loadDashboard() {
        fragment = new DashboardFragment();
        loadFragment(fragment);
//        unCheckSelected();
        changeAppBarTitle("Dashboard");
    }

    private void unCheckAllItems(){
        for (int i =0; i < navView.getMenu().size(); i++){
            navView.getMenu().getItem(i).setCheckable(false);
        }
    }

    private void unCheckSelected(){
        navView.getMenu().findItem(navView.getSelectedItemId()).setCheckable(false);
    }

    private void uncheckAllSideBarMenuItems(){

//        int size = nav_view.getMenu().size();
//        for (int i = 0; i < size; i++) {
//            nav_view.getMenu().getItem(i).setCheckable(false);
//        }
    }

    private void checkSelected(){
        navView.getMenu().findItem(navView.getSelectedItemId()).setCheckable(true);
    }


    private void changeAppBarTitle(String title) {
        appBarTitle.setText(title);
    }

    private void logout(){
        if(myApplication.logout(mContext)){
            Intent intent=new Intent(mContext,SigninSignup.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    private void showMapFragmentBottomSheet(final String value, final String sectionId) {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.map_fragment_sheet_basic, null);

        ((TextView) view.findViewById(R.id.name)).setText(value);

        view.findViewById(R.id.view_full_map).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
                showFullMapDialog(sectionId);
            }
        });
//
        (view.findViewById(R.id.add_to_form)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                openForms(sectionId);
                mBottomSheetDialog.dismiss();
            }
        });

//        (view.findViewById(R.id.attach_map_to_plot)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showAttachMapDialogFragment(sectionId);
//                mBottomSheetDialog.dismiss();
//            }
//        });

        (view.findViewById(R.id.remove_map)).setOnClickListener(view1 -> removeMapping(sectionId,value));


        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);
    }

    private void showFullMapDialog(String sectionId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FullMapDialogFragment newFragment = FullMapDialogFragment.newInstance(sectionId);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
    }


    private void showChooseMapDialog() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        chooseMapDialogFragment = new ChooseMapDialogFragment();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, chooseMapDialogFragment).addToBackStack(null).commit();
    }

    private void showNoMappedDataDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("No Mapped Data!");
        builder.setMessage("You currently do not have any mapped data!");
        builder.setPositiveButton("MAP NOW!", (dialogInterface, i) -> showAddMappingFragment(Constants.PLOTS_FRAGMENT));
        builder.setNegativeButton("CLOSE", (dialog, which) -> { });
        builder.show();
    }

    private void openForms(String section_id){

        List<InstructionScout> getAllItems=databaseHelper.getScoutListAllStates(section_id, ChatbotAdapterRV.MODE_MAP);
        ArrayList<String> strPois =new ArrayList<>();

        int index2 =0;
        for (InstructionScout instructionScout : getAllItems) {
            if(instructionScout!=null) {
                String message = instructionScout.getMessage();
                if (message != null && !message.trim().isEmpty())
                    strPois.add(message);
            }
            index2++;
        }

        ArrayList<POI> pois= CreateForm.getInstance(getApplicationContext()).buildPOI_Mapping(strPois);

        Log.d(TAG,"openForms scout_id: "+section_id);
        MainActivityAutomated.mapModel=databaseHelper.getMapping(section_id);
        Log.d(TAG,"openforms map.id: "+MainActivityAutomated.mapModel.id+" map.type: "+MainActivityAutomated.mapModel.type);
        MainActivityAutomated.pois=pois;
        Intent intent=new Intent(getApplicationContext(),MainActivityAutomated.class);
        startActivity(intent);
    }

    private void showAddMappingFragment(String origination) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.fragment_add_mapping_dialog);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        typeRadioButton = dialog.findViewById(R.id.radio_polygon);
        typeRadioButton.performClick();
        Log.d(TAG, "onClick:  Map Type " + mapType);

        dialog.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.findViewById(R.id.continue_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText editTextName = dialog.findViewById(R.id.map_name);
                EditText editTextDescription = dialog.findViewById(R.id.map_description);

                final String name = editTextName.getText().toString();
                final String description = editTextDescription.getText().toString();
                final AtomicReference<MapModel> mapName = new AtomicReference<>();


                AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mapName.set(mapDatabase.daoAccess().mapNameExists(name));
                    }
                });

                if (name.isEmpty()){
                    editTextName.setError("Field required!");
                }

                if (description.isEmpty()){
                    editTextDescription.setError("Field required!");
                }

                if (!name.isEmpty() && !description.isEmpty()){
                    dialog.findViewById(R.id.loading_indicator).setVisibility(View.VISIBLE);

                    new Handler().postDelayed(() -> {
//                            ViewAnimation.fadeOut(lyt_progress);

                        if (mapName.get() == null){

                            Toast.makeText(mContext, "starting mapping wizard now..", Toast.LENGTH_SHORT).show();
                            dialog.findViewById(R.id.continue_button).setVisibility(View.GONE);

                            dialog.dismiss();
                            Intent mainIntent = new Intent(getApplicationContext(), ScoutAutomated.class);
                            mainIntent.putExtra("mode", 0);
                            mainIntent.putExtra("name", name);
                            mainIntent.putExtra("description" ,description);
                            mainIntent.putExtra("map_type", mapType);
                            mainIntent.putExtra("origination", origination);
                            startActivity(mainIntent);

                        }else {
                            editTextName.setError(" Map name already exists!");
                            dialog.findViewById(R.id.loading_indicator).setVisibility(View.GONE);

                        }

                    }, 3500);

                }
            }
        });

        hideKeyBoard();

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void hideKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        }
    }

    private void removeMapping(final String sectionId ,final  String name){
        new AlertDialog.Builder(mContext)
                .setTitle(name)
                .setMessage("Do you want to remove this map?")
                .setNegativeButton("NO", (dialogInterface, i) -> dialogInterface.dismiss())
                .setPositiveButton("YES", (dialogInterface, i) -> {
                    AppExecutors.getInstance().getDiskIO().execute(() -> mapDatabase.daoAccess().deleteMapById((sectionId)));

                    new Handler().postDelayed(() -> loadMapFragment(), 2000);

                    MyApplication.showToast(mContext, mContext.getResources().getString(R.string.success), Gravity.CENTER);
                    mBottomSheetDialog.dismiss();
                })
                .show();
    }

    private void showFarmActivitiesSortDialog(String mapId, double size) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FarmActivitiesSortFragment newFragment = FarmActivitiesSortFragment.newInstance(mapId, size);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
    }

    private void fetchMap(){
        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
            @Override
            public void run() {
//                mapCount = mapDatabase.daoAccess().mapCount(userId);
            }
        });
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.radio_polygon:
            case R.id.radio_line:
                if (checked)
                    mapType = ((RadioButton)view).getTag().toString();
                break;
        }
    }

    public void onCropRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.radio_crop:
            case R.id.radio_livestock:
                if (checked)
                    cropType = view.getTag().toString();
                break;
        }
    }

    private void showAddNewActivityBottomSheetDialog() {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.sheet_basic, null);
        ((TextView) view.findViewById(R.id.message)).setText("Do you want to add an activity to an existing plot? ");
        (view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (plotsCount != 0){
                    showChoosePlotDialogFragment();
                }else {
                    showNoPlotsDialog();
                }
                mBottomSheetDialog.dismiss();
            }
        });

        (view.findViewById(R.id.bt_details)).setOnClickListener(v -> {
            loadPlotsFragment();
            mBottomSheetDialog.dismiss();
        });

        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }


    private void showFarmActivitiesFragmentBottomSheet(String activityId, String name , long id) {

        AtomicReference<FarmActivity> farmActivity = new AtomicReference<>(new FarmActivity());

        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
            @Override
            public void run() {
                farmActivity.set(mapDatabase.farmActivityDaoAccess().fetchActivityByFarmActivityId(id));
            }
        });


        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.farm_activities_fragment_sheet_basic, null);

        TextView nameTextView = view.findViewById(R.id.name);
        nameTextView.setText(name);
        nameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        TextView completeActivityTextView = view.findViewById(R.id.complete_activity);
//        (view.findViewById(R.id.complete_activity)).setOnClickListener(view1 -> {

        if (farmActivity.get().isCompleted()){
            completeActivityTextView.setTextColor(mContext.getResources().getColor(R.color.disabledBlack));
            completeActivityTextView.setText("Complete this activity (completed)");
            completeActivityTextView.setEnabled(false);
            completeActivityTextView.setClickable(false);
//                    completeActivityTextView.setTooltipText("complete " + name + " activity");
        }else {
            completeActivityTextView.setTextColor(mContext.getResources().getColor(R.color.grey_90));
            completeActivityTextView.setText("Complete this activity");
            completeActivityTextView.setEnabled(true);
            completeActivityTextView.setClickable(true);
        }

        completeActivityTextView.setOnClickListener(view1 -> {

            AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                @Override
                public void run() {
                    mapDatabase.farmActivityDaoAccess().markActivityAsCompleted(id);
                    snackBarWithAction(id);
                }
            });

            mBottomSheetDialog.dismiss();
        });


        TextView pushToServerTextView = view.findViewById(R.id.push_to_server);

        if (farmActivity.get().isSynced()){
            pushToServerTextView.setTextColor(mContext.getResources().getColor(R.color.disabledBlack));
            pushToServerTextView.setText("Pushed to server");
            pushToServerTextView.setEnabled(false);
            pushToServerTextView.setClickable(false);
        }else {
            pushToServerTextView.setTextColor(mContext.getResources().getColor(R.color.grey_90));
            pushToServerTextView.setText("Push to server");
            pushToServerTextView.setEnabled(true);
            pushToServerTextView.setClickable(true);
        }

        pushToServerTextView.setOnClickListener(v -> {
            FarmActivitySyncTask.syncFarmActivity(handler, getApplicationContext(), id );
            mBottomSheetDialog.dismiss();
        });

        (view.findViewById(R.id.remove_activity)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppExecutors.getInstance().getDiskIO().execute(() -> {
                    mapDatabase.farmActivityDaoAccess().deleteFarmActivity(id);
                });

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadFarmAcctivitiesFragment();
                    }
                }, 1500);

                mBottomSheetDialog.dismiss();
            }
        });


        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void getMapMainViewModel(){

        try{
            mMainViewModel.getMaps().observe(this, mapModelList -> {
                mapCount = mapModelList.size();
            });

        }catch (Exception e){
            Log.e(TAG, "getMapMainViewModel:  " + e.getMessage() );
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {}

    @Override
    public void onClicked(int position) {

        switch (position){
            case 0:
                loadFragment(new FarmActivityFragment());
                selectedItemId = navView.getSelectedItemId();
//                unCheckSelected();
                break;

            case 1:
                startActivity(new Intent(this, ScanActivity.class));
                break;

            case 2:
                loadMapFragment();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        navView.setSelectedItemId(selectedItemId);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) { return true; }

    @Override
    public void onAddMapButtonClicked() {
        showAddMappingFragment("");
    }

    @Override
    public void onMoreIconClicked(String value, String sectionId) {
        showMapFragmentBottomSheet(value, sectionId);
    }

    @Override
    public void shareButtonClicked(String sectionId) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, Keys.AGRO_BASE+"faw-api/get-map/?mapping_id="+sectionId);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.d(TAG, "onResume:  MainActivity on Resume" );

//        fetchMap();
    }

    private void showChooseActivityDialog(long plotId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        chooseActivityDialogFragment = ChooseActivityDialogFragment.newInstance(plotId);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, chooseActivityDialogFragment).addToBackStack(null).commit();
    }

    private void removeChooseActivityDialog(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.remove( chooseActivityDialogFragment).addToBackStack(null).commit();
    }

    @Override
    public void onAddFarmActivityDialogContinueButton(long plotId) {
        showChooseActivityDialog((plotId));
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void showMapSortDialog() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        MapSortFragment newFragment = new MapSortFragment();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMapSortClicked(MapSortButtonClicked event) {
//        Toast.makeText(mContext, "map sort clicked!", Toast.LENGTH_SHORT).show();
        showMapSortDialog();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMapSortClosed(MapSortClosed event) {
        loadMapFragment();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(DBSyncingComplete event) {
        Toast.makeText(mContext, "Sync Successfully completed", Toast.LENGTH_SHORT).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onScanDeleted(ScanDeletedEvent event) {
        loadScanFragment();
//        Toast.makeText(mContext, "Event from Scan Fragment", Toast.LENGTH_SHORT).show();
    }

    @Subscribe( threadMode = ThreadMode.MAIN)
    public void onScanCompleted(ScanCompletedEvent event) {

        String channelName = "pestScanResults";
        String channelId = "pestScanResultId";
        int notificationId = 20003;

        Toast.makeText(mContext, "Scan Successfully completed!", Toast.LENGTH_SHORT).show();

        NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Scan Results in!"));
        HelperClass.showNotification(notificationId, channelId, channelName, getApplicationContext(), "New Pest Scan Results in !", notificationStyle, null);

        AtomicReference<ScanModel> scanModel = new AtomicReference<>();

//        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
//            @Override
//            public void run() {
//                scanModel.set(mapDatabase.scanDaoAccess().fetchScanById(event.getId()));
//            }
//        });

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                HelperClass.showDialogPolygon(getApplicationContext(), scanModel.get());
//            }
//        },1000);

//        HelperClass.showDialogPolygon(getApplicationContext());

        final Dialog dialog = new Dialog(getApplicationContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_header_polygon);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);

//        ((Button) dialog.findViewById(R.id.bt_join)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getApplicationContext(), "Button Join Clicked", Toast.LENGTH_SHORT).show();
//            }
//        });

//        ((Button) dialog.findViewById(R.id.bt_decline)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getApplicationContext(), "Button Decline Clicked", Toast.LENGTH_SHORT).show();
//            }
//        });

//        dialog.show();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onKBFragmentDownloadCompleted(KBDownloadCompleted event) {
        loadFragment(new LearnKnowledgeBaseFragment());
        checkSelected();
    }

        @Override
    public void onAddNewFarmActivityButton() {
        showAddNewActivityBottomSheetDialog();
    }

    @Override
    public void onActivityPlotClicked(long plotId) {

        AppExecutors.getInstance().getDiskIO().execute(() -> {
            FarmPlot farmPlot = mapDatabase.plotDaoAccess().fetchPlotByPlotId(plotId);

            PlotDetailDialogFragment fragment = new PlotDetailDialogFragment();
            fragment.setData(farmPlot);

            fragment.show(getSupportFragmentManager(), fragment.getTag());
        });
    }

    @Override
    public void onActivityMoreClicked(String activityId, String name, long id) {
        showFarmActivitiesFragmentBottomSheet(activityId, name, id);
    }

    private void snackBarWithAction(long id) {
        Snackbar snackbar = Snackbar.make(parent_view, "Successfully completed an activity!", Snackbar.LENGTH_LONG)
                .setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                mapDatabase.farmActivityDaoAccess().markActivityAsPending(id);
                            }
                        });
                        Snackbar.make(parent_view, "Activity marked as pending!", Snackbar.LENGTH_SHORT).show();
                    }
                });
        snackbar.show();
    }

    @Override
    public void onDismiss() {
//            choosePlotDialogFragment.dismiss();
//        Log.i(TAG, "onDismiss:   Dismiss from MainHomePageActivity");
    }

    @Override
    public void onChooseActivityDialogFragmentSave() {

        chooseActivityDialogFragment.dismiss();
        Log.i(TAG, "onChooseActivityDialogFragmentSave:" + chooseActivityDialogFragment );
        removeChooseActivityDialog();
    }

    @Override
    public void onPlotSortSave() {
        loadPlotsFragment();
    }

    @Override
    public void onPlotsSortDismiss() {
        loadPlotsFragment();
    }

    @Override
    public void onFarmActivitiesSortSave() {
        loadFarmAcctivitiesFragment();
    }

    private void loadFarmAcctivitiesFragment() {
        fragment = new FarmActivityFragment();
        loadFragment(fragment);
    }

    @Override
    public void onFarmActivitiesSortDismiss() {
        loadFarmAcctivitiesFragment();
    }

    @Override
    public void onFarmActivitiesFragmentSortButtonClicked() {
        showFarmActivitiesSortDialog("", 0);
    }

    private void demoAlarm(Context context){

        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
//        Intent intent = new Intent(context, AlarmReceiver.class);
        Intent intent = new Intent(context, MyBroadcastReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + 1000*60,
                1000*60, alarmIntent);
    }

    @Override
    public void onChoosePlotDialogFragmentDismiss() {
        removeChoosePlotDialogFragment();
    }

    @Override
    public void onChoosePlotDialogFragmentListenerContinue(long plotId) {
        removeChoosePlotDialogFragment();
        showChooseActivityDialog((plotId));
    }

    private void removeScoutDialog(long id){
        new AlertDialog.Builder(mContext)
                .setTitle("REMOVE SCOUT!")
                .setMessage("Removing this scout will also remove associated scans. Are you sure?")
                .setNegativeButton("NO", (dialogInterface, i) -> dialogInterface.dismiss())

                .setPositiveButton("YES", (dialogInterface, i) -> {

                    AppExecutors.getInstance().getDiskIO().execute(() -> {

                        try {
                            mapDatabase.scoutDaoAccess().deleteByScoutId(id);
                            mapDatabase.scanDaoAccess().deleteByScoutId(id);
                        }catch (Exception e){
                            Log.e(TAG, "removeScoutDialog:  " + e.getMessage() );
                        }
                    });
                    MyApplication.showToast(mContext, mContext.getResources().getString(R.string.success), Gravity.CENTER);
                    mBottomSheetDialog.dismiss();
                })
                .show();
    }


    private void showViewScansDialog(long plotId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        chooseActivityDialogFragment = ChooseActivityDialogFragment.newInstance(plotId);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, chooseActivityDialogFragment).addToBackStack(null).commit();
    }

    private void showScoutingFragmentBottomSheet(final long id, final long plotId) {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.scouting_fragment_sheet_basic, null);

        TextView nameTextView = view.findViewById(R.id.name);

        AtomicReference<FarmPlot> farmPlot = new AtomicReference<>();

        AppExecutors.getInstance().getDiskIO().execute(
                () -> farmPlot.set(mapDatabase.plotDaoAccess().fetchPlotByPlotId(plotId)));

        new Handler().postDelayed(() -> {

            Log.i(TAG, "onClick:  Scout BottomSheet " + farmPlot.get());

            if (farmPlot.get() != null){
                nameTextView.setText(farmPlot.get().getName());
            }
        },500);

        (view.findViewById(R.id.remove_scout)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();

                removeScoutDialog(id);
            }
        });

//        (view.findViewById(R.id.view_scout_scans)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                loadFragment(ScanFragment.newInstance());
//                mBottomSheetDialog.dismiss();
//
//            }
//        });

        (view.findViewById(R.id.export_as_pdf)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();

                AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        List<ScanModel> scanModels = mapDatabase.scanDaoAccess().scansByScoutId(id);
                        StringBuilder scanReportsString = new StringBuilder();

                        if (scanModels != null){
                            for (ScanModel scanModel : scanModels){
                                Log.i(TAG, "run:  " + scanModels.size());
                                scanReportsString.append(scanModel.getScanReportText());
                                scanReportsString.append("\n");
                                scanReportsString.append("\n");
                                scanReportsString.append(System.getProperty("line.separator"));
                            }

                            HelperClass.createPdf(getApplicationContext(), scanReportsString.toString());

                        }
                    }
                });

            }
        });

        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void showScanFragmentBottomSheet(final long id) {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.scan_fragment_sheet_basic, null);

        (view.findViewById(R.id.push_to_server)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "push to server feature!", Toast.LENGTH_SHORT).show();
            }
        });


        (view.findViewById(R.id.remove_scan)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AtomicReference<ScanModel> scanModel = new AtomicReference<>();

                AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        scanModel.set(mapDatabase.scanDaoAccess().fetchScanById(id));
                        mapDatabase.scanDaoAccess().deleteById(id);
                    }
                });

                mBottomSheetDialog.dismiss();

                new Handler().postDelayed(() -> {
                    if (scanModel.get() != null){
                        EventBus.getDefault().post(new ScanDeletedEvent());
                        Snackbar.make(parent_view, "Successfully removed scan", Snackbar.LENGTH_SHORT).show();
                    }
                },700);

            }
        });

        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }



    @Override
    public void scoutingFragmentMoreClicked(long id, long plotId) {
        showScoutingFragmentBottomSheet(id, plotId);
    }

    @Override
    public void scoutingFragmentAddButtonClicked() {
        loadPlotsFragment();
        unCheckSelected();
//        checkSelected();
    }

    @Override
    public void onScanFragmentMoreIconClicked(long id) {
//        EventBus.getDefault().post(new DBSyncingComplete());
        showScanFragmentBottomSheet(id);
    }

    private void showAEAFragmentBottomSheet(long memberIdd, String name, String id) {

        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.aea_fragment_bottom_sheet, null);

        TextView nameTextView = view.findViewById(R.id.name);
        nameTextView.setText(String.format("%s account ", name));

        TextView addPlotTextView = view.findViewById(R.id.add_new_plot);

        addPlotTextView.setOnClickListener(v -> {

            if (SharePrefManager.isSecondaryUserSelectionAvailable(getApplicationContext())){
                SharePrefManager.clearSecondaryUserPref(getApplicationContext());

                AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        Member member = mapDatabase.memberDaoAccess().fetchMemberById(memberIdd);
                        SharePrefManager.setSecondaryUserPref(getApplicationContext(), member.getEmail());

//                        mMainViewModel.setUserId(member.getEmail());

                    }
                });

            }

            SharePrefManager.setCurrentSecondaryUserSelection(getApplicationContext(), memberIdd);
            SharePrefManager.setSecondaryUserPref(getApplicationContext(), (id));

//            loadFragment(new LoadUserPropertiesFragment());

            startActivity(new Intent(getApplicationContext(), LoadUserActivity.class));
            mBottomSheetDialog.dismiss();
        });


        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void showPlotsDialogFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        PlotsDialogFragment plotsDialogFragment = new PlotsDialogFragment();
        transaction.add(android.R.id.content, plotsDialogFragment).addToBackStack(null).commit();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onItemClicked(AEAItemClickedEvent event) {
        showAEAFragmentBottomSheet(event.getMemberId(),event.getName(), event.getId());
    }

    // PLOTS RELATED METHODS

    private void loadPlotsFragment() {
        fragment = new PlotsFragment();
        loadFragment(fragment);
        checkSelected();
        changeAppBarTitle("Plots");
    }

    private void loadScanFragment() {
        loadFragment(new ScanFragment());
        checkSelected();
        changeAppBarTitle("Scans");
    }

    private void showChoosePlotDialogFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, choosePlotDialogFragment).addToBackStack(null).commit();
    }

    private void removeChoosePlotDialogFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.remove( choosePlotDialogFragment).addToBackStack(null).commit();
    }

    private void showAttachMapDialogFragment(String sectionId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        AttachMapDialogFragment newFragment =  AttachMapDialogFragment.newInstance(sectionId);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
    }

    private void showAddNewPlotBottomSheetDialog() {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.sheet_basic, null);
        ((TextView) view.findViewById(R.id.message)).setText("Do you want to add an activity to an existing plot? ");

        (view.findViewById(R.id.bt_close)).setOnClickListener(view1 -> {
            showChoosePlotDialogFragment();
            mBottomSheetDialog.dismiss();
        });

        (view.findViewById(R.id.bt_details)).setOnClickListener(v -> {
            loadPlotsFragment();
            mBottomSheetDialog.dismiss();
        });

        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);
    }

    private void showPlotActivitiesDialog(long plotId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        PlotActivitiesFragment newFragment = PlotActivitiesFragment.newInstance(plotId);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
    }

    private void showAddNewPlotDialog(String mapId, double size) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        AddPlotDialogFragment newFragment = AddPlotDialogFragment.newInstance(mapId, size);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
    }

    private void showEditPlotDialog(String mapId, double size, long plotId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        EditPlotDialogFragment newFragment = EditPlotDialogFragment.newInstance(mapId, size, plotId);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
    }

    private void showPlotsSortDialog(String mapId, double size) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        PlotsSortFragment newFragment = PlotsSortFragment.newInstance(mapId, size);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
    }

    private void showPlotsFragmentBottomSheet(final long id, final String name, final String plotId, String mapId) {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.plots_fragment_sheet_basic, null);

        ((TextView) view.findViewById(R.id.name)).setText(name);

        view.findViewById(R.id.view_full_map).setOnClickListener(v -> {
            mBottomSheetDialog.dismiss();
            showFullMapDialog(mapId);
        });

        view.findViewById(R.id.view_plot_activities).setOnClickListener(v -> {
            mBottomSheetDialog.dismiss();
            showPlotActivitiesDialog(id);
        });

        view.findViewById(R.id.edit_plot).setOnClickListener(v -> {
            mBottomSheetDialog.dismiss();
            showEditPlotDialog(mapId, mapSize, id);
        });

        (view.findViewById(R.id.perform_activity)).setOnClickListener(view1 -> {
            showChooseActivityDialog(id);
            mBottomSheetDialog.dismiss();
        });

        (view.findViewById(R.id.scout_plot)).setOnClickListener(view1 -> {
            Intent scouting=new Intent(getApplicationContext(), ScoutAutomated.class);
            scouting.putExtra("mode", ChatbotAdapterRV.MODE_SCOUT);
            scouting.putExtra("plot_id", id);
            startActivityForResult(scouting,100);
            mBottomSheetDialog.dismiss();
        });

        (view.findViewById(R.id.push_to_server)).setOnClickListener(v -> {

        });

        (view.findViewById(R.id.remove_plot)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removePlot(name, id);
            }
        });

        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    @Override
    public void onPlotActivitiesDialogDismiss() {

    }

    @Override
    public void onNewPlotActivitiesFragmentButtonClicked(long plotId) {
        showChooseActivityDialog(plotId);
    }

    private void showNoPlotsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("No Plots!");
        builder.setMessage("You currently do not have any plots data!");
        builder.setPositiveButton("ADD PLOT NOW!", (dialogInterface, i) -> {
            loadPlotsFragment();
            unCheckSelected();
        });
        builder.setNegativeButton("CLOSE", (dialog, which) -> { });
        builder.show();
    }

    private void removePlot(String name, long plotId){

        new AlertDialog.Builder(mContext)
                .setTitle(name)
                .setMessage("Do you want to remove this plot?")
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                mapDatabase.plotDaoAccess().deletePlotById(plotId);
                            }
                        });

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                loadPlotsFragment();
                            }
                        }, 2000);

                        MyApplication.showToast(mContext, mContext.getResources().getString(R.string.success), Gravity.CENTER);
                        mBottomSheetDialog.dismiss();

                    }
                }).show();
    }

    private void getPlotsMainViewModel(){
        mMainViewModel.getPlots().observe(this, new Observer<List<FarmPlot>>() {
            @Override
            public void onChanged(List<FarmPlot> farmPlotList) {
                plotsCount = farmPlotList.size();
            }
        });
    }

    @Override
    public void onAddNewPlot() {

        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.sheet_basic, null);
        ((TextView) view.findViewById(R.id.title)).setText("Add A New Plot ");
        ((TextView) view.findViewById(R.id.message)).setText("Have you mapped your plot yet? ");
        yesButton = view.findViewById(R.id.bt_close);
        yesButton.setText("YES ");

        ((Button) view.findViewById(R.id.bt_details)).setText("NO, ADD A MAP FIRST  ");

        (view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getMapMainViewModel();

                Log.i(TAG, "onClick:  Random Map " + mapCount);

                if (mapCount != 0 ){
                    showChooseMapDialog();
                }else{
                    showNoMappedDataDialog();
                }

                mBottomSheetDialog.dismiss();
            }
        });

        (view.findViewById(R.id.bt_details)).setOnClickListener(v -> {
            loadMapFragment();
            mBottomSheetDialog.dismiss();
            SharePrefManager.setDestination(getApplicationContext(), Constants.PLOTS_FRAGMENT);
            showAddMappingFragment(Constants.PLOTS_FRAGMENT);
        });

        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);

    }

    @Override
    public void onPlotsFragmentMoreClicked(long id, String name, String plotId, String mapId) {
        showPlotsFragmentBottomSheet(id, name, plotId, mapId);
    }

    @Override
    public void onPlotsFragmentSortButtonClicked() {
        Toast.makeText(getApplicationContext(), "Clicked.. ", Toast.LENGTH_SHORT).show();
        showPlotsSortDialog("",0);
    }

    @Override
    public void onContinueButtonClicked(String mapId, double size) {
        mappingId = mapId;
        mapSize = size;
        chooseMapDialogFragment.dismiss();
        showAddNewPlotDialog(mapId, size);
    }

    @Override
    public void onSave() {

    }

    private void showAddScout() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.fragment_add_scouting_dialog);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        plots_spinner = dialog.findViewById(R.id.spn_plots);

        fetchPlotsFromDatabase();

        final long[] plot_id = new long[1];


        plots_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Integer plot_id = Integer.valueOf(mapSpinnerHashMap.get(plots_spinner.getSelectedItemPosition()));
                plot_id[0] = Long.valueOf(mapSpinnerHashMap.get(plots_spinner.getSelectedItemPosition()));
//                Log.i(TAG, "onItemSelected:  PLOT ID " + plot_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        dialog.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.continue_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText editTextName = dialog.findViewById(R.id.map_name);

                final String name = editTextName.getText().toString();

                if (name.isEmpty()){
                    editTextName.setError("Field required!");
                }


                if (!name.isEmpty()){
                    dialog.findViewById(R.id.loading_indicator).setVisibility(View.VISIBLE);

                    new Handler().postDelayed(() -> {

                            Toast.makeText(mContext, "starting scouting wizard now..", Toast.LENGTH_SHORT).show();
                            dialog.findViewById(R.id.continue_button).setVisibility(View.GONE);

                            dialog.dismiss();
                            Intent mainIntent = new Intent(getApplicationContext(), ScoutAutomated.class);
                            mainIntent.putExtra("mode", 1);
                            mainIntent.putExtra("name", name);
                            mainIntent.putExtra("plot_id", plot_id[0]  );
                            startActivity(mainIntent);

                    }, 3500);
                }
            }
        });

        hideKeyBoard();

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onScoutButtonClickedEvent(ScoutButtonClickedEvent event) {
        showAddScout();
    }

    private void fetchPlotsFromDatabase(){
        mMainViewModel.getPlots().observe(this, farmPlotList -> {
            String[] spinnerArray = new String[farmPlotList.size()];
            try {

                mapSpinnerHashMap = new HashMap<Integer, String>();
                for ( int i =0; i < farmPlotList.size(); i++ ){
//                              mapSpinnerHashMap.put(i, String.valueOf(farmPlotList.get(i).getId()));
                    mapSpinnerHashMap.put(i, String.valueOf(farmPlotList.get(i).getPlotId()));
                    spinnerArray[i] = farmPlotList.get(i).getName();
                    Log.i(TAG, "onChanged:  Plots Spinner " + farmPlotList.get(i).getPlotId());
                    Log.i(TAG, "onChanged:  Plots Spinner " + farmPlotList.get(i).getName());
                }
                HelperClass.setupArrayAdapter(this, spinnerArray, plots_spinner);
            } catch (Exception e) {
                Log.e(TAG, "onNext:  " + e.getMessage());
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPushedToServer(PushedToServerEvent event) {

        if (event.getEventType().equals("Activity")){
            Snackbar.make(parent_view, "Successfully pushed to server!", Snackbar.LENGTH_LONG).show();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onScoutButtonClickedEvent(ThreadCreatedEvent event) {
        loadFragment(new ForumFragment());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCommentPageBackEvent(CommentPageBackEvent event) {
        loadFragment(new ForumFragment());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDashBoardItemClicked(DashboardItemClickedEvent event) {

        switch (event.getEventType()){

            case"Activities":
                loadFarmAcctivitiesFragment();
                break;

            case "Plots":
                loadPlotsFragment();
                break;

            case "Scans":
                loadScanFragment();
                break;

            case "Maps":
                loadMapFragment();
                break;

            case "Farms":
                Toast.makeText(mContext, "Farms", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAlertReceived(NewAlertEvent event) {
        BottomNavigationView bottomNavigationViewMain  =  findViewById(R.id.nav_view);

        final int[] unreadAlertsCount = new int[1];

        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
            @Override
            public void run() {
                unreadAlertsCount[0] = mapDatabase.notificationDaoAccess().notificationsUnReadCount();
                Log.i(TAG, "run:  Unread Count  " + unreadAlertsCount[0]);
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                HelperClass.showBadge(getApplicationContext(), bottomNavigationViewMain, R.id.navigation_alert, String.valueOf(unreadAlertsCount[0]));
            }
        },1000);
    }

}