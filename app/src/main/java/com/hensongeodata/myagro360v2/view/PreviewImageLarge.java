package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Image;


/**
 * Created by user1 on 8/7/2017.
 */

public class PreviewImageLarge extends BaseActivity {
    private static String TAG=PreviewImageLarge.class.getSimpleName();
    Toolbar toolbar;
    ActionBar actionBar;
    MyApplication myApplication;
    private int globalIndex=-1;
    private int imagePositon=-1;
    private boolean isMultiImage=false;
    private Image image;
    //private ImageView imageView;
    private String image_path;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zoomimage);
        myApplication=new MyApplication();

          toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Preview Image");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

          SubsamplingScaleImageView mImage = findViewById(R.id.image);
        Bitmap imageBitmap=MyApplication.getImageModel().getImageBitmap();
        if(imageBitmap!=null){
            mImage.setImage(ImageSource.bitmap(imageBitmap));
        }
        else {
            Toast.makeText(this,"Invalid image format",Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        if(getIntent().getExtras()!=null) {
            isMultiImage=getIntent().getExtras().getBoolean("multi_image");

            if(isMultiImage){
            globalIndex=getIntent().getExtras().getInt("global_index");
            imagePositon=getIntent().getExtras().getInt("position");
            }

        }


          Button ibOk = findViewById(R.id.ib_ok);
          Button ibDelete = findViewById(R.id.ib_delete);

        ibOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close(false);
            }
        });

        ibDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close(true);
            }
        });
    }

    private void close(boolean delete){
        if(isMultiImage){
            if(delete)
                MyApplication.imageMultiArrayList.get(globalIndex).removeImage(imagePositon);
        }
        else {
            Intent i = new Intent();
            i.putExtra("delete", delete);
            setResult(Activity.RESULT_OK, i);
        }
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //getMenuInflater().inflate(R.menu.preview_image_large_simple_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

}
