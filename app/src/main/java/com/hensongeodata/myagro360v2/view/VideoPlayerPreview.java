package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.R;
import com.halilibo.bettervideoplayer.BetterVideoPlayer;

/**
 * Created by user1 on 4/23/2018.
 */

public class VideoPlayerPreview extends AppCompatActivity {

    private static final String TEST_URL = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";

    private BetterVideoPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_player_preview);

        String video_path = getIntent().getStringExtra("path");
        //video_path=TEST_URL;

        if(video_path ==null|| video_path.trim().isEmpty()){
            Toast.makeText(this,"Video file error.",Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        (findViewById(R.id.ib_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {close(false);
            }
        });

        // Grabs a reference to the player view
          player = findViewById(R.id.player);
        //player.setHideControlsDuration(600000);
        player.setAutoPlay(true);
        player.setBottomProgressBarVisibility(true);
        player.setHideControlsOnPlay(false);
        player.setSource(Uri.parse(video_path));
        player.showControls();


        ((TextView)findViewById(R.id.tv_caption)).setText(getIntent().getStringExtra("caption"));

   }

    @Override
    public void onPause() {
        super.onPause();
        // Make sure the player stops playing if the user presses the home button.
        player.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
      //  player.start();
    }

    private void close(boolean delete){
        Intent i = new Intent();
        i.putExtra("delete", delete);
        setResult(Activity.RESULT_OK, i);

        finish();
    }
}
