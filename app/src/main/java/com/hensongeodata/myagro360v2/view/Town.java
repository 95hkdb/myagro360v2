package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.MyState;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.User;

/**
 * Created by user1 on 10/13/2017.
 */

public class Town extends AppCompatActivity{
    private static String TAG=Town.class.getSimpleName();
    private  TextView tvCaption;
    private ListView listView;
    private ArrayAdapter<String> adapter;
    ArrayList<com.hensongeodata.myagro360v2.model.Town> towns;
    private EditText etSearch;
    private View bgDistrict;
    private View bgProgress;
    private ProgressBar progressBar;
    private TextView tvProgress;
    private View container;
    private View retryBg;

    private ArrayList<String> mList;
    private static int LOAD_COUNTRY=100;
    private static int LOAD_REGION=200;
    private static int LOAD_DISTRICT=300;
    private static int LOAD_TOWN=400;
    private int mAction;
    private String newDistrict=null;
    private String countryID;
    private String districtID;
    private String regionID;
    private Object objID;//either country_id returned or region_id returned from server to use to fetch the subsequent list itemsVisible

    private ProgressDialog progressDialog;
    private MyApplication myApplication;
    private DatabaseHelper databaseHelper;
    private NetworkRequest networkRequest;
    private MyState myState;
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_work_area);
        myApplication=new MyApplication();
        user=new User(Town.this);
        databaseHelper=new DatabaseHelper(this);
        networkRequest=new NetworkRequest(this);
        myState=new MyState(this);

        progressDialog=new ProgressDialog(this);
        progressDialog.setCancelable(false);

        bgDistrict=findViewById(R.id.bg_district);
          tvCaption = findViewById(R.id.tv_caption);
        tvCaption.setText("Choose Town");
          findViewById(R.id.ib_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {finish();}
        });
        bgProgress =findViewById(R.id.bg_progressbar);
          progressBar = findViewById(R.id.progressbar);
          tvProgress = findViewById(R.id.tv_progress);
        container=findViewById(R.id.container);
        retryBg=findViewById(R.id.bg_retry);
        retryBg.setVisibility(View.GONE);
          findViewById(R.id.btn_retry).setVisibility(View.GONE);
          listView = findViewById(R.id.listview);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                doItemSelected((String) listView.getItemAtPosition(i),i);
            }
        });

          etSearch = findViewById(R.id.et_search);
        etSearch.setHint("Type town name...");
        etSearch.addTextChangedListener(new TextWatcher() {


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               if(Town.this.adapter!=null)
                    Town.this.adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        towns=databaseHelper.getAllItems(DatabaseHelper.TOWN_TBL,"-1");

        if(towns.size()>0) {
            loadTowns();
        }
        else {
            listView.setVisibility(View.GONE);
        }


        findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG,"save button clicked");
                save();
            }
        });
        findViewById(R.id.tv_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
            }
        });
    }

    private void doItemSelected(String selection,int position){
        etSearch.setText(selection);
        if(user!=null&&towns!=null&&towns.size()>position)
            user.setTown_id(towns.get(position).getId());

        save();
    }


    private void loadTowns(){

        List<String> list=new ArrayList<>();
        for(int a=0; a<towns.size();a++){
            String townName=towns.get(a).getName().toLowerCase();
              townName = townName.substring(0, 1).toUpperCase() + townName.substring(1);
            list.add(townName);
        }
        adapter=new ArrayAdapter<String>(Town.this,android.R.layout.simple_list_item_1,list);

        listView.setAdapter(adapter);
        setBackground(false,true,false);
    }

    private void setBackground(boolean loading,boolean displayList,boolean retry){
        if(mAction==LOAD_TOWN){
            progressDialog.setMessage("Loading towns, Please wait...");

            if(loading) progressDialog.show(); else if(progressDialog.isShowing()) progressDialog.hide();

            container.setVisibility(displayList ? View.VISIBLE : View.GONE);
            retryBg.setVisibility(retry ? View.VISIBLE : View.GONE);
        }
        else {
            bgProgress.setVisibility(loading ? View.VISIBLE : View.GONE);
            progressBar.setVisibility(mAction != LOAD_TOWN ? View.VISIBLE : View.GONE);
            tvProgress.setVisibility(mAction == LOAD_TOWN ? View.VISIBLE : View.GONE);
            container.setVisibility(displayList ? View.VISIBLE : View.GONE);
            retryBg.setVisibility(retry ? View.VISIBLE : View.GONE);
        }
        }


    private View.OnClickListener saveListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           save();
        }
    };

    private void save(){
        Log.d(TAG,"executing save");
        user.setWorkarea(etSearch.getText().toString().trim());
        District.town=user.getWorkarea();
        Intent i=new Intent();
        setResult(Activity.RESULT_OK,i);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        databaseHelper.close();
    }
}
