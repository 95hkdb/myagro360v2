package com.hensongeodata.myagro360v2.view;

import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.Interface.NetworkResponse;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.LearningAdapterRV;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.model.KnowledgeBase;
import com.hensongeodata.myagro360v2.model.SettingsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by user1 on 1/25/2018.
 */

public class LibraryActivity extends BaseActivity implements NetworkResponse{
    private static String TAG= LibraryActivity.class.getSimpleName();
    public static String ACTION_SELECT="item selection action";
    public static String ACTION_DOWNLOAD="download action";
    private LearningAdapterRV adapter;
    private IntentFilter filter;
    public static List<KnowledgeBase> knowledgeBases;
    private RecyclerView recyclerView;
    private TextView tvPlaceholder;
    private FloatingActionButton fabAction;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kb_menu);
          Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        progressDialog.setCancelable(true);

        ((CollapsingToolbarLayout)findViewById(R.id.collapsingToolbarLayout)).setTitle(null);

          recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
          tvPlaceholder = findViewById(R.id.tv_placeholder);

          fabAction = findViewById(R.id.fab_action);
        fabAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(databaseHelper.dropTable(DatabaseHelper.TBL_KB))
                    databaseHelper.createTable(DatabaseHelper.TBL_KB,null);

                if(!loadList())
                    fetchList();
            }
        });

       // settings.setLang(SettingsModel.LAN_FR);

        if(!loadList())
            fetchList();

        //((TextView)(findViewById(R.id.tv_faw))).setText(resolveString(R.string.faw));

        if(settings.getLang().equalsIgnoreCase(SettingsModel.LAN_FR))
            ((TextView)(findViewById(R.id.tv_faw))).setText("Apprendre");
        else
            ((TextView)(findViewById(R.id.tv_faw))).setText(""+resolveString(R.string.kb));

    }


    @Override
    public void onRespond(Object object) {
        Log.d(TAG,"JoinCommunityResponse "+object);
        if(object!=null||(object instanceof String && ((String) object).equalsIgnoreCase("error"))){
            try {
                JSONObject jsonObject=new JSONObject(""+object);
                int status=jsonObject.optInt("success");
                Log.d(TAG,"status: "+status);
                if(status==1){
                    JSONArray jsonArray=jsonObject.optJSONArray("topics");
                    JSONObject item;
                    if(jsonArray!=null) {
                        for (int a = 0; a < jsonArray.length(); a++) {
                            item = jsonArray.optJSONObject(a);
                            Log.d(TAG,"item audio: "+item.optString("audio"));
                            Log.d(TAG,"item media: "+item.optString("media_url"));
                            if (item != null) {
                                KnowledgeBase knowledgeBase=new KnowledgeBase(
                                        a,
                                        item.optString("title"),
                                        item.optString("content"),
                                        item.optString("media_url"),
                                        item.optString("audio"),
                                        item.optString("media_type"),
                                settings.getLang()
                                );

                              databaseHelper.addKB(knowledgeBase);
                            }
                        }
                    }
                }
                else {
                    myApplication.showMessage(this,resolveString(R.string.no_content));
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d(TAG,"json exception: "+e);
            }
        }
        else {
            myApplication.showMessage(this,resolveString(R.string.no_response));
        }
        hideProgress();
        loadList();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideProgress();
        progressDialog.setCancelable(false);
    }

    private boolean loadList(){
        knowledgeBases=databaseHelper.getKBs(settings.getLang());
        Log.d(TAG,"size: "+knowledgeBases.size());
        if(knowledgeBases==null||knowledgeBases.size()<1){
            recyclerView.setVisibility(View.GONE);
            tvPlaceholder.setVisibility(View.VISIBLE);
            fabAction.show();
            return false;
        }
        else {
            tvPlaceholder.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter = new LearningAdapterRV(this, knowledgeBases);
            recyclerView.setAdapter(adapter);
            fabAction.show();
            Log.d(TAG,"audio: "+knowledgeBases.get(0).getAudio());
            return true;
        }
    }

    private void fetchList(){
        if(myApplication.hasNetworkConnection(this)){
            showProgress(resolveString(R.string.loading));
            networkRequest.fetchKnowledgeBase(this);
        }
        else {
            hideProgress();
            myApplication.showInternetError(this);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
