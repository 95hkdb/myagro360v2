package com.hensongeodata.myagro360v2.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.MyViewpager;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.fragment.FormFragmentFixed;
import com.hensongeodata.myagro360v2.model.Form;
import com.hensongeodata.myagro360v2.service.UploadFilesService;

import org.json.JSONException;

public class FixedActivity extends BaseActivity{

    private static String TAG=FixedActivity.class.getSimpleName();
    private static int ACTION_SUBMIT=100;
    private static int ACTION_DOWNLOAD =200;
    private static int MODE_DATA=300;
    private static int MODE_FILE=302;
    private static int submission_mode;
    private int action;
    private FloatingActionButton fabBack;
    private ActionBar actionBar;
    public static boolean scannerActive=false;

    private BottomSheetBehavior mBottomSheetBehavior;
    private ImageButton ibBSHandle;
    private  View bottomSheet;
    private View btnBSHandle;
    private boolean bsOpened=false;

    private FixedActivityReceiver receiver;
    private IntentFilter filterUpload,filterDownload;
    private Form form;
    private String form_code;
    private String form_id;
    FormFragmentAdapter adapter;
    MyViewpager viewPager;
    CollapsingToolbarLayout collapsingToolbarLayout;

    private Handler handler;
    private Runnable runnable;
    private String transaction_id;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fixed_activity);
        createForm.sections.clear();
        scannerActive=false;
        initBottomSheet();

        filterDownload=new IntentFilter(BroadcastCall.FORM_LIST);
        filterUpload=new IntentFilter(BroadcastCall.TRANSACTION_UPDATE);

        form_code=getIntent().getExtras().getString("form_code");
        MyCamera.autoStart=true;
        submission_mode=MODE_DATA;

          scannerActive = form_code != null && form_code.equalsIgnoreCase("NvkbnF");

          Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar=getSupportActionBar();
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


          collapsingToolbarLayout = findViewById(R.id.collapsingToolbarLayout);

         networkRequest.fetchForms(null);//no broadcast JoinCommunityResponse registered hence null

        LayoutInflater mInflater = LayoutInflater.from(this);
        View actionBarCustom = mInflater.inflate(R.layout.actionbar_custom, null);
        actionBar.setCustomView(actionBarCustom);

          fabBack = findViewById(R.id.fab_back);
        adapter = new FormFragmentAdapter(getSupportFragmentManager());

          viewPager = findViewById(R.id.viewpager);
        viewPager.setPagingEnabled(false);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(10);


        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewPager.getCurrentItem()!=-1)
                    viewPager.setCurrentItem(viewPager.getCurrentItem()-1);//scroll to previous page
            }
        });

        showMain();
        updateFabIcon();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updateFabIcon();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        form_id=getForm_id(form_code);
        form=databaseHelper.getForm(form_id);
        Log.d(TAG,"1 form: "+form+" form_code: "+form_code+" form_id: "+form_id);
        if(form==null||form.getId()==null){
            accessForm(form_code);
        }

        createForm.setShouldRefreshForm(true);
        Log.d(TAG,"refresh: "+createForm.isShouldRefreshForm());
    }

    private void accessForm(String code){
        action= ACTION_DOWNLOAD;
        registerReceiver(filterDownload);
        if(myApplication.hasNetworkConnection(this)){
            showProgress("Requesting access, Please wait...");
            networkRequest.fetchForm(code,false);
        }
        else
            myApplication.showInternetError(this);
    }

    private void registerReceiver(IntentFilter filter){

        receiver=new FixedActivityReceiver();
        registerReceiver(receiver,filter);
    }

    public void next(int sectionIndex){

        FormFragmentFixed formFragment= (FormFragmentFixed) adapter.instantiateItem(viewPager,sectionIndex);
        if(formFragment.validate()){ //particular form in focus is validated
            if((sectionIndex+1)>=adapter.getCount()){

                try {
                    if(MyApplication.isEmptyPOI(myApplication.getInstantPOI(this,true)))
                        return;

                        transaction_id=databaseHelper.saveArchive(createForm.buildJSON(FixedActivity.this,form_id,false));
                        submitForm();
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgress();
                    Toast.makeText(FixedActivity.this, "Form failed to submit.", Toast.LENGTH_SHORT).show();
                }
            }
            else {
                viewPager.setCurrentItem(sectionIndex+1);//scroll to next page
            }
        }

    }
    public void next(){

        FormFragmentFixed formFragment= (FormFragmentFixed) adapter.instantiateItem(viewPager,viewPager.getCurrentItem());
        if(formFragment.validate()){ //particular form in focus is validated
            if((viewPager.getCurrentItem()+1)>=adapter.getCount()){

                try {
                    if(MyApplication.isEmptyPOI(myApplication.getInstantPOI(this,true)))
                        return;

                    transaction_id=databaseHelper.saveArchive(createForm.buildJSON(FixedActivity.this,form_id,false));
                        submitForm();

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgress();
                    Toast.makeText(FixedActivity.this, "Form failed to save.", Toast.LENGTH_SHORT).show();
                }
            }
            else {
                viewPager.setCurrentItem(viewPager.getCurrentItem()+1);//scroll to next page
            }
        }

    }

    View.OnClickListener nextListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            next();
        }
    };


    private void showMain(){
        Log.d(TAG,"show Main");
        viewPager.setVisibility(View.VISIBLE);
        refreshForm();
        updateFabIcon();
    }

    private void refreshForm(){
        form_id=getForm_id(form_code);
        form=databaseHelper.getForm(form_id);
        Log.d(TAG,"refresh form: "+form_id);

        if(form!=null&&form.getId()!=null) {
            createForm.sections = databaseHelper.displayForm(form.getId());
            adapter = new FormFragmentAdapter(getSupportFragmentManager());
            viewPager.setAdapter(adapter);
        }
            Log.d(TAG,"form: "+form);
            if(form!=null)
                Log.d(TAG,"form title: "+form.getTitle());

            if(form!=null&&form.getTitle()!=null&&!form.getTitle().isEmpty()) {
                    collapsingToolbarLayout.setTitle(form.getTitle());
            }
        else {
            collapsingToolbarLayout.setTitle(getResources().getString(R.string.app_name));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateFabIcon();
      }


    public class FormFragmentAdapter extends FragmentStatePagerAdapter {
        private String TAG= FormFragmentAdapter.class.getSimpleName();
        private FormFragmentFixed formFragment;
        public FormFragmentAdapter(FragmentManager fm) {
            super(fm);
            formFragment =new FormFragmentFixed();
        }

        public Fragment getFragment(){
            return formFragment;
        }

        @Override
        public Parcelable saveState() {
            return super.saveState();
        }

        @Override
        public int getCount() {
            return createForm.sections.size();
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment= formFragment.newInstance(position);
            return fragment;
        }
    }

    private void updateFabIcon(){

        if(adapter!=null&&adapter.getCount()!=0) {
            if(findViewById(R.id.btn_next)!=null) {
                if (viewPager.getCurrentItem() + 1 == adapter.getCount() || viewPager.getCurrentItem() == adapter.getCount()) {
                        ((Button) findViewById(R.id.btn_next)).setText("Submit Information");
                } else
                    ((Button) findViewById(R.id.btn_next)).setText("Next page");

                  findViewById(R.id.btn_next).setOnClickListener(nextListener);
            }
            if (adapter.getCount() != 1) {
                if (viewPager.getCurrentItem() != 0)
                    fabBack.setVisibility(View.VISIBLE);
                else
                    fabBack.setVisibility(View.GONE);
            } else
                fabBack.setVisibility(View.GONE);
        }
        }


    public String getBtnText(int sectionIndex){

        if(adapter.getCount()!=0){

                if (sectionIndex+1 ==createForm.sections.size()) {
                        return "Submit Information";
                } else
                    return "Next page";
        }

        return null;
    }

    private void submitForm(){
        registerReceiver(filterUpload);
        action=ACTION_SUBMIT;
        submission_mode=MODE_DATA;
            if(myApplication.hasNetworkConnection(this)) {
                showProgress("Submitting data. Please wait...");
                try {
                    networkRequest.sendTransaction(databaseHelper.buildJSON(transaction_id,true));
                    //.buildJSON(FixedActivity.this,form_id,true));
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgress();
                    Toast.makeText(this, "Submission failed", Toast.LENGTH_SHORT).show();
                }
            }
            else {
                myApplication.showInternetError(this);
            }

        }


    class FixedActivityReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int status=intent.getExtras().getInt("status");
            String msg=intent.getExtras().getString("date");
            Log.d(TAG,"status: "+status+" msg: "+msg+" submission_mode:"+submission_mode);
            if(status== NetworkRequest.STATUS_SUCCESS){
                if(action==ACTION_SUBMIT) {
                    if (submission_mode == MODE_DATA) {
                        submission_mode = MODE_FILE;
                        UploadFilesService.progressDialog = getProgress();
                        startService(new Intent(FixedActivity.this, UploadFilesService.class));
                        // hideProgress();
                    } else if (submission_mode == MODE_FILE) {
                        refreshForm();
                        Toast.makeText(context, "Data submitted successfully", Toast.LENGTH_SHORT).show();
                        databaseHelper.removeArchive(transaction_id);
                        hideProgress();
                    }
                }
                else if(action==ACTION_DOWNLOAD){
                    refreshForm();
                    hideProgress();
                    Log.d(TAG,"downloaded");
                }
                else {
                    if(msg!=null)
                        myApplication.showMessage(context,msg);
                    else
                        myApplication.showMessage(context,"Submission mode not specified.");

                    hideProgress();
                }
            }
            else if(status==NetworkRequest.STATUS_FAIL){
                if(action==ACTION_DOWNLOAD){
                    refreshForm();
                }
                else {
                    if (submission_mode == MODE_DATA) {
                        if (msg != null)
                            myApplication.showMessage(context, msg);
                        else
                            myApplication.showMessage(context, "Data submission failed.");

                    } else if (submission_mode == MODE_FILE)
                        myApplication.showMessage(context, "Data submission incomplete.");
                }
                hideProgress();
            }
        }
    }


    private String getForm_id(String form_code){
        if(form_code.equalsIgnoreCase("tLSxRB"))
            return "965";
        else if(form_code.equalsIgnoreCase("AdlUZ6"))
            return "1115";
        else if(form_code.equalsIgnoreCase("NvkbnF"))
            return "3628";
        else
            return null;
    }

    private void initBottomSheet(){

        bottomSheet = findViewById(R.id.bottom_sheet);
        btnBSHandle=bottomSheet.findViewById(R.id.bg_bottomsheet_handle);
          ibBSHandle = bottomSheet.findViewById(R.id.ib_bottomsheet_handle);
        ibBSHandle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getActivity(),"handle click",Toast.LENGTH_SHORT).show();
                int bsState=mBottomSheetBehavior.getState();
                if(bsState==BottomSheetBehavior.STATE_COLLAPSED){
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                else if(bsState==BottomSheetBehavior.STATE_EXPANDED) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });

        btnBSHandle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ibBSHandle.performClick();
            }
        });

        //  View bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setPeekHeight(100);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                //  Toast.makeText(getActivity(),"bs callback",Toast.LENGTH_SHORT).show();
                if(newState==BottomSheetBehavior.STATE_COLLAPSED) {
                    ibBSHandle.setImageResource(R.drawable.ic_arrow_up);
                    bsOpened=false;
                }
                else if (newState==BottomSheetBehavior.STATE_EXPANDED) {
                    ibBSHandle.setImageResource(R.drawable.ic_arrow_down);
                    bsOpened=true;
                }
                //bsOpened=!bsOpened;
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        bottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickLocationPermission();
                Toast.makeText(FixedActivity.this,"Refreshing location updates...",Toast.LENGTH_SHORT).show();
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds itemsVisible to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_info) {
            Intent i=new Intent(this,FormDetail.class);
            i.putExtra("form_id",settings.getForm_id());
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(receiver!=null)
            unregisterReceiver(receiver);

        if(handler!=null)
            handler.removeCallbacks(runnable);

        scannerActive=false;
    }
}