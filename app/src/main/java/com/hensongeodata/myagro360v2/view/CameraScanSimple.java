package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.controller.Utility;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Model.ScanModel;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import id.zelory.compressor.Compressor;
import io.fotoapparat.Fotoapparat;
import io.fotoapparat.configuration.UpdateConfiguration;
import io.fotoapparat.error.CameraErrorListener;
import io.fotoapparat.exception.camera.CameraException;
import io.fotoapparat.parameter.ScaleType;
import io.fotoapparat.preview.Frame;
import io.fotoapparat.preview.FrameProcessor;
import io.fotoapparat.result.PhotoResult;
import okhttp3.ResponseBody;
import retrofit2.Call;

import static io.fotoapparat.selector.FlashSelectorsKt.off;
import static io.fotoapparat.selector.FlashSelectorsKt.torch;
import static io.fotoapparat.selector.LensPositionSelectorsKt.back;

/**
 * Created by user1 on 5/15/2018.
 */

public class CameraScanSimple extends MyCamera {
    private static String TAG=CameraScanSimple.class.getSimpleName();
    private Image image;
    private Handler handler;
    private Runnable snapRunnable;
    private Handler handlerProcessShot;
    private Runnable runnableProcessShot;
    private TextView tvStop;
    private ImageView ivPreview;
    private View vPreview;
    private String item_id;// the
    private String scout_id;
    private int point_number;
    private int plant_number;
    private Vibrator vibrator;
    private Call<ResponseBody> call;
    private TextView tvCountDown;
      private int count = 3;
    private Handler handlerTime=new Handler();
    private Runnable runnableTime;
    private MapDatabase mapDatabase;
    private String orgId;
    private String userId;
    private long plotId;
    private long scoutPrimaryId;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_scan_simple);
        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        mapDatabase = MapDatabase.getInstance(this);
        orgId  = SharePrefManager.getInstance(this).getOrganisationDetails().get(0);
        userId  = SharePrefManager.getInstance(this).getUserDetails().get(0);
          tvCountDown = findViewById(R.id.tv_count);

          cameraView = findViewById(R.id.camera_view);
          ibFlash = findViewById(R.id.ib_flash);
        ibFlash.setImageResource(R.drawable.ic_flash_off);

        item_id=getIntent().getStringExtra("item_id");
        scout_id=getIntent().getStringExtra("map_id");
        plotId =getIntent().getLongExtra("plot_id", 0);
        scoutPrimaryId =getIntent().getLongExtra("scout_primary_id", 0);
        if(scout_id==null)
            scout_id="";

        point_number=getIntent().getIntExtra("point_number",0);
        plant_number=getIntent().getIntExtra("plant_number",0);

        vCapture =findViewById(R.id.btn_snap);
        vCaptureInner=findViewById(R.id.snap_inner);
        vCaptureInner.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/10,1.0f,0.0f));

        vCaptureOutter=findViewById(R.id.snap_outter);
        vCaptureEffect=findViewById(R.id.snap_effect);

          tvFlash = findViewById(R.id.tv_flash);

          tvSnap = findViewById(R.id.tv_snap);
          tvAction = findViewById(R.id.tv_action);

          cameraView = findViewById(R.id.camera_view);
        fotoapparat= createFotoapparatImage();


        updateFlash(FLASH_OFF);
        updateFotoapparat(FLASH_OFF);

        vCapture.setOnClickListener(snapClickListener);
        tvSnap.setText(resolveString(R.string.tap_scan));

        handler=new Handler();
        snapRunnable=new Runnable() {
            @Override
            public void run() {
                vCapture.setOnClickListener(snapClickListener);
                vCaptureInner.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/12,1.0f,0.0f));
                vCapture.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/10,1.2f,1.0f));
                vCaptureEffect.setVisibility(View.GONE);
            }
        };

        (findViewById(R.id.ib_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        handlerProcessShot=new Handler();



        (findViewById(R.id.ib_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    View.OnClickListener snapClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
                snapEffectImage();
                snap();
        }
    };

    protected void startCamera(){
        if (Utility.checkPermission(this))
            fotoapparat.start();
        else
            Toast.makeText(this,resolveString(R.string.permission_camera),Toast.LENGTH_SHORT).show();
    }

    protected void stopCamera(){
        if (Utility.checkPermission(this))
            fotoapparat.stop();
        else
            Toast.makeText(this,resolveString(R.string.permission_camera),Toast.LENGTH_SHORT).show();

        finish();
    }

    protected void updateFlash(String flash){
        tvFlash.setText(flash);
         if(flash.equalsIgnoreCase(FLASH_ON))
            ibFlash.setOnClickListener(new FlashListener(FLASH_OFF));
        else if(flash.equalsIgnoreCase(FLASH_OFF))
            ibFlash.setOnClickListener(new FlashListener(FLASH_ON));
    }

    protected void updateFotoapparat(String flash) {
        Log.d(TAG,"fotoapparat: "+fotoapparat+" flash: "+flash);
        if (fotoapparat != null&&flash!=null) {
            fotoapparat.updateConfiguration(
                    flashConfiguration(flash)
            );
        }
    }

    protected UpdateConfiguration flashConfiguration(String flash){
        Log.d(TAG,"config flash: "+flash);
       if(flash.equalsIgnoreCase(FLASH_ON))
            return UpdateConfiguration.builder()
                    .flash(
                            torch()
                    )
                    .build();
        else
            return UpdateConfiguration.builder()
                    .flash(
                            off()
                    )
                    .build();
    }

    class FlashListener implements View.OnClickListener{
        String flash;
        FlashListener(String flash){
            this.flash=flash;
        }
        @Override
        public void onClick(View view) {
            if(flash.equalsIgnoreCase(FLASH_ON)){
                ibFlash.setImageResource(R.drawable.ic_flash_on);
            }
            else if(flash.equalsIgnoreCase(FLASH_OFF)){
                ibFlash.setImageResource(R.drawable.ic_flash_off);
            }

            updateFotoapparat(flash);
            updateFlash(flash);
        }
    }

    private void snapEffectImage(){
        ((TextView)(findViewById(R.id.tv_scan))).setText("");
        tvSnap.setText(resolveString(R.string.processing));
        vCaptureInner.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/12,0.0f,1.0f));
        vCapture.startAnimation(LocsmmanEngine.ScaleOnce(DURATION_SNAP/10,1.0f,1.2f));
    }

    private Fotoapparat createFotoapparatImage() {
        return Fotoapparat
                .with(this)
                .into(cameraView)
                .previewScaleType(ScaleType.CenterCrop)
                .lensPosition(back())
                .flash(off())
                .frameProcessor(new SampleFrameProcessor())
                .cameraErrorCallback(new CameraErrorListener() {
                    @Override
                    public void onError(@NotNull CameraException e) {
                        Toast.makeText(CameraScanSimple.this, e.toString(), Toast.LENGTH_LONG).show();
                    }
                })
                .build();
    }

    private class SampleFrameProcessor implements FrameProcessor {
        @Override
        public void process(@NotNull Frame frame) {
            // Perform frame processing, if needed
        }
    }

    private void snap() {
        final PhotoResult photoResult = fotoapparat.takePicture();
        image=new Image();
        image.setId(index+" Image");

        final String fileName=validate(image.getId()+(new CreateForm(this)).getCurrentTime());

        final File file=new File(getExternalFilesDir("photos"), fileName+".jpg");
        photoResult.saveToFile(file);

        image.setUri(Uri.fromFile(file));


        runnableProcessShot=new Runnable() {
            @Override
            public void run() {
                cancel(handlerProcessShot,runnableProcessShot);
                    processShot(image,file);
            }
        };

        runnableTime=new Runnable() {
            @Override
            public void run() {
              tvCountDown.setVisibility(View.VISIBLE);
              tvCountDown.setText(""+count);
              if(count>1){
                  count--;
                  handlerTime.postDelayed(runnableTime,1000);
              }
            }
        };

        handlerTime=new Handler();
        handlerTime.post(runnableTime);
          handlerProcessShot.postDelayed(runnableProcessShot, 3000);
    }

    private File saveImage(Bitmap bitmap, String path) {
        Log.d(TAG,"saving bmp...");
        File imageFile=null;
        if(bitmap!=null) {
            Log.d(TAG, "b4 bmp size: " + bitmap.getDensity());
            bitmap = (new MyApplication()).bitmapCompressLarge(bitmap);
            Log.d(TAG, "after bmp size: " + bitmap.getDensity());

            File filesDir = getFilesDir();
            imageFile = new File(getExternalFilesDir("photos"), path);

            OutputStream os;
            try {
                os = new FileOutputStream(imageFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 40, os);
                Log.d(TAG,"bmp size: "+bitmap.getDensity());
                os.flush();
                os.close();
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
            }
        }


        return imageFile;
    }

    private String validate(String fileName){
        fileName=fileName.replaceAll("\\s","");
        fileName=fileName.replaceAll(":","");
        fileName=fileName.trim();

        return fileName;
    }

    private void processShot(Image image,File file){
        //saveImage(file);
        File compressedImageFile=null;

        try {
            if(file!=null) {
                compressedImageFile = new Compressor(this).compressToFile(file);
            }
            } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(TAG,"compressed file: "+compressedImageFile);
        if(compressedImageFile!=null&&image!=null){
            image.setUri(Uri.fromFile(compressedImageFile));
            Log.d(TAG,"new uri: "+image.getUri());
        }
        InstructionScout instructionScout=new InstructionScout(
                null, image, InstructionScout.ACTION_SCAN, InstructionScout.STATE_UNPROCCESSED);

        instructionScout.setId(item_id);
        instructionScout.setScout_id(scout_id);
        instructionScout.setPoint_number(point_number);
        instructionScout.setPlant_number(plant_number);
        Log.d(TAG,"scout_id: "+scout_id);
        databaseHelper.addScoutItem(instructionScout, ChatbotAdapterRV.MODE_SCOUT);

        ScanModel scanModel = new ScanModel();
        scanModel.setItemId(item_id);
        scanModel.setImagePath(image != null ? image.getUri().toString() : null);
        scanModel.setStatus(ScanModel.STATE_UNPROCCESSED);
        scanModel.setMessage(null);
        scanModel.setScoutId(scoutPrimaryId);
        scanModel.setPointNumber(point_number);
        scanModel.setPlantNumber(plant_number);
        scanModel.setPlotId(plotId);
        scanModel.setUserId(userId);
        scanModel.setOrgId(orgId);

          AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                @Override
                public void run() {
                      mapDatabase.scanDaoAccess().insertSingleScout(scanModel);
                }
          });

        if(MyApplication.itemsLive==null)
            MyApplication.itemsLive=new ArrayList<>();

        MyApplication.itemsLive.add(instructionScout);

        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {

                end(true);
            }
        },1000);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"onStart camera");
        startCamera();
    }

    @Override
    protected void onStop() {
        super.onStop();
       stopCamera();
    }

    private void end(boolean success){
            Intent i = new Intent();
            i.putExtra("success",success?1:0);
            setResult(Activity.RESULT_OK, i);
            finish();
    }

    private void cancel(Handler handler,Runnable runnable){
        if(handler!=null&&runnable!=null)
            handler.removeCallbacks(runnable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancel(handlerProcessShot,runnableProcessShot);
        cancel(handlerTime,runnableTime);
    }
}
