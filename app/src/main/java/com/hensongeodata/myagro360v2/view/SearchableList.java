package com.hensongeodata.myagro360v2.view;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ArchiveAdapterRV;
import com.hensongeodata.myagro360v2.adapter.FormAdapterRV;
import com.hensongeodata.myagro360v2.adapter.HistoryAdapterRV;
import com.hensongeodata.myagro360v2.adapter.LearningAdapterRV;
import com.hensongeodata.myagro360v2.adapter.OrgAdapterRV;
import com.hensongeodata.myagro360v2.adapter.POIGroupAdapterRV;
import com.hensongeodata.myagro360v2.adapter.PaymentHistoryAdapterRV;

/**
 * Created by user1 on 1/25/2018.
 */

public class SearchableList extends BaseActivity {
    private static String TAG=SearchableList.class.getSimpleName();
    private static String ACTION_SEARCH="action_search";
    private static String ACTION_NORMAL="action_normal";

    protected RecyclerView recyclerView;
    protected TextView tvCaption;
    protected TextView tvPlaceholder;
    protected EditText etSearch;
    protected FloatingActionButton fabAction;
    protected View bgAction;
    protected View bgTopAction;

    private View bgNormal;
    private View bgSearch;

    //classes which are allowed to extend this searchable list class
    protected static String ARCHIVE="archive";
    protected static String HISTORY="history";
    protected static String RECEIPTS="receipts";
    protected static String ORGANIZATION="organization";
    protected static String FORM="form";
    protected static String POI_GROUP="poi_group";
    protected static String KNOWLEDGE_BASE="kb";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searchable_list);

          recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        //close this activity on close button click
        (findViewById(R.id.ib_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        bgNormal=findViewById(R.id.bg_normal);
        bgSearch=findViewById(R.id.bg_search);

          tvCaption = findViewById(R.id.tv_caption);
          tvPlaceholder = findViewById(R.id.tv_placeholder);

          etSearch = findViewById(R.id.et_search);

        (findViewById(R.id.ib_clear)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {etSearch.setText(null);}
        });

        (findViewById(R.id.ib_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSearch.setText(null);
                toggleActionBar(ACTION_NORMAL);}
        });

        findViewById(R.id.ib_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {toggleActionBar(ACTION_SEARCH);}
        });

        bgAction= findViewById(R.id.bg_action);
        bgTopAction=findViewById(R.id.bg_top_action);

          fabAction = findViewById(R.id.fab_action);
        fabAction.requestFocus();
    }


    private void toggleActionBar(String action){

        if(action==ACTION_NORMAL){
            bgSearch.setVisibility(View.GONE);
            bgNormal.setVisibility(View.VISIBLE);
        }
        else if(action==ACTION_SEARCH){
            bgNormal.setVisibility(View.GONE);
            bgSearch.setVisibility(View.VISIBLE);
        }
    }

    protected class MyWatcher implements TextWatcher{
        Object mAdapter;
        String mType;
       public MyWatcher(String type,Object adapter){
               mAdapter=adapter;
           mType=type;
       }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            Log.d(TAG, "mType: " + mType);
            if(mAdapter!=null) {
                if (mType != null) {
                    if (mType.equalsIgnoreCase(ARCHIVE)) {
                        ((ArchiveAdapterRV) mAdapter).search(String.valueOf(charSequence));
                        if (((ArchiveAdapterRV) mAdapter).getItemCount() == 0)
                            setTvPlaceholder("No search result found.");
                        else
                            tvPlaceholder.setVisibility(View.GONE);
                    } else if (mType.equalsIgnoreCase(HISTORY)) {
                        ((HistoryAdapterRV) mAdapter).search(String.valueOf(charSequence));
                        if (((HistoryAdapterRV) mAdapter).getItemCount() == 0)
                            setTvPlaceholder("No search result found.");
                        else
                            tvPlaceholder.setVisibility(View.GONE);
                    } else if (mType.equalsIgnoreCase(ORGANIZATION)) {
                        ((OrgAdapterRV) mAdapter).search(String.valueOf(charSequence));
                        if (((OrgAdapterRV) mAdapter).getItemCount() == 0)
                            setTvPlaceholder("No search result found.");
                        else
                            tvPlaceholder.setVisibility(View.GONE);
                    } else if (mType.equalsIgnoreCase(FORM)) {
                        ((FormAdapterRV) mAdapter).search(String.valueOf(charSequence));
                        if (((FormAdapterRV) mAdapter).getItemCount() == 0)
                            setTvPlaceholder("No search result found.");
                        else
                            tvPlaceholder.setVisibility(View.GONE);
                    } else if (mType.equalsIgnoreCase(POI_GROUP)) {
                        ((POIGroupAdapterRV) mAdapter).search(String.valueOf(charSequence));
                        if (((POIGroupAdapterRV) mAdapter).getItemCount() == 0)
                            setTvPlaceholder("No search result found.");
                        else
                            tvPlaceholder.setVisibility(View.GONE);
                    }
                    else if (mType.equalsIgnoreCase(KNOWLEDGE_BASE)) {
                        ((LearningAdapterRV) mAdapter).search(String.valueOf(charSequence));
                        if (((LearningAdapterRV) mAdapter).getItemCount() == 0)
                            setTvPlaceholder("No search result found.");
                        else
                            tvPlaceholder.setVisibility(View.GONE);
                    }
                    else if (mType.equalsIgnoreCase(RECEIPTS)) {
                        ((PaymentHistoryAdapterRV) mAdapter).search(String.valueOf(charSequence));
                        if (((PaymentHistoryAdapterRV) mAdapter).getItemCount() == 0)
                            setTvPlaceholder("No search result found.");
                        else
                            tvPlaceholder.setVisibility(View.GONE);
                    }
                }
            }
            }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }

    protected void setTvPlaceholder(String message){
        tvPlaceholder.setText(message);
        tvPlaceholder.setVisibility(View.VISIBLE);
    }
}
