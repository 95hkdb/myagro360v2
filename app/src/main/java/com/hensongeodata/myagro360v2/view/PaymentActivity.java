package com.hensongeodata.myagro360v2.view;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.braintreepayments.cardform.OnCardFormValidListener;
import com.braintreepayments.cardform.view.CardForm;
import com.hensongeodata.myagro360v2.Interface.PaymentListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.Pay;
import com.hensongeodata.myagro360v2.model.PaymentReceipt;
import com.hensongeodata.myagro360v2.model.Product;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.receiver.PushReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.UUID;

public class PaymentActivity extends BaseActivity implements PaymentListener{
 private static String TAG=PaymentActivity.class.getSimpleName();
Pay pay;
List<Product> productList;
Button btnSubmit;
TextView tvAmount;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment);

        pay=new Pay();

        ((TextView)findViewById(R.id.tv_caption)).setText("Enter Card Info");
        ImageButton ibBack=findViewById(R.id.ib_back);
        ibBack.setImageResource(R.drawable.ic_close_dark);
        ibBack.setVisibility(View.VISIBLE);
        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvAmount=findViewById(R.id.tv_amount);
        tvAmount.setText("GHc "+getProductsAndAmountDue());

          final CardForm cardForm = findViewById(R.id.card_form);

        cardForm.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(true)
                .cardholderName(CardForm.FIELD_REQUIRED)
                .actionLabel("Purchase")
                .setup(this);

          btnSubmit = findViewById(R.id.btn_pay);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(cardForm.isValid()) {
                    pay.cardName=cardForm.getCardholderName();
                    pay.cardNumber=cardForm.getCardNumber();
                    pay.cvv=cardForm.getCvv();
                    pay.yearExpiration=cardForm.getExpirationYear();
                    pay.monthExpiration=cardForm.getExpirationMonth();
                    pay.user=new User(PaymentActivity.this);

        //            getProductsAndAmountDue();

                    makePayment();
                }
                else
                    cardForm.validate();
            }
        });

        cardForm.setOnCardFormValidListener(new OnCardFormValidListener() {
            @Override
            public void onCardFormValid(boolean valid) {
                btnSubmit.performClick();
            }
        });
    }

    private void makePayment(){
        if(myApplication.hasNetworkConnection(this)){
            showProgress(resolveString(R.string.processing));
            networkRequest.makePayment(pay,this);
        }
        else {
            hideProgress();
            myApplication.showInternetError(this);
        }
    }

    private long getProductsAndAmountDue(){
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray=new JSONArray();
        long amountDue=0;
        productList=Checkout.productList;
        if(productList!=null) {
            Log.d(TAG,"size: "+productList.size());
           try {
               for (Product product:productList){
               JSONObject object=new JSONObject();
                   object.put("Key",product.id);
                   object.put("Description",product.description);
                   object.put("Quantity",product.quantity);
                   object.put("Name",product.name);
                   object.put("Image",product.image);
                   object.put("Org",product.image);
                   object.put("Price",product.price);

                   jsonArray.put(object);

                   amountDue+=(product.price*product.quantity);
               }
               jsonObject.put("items",jsonArray);
           }
           catch (JSONException e) {
               e.printStackTrace();
           }
       }

       pay.products=jsonObject;
       pay.amountDue=amountDue;

       return pay.amountDue;
    }

    private String createReceipt(){
        PaymentReceipt receipt=new PaymentReceipt();
        String order_number=String.valueOf(UUID.randomUUID()).substring(0,7);
        receipt.order_number= order_number;
        receipt.amount_paid= String.valueOf(pay.amountDue);
        receipt.date= CreateForm.getInstance(this).getCurrentDate2();
        receipt.order_status="Processing";
        databaseHelper.addReceipt(receipt);
        PushReceiver.showReceiptNotification(this,receipt);

        return order_number;
    }

    @Override
    public void onRespond(int status, Object object) {
        Log.d(TAG,"status: "+status+" object: "+object);
        if(status== NetworkRequest.STATUS_SUCCESS){
            String order_number= createReceipt();
            if(productList!=null){
                for(int a =0;a<productList.size();a++){
                    Product product=productList.get(a);
                    product.status=Product.STATUS_BOUGHT;
                    product.order_number=order_number;
                    databaseHelper.addProduct(product);
                }
                hideProgress();
                MyApplication.showToast(PaymentActivity.this,resolveString(R.string.success), Gravity.CENTER);
                finish();
            }
        }
        else {
            hideProgress();
            MyApplication.showToast(PaymentActivity.this,resolveString(R.string.failed), Gravity.CENTER);
        }
    }
}
