package com.hensongeodata.myagro360v2.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

//import com.hensongeodata.locsmman.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SettingsModel;
import com.wang.avi.AVLoadingIndicatorView;

//import com.hensongeodata.locsmman.MyApplication;
//import com.hensongeodata.locsmman.R;
//import com.hensongeodata.locsmman.model.Keys;

public class ShareLocation extends BaseActivity {

    private static String TAG=ShareLocation.class.getSimpleName();
   // private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;

    //private int maxAccuracy=10;
    private FloatingActionButton fab;
    public static String ACTION_LOCATION="location";

    private boolean fabEnabled=false;
    private boolean locUpdated=false;
    private Handler handler;
    private Runnable runnable;
    private long promptDelay=20000;


    private TextView tvCaption;
    private TextView tvAccuracy;
    private TextView tvLongitude;
    private TextView tvLatitude;
    private TextView tvAltitude;

    private AVLoadingIndicatorView avl_loading;
    private View bgLoading;

    private ShareBroadcastReceiver receiver;
    private IntentFilter filter;
    private int maxAccuracy;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_location);
          Toolbar toolbar = findViewById(R.id.toolbar);


        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getResources().getString(R.string.share_location));
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }

        maxAccuracy=(new SettingsModel(this)).getAccuracy_max();

        receiver=new ShareBroadcastReceiver();
        filter=new IntentFilter(BroadcastCall.BSFRAGMENT);
        registerReceiver(receiver,filter);

        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!fabEnabled){
                    Toast.makeText(ShareLocation.this, "Please ensure Location accuracy is below "+(maxAccuracy+1), Toast.LENGTH_SHORT).show();
                } else {
                    String sharetxt = Keys.SILAPHA+ tvLatitude.getText() + "," + tvLongitude.getText();//passing the message in the textview to sharetxt

                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, sharetxt);//to put the msg into the intent
                    sendIntent.setType("text/plain");
                    startActivity(Intent.createChooser(sendIntent, "Share Location to"));
                }
                }
        });

        initializeViews();

    }


    private void initializeViews() {
        bgLoading=findViewById(R.id.bg_progress);
          avl_loading = findViewById(R.id.avl);
        avl_loading.show();
          tvAccuracy = (findViewById(R.id.accuracy)).findViewById(R.id.tv_value);
        ((TextView)(findViewById(R.id.accuracy)).findViewById(R.id.tv_tag)).setText(resolveString(R.string.accuracy));

          tvLongitude = (findViewById(R.id.longitude)).findViewById(R.id.tv_value);
        ((TextView)(findViewById(R.id.longitude)).findViewById(R.id.tv_tag)).setText(resolveString(R.string.longitude));

          tvLatitude = (findViewById(R.id.latitude)).findViewById(R.id.tv_value);
          TextView tagLatitude = (findViewById(R.id.latitude)).findViewById(R.id.tv_tag);
        tagLatitude.setText(resolveString(R.string.latitude));
//        tagLatitude.setTextColor(getResources().getColor(R.color.primaryBlack));
//        tagLatitude.setVisibility(View.VISIBLE);

          tvAltitude = (findViewById(R.id.altitude)).findViewById(R.id.tv_value);
        ((TextView)(findViewById(R.id.altitude)).findViewById(R.id.tv_tag)).setText(resolveString(R.string.altitude));

    updateLocation();
    }

    private void promptUser(){
    Log.d(TAG,"promptUser locUpdated "+locUpdated);
        if(!locUpdated) {
            Log.d(TAG,"handler "+handler+" runnable "+runnable);
        if(handler!=null&&runnable!=null)
            handler.removeCallbacks(runnable);

        handler = new Handler();
        runnable=new Runnable() {
            @Override
            public void run() {
                if(!locUpdated) {
                    new AlertDialog.Builder(ShareLocation.this)
                            .setTitle("Taking too long?")
                            .setMessage("Please ensure your Mobile data is connected to the internet and your location is enabled.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                    promptUser();
                                }
                            })
                            .setNegativeButton("HIDE THIS", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            })
                            .show();
                }
            }
        };
        handler.postDelayed(runnable, promptDelay);
        promptDelay+=promptDelay;
    }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }



//    android.location.LocationListener locationListenerGPS=new android.location.LocationListener() {
//        @Override
//        public void onLocationChanged(Location location) {
//            Log.d(TAG,"location changed");
//           // updateLocation(location);
//        }
//
//        @Override
//        public void onStatusChanged(String provider, int status, Bundle extras) {
//
//        }
//
//        @Override
//        public void onProviderEnabled(String provider) {
//
//        }
//
//        @Override
//        public void onProviderDisabled(String provider) {
//
//        }
//    };

    private void red(){
     fabEnabled=false;
     fab.setBackgroundColor(getResources().getColor(R.color.red));
    }

//    private void updateLocation(Location location){
//        if(location!=null){
//            avl_loading.hide();
//            bgLoading.setVisibility(View.GONE);
//
//            tvAccuracy.setText(""+MyApplication.locationAccuracy+"m");
//            tvLatitude.setText(""+MyApplication.latitude);
//            tvLongitude.setText(""+location.getLongitude());
//            tvAltitude.setText(""+location.getAltitude());
//
//            int accur= (int) Math.floor(location.getAccuracy());
//            if(accur<=maxAccuracy)
//                    fabEnabled=true;
//            else
//                fabEnabled=false;
//
//            tvAccuracy.setTextColor(MyApplication.getAccuracyColor(ShareLocation.this,accur,maxAccuracy));
//        }
//    }


    private void updateLocation(){
        Log.d(TAG,"updateLocation");
        if(MyApplication.poi!=null) {

            String accuracy=MyApplication.poi.getAccuracy();
            if(accuracy!=null&&!accuracy.equalsIgnoreCase("null")) {

            avl_loading.hide();
            bgLoading.setVisibility(View.GONE);

            tvAccuracy.setText("" + MyApplication.poi.getAccuracy() + "m");
            tvLatitude.setText("" + MyApplication.poi.getLat());
            tvLongitude.setText("" + MyApplication.poi.getLon());
            tvAltitude.setText("" + MyApplication.poi.getAlt());



                int accur = (int) Math.floor(Double.parseDouble(accuracy));
                  fabEnabled = accur <= maxAccuracy;
                tvAccuracy.setTextColor((new BaseActivity()).getAccuracyColor(this,Integer.parseInt(accuracy)));
            }
        }

        }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.menu_share_location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            Toast.makeText(ShareLocation.this,"Refreshing location feed...",Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }



    class ShareBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getExtras().getString("action");
            int status = intent.getExtras().getInt("status");
            Log.d(TAG,"action: "+action);
            if(action.equals(ACTION_LOCATION)) {
                if (status == NetworkRequest.STATUS_SUCCESS) {

                    updateLocation();
                }
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(receiver!=null)
            unregisterReceiver(receiver);
    }
}


