package com.hensongeodata.myagro360v2.view;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import com.hensongeodata.myagro360v2.adapter.POIGroupAdapterRV;
import com.hensongeodata.myagro360v2.model.POIGroup;

/**
 * Created by user1 on 1/25/2018.
 */

public class POIGroupList extends SearchableList {
    private static String TAG=POIGroupList.class.getSimpleName();
    private boolean locked=false;
    private POIGroupAdapterRV adapter;
    private ArrayList<POIGroup> poiGroups;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        recyclerView.setLayoutManager(new GridLayoutManager(this,2));

        fetchPOIGroups();//load archives into list view
        tvCaption.setText("Saved locations");
        etSearch.addTextChangedListener(new MyWatcher(POI_GROUP,adapter));
    }

    private void fetchPOIGroups(){
        showProgress("Loading...");
        poiGroups=new ArrayList<>();
        poiGroups=databaseHelper.getPOIGroupList();
        Log.d(TAG,"poiGroups: "+poiGroups.size());
        if(poiGroups==null||poiGroups.size()<1){
            recyclerView.setVisibility(View.GONE);
            setTvPlaceholder("No Location saved");
        }
        else {
            tvPlaceholder.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter = new POIGroupAdapterRV(this, poiGroups);
            recyclerView.setAdapter(adapter);
        }


        hideProgress();
}


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"onresume");
        fetchPOIGroups();
        //adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myApplication.selectPOIGroup(this,databaseHelper);
    }
}
