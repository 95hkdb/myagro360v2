package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.Country;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.Region;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 10/13/2017.
 */

public class District_backup2 extends BaseActivity{
    private static String TAG=District_backup2.class.getSimpleName();
    private  TextView tvCaption;
    private ListView listView;
    private ArrayAdapter<String> adapter;
    private EditText etSearch;
    private View bgDistrict;
    private View bgProgress;
    private ProgressBar progressBar;
    private TextView tvProgress;
    private View container;
    private View retryBg;

    private static int LOAD_COUNTRY=100;
    private static int LOAD_REGION=200;
    private static int LOAD_DISTRICT=300;
    private static int LOAD_TOWN=400;
    private int mAction;
    private String newDistrict=null;
    private Object objID;//either country_id returned or region_id returned from server to use to fetch the subsequent list itemsVisible

    private ProgressDialog progressDialog;
    DistrictReceiver receiver;
    IntentFilter filter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.district);
        MyApplication.initLists();

        receiver=new DistrictReceiver();
        filter=new IntentFilter(BroadcastCall.DISTRICT_LOAD_UPDATE);
        registerReceiver(receiver,filter);

        progressDialog=new ProgressDialog(this);
        progressDialog.setCancelable(false);

        bgDistrict=findViewById(R.id.bg_district);
          tvCaption = findViewById(R.id.tv_caption);
        (findViewById(R.id.ib_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {finish();}
        });
        bgProgress =findViewById(R.id.bg_progressbar);
          progressBar = findViewById(R.id.progressbar);
          tvProgress = findViewById(R.id.tv_progress);
        container=findViewById(R.id.container);
        retryBg=findViewById(R.id.bg_retry);
        retryBg.setOnClickListener(doRetry);
        (findViewById(R.id.btn_retry)).setOnClickListener(doRetry);
          listView = findViewById(R.id.listview);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d("District","onitemclick");
                Log.d("District",(String) listView.getItemAtPosition(i));
                doItemSelected((String) listView.getItemAtPosition(i));
            }
        });

          etSearch = findViewById(R.id.et_search);
        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               if(District_backup2.this.adapter!=null)
                    District_backup2.this.adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //Start by loading the list of countries
       loadCountries();
        //Start by loading the list of regions in Ghana
       // loadRegions("b21");

        findViewById(R.id.bg_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user.setTown_id(null);
                user.setWorkarea(etSearch.getText().toString());
                save();
            }
        });
    }

    private void doItemSelected(String selection){
        if(mAction==LOAD_COUNTRY){
              int index = MyApplication.sCountryList.indexOf(selection);
            if(index!=-1) {
                  String countryID = MyApplication.sCountryIDList.get(index);
                Log.d(TAG,"countryID "+countryID);
                loadRegions(countryID);
                objID=countryID;
            }
            else {
                setBackground(false,false,true);
            }
        }
        else if(mAction==LOAD_REGION){
              int index = MyApplication.sTownList.indexOf(selection);
            if(index!=-1) {
                  String townID = MyApplication.sTownIDList.get(index);
                user.setTown_id(townID);
            }
            user.setWorkarea(selection);
            save();

//            int index=myApplication.sRegionList.indexOf(selection);
//            if(index!=-1) {
//                String regionID = myApplication.sRegionIDList.get(index);
//                Log.d(TAG,"region id "+regionID);
//                loadDistricts(regionID);
//                objID=regionID;
//            }
//            else {
//                setBackground(false,false,true);
//            }
        }
        else if(mAction==LOAD_DISTRICT){
              int index = MyApplication.sDistrictList.indexOf(selection);
            if(index!=-1) {
                  String districtID = MyApplication.sDistrictIDList.get(index);
                newDistrict = selection;
               // Log.d("District","districtID "+districtID);
                loadTowns(districtID);
                objID=districtID;
            }
            else {
                setBackground(false,false,true);
            }
        }

        else if(mAction==LOAD_TOWN) {
              int index = MyApplication.sTownList.indexOf(selection);
            if(index!=-1) {
                  String townID = MyApplication.sTownIDList.get(index);
                user.setTown_id(townID);
            }
                user.setWorkarea(selection);
                save();
        }

    }

    private void loadCountries(){
        tvCaption.setText("Select Your Country");
        mAction=LOAD_COUNTRY;
        setBackground(true,false,false);
          if (MyApplication.countryList != null && MyApplication.countryList.size() > 0) {
           // myApplication.countryList=databaseHelper.getAllItems(DatabaseHelper.COUNTRY_TBL,"-1");
            Country country=new Country();
                for (int a = 0; a < MyApplication.countryList.size(); a++) {
                      country = MyApplication.countryList.get(a);
                      MyApplication.sCountryList.add(country.getName());
                      MyApplication.sCountryIDList.add(country.getId());
            }
            BroadcastCall.publishDistrict(this,NetworkRequest.STATUS_SUCCESS,"success");
        }
        else if(myApplication.hasNetworkConnection(this)){
            networkRequest.fetchCountries();
            etSearch.setText(null);
        }
        else {
            myApplication.showInternetError(this);
            setBackground(false,false,true);
        }
    }
    private void loadRegions(String countryID){
        tvCaption.setText("Select Your Region");
        mAction=LOAD_REGION;
        setBackground(true,false,false);
          if (MyApplication.regionList != null && MyApplication.regionList.size() > 0) {
                MyApplication.regionList = databaseHelper.getAllItems(DatabaseHelper.REGION_TBL, countryID);
            Region region;
                for (int a = 0; a < MyApplication.regionList.size(); a++) {
                      region = MyApplication.regionList.get(a);
                      MyApplication.sRegionList.add(region.getName());
                      MyApplication.sRegionIDList.add(region.getId());
            }
            BroadcastCall.publishDistrict(this,NetworkRequest.STATUS_SUCCESS,"success");
        }
        else if(myApplication.hasNetworkConnection(this)){
            //this.countryID=countryID;//saving countryID globally
            networkRequest.fetchRegions(countryID);
            etSearch.setText(null);
        }
        else {
            myApplication.showInternetError(this);
            setBackground(false,false,true);
        }
    }
    private void loadDistricts(String regionID){
        tvCaption.setText("Select Your District");
        mAction=LOAD_DISTRICT;
        setBackground(true,false,false);
        if(myApplication.hasNetworkConnection(this)){
            //this.regionID=regionID;
            networkRequest.fetchDistricts(regionID);
            etSearch.setText(null);
        }
        else {
            myApplication.showInternetError(this);
            setBackground(false,false,true);
        }
    }
    private void loadTowns(String districtID){
        tvCaption.setText("Loading Towns...");
        mAction=LOAD_TOWN;
        setBackground(true,false,false);
        if(myApplication.hasNetworkConnection(this)){
            //this.districtID=districtID;
            networkRequest.fetchTowns(districtID);
            etSearch.setText(null);
        }
        else {
            myApplication.showInternetError(this);
            setBackground(false,false,true);
        }
    }

    private void setBackground(boolean loading,boolean displayList,boolean retry){
        if(mAction==LOAD_TOWN){
            progressDialog.setMessage("Loading towns, Please wait...");

            if(loading) progressDialog.show(); else if(progressDialog.isShowing()) progressDialog.hide();

            container.setVisibility(displayList ? View.VISIBLE : View.GONE);
            retryBg.setVisibility(retry ? View.VISIBLE : View.GONE);
        }
        else {
            bgProgress.setVisibility(loading ? View.VISIBLE : View.GONE);
            progressBar.setVisibility(mAction != LOAD_TOWN ? View.VISIBLE : View.GONE);
            tvProgress.setVisibility(mAction == LOAD_TOWN ? View.VISIBLE : View.GONE);
            container.setVisibility(displayList ? View.VISIBLE : View.GONE);
            retryBg.setVisibility(retry ? View.VISIBLE : View.GONE);
        }
        }

    private View.OnClickListener doRetry=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(mAction==LOAD_COUNTRY){
                loadCountries();
            }
            else if(mAction==LOAD_REGION){
                loadRegions((String) objID);
            }
            else if(mAction==LOAD_DISTRICT){
                loadDistricts((String) objID);
            }
            else if(mAction==LOAD_TOWN){
                loadTowns((String)objID);
            }
        }
    };

    private void save(){

        Intent i=new Intent();
        setResult(Activity.RESULT_OK,i);
        finish();
    }

    class DistrictReceiver extends BroadcastReceiver {
        private String TAG=DistrictReceiver.class.getSimpleName();
        @Override
        public void onReceive(Context context, Intent intent) {

            String message=intent.getExtras().getString("date");
            int status=intent.getExtras().getInt("status");
            if(status==NetworkRequest.STATUS_SUCCESS) {
                if(mAction==LOAD_REGION){
                    if(progressDialog.isShowing())
                        progressDialog.hide();
                    ArrayList<String> regions;
                      if (MyApplication.sRegionList != null)
                            regions = MyApplication.sRegionList;
                    else
                        regions=new ArrayList<>();

                    List<String> list=new ArrayList<>();

                    for(int a=0; a<regions.size();a++){
                        if(regions.get(a)!=null) {
                            String townName = regions.get(a).toLowerCase();
                              townName = townName.substring(0, 1).toUpperCase() + townName.substring(1);
                            list.add(townName);
                        }
                    }
                    adapter=new ArrayAdapter<String>(District_backup2.this,android.R.layout.simple_list_item_1,list);

                    listView.setAdapter(adapter);
                    setBackground(false,true,false);

                    if(newDistrict!=null)
                        myState.saveString(Keys.SP_STR_DISTRICT,newDistrict);

                    findViewById(R.id.v_save_container).setVisibility(View.VISIBLE);
                   etSearch.setHint("Enter (Select) region name here...");
                   tvCaption.setText("Regions");

                    // Toast.makeText(District.this,"Towns updated successfully. ",Toast.LENGTH_SHORT).show();
                }
                else {
                    if(mAction==LOAD_COUNTRY)
                          adapter = new ArrayAdapter<String>(District_backup2.this, android.R.layout.simple_list_item_1, MyApplication.sCountryList);
                    else  if(mAction==LOAD_REGION)
                          adapter = new ArrayAdapter<String>(District_backup2.this, android.R.layout.simple_list_item_1, MyApplication.sRegionList);
                    else if(mAction==LOAD_DISTRICT)
                          adapter = new ArrayAdapter<String>(District_backup2.this, android.R.layout.simple_list_item_1, MyApplication.sDistrictList);

                    listView.setAdapter(adapter);
                    setBackground(false,true,false);
                }
            }
            else {
                if(progressDialog.isShowing())
                    progressDialog.hide();
                //Failed to load list
                setBackground(false,false,true);
                Toast.makeText(District_backup2.this,"Failed to load Items. ",Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        databaseHelper.close();
        unregisterReceiver(receiver);
    }
}
