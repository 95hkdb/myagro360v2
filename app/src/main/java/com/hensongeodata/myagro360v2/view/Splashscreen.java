package com.hensongeodata.myagro360v2.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.service.WeatherService;
import com.hensongeodata.myagro360v2.version2_0.WalkthroughActivity;

/**
 * Created by user1 on 3/7/2018.
 */

public class Splashscreen extends AppCompatActivity {
      Handler handler;
      Runnable runnable;
      MyApplication myApplication;

      @Override
      protected void onCreate(@Nullable Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);
            setContentView(R.layout.splash);

            init();
      }

      private void init() {
            myApplication = new MyApplication();

            if (myApplication.hasSession(getApplicationContext())) {
                  startService(new Intent(this, WeatherService.class));
//                  Intent intent = new Intent(getApplicationContext(), MenuDashboard.class);
                  Intent intent = new Intent(getApplicationContext(), MainHomePageActivity.class);
//                  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                  startActivity(intent);
                  finish();
            } else {
                  Intent intent = new Intent(getApplicationContext(), WalkthroughActivity.class);
//                  Intent intent = new Intent(Splashscreen.this, SigninSignup.class);
                  startActivity(intent);
                  finish();
            }
      }

      @Override
      protected void onDestroy() {
            super.onDestroy();
            if (handler != null && runnable != null)
                  handler.removeCallbacks(runnable);
      }

}
