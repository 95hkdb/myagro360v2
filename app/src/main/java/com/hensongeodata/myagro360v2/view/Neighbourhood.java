package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.MyState;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.User;

/**
 * Created by user1 on 10/13/2017.
 */

public class Neighbourhood extends AppCompatActivity{
    private static String TAG=Neighbourhood.class.getSimpleName();
    private  TextView tvCaption;
    private ListView listView;
    private ArrayAdapter<String> adapter;
      ArrayList<com.hensongeodata.myagro360v2.model.Neighbourhood> neighbourhoods;
    private EditText etSearch;
    private View bgDistrict;
    private View bgProgress;
    private ProgressBar progressBar;
    private TextView tvProgress;
    private View container;
    private View retryBg;
    private int mAction=-1;
    private ArrayList<String> mList;
    private static int LOAD_NEIGHBOURHOOD =400;

    private ProgressDialog progressDialog;
    private MyApplication myApplication;
    private DatabaseHelper databaseHelper;
    private NetworkRequest networkRequest;
    private MyState myState;
    private User user;

    private NeighbourhoodBroadcastReceiver receiver;
    private IntentFilter filter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.neighbourhood);
        myApplication=new MyApplication();
        //MyApplication.initLists();
        user=new User(Neighbourhood.this);
        databaseHelper=new DatabaseHelper(this);
        networkRequest=new NetworkRequest(this);
        myState=new MyState(this);

        receiver=new NeighbourhoodBroadcastReceiver();
        filter=new IntentFilter(BroadcastCall.UPDATE_NEIGHBOURHOOD);
        registerReceiver(receiver,filter);

        progressDialog=new ProgressDialog(this);
        progressDialog.setCancelable(false);

        bgDistrict=findViewById(R.id.bg_district);
          tvCaption = findViewById(R.id.tv_caption);
        tvCaption.setText("Choose Neighbourhood");
          findViewById(R.id.ib_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {finish();}
        });
        bgProgress =findViewById(R.id.bg_progressbar);
          progressBar = findViewById(R.id.progressbar);
          tvProgress = findViewById(R.id.tv_progress);
        container=findViewById(R.id.container);
        retryBg=findViewById(R.id.bg_retry);
        retryBg.setVisibility(View.GONE);
          findViewById(R.id.btn_retry).setVisibility(View.GONE);
          listView = findViewById(R.id.listview);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                doItemSelected((String) listView.getItemAtPosition(i));
            }
        });

          etSearch = findViewById(R.id.et_search);
        etSearch.setHint("Type neighbourhood here...");
        etSearch.addTextChangedListener(new TextWatcher() {


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               if(Neighbourhood.this.adapter!=null)
                    Neighbourhood.this.adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        if(user.getTown_id()!=null){
         downloadNeighbourhoods(user.getTown_id());
        }
        else {
            neighbourhoods = databaseHelper.getAllItems(DatabaseHelper.NEIGHBOURHOOD_TBL, "-1");

            if(neighbourhoods!=null&&neighbourhoods.size()>0) {
                loadNeighbourhoods();
            }
            else {
                if(user.getTown_id()!=null)
                    downloadNeighbourhoods(user.getTown_id());
            }
        }



        (findViewById(R.id.bg_save)).setOnClickListener(saveListener);
        findViewById(R.id.tv_save).setOnClickListener(saveListener);
    }

    private void downloadNeighbourhoods(String townID){
            tvCaption.setText("Loading Neighbourhoods...");
            mAction=LOAD_NEIGHBOURHOOD;
            setBackground(true,false,false);
            if(myApplication.hasNetworkConnection(this)){
                networkRequest.fetchNeighbourhood(townID);
                etSearch.setText(null);
            }
            else {
                myApplication.showInternetError(this);
                setBackground(false, false, true);
            }
    }

    private void doItemSelected(String selection){
        etSearch.setText(selection);
        save();
    }


    private void loadNeighbourhoods(){
        tvCaption.setText("Choose Neighbourhood");
        List<String> list=new ArrayList<>();
        for(int a=0; a<neighbourhoods.size();a++){
            String townName=neighbourhoods.get(a).getName().toLowerCase();
              townName = townName.substring(0, 1).toUpperCase() + townName.substring(1);
            list.add(townName);
        }
        adapter=new ArrayAdapter<String>(Neighbourhood.this,android.R.layout.simple_list_item_1,list);

        listView.setAdapter(adapter);
        setBackground(false,true,false);
    }

    private void setBackground(boolean loading,boolean displayList,boolean retry){
        if(mAction== LOAD_NEIGHBOURHOOD){
            progressDialog.setMessage("Loading neighbourhoods, Please wait...");

            if(loading) progressDialog.show(); else if(progressDialog.isShowing()) progressDialog.hide();

            container.setVisibility(displayList ? View.VISIBLE : View.GONE);
            retryBg.setVisibility(retry ? View.VISIBLE : View.GONE);
        }
        else {
            bgProgress.setVisibility(loading ? View.VISIBLE : View.GONE);
            progressBar.setVisibility(mAction != LOAD_NEIGHBOURHOOD ? View.VISIBLE : View.GONE);
            tvProgress.setVisibility(mAction == LOAD_NEIGHBOURHOOD ? View.VISIBLE : View.GONE);
            container.setVisibility(displayList ? View.VISIBLE : View.GONE);
            retryBg.setVisibility(retry ? View.VISIBLE : View.GONE);
        }
        }


    private View.OnClickListener saveListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           save();
        }
    };

    private void save(){
        user.setNeighbourhood(etSearch.getText().toString().trim());
        Intent i=new Intent();
        setResult(Activity.RESULT_OK,i);
        finish();
    }


    class NeighbourhoodBroadcastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            int status=intent.getExtras().getInt("status");
            if(status==NetworkRequest.STATUS_SUCCESS) {
                tvCaption.setText("Choose Neighbourhood");
                setBackground(false, true, false);
                neighbourhoods = databaseHelper.getAllItems(DatabaseHelper.NEIGHBOURHOOD_TBL, "-1");
                loadNeighbourhoods();
            }
            else if(status==NetworkRequest.STATUS_FAIL){
                tvCaption.setText("Enter Neighbourhood");
                setBackground(false, true, false);
            }
            else {
                tvCaption.setText("Enter Neighbourhood");
                setBackground(false, true, false);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
        databaseHelper.close();
    }
}
