package com.hensongeodata.myagro360v2.view;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageButton;

import com.hensongeodata.myagro360v2.Interface.ProductsListener;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Product;

import java.util.List;

public class BrowseToBuy extends BaseActivity implements ProductsListener {
private static String TAG=BrowseToBuy.class.getSimpleName();
//private EditText etSearch;
//private RecyclerView recyclerView;
//private ProductAdapterRV adapter;
//private List<Product> productList;
//private int retried=0;
//private String category="fertilizer";
//private Button btnFertilizer;
//private Button btnSeed;
//private Button btnPesticide;
//private View vCart;
//private View vCount;
//private TextView tvCount;
//private static int CHECKOUT=1000;
//private TextView tvNothingToDisplay;

    EditText ed_url;
    ImageButton ibut_back;
    SwipeRefreshLayout swipe_shop;
    WebView web_shop;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.browse_to_buy);
//        tvNothingToDisplay=findViewById(R.id.tv_placeholder);
//        vCart=findViewById(R.id.v_cart);
//        vCount=findViewById(R.id.v_count);
//        tvCount=(TextView) vCart.findViewById(R.id.tv_count);
//        recyclerView=findViewById(R.id.recyclerview);
//        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
//
//        btnFertilizer=(Button)findViewById(R.id.btn_fertilizer);
//        btnFertilizer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onAddMapButtonClicked(View view) {
//                enableTab((Button)view);
//                if(!category.equalsIgnoreCase("fertilizer")){
//                    if(productList!=null){
//                        productList.clear();
//                        if(adapter!=null)adapter.notifyDataSetChanged();
//                        tvNothingToDisplay.setVisibility(View.VISIBLE);
//                    }
//                }
//                category="fertilizer";
//                loadItems();
//            }
//        });
//
//        btnSeed=(Button)findViewById(R.id.btn_seed);
//        btnSeed.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onAddMapButtonClicked(View view) {
//                enableTab((Button)view);
//                if(!category.equalsIgnoreCase("seed")){
//                    if(productList!=null){
//                        productList.clear();
//                        if(adapter!=null)adapter.notifyDataSetChanged();
//                        tvNothingToDisplay.setVisibility(View.VISIBLE);
//                    }
//                }
//                category="seed";
//                //startActivity(new Intent(BrowseToBuy.this,Checkout.class));
//                loadItems();
//            }
//        });
//
//        btnPesticide=(Button)findViewById(R.id.btn_pesticide);
//        btnPesticide.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onAddMapButtonClicked(View view) {
//                enableTab((Button)view);
//                if(!category.equalsIgnoreCase("all")){
//                    if(productList!=null){
//                        productList.clear();
//                        if(adapter!=null)adapter.notifyDataSetChanged();
//                        tvNothingToDisplay.setVisibility(View.VISIBLE);
//                    }
//                }
//                category="all";
//                loadItems();
//            }
//        });
//
//
//        btnFertilizer.performClick();
//
//        etSearch=(EditText)findViewById(R.id.et_search);
//        etSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                adapter.search(charSequence.toString());
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//
//        vCart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onAddMapButtonClicked(View view) {
//                Log.d("BrowseToBuy","vCart onclick");
//                startActivityForResult(new Intent(BrowseToBuy.this,Checkout.class),CHECKOUT);
//            }
//        });
//
//        findViewById(R.id.v_count).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onAddMapButtonClicked(View view) {
//                vCart.performClick();
//            }
//        });

//        findViewById(R.id.ib_back).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onAddMapButtonClicked(View view) {
//                finish();
//            }
//        });


//        findViewById(R.id.ib_receipts).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onAddMapButtonClicked(View view) {
//                startActivity(new Intent(BrowseToBuy.this,Receipts.class));
//            }
//        });

        init();
    }

    private void init(){

        ed_url = findViewById(R.id.shop_url_edit);
        ibut_back = findViewById(R.id.ib_back);
        swipe_shop = findViewById(R.id.shop_swipe);
        web_shop = findViewById(R.id.shop_webview);
        ed_url.setText("https://asime.online");

        ibut_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(getApplicationContext(), MenuDashboard.class));
            }
        });

        swipe_shop.setColorSchemeResources(R.color.colorOrange10, R.color.colorBlue20,R.color.colorGreen40,R.color.colorRed40);
        swipe_shop.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                WebAction();
            }
        });

        WebAction();

    }

    public void WebAction(){

        WebSettings webSettings = web_shop.getSettings();
        webSettings.setBuiltInZoomControls(true);
        webSettings.setJavaScriptEnabled(true);
        web_shop.getSettings().setAppCacheEnabled(true);
        //webView.loadUrl("https://silapha.com/");
        String url = ed_url.getText().toString();
        web_shop.loadUrl(url);
        swipe_shop.setRefreshing(true);
        web_shop.setWebViewClient(new WebViewClient(){

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

//                webView.loadUrl(String.valueOf(R.drawable.offline_first_logo));
            }

            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                swipe_shop.setRefreshing(false);
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if(resultCode== Activity.RESULT_OK&& requestCode==CHECKOUT){
//            int response=data.getIntExtra("response",0);
//            if(response==1){
//                btnFertilizer.performClick();//reload items to get new max limit
//            }
//        }
    }

//    private void enableTab(Button button){
//        disableTabs();
//        button.setBackground(getResources().getDrawable(R.drawable.bg_scout_negative));
//        button.setTextColor(getResources().getColor(R.color.primaryBlack));
//    }
//
//    private void disableTabs(){
//        btnFertilizer.setBackground(getResources().getDrawable(R.drawable.bg_scout_ok));
//        btnFertilizer.setTextColor(getResources().getColor(R.color.primaryWhite));
//        btnSeed.setBackground(getResources().getDrawable(R.drawable.bg_scout_ok));
//        btnSeed.setTextColor(getResources().getColor(R.color.primaryWhite));
//        btnPesticide.setBackground(getResources().getDrawable(R.drawable.bg_scout_ok));
//        btnPesticide.setTextColor(getResources().getColor(R.color.primaryWhite));
//    }
//
//    private void loadItems(){
//        if(myApplication.hasNetworkConnection(this)){
//            showProgress("Loading "+category);
//            networkRequest.getProducts(category,this);
//        }
//        else {
//            hideProgress();
//            myApplication.showInternetError(this);
//        }
//    }
//
//    private void showProducts(List<Product> items){
//        if(items!=null) {
//            productList=items;
//            adapter = new ProductAdapterRV(this, productList);
//            recyclerView.setAdapter(adapter);
//        }
//    }

    @Override
    public void loaded(int status, List<Product> products) {
//       Log.d(TAG,"status: "+status);
//        // products=dummy(category);
//        status=NetworkRequest.STATUS_SUCCESS;
//        if(status== NetworkRequest.STATUS_SUCCESS) {
//            if(products!=null&&products.size()>0) {
//                tvNothingToDisplay.setVisibility(View.GONE);
//                showProducts(products);
//            }
//            else {
//                tvNothingToDisplay.setVisibility(View.VISIBLE);
//                myApplication.showMessage(BrowseToBuy.this,"No items found");
//            }
//        }else{
//            tvNothingToDisplay.setVisibility(View.VISIBLE);
//            Log.d(TAG,"retried: "+retried);
//            if(retried<3) {
//                retried++;
//                loadItems();
//                MyApplication.showToast(BrowseToBuy.this, "Retrying", Gravity.CENTER);
//            }else {
//                retried=0;
//                MyApplication.showToast(BrowseToBuy.this, "No items received from server", Gravity.CENTER);
//            }
//        }
//
//        hideProgress();
    }

    @Override
    public void onResume() {
        super.onResume();
//        ArrayList<Product> prods=databaseHelper.getProducts(Product.STATUS_CART);
//        if(prods!=null&&prods.size()>0){
//            int cart_size=prods.size();
//            if(cart_size<100)
//                tvCount.setText(""+cart_size);
//            else
//                tvCount.setText("99+");
//            vCount.setVisibility(View.VISIBLE);
//
//        }
//        else {
//            tvCount.setText("0");
//        }
    }

    /*private List<Product> dummy(String category){
        List<Product> products=new ArrayList<>();
        String[]urls={"https://hital360.com/assets/InvUploads/c148864129/fertilizer-bag.jpg",
                "https://hital360.com/assets/InvUploads/79f332c3bc/fertilizer.jpg",
                "https://hital360.com/assets/InvUploads/bd27949b4d/fertilizer-bags-.jpg"};

        String[]names={"Zico",
                "Octape",
                "Ollofo",
                "https://hital360.com/assets/InvUploads/c148864129/fertilizer-bag.jpg"};

        String[]descriptions={"An NDK Fertilizerr",
                "A BCA Fertilizer",
                "An MCF Fertilizer"};

        long[]prices={50,
                100,
                100};

        for(int a=0;a<3;a++){
            Product product=new Product();
            product.name=names[a];
            product.description= descriptions[a];
            //product.description=product.description+" "+product.description;
            product.price=prices[a];
            product.id=""+a;
            product.category=category;
            product.owner_name=urls[a];
           *//* if(a%2==0)
                product.image=R.drawable.fert_one;
            else
                product.image=R.drawable.fert_two;
*//*

            POI poi=new POI();
            poi.setName("Ashale Botwe");
            poi.setAccuracy("5");

            product.poi=poi;

            products.add(product);
        }

        return products;
    }*/
}
