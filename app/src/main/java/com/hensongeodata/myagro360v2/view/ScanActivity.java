package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.hensongeodata.myagro360v2.Interface.PercentageListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.adapter.PesticideAdapterRV;
import com.hensongeodata.myagro360v2.api.request.LocsmmanProAPI;
import com.hensongeodata.myagro360v2.api.response.ScanResponse;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.LinearLayoutManagerWithSmoothScroller;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.controller.ProgressAnimation;
import com.hensongeodata.myagro360v2.controller.RetrofitHelper;
import com.hensongeodata.myagro360v2.controller.Utility;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.ScanFAW;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.model.User;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Part;
import retrofit2.http.Query;

import static com.hensongeodata.myagro360v2.controller.RetrofitHelper.createPartFromString;
import static com.hensongeodata.myagro360v2.fragment.MediaFragment.ACTION_HIDE_ALL;
import static com.hensongeodata.myagro360v2.fragment.MediaFragment.ACTION_SHOW_ALL;
import static com.hensongeodata.myagro360v2.fragment.MediaFragment.ACTION_SHOW_PROGRESS;


public class ScanActivity extends BaseActivity {
      private static String TAG = ScanActivity.class.getSimpleName();
      private static int REQUEST_SCANNER = 100;
      private ScanReceiver receiver;
      private IntentFilter filter;
      private View vNotice;
      private ImageView ivNotice;
      private View bgNotice;
      private TextView tvHeader;
      private TextView tvMessage;
      private ScanFAW scanFAW;
      public User user;
      SharePrefManager sharePrefManager;
      private String NOTICE_MESSAGE = "";
      public static int FAW_DETECTION_TRUE = 1;
      public static int FAW_DETECTION_FALSE = 0;
      private int index = 0;
      private TextView tvLocation;
      LinearLayout notifyLayout;
      private String NOTICE_HEADER = "ScanActivity";

      private TextView tvPercentage;
      private TextView tvScanning;
      private ProgressBar vProgress;
      private View bgProgress;
      // private Handler handler;
      // private Runnable runnable;
      private Vibrator vibrator;
      private boolean isCancelled = false;
      protected FloatingActionButton fabCapture;
      protected ImageButton ibAction;
      protected View wrapper;
      protected ImageView imageView;
      protected AVLoadingIndicatorView progressBar;
      protected View vMenu;
      private Dialog dialogPesticide;
      private Button btnOverride;
      private Button btnShare;
      private CountDownTimer countDownTimer;
      private RoundCornerProgressBar roundCornerProgressBar;

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.scan);

            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(resolveString(R.string.scan_faw));
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        finish();
//                        startActivity(new Intent(getApplicationContext(), MenuDashboard.class));
                        startActivity(new Intent(getApplicationContext(), MainHomePageActivity.class));
                  }
            });


            if (MyApplication.poi != null){
                  String gps = MyApplication.poi.getLat() + "," + MyApplication.poi.getLon();

            Log.d(TAG, "gps: " + gps);
      }



            sharePrefManager = new SharePrefManager(this);

            receiver = new ScanReceiver();
            filter = new IntentFilter(BroadcastCall.BSFRAGMENT);
            registerReceiver(receiver, filter);

            initPesticideDialog();

            wrapper = findView(wrapper, R.id.bg_wrapper);
            fabCapture = (FloatingActionButton) findView(fabCapture, R.id.fab_capture);
            ibAction = (ImageButton) findView(ibAction, R.id.ib_action);
            imageView = (ImageView) findView(imageView, R.id.iv_image);
            progressBar = (AVLoadingIndicatorView) findView(progressBar, R.id.avl);
            progressBar.hide();
            progressBar.setVisibility(View.GONE);

            bgProgress = findViewById(R.id.bg_progress);
            bgProgress.setVisibility(View.GONE);

            scanFAW = new ScanFAW(this);
            vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

            MyApplication.scanFAWList.add(scanFAW);
            index = MyApplication.scanFAWList.size() - 1;

            tvLocation = findViewById(R.id.tv_location);
            fabCapture.setOnClickListener(scanImageListener);
            fabCapture.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_scan_white));

            vMenu = findViewById(R.id.v_faw_menu);
            vNotice = findViewById(R.id.v_faw_notice);
            vNotice.setVisibility(View.VISIBLE);
            ivNotice = vNotice.findViewById(R.id.iv_notice);
            ivNotice.setImageResource(R.drawable.ic_information);
            tvHeader = vNotice.findViewById(R.id.tv_header);
            tvMessage = vNotice.findViewById(R.id.tv_message);
            bgNotice = findViewById(R.id.v_notice);
            btnOverride = findViewById(R.id.btn_override);
            btnShare = findViewById(R.id.btn_share);

            btnOverride.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        vMenu.setVisibility(View.VISIBLE);
                        btnOverride.setVisibility(View.GONE);
                  }
            });

            if (btnShare != null)
                  btnShare.setVisibility(View.GONE);
            else
                  btnShare.setVisibility(View.GONE);

            btnShare.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        share();
                  }
            });


            vMenu.findViewById(R.id.btn_one).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
//                        Intent intent = new Intent(getApplicationContext(), MenuDashboard.class);
                        Intent intent = new Intent(getApplicationContext(), MainHomePageActivity.class);
                        intent.putExtra("learn", "openLearn");
                        startActivity(intent);
                  }
            });

            vMenu.findViewById(R.id.btn_two).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        //dialogPesticide.show();
                        startActivity(new Intent(ScanActivity.this, BrowseToBuy.class));
                  }
            });

            vMenu.findViewById(R.id.btn_three).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        openDailer();
                  }
            });

            openScanner();

            updateScannerStatus();

            // imageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.placeholder));
            Picasso.get().load(R.drawable.placeholder).resize(500, 500).into(imageView);

        /*handler=new Handler();
        runnable=new Runnable() {
            @Override
            public void run() {
                onScanImageResult();
            }
        };*/

            vProgress = findViewById(R.id.v_progress);
            roundCornerProgressBar = findViewById(R.id.rcp);
            roundCornerProgressBar.setProgress(0);
            vProgress.setProgress(0);
            vProgress.setMax(10000);
            tvPercentage = findViewById(R.id.tv_percentage);
            tvScanning = findViewById(R.id.tv_tag);

            setNotice();

            // vMenu.setVisibility(View.VISIBLE);
      }

      private View findView(View v, int id) {
            v = findViewById(id);
            return v;
      }

      View.OnClickListener scanImageListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                  openScanner();
            }
      };


      @Override
      public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
            Log.d(TAG, "status: " + Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            switch (requestCode) {
                  case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                              openScanner();
                        } else {
                              //code for deny
                              Toast.makeText(this, resolveString(R.string.permission_required), Toast.LENGTH_SHORT).show();
                        }
                        break;
                  default:
                        return;
            }
      }

      private void openScanner() {
            if (MyApplication.hasLocation()) {
                  if (myApplication.hasNetworkConnection(this)) {
                        MyCamera.action = MyCamera.ACTION_SCAN;
                        Intent i = new Intent(this, CameraScan.class);
                        i.putExtra("index", index);
                        startActivityForResult(i, REQUEST_SCANNER);
                  } else {
                        myApplication.showInternetError(this);
                  }
            } else {
                  MyApplication.showToast(this, resolveString(R.string.location_generating), Gravity.CENTER);
            }
      }

    /*private void onScanImageResult(String path) {
        if(path!=null&&!path.trim().isEmpty()) setImage(path);
        else Toast.makeText(this,resolveString(R.string.image_failed),Toast.LENGTH_SHORT).show();

        setNotice();
    }*/

 /*   private void onScanImageResult() {
        scanFAW=MyApplication.scanFAWList.get(index);
        Image image=scanFAW.getImage();
        if(image!=null&&image.getUri()!=null&&image.getUri().getPath()!=null
                &&!image.getUri().getPath().trim().isEmpty())
            setImage(image.getUri().getPath());
        else {

        }
            setNotice();
    }*/

      private void setNotice() {
            bgNotice.setVisibility(View.VISIBLE);
            fabCapture.setVisibility(View.VISIBLE);
            bgProgress.setVisibility(View.GONE);
            if (countDownTimer != null) countDownTimer.cancel();
            MyApplication.scanFAWList.set(index, scanFAW);
            tvHeader.setVisibility(View.VISIBLE);
            tvHeader.setText(scanFAW.getHeader());

            Log.e(TAG, scanFAW.getHeader());

            tvMessage.setText(scanFAW.getMessage());
            updateNoticeTheme();
      }

      private void updateNoticeTheme() {
            switch (scanFAW.getResponse()) {
                  case -1:
                        setNoticeTheme(R.color.disabledBlack);
                        tvLocation.setVisibility(View.VISIBLE);
                        vMenu.setVisibility(View.GONE);
                        btnOverride.setVisibility(View.GONE);
                        break;
                  case 0:
                        setNoticeTheme(R.color.colorAccent);
                        tvLocation.setVisibility(View.GONE);
                        vMenu.setVisibility(View.GONE);
                        btnShare.setVisibility(View.GONE);
                        btnOverride.setVisibility(View.VISIBLE);
                        break;
                  case 1:
                        setNoticeTheme(R.color.red);
                        tvLocation.setVisibility(View.GONE);
                        vMenu.setVisibility(View.VISIBLE);
                        btnShare.setVisibility(View.GONE);
                        btnOverride.setVisibility(View.GONE);
                        break;
                  default:
                        setNoticeTheme(R.color.disabledBlack);
                        tvLocation.setVisibility(View.GONE);
                        btnShare.setVisibility(View.GONE);
                        vMenu.setVisibility(View.GONE);
                        break;
            }
      }

      private void setNoticeTheme(int color) {
            tvHeader.setTextColor(getResources().getColor(color));
            tvMessage.setTextColor(getResources().getColor(color));
            ivNotice.setColorFilter(getResources().getColor(color));
      }

      private void setImage(String path) {
            if (btnShare != null) btnShare.setVisibility(View.VISIBLE);
            Log.d(TAG, "path: " + path);
            Image image = new Image();
            image.setImageBitmap(myApplication.bitmapCompressLarge(path));
            if (image.getImageBitmap() != null) {
                  image.setImageString(MyApplication.encodeBitmap(image.getImageBitmap()));
                  imageView.setImageBitmap(image.getImageBitmap());
                  image.setImage_url(path);
                  image.setUri(Uri.parse(path));
                  scanFAW.setImage(image);
            }

      }

      private void updateScannerStatus() {
            if (MyApplication.hasLocation()) {
                  tvLocation.setText(resolveString(R.string.scanner_ready) + "!");
                  tvLocation.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            }
      }

      @Override
      public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == Activity.RESULT_OK) {
                  if (requestCode == REQUEST_SCANNER) {
                        String path = data.getStringExtra("path");
                        Log.d(TAG, "path: " + path);
                        if (path != null && !path.trim().isEmpty()) {
                              processImage(path);
                        } else {
                              MyApplication.showToast(this, resolveString(R.string.image_failed_process), Gravity.CENTER);
                        }
                  }
            }
      }

      private void processImage(String path) {
            Log.d(TAG, "process image: " + path);
            setImage(path);
            MultipartBody.Part file = RetrofitHelper.prepareFilePart(this, "image", path);
            sendFile(file);
            animateProgress();
      }

      public void sendFile(MultipartBody.Part file) {
//            @Part("app_id") RequestBody app_id,
//            @Part("ext_code") RequestBody ext_code,
//            @Part("gps") RequestBody latlng,
//            @Part("user_id") RequestBody u_email,
//            @Part MultipartBody.Part file,
//            @Query("api_key") String api_key
//            Toast.makeText(ScanActivity.this, "Sending file", Toast.LENGTH_SHORT).show();
            isCancelled = false;
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(Keys.AGRO_BASE)
                    .addConverterFactory(GsonConverterFactory.create());

            Retrofit retrofit = builder.build();

            LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

            String gps = "";
            String email = SharePrefManager.getInstance(getApplicationContext()).getUserDetails().get(0);
            if (MyApplication.poi != null)
                  gps = MyApplication.poi.getLat() + "," + MyApplication.poi.getLon();

            Log.d(TAG, "gps: " + gps);

            Call<ScanResponse> call = service.scanFAW(
                    createPartFromString(Keys.APP_ID),
                    createPartFromString(sharePrefManager.getOrganisationDetails().get(0)),
                    createPartFromString(gps),
                    createPartFromString(email),
                    file, Keys.API_KEY);

            Log.e(TAG, "ext_code: " + sharePrefManager.getOrganisationDetails().get(0));

//        Call<ScanResponse> call= service.scanFAW(createPartFromString(Keys.APP_ID),createPartFromString(gps),
//                createPartFromString((new User(this)).getId()!=null?
//                        (new User(this)).getId():""), file,Keys.API_KEY);

            Log.e(TAG, "Details: " + Keys.APP_ID + ", " + gps + ", " + email + ", " + file + ", " + Keys.API_KEY);

            call.enqueue(new Callback<ScanResponse>() {
                  @Override
                  public void onResponse(Call<ScanResponse> call, Response<ScanResponse> response) {

                        ScanResponse responseBody = response.body();
                        String message = null;
                        String success = null;
                        try {
                              if (responseBody != null) {
                                    message = responseBody.getMsg();
                                    success = responseBody.getSuccess();
                              }

//                    Toast.makeText(ScanActivity.this,"Success: " + success +", ScanActivity response: "+message+" body: "+responseBody
//                            +" code: "+response.code()+" resp message: "+response.message(), Toast.LENGTH_LONG).show();

                              Log.e(TAG, "Success: " + success + ", ScanActivity response: " + message + " body: " + responseBody
                                      + " code: " + response.code() + " resp message: " + response.message());
                        } catch (Throwable e) {
                              e.printStackTrace();
                              processResult(NetworkRequest.STATUS_FAIL, message);
                        }

                        if (message.equalsIgnoreCase("No label found")) {
                              scanFAW.setHeader(message);
                              Log.e(TAG, "Scanned Image detected is: " + scanFAW.getHeader());
                              processResult(NetworkRequest.STATUS_SUCCESS, message);
                        } else {
                              Log.e(TAG, message);
                              scanFAW.setHeader(message);
                              Log.e(TAG, "Scanned Image detected is: " + scanFAW.getHeader());
                              processResult(NetworkRequest.STATUS_SUCCESS, message);
                        }

//                if(message==null||message.trim().equalsIgnoreCase("null"))
//                    processResult(NetworkRequest.STATUS_FAIL,message);
//                else
//                    processResult(NetworkRequest.STATUS_SUCCESS,message);
                  }

                  @Override
                  public void onFailure(Call<ScanResponse> call, Throwable t) {
                        String message = t.getMessage();
                        Log.e(TAG, "throwable ScanActivity: " + t);
                        Toast.makeText(ScanActivity.this, "Scan Failed: " + message, Toast.LENGTH_LONG).show();
                        processResult(NetworkRequest.STATUS_FAIL, message);
                  }
            });

            //animateProgress();
      }

      private void processResult(int status, String response) {

            if (!isCancelled) {
                  if (status == NetworkRequest.STATUS_SUCCESS) {
                        try {

                              if (scanFAW.getHeader().equals("No label found")) {
//                        scanFAW.setHeader(resolveString(R.string.faw_detected_not));
                                    Log.e(TAG, "Not Detected: " + scanFAW.getHeader());
//                        scanFAW.setHeader(scanFAW.getHeader());
                                    scanFAW.setHeader("Pest Not Detected");
                                    setNotice();
                                    tvMessage.setVisibility(View.GONE);
                                    bgProgress.setVisibility(View.GONE);
                                    //vMenu.setVisibility(View.VISIBLE);

                              } else {
                                    vibrator.vibrate(4000);
//                        scanFAW.setHeader(resolveString(R.string.faw_detected));
                                    scanFAW.setHeader(scanFAW.getHeader());
                                    Log.e(TAG, "Detected: " + scanFAW.getHeader());
                                    setNotice();
                                    tvMessage.setVisibility(View.GONE);
                                    bgProgress.setVisibility(View.GONE);
                                    vMenu.setVisibility(View.VISIBLE);
                              }

                        } catch (Exception e) {
                              e.printStackTrace();
                        }
                  } else {
                        onFailed();
                  }
            } else {

                  onFailed();
            }
      }

//    private void processResult(int status,String response){
//
//        if(!isCancelled) {
//            if (status == NetworkRequest.STATUS_SUCCESS) {
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//
//                    if (jsonObject.has("success")) {
//                        int success = jsonObject.getInt("success");
//                        Log.d(TAG, "success: " + success);
//                        if (success == 1) {
//                            scanFAW.setMessage(jsonObject.optString("date"));
//                            scanFAW.setResponse(jsonObject.optInt("detected"));
//                            scanFAW.setHeader(response);
//
//                           // scanFAW.setResponse(1);
//                           // scanFAW.setMessage("Possibility of Fall Armyworm");
//
////                            if (scanFAW.getResponse() != "")
//
//                            if (scanFAW.getHeader().equals("No label found")) {
//                                scanFAW.setHeader(resolveString(R.string.faw_detected_not));
//                                Log.e(TAG, "Not Detected: " + scanFAW.getHeader());
//
//                            } else{
//                                vibrator.vibrate(1000);
//                                scanFAW.setHeader(resolveString(R.string.faw_detected));
//                                Log.e(TAG, "Detected: " + scanFAW.getHeader());
//                            }
//                        }
//                    }
//
//                    setNotice();
//
//                    Log.d(TAG, "date: " + scanFAW.getMessage());
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                onFailed();
//            }
//            bgProgress.setVisibility(View.GONE);
//        }
//        else
//            onFailed();
//    }

      public void onFailed() {
            Log.d(TAG, "failed");
            isCancelled = false;
            scanFAW.setMessage(resolveString(R.string.image_failed_process));
            scanFAW.setResponse(-1);
            // MyApplication.showToast(this,"Please take another image and scan again.",Gravity.CENTER);
            setNotice();
            bgProgress.setVisibility(View.GONE);
      }

      @Override
      public void onResume() {
            super.onResume();

      }

      private void animateProgress() {
            Log.d(TAG, "animate progress");
            scanFAW.setMessage(resolveString(R.string.faw_analysing));
            scanFAW.setResponse(-1);
            setNotice();
            bgNotice.setVisibility(View.GONE);
            fabCapture.setVisibility(View.GONE);
            bgProgress.setVisibility(View.VISIBLE);
            vProgress.setProgress(0);
            ProgressAnimation anim = new ProgressAnimation(vProgress, 0, vProgress.getMax());
            anim.setDuration(60000);
            anim.setPercentageListener(new PercentageListener() {
                  @Override
                  public void onUpdate(int percentage) {
                        tvPercentage.setText(percentage + " %");
                        if (percentage >= 100) {
                              onFailed();
                              scanFAW = new ScanFAW(ScanActivity.this);
                        }
                  }
            });

            vProgress.startAnimation(anim);

            roundCornerProgressBar.setProgress(0);
            roundCornerProgressBar.setMax(60000);
            countDownTimer = new CountDownTimer(60000, 1) {
                  @Override
                  public void onTick(long l) {
                        roundCornerProgressBar.setProgress((60000 - l));
                  }

                  @Override
                  public void onFinish() {
                        countDownTimer = null;
                  }
            }.start();

            tvScanning.setText(resolveString(R.string.scanning));

      }

      @Override
      public void onDestroy() {
            super.onDestroy();
            //if(handler!=null)handler.removeCallbacks(runnable);
            if (receiver != null) unregisterReceiver(receiver);
            if (countDownTimer != null) countDownTimer.cancel();
      }


      class ScanReceiver extends BroadcastReceiver {

            @Override
            public void onReceive(Context context, Intent intent) {
                  updateScannerStatus();
            }
      }

      protected void toggleView(String action) {
            if (action != null) {
                  if (action.equalsIgnoreCase(ACTION_SHOW_PROGRESS)) {
                        wrapper.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.VISIBLE);
                        progressBar.show();
                  } else if (action.equalsIgnoreCase(ACTION_SHOW_ALL)) {
                        wrapper.setVisibility(View.VISIBLE);
                        imageView.setVisibility(View.VISIBLE);
                        ibAction.setVisibility(View.VISIBLE);
                        progressBar.hide();
                        progressBar.setVisibility(View.GONE);
                  } else if (action.equalsIgnoreCase(ACTION_HIDE_ALL)) {
                        wrapper.setVisibility(View.GONE);
                        ibAction.setVisibility(View.GONE);
                        imageView.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        progressBar.hide();
                  }
            }
      }

      //Show textbox to edit username
      private void initPesticideDialog() {

            dialogPesticide = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
            dialogPesticide.setContentView(R.layout.pesticide);

            ((TextView) dialogPesticide.findViewById(R.id.tv_caption)).setText("Recommended FAW pesticides");

            dialogPesticide.findViewById(R.id.ib_back).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        dialogPesticide.cancel();
                  }
            });

            RecyclerView recyclerView = dialogPesticide.findViewById(R.id.recyclerview);

            recyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(this));
            recyclerView.setAdapter(new PesticideAdapterRV(this, LocsmmanEngine.getPesticides()));
      }

      private void openDailer() {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:00233242426336"));
            startActivity(intent);
      }


      private void share() {
//        Image image=scanFAW.getImage();
//        Log.d(TAG,"share path: "+image.getImage_url());
//
//        if(image==null||(image.getUri()==null&&image.getImage_url()==null)){
//            MyApplication.showToast(this,resolveString(R.string.image_failed),Gravity.CENTER);
//            return;
//        }
//       String msg="";
//        if(!scanFAW.getMessage().equalsIgnoreCase(getResources().getString(R.string.faw_msg_default)))
//            msg=scanFAW.getMessage();
//
//        BaseActivity.sharetoChat(this,image, msg);

            openWesync();

      }

      void openWesync() {
            String packageName = "com.hensongeodata.wesync";
            Intent intent = getPackageManager().getLaunchIntentForPackage(packageName);
            if (intent == null) {
                  // Bring user to the market or let them choose an app?
                  intent = new Intent(Intent.ACTION_VIEW);
                  //intent.setData(Uri.parse("market://details?id=" + packageName));
                  intent.setData(Uri.parse("https://1drv.ms/u/s!AqpaVLehV_otfY6BsreT57ifXTg"));

                  startActivity(intent);
                  return;
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setAction("com.hensongeodata");
            intent.putExtra("email", user.getEmail());
            intent.putExtra("password", user.getPassword());

            startActivity(intent);
      }
}
