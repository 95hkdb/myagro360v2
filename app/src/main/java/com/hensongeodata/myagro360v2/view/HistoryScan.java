package com.hensongeodata.myagro360v2.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.Interface.HistoryTabListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.adapter.HistoryScanTabsRV;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.fragment.HistoryMappingFragment;
import com.hensongeodata.myagro360v2.fragment.HistoryScanFragment;

import java.util.ArrayList;
import java.util.HashMap;


public class HistoryScan extends BaseActivity implements HistoryTabListener {
    private static String TAG=HistoryScan.class.getSimpleName();

    protected RecyclerView recyclerView;
    protected HistoryScanTabsRV adapter;
    protected LocsmmanEngine locsmmanEngine;
    protected TextView tvCaption;

    public ImageButton ibTopLeft;
    protected ImageButton ibTopRight;
    private Context context;
    private TextView tvPlaceholder;
    ArrayList<String> headers;
    ArrayList<HashMap<String,String>>items;
    private int mode;
    private String form_id;
    private boolean refresh=false;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_scan);
        locsmmanEngine=new LocsmmanEngine(this);
        mode=getIntent().getIntExtra("mode",-1);
        form_id=getIntent().getStringExtra("form_id");


          ibTopLeft = findViewById(R.id.ib_back);
          ibTopRight = findViewById(R.id.ib_action);
        ibTopRight.setImageResource(R.drawable.ic_close_dark);
        ibTopRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        ((TextView)findViewById(R.id.tv_caption))
                .setText(mode==ChatbotAdapterRV.MODE_SCOUT?resolveString(R.string.scout_history)
                        : resolveString(R.string.mapping_history));

          recyclerView = findViewById(R.id.recyclerview_horizontal);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));

        init();
    }


    private void init(){
        items=new ArrayList<>();

        if(form_id!=null)
            headers=databaseHelper.getScoutIDList(form_id,mode);
        else
            headers=databaseHelper.getScoutIDList(mode);

        if(headers!=null)
            Log.d(TAG,"headers size: "+headers.size());

        if(headers!=null&&headers.size()>0){
            int index=0;
            int tag= headers.size();
            for(String header:headers){
                HashMap<String, String >map=new HashMap<>();
                map.put("value",header);
                map.put("id",""+tag);
                map.put("focus", ""+(index==0? 1:0));
                items.add(map);

                index++;
                tag--;
            }
            addFragment(headers.get(0),true);
        }
        else{
              findViewById(R.id.tv_placeholder).setVisibility(View.VISIBLE);
        }


        Log.d(TAG,"item size: "+items.size());
        //adapter=new HistoryScanTabsRV(this,items,this,this);
        //adapter.setMode(mode);
        //recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void openMapping(){
        Intent mapping=new Intent(this,Scout.class);
        mapping.putExtra("mode",ChatbotAdapterRV.MODE_SCOUT);
        startActivity(mapping);
        refresh=true;
    }

    private void openScouting(){
        Intent scouting=new Intent(this,Scout.class);
        scouting.putExtra("mode",ChatbotAdapterRV.MODE_SCOUT);
        startActivity(scouting);
        refresh=true;
    }

    @Override
    public void onSelected(String item) {
        addFragment(item,false);
    }

    private void addFragment(String section_id, boolean firstTime){
        Bundle bundle = new Bundle();
        bundle.putString("section_id", section_id);
        FragmentManager fm = getSupportFragmentManager();

        if(mode==ChatbotAdapterRV.MODE_SCOUT) {
            HistoryScanFragment fragment = new HistoryScanFragment();
            fragment.setArguments(bundle);

            if (firstTime)
                fm.beginTransaction().add(R.id.v_container, fragment).commit();
            else
                fm.beginTransaction().replace(R.id.v_container, fragment).commit();
        }
        else {
            HistoryMappingFragment fragment=new HistoryMappingFragment();
            fragment.setArguments(bundle);
            if(firstTime)
                fm.beginTransaction().add(R.id.v_container,fragment).commit();
            else
                fm.beginTransaction().replace(R.id.v_container,fragment).commit();
        }
        }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.shutdownLive();
    }
}
