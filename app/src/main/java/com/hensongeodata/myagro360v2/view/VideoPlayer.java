package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.halilibo.bettervideoplayer.BetterVideoCallback;
import com.halilibo.bettervideoplayer.BetterVideoPlayer;
import com.hensongeodata.myagro360v2.R;

import java.io.File;

/**
 * Created by user1 on 4/23/2018.
 */

public class VideoPlayer extends BaseActivity implements BetterVideoCallback {

   // private static final String TEST_URL = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";

    public static int SOURCE_FORM=1;
    public static int SOURCE_KB=2;
    private BetterVideoPlayer player;
    private String video_path;
    private int source;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_player);

        source=getIntent().getIntExtra("source",-1);
        video_path=getIntent().getExtras().getString("video_path");

        (findViewById(R.id.ib_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close(false);
            }
        });


        if(source==SOURCE_KB)
            findViewById(R.id.ib_delete).setVisibility(View.GONE);
        else {
            (findViewById(R.id.ib_delete)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    close(true);
                }
            });
        }

        // Grabs a reference to the player view
          player = findViewById(R.id.player);
        //player.setHideControlsDuration(600000);
        player.setAutoPlay(true);
        player.setBottomProgressBarVisibility(true);
        player.setHideControlsOnPlay(false);
        // Sets the callback to this Activity, since it inherits EasyVideoCallback
        player.setCallback(this);

        // Sets the source to the HTTP URL held in the TEST_URL variable.
        // To play files, you can use Uri.fromFile(new File("..."))
        //player.setSource(Uri.parse(TEST_URL));

        if(video_path==null||video_path.trim().isEmpty()){
            Toast.makeText(this,"Video file error.",Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

             player.setSource(source!=SOURCE_KB?Uri.fromFile(new File(video_path))
                     :Uri.parse(video_path));

        player.showControls();
   }

    @Override
    public void onPause() {
        super.onPause();
        // Make sure the player stops playing if the user presses the home button.
        player.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
      //  player.start();
    }

    private void close(boolean delete){
        Intent i = new Intent();
        i.putExtra("delete", delete);
        setResult(Activity.RESULT_OK, i);

        finish();
    }

    // Methods for the implemented EasyVideoCallback

    @Override
    public void onStarted(BetterVideoPlayer player) {
        //Log.i(TAG, "Started");
    }

    @Override
    public void onPaused(BetterVideoPlayer player) {
        //Log.i(TAG, "Paused");
    }

    @Override
    public void onPreparing(BetterVideoPlayer player) {
        //Log.i(TAG, "Preparing");
    }

    @Override
    public void onPrepared(BetterVideoPlayer player) {
    }

    @Override
    public void onBuffering(int percent) {
        //Log.i(TAG, "Buffering " + percent);
    }

    @Override
    public void onError(BetterVideoPlayer player, Exception e) {
        //Log.i(TAG, "Error " +e.getMessage());
    }

    @Override
    public void onCompletion(BetterVideoPlayer player) {
        Log.i("VideoPlayer", "Completed");
        close(false);
    }

    @Override
    public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {
        //Log.i(TAG, "Controls toggled " + isShowing);
    }
}
