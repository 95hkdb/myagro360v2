package com.hensongeodata.myagro360v2.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.fragment.BSFragment;
import com.hensongeodata.myagro360v2.model.POI;

/**
 * Created by user1 on 2/8/2018.
 */

public class PreviewPOI extends BaseActivity {
    private static String TAG=PreviewPOI.class.getSimpleName();
    private static String ACTION_ADD="addition";
    private static String ACTION_SUB="subtraction";
    private static String ACTION_DELETE="delete";
    private String action;
    private Toolbar toolbar;
    private androidx.appcompat.app.ActionBar actionBar;


    private TextView tvCaption;
    private TextView tvAccuracy;
    private TextView tvLongitude;
    private TextView tvLatitude;
    private TextView tvAltitude;

    private EditText etCaption;
    private ImageButton ibCaption;

    private View bottom_sheet;
    private TextView bsAccuracy;
    private TextView bsLongitude;
    private TextView bsLatitude;
    private TextView bsAltitude;
    private FloatingActionButton fabAddPOI;

    private TextView tvPosition;
    private TextView tvSize;
    private ImageButton ibAddition;
    private ImageButton ibSubtraction;

    private IntentFilter filter;
    private PreviewPOIReceiver receiver;

    private int position;
    private POI poi;
    private String cachedCaption;

    private BottomSheetBehavior mBottomSheetBehavior;
    private ImageButton ibBSHandle;
    private View btnBSHandle;
    private boolean bsOpened=false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poi_preview_backup);
          toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Preview");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        initBottomSheet();

        position=getIntent().getExtras().getInt("position");
        poi=new POI();
        if(MyApplication.poiGroup.getPoiList().size()>position
                &&MyApplication.poiGroup.getPoiList().get(position)!=null)
                poi=MyApplication.poiGroup.getPoiList().get(position);
        else{
            Toast.makeText(this,"Can't preview. Way point is invalid.",Toast.LENGTH_SHORT).show();
        }

        initPOIView(poi);//instantiate poi views and set initial values
        initPOIBootomSheetView(MyApplication.poi);

          etCaption = findViewById(R.id.et_caption);
          tvCaption = findViewById(R.id.tv_caption);


          ibCaption = findViewById(R.id.ib_caption);
        ibCaption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleMode(true);
            }
        });


        etCaption.setText(MyApplication.poiGroup.getPoiList().get(position).getName());
        tvCaption.setText(MyApplication.poiGroup.getPoiList().get(position).getName());


        filter=new IntentFilter(BroadcastCall.BSFRAGMENT);
        receiver=new PreviewPOIReceiver();
        registerReceiver(receiver,filter);


          fabAddPOI = findViewById(R.id.fab_addpoint);
        fabAddPOI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doAddPoint();
            }
        });

    }

    private void initPOIBootomSheetView(POI poi){

          bsAccuracy = (findViewById(R.id.bs_accuracy)).findViewById(R.id.tv_value);
        ((TextView)(findViewById(R.id.bs_accuracy)).findViewById(R.id.tv_tag)).setText("ACCURACY");
        bsAccuracy.setTextColor(ContextCompat.getColor(this,R.color.primaryWhite));
        ((TextView)(findViewById(R.id.bs_accuracy)).findViewById(R.id.tv_tag))
                .setTextColor(ContextCompat.getColor(this,R.color.secondaryWhite));

          bsLongitude = (findViewById(R.id.bs_longitude)).findViewById(R.id.tv_value);
        ((TextView)(findViewById(R.id.bs_longitude)).findViewById(R.id.tv_tag)).setText("LONGITUDE");
        bsLongitude.setTextColor(ContextCompat.getColor(this,R.color.primaryWhite));
        ((TextView)(findViewById(R.id.bs_longitude)).findViewById(R.id.tv_tag))
                .setTextColor(ContextCompat.getColor(this,R.color.secondaryWhite));

          bsLatitude = (findViewById(R.id.bs_latitude)).findViewById(R.id.tv_value);
        ((TextView)(findViewById(R.id.bs_latitude)).findViewById(R.id.tv_tag)).setText("LATITUDE");
        bsLatitude.setTextColor(ContextCompat.getColor(this,R.color.primaryWhite));
        ((TextView)(findViewById(R.id.bs_latitude)).findViewById(R.id.tv_tag))
                .setTextColor(ContextCompat.getColor(this,R.color.secondaryWhite));

          bsAltitude = (findViewById(R.id.bs_altitude)).findViewById(R.id.tv_value);
        ((TextView)(findViewById(R.id.bs_altitude)).findViewById(R.id.tv_tag)).setText("ALTITUDE");
        bsAltitude.setTextColor(ContextCompat.getColor(this,R.color.primaryWhite));
        ((TextView)(findViewById(R.id.bs_altitude)).findViewById(R.id.tv_tag))
                .setTextColor(ContextCompat.getColor(this,R.color.secondaryWhite));

        setBottomSheetPOIValues(poi);
    }

    private void initPOIView(POI poi){
          tvAccuracy = (findViewById(R.id.accuracy)).findViewById(R.id.tv_value);
        ((TextView)(findViewById(R.id.accuracy)).findViewById(R.id.tv_tag)).setText("ACCURACY");

          tvLongitude = (findViewById(R.id.longitude)).findViewById(R.id.tv_value);
        ((TextView)(findViewById(R.id.longitude)).findViewById(R.id.tv_tag)).setText("LONGITUDE");

          tvLatitude = (findViewById(R.id.latitude)).findViewById(R.id.tv_value);
        ((TextView)(findViewById(R.id.latitude)).findViewById(R.id.tv_tag)).setText("LATITUDE");

          tvAltitude = (findViewById(R.id.altitude)).findViewById(R.id.tv_value);
        ((TextView)(findViewById(R.id.altitude)).findViewById(R.id.tv_tag)).setText("ALTITUDE");

        setPOIValues(poi);
    }

    private void setBottomSheetPOIValues(POI poi){
        Log.d(TAG,"BS lon: "+poi.getLon()+" lat: "+poi.getLat()+" alt: "+poi.getAlt());
        String accuracy=poi.getAccuracy();
        if(accuracy!=null&&!accuracy.trim().equalsIgnoreCase("null")) {
            bsAccuracy.setText(accuracy+"m");
            bsAccuracy.setTextColor(getAccuracyColor(this,Integer.parseInt(accuracy)));

            bsLongitude.setText(poi.getLon());
            bsLatitude.setText(poi.getLat());
            bsAltitude.setText(poi.getAlt()+"m");
        }
    }

    private void setPOIValues(POI poi){
        String accuracy=poi.getAccuracy();
        if(accuracy!=null&&!accuracy.trim().equalsIgnoreCase("null")) {
            tvAccuracy.setText(accuracy+"m");
            tvAccuracy.setTextColor(getAccuracyColor(this,Integer.parseInt(accuracy)));

            tvLongitude.setText(poi.getLon());
            tvLatitude.setText(poi.getLat());
            tvAltitude.setText(poi.getAlt());
            tvAltitude.setText(poi.getAlt()+"m");
        }
    }


    private void initBottomSheet(){

        View bottomSheet = findViewById(R.id.bottom_sheet);
        btnBSHandle=bottomSheet.findViewById(R.id.bg_bottomsheet_handle);
          ibBSHandle = bottomSheet.findViewById(R.id.ib_bottomsheet_handle);
        ibBSHandle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getActivity(),"handle click",Toast.LENGTH_SHORT).show();
                int bsState=mBottomSheetBehavior.getState();
                if(bsState== BottomSheetBehavior.STATE_COLLAPSED){
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                else if(bsState==BottomSheetBehavior.STATE_EXPANDED) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });

        btnBSHandle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ibBSHandle.performClick();
            }
        });

        //  View bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setPeekHeight(200);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                //  Toast.makeText(getActivity(),"bs callback",Toast.LENGTH_SHORT).show();
                if(newState==BottomSheetBehavior.STATE_COLLAPSED) {
                    ibBSHandle.setImageResource(R.drawable.ic_arrow_up);
                    bsOpened=false;
                }
                else if (newState==BottomSheetBehavior.STATE_EXPANDED) {
                    ibBSHandle.setImageResource(R.drawable.ic_arrow_down);
                    bsOpened=true;
                }
                //bsOpened=!bsOpened;
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

    }

    private void toggleMode(boolean editMode){
        if(editMode){
            cachedCaption=tvCaption.getText().toString().trim();
            tvCaption.setVisibility(View.GONE);
            etCaption.setVisibility(View.VISIBLE);
            etCaption.setText(tvCaption.getText().toString().trim());
            etCaption.requestFocus();
            ibCaption.setImageResource(R.drawable.ic_check);
            ibCaption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleMode(false);
                }
            });
        }
        else {

            String caption=etCaption.getText().toString().trim();
            if(caption.trim().isEmpty()){
                caption=cachedCaption;//revert to the previous caption displayed
                Toast.makeText(this,"Way point name cannot be empty.",Toast.LENGTH_SHORT).show();
            }

            etCaption.setVisibility(View.GONE);
            tvCaption.setVisibility(View.VISIBLE);
            tvCaption.setText(caption);
            ibCaption.setImageResource(R.drawable.ic_edit);
            ibCaption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleMode(true);
                }
            });

            poi.setName(caption);
            modifyPOI(poi);
        }
    }

    private void doAddPoint(){
        if(MyApplication.poi.getAccuracy()!=null) {
            int maxAccuracy=settings.getAccuracy_max();
            if (Integer.parseInt(MyApplication.poi.getAccuracy())<=maxAccuracy){
                poi=MyApplication.poi;
                modifyPOI(poi);//modify poi of original list at index=position with new values;
                setPOIValues(poi);
            }
            else
                Toast.makeText(this,"Please ensure your accuracy is less than "+(maxAccuracy+1),Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this,"No POI available",Toast.LENGTH_SHORT).show();
        }
    }

    //modify poi of original list
    private void modifyPOI(POI poi){
        MyApplication.poiGroup.getPoiList().set(position,new POI(poi.getName(),poi.getLon(),poi.getLat(),poi.getAlt(), poi.getAccuracy()));
    }


    class PreviewPOIReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG,"on receive");
            int status=intent.getExtras().getInt("status");
            if(status== NetworkRequest.STATUS_SUCCESS){
                Log.d(TAG,"on status success");
                setBottomSheetPOIValues(MyApplication.poi);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds itemsVisible to the action bar if it is present.
        getMenuInflater().inflate(R.menu.preview_poi, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete) {
            MyApplication.poiGroup.getPoiList().remove(position);
            action=ACTION_DELETE;
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
        BroadcastCall.publishLocationUpdate(this,NetworkRequest.STATUS_SUCCESS, BSFragment.ACTION_DIMENSION);
    }
}
