package com.hensongeodata.myagro360v2.view;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.LinearLayoutManagerWithSmoothScroller;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.model.MapModel;


public class Chatbot extends BaseActivity implements RadioGroup.OnCheckedChangeListener {
    private static String TAG=Chatbot.class.getSimpleName();

    protected RecyclerView recyclerView;
    protected View bgAction;
    protected Button btnOk;
    protected Button btnNegative;
    protected Button btnPositive;
    protected LocsmmanEngine locsmmanEngine;
    protected TextView tvCaption;
    protected RadioGroup rgMappingType;
    protected RadioButton rbLine;
    protected RadioButton rbPolygon;
    protected ImageButton ibTopLeft;
    protected ImageButton ibTopRight;
    private Context context;
    protected boolean speakEnabled=false;
   // protected boolean ttsAvailable=false;
    //RecyclerView.SmoothScroller smoothScroller;

    protected MapModel mapModel;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chatbot);
        mapModel=new MapModel();
        locsmmanEngine=new LocsmmanEngine(this);
          recyclerView = findViewById(R.id.recyclerview);

        rgMappingType=findViewById(R.id.rg_type);
        rbLine=findViewById(R.id.rb_line);
        rbPolygon=findViewById(R.id.rb_polygon);

        rbLine.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    mapModel.type=MapModel.TYPE_LINE;
            }
        });


        rbPolygon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    mapModel.type=MapModel.TYPE_POLYGON;
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(context));

          bgAction= findViewById(R.id.bg_action);
          btnOk = findViewById(R.id.btn_ok);
          btnPositive = findViewById(R.id.btn_positive);
          btnNegative = findViewById(R.id.btn_negative);

          tvCaption = findViewById(R.id.tv_caption);
          ibTopLeft = findViewById(R.id.ib_back);
          ibTopRight = findViewById(R.id.ib_action);

        ibTopLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSpeaker();
            }
        });
    }

    protected void showButton(int...ids){
        locsmmanEngine.hideView(btnNegative);
        locsmmanEngine.hideView(btnOk);
        locsmmanEngine.hideView(btnPositive);

        for(int id:ids){
          //hide all ids
            if(btnNegative.getId()==id) locsmmanEngine.showView(btnNegative,false);
            if(btnPositive.getId()==id) locsmmanEngine.showView(btnPositive,false);
            if(btnOk.getId()==id) locsmmanEngine.showView(btnOk,false);
        }
    }

    protected void scroll(int position){
        //smoothScroller.setTargetPosition(position);
        recyclerView.smoothScrollToPosition(position);
    }


    protected void toggleSpeaker(){
        if(MyApplication.ttsAvailable&& Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (speakEnabled)
                ibTopLeft.setImageResource(R.drawable.baseline_voice_over_off);
            else
                ibTopLeft.setImageResource(R.drawable.baseline_record_voice);

            speakEnabled = !speakEnabled;

            if(MyApplication.tts!=null)
                        MyApplication.tts.stop();
        }
        else {
            MyApplication.showToast(this,"Voice over not available", Gravity.CENTER);
            ibTopLeft.setImageResource(R.drawable.baseline_voice_over_off);
            speakEnabled=false;
        }
    }


    protected void showMappingType(){
        if(rgMappingType!=null) {
            rgMappingType.setVisibility(View.VISIBLE);
            if(rbLine!=null)
                rbLine.performClick();
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

    }
}
