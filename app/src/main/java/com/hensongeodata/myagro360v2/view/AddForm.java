package com.hensongeodata.myagro360v2.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;

/**
 * Created by user1 on 2/5/2018.
 */

public class AddForm extends BaseActivity {
      private Toolbar toolbar;
      private ActionBar actionBar;
      private Button btnAddForm;
      private TextInputEditText etAddForm;
      private String accessCode;

      FormListReceiver receiver;
      IntentFilter filter;

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.add_form);

            receiver = new FormListReceiver();
            filter = new IntentFilter(BroadcastCall.FORM_LIST);
            registerReceiver(receiver, filter);

            toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            String title = getResources().getString(R.string.add_form);
            actionBar.setTitle(title);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        finish();
                  }
            });


            etAddForm = findViewById(R.id.et_add_form);
            btnAddForm = findViewById(R.id.btn_add_form);

            btnAddForm.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        etAddForm.setError(null);
                        accessCode = etAddForm.getText().toString().trim();
                        if (TextUtils.isEmpty(accessCode)) {
                              etAddForm.setError(getResources().getString(R.string.error_field_required));
                              etAddForm.requestFocus();
                              return;
                        }

                        accessForm(accessCode);
                  }
            });
      }


      private void accessForm(String code) {
            if (myApplication.hasNetworkConnection(this)) {
                  showProgress(getResources().getString(R.string.requesting_access));
                  networkRequest.fetchForm(code, true);
            } else
                  myApplication.showInternetError(this);
      }

      class FormListReceiver extends BroadcastReceiver {
            @Override
            public void onReceive(Context context, Intent intent) {

                  int status = intent.getExtras().getInt("status");
                  if (status == NetworkRequest.STATUS_SUCCESS) {
                        createForm.setShouldRefreshForm(true);//allow form refresh on main
//                        Intent i=new Intent(AddForm.this,MainActivity.class);
                        Intent i = new Intent(AddForm.this, MainActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        Toast.makeText(AddForm.this, getResources().getString(R.string.access_granted), Toast.LENGTH_SHORT).show();
                        startActivity(i);
                        hideProgress();
                        finish();

                  } else if (status == NetworkRequest.STATUS_FAIL) {
                        myApplication.showMessage(AddForm.this,
                                getResources().getString(R.string.form_not_found) + ", " + getResources().getString(R.string.try_again));
                        hideProgress();
                  }
            }
      }

      @Override
      protected void onDestroy() {
            super.onDestroy();
            unregisterReceiver(receiver);
      }
}
