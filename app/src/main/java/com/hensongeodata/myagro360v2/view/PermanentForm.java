package com.hensongeodata.myagro360v2.view;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.MultiMediaAdapterRV;
import com.hensongeodata.myagro360v2.fragment.MultiImageFragment;
import com.hensongeodata.myagro360v2.model.ImageMulti;

/**
 * Created by user1 on 5/15/2018.
 */

public class PermanentForm extends BaseActivity {

    private int globalMediaIndex;
    private ImageMulti imageMulti;
    private int mediaType;
    private  MultiImageFragment multiImageFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.permanent_form);
        mediaType=getIntent().getExtras().getInt("media_type");
        imageMulti=new ImageMulti();
       if(mediaType== MultiMediaAdapterRV.TYPE_IMAGE)
           addMultiImageFragment();


    }

    private void addMultiImageFragment(){
        MyApplication.imageMultiArrayList.add(imageMulti);
        globalMediaIndex=MyApplication.imageMultiArrayList.size()-1;

        Bundle bundle = new Bundle();
        bundle.putInt("index", globalMediaIndex);

        multiImageFragment=new MultiImageFragment();
        multiImageFragment.setArguments(bundle);
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.container,multiImageFragment,"multiImage").commit();
    }


}
