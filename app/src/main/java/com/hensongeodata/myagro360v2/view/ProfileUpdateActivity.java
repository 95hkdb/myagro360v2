package com.hensongeodata.myagro360v2.view;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import androidx.fragment.app.FragmentManager;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.fragment.ProfileUpdate;

public class ProfileUpdateActivity extends BaseActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppThemeLogin);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_update);
          Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(resolveString(R.string.update_profile));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        ProfileUpdate profileUpdate=new ProfileUpdate();
        FragmentManager fm=getSupportFragmentManager();
        fm.beginTransaction().add(R.id.v_container,profileUpdate).commit();

    }
}
