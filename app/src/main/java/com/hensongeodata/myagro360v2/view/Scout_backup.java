package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.hensongeodata.myagro360v2.Interface.TaskListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.model.Pauline;

import java.util.ArrayList;

/**
 * Created by user1 on 7/16/2018.
 */

public class Scout_backup extends Chatbot implements TaskListener {
    private static String TAG=Scout.class.getSimpleName();
    Pauline pauline;
    ChatbotAdapterRV adapter;
    //ArrayList<InstructionScout> itemsAll;
    ArrayList<InstructionScout> itemsVisible;
    LocsmmanEngine locsmmanEngine;
    Runnable runnable;
    Handler handler;
    int index=0;
    public static int OPEN_SCANNER=100;
    private long DELAY_TIME=3000; //delay time is 10 seconds
    public static String scannning_id;
    private TextToSpeech tts;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSystemUI();
        tts=MyApplication.tts;
        tvCaption.setText("FAW Scouting Assistant");
        locsmmanEngine = new LocsmmanEngine(this);
        pauline = new Pauline(this);
        pauline.setTaskListener(this);
        MyApplication.initLive(this,pauline.getTaskListener());
        showButton();//hides all buttons

        itemsVisible = new ArrayList<>();
        itemsVisible = pauline.getChatScout();

        adapter = new ChatbotAdapterRV(this);
        adapter.setMode(ChatbotAdapterRV.MODE_SCOUT);
        recyclerView.setAdapter(adapter);

        handler=new Handler();

        runnable=new Runnable() {
            @Override
            public void run() {
           processItem();
            }
        };

        //if(firstTime)
            handler.postDelayed(runnable,1000);

        //firstTime=false;

        ibTopRight.setImageResource(R.drawable.ic_close_dark);
        ibTopRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        toggleSpeaker();
    }

    private void processItem(){
        if(shouldProcess()) {
            Log.d(TAG, "process item");
            InstructionScout instruction = null;
            if (index < itemsVisible.size())
                instruction = itemsVisible.get(index);
            else
                end();

            if (instruction != null) {
                adapter.addItem(instruction);
                scroll(adapter.getItemCount() - 1);

                if (instruction.getMessage() != null) speak(instruction.getMessage());
                // recyclerView.smoothScrollToPosition(adapter.getItemCount()-1);
            }
            index++;

            if (instruction != null) {
                if (instruction.getAction() == InstructionScout.ACTION_TASK) {
                    //show ok button
                    showButton(btnOk.getId());
                    btnOk.setText("Yes");
                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openScanner();
                        }
                    });

                    stopHandler();
                } else {
                    Log.d(TAG, "init handler");
                    handler.postDelayed(runnable, DELAY_TIME);
                    showButton();//empty argument means hide all buttons
                }
            }
        }
    }

    private void openScanner(){
        Intent i=new Intent(Scout_backup.this,CameraScanSimple.class);
        InstructionScout instructionScout=null;

        if(index<itemsVisible.size())
              instructionScout=itemsVisible.get(index);

        if(instructionScout!=null&&instructionScout.getId()!=null) {
            i.putExtra("item_id",instructionScout.getId());
            i.putExtra("map_id",instructionScout.getScout_id());
            stopHandler();
            startActivityForResult(i, OPEN_SCANNER);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== Activity.RESULT_OK){
            if(requestCode==OPEN_SCANNER){
                int success=data.getIntExtra("success",0);
                Log.d(TAG,"success: "+success);
                if(success==1) {
                    if(MyApplication.itemsLivePosition!=null)
                        MyApplication.itemsLivePosition.add(adapter.getItemCount());

                    handler.postDelayed(runnable, 1000);
                }
                MyApplication.refreshScanService(this);
            }
        }
        else {}
    }

    public void end(){
        Log.d(TAG,"ended");
        //finish();
       // return;
    }

    private void stopHandler(){
        Log.d(TAG,"removing handler...");
        if(handler!=null) handler.removeCallbacks(runnable);
    }

    @Override
    public void onTaskAccepted(String id) {
        //id is the position of the task in the list
    }

    @Override
    public void onTaskStarted(String id) {

    }

    @Override
    public void onTaskStopped(String id, boolean complete) {
       Log.d(TAG,"task complete "+complete);
        if(complete){
            InstructionScout instructionScout=databaseHelper.getInstructScout(id,ChatbotAdapterRV.MODE_SCOUT);
            Log.d(TAG,"instruction: "+instructionScout+" id "+instructionScout.getId());
            if(instructionScout!=null){
                    int index=getIndex(itemsVisible,instructionScout);
                    if(index>=0)
                        adapter.updateItem(index,instructionScout);
            }
        }
    }

    @Override
    public void onTaskCancelled(String id) {

    }

    @Override
    protected void onDestroy() {
        if(MyApplication.tts!=null){
            MyApplication.tts.stop();
            MyApplication.tts.shutdown();
        }
        super.onDestroy();
        stopHandler();
        MyApplication.shutdownLive();

    }

   private boolean shouldProcess(){
        if(MyApplication.tts!=null&&MyApplication.ttsAvailable&&speakEnabled&&MyApplication.tts.isSpeaking()){
            handler.postDelayed(runnable, 1000);
            Log.d(TAG,"speaking...");
            return false;
        }

        return true;
    }

    public void speak(CharSequence text){
        if (MyApplication.ttsAvailable&&speakEnabled&&Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                tts.speak(text,TextToSpeech.QUEUE_FLUSH,null,"id1");
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    public static int getIndex(ArrayList<InstructionScout> items,InstructionScout instructionScout){
        int index=-1;
        if(MyApplication.itemsLivePosition!=null&&MyApplication.itemsLivePosition.size()>0){
            index=MyApplication.itemsLivePosition.get(0);
            MyApplication.itemsLivePosition.remove(0);
        }
        else {
            index=(new MyApplication()).containsInstructScout(items, instructionScout);
        }

        return index;
    }
}
