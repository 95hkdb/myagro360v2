package com.hensongeodata.myagro360v2.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ArchiveAdapterRV;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.Archive;
import com.hensongeodata.myagro360v2.service.UploadFilesService;

/**
 * Created by user1 on 1/25/2018.
 */

public class ArchiveList extends SearchableList {
    private static String TAG=ArchiveList.class.getSimpleName();
    private static int MODE_DATA=200;
    private static int MODE_FILE=202;
    private static int submission_mode;
    private boolean locked=false;
    private ArchiveAdapterRV adapter;
    private ArrayList<Archive> archiveList;
    private ArrayList<Archive> sendList;
    private ImageButton ibDelete;
    private Button btnSubmit;
    private int pendingArchiveSize=0;

    private ArchiveReceiver receiver;
    private IntentFilter filter;
    private ArrayList<Object> adapterArchiveList;
    private HashMap<String,Boolean> topAction;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        filter=new IntentFilter(BroadcastCall.TRANSACTION_UPDATE);
        receiver=new ArchiveReceiver();
        registerReceiver(receiver,filter);

        topAction =new HashMap<>();
        topAction.put("value",false);//initial value of select all option
        fetchArchives();//load archives into list view

        tvCaption.setText(getResources().getString(R.string.saved_forms));
        etSearch.addTextChangedListener(new MyWatcher(ARCHIVE,adapter));

          ibDelete = bgAction.findViewById(R.id.ib_delete);
        ibDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doDeleteArchive();
            }
        });
          btnSubmit = bgAction.findViewById(R.id.btn_submit);
        btnSubmit.setText(resolveString(R.string.submit));
        btnSubmit.setTextColor(getResources().getColor(R.color.primaryWhite));
        btnSubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_green));
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendArchives();
            }
        });


        settings.setTransaction_id(null);//remove reference to any archived form being edited
    }

    private void fetchArchives(){
        showProgress(resolveString(R.string.forms_loading));
        adapterArchiveList=new ArrayList<>();
        adapterArchiveList.add(topAction);
        adapterArchiveList.addAll(databaseHelper.getArchives());

        Log.d(TAG," archive size: "+adapterArchiveList.size());

        if(adapterArchiveList==null||adapterArchiveList.size()<2){
            recyclerView.setVisibility(View.GONE);
            bgAction.setVisibility(View.GONE);
            setTvPlaceholder(resolveString(R.string.no)+" "+resolveString(R.string.form)
                    +" "+resolveString(R.string.available));
        }
        else {
            tvPlaceholder.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter = new ArchiveAdapterRV(this, adapterArchiveList);
            recyclerView.setAdapter(adapter);
            bgAction.setVisibility(View.VISIBLE);
        }

        hideProgress();
    }

    private void sendArchives(){
        ArrayList<Archive> archiveList=(ArrayList<Archive>) adapter.getPendingList();//get all itemsVisible inside archive list

        sendList=new ArrayList<>();
        for(int a=0;a<archiveList.size();a++){
            if(archiveList.get(a).isChecked())
                sendList.add(archiveList.get(a));
        }

        pendingArchiveSize=sendList.size();
    if(pendingArchiveSize>0) {
        if (myApplication.hasNetworkConnection(this)) {
            if (pendingArchiveSize > 0) {
                doSendArchive(sendList);
            } else {
                myApplication.showMessage(this, resolveString(R.string.no)+" " +
                        ""+resolveString(R.string.form)+" "+resolveString(R.string.selected));
            }
        } else {
            myApplication.showInternetError(this);
            }
        }
        else {
            Toast.makeText(this,resolveString(R.string.no)+" " +
                    ""+resolveString(R.string.form)+" "+resolveString(R.string.selected),Toast.LENGTH_SHORT).show();
        }
    }

    private void doDeleteArchive(){
        archiveList=(ArrayList<Archive>) adapter.getPendingList();//get all itemsVisible inside archive list
        final ArrayList<Archive> deletionList=new ArrayList<>();
        for(int a=0;a<archiveList.size();a++ ){
            if(archiveList.get(a).isChecked())
                deletionList.add(archiveList.get(a));
        }

        if(deletionList.size()>0) {
            new AlertDialog.Builder(this)
                    .setMessage(resolveString(R.string.delete)+" "+deletionList.size()+" " +
                            ""+(deletionList.size()==1? resolveString(R.string.form):resolveString(R.string._forms))+" ?")
                    .setPositiveButton(resolveString(R.string.yes_), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            for(int b=0;b<deletionList.size();b++) {
                                showProgress(resolveString(R.string.deleting)+" " + (b + 1) + " "+resolveString(R.string.of)+" " + deletionList.size());
                                Archive archive = deletionList.get(b);
                                databaseHelper.removeArchive(archive.getTransaction_id());
                            }
                            fetchArchives();
                            hideProgress();
                            dialogInterface.dismiss();
                        }
                    })
                    .setNegativeButton(resolveString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .show();
        }
        else {
            Toast.makeText(this,"No itemsVisible selected",Toast.LENGTH_SHORT).show();
            }
    }

    private void doSendArchive(ArrayList<Archive> sendList){
        Log.d(TAG,"send Forms "+sendList.size());
        submission_mode=MODE_DATA;
        if(sendList.size()>0) {
            Archive archive = sendList.get(sendList.size()-1);
            try {
                Log.d(TAG,"sending start");
                JSONObject jsonObject = databaseHelper.buildJSON(archive.getTransaction_id(),true);
                Log.d(TAG,"sending "+jsonObject);
                locked=true;
                networkRequest.sendTransaction(jsonObject);

                showProgress("Submitting Form "+(pendingArchiveSize-(sendList.size()-1))+" of "+pendingArchiveSize);
            } catch (JSONException e) {
                e.printStackTrace();
                locked=false;
                hideProgress();
                fetchArchives();//refreshing adapter values
                myApplication.showMessage(this,"Could not submit Form.");
                Log.d(TAG,"json error: "+e);
            }
        }
        else {
            locked=false;
            hideProgress();
            fetchArchives();//refreshing adapter values
            myApplication.showMessage(this,"Forms submitted successfully.");
        }
    }

    class ArchiveReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            int status=intent.getExtras().getInt("status");
            String action=intent.getExtras().getString("action");

            Log.d(TAG,"locked: "+locked+" status: "+status);
            if(status== NetworkRequest.STATUS_SUCCESS){
                if(locked) {
                    if(submission_mode==MODE_DATA){
                        submission_mode=MODE_FILE;
                        UploadFilesService.progressDialog=getProgress();
                        startService(new Intent(ArchiveList.this, UploadFilesService.class));
                    }
                    else if(submission_mode==MODE_FILE) {
                        Log.d(TAG,"locked");
                        Archive archive = sendList.get(sendList.size() - 1);
                        sendList.remove(sendList.size() - 1);
                        databaseHelper.removeArchive(archive.getTransaction_id());
                        doSendArchive(sendList);
                    }
                    else {
                        myApplication.showMessage(context,"Submission mode not specified.");
                        hideProgress();
                    }
                }
                else {
                    locked=false;
                }
            }
            else if(status==NetworkRequest.STATUS_FAIL){
                if(submission_mode==MODE_DATA) {
                    myApplication.showMessage(context, "Failed to submit Forms.");
                }
                else if(submission_mode==MODE_FILE)
                    myApplication.showMessage(context,"Data submission incomplete.");

                fetchArchives();
                hideProgress();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"onresume");
        fetchArchives();
        //adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
