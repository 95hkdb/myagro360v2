package com.hensongeodata.myagro360v2.view;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.CustomViewpager;
import com.hensongeodata.myagro360v2.fragment.CligisFragment;


public class Map extends AppCompatActivity {
    CustomViewpager viewPager;
    ViewPagerAdapter adapter;
    private Toolbar toolbar;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
          toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Silapha Map Model");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

          viewPager = findViewById(R.id.map_viewpager);
        adapter=new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new CligisFragment());
       // adapter.addFragment(new GoogleEarthFragment());

        viewPager.setAdapter(adapter);
        viewPager.setPagingEnabled(false);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


        viewPager.setCurrentItem(0);


         }


    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            return  mFragmentList.get(position);}

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }

        public void addFragment(Fragment fragment){
            mFragmentList.add(fragment);
        }
    }

}
