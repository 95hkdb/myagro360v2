package com.hensongeodata.myagro360v2.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;

/**
 * Created by user1 on 2/5/2018.
 */

public class Feedback extends BaseActivity {
    private Toolbar toolbar;
    private ActionBar actionBar;
    private Button btnFeedback;
    private TextInputEditText etFeedback;
    private String feedback;

    FeedbackReceiver receiver;
    IntentFilter filter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback);

        receiver=new FeedbackReceiver();
        filter=new IntentFilter(BroadcastCall.FEEDBACK);
        registerReceiver(receiver,filter);

          toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Feedback");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


          etFeedback = findViewById(R.id.et_feedback);
          btnFeedback = findViewById(R.id.btn_feedback);

        btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etFeedback.setError(null);
                feedback = etFeedback.getText().toString().trim();
                if(TextUtils.isEmpty(feedback)){
                    etFeedback.setError(getResources().getString(R.string.error_field_required));
                    etFeedback.requestFocus();
                    return;
                }

                sendFeedback(feedback);
            }
        });
    }


private void sendFeedback(String feedback){
    if(myApplication.hasNetworkConnection(this)){
        showProgress("Submitting feedback, Please wait...");
        networkRequest.sendFeedback(feedback);
    }
    else
        myApplication.showInternetError(this);
}

    class FeedbackReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

           int status=intent.getExtras().getInt("status");
                if(status== NetworkRequest.STATUS_SUCCESS) {
                        Intent i=new Intent(Feedback.this,MainActivity.class);
                        Toast.makeText(Feedback.this,"Thank you for your feedback.",Toast.LENGTH_SHORT).show();
                        hideProgress();
                        finish();
                }
                else if(status==NetworkRequest.STATUS_FAIL){
                    myApplication.showMessage(Feedback.this,"Feedback wasn't submitted. Try again");
                    hideProgress();
                }
            }
        }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
