package com.hensongeodata.myagro360v2.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.hensongeodata.myagro360v2.Interface.ControlListener;
import com.hensongeodata.myagro360v2.Interface.NetworkResponse;
import com.hensongeodata.myagro360v2.Interface.TaskListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.fragment.BSFragmentSimple;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.helper.Constants;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.model.Pauline;
import com.hensongeodata.myagro360v2.model.SettingsModel;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Model.POISModel;
import com.hensongeodata.myagro360v2.version2_0.Model.ScoutModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 7/16/2018.
 */

public class ScoutAutomated extends Chatbot implements TaskListener,NetworkResponse, ControlListener {
    private static String TAG= ScoutAutomated.class.getSimpleName();
    private BottomSheetBehavior mBottomSheetBehavior;
    private  View bottomSheet;
    private View btnBSHandle;
    private boolean bsOpened=false;
    private BSFragmentSimple bsFragmentSimple;

    Pauline pauline;
    ChatbotAdapterRV adapter;
    ArrayList<InstructionScout> itemsVisible;
    LocsmmanEngine locsmmanEngine;
    Runnable runnable;
    Handler handler;
    int index=0;
    int action=0; //number of times an action e.g taking location coordinates has been performed
    public static int OPEN_SCANNER=100;
    private long DELAY_TIME=2000; //delay time is 10 seconds
    public static String scannning_id;
    private TextToSpeech tts;
    private int mode;
    private String form_id;
    private String name;
    private String description;
    private boolean isFrench=false;
    private ArrayList<String> strPOIS;
    private ArrayList<InstructionScout> items;
    private String mapping_id="";
    private final int SPEED_FAST=400;
    private final int SPEED_SLOW=500;
    private int speed=SPEED_SLOW;
    private boolean speedShowing=false;
    private int locSnapshotCount=0;
    private ArrayList<InstructionScout> capturedScouts;
    POI mPoi;
    private boolean allowMapping=false;
    private Runnable runnableLocUpdate,runnableLocCapture;
    private  Handler handlerLocUpdate,handlerLocCapture;
    private boolean ended=false;
    private boolean paused=true;
    private boolean allowSnapLoc=false;
    private long AUTO_SNAP_DELAY=10000;
    private String mapType;
    private String origination;
    private long plotId;
    private String orgId;
    private String userId;
    private long scoutPrimarIyd;
    private String batchId;
    private MapModel mapModel;

    private MapDatabase mapDatabase;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSystemUI();

        mapModel = new MapModel();

        mode=getIntent().getIntExtra("mode",-1);
        name= getIntent().getStringExtra("name");
        description = getIntent().getStringExtra("description");
        mapType      = getIntent().getStringExtra("map_type");
        origination   = getIntent().getStringExtra("origination");
        plotId   = getIntent().getLongExtra("plot_id", -1);
        orgId     = SharePrefManager.getInstance(this).getOrganisationDetails().get(0);
//        userId     = SharePrefManager.getInstance(this).getUserDetails().get(0);

        if (SharePrefManager.isSecondaryUserAvailable(getApplicationContext())){
            userId = SharePrefManager.getSecondaryUserPref(getApplicationContext());
        }else {
            userId = SharePrefManager.getInstance(getApplicationContext()).getUserDetails().get(0);
        }

        form_id=getIntent().getStringExtra("form_id");
        strPOIS=new ArrayList<>();
        items=new ArrayList<>();
        capturedScouts =new ArrayList<>();
        mapDatabase = MapDatabase.getInstance(this);

        MyApplication.modeScout=mode;
        isFrench=settings.getLang().equalsIgnoreCase(SettingsModel.LAN_FR);
        tts=MyApplication.tts;
        String caption="Assistant";
        if(mode==ChatbotAdapterRV.MODE_SCOUT)
            caption=isFrench?"Assistant scout":"Scouting assistant";

        else if(mode==ChatbotAdapterRV.MODE_MAP)
                caption=isFrench?"Assistant de carte":"Mapping assistant";

        tvCaption.setText(caption);
        locsmmanEngine = new LocsmmanEngine(this);
        pauline = new Pauline(this);
        pauline.setTaskListener(this);
        //mapModel.id=pauline.getScoutID();
        mapModel.type=MapModel.TYPE_POINT;

        MyApplication.initLive(this,pauline.getTaskListener());
        MyApplication.refreshScanService(this);

        showButton();//hides all buttons

        itemsVisible = new ArrayList<>();

        if(mode==ChatbotAdapterRV.MODE_SCOUT)
                itemsVisible = pauline.getChatScout();
        else if(mode==ChatbotAdapterRV.MODE_MAP)
                itemsVisible=pauline.getChatMap();

        adapter = new ChatbotAdapterRV(this);
        adapter.setMode(mode);
        adapter.setControlListener(this);
        recyclerView.setAdapter(adapter);


        if (mode == ChatbotAdapterRV.MODE_SCOUT){
            AppExecutors.getInstance().getDiskIO().execute(() -> {

//                String batchId = "";

                if(index<itemsVisible.size()){
                    batchId =             itemsVisible.get(index).getId();
                }

                ScoutModel scoutModel = new ScoutModel();
                scoutModel.setOrgId(orgId);
                scoutModel.setUserId(userId);
                scoutModel.setPlotId(plotId);
                scoutModel.setBatchId(batchId);
                Log.i(TAG, "run:  Scout Model " + batchId);
                scoutModel.setDateCreated(System.currentTimeMillis());

                scoutPrimarIyd = mapDatabase.scoutDaoAccess().insert(scoutModel);

                Log.i(TAG, "run:  Scout Primary ID " + scoutPrimarIyd ) ;
            });

        }

        handler=new Handler();

        runnable=new Runnable() {
            @Override
            public void run() {
           processItem();
            }
        };

        handler.postDelayed(runnable,1000);

        ibTopRight.setImageResource(R.drawable.ic_close_dark);

        ibTopRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mode==ChatbotAdapterRV.MODE_MAP&&locSnapshotCount>0){
//                    databaseHelper.addMapping(mapModel);
                }
                else if(mode==ChatbotAdapterRV.MODE_SCOUT){
                    close();
                }
                else {
                finish();
                }
            }
        });

        toggleSpeaker();

        FragmentManager fm = getSupportFragmentManager();
        bsFragmentSimple= (BSFragmentSimple) fm.findFragmentById(R.id.frag_bsfragment);
        if(mode==ChatbotAdapterRV.MODE_MAP)
            initBottomSheet(true);
        else
            initBottomSheet(false);

        handlerLocUpdate=new Handler();

        runnableLocUpdate=new Runnable() {
            @Override
            public void run() {
                updateLocation();
                handlerLocUpdate.postDelayed(this,AUTO_SNAP_DELAY);
            }
        };

        handlerLocUpdate.postDelayed(runnableLocUpdate,AUTO_SNAP_DELAY);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLocationUpdate(POI poi){
        mPoi=poi;
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private boolean allow(POI poi1,POI poi2){
        List<POI> pois=new ArrayList<>();
        if(poi1==null||poi2==null){
            Log.d(TAG, "distance: null");
            return true;
        }
        else {
            pois.add(poi1);
            pois.add(poi2);
            Log.d(TAG, "distance: " + MyApplication.getPerimeter(pois));
            return MyApplication.getPerimeter(pois) >= 2;
        }
    }

    private void updateLocation(){
            if (mode == ChatbotAdapterRV.MODE_MAP && locSnapshotCount > 0 && shouldProcess() && !ended && !paused) {
                Toast.makeText(ScoutAutomated.this, "Captured", Toast.LENGTH_SHORT).show();

                 //   handlerLocCapture.postDelayed(runnableLocCapture, AUTO_SNAP_DELAY);
                    snapshotLocationAutomated();
            }
    }


    private void processItem(){
        if(shouldProcess()) {
            Log.d(TAG, "process item");
            InstructionScout instruction = null;
            if (index < itemsVisible.size())
                instruction = itemsVisible.get(index);
            else
                end();

            if (instruction != null) {
                adapter.addItem(instruction);
                scroll(adapter.getItemCount() - 1);

                if(instruction.getAction()==InstructionScout.ACTION_SCAN&&mode==ChatbotAdapterRV.MODE_MAP)
                    speak(isFrench? resolveString(R.string.ai_map_attached_fr):resolveString(R.string.ai_map_attached));
                else if (instruction.getMessage() != null) speak(instruction.getMessage());
            }
            index++;

            if (instruction != null) {
                if (instruction.getAction() == InstructionScout.ACTION_TASK) {
                    speedShowing=false;
                    //show ok button
                    showButton(btnOk.getId());

                    btnOk.setText(isFrench? resolveString(R.string.yes_fr): resolveString(R.string.yes_));

                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(mode==ChatbotAdapterRV.MODE_SCOUT)
                                    openScanner();
                            else if(mode==ChatbotAdapterRV.MODE_MAP)
                                    snapshotLocation();
                        }
                    });

                    if(mode==ChatbotAdapterRV.MODE_MAP)
                        if(locSnapshotCount==0) {
                            showButton(btnOk.getId(), btnPositive.getId());
                        }
                        else {
                            btnOk.setText(paused?"Continue":"Pause");
                            btnOk.setOnClickListener(paused? proceed: pause);
                            showButton(btnOk.getId(),btnPositive.getId());
                        }

                    btnPositive.setText(isFrench?resolveString(R.string.ai_map_done_fr):resolveString(R.string.ai_map_done));

                    btnPositive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                if(mode==ChatbotAdapterRV.MODE_MAP) {
                                    //snapshotLocation();
                                    ended=true;
                                    submitMapping();
                                    //databaseHelper.addScoutItems(capturedScouts,mode);
                                }
                                else
                                    finish();
                        }
                    });

                    stopHandler();
                }
                else {
                    if(!speedShowing) {
                        showButton(btnOk.getId());
                        speedShowing=true;
                    }
                    btnOk.setText(speed==SPEED_SLOW? resolveString(R.string.speed_up):resolveString(R.string.slow_down));
                    btnOk.setOnClickListener(speedListener);
                    handler.postDelayed(runnable, DELAY_TIME);
                    //showButton();//empty argument means hide all buttons
                }
            }
        }
    }

    View.OnClickListener pause=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            btnOk.setOnClickListener(proceed);
            paused=true;
            btnOk.setText("Continue");
        }
    };

    View.OnClickListener proceed=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            btnOk.setOnClickListener(pause);
            paused=false;
            btnOk.setText("Pause");
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        refresh();
        if(adapter!=null)
            adapter.notifyDataSetChanged();
    }

    private void refresh(){
       // MainActivityAutomated.mapModel=new MapModel();
       // MainActivityAutomated.pois=new ArrayList<>();
    }

    private void openScanner(){
        Intent i=new Intent(ScoutAutomated.this,CameraScanSimple.class);
        InstructionScout instructionScout=null;

        if(index<itemsVisible.size())
              instructionScout=itemsVisible.get(index);

        Log.i(TAG, "run:  Scout Model " + itemsVisible.get(index).getId());

        if(instructionScout!=null&&instructionScout.getId()!=null) {
            i.putExtra("item_id",instructionScout.getId());
            i.putExtra("map_id",instructionScout.getScout_id());
            i.putExtra("point_number",instructionScout.getPoint_number());
            i.putExtra("plant_number",instructionScout.getPlant_number());
            i.putExtra("plot_id", plotId);
            i.putExtra("scout_primary_id", scoutPrimarIyd);
            i.putExtra("batch_id", scoutPrimarIyd);
            stopHandler();
            startActivityForResult(i, OPEN_SCANNER);
        }
    }

    private void snapshotLocationAutomated() {

        POI poi = getPOI();

        Log.d(TAG,"snapshot");
        if (poi != null) {
            Log.d(TAG,"before poi");
            String accuracy = poi.getAccuracy();
            if (accuracy != null && !accuracy.trim().equalsIgnoreCase("null")
                    && !accuracy.trim().isEmpty()) {
                Log.d(TAG,"before accuracy");
                if (Integer.parseInt(accuracy) <= settings.getAccuracy_max()) {
                    Log.d(TAG,"after accuracy");
                    String message = poi.getLat() + "#" + poi.getLon() + "#" + poi.getAccuracy();
                    strPOIS.add(message);
                    Log.d(TAG, "Message :  " + message);
                    InstructionScout instructionScout = null;
                    if (itemsVisible.size() > adapter.getItemCount())
                        instructionScout = itemsVisible.get(adapter.getItemCount());

                    Log.d(TAG, "form_id: " + form_id + " instructionScount: " + instructionScout);
                    if (instructionScout != null) {
                        if (form_id != null)
                            instructionScout.setScout_id(instructionScout.getScout_id() + "_" + form_id);

                        mapping_id = instructionScout.getScout_id();
                        mapModel.id=mapping_id;
                        instructionScout.setMessage(message);
                        itemsVisible.set(adapter.getItemCount(), instructionScout);

                        instructionScout.setStatus(InstructionScout.STATE_UNPROCCESSED);

                        action++;
                        Log.d(TAG, "before add scoutItem");
                        databaseHelper.addScoutItem(instructionScout, mode);
//                        instructionScout.setId(generateID());
                       // capturedScouts.add(instructionScout);
                        items.add(instructionScout);
                        locSnapshotCount++;
                        if(locSnapshotCount==2)
                            rbLine.performClick();

//                            mapModel.setType(mapType);

                        if(locSnapshotCount==3)

                            showMappingType();

//                        mapModel.setType(mapType);

                        handler.postDelayed(runnable, 1000);
                    } else {

                        MyApplication.showToast(this,
                                "Ensure accuracy is below " + (settings.getAccuracy_max() + 1)
                                        + " before you continue", Gravity.CENTER);
                        btnBSHandle.performClick();
                    }
                } else {
                    btnBSHandle.performClick();
                }
            } else
                btnBSHandle.performClick();
        }
    }

    POI getPOI(){
        POI poi=new POI();
        if(mPoi!=null){
            poi.setAccuracy(mPoi.getAccuracy());
            poi.setId(mPoi.getId());
            poi.setLat(mPoi.getLat());
            poi.setLon(mPoi.getLon());
            poi.setAlt(mPoi.getAlt());

            String strAccur = poi.getAccuracy();
            if (strAccur != null && !strAccur.trim().isEmpty()) {
                Double accuracy = Math.ceil(Double.valueOf(strAccur));
                String accur = String.valueOf(accuracy.intValue());
                poi.setAccuracy(accur);
            }
        }

        return poi;
    }

    private void snapshotLocation() {
       // POI poi = bsFragmentSimple.getPoi();
        POI poi = getPOI();
Log.d(TAG,"snapshotLocation");

        if (poi != null) {
            String accuracy = poi.getAccuracy();
            if (accuracy != null && !accuracy.trim().equalsIgnoreCase("null")
                    && !accuracy.trim().isEmpty()) {

                if (Integer.parseInt(accuracy) <= settings.getAccuracy_max()) {
                    String message = poi.getLat() + "#" + poi.getLon() + "#" + poi.getAccuracy();
                    strPOIS.add(message);
                    Log.d(TAG, "date: " + message);
                    InstructionScout instructionScout = null;
                    if (itemsVisible.size() > adapter.getItemCount())
                        instructionScout = itemsVisible.get(adapter.getItemCount());

                    Log.d(TAG, "form_id: " + form_id + " instructionScount: " + instructionScout);
                    if (instructionScout != null) {
                        if (form_id != null)
                            instructionScout.setScout_id(instructionScout.getScout_id() + "_" + form_id);

                        mapping_id = instructionScout.getScout_id();
                        mapModel.id=mapping_id;
                        instructionScout.setMessage(message);
                        itemsVisible.set(adapter.getItemCount(), instructionScout);

                        instructionScout.setStatus(InstructionScout.STATE_UNPROCCESSED);

                        action++;
                        Log.d(TAG, "before add scoutItem");
                        databaseHelper.addScoutItem(instructionScout, mode);
                        //instructionScout.setId(generateID());
                       // capturedScouts.add(instructionScout);
                        items.add(instructionScout);
                        locSnapshotCount++;

                        if(locSnapshotCount==2)
                            rbLine.performClick();

                        if(locSnapshotCount==3)
                            showMappingType();

                        handler.postDelayed(runnable, 1000);
                    } else {
//
                        MyApplication.showToast(this,
                                "Ensure accuracy is below " + (settings.getAccuracy_max() + 1)
                                        + " before you continue", Gravity.CENTER);
                        btnBSHandle.performClick();
                    }
                } else {
                    btnBSHandle.performClick();
                }
            } else
                btnBSHandle.performClick();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== Activity.RESULT_OK){
            if(requestCode==OPEN_SCANNER){
                int success=data.getIntExtra("success",0);
                Log.d(TAG,"success: "+success);
                if(success==1) {
                    if(MyApplication.itemsLivePosition!=null)
                        MyApplication.itemsLivePosition.add(adapter.getItemCount());

                    handler.postDelayed(runnable, 1000);
                }
                MyApplication.refreshScanService(this);
            }
        }
        else {}
    }

    public void end(){
       showButton(btnNegative.getId());
       btnNegative.setText(isFrench?resolveString(R.string.ai_scout_done_fr):resolveString(R.string.ai_scout_done));
       btnNegative.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               finish();
           }
       });
    }

    private void stopHandler(){
        Log.d(TAG,"removing handler...");
        if(handler!=null) handler.removeCallbacks(runnable);
    }

    @Override
    public void onTaskAccepted(String id) {
        //id is the position of the task in the list
    }

    @Override
    public void onTaskStarted(String id) {

    }

    @Override
    public void onTaskStopped(String id, boolean complete) {
       Log.d(TAG,"task complete "+complete);
        if(complete){
            InstructionScout instructionScout=databaseHelper.getInstructScout(id,mode);
            Log.d(TAG,"instruction: "+instructionScout+" id "+instructionScout.getId());
            if(instructionScout!=null){
                    int index=getIndex(itemsVisible,instructionScout);
                    if(index>=0)
                        adapter.updateItem(index,instructionScout);
            }
        }
    }

    @Override
    public void onTaskCancelled(String id) {

    }

    @Override
    protected void onDestroy() {
        if(MyApplication.tts!=null){
            MyApplication.tts.stop();
        }
        super.onDestroy();
        stopHandler();
        MyApplication.shutdownLive();
        if(handlerLocUpdate!=null)
            handlerLocUpdate.removeCallbacks(runnableLocUpdate);
    }

    private boolean shouldProcess(){
        if(speed==SPEED_SLOW&&MyApplication.tts!=null&&MyApplication.ttsAvailable&&speakEnabled&&MyApplication.tts.isSpeaking()){
            handler.postDelayed(runnable, 1000);
            Log.d(TAG,"speaking...");
            return false;
        }

        return true;
    }

    private void initBottomSheet(boolean init){

        bottomSheet = findViewById(R.id.bottom_sheet);

        if(!init) bottomSheet.setVisibility(View.GONE);

        btnBSHandle=bottomSheet.findViewById(R.id.bg_bottomsheet_handle);
        btnBSHandle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int bsState=mBottomSheetBehavior.getState();
                if(bsState==BottomSheetBehavior.STATE_COLLAPSED){
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                else if(bsState==BottomSheetBehavior.STATE_EXPANDED) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });

        //  View bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setPeekHeight(70);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                //  Toast.makeText(getActivity(),"bs callback",Toast.LENGTH_SHORT).show();
                if(newState==BottomSheetBehavior.STATE_COLLAPSED) {
                   // ibBSHandle.setImageResource(R.drawable.ic_arrow_up);
                    bsOpened=false;
                }
                else if (newState==BottomSheetBehavior.STATE_EXPANDED) {
                //    ibBSHandle.setImageResource(R.drawable.ic_arrow_down);
                    bsOpened=true;
                }
                //bsOpened=!bsOpened;
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        bottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickLocationPermission();
                //Toast.makeText(ScanModel.this,"Refreshing location updates...",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void speak(CharSequence text){
        if (MyApplication.ttsAvailable&&speakEnabled&&Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                tts.speak(text,TextToSpeech.QUEUE_FLUSH,null,"id1");
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void close(){
        Intent i=new Intent();
        setResult(Activity.RESULT_OK,i);
        finish();
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    public static int getIndex(ArrayList<InstructionScout> items,InstructionScout instructionScout){
        int index=-1;
        if(MyApplication.itemsLivePosition!=null&&MyApplication.itemsLivePosition.size()>0){
            index=MyApplication.itemsLivePosition.get(0);
            MyApplication.itemsLivePosition.remove(0);
        }
        else {
            index=(new MyApplication()).containsInstructScout(items, instructionScout);
        }

        return index;
    }

    private void showPop(String caption){

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.map_save_menu);

        // set the custom dialog components - text, image and button
          TextView tvCaption = dialog.findViewById(R.id.tv_caption);
        tvCaption.setText(caption);
        dialog.findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//            submitMapping();
//            dialog.cancel();
            dialog.dismiss();
            finish();
            }
        });

//        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                close();
//                dialog.cancel();
//            }
//        });

        dialog.show();
    }

    private void submitMapping(){
        Log.d(TAG,"xxx submitMapping action: "+action);
        mapModel.setName(name);
        mapModel.setSynced(true);

       String orgId =  SharePrefManager.getInstance(this).getOrganisationDetails().get(0);
//       String userId =  SharePrefManager.getInstance(this).getUserDetails().get(0);
        mapModel.setOrgId(orgId);
        mapModel.setUserId(userId);

            if (mode == ChatbotAdapterRV.MODE_MAP && strPOIS!=null&&strPOIS.size() > 0) {
                Log.d(TAG,"strPOIS.size: "+strPOIS.size());
                int indx=0;

                AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        for(String poi:strPOIS){
                            Log.d(TAG,index+": "+poi);
                            POISModel poisModel = new POISModel();
                            poisModel.setInfo(poi);
                            poisModel.setMapId(mapModel.id);
                            mapDatabase.poisDaoAccess().insert(poisModel);
                        }

                        ArrayList<POI> pois= CreateForm.getInstance(getApplicationContext()).buildPOI_Mapping(strPOIS);
                            int area=MyApplication.getArea(pois);
//
                            long perimeter = MyApplication.getPerimeter(pois);
                            double inKilometers = metersToKilo(perimeter);
                            double acreage = metersToAcres(perimeter);

                            mapModel.setSizeAc(acreage);
                            mapModel.setSizeM(perimeter);
                            mapModel.setSizeKm(inKilometers);

                        mapDatabase.daoAccess().insert(mapModel);
                    }
                });

//                AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
//                    @Override
//                    public void run() {
//                        try{

//                            List<POISModel> poisModelList = mapDatabase.poisDaoAccess().fetchPOISByMapId(mapModel.getId());
//                            ArrayList<String> strPois = new ArrayList<>();

//                            for (POISModel poisModel : poisModelList) {
//                                if(poisModel!=null) {
//                                    String message = poisModel.getInfo();
//                                    if (message != null && !message.trim().isEmpty())
//                                        strPois.add(message);
//
//                                    Log.d(TAG, "run: show Perimeter:  " + message);
//                                }
//                            }

//                            ArrayList<POI> pois= CreateForm.getInstance(getApplicationContext()).buildPOI_Mapping(strPois);
//                            int area=MyApplication.getArea(pois);
//
//                            long perimeter = MyApplication.getPerimeter(pois);
//                            double inKilometers = metersToKilo(perimeter);
//                            double acreage = metersToAcres(perimeter);
//
//                            mapModel.setSizeAc(acreage);
//                            mapModel.setSizeM(perimeter);
//                            mapModel.setSizeKm(inKilometers);

//                        }catch (Exception e){
//
//                            Log.e(TAG, "run: SyncMaps loop  " + e.getMessage());
//                        }
//                    }
//                });

//                AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
//                    @Override
//                    public void run() {
//                        mapDatabase.daoAccess().insert(mapModel);
//                    }
//                });

//                if (ended){
//                    backToSource();
//                }

                if(myApplication.hasNetworkConnection(this)) {
                    try {
                        showProgress("Saving your work. Please wait...");
//                        networkRequest.sendFAWMappingBulk(
//                                createForm.buildJSON_Mapping(
//                                createForm.buildPOI_Mapping(strPOIS)),
//                                mapping_id,mapModel.type,this, mapModel.name, mapModel.getSizeAc());

                        networkRequest.sendFAWMappingBulk(
                                createForm.buildJSON_Mapping(
                                        createForm.buildPOI_Mapping(strPOIS)),
                                mapping_id,mapModel.type,this);

                        Log.d(TAG, "submitMapping:  " +   createForm.buildPOI_Mapping(strPOIS).size());

                    } catch (JSONException e) {
                        hideProgress();
                        showPop("Couldn't process your work. Try again. " +
                                "If error persists, kindly restart the mapping procedure");
                        e.printStackTrace();
                    }
                }
                else {
                    hideProgress();
                    showPop("You don't have internet connectivity now. " +
                            "You can activate your internet and try again, or attempt save later");
                }
            } else {
                    finish();
            }
        }

    private void backToSource() {
        Intent intent = new Intent(getApplicationContext(), MainHomePageActivity.class);
        intent.putExtra("origination", Constants.MAPPING_FRAGMENT);
        intent.putExtra("map_id", mapModel.getId());
        startActivity(intent);
    }

    @Override
    public void onRespond(Object object) {

        Log.d(TAG,"object: "+object);
        if(object!=null) {
            if(object instanceof String&&((String) object).equalsIgnoreCase("error")){
                showPop("Connection to server failed. Try again.");
                hideProgress();
                return;
            }

            if(object instanceof String&&((String) object).equalsIgnoreCase("1")) {
                int index = 0;
                for (InstructionScout instructionScout : items) {
                    instructionScout.setStatus(InstructionScout.STATE_PROCCESSED);
                    items.set(index, instructionScout);
                    index++;
                }

                mapModel.setSynced(true);
                Log.i(TAG, "onRespond:  Check Sync " + mapModel.isSynced());
                AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mapDatabase.daoAccess().updateMap(mapModel);
                    }
                });

                databaseHelper.addScoutItems(items, ChatbotAdapterRV.MODE_MAP);
                hideProgress();
                MyApplication.showToast(ScoutAutomated.this,
                        "MapModel saved successfully",
                        Gravity.CENTER);

//                if (origination.equals(Constants.PLOTS_FRAGMENT)) {
                if (SharePrefManager.isDestinationAvailable(this)){
                    backToSource();

                }else{
                    close();
                }
//
            }
            else {
                hideProgress();
                showPop("Couldn't save work. Try again.");
            }
        }
        else {
            hideProgress();
            showPop("Couldn't connect to server. Try again.");
        }
    }


    View.OnClickListener speedListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switchSpeed();
            ((Button)view).setText(speed==SPEED_SLOW? resolveString(R.string.speed_up): resolveString(R.string.slow_down));
        }
    };

    void switchSpeed(){
        if(speed==SPEED_SLOW) {
            speed = SPEED_FAST;
            DELAY_TIME=200;
            if(speakEnabled)
                toggleSpeaker();
        }
        else if(speed==SPEED_FAST) {
            speed = SPEED_SLOW;
            DELAY_TIME=3000;
            if(!speakEnabled)
                toggleSpeaker();
        }
    }

    @Override
    public void pause() {
        if(!paused&&btnOk!=null&&mode==ChatbotAdapterRV.MODE_MAP){
            if(btnOk.getText().equals("Pause")){
                btnOk.setText("Continue");
                btnOk.setOnClickListener(proceed);
            }
        }
        paused=true;

    }

    @Override
    public void play() {
        if(paused&&btnOk!=null&&mode==ChatbotAdapterRV.MODE_MAP){
            if(btnOk.getText().equals("Continue")){
                btnOk.setText("Pause");
                btnOk.setOnClickListener(pause);
            }
        }
        paused=false;

    }

    private static double metersToAcres(long meterSquared){
        return Math.round((meterSquared*0.00024711) * 1000.0) / 1000.0;
    }

    private static double metersToKilo(long metersSquared){
        return metersSquared*0.001;
    }


}
