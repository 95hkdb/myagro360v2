package com.hensongeodata.myagro360v2.view;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Organization;

/**
 * Created by user1 on 1/30/2018.
 */

public class OrgDetail extends BaseActivity {
    private static String TAG=OrgDetail.class.getSimpleName();
    private TextView tvTitle;
    private TextView tvDescription;
    private TextView tvFormCount;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private Organization organization;

    private String org_id;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.org_details);

          toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Organization Details");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

          tvTitle = findViewById(R.id.tv_name);
          tvDescription = findViewById(R.id.tv_description);
          tvFormCount = findViewById(R.id.tv_form_count);

        org_id=getIntent().getExtras().getString("org_id");
        organization=databaseHelper.getOrganization(org_id);

        if(organization!=null){
            tvTitle.setText(organization.getName());
            tvDescription.setText(organization.getDescription());
            tvFormCount.setText(""+organization.getFormCount()+" Forms available");
        }
    }
}
