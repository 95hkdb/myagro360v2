package com.hensongeodata.myagro360v2.view;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.hensongeodata.myagro360v2.Interface.LocalListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.fragment.LangFragment;
import com.hensongeodata.myagro360v2.fragment.Signin;
import com.hensongeodata.myagro360v2.fragment.Signup;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.service.NotifyFAW;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 10/14/2017.
 */

public class SigninSignup extends BaseActivity implements LocalListener{
    private static String TAG=SigninSignup.class.getSimpleName();
    private static String ACTION_SIGNIN="signin";
    private static String ACTION_SIGNUP="signup";
    ViewPagerAdapter adapter;
    ViewPager viewPager;
    User user;
    LocalListener localListener;
    View btnSignin;
    TextView tvSignin;
    View indicatorSignin;

    View btnSignup;
    TextView tvSignup;
    View indicatorSignup;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeLogin);
        setContentView(R.layout.signin_signup);
        LangFragment.localListener=this;

        viewPager= (ViewPager) findViewById(R.id.viewpager);
        adapter=new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Signin());
        adapter.addFragment(new Signup());

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);

        btnSignin=(findViewById(R.id.selector_signin)).findViewById(R.id.bg_selector);
        indicatorSignin=(findViewById(R.id.selector_signin)).findViewById(R.id.bg_indicator);
        tvSignin= (TextView) (findViewById(R.id.selector_signin)).findViewById(R.id.tv_label);
        tvSignin.setText(resolveString(R.string.sign_in));

        btnSignup=(findViewById(R.id.selector_signup)).findViewById(R.id.bg_selector);
        indicatorSignup=(findViewById(R.id.selector_signup)).findViewById(R.id.bg_indicator);
        tvSignup= (TextView) (findViewById(R.id.selector_signup)).findViewById(R.id.tv_label);
        tvSignup.setText(resolveString(R.string.sign_up));

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(1);
            }
        });
        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(0);
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==0){
                   toggleSelector(ACTION_SIGNIN);
                }
                else if(position==1){
                    toggleSelector(ACTION_SIGNUP);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        //viewPager.setCurrentItem(0);
        toggleSelector(ACTION_SIGNIN);
        btnSignin.performClick();

        findViewById(R.id.bg_spinner).setBackgroundResource(R.drawable.search_bg_dark);
    }

    public void navigateLogin(){
        indicatorSignin.setBackgroundColor(getResources().getColor(R.color.primaryWhite));
        tvSignin.setTextColor(getResources().getColor(R.color.primaryWhite));

        indicatorSignup.setBackgroundColor(getResources().getColor(R.color.disabledWhite));
        tvSignup.setTextColor(getResources().getColor(R.color.disabledWhite));
    }


    private void toggleSelector(String action){
        if(action.equalsIgnoreCase(ACTION_SIGNIN)){
            indicatorSignin.setBackgroundColor(getResources().getColor(R.color.primaryWhite));
            tvSignin.setTextColor(getResources().getColor(R.color.primaryWhite));

            indicatorSignup.setBackgroundColor(getResources().getColor(R.color.disabledWhite));
            tvSignup.setTextColor(getResources().getColor(R.color.disabledWhite));
        }
        else if(action.equalsIgnoreCase(ACTION_SIGNUP)){
            indicatorSignup.setBackgroundColor(getResources().getColor(R.color.primaryWhite));
            tvSignup.setTextColor(getResources().getColor(R.color.primaryWhite));

            indicatorSignin.setBackgroundColor(getResources().getColor(R.color.disabledWhite));
            tvSignin.setTextColor(getResources().getColor(R.color.disabledWhite));
        }
    }

    @Override
    public void onLanguageChanged(String lan_code) {
        Log.d(TAG,"language changed");
        changeLanguage(lan_code);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return  mFragmentList.get(position);}

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }

        public void addFragment(Fragment fragment){
            mFragmentList.add(fragment);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.firstTime=true;

        if(NotifyFAW.intent!=null) {
            stopService(NotifyFAW.intent);
            MyApplication.online=false;
        }
    }
}

