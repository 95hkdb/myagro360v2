package com.hensongeodata.myagro360v2.view;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.crashlytics.android.Crashlytics;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.MyState;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.controller.Utility;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.Message;
import com.hensongeodata.myagro360v2.model.SettingsModel;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.service.LocationOffline;
import com.hensongeodata.myagro360v2.service.NotifyFAW;
import com.kobakei.ratethisapp.RateThisApp;

import net.alexandroid.gps.GpsStatusDetector;

import io.fabric.sdk.android.Fabric;
import me.pushy.sdk.Pushy;

//import com.crashlytics.android.Crashlytics;
//
//import io.fabric.sdk.android.Fabric;

/**
 * Created by user1 on 1/25/2018.
 */

public class BaseActivity extends LocalizationActivity implements GpsStatusDetector.GpsStatusDetectorCallBack{
    private static String TAG=BaseActivity.class.getSimpleName();
    protected CreateForm createForm;
    protected MyApplication myApplication;
    protected DatabaseHelper databaseHelper;
    protected User user;
    protected MyState myState;
    protected SettingsModel settings;
    protected NetworkRequest networkRequest;
    protected ProgressDialog progressDialog;
    private boolean gpsActivated=true;

    private static int ACTIVATE_GPS=100;
   // private boolean isShowingGPSDialog=false;
    private static final int MY_PERMISSIONS_ACCESS_FINE_LOCATION = 121;
    private int action=-1;
    GpsStatusDetector mGpsStatusDetector;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setDefaultLanguage("en");
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MyApplication.context=this;
        createForm =CreateForm.getInstance(this);
        myApplication=new MyApplication();

        user=new User(this);
        myState=new MyState(this);
        settings=new SettingsModel(this);
        networkRequest=new NetworkRequest(this);
        databaseHelper=new DatabaseHelper(this);

        progressDialog=new ProgressDialog(this);
        progressDialog.setCancelable(false);

        mGpsStatusDetector = new GpsStatusDetector(this);

        if(Utility.checkPermission(this)) {
            pickLocationPermission();
            initPushy();
        }

        RateThisApp.onCreate(this);
    }


    protected void showPreview(int source,String transaction_id){
        if((new SettingsModel(this)).isPreviewForm()) {
            Intent i = new Intent(this, PreviewForm.class);
            i.putExtra("transaction_id", transaction_id);
            i.putExtra("source",source);
            startActivity(i);
        }
        else {
            Toast.makeText(this,"Submit directly",Toast.LENGTH_SHORT).show();
        }
    }

    private void stopNotifyFAW(){
        Log.d(TAG,"stopNotifyFaw intent: "+NotifyFAW.intent);
        if(NotifyFAW.intent!=null){
            stopService(NotifyFAW.intent);
        }
    }

    protected void showProgress(String message){
        progressDialog.setMessage(message);

        if(progressDialog!=null)
                progressDialog.show();
    }

    protected void hideProgress(){
        if(progressDialog.isShowing())
                progressDialog.hide();

    }

    protected ProgressDialog getProgress(){
        return progressDialog;
    }

    public void pickLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(this, resolveString(R.string.notif_geo), Toast.LENGTH_SHORT).show();
            }
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},  MY_PERMISSIONS_ACCESS_FINE_LOCATION );
        } else {
            checkGpsStatus();
        }
    }

    public void checkGpsStatus() {

     /*   LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
*/
        mGpsStatusDetector.checkGpsStatus();
    }

    public static void startLocationOffline(Context context){
       // if(stopLocationOffline(context))
        //    MyApplication.showToast(context,context.getResources().getString(R.string.notif_refresh_location),Gravity.CENTER);

        Intent intent=new Intent(context, LocationOffline.class);
        context.startService(intent);
        LocationOffline.intent=intent;
    }

    private static boolean stopLocationOffline(Context context){
        if(LocationOffline.started&&LocationOffline.intent!=null) {
            context.stopService(LocationOffline.intent);
            return true;
        }
        return false;
    }

    private void settingsGPS(){
        Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
        action=ACTIVATE_GPS;
    }

    public int getAccuracyColor(Context context,int accuracy){
       return accuracy>(new SettingsModel(context)).getAccuracy_max()?
                ContextCompat.getColor(context,android.R.color.holo_red_light):ContextCompat.getColor(context, R.color.colorAccent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PERMISSIONS_ACCESS_FINE_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finish();
            }
            checkGpsStatus();
            initPushy();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mGpsStatusDetector.checkOnActivityResult(requestCode, resultCode);
    }

    protected void shareApp(){
        Intent i=new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
          i.putExtra(Intent.EXTRA_TEXT, "MyAgro360.\nYour no. 1 Farm Management and & Pest Detection Mobile App\n" +
                "Download the App from here "+MyApplication.APP_URL);

        startActivity(Intent.createChooser(i,"Share App via"));
    }

    protected void exit(final Context context){
        new AlertDialog.Builder(context)
                .setTitle("Confirm Logout")
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(myApplication.logout(context)){
                            Intent intent=new Intent(context,SigninSignup.class);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            Toast.makeText(context,"You are logged out successfully",Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else {
                            Toast.makeText(context,"Failed to logout",Toast.LENGTH_SHORT).show();
                        }
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.cancelToast();
       // checkGpsStatus();
        stopNotifyFAW();
        MyApplication.online=true;
    }

    private void initPushy(){
        Pushy.listen(this);
    }

    protected String resolveString(int id){
        return  getResources().getString(id);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }

    public void changeLanguage(String lan_code){
        if(lan_code!=null) {
         String code="";
         if(lan_code.equalsIgnoreCase(resolveString(R.string.french)))
             code="fr";
         else if(lan_code.equalsIgnoreCase(resolveString(R.string.english)))
             code="en";
         else if(lan_code.equalsIgnoreCase(resolveString(R.string.swahili)))
             code="sw";

         Log.d(TAG,"code: "+code);
         if(!code.trim().isEmpty())
                    setLanguage(code);
        }
    }

    private void syncSystemLanguage(){
        String systemLanguage= Resources.getSystem().getConfiguration().locale.getLanguage();
        Log.d(TAG,"system lang: "+systemLanguage);
        if(systemLanguage!=null&&
                (systemLanguage.equalsIgnoreCase("en")||systemLanguage.equalsIgnoreCase("fr"))){

            String currentLanuage=null;
            if(getCurrentLanguage()!=null)
                currentLanuage=getCurrentLanguage().toString();

            Log.d(TAG,"current lang: "+currentLanuage);
            if(currentLanuage!=null&&!currentLanuage.equalsIgnoreCase(systemLanguage)){
                Log.d(TAG,"current lang!=system lang");
                setLanguage(systemLanguage);
            }

        }
    }


    public static void sharetoChat(Context context, Image image, String text){
        Intent intent=new Intent(context,Chat.class);
        intent.putExtra(Chat.SOURCE,Chat.SOURCE_SHARE);
        Message message=new Message();
        Log.d(TAG,"baseactivity share: "+image.getImage_url());
        message.setImage(image);
        message.setAuthor(Chat.getAuthor(context));
            message.setText(text);

        MyApplication.xternalMessage=message;

        context.startActivity(intent);
    }

    public static void sharetoChat(Context context, Image image, String text,boolean clear_stack){
        Intent intent=new Intent(context,Chat.class);
        if(clear_stack)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Chat.SOURCE,Chat.SOURCE_SHARE);
        Message message=new Message();
//        Log.d(TAG,"baseactivity share: "+image.getImage_url());
        message.setImage(image);
        message.setAuthor(Chat.getAuthor(context));
        message.setText(text);

        MyApplication.xternalMessage=message;
                context.startActivity(intent);
    }


   /* private void checkGPS_playservices(){
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // All location settings are satisfied. The client can initialize
                // location requests here.
                // ...
            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(BaseActivity.this,
                                REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.online=false;
    }

    @Override
    public void onGpsSettingStatus(boolean enabled) {
        if(enabled) {
            startLocationOffline(this);
            action=-1;
            if(!gpsActivated)
                MyApplication.showToast(this,resolveString(R.string.notif_gps),Gravity.CENTER);

            gpsActivated=true;
        }
        else
            checkGpsStatus();
    }

    @Override
    public void onGpsAlertCanceledByUser() {
        checkGpsStatus();
    }

}

/*
* if (!enabled){
           // isShowingGPSDialog=true;
            new AlertDialog.Builder(this)
                    .setTitle(resolveString(R.string.notif_gps_caption))
                    .setMessage(resolveString(R.string.notif_gps_message))
                    .setNegativeButton(resolveString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onAddMapButtonClicked(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
            //                isShowingGPSDialog=false;
                            settingsGPS();
                        }
                    })
                    .show();

            gpsActivated=false;
        }
* */