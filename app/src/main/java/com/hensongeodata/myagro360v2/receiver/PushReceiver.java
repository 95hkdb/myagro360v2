package com.hensongeodata.myagro360v2.receiver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.os.PowerManager;
import android.os.Vibrator;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.google.gson.Gson;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Author;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.Message;
import com.hensongeodata.myagro360v2.model.PaymentReceipt;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.service.NotifyFAW;
import com.hensongeodata.myagro360v2.version2_0.Broadcast.MyBroadcastReceiver;
import com.hensongeodata.myagro360v2.version2_0.Events.NewAlertEvent;
import com.hensongeodata.myagro360v2.version2_0.Model.AppNotification;
import com.hensongeodata.myagro360v2.version2_0.Model.Post;
import com.hensongeodata.myagro360v2.version2_0.NotificationDetailActivity;
import com.hensongeodata.myagro360v2.view.Chat;
import com.hensongeodata.myagro360v2.view.MenuDashboard;
import com.hensongeodata.myagro360v2.view.Receipts;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.UUID;

import me.pushy.sdk.Pushy;

/**
 * Created by user1 on 6/21/2018.
 */

public class PushReceiver extends BroadcastReceiver {
    public static int SOURCE_PUSHY=100;
    private static String TYPE_MESSAGE="message";
    private static String TYPE_FAW_NOTIFY="faw_notify";
    private static final String NOTIFICATION_DELETED_ACTION = "NOTIFICATION_DELETED";
    private static String TAG = PushReceiver.class.getSimpleName();
    private static int message_count=0;
    private static int max_message=5;
    private static final long[] DEFAULT_VIBRATION_PATTERN =  {0, 250, 250, 250};

//    @Override
//    public void onReceive(Context context, Intent intent) {
//        if(intent!=null) {
//            String type=intent.getStringExtra("type");
//
//            if(type!=null&&type.trim().equalsIgnoreCase(TYPE_MESSAGE)){
//                wakescreen(context, PowerManager.FULL_WAKE_LOCK);
//                // wakescreen(context, PowerManager.PARTIAL_WAKE_LOCK);
//             pushMessage(context,intent);
//            }
//            else {
//                wakescreen(context, PowerManager.FULL_WAKE_LOCK);
//                showNotification(context,
//                        intent.getStringExtra("title"),
//                        intent.getStringExtra("date"));
//
//            }
//            }
//        }

    public static void showNotification(Context context, String title, String message) {

        if(title==null||title.trim().equalsIgnoreCase(""))
            title="Fall Armyworm detection alert!";

        // Prepare a notification with vibration, sound and lights
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.logo)
                .setContentTitle(title)
                .setContentText(message)
                .setLights(Color.RED, 1000, 1000)
                .setVibrate(new long[]{0, 400, 250, 400})
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM))
                .setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, MenuDashboard.class), PendingIntent.FLAG_UPDATE_CURRENT));
//        Intent resultIntent = new Intent(context, DashboardFAW.class);
//        resultIntent.putExtra("source",SOURCE_PUSHY);

        //TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        //taskStackBuilder.addParentStack(Home.class);
        //taskStackBuilder.addNextIntent(resultIntent);
        //PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        //mBuilder.setContentIntent(pendingIntent);

        // Automatically configure a Notification Channel for devices running Android O+
        Pushy.setNotificationChannel(mBuilder, context);

        Intent intent = new Intent(NOTIFICATION_DELETED_ACTION);
        PendingIntent toneIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        context.getApplicationContext().registerReceiver(receiver, new IntentFilter(NOTIFICATION_DELETED_ACTION));
        mBuilder.setDeleteIntent(toneIntent);

        // Get an instance of the NotificationManager service
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // Build the notification and display it
        notificationManager.notify(1, mBuilder.build());

        NotifyFAW.intent=new Intent(context,NotifyFAW.class);
        context.startService(NotifyFAW.intent);

        Log.d(TAG,"notified");
    }

    public static void wakescreen(Context context, int wake_lock_type){
        PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        Log.e("screen on.", ""+isScreenOn);
        if(isScreenOn==false)
        {
            PowerManager.WakeLock wl = pm.newWakeLock(wake_lock_type |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,TAG+": MyLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,TAG +": MyCpuLock");
            wl_cpu.acquire(10000);
        }
    }

    private static final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(NotifyFAW.tone!=null)
                NotifyFAW.tone.stop();
            if(NotifyFAW.vibrator!=null)
                NotifyFAW.vibrator.cancel();

        Log.d(TAG,"destroyed receiver");
        }
    };


    public static void pushMessage(Context context,Intent intent){
        Log.d(TAG,"push message");
        Message message=new Message();
        Author author=new Author();

        message.setText(intent.getStringExtra("message"));
        Image image=new Image();
        image.setImage_url(intent.getStringExtra("img_url"));
        message.setImage(image);
        message.setCreatedAt(new Date());
        message.setId(intent.getStringExtra("id"));
        message.setGroup_id(intent.getStringExtra("room_id"));

        author.setId(intent.getStringExtra("user_id"));
        author.setName(intent.getStringExtra("user_name"));

       // author.setId("test_response");
        message.setAuthor(author);
        Log.d(TAG,"group_id: "+message.getGroup_id());
        Log.d(TAG,"text: "+message.getText());
        Log.d(TAG,"image_url: "+message.getImageUrl());

       // message.getUser().setId("wence");
        User user=new User(context);
        Log.d(TAG,message.getUser().getId()+" user_id "+user.getId());
       // message.getUser().setId("wence");
        message.setStatus(Message.STATUS_PENDING);
        if(message.getUser().getId()!=null&&!message.getUser().getId().equalsIgnoreCase(user.getId())) {
            boolean hasGroup=false;
            if(message.getGroup_id()!=null) {
                for (String room : user.getChatRoom_ids()) {
                    Log.d(TAG,message.getGroup_id()+" compare "+room);
                    if (message.getGroup_id().equalsIgnoreCase(room))
                        hasGroup = true;
                }

                Log.d(TAG,"hasgroup: "+hasGroup);
                if (hasGroup) {
                    message.setId( UUID.randomUUID().toString().replaceAll("-", "").toUpperCase());
                    Log.d(TAG,"message_id: "+message.getId());
                    (new DatabaseHelper(context)).addMessage(message);

                   if(message.getGroup_id()!=null&&message.getGroup_id().equalsIgnoreCase(user.getChatroomActive_id()))
                          BroadcastCall.publishMessage(context, message.getId(), NetworkRequest.STATUS_SUCCESS);

                   if(!MyApplication.in_chat&&(new MyApplication()).hasSession(context))
                       showMessageNotification(context,message);

                   Log.d(TAG,"online: "+MyApplication.online);
                }
            }
            }

        }

    private static void showMessageNotification(Context context,Message message) {
        String room_id=message.getGroup_id();
        String room_name=context.getResources().getString(R.string.app_name);
        if(MyApplication.isInteger(room_id))
            room_name=Chat.getChatroomName(context,Integer.parseInt(room_id));

        Intent resultIntent = new Intent(context, Chat.class);
        resultIntent.putExtra("source",SOURCE_PUSHY);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, resultIntent, 0);
          NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setCategory(Notification.CATEGORY_MESSAGE)
                .setSmallIcon(R.mipmap.logo)
                .setContentTitle(room_name)
                .setContentText(message.getText())
                .setSubText(message.getUser().getName())
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setCategory(Notification.CATEGORY_MESSAGE)
                .addAction(android.R.drawable.ic_menu_view, "View details", contentIntent)
                .setSmallIcon(R.mipmap.logo)
                .setAutoCancel(true)
                .setVibrate(new long[]{1000,1000});

        //.setDefaults(Notification.DEFAULT_ALL) // must requires VIBRATE permission

        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        //taskStackBuilder.addParentStack(Home.class);
        taskStackBuilder.addNextIntent(resultIntent);
        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(pendingIntent);

        Intent intent = new Intent(NOTIFICATION_DELETED_ACTION);
        PendingIntent toneIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        context.getApplicationContext().registerReceiver(receiver, new IntentFilter(NOTIFICATION_DELETED_ACTION));
        mBuilder.setDeleteIntent(toneIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(10, mBuilder.build());

        //vibrate(context);
        playtone(context,R.raw.message_new);

        Log.d(TAG,"notified");
    }

    public static void showReceiptNotification(Context context, PaymentReceipt receipt) {

        Intent resultIntent = new Intent(context, Receipts.class);
        resultIntent.putExtra("source",SOURCE_PUSHY);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, resultIntent, 0);
          NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setCategory(Notification.CATEGORY_MESSAGE)
                .setSmallIcon(R.mipmap.logo)
                .setContentTitle("PFJ Shop Order")
                .setContentText("Order # "+receipt.order_number)
                .setSubText("Status: "+receipt.order_status)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setCategory(Notification.CATEGORY_MESSAGE)
                .addAction(android.R.drawable.ic_menu_view, "View details", contentIntent)
                .setSmallIcon(R.mipmap.logo)
                .setAutoCancel(true)
                .setVibrate(new long[]{1000,1000});

        //.setDefaults(Notification.DEFAULT_ALL) // must requires VIBRATE permission

        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        //taskStackBuilder.addParentStack(Home.class);
        taskStackBuilder.addNextIntent(resultIntent);
        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(pendingIntent);

        Intent intent = new Intent(NOTIFICATION_DELETED_ACTION);
        PendingIntent toneIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        context.getApplicationContext().registerReceiver(receiver, new IntentFilter(NOTIFICATION_DELETED_ACTION));
        mBuilder.setDeleteIntent(toneIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(11, mBuilder.build());

        //vibrate(context);
        playtone(context,R.raw.message_sent);

        Log.d(TAG,"notified");
    }

    public static void vibrate(Context context){
        Vibrator vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(500);
    }

    public static void playtone(Context context,int src){
        if(context!=null) {
            MediaPlayer tone = MediaPlayer.create(context, src);
            tone.setVolume(1.0f, 1.0f);
            tone.setLooping(false);
            tone.start();
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        String notificationTitle = "Pest Detected";
        String notificationText = "Probable Pest Detected";

        if (intent.getStringExtra(TYPE_MESSAGE) != null){
            notificationText = intent.getStringExtra(TYPE_MESSAGE);
            wakescreen(context,PowerManager.FULL_WAKE_LOCK);
            //pushMessage(context,intent);
        }

        showNotification(context,notificationTitle,notificationText);

//        String notificationTitle = "Pest Detected";
//        String notificationText = "Probable Pest Detected";c
//        String postNotificationText = "Message From Post";

        String type = intent.getStringExtra("type");
        String image = intent.getStringExtra("image");
        String link = intent.getStringExtra("link");
        boolean hasLink = intent.getBooleanExtra("has_link", false);
        boolean hasImage =  intent.getBooleanExtra("has_image", false);

        Gson gson = new Gson();
        com.hensongeodata.myagro360v2.version2_0.Model.Message message;
        message = gson.fromJson(intent.getStringExtra("message"), com.hensongeodata.myagro360v2.version2_0.Model.Message.class);

        Post post = new Post();
        post.setMessage(message);
        post.setImage(image);
        post.setHas_image(hasImage);
        post.setLink(link);
        post.setHas_link(hasLink);
        post.setType(type);

        int notificationlId = 1002;
        String channelName = "locationUpdatesChannel";
        String channelId = "locationUpdatesChannelId";

        if (post.getType().equals("post" )){
            notificationText = intent.getStringExtra(TYPE_MESSAGE);

            try {
                NotificationCompat.Style notificationStyle;

                if (post.isHas_image()){
                    Bitmap largeBitmapIcon = processImageUrlToBitmap(post.getImage());
                    notificationStyle = ( new NotificationCompat.BigPictureStyle().bigPicture(largeBitmapIcon));
                }else {
                     notificationStyle = (new NotificationCompat.BigTextStyle().bigText(post.getMessage().getContent()));
                }


                AppNotification appNotification = new AppNotification();
                appNotification.setName(post.getMessage().getTitle());
                appNotification.setMessage(post.getMessage().getContent());
                appNotification.setOwner(AppNotification.NOTIFICATION_POSTS);
                appNotification.setImage(post.getImage());
                appNotification.setHasImage(post.isHas_image());
                appNotification.setHasLink(post.isHas_link());
                appNotification.setLink(post.getLink());
                appNotification.setDateCreated(System.currentTimeMillis());

                AppExecutors.getInstance().getDiskIO().execute(() -> {
                    MapDatabase mapDatabase = MapDatabase.getInstance(context);
                    long id = mapDatabase.notificationDaoAccess().insert(appNotification);

                    Intent notificationIntent = new Intent(context, NotificationDetailActivity.class);
                    notificationIntent.putExtra("id", id);
                    notificationIntent.putExtra("title", post.getMessage().getTitle());
                    notificationIntent.putExtra("content", post.getMessage().getContent());
                    notificationIntent.putExtra("has_image", post.isHas_image());
                    notificationIntent.putExtra("image", post.getImage());
                    notificationIntent.putExtra("link", post.getLink());
                    notificationIntent.putExtra("has_link", post.isHas_link());

                    showNotificationForPost(notificationlId, channelId, channelName,context,  post.getMessage().getContent(), post.getMessage().getTitle(), notificationStyle,notificationIntent );

                    EventBus.getDefault().post(new NewAlertEvent());

                });


            } catch (Exception e) {
                Log.e("PushReceiverClass", "location Updates: " + e.getMessage());

            }
//        }


        }

//        if (intent.getStringExtra(TYPE_MESSAGE) != null){
//            notificationText = intent.getStringExtra(TYPE_MESSAGE);
//            wakescreen(context,PowerManager.FULL_WAKE_LOCK);
//            //pushMessage(context,intent);
//        }

//        showNotification(context,notificationTitle,notificationText);

    }

    public static void showNotificationForPost(int notificationId,String channelId,
                                        String channelName,Context context, String body, String title,  NotificationCompat.Style notificationStyle, Intent intent) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int importance = NotificationManager.IMPORTANCE_HIGH;

        Intent snoozeIntent = new Intent(context, MyBroadcastReceiver.class);
        snoozeIntent.setAction("READ MORE");
//        snoozeIntent.putExtra("title", title);
//        snoozeIntent.putExtra("content", body);
        PendingIntent snoozePendingIntent =
                PendingIntent.getBroadcast(context, 0, snoozeIntent, 0);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        androidx.core.app.NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setVibrate(DEFAULT_VIBRATION_PATTERN)
                .setContentText(body)
                .setStyle(notificationStyle)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setNumber(1)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
                .setAutoCancel(true);

        if (intent != null){
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addNextIntent(intent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );
              mBuilder.addAction(R.drawable.ic_more, "READ MORE",
                      resultPendingIntent);

            mBuilder.setContentIntent(resultPendingIntent);
        }

        notificationManager.notify(notificationId, mBuilder.build());
    }

    private static Bitmap processImageUrlToBitmap(String imageUrl){

        InputStream inputStream;

        try{
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            inputStream = connection.getInputStream();
            return BitmapFactory.decodeStream(inputStream);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
