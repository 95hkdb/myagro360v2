package com.hensongeodata.myagro360v2.receiver;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.hensongeodata.myagro360v2.R;

//import android.support.v7.app.NotificationCompat;

/**
 * Created by user1 on 6/21/2018.
 */

public class BootReceiver extends BroadcastReceiver {
    private static String TAG=BootReceiver.class.getSimpleName();
    @Override
    public void onReceive(Context context, Intent intent) {
              //  context.startService(new Intent(context, ScoutFawService.class));
        }

    public static void showNotification(Context context,String title,String message) {

          NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.logo)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, mBuilder.build());

        Log.d(TAG,"notified");
    }

}
