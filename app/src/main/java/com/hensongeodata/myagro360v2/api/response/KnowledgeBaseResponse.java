package com.hensongeodata.myagro360v2.api.response;

import com.hensongeodata.myagro360v2.model.KnowledgeBase;

import java.util.List;

public class KnowledgeBaseResponse {

      private int success;

      private int total;

      private String message;

      private List<KnowledgeBase> topics;

      public int getSuccess() {
            return success;
      }

      public void setSuccess(int success) {
            this.success = success;
      }

      public int getTotal() {
            return total;
      }

      public void setTotal(int total) {
            this.total = total;
      }

      public String getMessage() {
            return message;
      }

      public void setMessage(String message) {
            this.message = message;
      }

      public List<KnowledgeBase> getTopics() {
            return topics;
      }

      public void setTopics(List<KnowledgeBase> topics) {
            this.topics = topics;
      }
}
