package com.hensongeodata.myagro360v2.api.response;

import com.hensongeodata.myagro360v2.version2_0.Model.Member;

import java.util.List;

public class MemberResponse {

      private List<Member> members;

      public List<Member> getMembers() {
            return members;
      }

      public void setMembers(List<Member> members) {
            this.members = members;
      }
}
