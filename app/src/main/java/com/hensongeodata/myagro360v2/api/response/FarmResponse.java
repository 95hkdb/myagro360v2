package com.hensongeodata.myagro360v2.api.response;

import com.google.gson.annotations.SerializedName;
import com.hensongeodata.myagro360v2.version2_0.Model.Farm;

import java.util.List;

public class FarmResponse {

      @SerializedName("farms")
      private List<com.hensongeodata.myagro360v2.version2_0.Model.Farm> farms;

      public List<com.hensongeodata.myagro360v2.version2_0.Model.Farm> getFarms() {
            return farms;
      }

      public void setFarms(List<Farm> farms) {
            this.farms = farms;
      }
}
