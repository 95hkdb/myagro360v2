package com.hensongeodata.myagro360v2.api.response;

import com.google.gson.annotations.SerializedName;

public class ScanResponse {

    @SerializedName("success")
    String success;
    @SerializedName("message")
    String msg;
      @SerializedName("detected")
      String detected;

      public ScanResponse(String success, String msg, String detected) {
        this.success = success;
        this.msg = msg;
            this.detected = detected;
    }

    public String getSuccess() {
        return success;
    }

    public String getMsg() {
        return msg;
    }

      public String getDetected() {
            return detected;
      }

}
