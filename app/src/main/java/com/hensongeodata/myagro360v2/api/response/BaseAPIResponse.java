package com.hensongeodata.myagro360v2.api.response;

/**
 * Created by blanka on 3/5/2017.
 */
public class BaseAPIResponse {

    private boolean error;
    private String msg;

    public boolean isError() {
        return error;
    }

    public String getMsg() {
        return msg;
    }
}
