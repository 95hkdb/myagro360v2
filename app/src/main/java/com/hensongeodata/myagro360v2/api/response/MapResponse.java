package com.hensongeodata.myagro360v2.api.response;

import com.google.gson.annotations.SerializedName;
import com.hensongeodata.myagro360v2.version2_0.Model.Point;

import java.util.List;

public class MapResponse {

      private String id;

      private String name;

      @SerializedName("mappingcode")
      private String mappingCode;

      private String type;

      private List<Point> points;

      @SerializedName("created_date")
      private String createdDate;

      public String getId() {
            return id;
      }

      public void setId(String id) {
            this.id = id;
      }

      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      public String getMappingCode() {
            return mappingCode;
      }

      public void setMappingCode(String mappingCode) {
            this.mappingCode = mappingCode;
      }

      public String getType() {
            return type;
      }

      public void setType(String type) {
            this.type = type;
      }

      public List<Point> getPoints() {
            return points;
      }

      public void setPoints(List<Point> points) {
            this.points = points;
      }

      public String getCreatedDate() {
            return createdDate;
      }

      public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
      }
}
