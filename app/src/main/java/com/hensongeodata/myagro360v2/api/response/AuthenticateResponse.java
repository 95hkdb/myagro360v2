package com.hensongeodata.myagro360v2.api.response;

import com.google.gson.annotations.SerializedName;

public class AuthenticateResponse {
    @SerializedName("error")
    private Boolean error;
    @SerializedName("msg")
    private String message;
    @SerializedName("org_name")
    private String org_name;
    @SerializedName("org_phone")
    private String org_phone;
    @SerializedName("logo_url")
    private String logo_url;
    @SerializedName("email")
    private String org_email;

    public AuthenticateResponse(Boolean error, String message, String org_name, String org_phone, String logo_url, String org_email) {
        this.error = error;
        this.message = message;
        this.org_name = org_name;
        this.org_phone = org_phone;
        this.logo_url = logo_url;
        this.org_email = org_email;
    }

    public Boolean getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public String getOrg_name() {
        return org_name;
    }

    public String getOrg_phone() {
        return org_phone;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public String getOrg_email() {
        return org_email;
    }
}
