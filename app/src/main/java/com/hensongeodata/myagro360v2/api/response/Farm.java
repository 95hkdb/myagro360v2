package com.hensongeodata.myagro360v2.api.response;

public class Farm {

      private String id;

      private String name;


      public String getId() {
            return id;
      }

      public void setId(String id) {
            this.id = id;
      }

      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }
}
