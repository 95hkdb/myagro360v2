package com.hensongeodata.myagro360v2.api.response;

import com.google.gson.annotations.SerializedName;
import com.hensongeodata.myagro360v2.version2_0.Model.ThreadModel;

import java.util.List;

public class DataResponse {

      @SerializedName("threads")
      private List<ThreadModel> threads;

      @SerializedName("thread")
      private ThreadModel thread;


      public DataResponse() {
      }

      public List<ThreadModel> getThreads() {
            return threads;
      }

      public void setThreads(List<ThreadModel> threads) {
            this.threads = threads;
      }

      public ThreadModel getThread() {
            return thread;
      }

      public void setThread(ThreadModel thread) {
            this.thread = thread;
      }
}
