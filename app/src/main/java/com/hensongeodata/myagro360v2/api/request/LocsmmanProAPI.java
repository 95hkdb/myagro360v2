package com.hensongeodata.myagro360v2.api.request;


import com.hensongeodata.myagro360v2.api.response.AuthenticateResponse;
import com.hensongeodata.myagro360v2.api.response.ScanResponse;
import com.hensongeodata.myagro360v2.newmodel.ActivityModel;
import com.hensongeodata.myagro360v2.newmodel.PostResponse;
import com.hensongeodata.myagro360v2.newmodel.WeatherModel;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by blanka on 3/5/2017.
 */
public interface LocsmmanProAPI {

    @Multipart
    @POST("locsinsert")
    Call<ResponseBody> locsinsert(
                                  @Part("app_id") RequestBody app_id,
                                  @Part("payload") JSONObject payload,
                                  @Query("api_key") String api_key);


    @Multipart
    @POST("attachments")
    Call<ResponseBody> uploadFile(@Query("api_key") String api_key,
                                   @Part("form_id") RequestBody form_id,
                                  @Part("field_id") RequestBody field_id,
                                  @Part("type") RequestBody type,
                                  @Part("tranx_id") RequestBody tranx_id,
                                  @Part MultipartBody.Part file);

    @Multipart
    @POST("faw-api/process-scan")
    Call<ScanResponse> scanFAW(
            @Part("app_id") RequestBody app_id,
            @Part("ext_code") RequestBody ext_code,
            @Part("gps") RequestBody latlng,
            @Part("user_id") RequestBody u_email,
            @Part MultipartBody.Part file,
            @Query("api_key") String api_key
    );

    @Multipart
    @POST("faw-api/process-scan")
    Call<ResponseBody> scanFAWExtra(
            @Part("app_id") RequestBody app_id,
            @Part("ext_code") RequestBody ext_code,
            @Part("gps") RequestBody latlng,
            @Part("user_id") RequestBody u_email,
            @Part("scout_code") RequestBody scout_code,
            @Part("point_number") RequestBody point_number,
            @Part("plant_number") RequestBody plant_number,
            @Part MultipartBody.Part file,
            @Query("api_key") String api_key);


    @Multipart
    @POST("process-mapping")
    Call<ResponseBody> sendFAWMappingBulk(
            @Part("type") RequestBody type,
            @Part("app_id") RequestBody app_id,
            @Part("mapping_id") RequestBody mapping_id,
            @Part("user_id") RequestBody user_id,
            @Part("gps_payload") JSONArray payload,
            @Query("api_key") String api_key);

//    @Multipart
//    @POST("faw-api/process-mapping")
//    Call<ResponseBody> sendFAWMappingBulk(
//            @Part("type") RequestBody type,
//            @Part("app_id") RequestBody app_id,
//            @Part("mapping_id") RequestBody mapping_id,
//            @Part("ext_code") RequestBody ext_code,
//            @Part("user_id") RequestBody u_email,
//            @Part("label") RequestBody label,
//            @Part("comment") RequestBody comment,
//            @Part("plot_size") double size,
//            @Part("gps_payload") JSONArray payload,
//            @Query("api_key") String api_key);

    @Multipart
    @POST("postFormsCode")
            Call<ResponseBody> fetchFormByCode(@Part("userid") RequestBody userid,
                                  @Part("code") RequestBody form_code,
                                               @Query("api_key") String api_key);

    @GET("kb")
    Call<ResponseBody> fetchKB(
            @Query("lang") String lang,
            @Query("platform") String platform,
//            @Query("ext_code") String ext_code,
            @Query("api_key") String api_key);


    @Multipart
    @POST("sendMessage")
    Call<ResponseBody> sendMessage(@Part("app_id") RequestBody app_id,
                                   @Part("room_id") RequestBody chatroom_id,
                                  @Part("message") RequestBody message,
                                  @Part("has_image") RequestBody field_id,
                                  @Part("user_id") RequestBody type,
                                  @Part MultipartBody.Part file,
                                   @Query("api_key") String api_key);



    @Multipart
    @POST("joinCommunity")
    Call<ResponseBody> joinCommunity(@Part("email") RequestBody chatroom_id,
                                   @Part("push_token") RequestBody form_id,
                                   @Part("full_name") RequestBody field_id,
                                     @Part("rooms") JSONArray payload,
                                     @Query("api_key") String api_key);


    @Multipart
    @POST("signup")
    Call<ResponseBody> signup(@Part("appid") RequestBody app_id,
                                   @Part("email") RequestBody email,
                                   @Part("password") RequestBody password,
                                   @Part("firstName") RequestBody firstname,
                                   @Part("lastName") RequestBody lastname,
                                   @Part("type") RequestBody type,
                                   @Part("country") RequestBody country,
                                   @Part("region") RequestBody region,
                                   @Part("district") RequestBody district,
                                   @Part("town") RequestBody town,
                                   @Part("org") RequestBody org,
                                   @Part("pushID") RequestBody push_id,
                              @Query("api_key") String api_key);

    @Multipart
    @POST("update")
    Call<ResponseBody> update(@Part("app_id") RequestBody app_id,
                              @Part("email") RequestBody email,
                              @Part("password") RequestBody password,
                              @Part("firstName") RequestBody firstname,
                              @Part("lastName") RequestBody lastname,
                              @Part("type") RequestBody type,
                              @Part("country") RequestBody country,
                              @Part("region") RequestBody region,
                              @Part("district") RequestBody district,
                              @Part("town") RequestBody town,
                              @Part("org") RequestBody org,
                              @Part("pushID") RequestBody push_id,
                              @Query("api_key") String api_key);

    @Multipart
    @POST("signin")
    Call<ResponseBody> signin(@Part("email") RequestBody email,
                              @Part("password") RequestBody password,
                              @Part("device_name") RequestBody device_name,
                              @Part("version") RequestBody version,
                              @Part("model") RequestBody model,
                              @Part("serial_number") RequestBody serial_number,
                              @Query("api_key") String api_key);

//Verify user organisation
    @FormUrlEncoded
    @POST("authenticate/verify-code")
    Call<AuthenticateResponse> checkOrganisation(
            @Field("org_code") String orgcode,
            @Field("u_email") String u_email,
            @Field("u_fname") String firstname,
            @Field("u_lname") String lastname,
            @Field("u_role") String u_role,
            @Field("u_push_token") String u_push
    );

    // Check recent post
    @FormUrlEncoded
    @POST("mobile/get-post")
    Call<PostResponse> newPost(
            @Field("user_id") String user_id,
            @Field("org_code") String orgcode
    );

      //check weather
      @FormUrlEncoded
      @POST(".")
      Call<WeatherModel> weather(
              @Field("api_key") String user_id,
              @Field("lon") String lon,
              @Field("lat") String lat
      );

    //activity performed on farm
    @Multipart
    @POST("faw-api/process-activity")
    Call<ActivityModel> sendRecentActivity(
            @Part("name") RequestBody activity,
            @Part("time") RequestBody duration,
            @Part("comment") RequestBody comment,
            @Part("workers") RequestBody workers,
            @Part("ext_code") RequestBody org_code,
            @Part("user_id") RequestBody u_email);


    @Multipart
    @POST("authenticateUser")
    Call<ResponseBody> registerPush(@Part("user_id") RequestBody email,
                              @Part("push_id") RequestBody password,
                                    @Query("api_key") String api_key);


    @Multipart
    @POST("externals/api/getItemsInCategory")
    Call<ResponseBody> getProducts(@Part("app_id") RequestBody app_id,@Part("category") RequestBody category,@Query("api_key") String api_key);

    @Multipart
    @POST("externals/api/transactionTracker")
    Call<ResponseBody> payment(@Part("products") JSONObject products,
                               @Part("user_id") RequestBody user_id,
                               @Part("amount") RequestBody amount,
                               @Part("card_number") RequestBody cardNumber,
                               @Part("card_name") RequestBody cardName,
                               @Part("cvv") RequestBody cvv,
                               @Part("year_expire") RequestBody yearExpire,
                               @Part("month_expire") RequestBody monthExpire,
                               @Part("card_type") RequestBody cardType,
                               @Query("api_key") String api_key);
}
