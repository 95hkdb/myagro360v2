package com.hensongeodata.myagro360v2.api.response;

import com.google.gson.annotations.SerializedName;
import com.hensongeodata.myagro360v2.model.FarmActivityModel;

import java.util.List;

public class FarmActivityResponse {

      @SerializedName("activities")
      private List<FarmActivityModel> farmActivityModelList;

      public List<FarmActivityModel> getFarmActivityModelList() {
            return farmActivityModelList;
      }

      public void setFarmActivityModelList(List<FarmActivityModel> farmActivityModelList) {
            this.farmActivityModelList = farmActivityModelList;
      }
}
