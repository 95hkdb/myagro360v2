package com.hensongeodata.myagro360v2.api.response;

import com.google.gson.annotations.SerializedName;
import com.hensongeodata.myagro360v2.version2_0.Model.ScanModel;

import java.util.List;

public class UserScanResponse {

      @SerializedName("scans")
      private List<ScanModel> scans;

      public List<ScanModel> getScans() {
            return scans;
      }

      public void setScans(List<ScanModel> scans) {
            this.scans = scans;
      }
}
