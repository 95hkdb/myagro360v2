package com.hensongeodata.myagro360v2.api.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserMapResponse {

      @SerializedName("maps")
      private List<MapResponse> maps;

      public List<MapResponse> getMaps() {
            return maps;
      }

      public void setMaps(List<MapResponse> maps) {
            this.maps = maps;
      }
}
