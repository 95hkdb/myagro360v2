package com.hensongeodata.myagro360v2.api.response;

import com.hensongeodata.myagro360v2.version2_0.Model.Crop;

import java.util.List;

public class CropResponse {

      private List<Crop> crops;

      public List<Crop> getCrops() {
            return crops;
      }

      public void setCrops(List<Crop> crops) {
            this.crops = crops;
      }
}
