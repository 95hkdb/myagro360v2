package com.hensongeodata.myagro360v2.api.response;

import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;

import java.util.List;

public class PlotResponse {

      private List<FarmPlot> plots;

      public List<FarmPlot> getPlots() {
            return plots;
      }

      public void setPlots(List<FarmPlot> plots) {
            this.plots = plots;
      }
}
