package com.hensongeodata.myagro360v2.api.response;

import com.google.gson.annotations.SerializedName;

public class UserActivityResponse {

      @SerializedName("ActivityID")
      private String activityId;

      @SerializedName("ID")
      private String id;

      @SerializedName("Name")
      private String name;

      @SerializedName("Comment")
      private String additionalInfo;

      @SerializedName("StartDate")
      private String startDate;

      @SerializedName("EndDate")
      private String endDate;

      @SerializedName("PlotID")
      private String plotId;

      @SerializedName("FarmID")
      private String farmId;

      private boolean outSourced;

      @SerializedName("PerformerID")
      private String staffId;

      private String outSourcedName;

      private String userId;

      @SerializedName("PerformerType")
      private String type;

      private String orgId;

      @SerializedName("Completed")
      private boolean completed;

      @SerializedName("Created")
      private long createdAt;

      @SerializedName("LastEdited")
      private long updatedAt;

      public String getActivityId() {
            return activityId;
      }

      public void setActivityId(String activityId) {
            this.activityId = activityId;
      }

      public String getId() {
            return id;
      }

      public void setId(String id) {
            this.id = id;
      }

      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      public String getAdditionalInfo() {
            return additionalInfo;
      }

      public void setAdditionalInfo(String additionalInfo) {
            this.additionalInfo = additionalInfo;
      }

      public String getStartDate() {
            return startDate;
      }

      public void setStartDate(String startDate) {
            this.startDate = startDate;
      }

      public String getEndDate() {
            return endDate;
      }

      public void setEndDate(String endDate) {
            this.endDate = endDate;
      }

      public String getPlotId() {
            return plotId;
      }

      public void setPlotId(String plotId) {
            this.plotId = plotId;
      }

      public String getFarmId() {
            return farmId;
      }

      public void setFarmId(String farmId) {
            this.farmId = farmId;
      }

      public boolean isOutSourced() {
            return outSourced;
      }

      public void setOutSourced(boolean outSourced) {
            this.outSourced = outSourced;
      }

      public String getStaffId() {
            return staffId;
      }

      public void setStaffId(String staffId) {
            this.staffId = staffId;
      }

      public String getOutSourcedName() {
            return outSourcedName;
      }

      public void setOutSourcedName(String outSourcedName) {
            this.outSourcedName = outSourcedName;
      }

      public String getUserId() {
            return userId;
      }

      public void setUserId(String userId) {
            this.userId = userId;
      }

      public String getOrgId() {
            return orgId;
      }

      public void setOrgId(String orgId) {
            this.orgId = orgId;
      }

      public String getType() {
            return type;
      }

      public void setType(String type) {
            this.type = type;
      }

      public boolean isCompleted() {
            return completed;
      }

      public void setCompleted(boolean completed) {
            this.completed = completed;
      }

      public long getCreatedAt() {
            return createdAt;
      }

      public void setCreatedAt(long createdAt) {
            this.createdAt = createdAt;
      }

      public long getUpdatedAt() {
            return updatedAt;
      }

      public void setUpdatedAt(long updatedAt) {
            this.updatedAt = updatedAt;
      }
}
