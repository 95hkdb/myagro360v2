package com.hensongeodata.myagro360v2.api.response;

public class GenericResponse {

      private UserMapResponse response;

      public UserMapResponse getResponse() {
            return response;
      }

      public void setResponse(UserMapResponse response) {
            this.response = response;
      }
}
