package com.hensongeodata.myagro360v2.api.response;

import com.google.gson.annotations.SerializedName;

public class ThreadResponse {

      @SerializedName("data")
      private DataResponse data;

      public ThreadResponse() {
      }

      public DataResponse getData() {
            return data;
      }

      public void setData(DataResponse data) {
            this.data = data;
      }
}
