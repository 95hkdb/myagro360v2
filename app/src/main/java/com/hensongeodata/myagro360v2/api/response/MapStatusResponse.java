package com.hensongeodata.myagro360v2.api.response;

public class MapStatusResponse {

      private int status;

      private String plot_name;

      private String plot_id;

      public int getStatus() {
            return status;
      }

      public void setStatus(int status) {
            this.status = status;
      }

      public String getPlot_name() {
            return plot_name;
      }

      public void setPlot_name(String plot_name) {
            this.plot_name = plot_name;
      }

      public String getPlot_id() {
            return plot_id;
      }

      public void setPlot_id(String plot_id) {
            this.plot_id = plot_id;
      }
}
