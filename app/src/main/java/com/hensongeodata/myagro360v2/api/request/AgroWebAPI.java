package com.hensongeodata.myagro360v2.api.request;

import com.google.gson.JsonElement;
import com.hensongeodata.myagro360v2.api.response.CropResponse;
import com.hensongeodata.myagro360v2.api.response.FarmActivityResponse;
import com.hensongeodata.myagro360v2.api.response.FarmResponse;
import com.hensongeodata.myagro360v2.api.response.GenericResponse;
import com.hensongeodata.myagro360v2.api.response.KnowledgeBaseResponse;
import com.hensongeodata.myagro360v2.api.response.MapStatusResponse;
import com.hensongeodata.myagro360v2.api.response.MemberResponse;
import com.hensongeodata.myagro360v2.api.response.PlotResponse;
import com.hensongeodata.myagro360v2.api.response.ThreadResponse;
import com.hensongeodata.myagro360v2.api.response.UserActivityResponse;
import com.hensongeodata.myagro360v2.api.response.UserMapResponse;
import com.hensongeodata.myagro360v2.newmodel.WeatherModel;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmActivity;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AgroWebAPI {

      @GET("/")
      Observable<WeatherModel> getWeather(@Query("api_key") String version,
                                                                                           @Query("lat") String lat,
                                                                                           @Query("lon") String lon);

      @GET("/mobile/get-activity")
      Observable<FarmActivityResponse> getActivities();

      @GET("/mobile/get-plots")
      Observable<PlotResponse> getPlots();

      @FormUrlEncoded
      @POST("/mobile/get-plots")
      Call<PlotResponse> getPlotsCall(@Field("org_id") String orgId, @Field("user_id") String userId);

      @FormUrlEncoded
      @POST("/mobile/get-user-scans")
      Call<JsonElement> getUserScans(@Field("org_id") String orgId, @Field("user_id") String userId);

      @FormUrlEncoded
      @POST("/mobile/get-user-scouts")
      Call<JsonElement> getUserScouts(@Field("org_id") String orgId, @Field("user_id") String userId);

      @FormUrlEncoded
      @POST("/mobile/attach-map")
      Call<JsonElement> attachPlotToMap(@Field("mapping_id") String mapping_id, @Field("plot_id") String plot_id);

      @FormUrlEncoded
      @POST("/mobile/create-plot")
      Call<JsonElement> createFarmPlot(
                                       @Field("plot_name") String name, @Field("description") String description,
                                       @Field("plot_size") double plotSize, @Field("map_id") String mapId,
                                       @Field("crop_id") String cropId, @Field("org_id") String orgId,
                                       @Field("user_id") String userId, @Field("farm_id") String farmId);

      @FormUrlEncoded
      @POST("/mobile/check-map-status")
      Observable<MapStatusResponse> checkMapStatus(@Field("mapping_id") String mapping_id);

   @FormUrlEncoded
      @POST("mobile/api/comment")
      Observable<JsonElement> commentThread(@Field("ID") String id, @Field("content") String content, @Field("firstname") String firstName, @Field("email") String email);

      @GET("kb")
      Observable<KnowledgeBaseResponse> fetchKnowledgeBase(
              @Query("lang") String lang,
              @Query("platform") String platform,
              @Query("api_key") String api_key);

      @GET("kb")
      Call<KnowledgeBaseResponse> fetchKnowledgeBaseCall(
              @Query("lang") String lang,
              @Query("platform") String platform,
              @Query("api_key") String api_key);

      @GET("mobile/api/threads")
      Observable<ThreadResponse> fetchThreads();

      @GET("mobile/api/thread/{id}")
      Observable<ThreadResponse> fetchSingleThread(@Path("id") String id);

//      @GET("/v2/venues/{venue_id}/photos")
//      Call<FourSquareResponse> getVenuePhoto(@Path("venue_id") String venueId, @Query("v") String version, @Query("client_id") String clientID,
//                                             @Query("client_secret") String clientSecret , @Query("group") String group, @Query("limit") int limit );

      @GET("/mobile/get-crops")
      Observable<CropResponse> fetchCrops();

      @GET("/mobile/get-crops")
      Call<CropResponse> fetchCropsCall();

      @GET("/mobile/get-roles")
      Observable<JsonElement> fetchRolesCall();

      @FormUrlEncoded
      @POST("/mobile/get-org-farms")
      Observable<FarmResponse> fetchFarms(@Field("user_id") String userId, @Field("org_id") String orgId);

      @FormUrlEncoded
      @POST("/mobile/get-user-activities")
      Observable<List<UserActivityResponse>> fetchUserActivities(@Field("user_id") String userId, @Field("org_id") String orgId);

      @FormUrlEncoded
      @POST("mobile/api/create-thread")
      Observable<JsonElement> createThread(@Field("title") String title,
                                            @Field("content") String content, @Field("firstname") String firstname,
                                            @Field("email") String email, @Field("tags") List<String> tags);


      @FormUrlEncoded
      @POST("/mobile/get-org-farms")
      Call<FarmResponse> fetchFarmsCall(@Field("user_id") String userId, @Field("org_id") String orgId);

      @FormUrlEncoded
      @POST("/mobile/get-user-maps")
      Call<GenericResponse> getUserMaps(@Field("user_id") String userId, @Field("org_id") String orgId);

      @FormUrlEncoded
      @POST("/mobile/get-user-maps")
      Call<UserMapResponse> getUserMap(@Field("user_id") String userId, @Field("org_id") String orgId);


      @FormUrlEncoded
      @POST("/mobile/get-members")
      Call<MemberResponse> getMembers(@Field("user_id") String userId, @Field("org_id") String orgId);

      @FormUrlEncoded
      @POST("/mobile/create-farm-activity")
      Call<FarmActivity> createFarmActivity(@Field("user_id") String userId, @Field("org_id") String orgId,
                                            @Field("name") String name, @Field("comment") String comment, @Field("plot_id") String plotId,
                                            @Field("start_date") String start_date, @Field("end_date") String end_date, @Field("staff_id") String staffId,
                                            @Field("type") String type, @Field("performer_name") String performerName, @Field("activity_id") String activityId,
                                            @Field("completed") boolean completed);

}


