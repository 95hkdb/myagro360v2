package com.hensongeodata.myagro360v2;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
