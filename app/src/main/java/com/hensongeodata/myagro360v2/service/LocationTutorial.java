package com.hensongeodata.myagro360v2.service;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.util.Log;

/**
 * Created by brasaeed on 03/06/2017.
 */

public class LocationTutorial extends Service {
    //The TAG created below let's you log information easily with the class's unique simple name
    private static String TAG = LocationTutorial.class.getSimpleName();

    //The onCreate() method forms part of android service lifecycle.
    //It is called immediately after the OnAttach method whenever a service is started
    @Override
    public void onCreate() {
        super.onCreate();

        //Instantiating locationManager and Requesting access to built in android Location
        //LocationManager gives us total control over the built in android location service
        //With this, we can use the location service however and whenever we want in our application
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

       //Runtime permission request. This will be displayed to android phones running higher than sdk 23
       //Without this runtime permission request, phones running on api 23 and above will crush -
        //thus if Location permission is not explicitly allowed
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG,"permission not set");
            return;
        }

        //There are multiple ways to requestLocationUpdates. we can get Location updates from
        //Network providers (via network towers): This is not very accurate as compared to GPS provider
        //GPS providers (via satellite communication): This is more often than not, the most accurate
        //etc

        //the requestLocationUpdates method used here takes 4 arguments
        //(there are implementations of this method which takes varying number of arguments.
        //I personally decided to use this one).
        //The arguments are described as follows:
        //1. The location provider - below we are using the GPS provider
        //2. The minimum update time : the minimum time interval between every location update. Below it's
        //set to 2secs (2000 milliseconds)
        //3. The minimum distance: The minimum distance before requesting for another update. Below we set it
        //to to 1 meter.
        //4. A location updates listener: Maybe you're not very familiar with listeners right now but don't worry,
        //we'll discuss that in full detail in my next tutorial. But basically, the listener provided below
        // helps us pay attention to incoming location updates and when received, process them the way we want to
        //Below we are using the LocationListenerGPS listener
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                2000,
                1, locationListenerGPS);

        //Now, we are good to start listening to location updates.
        //NB - usually, positioning affects the delay in reading location updates. usually,
        //Network providers give a quicker location feedback whiles a GPS provider can delay for
        //a while (2-3 mins). Also, GPS providers tend to be more accurate when you are in an open space,
        //preferably outside in the open (Because of the satellite).
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        return START_STICKY;
    }

    private void setLocation(Location location) {
        Log.d(TAG, "setLocation: "+location);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    //Here is our locationListener instantiation. This was the listener we registered inside our
    //locationManager.requestLocationUpdates() above
    LocationListener locationListenerGPS=new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
        //This is probably the method you'll most likely be working with the most
        //Here, new location updates are received and can be used
            setLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            //The status of your location update can changed due to several reasons
        }

        @Override
        public void onProviderEnabled(String provider) {
        //Whenever the registered provider is enabled, this method is called
        }

        @Override
        public void onProviderDisabled(String provider) {
        //Whenever the registered provider is disabled, this method is called
        }
    };

    //So there we have it, a fully working implementation of an android Location Service.
    //Feel free to copy and paste this code and use for your own project.
}
