package com.hensongeodata.myagro360v2.service;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;

/**
 * Created by brasaeed on 03/06/2017.
 */

public class UploadFilesService extends Service {
    private static String TAG = UploadFilesService.class.getSimpleName();
    public final static int SOURCE_MAIN_ACTIVITY=100;
    public final static int SOURCE_FIXED_ACTIVITY=200;
    public final static int SOURCE_ARCHIVES=300;
    public final static int SOURCE_PREVIEW=400;

    public static ProgressDialog progressDialog;

    private int source;
    private MyApplication myApplication;
    private NetworkRequest networkRequest;
    private UploadFileReceiver receiver;
    private IntentFilter filter;
    private boolean isInit=true;

    @Override
    public void onCreate() {
        super.onCreate();

        receiver=new UploadFileReceiver();
        filter=new IntentFilter(BroadcastCall.FILE_UPDATE);
        registerReceiver(receiver,filter);

        myApplication=new MyApplication();
        networkRequest=new NetworkRequest(this);

        isInit=true;
        BroadcastCall.publishFileUpload(this,NetworkRequest.STATUS_SUCCESS,"init");
        //fileUploadInterrupt("Interruption test","hello there!");
        Log.d(TAG, "oncreate looks good");
    }

    private void uploadFile(){
        Log.d(TAG,"uploadFile");
        progressDialog.show();

        if(myApplication.hasNetworkConnection(this)){
            if(MyApplication.filesToUpload==null||MyApplication.filesToUpload.size()==0){
                uploadComplete(true);
                return;
            }

            showProgress("Uploading attachments. "+(MyApplication.filesToUploadSize-(MyApplication.filesToUpload.size()-1))+" of "
                    +MyApplication.filesToUploadSize);
            networkRequest.sendFile(MyApplication.filesToUpload.get(MyApplication.filesToUpload.size()-1));
        }
        else {
            hideProgress();
            fileUploadInterrupt("No internet","Failed to upload files due to no internet connectivity");
        }
    }

    private void fileUploadInterrupt(String header,String msg){
        Log.d(TAG,"interrupt");
        showMessage(header, msg,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        uploadComplete(false);
                        Toast.makeText(UploadFilesService.this,"Data submission interrupted.",Toast.LENGTH_SHORT).show();
                        dialogInterface.cancel();
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        uploadFile();
                        dialogInterface.cancel();
                    }
                });
    }


    class UploadFileReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int status=intent.getExtras().getInt("status");
            Log.d(TAG,"status: "+status);
            if(status== NetworkRequest.STATUS_SUCCESS){
                Log.d(TAG,"isInit: "+isInit+" size: "+MyApplication.filesToUpload.size());
                if(!isInit&&MyApplication.filesToUpload.size()>0) {
                    Log.d(TAG,"removing last item");
                    MyApplication.filesToUpload.remove(MyApplication.filesToUpload.size() - 1);
                }
                isInit=false;

                Log.d(TAG,"pending upload size: "+MyApplication.filesToUpload.size());

                if(MyApplication.filesToUpload.size()!=0)
                    uploadFile();
                else
                    uploadComplete(true);

                Log.d(TAG,"success");

            }
            else if(status==NetworkRequest.STATUS_FAIL){
                fileUploadInterrupt("Error","Failed to finish file upload");
                hideProgress();
                Log.d(TAG,"fail");
            }
            else {
                uploadComplete(true);
                hideProgress();
                Log.d(TAG,"else");
            }
        }
    }

    private void uploadComplete(boolean status){
        hideProgress();
        if(MyApplication.filesToUpload!=null)
            MyApplication.filesToUpload.clear();

        Log.d(TAG,"uploadComplete: "+status);
        BroadcastCall.publishTransaction(this,status?NetworkRequest.STATUS_SUCCESS:
        NetworkRequest.STATUS_FAIL,"file upload");

        stopSelf();
    }

    private void showProgress(String msg){
        progressDialog.setMessage(msg);
    }

    private void hideProgress(){
       progressDialog.hide();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(receiver!=null) unregisterReceiver(receiver);
    }

    public void showMessage(String header,String message,
                            DialogInterface.OnClickListener negativeCallback,
                            DialogInterface.OnClickListener positiveCallback){
        new AlertDialog.Builder(MyApplication.context)
                .setTitle(header)
                .setMessage(message)
                .setNegativeButton("CANCEL",negativeCallback)
                .setPositiveButton("RETRY", positiveCallback)
                .show();

        Log.d(TAG,"showMessage "+header);
    }
}
