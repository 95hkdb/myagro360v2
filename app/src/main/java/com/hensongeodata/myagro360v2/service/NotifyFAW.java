package com.hensongeodata.myagro360v2.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import androidx.annotation.Nullable;
import android.util.Log;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;

/**
 * Created by brasaeed on 03/06/2017.
 */

public class NotifyFAW extends Service {
    private static String TAG = NotifyFAW.class.getSimpleName();
    public static Intent intent;
    private Runnable runnable;
    private Handler handler;
    public static Vibrator vibrator;
    private long DURATION_LONG=60000;
    private  long DURATION_SHORT=10000;
    private long duration;
    public static MediaPlayer tone;
    @Override
    public void onCreate() {
        super.onCreate();

        if(MyApplication.online)
            duration=DURATION_SHORT;
        else
            duration=DURATION_LONG;

        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(duration);

        tone= MediaPlayer.create(this,R.raw.impressed);
        tone.setVolume(1.0f,1.0f);
        tone.setLooping(true);
        tone.start();


        handler=new Handler();
        runnable=new Runnable() {
            @Override
            public void run() {
                stopSelf();
            }
        };

        stopNotify(duration);

        Log.d(TAG, "oncreate looks good");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void stopNotify(long after){
        handler.postDelayed(runnable,after);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(handler!=null&&runnable!=null)
            handler.removeCallbacks(runnable);

        if(vibrator!=null&&vibrator.hasVibrator())
            vibrator.cancel();

        if(tone!=null&&tone.isPlaying())
            tone.stop();

        intent=null;

        Log.d(TAG,"on destroy");
    }
}
