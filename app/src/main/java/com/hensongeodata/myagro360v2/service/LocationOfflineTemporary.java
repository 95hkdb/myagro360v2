package com.hensongeodata.myagro360v2.service;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.util.Log;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.fragment.BSFragment;
import com.hensongeodata.myagro360v2.model.POI;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by brasaeed on 03/06/2017.
 */

public class LocationOfflineTemporary extends Service {
    private static String TAG = LocationOfflineTemporary.class.getSimpleName();
    private MyApplication myApplication;
    public static Intent intent;
    public static boolean started=false;
    @Override
    public void onCreate() {
        super.onCreate();

        myApplication=new MyApplication();
        started=true;
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG,"permission not set");
            return;
        }

        String provider=null;
        if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
            provider=LocationManager.NETWORK_PROVIDER;
        else if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            provider=LocationManager.GPS_PROVIDER;

        if(provider!=null) {
            locationManager.requestLocationUpdates(provider,
                    2000, 1, locationListenerGPS);

        }
        Log.d(TAG,"provider: "+provider);

        Log.d(TAG, "oncreate looks good");
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        return START_STICKY;
    }

    private void setLocation(Location location) {
        Log.d(TAG, "setLocation: "+location);
        if (location != null&&MyApplication.poi!=null) {
              MyApplication.poi.setAccuracy(String.valueOf(Math.ceil(location.getAccuracy())));
              MyApplication.poi.setLat(String.valueOf(location.getLatitude()));
              MyApplication.poi.setLon(String.valueOf(location.getLongitude()));
              MyApplication.poi.setAlt(String.valueOf(location.getAltitude()));
              EventBus.getDefault().post(new POI(MyApplication.poi.name, MyApplication.poi.lon, MyApplication.poi.lat, MyApplication.poi.alt, MyApplication.poi.accuracy));

            BroadcastCall.publishLocationUpdate(LocationOfflineTemporary.this, NetworkRequest.STATUS_SUCCESS, BSFragment.ACTION_LOCATION);
            Log.d(TAG, "location sent");
        } else {
            Log.d(TAG, "Location is null");
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    LocationListener locationListenerGPS=new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.d(TAG,"location changed");
            setLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };
}
