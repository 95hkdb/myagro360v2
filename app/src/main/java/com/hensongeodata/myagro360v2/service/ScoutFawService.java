package com.hensongeodata.myagro360v2.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.api.request.LocsmmanProAPI;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.controller.RetrofitHelper;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.receiver.BootReceiver;
import com.hensongeodata.myagro360v2.version2_0.Events.DBSyncingComplete;
import com.hensongeodata.myagro360v2.version2_0.Model.ScanModel;
import com.hensongeodata.myagro360v2.view.Scout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.hensongeodata.myagro360v2.controller.RetrofitHelper.createPartFromString;

/**
 * Created by brasaeed on 03/06/2017.
 */

public class ScoutFawService extends Service {
    private static String TAG = ScoutFawService.class.getSimpleName();
    public static boolean dispatching=false;
    private MyApplication myApplication;
    private DatabaseHelper databaseHelper;
    private static int STATE_DB=100;
    private static int STATE_LIVE=200;
    private int state;
    private static int counter=0;
    ArrayList<InstructionScout> itemsDB;
    ArrayList<InstructionScout> itemsLive;
    InstructionScout instructionScout=null;
    Call<ResponseBody> call;

    private Runnable runnable;
    private Handler handler;
    private long TIME=60000;
    private MapDatabase mapDatabase;
    private long scoutPrimaryId;

    @Override
    public void onCreate() {
        super.onCreate();

        EventBus.getDefault().register(this);
        handler=new Handler();
        databaseHelper=new DatabaseHelper(this);
        mapDatabase    = MapDatabase.getInstance(this);
        myApplication=new MyApplication();
        itemsDB =databaseHelper.getScoutList(ChatbotAdapterRV.MODE_SCOUT);

        dispatch();

        runnable=new Runnable() {
            @Override
            public void run() {
                dispatch();
                handler.postDelayed(runnable,TIME);
            }
        };

        handler.postDelayed(runnable,TIME);

        MyApplication.scanServiceActive=true;
        Log.d(TAG, "oncreate looks good");
    }

    private void dispatch(){
            if(!dispatching) {
                if(databaseHelper.getScoutList(ChatbotAdapterRV.MODE_SCOUT).size()>0)
                         BootReceiver.showNotification(this,getResources().getString(R.string.app_name),"Syncing...");

                prepareFile();
            }
    }

    private void prepareFile(){
        dispatching=true;

        if(MyApplication.modeScout==ChatbotAdapterRV.MODE_SCOUT)
            itemsLive=MyApplication.itemsLive;
        else
            itemsLive=new ArrayList<>();

        if(itemsLive!=null&&itemsLive.size()>0){
            state=STATE_LIVE;
            instructionScout= itemsLive.get(0);
        }
        else{
//            if(state==STATE_LIVE){
//
//            }
            itemsDB=databaseHelper.getScoutList(ChatbotAdapterRV.MODE_SCOUT);
            Log.d(TAG,"itemsDB size: "+itemsDB.size());
            state=STATE_DB;
            if(itemsDB !=null&& itemsDB.size()>0)
                    instructionScout= itemsDB.get(0);
            else {
                Log.d(TAG,"killing service");
                dispatching=false;
                return;
            }
        }

        if(instructionScout!=null) {
            Scout.scannning_id = instructionScout.getId();
            processImage(instructionScout);
        }
        else{
            removeProccessed();
          //  prepareFile();
        }

    }

    private void processImage(InstructionScout instructionScout){
        Image image=instructionScout.getImage();
        if(image!=null&&image.getUri()!=null&&image.getUri().getPath()!=null) {
            MultipartBody.Part file = RetrofitHelper.prepareFilePart(this, "image", image.getUri().getPath());
            sendFile(instructionScout,file);
        }
        else {
            removeProccessed();
          //  prepareFile();
        }
    }

    private void removeProccessed(){
        //removeProccessed the most recently scanned item from pending scan list
        if(state==STATE_DB) {
            if (itemsDB.size() > 0) {
                Log.d(TAG, "remove db");
                itemsDB.remove(0);
            }
        }
        else if(state==STATE_LIVE&&MyApplication.modeScout== ChatbotAdapterRV.MODE_SCOUT) {
            if (MyApplication.itemsLive != null && MyApplication.itemsLive.size() > 0) {
                MyApplication.itemsLive.remove(0);
                Log.d(TAG, "remove live "+MyApplication.itemsLive.size());
            }
            if(MyApplication.itemsLive!=null)
                    itemsLive = MyApplication.itemsLive;
            else
                itemsLive=new ArrayList<>();
        }
        prepareFile();
    }

    public void sendFile(InstructionScout instructionScout,MultipartBody.Part file){
        String ext_code = SharePrefManager.getInstance(getApplicationContext()).getOrganisationDetails().get(0);
        String u_email = "";


        if (SharePrefManager.isSecondaryUserAvailable(this)){
            u_email = SharePrefManager.getSecondaryUserPref(this);
        }else {
            u_email = SharePrefManager.getInstance(this).getUserDetails().get(0);
        }

        Retrofit.Builder builder=new Retrofit.Builder()
                .baseUrl(Keys.AGRO_BASE)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit=builder.build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

        String gps=null;
        if(MyApplication.poi!=null)
            gps=MyApplication.poi.getLat()+","+MyApplication.poi.getLon();

        Log.d(TAG,"gps: "+gps+" state: "+state+" counter: "+(++counter)+" item_id: "+instructionScout.getId());
        Log.d(TAG,"scout_id: "+instructionScout.getScout_id());
        Log.d(TAG,"point_no: "+instructionScout.getPoint_number());
        Log.d(TAG,"plant_no: "+instructionScout.getPlant_number());

        call= service.scanFAWExtra(
                createPartFromString(Keys.APP_ID),
                createPartFromString(""+ext_code),
                createPartFromString(gps),
                createPartFromString(u_email),
                createPartFromString(""+instructionScout.getScout_id()),
                createPartFromString(""+instructionScout.getPoint_number()),
                createPartFromString(""+instructionScout.getPlant_number()), file,Keys.API_KEY);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                ResponseBody responseBody=response.body();
                String message=null;
                try {
                    if(responseBody!=null)
                        message=responseBody.string();

                    Log.d(TAG,"scout result response: "+message+" body: "+responseBody
                            +" code: "+response.code()+" error: "+response.errorBody());

                    call.cancel();

                } catch (IOException e) {
                    e.printStackTrace();
                    call.cancel();
                    processResult(NetworkRequest.STATUS_FAIL,message);
                }

                Log.d(TAG,"message: "+message);
                if(message==null||message.trim().equalsIgnoreCase("null"))
                    processResult(NetworkRequest.STATUS_FAIL,message);
                else
                    processResult(NetworkRequest.STATUS_SUCCESS,message);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String message=t.getMessage();
                Log.d(TAG,"throwable: "+t.getMessage()+" class "+t.getClass()+" cause "+t.getCause());


                call.cancel();
                if(t.getClass() == java.io.FileNotFoundException.class){
                 Log.d(TAG,"file not found");
                    removeProccessed();
                }
                removeProccessed();
            }
        });
    }
    private void processResult(int status,String response){
        if(status==NetworkRequest.STATUS_SUCCESS){
            try {
                JSONObject jsonObject=new JSONObject(response);

                if(jsonObject.has("success")){
                    int success=jsonObject.getInt("success");
                    String message=jsonObject.optString("message");
                    Log.d(TAG,"success: "+success+" message: "+message);
                    if(success==1){
                        int detected=jsonObject.optInt("detected");

                        String message1 = detected == 1 ? "Pest detected" : message != null ? message : "Pest not detected";
                        databaseHelper.addScoutItem(Scout.scannning_id,
                                    InstructionScout.STATE_PROCCESSED, message1,
                                    ChatbotAdapterRV.MODE_SCOUT);

                        AppExecutors.getInstance().getDiskIO().execute(() -> {
                            ScanModel scanModel =  mapDatabase.scanDaoAccess().fetchScoutByItemId(Scout.scannning_id);
                            scanModel.setStatus(ScanModel.STATE_PROCESSED);
                            scanModel.setMessage(message1);
                            scanModel.setPestIdentified(message);

                            if (message != null && !message.equals("No label found")) {
                                scanModel.setPestFound(true);
                            }else {
                                scanModel.setPestFound(false);
                            }

                            String reportText  = generateScanReportText(scanModel).toString();
                            Log.i(TAG, "processResult:  SB " + reportText);
                            scanModel.setScanReportText(reportText);

                            mapDatabase.scanDaoAccess().updateScout(scanModel);

//                            try {
//
//                                EventBus.getDefault().postSticky(new ScanCompletedEvent());
//                            }catch (Exception e){
//                                Log.e(TAG, "processResult Erorr:  " + e.getMessage() );
//                            }

                            Log.i(TAG, "processResult:  Count" );
                        });


                            if(MyApplication.taskListener!=null)
                                MyApplication.taskListener.onTaskStopped(Scout.scannning_id,true);

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //prepareFile();

        removeProccessed();
    }


    private StringBuilder generateScanReportText(ScanModel scanModel){

        StringBuilder stringBuilder = new StringBuilder();

        if (scanModel != null ){
            long id = scanModel.getId();
            int status = scanModel.getStatus();
            String message = scanModel.getMessage();
            long scoutId = scanModel.getScoutId();
            String type = scanModel.getType();
            int plantNumber = scanModel.getPlantNumber();
            int plantPoint = scanModel.getPointNumber();
            String pestIdentified = scanModel.getPestIdentified();
            String spacing = "     ";

            stringBuilder.append(id);
            stringBuilder.append(spacing);
            stringBuilder.append(status);
            stringBuilder.append(spacing);
            stringBuilder.append(message);
            stringBuilder.append(spacing);
            stringBuilder.append(scoutId);
            stringBuilder.append(spacing);
            stringBuilder.append(type);
            stringBuilder.append(spacing);
            stringBuilder.append(plantNumber);
            stringBuilder.append(spacing);
            stringBuilder.append(plantPoint);
            stringBuilder.append(spacing);
            stringBuilder.append(pestIdentified);
        }

        return stringBuilder;

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        MyApplication.scanServiceActive=false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(DBSyncingComplete event) {
        Toast.makeText(this, "Sync Successfully completed", Toast.LENGTH_SHORT).show();
    }

}
