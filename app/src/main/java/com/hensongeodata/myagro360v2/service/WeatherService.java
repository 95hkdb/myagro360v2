package com.hensongeodata.myagro360v2.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.api.request.HttpHandler;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class WeatherService extends Service {

      private static final String TAG = WeatherService.class.getSimpleName();
      private MyApplication myApplication;
      public Runnable mRunnable = null;
      private IntentFilter intentFilter;
      NetworkRequest networkRequest;
      HttpHandler httpHandler;

      public WeatherService() {
      }

      @Override
      public IBinder onBind(Intent intent) {
            // TODO: Return the communication channel to the service.
            return null;
      }

      @Override
      public void onCreate() {
            super.onCreate();
            Log.e(TAG, "onCreate: Created in service" );

            myApplication = new MyApplication();
            networkRequest = new NetworkRequest(this);
      }

      @Override
      public int onStartCommand(Intent intent, int flags, int startId) {
            final Handler mHandler = new Handler();

            mRunnable = new Runnable() {
                  @Override
                  public void run() {
                        if (myApplication.hasNetworkConnection(getApplicationContext())){
                                    new GetWeather().execute();
                        }
                  }
            };
            mHandler.postDelayed(mRunnable, 12 *10000);

            return START_NOT_STICKY;
      }

      @SuppressLint("StaticFieldLeak")
      class GetWeather extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                  String api_key = "8ErkyMLldtp33M1UYN3kpDE5mQlB809pmhJWgC7a";
                  String  lat ="";
                  String log = "";

                  if (MyApplication.poi != null) {
                        lat = MyApplication.poi.getLat();
                        log = MyApplication.poi.getLon();

                        String logitude = log;
                        String latitude = lat;

                        httpHandler = new HttpHandler();
                        String url = "https://weather.api.silapha.com/?api_key=" + api_key + "&lon=" + logitude + "&lat=" + latitude;
                        String jsonString = httpHandler.makeGetCall(url);

                        if (jsonString != null) {

                              try {
                                    JSONObject jsonObj = new JSONObject(jsonString);
                                    String weatherLocation = jsonObj.getString("LocationName");
                                    String weatherType = jsonObj.getString("WeatherMain");
                                    String weatherDescrip = jsonObj.getString("WeatherDescription");
                                    String weatherTemp = jsonObj.getString("Temperature");
                                    String weatherHumi = jsonObj.getString("Humidity");
                                    String weatherPress = jsonObj.getString("Pressure");

//                                    Log.e(TAG, "doInBackground: " + weatherType );

                                    BroadcastCall.publishWeather(getApplicationContext(), weatherLocation, weatherType,weatherDescrip,
                                            weatherTemp, weatherHumi, weatherPress);
//                                    SharePrefManager.getInstance(getApplicationContext()).newWeather(weatherLocation, weatherType, weatherDescrip, weatherTemp, weatherHumi, weatherPress);
                              } catch (final JSONException e) {
                                    //progressBar.setVisibility(View.GONE);
                                    Log.v(TAG, "Weather Json parsing error: " + e.getMessage());
                              }
                        }
                  }
                  return null;
            }
      }
}
