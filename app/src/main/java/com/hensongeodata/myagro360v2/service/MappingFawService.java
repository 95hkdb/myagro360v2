package com.hensongeodata.myagro360v2.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import androidx.annotation.Nullable;
import android.util.Log;

import com.hensongeodata.myagro360v2.Interface.NetworkResponse;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.receiver.BootReceiver;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by brasaeed on 03/06/2017.
 */

public class MappingFawService extends Service implements NetworkResponse{
    private static String TAG = MappingFawService.class.getSimpleName();
    public static boolean dispatching=false;
    private NetworkRequest networkRequest;
    private DatabaseHelper databaseHelper;
    private static int STATE_DB=100;
    private static int STATE_LIVE=200;
    private int state=-1;
    private static int counter=0;
    ArrayList<InstructionScout> itemsDB;
    ArrayList<InstructionScout> itemsLive;
    InstructionScout instructionScout=null;
    Call<ResponseBody> call;

    private Runnable runnable;
    private Handler handler;
    private long TIME=60000;
    private String item_id;

    @Override
    public void onCreate() {
        super.onCreate();

        handler=new Handler();
        databaseHelper=new DatabaseHelper(this);
        networkRequest=new NetworkRequest(this);
        itemsDB =databaseHelper.getScoutList(ChatbotAdapterRV.MODE_MAP);

       // prepareFile();

        dispatch();

        runnable=new Runnable() {
            @Override
            public void run() {
                dispatch();
                handler.postDelayed(runnable,TIME);
            }
        };

        handler.postDelayed(runnable,TIME);

        MyApplication.mapServiceActive=true;
        Log.d(TAG, "oncreate looks good");
    }

    private void dispatch(){
        Log.d(TAG,"dispatch");
            if(!dispatching) {
                Log.d(TAG,"not dispatching");
                if(databaseHelper.getScoutList(ChatbotAdapterRV.MODE_MAP).size()>0)
                         BootReceiver.showNotification(this,getResources().getString(R.string.app_name),"Syncing...");

                prepareFile();
            }
    }

    private void prepareFile(){
        dispatching=true;

        if(MyApplication.modeScout==ChatbotAdapterRV.MODE_MAP)
            itemsLive=MyApplication.itemsLive;
        else
            itemsLive=new ArrayList<>();

        if(itemsLive!=null&&itemsLive.size()>0){
            Log.d(TAG,"itemsLive size: "+itemsLive.size());
            state=STATE_LIVE;
            instructionScout= itemsLive.get(0);
        }
        else{
            state=STATE_DB;
            itemsDB=databaseHelper.getScoutList(ChatbotAdapterRV.MODE_MAP);
            Log.d(TAG,"itemsDB size: "+itemsDB.size());
            Log.d(TAG,"itemsDB: "+itemsDB);
            if(itemsDB !=null&& itemsDB.size()>0) {
                Log.d(TAG,"itemsDB size: "+itemsDB.size());

                instructionScout = itemsDB.get(0);
            }
            else {
                Log.d(TAG,"killing service");
                dispatching=false;
                return;
            }
        }

        Log.d(TAG,"instruction scout "+instructionScout);
        if(instructionScout!=null) {
            item_id = instructionScout.getId();
            POI poi=new POI();

            String message=instructionScout.getMessage();
            Log.d(TAG,"date: "+message);
            poi=(new CreateForm(this)).resolvePOI(message);

            if(poi.getAccuracy()!=null)
                sendLocation(poi, instructionScout.getScout_id());
            else
                removeProccessed();

        }
        else{
            removeProccessed();
           // prepareFile();
        }

    }

    private void removeProccessed(){
        //removeProccessed the most recently scanned item from pending scan list
        if(state==STATE_DB) {
            if (itemsDB.size() > 0) {
                Log.d(TAG, "remove db");
                itemsDB.remove(0);
            }
        }
        else if(state==STATE_LIVE&&MyApplication.modeScout== ChatbotAdapterRV.MODE_MAP) {
            if (MyApplication.itemsLive != null && MyApplication.itemsLive.size() > 0) {
                MyApplication.itemsLive.remove(0);
                Log.d(TAG, "remove live "+MyApplication.itemsLive.size());
            }
            if(MyApplication.itemsLive!=null)
                    itemsLive = MyApplication.itemsLive;
            else
                itemsLive=new ArrayList<>();
        }

        prepareFile();
    }

    public void sendLocation(POI poi,String mapping_id){
        Log.d(TAG,"send location "+state);
        networkRequest.sendFAWMapping(poi,mapping_id,this);
    }

//    private void processResult(int status,String JoinCommunityResponse){
//        if(status==NetworkRequest.STATUS_SUCCESS){
//            try {
//                JSONObject jsonObject=new JSONObject(JoinCommunityResponse);
//
//                if(jsonObject.has("success")){
//                    int success=jsonObject.getInt("success");
//                    String date=jsonObject.optString("date");
//                    Log.d(TAG,"success: "+success);
//                    if(success==1){
//                        int detected=jsonObject.optInt("detected");
//
//                            databaseHelper.addScoutItem(ScanModel.scannning_id,
//                                    InstructionScout.STATE_PROCCESSED,instructionScout.getMessage(),
//                                    ChatbotAdapterRV.MODE_MAP);
//
//                            removeProccessed();
//                            if(MyApplication.taskListener!=null)
//                                MyApplication.taskListener.onTaskStopped(ScanModel.scannning_id,true);
//
//                    }
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//
//        prepareFile();
//    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MyApplication.scanServiceActive=false;
    }

    @Override
    public void onRespond(Object object) {
        Log.d(TAG,"JoinCommunityResponse: "+object);
        if(object!=null&&instructionScout!=null) {
            Log.d(TAG,"saving...");
            databaseHelper.addScoutItem(item_id,
                    InstructionScout.STATE_PROCCESSED, instructionScout.getMessage(),
                    ChatbotAdapterRV.MODE_MAP);
        }

        removeProccessed();
    }
}
