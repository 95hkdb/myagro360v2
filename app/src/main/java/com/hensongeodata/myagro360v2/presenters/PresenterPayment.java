package com.hensongeodata.myagro360v2.presenters;

import com.hensongeodata.myagro360v2.contract.ContractPayment;
import com.hensongeodata.myagro360v2.model.Pay;

import io.reactivex.disposables.CompositeDisposable;

public class PresenterPayment implements ContractPayment.Presenter{

    private ContractPayment.View view;
    private Pay pay;
    private CompositeDisposable compositeDisposable;

    public PresenterPayment(ContractPayment.View view, Pay pay){
        this.pay = pay;
        compositeDisposable=new CompositeDisposable();
        this.view = view;
        this.view.setPresenter(this);
    }


    @Override
    public void initializePayment() {

    }

    @Override
    public void onError() {
        showPaymentError();
    }

    @Override
    public void onSuccess() {
        showPaymentSuccess();
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        compositeDisposable.clear();
    }

    private void showPaymentError(){
        view.showError();
    }

    private void showPaymentSuccess(){
        view.showSuccess();
    }
}
