package com.hensongeodata.myagro360v2;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Location;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.multidex.MultiDexApplication;

import com.akexorcist.localizationactivity.core.LocalizationApplicationDelegate;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.hensongeodata.myagro360v2.Interface.TaskListener;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.controller.TypefaceUtil;
import com.hensongeodata.myagro360v2.fragment.BSFragment;
import com.hensongeodata.myagro360v2.model.Country;
import com.hensongeodata.myagro360v2.model.Device;
import com.hensongeodata.myagro360v2.model.District;
import com.hensongeodata.myagro360v2.model.FileField;
import com.hensongeodata.myagro360v2.model.Form;
import com.hensongeodata.myagro360v2.model.Guide;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.ImageMulti;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.model.Message;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.model.POIGroup;
import com.hensongeodata.myagro360v2.model.Region;
import com.hensongeodata.myagro360v2.model.ScanFAW;
import com.hensongeodata.myagro360v2.model.ServiceClass;
import com.hensongeodata.myagro360v2.model.SettingsModel;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.model.Town;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.model.Video;
import com.hensongeodata.myagro360v2.receiver.PushReceiver;
import com.hensongeodata.myagro360v2.service.ScoutFawService;
import com.hensongeodata.myagro360v2.version2_0.Preference.DownloadsSharePref;
import com.orm.SugarContext;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TimeZone;
import java.util.TreeMap;

import io.fabric.sdk.android.Fabric;

//import android.support.v7.app.AlertDialog;
//import com.crashlytics.android.Crashlytics;
//import io.fabric.sdk.android.Fabric;

/**
 * Created by user1 on 9/25/2017.
 */

public class MyApplication extends MultiDexApplication implements TextToSpeech.OnInitListener{
    private static String TAG=MyApplication.class.getSimpleName();
    public static boolean online=false;
    public static boolean in_chat=false;
    public static boolean firstTime=false;
    public static POIGroup poiGroup;
    public static POI poi;
    public static ArrayList<FileField> filesToUpload;
    public static int filesToUploadSize=0;
    public static String tranx_id_attahcment;
    public static String form_id_attachment;
    public static Context context;

    public static int ACCURACY_DEFAULT = 25;
      public static String APP_URL = "https://play.google.com/store/apps/details?id=com.hensongeodata.myagro360v2";
    public static String APP_ID="5001";
    public static ServiceClass serviceClass;
    public static ArrayList<Country> countryList;
    public static ArrayList<String> sCountryList;
    public static ArrayList<String> sCountryIDList;
    public static ArrayList<Region> regionList;
    public static ArrayList<String> sRegionList;
    public static ArrayList<String> sRegionIDList;
    public static ArrayList<District> districtList;
    public static ArrayList<String> sDistrictList;
    public static ArrayList<String> sDistrictIDList;
    public static ArrayList<Town> townList;
    public static ArrayList<String> sTownList;
    public static ArrayList<String> sTownIDList;
    public static ArrayList<ServiceClass> serviceClasses;

    public static Image imageModel;
    public static Video videoModel;
    public static ArrayList<ImageMulti> imageMultiArrayList;
    public static ArrayList<ScanFAW> scanFAWList;
    public static ArrayList<Image> imageList;
    public static ArrayList<Video> videoList;

    public static int poiIndex=0;
    public static ArrayList<POI> fakePOIlist;

    public static TaskListener taskListener;
    public static ArrayList<InstructionScout> itemsLive;
    public static ArrayList<Integer> itemsLivePosition;
    public static int modeScout;
    public static ArrayList<Guide> guides;
    public static boolean scanServiceActive=false;
    public static boolean mapServiceActive=false;
    public static TextToSpeech tts;
    public static boolean ttsAvailable;
    public static Toast toast;
    private Locale locale;
    public static Message xternalMessage;
    private LocalizationApplicationDelegate localizationDelegate = new LocalizationApplicationDelegate(this);

    private RequestQueue mRequestQueue;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        tts=new TextToSpeech(this,this);
        SugarContext.init(this);

            filesToUpload=new ArrayList<>();
            //videoList=new ArrayList<>();
            guides=new ArrayList<>();
            poiGroup=new POIGroup();
            poiGroup.setPoiList(new ArrayList<POI>());
            poi=new POI();

        initLists();

        scanFAWList=new ArrayList<>();
        imageMultiArrayList=new ArrayList<>();
        imageList=new ArrayList<>();
        videoList=new ArrayList<>();

        Log.d(TAG,"oncreate");
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Lato-Regular.ttf");

        context = getBaseContext();
            }

    public static Context getAppContext() {
        return context;
    }

    public static void initLists(){
        countryList=new ArrayList<>();
        sCountryList=new ArrayList<>();
        sCountryIDList=new ArrayList<>();
        regionList=new ArrayList<>();
        sRegionList=new ArrayList<>();
        sRegionIDList=new ArrayList<>();
        districtList=new ArrayList<>();
        sDistrictList=new ArrayList<>();
        sDistrictIDList=new ArrayList<>();
        townList=new ArrayList<>();
        sTownList=new ArrayList<>();
        sTownIDList=new ArrayList<>();
    }

    public static long getPerimeter(List<POI> poiList){
        int perimeter=0;
        if(poiList.size()>1) {

            for (int a = 0; a < poiList.size(); a++) {
                if(a+1<poiList.size())
                    perimeter+=(convertPOI(poiList.get(a))).distanceTo((convertPOI(poiList.get(a+1))));
                else
                    perimeter+=convertPOI(poiList.get(0)).distanceTo(convertPOI(poiList.get(a)));

            }
        }

        return perimeter;
    }

    private static Location convertPOI(POI poi){
        Location location=new Location("");
        location.setLongitude(Double.valueOf(poi.getLon()));
        location.setLatitude(Double.valueOf(poi.getLat()));

        return location;
    }

    public static int getArea(List<POI> poiList){
        List<LatLng> latlonList=new ArrayList();

        for (int a = 0; a < poiList.size(); a++) {
            POI poi=poiList.get(a);
           latlonList.add(new LatLng(Double.valueOf(poi.getLat()),Double.valueOf(poi.getLon())));
        }

        int area=(int)Math.floor(SphericalUtil.computeArea(latlonList));
        return area;
    }

    public POI getInstantPOI(Context context,boolean insistOnAccuracy){
        Log.d(TAG,"instant poi");
//        MyApplication.poi=new POI();
//        MyApplication.poi.setAlt("9");
//        MyApplication.poi.setLon("333");
//        MyApplication.poi.setLat("00");
//        MyApplication.poi.setAlt("9");

        int maxAccur=(new SettingsModel(context)).getAccuracy_max();

//        return MyApplication.poi;

        if(isEmptyPOI(MyApplication.poi)){
            Toast.makeText(context,"Instant way point still reading...",Toast.LENGTH_SHORT).show();
            return new POI();
        }
        else if(Integer.parseInt(MyApplication.poi.getAccuracy())>maxAccur && insistOnAccuracy){
            Toast.makeText(context,"Accuracy should be less than "+(maxAccur+1)+" to proceed",Toast.LENGTH_SHORT).show();
            return new POI();
        }
        else {
            Toast.makeText(context,"Instant way point attached",Toast.LENGTH_SHORT).show();
            return MyApplication.poi;
        }
    }

    public static boolean isEmptyPOI(POI poi){
//        MyApplication.poi=new POI();
//        MyApplication.poi.setAlt("9");
//        MyApplication.poi.setLon("333");
//        MyApplication.poi.setLat("00");
//        MyApplication.poi.setAlt("9");

//        poi=MyApplication.poi;
        return poi==null
                ||poi.getAccuracy()==null
                ||poi.getAccuracy().trim().isEmpty();

    }


    public static Bitmap getBitmap(String imageString){
        if(imageString==null||imageString==""||imageString.isEmpty())
            return null;

        byte[]imageBytes= Base64.decode(imageString.getBytes(),Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageBytes,0,imageBytes.length);
    }

    public ArrayList<ServiceClass> getServicesLoaded(){
            return serviceClasses;
    }


    public static Image getImageModel() {
        return imageModel;
    }

    public static void setImageModel(Image imageModel) {
        MyApplication.imageModel = imageModel;
    }

    public static Video getVideoModel() {
        return videoModel;
    }

    public static void setVideoModel(Video videoModel) {
        MyApplication.videoModel = videoModel;
    }

    public boolean hasSession(Context context){
        User user=new User(context);
//        user.setId("user"+(new Random()).nextInt(10000));
//        user.setFirstname("Kosi");
//        user.setLastname("Adzo");
//        user.setProfession_id("5348");
//        user.setEmail("ee.31@hgt.com");
//       // user.setType(context.getResources().getString(R.string.extension_officer));
//        user.setOrganization("hgt");
//        user.setHgt_user_id("hgt_user");
//        user.setPush_id("e401ce17d785018bc69d70");
//        user.setPhone("0271154324");
        return user.getEmail()!=null;
    }

    public boolean logout(Context context){
        User user=new User(context);
        user.setFirstname(null);
        user.setLastname(null);
        user.setPhone(null);
        user.setEmail(null);
        //user.setId(null);
        user.setPassword(null);

        SharePrefManager.getInstance(context).logout();
        DownloadsSharePref.clearScoutsDownloadedPreference(context);
        DownloadsSharePref.clearMapsDownloadedPreference(context);
        DownloadsSharePref.clearPlotsDownloadedPreference(context);

        return true;
    }

    public void initUser(Context context,String user_id){
        Log.d(TAG,"user_id: "+user_id);
        Log.d(TAG,"mUser_id: "+(new User(context)).getId());
        if(user_id!=null&&!user_id.trim().isEmpty()) {
            User mUser=new User(context);
            if (mUser.getId()!=null&&!mUser.getId().equalsIgnoreCase(user_id)
                    &&mUser.getId()!=null) {
                Log.d(TAG, "different");
//                DatabaseHelper databaseHelper = new DatabaseHelper(context);
//                SettingsModel settings = new SettingsModel(context);
//
//                if (databaseHelper.removeForms()) {
//                    databaseHelper.mashTable(DatabaseHelper.TBL_ORGANIZATION);
//                    databaseHelper.mashTable(DatabaseHelper.TBL_POIS);
//                    databaseHelper.mashTable(DatabaseHelper.TBL_TRANSACTION);
//                    databaseHelper.mashTable(DatabaseHelper.TBL_POI_GROUP);
//                    settings.setTransaction_id(null);
//                    settings.setForm_id(null);
//                    settings.setOrg_id(null);
//                }
                (new DatabaseHelper(context)).deleteDatabase(context);
                SettingsModel settingsModel=new SettingsModel(context);
                settingsModel.setForm_id(null);
                settingsModel.setOrg_id(null);
                settingsModel.setAccuracy_max(10);
                settingsModel.setArchive_mode(false);
                settingsModel.setTransaction_id(null);

            }
        }
        CreateForm.getInstance(context).setShouldRefreshForm(true);
    }

    public boolean hasNetworkConnection(Context context) {
        boolean hasWifi=false;
        boolean hasMobileData=false;

        ConnectivityManager connectivityManager= (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos=connectivityManager.getAllNetworkInfo();

        for(NetworkInfo ni:networkInfos){
            if(ni.getTypeName().equalsIgnoreCase("WIFI")){
                if(ni.isConnected())
                    hasWifi=true;
            }

            if(ni.getTypeName().equalsIgnoreCase("MOBILE")){
                if(ni.isConnected())
                    hasMobileData=true;
            }
        }

        return hasMobileData|| hasWifi;
    }


    public void showMessage(Context context,String header,String message){
//        new AlertDialog.Builder(context)
//                .setTitle(header)
//                .setMessage(message)
//                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onAddMapButtonClicked(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//                    }
//                })
//                .show();
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }

    public void showMessage(Context context,String message){
//        new AlertDialog.Builder(context)
//                .setMessage(message)
//                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onAddMapButtonClicked(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//                    }
//                })
//                .show();
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }

    public void showInternetError(Context context){
//        new AlertDialog.Builder(context)
//                .setTitle(context.getResources().getString(R.string.no_internet_caption))
//                .setMessage(context.getResources().getString(R.string.no_internet_message))
//                .setNegativeButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onAddMapButtonClicked(DialogInterface dialogInterface, int i) {
//                        dialogInterface.cancel();
//                    }
//                }).show();

        Toast.makeText(context,context.getResources().getString(R.string.no_internet_message),Toast.LENGTH_SHORT).show();
    }

    public static boolean hasLocation(){
        return MyApplication.poi!=null&&MyApplication.poi.getAccuracy()!=null;
    }

    public static void showToast(Context context,String message,int gravity){
        Toast toast=Toast.makeText(context,message,Toast.LENGTH_SHORT);
        toast.setGravity(gravity,0,0);
        toast.show();
    }

    public static void cancelToast(){
        if(toast!=null)
            toast.cancel();
    }

    public Animation blinkAnim(){
        Animation blink=new AlphaAnimation(1.0f,0.5f);
        blink.setDuration(1000);
        blink.setRepeatMode(Animation.REVERSE);
        blink.setRepeatCount(Animation.INFINITE);

        return blink;
    }

    public static String encodeBitmap(Bitmap bitmap) {
        Log.d(TAG,"bitmap: "+bitmap);
        if(bitmap==null)
            return null;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
    }

    public Bitmap bitmapCompressLarge(String imagePath) {
        float maxHeight = 960.0f;
        float maxWidth = 640.0f;
        // float maxHeight = 1080.0f;
        // float maxWidth = 720.0f;

        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);
        // Bitmap bmp=bitmap;
        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            if(actualWidth<1&&actualHeight<1){
                return null;
            }

            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        byte[] byteArray = out.toByteArray();

        Bitmap updatedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        return updatedBitmap;
    }

    public static Bitmap bitmapCompressLarge(Bitmap bitmap){
       if(bitmap!=null) {
           ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
           bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytearrayoutputstream);

           byte[] BYTE = bytearrayoutputstream.toByteArray();

           return BitmapFactory.decodeByteArray(BYTE, 0, BYTE.length);
       }
       else
           return null;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }

    public Bitmap compressBitmap(Bitmap bitmap){
        ByteArrayOutputStream bytearrayoutputstream= new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,50,bytearrayoutputstream);
        return BitmapFactory.decodeByteArray(bytearrayoutputstream.toByteArray(),0,bytearrayoutputstream.toByteArray().length);

    }

    public String getCurrentTime(){
        TimeZone tz = TimeZone.getTimeZone("GMT+00:00");
        Calendar c = Calendar.getInstance(tz);

        return String.format("%02d" , c.get(Calendar.HOUR_OF_DAY))+" : "+
                String.format("%02d" , c.get(Calendar.MINUTE))+" : "+
                String.format("%02d" , c.get(Calendar.SECOND));
    }

    public String getCurrentDate(){

        DateFormat df = new SimpleDateFormat("yyy-MM-dd");
        df.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));

        String date=df.format(new Date());

        return date;
        // return  (new SimpleDateFormat("yyyy-MM-dd")).format(c);
    }

    public boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public Device getDevice(){
        Device device=new Device();

        device.setName(Build.DEVICE);
        device.setAndroidVersion(Build.VERSION.RELEASE);
        device.setModel(Build.MODEL);
        device.setSerial(Build.SERIAL);

        return device;
    }


    /*public static POI getFakePOI(){
        POI poi=fakePOIlist.get(poiIndex);
        poiIndex++;
        if (poiIndex==4)
            poiIndex=0;
        return poi;
    }*/



    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();
    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "G");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }


    //Change numbers to formats like 2k, 5.5M views
    public static String format(long value) {
        //  long value=changeFormat(v);
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
        if (value == Long.MIN_VALUE) return format(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + format(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case

        Map.Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

    public static long getMillis(String date,String time){
        date=date.replace(" ","");
        time=time.replace(" ","");
        String strDate=date+" "+time;
        Log.d(TAG,"strDate: "+strDate);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        long timeMillis=-1;
        try {
            Date mDate = formatter.parse(strDate);
            Log.d(TAG,"date: "+mDate);
            timeMillis = mDate.getTime();
            Log.d(TAG,"timeMillis: "+timeMillis);
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d(TAG,"e: "+e);
        }

        Log.d(TAG,"timeMillis2: "+timeMillis);
        return timeMillis;
    }

    public void selectPOIGroup(Context context,DatabaseHelper databaseHelper){
        MyApplication.poiGroup=databaseHelper.getPOIGroup();
        Log.d(TAG,"poiGroup: "+poiGroup.getId());
        if(MyApplication.poiGroup==null||MyApplication.poiGroup.getId()==null
                ||MyApplication.poiGroup.getId().trim().isEmpty())
            setDefaultPoiGroup(databaseHelper);

        BroadcastCall.publishLocationUpdate(context, NetworkRequest.STATUS_SUCCESS,BSFragment.ACTION_ADD_POI_GROUP);
    }

    public void setDefaultPoiGroup(DatabaseHelper databaseHelper){
        POIGroup poiGroup=new POIGroup();
        poiGroup.setName(databaseHelper.createTransactionID());
        poiGroup.setName(BSFragment.DEAFAULT_GROUP_NAME);
        poiGroup.setSelect(false);
        poiGroup.setPoiList(new ArrayList<POI>());

        MyApplication.poiGroup=poiGroup;

        Log.d(TAG,"set Default poiGroup: "+MyApplication.poiGroup);
        //BroadcastCall.publishLocationUpdate(context, NetworkRequest.STATUS_SUCCESS,BSFragment.ACTION_ADD_POI_GROUP);

    }

//    public void buildGuide(){
//        guides=new ArrayList<>();
//        Guide guide1=new Guide(
//                R.drawable.guide_one,
//                "Left Main Menu",
//                 "1. List of menu items each performing a unique functionality."
//                );
//
//        Guide guide2=new Guide(
//                R.drawable.guide_two,
//                "Organizations Menu Item",
//                "1. Tap on background to select organization." +
//                        "\n2. Tap to search through list of organizations" +
//                        "\n3. Tap to view organization details" +
//                        "\n4. Tap to refresh list of organizations"
//        );
//
//        guides.add(guide1);
//        guides.add(guide2);
//    }

    private boolean checkCameraHardware(Context context) {
        // this device has a camera
// no camera on this device
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    public static void refreshScanService(Context context){
        if(!MyApplication.scanServiceActive)
            context.startService(new Intent(context, ScoutFawService.class));

     //   if(!MyApplication.mapServiceActive)
     //       context.startService(new Intent(context, MappingFawService.class));

    }

    public int containsInstructScout(List<InstructionScout> instructionScouts, InstructionScout instructionScout) {

        return Collections.binarySearch(instructionScouts,instructionScout, new Comparator<InstructionScout>() {
            @Override
            public int compare(InstructionScout o1, InstructionScout o2) {
                Log.d(TAG,"id1:"+o1.getId_Integer()+" id2: "+o2.getId_Integer());
                return o1.getId().compareTo(o2.getId());
            }
        });
    }

    @Override
    public void onInit(int i) {
        if(i==TextToSpeech.SUCCESS){
            int result=tts.setLanguage(Locale.ENGLISH);
            if(result==TextToSpeech.LANG_MISSING_DATA||result==TextToSpeech.LANG_NOT_SUPPORTED){}
            else{
                ttsAvailable=true;
            }
        }
    }

    public static void initLive(Context context,TaskListener listener){
        MyApplication.taskListener=listener;
        MyApplication.itemsLive=new ArrayList<>();
        MyApplication.itemsLivePosition=new ArrayList<>();
        MyApplication.refreshScanService(context);
    }

    public static void shutdownLive(){
        MyApplication.taskListener=null;
        MyApplication.itemsLive=new ArrayList<>();
        MyApplication.itemsLivePosition=new ArrayList<>();
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        if(MyApplication.tts!=null) {
            MyApplication.tts.stop();
            MyApplication.tts.shutdown();
        }

        SugarContext.terminate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(localizationDelegate.attachBaseContext(base));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        localizationDelegate.onConfigurationChanged(this);
    }

    @Override
    public Context getApplicationContext() {
        return localizationDelegate.getApplicationContext(super.getApplicationContext());
    }

    public static void simulateMessage(final Context context){
        final int[] message_id = {0};
        final Intent[] intent = new Intent[1];
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                intent[0] = new Intent();
                intent[0].putExtra("id","message_"+ message_id[0]);
                intent[0].putExtra("text","here is my "+ message_id[0] +" date");
                intent[0].putExtra("image_url","image");
                intent[0].putExtra("author_id","external author");
                intent[0].putExtra("author_name","Kojo Stiles");

                PushReceiver.pushMessage(context, intent[0]);
                message_id[0]++;
                (new Handler()).postDelayed(this,5000);
            }
        },5000);
    }

    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage)
            throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(context, img, selectedImage);
        return img;
    }

    private static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = context.getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            ei = new ExifInterface(selectedImage.getPath());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    private boolean checkGooglePlayServicesAvailable(AppCompatActivity appCompatActivity)
    {
        final int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (status == ConnectionResult.SUCCESS)
        {
            return true;
        }
        if (GooglePlayServicesUtil.isUserRecoverableError(status))
        {
            final Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(status, appCompatActivity, 1);
            if (errorDialog != null)
            {
                errorDialog.show();
            }
        }

        return false;
    }

    public static boolean isInteger(String str){
        if(str==null)
            return false;

        try{
            int num = Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean shouldMap(Context context, String form_id){
   //     return  true;/*

        DatabaseHelper databaseHelper=new DatabaseHelper(context);
        Form form=databaseHelper.getForm(form_id);
        Log.d(TAG,"form mapping: "+(form!=null?form.mapping:"null"));
        return form!=null&&form.mapping== Form.MAPPING_ENABLED;
   }

    public static boolean hasMap(Context context,String form_id){
        DatabaseHelper databaseHelper=new DatabaseHelper(context);
        ArrayList<String> items=databaseHelper.getScoutIDList(form_id, ChatbotAdapterRV.MODE_MAP);
        Log.d(TAG,"hasMap items size: "+(items!=null?items.size():"null"));
        return (items!=null&&items.size()>0);
    }

    public RequestQueue getRequestQueue(Context mcontext) {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mcontext);
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag, Context mContext) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue(mContext).add(req);
    }

    public <T> void addToRequestQueue(Request<T> req, Context mContext) {
        req.setTag(TAG);
        getRequestQueue(mContext).add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
