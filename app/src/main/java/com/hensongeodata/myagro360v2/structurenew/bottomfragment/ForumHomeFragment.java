package com.hensongeodata.myagro360v2.structurenew.bottomfragment;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.victor.loading.rotate.RotateLoading;

public class ForumHomeFragment extends Fragment {

      View forumView;
      WebView forumWeb;
      MyApplication myApplication;
      RotateLoading rotateLoading;

      public ForumHomeFragment() {
            // Required empty public constructor
      }

      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            forumView = inflater.inflate(R.layout.forum_layout, container, false);

            init();

            return forumView;
      }

      private void init() {

            myApplication = new MyApplication();
            rotateLoading = forumView.findViewById(R.id.forum_loader);
            forumWeb = forumView.findViewById(R.id.forum_web);

            forumNavigation(getActivity());

      }

      private void forumNavigation(final Context context) {

            rotateLoading.setLoadingColor(context.getResources().getColor(R.color.colorBlue80));
            rotateLoading.start();

            forumWeb.getSettings().setJavaScriptEnabled(true);
            forumWeb.getSettings().setAppCacheEnabled(true);
            forumWeb.loadUrl("https://forum.myagro360.com/");
            forumWeb.setWebViewClient(new WebViewClient() {
                  public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                        webView.loadUrl("file:///android_assets/error.html");
                        myApplication.showMessage(context, "Unable to load url");
                  }

                  public void onPageFinished(WebView view, String url) {
                        // do your stuff here
                        rotateLoading.stop();
                  }

            });
      }

}
