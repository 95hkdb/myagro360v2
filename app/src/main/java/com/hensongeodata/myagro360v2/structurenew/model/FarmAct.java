package com.hensongeodata.myagro360v2.structurenew.model;

public class FarmAct {

      private String title;
      private String image;
      private String price;

      public String getTitle() {
            return title;
      }

      public void setTitle(String title) {
            this.title = title;
      }

      public String getImage() {
            return image;
      }

      public void setImage(String image) {
            this.image = image;
      }

      public String getPrice() {
            return price;
      }

      public void setPrice(String price) {
            this.price = price;
      }
}
