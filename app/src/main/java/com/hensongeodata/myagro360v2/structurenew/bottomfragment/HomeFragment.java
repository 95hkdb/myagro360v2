package com.hensongeodata.myagro360v2.structurenew.bottomfragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.service.WeatherService;
import com.hensongeodata.myagro360v2.view.LibraryActivity;

public class HomeFragment extends Fragment {

    private static final String TAG = HomeFragment.class.getSimpleName();

    View homeView;
    private IntentFilter intentFilter;
    private HomeFragmentBroadcastReceiver receiver;
    private Runnable mRunnable = null;
    private MyApplication myApplication;
    private NetworkRequest networkRequest;

    private TextView tvGreeting, weather_name, weather_loca, weather_temp, weather_humidity, weather_description;
    private TextView tvRecentActivity, tvRecentFeed;
    private TextView tvLibrary;
    ImageView weather_icon;
    private RelativeLayout weather_rela;
    Animation slide_in_from_left;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        homeView = inflater.inflate(R.layout.fragment_home, container, false);

        init();
        return homeView;
    }

    private void init(){
        intentFilter = new IntentFilter(BroadcastCall.WEATHER);
        receiver = new HomeFragmentBroadcastReceiver();
        myApplication = new MyApplication();
        networkRequest = new NetworkRequest(getActivity());

        tvRecentActivity = homeView.findViewById(R.id.recent_activity_text);
        tvRecentFeed = homeView.findViewById(R.id.feed_text);
        tvGreeting = homeView.findViewById(R.id.greeting);
        tvLibrary = homeView.findViewById(R.id.tv_learn);
        weather_rela = homeView.findViewById(R.id.weather_relative);
        weather_name = homeView.findViewById(R.id.weather_name_text);
        weather_loca = homeView.findViewById(R.id.weather_location_text);
        weather_temp = homeView.findViewById(R.id.weather_temp_text);
        weather_humidity = homeView.findViewById(R.id.weather_humidity_text);
        weather_description = homeView.findViewById(R.id.weather_description_text);
        weather_icon = homeView.findViewById(R.id.weather_icon);

        slide_in_from_left = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_left);

        getActivity().registerReceiver(receiver, intentFilter);
        getActivity().startService(new Intent(getActivity(), WeatherService.class));

//        String greet =  ((MenuDashboard)getActivity()).checkTime();

        tvRecentFeed.setText("No recent Feed.");
        tvRecentActivity.setText("No Recent Activity");
//        tvGreeting.setText("" + greet);
        tvGreeting.startAnimation(slide_in_from_left);
        tvLibrary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), LibraryActivity.class));
                getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });

          getCurrentActivities();
          getCurrentFeeds();

    }

    private void getCurrentActvity(){
          networkRequest.getRecentActivities();
    }

    private void getCurrentFeed(){
          networkRequest.recentPost();
    }

    private void getCurrentActivities(){
          getCurrentActvity();
          final Handler mHandler = new Handler();
          mRunnable = new Runnable() {
                @Override
                public void run() {
                      if (myApplication.hasNetworkConnection(getActivity())){
                            getCurrentActvity();
                      }
                }
          };
          mHandler.postDelayed(mRunnable, 12 *10000);
    }

    private void getCurrentFeeds(){
          getCurrentFeed();
          final Handler mHandler = new Handler();
          mRunnable = new Runnable() {
                @Override
                public void run() {
                      if (myApplication.hasNetworkConnection(getActivity())){
                            getCurrentFeed();
                      }
                }
          };
          mHandler.postDelayed(mRunnable, 12 *10000);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(receiver!=null)
            getActivity().unregisterReceiver(receiver);
    }

    class HomeFragmentBroadcastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(BroadcastCall.WEATHER)) {
                String weatherLocation = intent.getStringExtra("location");
                String weatherMainName = intent.getStringExtra("weather");
                String weatherDescrip = intent.getStringExtra("description");
                String weatherTemp = intent.getStringExtra("temperature");
                String weatherHumi = intent.getStringExtra("humidity");
                String weatherPress = intent.getStringExtra("pressure");

                double temp = Double.parseDouble(weatherTemp);
                double value = 273.15;
                double degreeCelsius = temp - value;

                String tempera = String.format("%.1f", degreeCelsius);

                weather_rela.setVisibility(View.VISIBLE);
                weather_rela.startAnimation(slide_in_from_left);

                if (weatherMainName.equalsIgnoreCase("clouds")) {
                    weather_icon.setImageResource(R.drawable.ic_cloud);
                    weather_icon.startAnimation(slide_in_from_left);
                }

                weather_loca.setText(weatherLocation);
                weather_name.setText(weatherMainName);
                weather_description.setText(weatherDescrip);
                weather_temp.setText(tempera);
                weather_humidity.setText(weatherHumi);
//                weatherPressure.setText(weatherPress);
            }
        }

    }

}
