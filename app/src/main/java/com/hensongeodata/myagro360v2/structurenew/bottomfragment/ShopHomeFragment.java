package com.hensongeodata.myagro360v2.structurenew.bottomfragment;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.victor.loading.rotate.RotateLoading;

public class ShopHomeFragment extends Fragment {

    View shopView;
    WebView asimeWeb;
    RotateLoading asimeLoading;
    MyApplication myApplication;
    SwipeRefreshLayout asimeSwipe;

    public ShopHomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        shopView = inflater.inflate(R.layout.fragment_shop, container, false);

        init();

        return shopView;
    }

    private void init(){

        myApplication = new MyApplication();
        asimeWeb = shopView.findViewById(R.id.asime_web);
        asimeLoading = shopView.findViewById(R.id.asime_loader);
        asimeSwipe = shopView.findViewById(R.id.asime_swipe);

        asimeSwipe.setColorSchemeResources(R.color.colorOrange10, R.color.colorBlue20,R.color.colorGreen40,R.color.colorRed40);
        asimeSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadShop(getActivity());
            }
        });

        loadShop(getActivity());
    }

    private void loadShop(final Context context) {
//        asimeLoading.setLoadingColor(context.getResources().getColor(R.color.colorBlue80));
//        asimeLoading.start();

        WebSettings webSettings = asimeWeb.getSettings();
        webSettings.setBuiltInZoomControls(true);
        webSettings.setJavaScriptEnabled(true);
        asimeWeb.getSettings().setAppCacheEnabled(true);
        asimeWeb.loadUrl("https://asime.online/");
        asimeSwipe.setRefreshing(true);
        asimeWeb.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                        webView.loadUrl("file:///android_assets/error.html");
                myApplication.showMessage(context, "Unable to load url");
            }

            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                asimeSwipe.setRefreshing(false);
//                asimeLoading.stop();
            }

        });
    }

}
