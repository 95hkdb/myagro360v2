package com.hensongeodata.myagro360v2.structurenew.bottomfragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.hensongeodata.myagro360v2.R;

public class FarmHomeFragment extends Fragment {

      private FarmHomeFragmentListener farmHomeFragmentListener;

      @Nullable
      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_farm_activity, container, false);
            initComponent(view);

            return view;
      }


      private void initComponent(View view) {

            ( view.findViewById(R.id.add_new_activity_button)).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        onAddFarmActivityButton();
                  }
            });
      }

      public void onAddFarmActivityButton(){
            if (farmHomeFragmentListener != null){
                  farmHomeFragmentListener.onAddButtonClicked();
            }
      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof  FarmHomeFragmentListener){
                  farmHomeFragmentListener = (FarmHomeFragmentListener)context;
            }else{
                  throw new RuntimeException(context.toString()
                  + " must implement FarmHomeFragmentListener");
            }
      }

      public interface FarmHomeFragmentListener{
            void onAddButtonClicked();
      }

}

