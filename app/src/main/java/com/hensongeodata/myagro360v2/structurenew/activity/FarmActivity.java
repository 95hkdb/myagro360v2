package com.hensongeodata.myagro360v2.structurenew.activity;

import android.content.Intent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.view.MenuDashboard;

import org.angmarch.views.NiceSpinner;
import org.angmarch.views.OnSpinnerItemSelectedListener;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FarmActivity extends AppCompatActivity {

      private static final String TAG = FarmActivity.class.getSimpleName();

      NiceSpinner niceSpinner1;
      CircleImageView orgLogo;
      ImageButton log_back, shop;
      FloatingActionButton saved;
      TextView orgName, selectedname;
      EditText duration, workers, comment;
      TextView text;

      NetworkRequest networkRequest;

      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_farm);
            init();

      }

      private void init(){

            niceSpinner1 = findViewById(R.id.nice_spinner1);
            duration = findViewById(R.id.act_duration);
            workers = findViewById(R.id.act_num);
            comment = findViewById(R.id.activity_comment);
            orgLogo = findViewById(R.id.org_logo);
            orgName = findViewById(R.id.toolbar_title);
//            shop = findViewById(R.id.shop_logo);
            log_back = findViewById(R.id.tool_back);
            saved = findViewById(R.id.float_save);
            selectedname = findViewById(R.id.textcollect);
//            text = findViewById(R.id.textText);

            networkRequest = new NetworkRequest(this);

            log_back.setVisibility(View.VISIBLE);
            orgLogo.setVisibility(View.GONE);
//            text.setVisibility(View.GONE);
            log_back.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
            orgName.setText("Farm Activity");
            List<String> dataset = new LinkedList<>(Arrays.asList("Tillage", " Ploughing Fields"," Sowing Seeds", "Harvesting", "Grazing"," Feeding Livestocks"));

            niceSpinner1.attachDataSource(dataset);

            spinnerSelected();
            goBack();

      }

      private void spinnerSelected(){

            //final String duration_time = duration.getText().toString();

            niceSpinner1.setOnSpinnerItemSelectedListener(new OnSpinnerItemSelectedListener() {
                  @Override
                  public void onItemSelected(NiceSpinner parent, View view, int position, long id) {
                        String itemSelected  = String.valueOf(parent.getItemAtPosition(position));
                        selectedname.setText(itemSelected);
                        Log.e(TAG, "spinner " + itemSelected);
                  }
            });

            saved.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        String act_type = selectedname.getText().toString();
                        String duration_time = duration.getText().toString();
                        String workers_in = workers.getText().toString();
                        String mycomment= comment.getText().toString();
                        if (checkData(act_type, duration_time, workers_in)) {

                             // Toast.makeText(FarmActivityModel.this, "Selected " + act_type + ", "+ duration_time, Toast.LENGTH_LONG).show();
                              networkRequest.sendActivity(act_type, duration_time, mycomment, workers_in);
                              Log.e(TAG, "Saving clicked");
                              comment.setText("");
                              duration.setText("");
                              act_type = "";

                        }

                  }
            });
      }

      private void goBack(){
            log_back.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        finish();
                        startActivity(new Intent(getApplicationContext(), MenuDashboard.class));
                  }
            });
      }

      private boolean checkData(String type, String duration_time, String number_workers) {

            if (duration_time.isEmpty()){
                  Toast.makeText(this, "Please enter the duration of activity", Toast.LENGTH_SHORT).show();
                  duration.setError("Please enter the duration of activity");
                  return false;
            }
            if (type.isEmpty()){
                  Toast.makeText(this, "Please select activity type", Toast.LENGTH_SHORT).show();
                  return false;
            }

            if (number_workers.isEmpty()) {
                  Toast.makeText(this, "Please enter the duration of activity", Toast.LENGTH_SHORT).show();
                  workers.setError("Please enter the total number of workers");
                  return false;
            }

            return true;
      }

      @Override
      public void onBackPressed() {
            super.onBackPressed();
            finish();
            startActivity(new Intent(getApplicationContext(), MenuDashboard.class));
      }
}
