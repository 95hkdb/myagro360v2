package com.hensongeodata.myagro360v2.structurenew.activity;

import android.app.Activity;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.version2_0.Fragment.MainHomeFragment;
import com.hensongeodata.myagro360v2.structurenew.bottomfragment.HomeFragment;
import com.hensongeodata.myagro360v2.structurenew.bottomfragment.ForumHomeFragment;
import com.hensongeodata.myagro360v2.structurenew.bottomfragment.ShopHomeFragment;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.view.BaseActivity;

import java.util.Calendar;
import java.util.Date;

public class HomePage extends BaseActivity {

      private static final String FRAGMENT_OTHER = "other_fragments" ;
      private static final String FRAGMENT_HOME = "home_fragment" ;
      private static String TAG = HomePage.class.getSimpleName();

      private BottomNavigationView navigationView;
      private TextView organ;


      private Handler handler;
      private Runnable runnable;
      private boolean allowExit=false;

      private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
              = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                  Fragment fragment;
                  switch (item.getItemId()) {
                        case R.id.navigation_home:
                              fragment = new MainHomeFragment();
                              loadFragment(fragment);

                              viewFragment(new HomeFragment(), FRAGMENT_HOME);
                              toastSelectedItem();
                              return true;
                        case R.id.navigation_plots:
//                              fragment = new ShopHomeFragment();
//                              loadFragment(fragment);
                              viewFragment(new ShopHomeFragment(), FRAGMENT_OTHER);
                              toastSelectedItem();
                              return true;
                        case R.id.navigation_farm_activity:
//                              fragment = new ForumHomeFragment();
//                              loadFragment(fragment);
                              viewFragment(new ForumHomeFragment(), FRAGMENT_OTHER);
                              toastSelectedItem();
                              return true;
                        case R.id.navigation_learn:
//                              fragment = new FarmActivityFragment();
//                              loadFragment(fragment);
//                              viewFragment(new FarmActivityFragment(), FRAGMENT_OTHER);
                              toastSelectedItem();
                              return true;
                  }
                  return false;
            }
      };


      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_home_page);

            init();
      }

      private void init(){

            navigationView = findViewById(R.id.nav_view);
            navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
            organ = findViewById(R.id.organisation_name);

            organ.setText(SharePrefManager.getInstance(getApplicationContext()).getOrganisationDetails().get(1));

//            loadFragment(new HomeFragm/ent());
            viewFragment(new HomeFragment(), FRAGMENT_HOME);
      }

      private void loadFragment(Fragment fragment) {
            // load fragment
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
      }

      public String checkTime(){

            //Get the time of day
            Date date = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int hour = cal.get(Calendar.HOUR_OF_DAY);

            Log.e(TAG, "checkTime: " + String.valueOf(hour));


            //Set greeting
            String greeting = null;
            if(hour >=6 && hour<12){
                  greeting = "Good Morning";
            } else if(hour>= 12 && hour < 17){
                  greeting = "Good Afternoon";
            } else if(hour >= 17 && hour < 21){
                  greeting = "Good Evening";
            } else if(hour >= 21 && hour < 24){
                  greeting = "Good Night";
            }

            return greeting;

      }

      private void toastSelectedItem(){
            int selectedItem = navigationView.getSelectedItemId();
            MyApplication.showToast(getApplicationContext(), String.valueOf(selectedItem), Gravity.END);
      }

      private static void setHomeItem(Activity activity) {
            BottomNavigationView navigationView = (BottomNavigationView) activity.findViewById(R.id.navigation);
            navigationView.setSelectedItemId(R.id.navigation_home);
      }

      private void viewFragment(Fragment fragment, String name){
            final FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_container, fragment);
            // know how many fragments there are in the stack
            final int count = fragmentManager.getBackStackEntryCount();
            // 2. If the fragment is **not** "home type", save it to the stack
            if (name.equals(FRAGMENT_OTHER)){
                  fragmentTransaction.addToBackStack(name);
            }
            // commit !
            fragmentTransaction.commit();

            // 3. After the commit, if the fragment is not an "home type" the back stack is changed, triggering the
            // OnBackStackChanged callback
            fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                  @Override
                  public void onBackStackChanged() {
                        // If the stack decreases it means I clicked the back button
                        if (fragmentManager.getBackStackEntryCount() <= count){
                              // pop all the fragment and remove the listener
                              fragmentManager.popBackStack(FRAGMENT_OTHER, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                              fragmentManager.removeOnBackStackChangedListener(this);
                              // set the home button selected
                              navigationView.getMenu().getItem(0).setChecked(true);
                        }
                  }
            });
      }

      @Override
      public void onBackPressed() {
//            int selectedItem = navigationView.getSelectedItemId();
//            if (R.id.navigation_home != selectedItem) {
//                  setHomeItem(HomePage.this);
//                  Log.e(TAG, "onBackPressed: " );
//            }
//            else
//                  super.onBackPressed();
         super.onBackPressed();
      }
}
