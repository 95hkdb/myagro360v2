package com.hensongeodata.myagro360v2.structurenew.bottomfragment;

import android.app.ProgressDialog;
import android.content.IntentFilter;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.Interface.NetworkResponse;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.LearningAdapterRV;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.MyState;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.KnowledgeBase;
import com.hensongeodata.myagro360v2.model.SettingsModel;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.view.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class LearnHomeFragment extends Fragment implements NetworkResponse {

      private static String TAG = LearnHomeFragment.class.getSimpleName();
      View learnView;
      public static String ACTION_SELECT="item selection action";
      public static String ACTION_DOWNLOAD="download action";
      private LearningAdapterRV adapter;
      private IntentFilter filter;
      public static List<KnowledgeBase> knowledgeBases;
      MyApplication myApplication;
       DatabaseHelper databaseHelper;
       User user;
       MyState myState;
       SettingsModel settings;
       NetworkRequest networkRequest;
       ProgressDialog progressDialog;
       BaseActivity baseActivity;
      private RecyclerView recyclerView;
      private TextView tvPlaceholder;
      private FloatingActionButton fabAction;

      public LearnHomeFragment() {
            // Required empty public constructor
      }

      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            learnView = inflater.inflate(R.layout.fragment_learn, container, false);

            init();
            return learnView;
      }


      private void init() {

            recyclerView = learnView.findViewById(R.id.frag_recyclerview);
            tvPlaceholder = learnView.findViewById(R.id.frag_placeholder);
            fabAction = learnView.findViewById(R.id.frag_fab_action);

            databaseHelper = new DatabaseHelper(getActivity());
            settings = new SettingsModel(getActivity());
            baseActivity = new BaseActivity();
            MyApplication.context = getActivity();
            myApplication = new MyApplication();
            networkRequest = new NetworkRequest(getActivity());
            progressDialog = new ProgressDialog(getActivity());

            showProgress();

            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));

            fabAction.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        if(databaseHelper.dropTable(DatabaseHelper.TBL_KB))
                              databaseHelper.createTable(DatabaseHelper.TBL_KB,null);

                        if(!loadList())
                              fetchList();
                  }
            });

            // settings.setLang(SettingsModel.LAN_FR);

            if(!loadList())
                  fetchList();

      }

      private void showProgress() {
            progressDialog.show();
            progressDialog.setMessage(resolveString(R.string.loading));
            progressDialog.setCancelable(true);
      }


      @Override
      public void onRespond(Object object) {

            Log.d(TAG, "JoinCommunityResponse " + object);
            if (object != null || (object instanceof String && ((String) object).equalsIgnoreCase("error"))) {
                  try {
                        JSONObject jsonObject = new JSONObject("" + object);
                        int status = jsonObject.optInt("success");
                        Log.d(TAG, "status: " + status);
                        if (status == 1) {
                              JSONArray jsonArray = jsonObject.optJSONArray("topics");
                              JSONObject item;
                              if (jsonArray != null) {
                                    for (int a = 0; a < jsonArray.length(); a++) {
                                          item = jsonArray.optJSONObject(a);
                                          Log.d(TAG, "item audio: " + item.optString("audio"));
                                          Log.d(TAG, "item media: " + item.optString("media_url"));
                                          if (item != null) {
                                                KnowledgeBase knowledgeBase = new KnowledgeBase(
                                                        a,
                                                        item.optString("title"),
                                                        item.optString("content"),
                                                        item.optString("media_url"),
                                                        item.optString("audio"),
                                                        item.optString("media_type"),
                                                        settings.getLang()
                                                );

                                                databaseHelper.addKB(knowledgeBase);
                                          }
                                    }
                              }
                        } else {
                              myApplication.showMessage(getActivity(), resolveString(R.string.no_content));
                        }
                  } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "json exception: " + e);
                  }
            } else {
                  myApplication.showMessage(getActivity(), resolveString(R.string.no_response));
            }
            progressDialog.dismiss();
            loadList();

      }

      private void fetchList() {
            if (myApplication.hasNetworkConnection(getActivity())) {
                  progressDialog.setMessage(resolveString(R.string.loading));
                  networkRequest.fetchKnowledgeBase(this);
            } else {
                  progressDialog.dismiss();
                  myApplication.showInternetError(getActivity());
            }
      }

      private boolean loadList() {
            knowledgeBases = databaseHelper.getKBs(settings.getLang());
            Log.d(TAG, "size: " + knowledgeBases.size());
            if (knowledgeBases == null || knowledgeBases.size() < 1) {
                  recyclerView.setVisibility(View.GONE);
                  progressDialog.show();
                  tvPlaceholder.setVisibility(View.VISIBLE);
                  fabAction.show();
                  return false;
            } else {
                  tvPlaceholder.setVisibility(View.GONE);
                  recyclerView.setVisibility(View.VISIBLE);
                  progressDialog.dismiss();
                  adapter = new LearningAdapterRV(getActivity(), knowledgeBases);
                  recyclerView.setAdapter(adapter);
                  fabAction.show();
                  Log.d(TAG, "audio: " + knowledgeBases.get(0).getAudio());
                  return true;
            }
      }

      private String resolveString(int id) {
            return getResources().getString(id);
      }

}
