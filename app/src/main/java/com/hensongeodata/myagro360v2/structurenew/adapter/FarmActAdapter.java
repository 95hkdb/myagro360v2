package com.hensongeodata.myagro360v2.structurenew.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.structurenew.model.FarmAct;
import com.hensongeodata.myagro360v2.view.LearnContent;

import java.util.List;

public class FarmActAdapter extends RecyclerView.Adapter<FarmActAdapter.MyViewHolder> {

      private Context context;
      private List<FarmAct> farmActList;

      public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView name;
            public ImageView thumbnail;

            public MyViewHolder(View view) {
                  super(view);
                  name = view.findViewById(R.id.subtitle);
                  thumbnail = view.findViewById(R.id.thumbnail);
            }
      }


      public FarmActAdapter(Context context, List<FarmAct> farmActList) {
            this.context = context;
            this.farmActList = farmActList;
      }

      @Override
      public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.sample_gridlayout, parent, false);

            return new MyViewHolder(itemView);
      }

      @Override
      public void onBindViewHolder(MyViewHolder holder, final int position) {
            final FarmAct farmAct = farmActList.get(position);
            holder.name.setText(farmAct.getTitle());

            Glide.with(context)
                    .load(farmAct.getImage())
                    .into(holder.thumbnail);

            holder.thumbnail.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {

                  }
            });
      }

      //      use point to navigate to scout and mapping as well as scan
      private void showActivity(int position, String point) {
            Intent intent = new Intent(context, LearnContent.class);
            intent.putExtra("kb_id", position);
            context.startActivity(intent);
      }

      @Override
      public int getItemCount() {
            return farmActList.size();
      }
}
