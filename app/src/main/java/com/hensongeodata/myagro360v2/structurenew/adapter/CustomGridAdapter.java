package com.hensongeodata.myagro360v2.structurenew.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.R;

public class CustomGridAdapter extends BaseAdapter {

      String[] title;
      int[] images;
      Context mCtx;
      private static LayoutInflater inflater=null;

      public CustomGridAdapter(Context mCtx, String[] title, int[] images) {
            // TODO Auto-generated constructor stub
            this.title = title;
            this.images = images;
            this.mCtx = mCtx;

            inflater = ( LayoutInflater )mCtx.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      }

      public class Holder
      {
            TextView os_text;
            ImageView os_img;
      }

      @Override
      public int getCount() {
            return title.length;
      }

      @Override
      public Object getItem(int position) {
            return position;
      }

      @Override
      public long getItemId(int position) {
            return 0;
      }

      @Override
      public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            Holder holder=new Holder();
            View rowView;

            rowView = inflater.inflate(R.layout.sample_gridlayout, null);
//            holder.os_text =(TextView) rowView.findViewById(R.id.os_texts);
//            holder.os_img =(ImageView) rowView.findViewById(R.id.os_images);

            holder.os_text.setText(title[position]);
            holder.os_img.setImageDrawable(mCtx.getResources().getDrawable(images[position]));
//            Picasso.get().load(images[position]).into(holder.os_img);

            rowView.setOnClickListener(new View.OnClickListener() {

                  @Override
                  public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Toast.makeText(mCtx, "You Clicked "+title[position], Toast.LENGTH_SHORT).show();
                  }
            });

            return rowView;
      }
}
