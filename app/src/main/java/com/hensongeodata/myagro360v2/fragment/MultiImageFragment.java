package com.hensongeodata.myagro360v2.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.MultiMediaAdapterRV;
import com.hensongeodata.myagro360v2.model.FileField;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.ImageMulti;
import com.hensongeodata.myagro360v2.view.MyCamera;
import com.hensongeodata.myagro360v2.view.CameraPhoto;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import id.zelory.compressor.Compressor;

/**
 * Created by user1 on 9/12/2017.
 */

public class MultiImageFragment extends MultiMediaFragment {
    private static String TAG=MultiImageFragment.class.getSimpleName();
    private Intent cameraIntent;
    private Image image;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       super.onCreateView(inflater,container,savedInstanceState);

        MyApplication.imageMultiArrayList.add(new ImageMulti());
        globalMediaIndex =(MyApplication.imageMultiArrayList.size()-1);

        adapter=new MultiMediaAdapterRV(getActivity(),MyApplication.imageMultiArrayList.get(globalMediaIndex).getImages());
       adapter.setMediaType(FileField.FILE_IMAGE);
       adapter.setGlobalMediaIndex(globalMediaIndex);

       recyclerView.setAdapter(adapter);

       cameraIntent=new Intent(getActivity(), CameraPhoto.class);
       cameraIntent.putExtra("index",globalMediaIndex);
        Log.d(TAG,"globalIndex: "+globalMediaIndex);

        fabCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
            }
        });

        if(MyCamera.autoStart)
            openCamera();

        MyCamera.autoStart=false;

        return baseView;
    }

    private void selectImage() {
        ImagePicker.create(this)
                .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Take farm image") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .toolbarArrowColor(Color.WHITE) // Toolbar 'up' arrow color
                .single() // single mode
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .theme(R.style.AppTheme) // must inherit ef_BaseTheme. please refer to sample
                .enableLog(false) // disabling log // custom image loader, must be serializeable
                .start();
    }

    private void addImage(Image image){
        MyApplication.imageMultiArrayList.get(globalMediaIndex).addImage(image);
        notifyDataChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            onSelectImage(ImagePicker.getFirstImageOrNull(data));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void onSelectImage(com.esafirm.imagepicker.model.Image image_lib) {

        String compressedPath=compressImage(image_lib.getPath());
        //String compressedPath=image_lib.getPath();
        Log.d(TAG,"path: "+compressedPath);
        image=new Image();
        image.setImage_url(compressedPath);
        image.setUri(Uri.parse(compressedPath));
        addImage(image);
    }

    private String compressImage(String path){
        String mPath = null;
        if(path!=null) {
            File file = new File(path);
            File compressedImageFile = null;
            try {
                if (file != null)
                    compressedImageFile = new Compressor(getActivity()).compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "compressed file: " + compressedImageFile);

            if (compressedImageFile != null) {
                mPath = compressedImageFile.getPath();
            }

            if(mPath==null||mPath.trim().isEmpty())
                mPath=path;
        }

        return mPath;
    }

    public ArrayList<Image> getAllUri(){
        return MyApplication.imageMultiArrayList.get(globalMediaIndex).getImages();
    }

    @Override
    public void onResume() {
        super.onResume();
      //  notifyDataChanged();
    }

    public void notifyDataChanged(){
        adapter.notifyDataSetChanged();
    }

    private void openCamera(){
        //MyCamera.action=MyCamera.ACTION_PHOTO;
       // getActivity().startActivity(cameraIntent);

        selectImage();
    }
}
