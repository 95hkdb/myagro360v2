package com.hensongeodata.myagro360v2.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;


/**
 * Created by user1 on 8/16/2017.
 */



public class BaseFragmentSimple extends Fragment {
    private static String TAG=BaseFragmentSimple.class.getSimpleName();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    protected String resolveString(int string_id){
        return getActivity().getResources().getString(string_id);
    }
}
