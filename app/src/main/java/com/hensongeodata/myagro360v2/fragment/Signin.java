package com.hensongeodata.myagro360v2.fragment;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.google.android.material.textfield.TextInputEditText;
import com.hensongeodata.myagro360v2.Interface.AuthListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SettingsModel;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.service.WeatherService;
import com.hensongeodata.myagro360v2.view.MainActivity;
import com.hensongeodata.myagro360v2.view.MainHomePageActivity;
import com.hensongeodata.myagro360v2.view.RecoverPasswordWeb;
import com.hensongeodata.myagro360v2.view.VideoPlayerPreview;

import me.pushy.sdk.Pushy;

/**
 * Created by user1 on 10/14/2017.
 */
public class Signin extends BaseFragmentSimple {
      private static String TAG = Signin.class.getSimpleName();
      View rootView;
      private MyApplication myApplication;
      private ProgressDialog progressDialog;
      private IntentFilter filter;
      private SigninBroadcastReceiver receiver;

      TextInputEditText etEmail;
      TextInputEditText etPassword;
      TextInputEditText etOrgId;
      Button btnSignin;

      NetworkRequest networkRequest;
      AuthListener authListener = new AuthListener() {
            @Override
            public void onAuthenticate(int status, int officer) {
                  if (status == NetworkRequest.STATUS_SUCCESS) {
                        if (progressDialog.isShowing())
                              progressDialog.cancel();
                        Log.d(TAG, "officer: " + officer);
                        if (officer != -1) {
//                              Intent i = new Intent(getActivity(), Menu.class);
                              Intent i = new Intent(getActivity(), MainHomePageActivity.class);
//                              Intent i = new Intent(getActivity(), LoadUserActivity.class);
                              i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

//                              DBSyncUtil.initialize(getContext());
//                              UpdatePlotSizesSyncUtil.initialize(getContext());

                              (new User(getActivity())).setOfficer(officer == 1);
                              Toast.makeText(getActivity(), resolveString(R.string.success), Toast.LENGTH_SHORT).show();
                              (new SettingsModel(getActivity())).setArchive_mode(false);
                              startActivity(i);
                              getActivity().finish();
                        } else {
                              myApplication.showMessage(getActivity(), resolveString(R.string.failed));
                              myApplication.logout(getActivity());
                        }
                  } else {
                        myApplication.logout(getActivity());
                        myApplication.showMessage(getActivity(), resolveString(R.string.failed));
                        if (progressDialog.isShowing())
                              progressDialog.cancel();
                  }
            }
      };

      private boolean isEmailValid(String email) {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
      }

      @Nullable
      @Override
      public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.signin, container, false);
            networkRequest = new NetworkRequest(getActivity());
            networkRequest.setAuthListener(authListener);
            myApplication = new MyApplication();


            if (myApplication.hasSession(getActivity())) {
                  getActivity().startService(new Intent(getActivity(), WeatherService.class));
//                  Intent intent = new Intent(getActivity(), Menu.class);
                  Intent intent = new Intent(getActivity(), MainHomePageActivity.class);
                  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                  startActivity(intent);
                  networkRequest.close();
                  getActivity().finish();
            } else {
                  receiver = new SigninBroadcastReceiver();
                  filter = new IntentFilter(BroadcastCall.SIGN_IN);
                  getActivity().registerReceiver(receiver, filter);
                  progressDialog = new ProgressDialog(getActivity());
                  progressDialog.setCancelable(false);

                  etEmail = rootView.findViewById(R.id.et_email);
                  etPassword = rootView.findViewById(R.id.et_password);
                  etOrgId = rootView.findViewById(R.id.et_org_id);

                  btnSignin = rootView.findViewById(R.id.btn_signin);
                  btnSignin.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                              etEmail.setError(null);
                              etPassword.setError(null);
                              etOrgId.setError(null);
                              String email = etEmail.getText().toString().trim();
                              String password = etPassword.getText().toString().trim();
                              String orgid = etOrgId.getText().toString();

                              if (TextUtils.isEmpty(email)) {
                                    etEmail.setError(resolveString(R.string.error_field_required));
                                    etEmail.requestFocus();
                                    return;
                              }
                              if (!isEmailValid(email)) {
                                    etEmail.setError(resolveString(R.string.error_invalid_email));
                                    etEmail.requestFocus();
                                    return;
                              }

                              if (TextUtils.isEmpty(password)) {
                                    etPassword.setError(resolveString(R.string.error_field_required));
                                    etPassword.requestFocus();
                                    return;
                              }

                              if (orgid.isEmpty()) {
                                    orgid = "AG1010";
                              }

                              Log.e(TAG, "my org id is: " + orgid);

                              User user = new User(null);
                              user.setEmail(email);
                              user.setPassword(password);
                              doSignin(user, orgid);
                        }
                  });

                  (rootView.findViewById(R.id.tv_forgot_password)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                              startActivity(new Intent(getActivity(), RecoverPasswordWeb.class));
                        }
                  });

            }

            rootView.findViewById(R.id.v_tutorial).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), VideoPlayerPreview.class);
                        intent.putExtra("path", Keys.VIDEO_URL_IGEZA);
                        intent.putExtra("caption", getActivity().getResources().getString(R.string.watch_tutorial));
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
                        // getActivity().startActivity(intent);
                  }
            });


            // rootView.findViewById(R.id.bg_spinner).setBackgroundResource(R.drawable.search_bg_dark);

            return rootView;
      }


      public void doAuthentication(User user) {

            if (myApplication.hasNetworkConnection(getActivity())) {
                  progressDialog.setMessage("Authenticating.Please wait...");
                  //progressDialog.show();
                  //networkRequest.registerPush(user);
                  authListener.onAuthenticate(NetworkRequest.STATUS_SUCCESS, 1);
            } else {
                  myApplication.showInternetError(getActivity());
            }
      }

      class SigninBroadcastReceiver extends BroadcastReceiver {

            @Override
            public void onReceive(Context context, Intent intent) {

                  String message = intent.getExtras().getString("date");
                  int status = intent.getExtras().getInt("status");


                  String notificationTitle = "MyApp";
                  String notificationText = "Test notification";

                  // Attempt to extract the "message" property from the payload: {"message":"Hello World!"}
                  if (intent.getStringExtra("message") != null) {
                        notificationText = intent.getStringExtra("message");
                  }

                  // Prepare a notification with vibration, sound and lights
                  NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                          .setAutoCancel(true)
                          .setSmallIcon(android.R.drawable.ic_dialog_info)
                          .setContentTitle(notificationTitle)
                          .setContentText(notificationText)
                          .setLights(Color.RED, 1000, 1000)
                          .setVibrate(new long[]{0, 400, 250, 400})
                          .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                          .setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));

                  // Automatically configure a Notification Channel for devices running Android O+
                  Pushy.setNotificationChannel(builder, context);

                  // Get an instance of the NotificationManager service
                  NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

                  // Build the notification and display it
//                  notificationManager.notify(1, builder.build());

                  if (status == NetworkRequest.STATUS_SUCCESS) {
                        if (myApplication.hasSession(getActivity())) {
                              doAuthentication(new User(getActivity()));
                        } else {
                              if (progressDialog.isShowing())
                                    progressDialog.cancel();

                              myApplication.showMessage(getActivity(), "No session created. Try again");
                        }
                  } else if (status == NetworkRequest.STATUS_FAIL) {
                        if (message == null || message.trim().isEmpty())
                              myApplication.showMessage(getActivity(), "No response from server. Try again");
                        else
                              myApplication.showMessage(getActivity(), message);

                        if (progressDialog.isShowing())
                              progressDialog.cancel();

                  }
            }
      }

      public void doSignin(User user, String orgid) {

            if (myApplication.hasNetworkConnection(getActivity())) {
                  networkRequest.setAuthListener(authListener);
                  progressDialog.setMessage(resolveString(R.string.progress_signin));
                  progressDialog.show();
                  networkRequest.signin(user, orgid);
            } else {
                  myApplication.showInternetError(getActivity());
            }
      }

      @Override
      public void onDestroy() {
            super.onDestroy();
            if (receiver != null)
                  getActivity().unregisterReceiver(receiver);
            if (networkRequest != null)
                  networkRequest.close();
      }
}