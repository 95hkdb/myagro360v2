package com.hensongeodata.myagro360v2.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SettingsModel;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.view.Chat;
import com.hensongeodata.myagro360v2.view.District;
import com.hensongeodata.myagro360v2.view.MenuDashboard;
import com.hensongeodata.myagro360v2.view.PrivacyPolicy;
import com.hensongeodata.myagro360v2.view.VideoPlayerPreview;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 10/14/2017.
 */

 public class ProfileUpdate extends BaseFragmentSimple {
    private static String TAG=ProfileUpdate.class.getSimpleName();
    View rootView;
    private MyApplication myApplication;
    private ProgressDialog progressDialog;
    private IntentFilter filter;
    private UpdateBroadcastReceiver receiver;
    private String prompt;
    private List<String> types;
    private Spinner spType;

    TextInputEditText etFirstname;
    TextInputEditText etLastname;
    TextInputEditText etEmail;
    TextInputEditText etPassword;
    TextInputEditText etPasswordConfirm;
    TextInputEditText etPhone;
    TextInputEditText etTown;
    TextInputEditText etOrg;
    Button btnSignup;
    CheckBox cbPrivacy;
    TextView tvError;
    String type;
    User mUser;
    SettingsModel settingsModel;

    NetworkRequest networkRequest;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.signup, container, false);
        mUser=new User(getActivity());
        settingsModel=new SettingsModel(getActivity());
        networkRequest=new NetworkRequest(getActivity());

        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage(resolveString(R.string.updating_wait));
        progressDialog.setCancelable(false);

        myApplication=new MyApplication();
        receiver=new UpdateBroadcastReceiver();
        filter=new IntentFilter(BroadcastCall.UPDATE_PROFILE);
        getActivity().registerReceiver(receiver,filter);

          etFirstname = rootView.findViewById(R.id.et_firstname);
        etFirstname.setText(mUser.getFirstname());
          etLastname = rootView.findViewById(R.id.et_lastname);
        etLastname.setText(mUser.getLastname());
          etEmail = rootView.findViewById(R.id.et_email);
        etEmail.setText(mUser.getEmail());
          etPassword = rootView.findViewById(R.id.et_password);
        etPassword.setVisibility(View.GONE);
          etPasswordConfirm = rootView.findViewById(R.id.et_password_confirm);
        etPasswordConfirm.setVisibility(View.GONE);
          etPhone = rootView.findViewById(R.id.et_phone);
        etPhone.setText(mUser.getPhone());
//          etTown = rootView.findViewById(R.id.et_town);
//        etTown.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onAddMapButtonClicked(View view) {
//              chooseTown();
//            }
//        });
//        etTown.setText(settingsModel.getTown());
//          etOrg = rootView.findViewById(R.id.et_organization);
//        etOrg.setText(mUser.getOrganization());


        rootView.findViewById(R.id.v_tutorial).setVisibility(View.GONE);
        rootView.findViewById(R.id.v_password).setVisibility(View.GONE);
        rootView.findViewById(R.id.v_password_confirm).setVisibility(View.GONE);
        rootView.findViewById(R.id.bg_privacy_policy).setVisibility(View.GONE);

          cbPrivacy = rootView.findViewById(R.id.cb_privacy);
          tvError = rootView.findViewById(R.id.tv_error);

        cbPrivacy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    tvError.setVisibility(View.GONE);
            }
        });

          btnSignup = rootView.findViewById(R.id.btn_signup);
        btnSignup.setText(resolveString(R.string.update_profile));

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"doupdateprofile");
                String firstname = etFirstname.getText().toString().trim();
                String lastname = etLastname.getText().toString().trim();
                String email = etEmail.getText().toString().trim();
                String phone=etPhone.getText().toString().trim();
//                String town=etTown.getText().toString().trim();

                if (TextUtils.isEmpty(firstname)) {
                    etFirstname.setError(resolveString(R.string.error_field_required));
                    etFirstname.requestFocus();
                    return;
                }

                if (!isNameValid(firstname)) {
                    etFirstname.setError(resolveString(R.string.first_name));
                    etFirstname.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(lastname)) {
                    etLastname.setError(resolveString(R.string.error_field_required));
                    etLastname.requestFocus();
                    return;
                }
                if (!isNameValid(lastname)) {
                    etFirstname.setError("Enter a valid Last Name.");
                    etFirstname.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(email)) {
                    etEmail.setError(resolveString(R.string.error_field_required));
                    etEmail.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(etOrg.getText().toString().trim())) {
                    etOrg.setError("Enter your organization name here.");
                    etOrg.requestFocus();
                    return;
                }
                if (!isEmailValid(email)) {
                    etEmail.setError("Enter a valid Email Address.");
                    etEmail.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(phone)) {
                    etPhone.setError(resolveString(R.string.error_field_required));
                    etPhone.requestFocus();
                    return;
                }
                if (!isPhoneValid(phone)||phone.length()<10) {
                    etPhone.setError("Enter a valid Phone Number.");
                    etPhone.requestFocus();
                    return;
                }

                if (type==null||type.isEmpty()||type.equalsIgnoreCase(prompt)) {
//                    MyApplication.showToast(getActivity(),
//                            resolveString(R.string.profession)+", "+resolveString(R.string.error_field_required),
//                            Gravity.CENTER);
//                    spType.requestFocus();
                   // return;

                    type=mUser.getType();
                }

//                if (TextUtils.isEmpty(etTown.getText().toString())) {
//                    MyApplication.showToast(getActivity(),
//                            resolveString(R.string.town)+", "+getActivity().getResources().getString(R.string.error_field_required),
//                            Gravity.CENTER);
//                    etTown.setError(resolveString(R.string.error_field_required));
//                    etTown.requestFocus();
//                    return;
//                }
                User user=new User(null);//null implies data wont be saved to sharedprefs
                user.setFirstname(firstname);
                user.setLastname(lastname);
                user.setEmail(email);
                user.setPhone(phone);
                user.setType(mUser.getType());
                user.setOrganization(etOrg.getText().toString().trim());
                user.setId(mUser.getId());
                user.setPassword(mUser.getPassword());

                Log.d(TAG,"onclick b4 update");
                    doUpdateProfile(user);
            }

        });

        rootView.findViewById(R.id.bg_privacy_policy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                etTown.setError(null);
                startActivity(new Intent(getActivity(),PrivacyPolicy.class));
            }
        });


          spType = rootView.findViewById(R.id.spinner_type);
      spType.setVisibility(View.GONE);
        rootView.findViewById(R.id.bg_spinner_type).setBackgroundResource(R.drawable.search_bg_dark);
        prompt=getActivity().getResources().getString(R.string.user_type);
        loadTypes();
        setSpinner(spType,types);
        spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //Log.d(TAG,"position: "+i);
                if(i!=types.size()-1)
                      type = spType.getItemAtPosition(i).toString().toLowerCase();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        rootView.findViewById(R.id.v_tutorial).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), VideoPlayerPreview.class);
                intent.putExtra("path", Keys.VIDEO_URL_IGEZA);
                intent.putExtra("caption",getActivity().getResources().getString(R.string.watch_tutorial));
                getActivity().startActivity(intent);
            }
        });

        return rootView;
    }

    private boolean isNameValid(String name) {
        return name.matches( "[a-zA-z]+([ '-][a-zA-Z]+)*" );
    }

    private boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isPhoneValid(String phone) {
        return PhoneNumberUtils.isGlobalPhoneNumber(phone);
    }

    public void doUpdateProfile(User user){
        Log.d(TAG,"doupdateprofile");
        if(myApplication.hasNetworkConnection(getActivity())){
            progressDialog.show();
            networkRequest.updateProfile(user);
        }
        else {
            if(progressDialog.isShowing())
                progressDialog.hide();

            myApplication.showInternetError(getActivity());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== Activity.RESULT_OK){
            if(requestCode==100){//Town name requested
//                etTown.setText(data.getExtras().getString("town_name"));
            }
        }
    }


    class UpdateBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            String message=intent.getExtras().getString("date");
            int status=intent.getExtras().getInt("status");

            Intent i=new Intent(getActivity(), MenuDashboard.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                if(status==NetworkRequest.STATUS_SUCCESS){
                    if(myApplication.hasSession(getActivity())){
                        (new SettingsModel(getActivity())).setArchive_mode(false);

                        User user=new User(getActivity());
                        user.setChatroomActive_id(null);
                        user.setProfession_id(null);
                        String profession_id=Chat.getChatroom_id(getActivity(),type);
                        user.setProfession_id(profession_id);
                        startActivity(i);
                        getActivity().finish();
                    }
                    else{
                        if(progressDialog.isShowing())
                            progressDialog.cancel();

                        myApplication.showMessage(getActivity(),"No session created. Try again");
                    }

                    }
                else if(status==NetworkRequest.STATUS_FAIL){
                    if(message==null||message.trim().isEmpty())
                    myApplication.showMessage(getActivity(),"No response from server. Try again");
                else
                    myApplication.showMessage(getActivity(),message);

                    if(progressDialog.isShowing())
                        progressDialog.cancel();
                }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(receiver!=null)
            getActivity().unregisterReceiver(receiver);
        if(networkRequest!=null)
        networkRequest.close();
    }

    private void loadTypes(){
        types=new ArrayList<>();
        types.add(getActivity().getResources().getString(R.string.coordinator));
        types.add(getActivity().getResources().getString(R.string.farmer));
        types.add(getActivity().getResources().getString(R.string.seed_dealer));
        types.add(getActivity().getResources().getString(R.string.fertilizer_dealer));
        types.add(getActivity().getResources().getString(R.string.fbo));
        types.add(getActivity().getResources().getString(R.string.insurance));
        types.add(getActivity().getResources().getString(R.string.funder));
        types.add(getActivity().getResources().getString(R.string.agronomist));
        types.add(getActivity().getResources().getString(R.string.soil_scientist));
        types.add(getActivity().getResources().getString(R.string.pathologist));
        types.add(getActivity().getResources().getString(R.string.entomologist));
        types.add(getActivity().getResources().getString(R.string.food_scientist));
        types.add(getActivity().getResources().getString(R.string.post_harvest_tech));
        types.add(getActivity().getResources().getString(R.string.nematologist));
        types.add(getActivity().getResources().getString(R.string.wholesaler));
        types.add(getActivity().getResources().getString(R.string.retailer));
        types.add(getActivity().getResources().getString(R.string.cabi));
        types.add(getActivity().getResources().getString(R.string.gavex));
        types.add(getActivity().getResources().getString(R.string.mofa));
        types.add(getActivity().getResources().getString(R.string.tech_support));

        types.add(prompt);
    }

    private int getTypePosition(String type){
        Log.d(TAG,"type: "+type);
//        if(type!=null){
//            if(type.equalsIgnoreCase(resolveString(R.string.extension_officer)))
//                return 0;
//            else if(type.equalsIgnoreCase(resolveString(R.string.agro_chemist)))
//                return 1;
//            else if(type.equalsIgnoreCase(resolveString(R.string.entomologist)))
//                return 2;
//            else if(type.equalsIgnoreCase(resolveString(R.string.farmer)))
//                return 3;
//            else if(type.equalsIgnoreCase(resolveString(R.string.input_dealer)))
//                return 4;
//            else if(type.equalsIgnoreCase(resolveString(R.string.nppo)))
//                return 5;
//            else if(type.equalsIgnoreCase(resolveString(R.string.surveyor)))
//                return 6;
//        }
        return -1;
    }

    private void chooseTown(){
        startActivity(new Intent(getActivity(), District.class));
    }
    private void setSpinner(Spinner spinner, List<String> list){
        if(list!=null) {
            final int listsize = list.size() - 1;
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list) {
                @Override
                public int getCount() {
                    return (listsize); // Truncate the list
                }
            };

            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapter);
            spinner.setPrompt(prompt);
            spinner.setFocusable(true);
            spinner.setSelection(listsize);
            spinner.setEnabled(true);

            Log.d(TAG,"type: "+mUser.getType());
            if(mUser.getType()!=null){
                Log.d(TAG,"mUser: "+getTypePosition(mUser.getType()));
                spinner.setSelection(getTypePosition(mUser.getType()));
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        etTown.setText(settingsModel.getTown());
    }

}