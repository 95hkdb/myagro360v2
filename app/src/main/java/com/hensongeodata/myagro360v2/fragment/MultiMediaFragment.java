package com.hensongeodata.myagro360v2.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.MultiMediaAdapterRV;

/**
 * Created by user1 on 9/12/2017.
 */

public class MultiMediaFragment extends MediaFragment {
    private static String TAG=MultiMediaFragment.class.getSimpleName();
    protected View baseView;

    protected FloatingActionButton fabCapture;
    protected RecyclerView recyclerView;
    protected TextView tvCaption;

    protected MultiMediaAdapterRV adapter;

    protected int globalMediaIndex;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle=this.getArguments();

        Log.d(TAG,"onCreate");
    }

    public int getGlobalMediaIndex() {
        return globalMediaIndex;
    }

    //    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        baseView = inflater.inflate(R.layout.frag_multiple_media, container, false);

        fabCapture= (FloatingActionButton) findView(fabCapture,R.id.fab_capture);
        recyclerView= (RecyclerView) findView(recyclerView,R.id.recyclerview);

        return baseView;
    }

    private View findView(View v,int id){
        v=baseView.findViewById(id);
        return v;
    }
}
