package com.hensongeodata.myagro360v2.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.SettingsModel;
import com.hensongeodata.myagro360v2.view.District;
import com.hensongeodata.myagro360v2.view.Town;


/**
 * Created by user1 on 8/16/2017.
 */



public class TownFragment extends BaseFragment {
    private static String TAG= TownFragment.class.getSimpleName();
    private View rootView;
    private String town;
    private EditText etTown;
    private SettingsModel settingsModel;
    private ImageButton ibTown;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         rootView =inflater.inflate(R.layout.frag_town, container, false);
        settingsModel=new SettingsModel(getActivity());

        etTown=rootView.findViewById(R.id.et_answer);
        etTown.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        ibTown=rootView.findViewById(R.id.ib_location);
        etTown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTown();
            }
        });
        ibTown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadTownSet();
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loadTownSet(){
        startActivity(new Intent(getActivity(), District.class));
    }

    private void changeTown(){
        startActivity(new Intent(getActivity(), Town.class));
    }

    @Override
    public void onResume() {
        super.onResume();
        if(District.town!=null&&!District.town.trim().isEmpty())
                settingsModel.setTown(District.town);

        etTown.setText(settingsModel.getTown());
    }

    public String getTown() {
        return etTown.getText().toString().trim();
    }

    public void setTown(String town) {
        this.town = town;
    }
}
