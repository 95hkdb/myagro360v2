package com.hensongeodata.myagro360v2.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.Utility;
import com.hensongeodata.myagro360v2.model.Video;
import com.hensongeodata.myagro360v2.view.CameraVideo;
import com.hensongeodata.myagro360v2.view.MyCamera;
import com.hensongeodata.myagro360v2.view.VideoPlayer;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenVideo;
import com.kbeanie.imagechooser.api.ChosenVideos;
import com.kbeanie.imagechooser.api.VideoChooserListener;
import com.kbeanie.imagechooser.api.VideoChooserManager;
import com.kbeanie.imagechooser.exceptions.ChooserException;

/**
 * Created by user1 on 9/12/2017.
 */

public class VideoFragment extends MediaFragment{
    private static String TAG=VideoFragment.class.getSimpleName();
    View rootView;
    private static int REQUEST_CAMERA=100;
    private static int SELECT_FILE=200;
    private static int PREVIEW_VIDEO=300;
    private static int DURATION=30;
    private String userChoosenTask;
    public Video videoModel;
    //public static String SPLITTER="#"; //spliiter joins the video path and previewImagePath Like this videoPath#previewImagePath

    private VideoChooserManager videoChooserManager;
    private Intent cameraIntent;
    static Uri uri;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        videoModel =new Video();
        MyApplication.videoList.add(videoModel);
        globalMediaIndex =(MyApplication.videoList.size()-1);

        cameraIntent=new Intent(getActivity(), CameraVideo.class);
        cameraIntent.putExtra("index",globalMediaIndex);
        Log.d(TAG,"globalIndex: "+globalMediaIndex);

        ibAction.setImageResource(R.drawable.ic_play_dark);
        ibAction.setOnClickListener(previewListener);

        fabCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectVideo();
            }
        });
        fabCapture.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.ic_video_white));

        imageView.setOnClickListener(previewListener);

        if(answer!=null&&!answer.trim().isEmpty()) {
            videoModel.getImage().setUri(Uri.parse(answer));
            setImage(videoModel.getUri());

            toggleView(ACTION_SHOW_ALL);
        }
        else {
            toggleView(ACTION_HIDE_ALL);
        }

        if(MyCamera.autoStart)
            openCamera();

        MyCamera.autoStart=false;

        return baseView;
    }

    private View.OnClickListener previewListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(videoModel!=null&&videoModel.getUri()!=null) {
                Intent intent = new Intent(getActivity(), VideoPlayer.class);
                intent.putExtra("video_path", videoModel.getUri().getPath());
                startActivityForResult(intent, PREVIEW_VIDEO);
            }
            else
                Toast.makeText(getActivity(),"Video uri not found.",Toast.LENGTH_SHORT).show();
        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK &&
                (requestCode == ChooserType.REQUEST_PICK_VIDEO ||
                        requestCode == ChooserType.REQUEST_CAPTURE_VIDEO)) {

            toggleView(ACTION_SHOW_PROGRESS);
            videoChooserManager.submit(requestCode, data);
        }
        else if(resultCode==Activity.RESULT_OK&&requestCode==PREVIEW_VIDEO){
            if(data.getExtras().getBoolean("delete")){
                deleteVideo();
            }
        }
        else if(resultCode==Activity.RESULT_OK&&requestCode==REQUEST_CAMERA){
            Log.d(TAG,"request cam feedback uri");
            if(uri!=null){
                videoModel.setUri(uri);
                setImage(videoModel.getUri());
            }
//
//            if(path!=null&&!path.trim().isEmpty()){
//                videoModel.setUri(Uri.parse(path));
//                setImage(videoModel.getUri());
//            }
        }
    }

    public String getPath(){
        if(videoModel!=null&&videoModel.getUri()!=null)
            return videoModel.getUri().getPath();

        return null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Record Video"))
                        openCamera();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                    Toast.makeText(getActivity(),"Permission required to continue.",Toast.LENGTH_SHORT).show();
                }
                break;
            default: return;
        }
    }


    public void deleteVideo(){
        toggleView(ACTION_HIDE_ALL);
        videoModel=new Video();
        if(videoModel !=null)
            videoModel.setUri(null);

        if(imageView!=null) {
            imageView.setVisibility(View.GONE);
            imageView.setImageBitmap(null);
        }

        uri=null;

        answer=null;
    }

    private void selectVideo() {
        userChoosenTask="cancel";
        final CharSequence[] items = { "Record Video", "Choose from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Video!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= Utility.checkPermission(getActivity());
                if (items[item].equals("Record Video")) {
                    userChoosenTask="Record Video";
                    if(result)
                        openCamera();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask="Choose from Library";
                    if(result)
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    userChoosenTask="Cancel";
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void openCamera(){
//        MyCamera.action=MyCamera.ACTION_VIDEO;
//        getActivity(). startActivityForResult(cameraIntent, REQUEST_CAMERA);;

        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        uri = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, DURATION);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    VideoChooserListener videoChooserListener=new VideoChooserListener() {
        @Override
        public void onVideoChosen(final ChosenVideo chosenVideo) {
            getActivity().runOnUiThread(new Runnable() {
               @Override
                    public void run() {
                    if (chosenVideo != null) {
                            videoModel.setUri(Uri.parse(chosenVideo.getVideoFilePath()));
                            Log.d(TAG,"path: "+chosenVideo.getVideoFilePath());
                            onVideoSelected();
                    }
                    else {
                        Toast.makeText(getActivity(),"Something went wrong. Try again.",Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }

        @Override
        public void onError(String s) {
            final String message=s;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onVideosChosen(ChosenVideos chosenVideos) {

        }
    };

    private void galleryIntent() {
        videoChooserManager = new VideoChooserManager(this, ChooserType.REQUEST_PICK_VIDEO);
        videoChooserManager.setVideoChooserListener(videoChooserListener);
        try {
            videoChooserManager.choose();
        } catch (ChooserException e) {
            e.printStackTrace();
        }
    }

    private void onVideoSelected() {
                Log.d(TAG,"uri: "+videoModel.getUri());
                setImage(videoModel.getUri());
                Log.d(TAG,"paths: "+answer);
                toggleView(ACTION_SHOW_ALL);

    }

    public Bitmap createThumbnailFromPath(String filePath){
        Log.d(TAG,"filePath: "+filePath+" thumbnail:"+ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Images.Thumbnails.MICRO_KIND));
        return  BitmapFactory.decodeFile(filePath);
    }

    private void setImage(Uri uri){
        if(uri!=null){
            Bitmap bitmap = null;
            try {
                bitmap = CameraVideo.retriveVideoFrameFromVideo(uri.getPath());
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(getActivity(),"Can't load image.",Toast.LENGTH_SHORT).show();
            }
            if (bitmap != null) {
                bitmap = Bitmap.createScaledBitmap(bitmap, 800, 800, false);
                imageView.setImageBitmap(bitmap);
                toggleView(ACTION_SHOW_ALL);
            }
        }
            //Picasso.with(getActivity()).load(ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, getVideoId(uri))).resize(800, 800).centerCrop().into(imageView);
    }
}
