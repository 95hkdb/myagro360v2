package com.hensongeodata.myagro360v2.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import com.hensongeodata.myagro360v2.R;

/**
 * Created by user1 on 9/12/2017.
 */

public class DateFragment extends BaseFragment {
    private static String TAG=DateFragment.class.getSimpleName();
    View rootView;
    EditText etDate;
    private String answer;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle=this.getArguments();
        if(bundle!=null) {
            answer = bundle.getString("answer", null);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag_date, container, false);

          etDate = rootView.findViewById(R.id.et_answer);
        etDate.setFocusable(false);
        etDate.setHint(getActivity().getResources().getString(R.string.prompt_date));
        if(answer!=null)
            etDate.setText(answer);

        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               datePicker();
            }
        });
        (rootView.findViewById(R.id.background)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker();
            }
        });

        (rootView.findViewById(R.id.ib_calendar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker();
            }
        });


        return rootView;
    }

    private void setEtDate(String date){
        etDate.setText(date);
    }

    public String getDate(){
        if(etDate!=null)
        return etDate.getText().toString().trim();
        else
            return null;
    }

    private void datePicker(){
        final Calendar calendar=Calendar.getInstance();
        final SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        final DatePickerDialog datePickerDialog=new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate=Calendar.getInstance();
                newDate.set(year,monthOfYear,dayOfMonth);
                setEtDate(convertDate(dateFormat.format(newDate.getTime())));

            }
        },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

}
