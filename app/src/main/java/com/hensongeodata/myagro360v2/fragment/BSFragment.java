package com.hensongeodata.myagro360v2.fragment;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import com.hensongeodata.myagro360v2.Interface.OnStartDragListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.POIAdapterRV;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.model.POIGroup;
import com.hensongeodata.myagro360v2.model.SettingsModel;
import com.hensongeodata.myagro360v2.view.BaseActivity;

/**
 * Created by user1 on 9/12/2017.
 */

public class BSFragment extends BaseFragment implements OnStartDragListener {
    private static String TAG=BSFragment.class.getSimpleName();
    public static String ACTION_DIMENSION="dimension";
    public static String ACTION_LOCATION="location";
    private static String ACTION_ADD_POI="addpoi";
    public static String ACTION_ADD_POI_GROUP="addgroup";
    public static String DEAFAULT_GROUP_NAME="Default Group";
    public static int MAX_POI_CHARS =16;
    public static int MAX_POI_GROUP_CHARS=32;

    View rootView;
    View bgAccuracy;
    TextView bsAccuracy;
    TextView bsLongitude;
    TextView bsLatitude;
    TextView bsAltitude;

    TextView tvPerimeter;
    TextView tvArea;


    private BSFragmentReceiver receiver;
    private IntentFilter filter;

    private BottomSheetBehavior mBottomSheetBehavior;
    private ImageButton ibBSHandle;
    private boolean bsOpened=false;
    private ArrayList<POIGroup> poiGroupList;
    private static RecyclerView recyclerView;
    private static POIAdapterRV adapter;
    private POI poi;
    private static POI cachedPOI;
    private TextView tvError;
    private boolean error=false;

    private CheckBox cbGroup;
    private TextView tvGroup;
    private View bgGroup;

    private ItemTouchHelper mItemTouchHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.bottomsheet_item, container, false);

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseActivity.startLocationOffline(getActivity());
            }
        });
        receiver=new BSFragmentReceiver();
        filter=new IntentFilter(BroadcastCall.BSFRAGMENT);
        getActivity().registerReceiver(receiver,filter);

          recyclerView = rootView.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        adapter=new POIAdapterRV(getActivity(),this,MyApplication.poiGroup.getPoiList());
        recyclerView.setAdapter(adapter);

        initPOIBootomSheetView(MyApplication.poi);
        myApplication.selectPOIGroup(getActivity(),databaseHelper);

        mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        mItemTouchHelper.attachToRecyclerView(recyclerView);

        (rootView.findViewById(R.id.ib_addpoint)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // MyApplication.poi=MyApplication.getFakePOI();
                POI poi=MyApplication.poi;
                if(poi.getAccuracy()!=null) {
                    if(!existsPOI(poi)) {
                        int maxAccuracy = (new SettingsModel(getActivity())).getAccuracy_max();
                        if (Integer.parseInt(poi.getAccuracy()) <= maxAccuracy) {
                            poi = new POI(null, poi.getLon(), poi.getLat(), poi.getAlt(), poi.getAccuracy());
                            showPop(poi, ACTION_ADD_POI);
                        } else
                            Toast.makeText(getActivity(), "Please ensure your accuracy is less or equal to " + maxAccuracy, Toast.LENGTH_SHORT).show();
                    }
                    else
                        Toast.makeText(getActivity(), "Way point instance already saved.", Toast.LENGTH_SHORT).show();
                }
                    else {
                    Toast.makeText(getActivity(),"No Way point available.",Toast.LENGTH_SHORT).show();
                }
            }
        });


        return rootView;
    }

    public void addPoi(POI poi,String poiName){
        poi.setName(poiName);

        adapter.addItem(poi);
        recyclerView.scrollToPosition(adapter.getItemCount()-1);
        cachedPOI=poi;
        BroadcastCall.publishLocationUpdate(getActivity(),NetworkRequest.STATUS_SUCCESS,ACTION_DIMENSION);

        Toast.makeText(getActivity(),"Way point saved.",Toast.LENGTH_SHORT).show();

    }


    public static void addInstantPoi(Context context,POI poi,String poiName){
        poi.setName(poiName);

        //adapter.addMessage(poi);
        //recyclerView.scrollToPosition(adapter.getItemCount()-1);
        cachedPOI=poi;
        BroadcastCall.publishLocationUpdate(context,NetworkRequest.STATUS_SUCCESS,ACTION_DIMENSION);
    }

    private boolean existsPOI(POI poi){
        boolean exist=false;
        if(MyApplication.poiGroup.getPoiList()!=null
                &&MyApplication.poiGroup.getPoiList().size()==0){
            cachedPOI=null;
            return false;
        }

        if(cachedPOI!=null){
            if(cachedPOI.getAccuracy().equalsIgnoreCase(poi.getAccuracy())
                    &&cachedPOI.getLat().equalsIgnoreCase(poi.getLat())
                    &&cachedPOI.getLon().equalsIgnoreCase(poi.getLon())
                    &&cachedPOI.getAlt().equalsIgnoreCase(poi.getAlt()))
                exist=true;
        }
        else exist=false;

        return exist;
    }

    private void addPoiGroup(POIGroup poiGroup, String groupName){
        poiGroup.setName(groupName);
        poiGroup.setId(databaseHelper.createTransactionID());
        poiGroup.setSelect(true);

        databaseHelper.savePOIGroup(poiGroup);
        Log.d(TAG,"new group_name: "+poiGroup.getName());
          MyApplication.poiGroup = databaseHelper.getPOIGroup();
          Log.d(TAG, "glob group_name: " + MyApplication.poiGroup.getName());
        updateGroup();
        Toast.makeText(getActivity(),"New Group saved.",Toast.LENGTH_SHORT).show();

    }

    private void showPop(final Object object, final String action){

        poiGroupList=databaseHelper.getPOIGroupList();
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.confirm_dialog);//here, the confirm delete layout is used to modify way point (poi) names
        ((TextView)dialog.findViewById(R.id.tv_caption)).setText(action.equals(ACTION_ADD_POI)?"Add new way point":"Create way point group");
          final EditText etName = dialog.findViewById(R.id.et_password);
        etName.setHint("Enter Name...");
        etName.addTextChangedListener(new MyPOIWatcher(action));
          tvError = dialog.findViewById(R.id.tv_message);
          Button btnCancel = dialog.findViewById(R.id.btn_negative);
          Button btnSave = dialog.findViewById(R.id.btn_positive);

        btnCancel.setText("Cancel");
        btnCancel.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_grey));
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btnSave.setText("Save");
        btnSave.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_green));
        btnSave.setTextColor(getResources().getColor(R.color.primaryWhite));
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            String name=etName.getText().toString().trim();
                if(!name.isEmpty()) {
                    if (error)//if there's an error, don't continue to save
                        return;

                    if (action != null) {
                        if (action.equalsIgnoreCase(ACTION_ADD_POI))
                            addPoi((POI) object, name);
                        else if (action.equalsIgnoreCase(ACTION_ADD_POI_GROUP))
                            addPoiGroup((POIGroup) object, name);
                    }
                }

                dialog.dismiss();
            }
        });

        dialog.show();

        InputMethodManager imm = (InputMethodManager)   getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }


    class MyPOIWatcher implements TextWatcher{
        String action;
        public MyPOIWatcher(String action){
            this.action=action;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            String name=charSequence.toString();
            Log.d(TAG,"action: "+action);
            if(action!=null&&tvError!=null){
                if(action.equalsIgnoreCase(ACTION_ADD_POI)){
                        for (int a = 0; a < MyApplication.poiGroup.getPoiList().size(); a++) {
                            if (MyApplication.poiGroup.getPoiList().get(a).getName()!=null
                                    &&MyApplication.poiGroup.getPoiList().get(a).getName().equalsIgnoreCase(name)) {
                                error = true;
                                tvError.setText("Way point name already exist.");
                                tvError.setVisibility(View.VISIBLE);
                                return;
                            }
                        }

                        if (name.length() >= MAX_POI_CHARS) {
                            error = true;
                            tvError.setText("Way point name should be max " + MAX_POI_CHARS + " characters.");
                            tvError.setVisibility(View.VISIBLE);
                            return;
                        }
                }
                if(action.equalsIgnoreCase(ACTION_ADD_POI_GROUP)){
                    for (int a = 0; a < poiGroupList.size(); a++) {
                        if (poiGroupList.get(a).getName()!=null
                                &&poiGroupList.get(a).getName().equalsIgnoreCase(name)) {
                            error = true;
                            tvError.setText("Group name already exist.");
                            tvError.setVisibility(View.VISIBLE);
                            return;
                        }
                    }

                    if (name.length() >= MAX_POI_GROUP_CHARS) {
                        error = true;
                        tvError.setText("Group name should be max " + MAX_POI_GROUP_CHARS + " characters.");
                        tvError.setVisibility(View.VISIBLE);
                        return;
                    }
                }


                tvError.setVisibility(View.GONE);
            }

            error=false;
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, ItemTouchHelper.UP | ItemTouchHelper.DOWN) {

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            Collections.swap(adapter.getList(), viewHolder.getAdapterPosition(), target.getAdapterPosition());
            adapter.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
            adapter.notifyDataSetChanged();
            BroadcastCall.publishLocationUpdate(getActivity(),NetworkRequest.STATUS_SUCCESS, BSFragment.ACTION_DIMENSION);
            return true;
        }

/*
        @Override
        public boolean isLongPressDragEnabled() {
            return true;
        }
        @Override
        public boolean isItemViewSwipeEnabled() {
            return true;
        }
        @Override
        public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return super.getSwipeDirs(recyclerView, viewHolder);
        }*/

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
          /*  adapter.getList().remove(viewHolder.getAdapterPosition());
            adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
        */
        }
    };


    private void initPOIBootomSheetView(POI poi){

          cbGroup = rootView.findViewById(R.id.cb_poi_group);
        cbGroup.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                MyApplication.poiGroup.setSelect(b);
                databaseHelper.updatePOIGroup(MyApplication.poiGroup);
                updateGroup();
            }
        });
          tvGroup = rootView.findViewById(R.id.tv_poi_group);
        bgGroup=rootView.findViewById(R.id.bg_poi_group);
        bgGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                  showPop(MyApplication.poiGroup, ACTION_ADD_POI_GROUP);
            }
        });
        tvGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bgGroup.performClick();
            }
        });

        updateGroup();//setValues to group checkbox and label

          bsAccuracy = (rootView.findViewById(R.id.bs_accuracy)).findViewById(R.id.tv_value);
        ((TextView)(rootView.findViewById(R.id.bs_accuracy)).findViewById(R.id.tv_tag)).setText("ACCURACY");
        bsAccuracy.setTextColor(ContextCompat.getColor(getActivity(),R.color.primaryWhite));
        ((TextView)(rootView.findViewById(R.id.bs_accuracy)).findViewById(R.id.tv_tag))
                .setTextColor(ContextCompat.getColor(getActivity(),R.color.secondaryWhite));

          bsLongitude = (rootView.findViewById(R.id.bs_longitude)).findViewById(R.id.tv_value);
        ((TextView)(rootView.findViewById(R.id.bs_longitude)).findViewById(R.id.tv_tag)).setText("LONGITUDE");
        bsLongitude.setTextColor(ContextCompat.getColor(getActivity(),R.color.primaryWhite));
        ((TextView)(rootView.findViewById(R.id.bs_longitude)).findViewById(R.id.tv_tag))
                .setTextColor(ContextCompat.getColor(getActivity(),R.color.secondaryWhite));

          bsLatitude = (rootView.findViewById(R.id.bs_latitude)).findViewById(R.id.tv_value);
        ((TextView)(rootView.findViewById(R.id.bs_latitude)).findViewById(R.id.tv_tag)).setText("LATITUDE");
        bsLatitude.setTextColor(ContextCompat.getColor(getActivity(),R.color.primaryWhite));
        ((TextView)(rootView.findViewById(R.id.bs_latitude)).findViewById(R.id.tv_tag))
                .setTextColor(ContextCompat.getColor(getActivity(),R.color.secondaryWhite));

          bsAltitude = (rootView.findViewById(R.id.bs_altitude)).findViewById(R.id.tv_value);
        ((TextView)(rootView.findViewById(R.id.bs_altitude)).findViewById(R.id.tv_tag)).setText("ALTITUDE");
        bsAltitude.setTextColor(ContextCompat.getColor(getActivity(),R.color.primaryWhite));
        ((TextView)(rootView.findViewById(R.id.bs_altitude)).findViewById(R.id.tv_tag))
                .setTextColor(ContextCompat.getColor(getActivity(),R.color.secondaryWhite));

          tvPerimeter = (rootView.findViewById(R.id.bs_distance)).findViewById(R.id.tv_value);
        ((TextView)(rootView.findViewById(R.id.bs_distance)).findViewById(R.id.tv_tag)).setText("DIST.");
        tvPerimeter.setTextColor(ContextCompat.getColor(getActivity(),R.color.primaryWhite));
        ((TextView)(rootView.findViewById(R.id.bs_distance)).findViewById(R.id.tv_tag))
                .setTextColor(ContextCompat.getColor(getActivity(),R.color.secondaryWhite));

          tvArea = (rootView.findViewById(R.id.bs_area)).findViewById(R.id.tv_value);
        ((TextView)(rootView.findViewById(R.id.bs_area)).findViewById(R.id.tv_tag)).setText("AREA");
        tvArea.setTextColor(ContextCompat.getColor(getActivity(),R.color.primaryWhite));
        ((TextView)(rootView.findViewById(R.id.bs_area)).findViewById(R.id.tv_tag))
                .setTextColor(ContextCompat.getColor(getActivity(),R.color.secondaryWhite));

        setBottomSheetPOIValues(poi);//demo purposes
    }

    private void updateGroup(){
          cbGroup.setChecked(MyApplication.poiGroup.isSelect());
          tvGroup.setText(MyApplication.poiGroup.getName());
        Log.d(TAG,"updated poiList: "+MyApplication.poiGroup.getPoiList());
        adapter=new POIAdapterRV(getActivity(),this,MyApplication.poiGroup.getPoiList());
        recyclerView.setAdapter(adapter);

        BroadcastCall.publishLocationUpdate(getActivity(),NetworkRequest.STATUS_SUCCESS,ACTION_DIMENSION);
    }

    private void setBottomSheetPOIValues(POI poi){
        Log.d(TAG,"BS lon: "+poi.getLon()+" lat: "+poi.getLat()+" alt: "+poi.getAlt());
        String accuracy=poi.getAccuracy();
        if(accuracy!=null&&!accuracy.trim().equalsIgnoreCase("null")) {
            bsAccuracy.setText(accuracy+"m");
            bsAccuracy.setTextColor((new BaseActivity()).getAccuracyColor(getActivity(),Integer.parseInt(accuracy)));

            bsLongitude.setText(poi.getLon());
            bsLatitude.setText(poi.getLat());
            bsAltitude.setText(poi.getAlt()+"m");
        }
    }

    class BSFragmentReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
                String action = intent.getExtras().getString("action");
                     int status = intent.getExtras().getInt("status");
            Log.d(TAG,"action: "+action);
            if(action.equals(ACTION_LOCATION)) {
                if (status == NetworkRequest.STATUS_SUCCESS) {
                    setBottomSheetPOIValues(MyApplication.poi);
                }
            }
            else if(action.equals(ACTION_DIMENSION)) {
                tvPerimeter.setText(MyApplication.getPerimeter(MyApplication.poiGroup.getPoiList())+" m");
                tvArea.setText(superscript(MyApplication.getArea(MyApplication.poiGroup.getPoiList())+""));
            }
            else if(action.equals(ACTION_ADD_POI_GROUP)) {
                updateGroup();
            }
        }
    }

    @SuppressWarnings("deprecation")
    private Spanned superscript(String text){
        String superscript = " m<sup><small>2</small></sup>";
        // Spanned formatted=Html.fromHtml(asteriks);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text+superscript, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text+superscript);
        }
        // return formatted;
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
        Log.d(TAG,"notifydatasetchanged");
        //adapter=new POIAdapterRV(getActivity(),MyApplication.poiList);
        //recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(receiver!=null)
                getActivity().unregisterReceiver(receiver);
    }

}
