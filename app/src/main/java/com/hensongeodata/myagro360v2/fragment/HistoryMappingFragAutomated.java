package com.hensongeodata.myagro360v2.fragment;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.hensongeodata.myagro360v2.Interface.NetworkResponse;
import com.hensongeodata.myagro360v2.Interface.TaskListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.view.MainActivityAutomated;
import com.hensongeodata.myagro360v2.view.MappingActivity;
import com.hensongeodata.myagro360v2.view.Scout;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by user1 on 8/16/2017.
 */



public class HistoryMappingFragAutomated extends BaseFragment implements TaskListener,NetworkResponse{
    private static String TAG= HistoryMappingFragAutomated.class.getSimpleName();

    private WebView webView;
    private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    private String section_id;
    private MapModel mapModel;
    private View rootView=null;
    private ArrayList<InstructionScout> itemsVisible;
    private int unprocessed=0;
    private ArrayList<String>messages=new ArrayList<>();
    private boolean upload_complete=false;
    private View btnRetry;
    //private boolean usingMeters=false;
    private int usingHaArea=100;
    private int usingAcreArea=600;
    private int usingKmArea=200;
    private int usingMArea=300;
    private int usingMPerimeter=400;
    private int usingKmPerimeter=500;
    private int usingArea=usingAcreArea;
    private int usingPerimeter=usingMPerimeter;
    private ImageButton ibReloadMap;
    ArrayList<String> strPois;
    ArrayList<POI>pois;
    private Button btnAddForm;
    private boolean automaticCapture=true;

    public HistoryMappingFragAutomated newInstance(int position) {
        HistoryMappingFragAutomated f = new HistoryMappingFragAutomated();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        //Log.d(TAG,"instance: "+position );
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        section_id = getArguments() != null ? getArguments().getString("section_id"):null;
        setRetainInstance(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"onResume");
        //MainActivityAutomated.clearMappingReference();
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         rootView = inflater.inflate(R.layout.history_mapping_frag, container, false);

         mapModel=new MapModel();
          progressBar = rootView.findViewById(R.id.progressBar);

        btnRetry=rootView.findViewById(R.id.bg_retry);
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
        progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if(!upload_complete)
                        getActivity().finish();
            }
        });

        ibReloadMap=((MappingActivity)getActivity()).ibTopLeft;
        if(ibReloadMap!=null){
            ibReloadMap.setImageResource(R.drawable.ic_retry_white);
            ibReloadMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadMap();
                }
            });
        }



        MyApplication.modeScout=ChatbotAdapterRV.MODE_MAP;
        itemsVisible=databaseHelper.getScoutList(section_id, ChatbotAdapterRV.MODE_MAP);
        mapModel=databaseHelper.getMapping(section_id);

        //mapModel=;

        if(itemsVisible!=null) {
            Log.d(TAG,"items size: "+itemsVisible.size());
            int index =0;
            for (InstructionScout instructionScout : itemsVisible) {
                String message = instructionScout.getMessage();
                Log.d(TAG,"date: "+message);
                if (message != null && !message.trim().isEmpty()) {
                    messages.add(message);
                }
                instructionScout.setStatus(InstructionScout.STATE_PROCCESSED);
                itemsVisible.set(index, instructionScout);

                index++;
            }
        }

        List<InstructionScout> getAllItems=databaseHelper.getScoutListAllStates(section_id, ChatbotAdapterRV.MODE_MAP);
        strPois =new ArrayList<>();

         int index2 =0;
            for (InstructionScout instructionScout : getAllItems) {
                if(instructionScout!=null) {
                    String message = instructionScout.getMessage();
                    if (message != null && !message.trim().isEmpty())
                        strPois.add(message);
                }
                index2++;
            }

          pois=CreateForm.getInstance(getActivity()).buildPOI_Mapping(strPois);

         int area=MyApplication.getArea(pois);
        ((TextView)rootView.findViewById(R.id.tv_area)).setText(isPolygon()?""+metersToAcres(area):"0");

        long perimeter=MyApplication.getPerimeter(pois);
        ((TextView)rootView.findViewById(R.id.tv_perimeter)).setText(""+perimeter);


        submitMapping();

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitMapping();
            }
        });


        (rootView.findViewById(R.id.v_area)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             swapAreaMetric();
            }
        });

//        if(mapModel!=null&&mapModel.type!=MapModel.TYPE_POLYGON)
//                  ((TextView) rootView.findViewById(R.id.tv_area)).setVisibility(View.GONE);
//        else
//            ((TextView) rootView.findViewById(R.id.tv_area)).setVisibility(View.VISIBLE);

        (rootView.findViewById(R.id.v_area)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if(mapModel!=null&&mapModel.type==MapModel.TYPE_POLYGON)
                  swapAreaMetric();
            }
        });

        (rootView.findViewById(R.id.v_perimeter)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swapPerimeterMetric();
            }
        });

        (rootView.findViewById(R.id.btn_add_form)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openForms();
            }
        });

        return rootView;
    }


    private void openForms(){
        Log.d(TAG,"openForms scout_id: "+section_id);
        MainActivityAutomated.mapModel=databaseHelper.getMapping(section_id);
        Log.d(TAG,"openforms map.id: "+MainActivityAutomated.mapModel.id+" map.type: "+MainActivityAutomated.mapModel.type);
        MainActivityAutomated.pois=pois;
        Intent intent=new Intent(getActivity(),MainActivityAutomated.class);
        getActivity().startActivity(intent);
    }


    private void swapAreaMetric(){
        if(usingArea==usingAcreArea){
            usingArea=usingHaArea;
        }
        else if(usingArea==usingHaArea){
            usingArea=usingKmArea;
        }
        else if(usingArea==usingKmArea){
            usingArea=usingMArea;
        }
        else
            usingArea=usingAcreArea;

        if(strPois !=null) {
            int area = isPolygon()?MyApplication.getArea(CreateForm.getInstance(getActivity()).buildPOI_Mapping(strPois)):0;
            if(area!=0) {
                if (usingArea==usingMArea) {
                    ((TextView) rootView.findViewById(R.id.tv_area)).setText("" + area);
                    ((TextView) rootView.findViewById(R.id.tag_area)).setText(getActivity().getResources().getString(R.string.area_meter));
                } else if(usingArea==usingAcreArea){
                    ((TextView) rootView.findViewById(R.id.tv_area)).setText("" + metersToAcres(area));
                    ((TextView) rootView.findViewById(R.id.tag_area)).setText(getActivity().getResources().getString(R.string.area));
                }else if(usingArea==usingKmArea){
                    ((TextView) rootView.findViewById(R.id.tv_area)).setText("" + metersToKilometers(area));
                    ((TextView) rootView.findViewById(R.id.tag_area)).setText(getActivity().getResources().getString(R.string.area_km2));
                }else if(usingArea==usingHaArea){
                    ((TextView) rootView.findViewById(R.id.tv_area)).setText("" + metersToHectares(area));
                    ((TextView) rootView.findViewById(R.id.tag_area)).setText(getActivity().getResources().getString(R.string.area_hectares));
                }
            }
        }

    }

    private boolean isPolygon(){
        return mapModel!=null&&mapModel.type!=null&&mapModel.type.equalsIgnoreCase(MapModel.TYPE_POLYGON);
    }

    private void swapPerimeterMetric(){
        if(usingPerimeter==usingMPerimeter){
            usingPerimeter=usingKmPerimeter;
        }
        else if(usingArea==usingKmPerimeter){
            usingPerimeter=usingMPerimeter;
        }
        else
            usingPerimeter=usingMPerimeter;

        if(strPois !=null) {
            long perimeter = MyApplication.getPerimeter(CreateForm.getInstance(getActivity()).buildPOI_Mapping(strPois));
            if(perimeter!=0) {
                if (usingPerimeter==usingMPerimeter) {
                    ((TextView) rootView.findViewById(R.id.tv_perimeter)).setText("" + perimeter);
                    ((TextView) rootView.findViewById(R.id.tag_perimeter)).setText(getActivity().getResources().getString(R.string.perimeter));
                } else if(usingPerimeter==usingKmPerimeter){
                    ((TextView) rootView.findViewById(R.id.tv_perimeter)).setText("" + metersToKilo(perimeter));
                    ((TextView) rootView.findViewById(R.id.tag_perimeter)).setText(getActivity().getResources().getString(R.string.perimeter_km));
                }
            }
        }

    }

    private void process(){
            Log.d(TAG,"msg size: "+messages.size());
            if (messages.size() > 0) {
                upload_complete=false;
                progressDialog.show();
                CreateForm createForm=new CreateForm(getActivity());
                btnRetry.setVisibility(View.GONE);
                if(myApplication.hasNetworkConnection(getActivity())) {
//                        networkRequest.sendFAWMappingBulk(createForm.buildJSON_Mapping(createForm.buildPOI_Mapping(messages)),
//                                section_id, mapModel.type, new NetworkResponse() {
//                                    @Override
//                                    public void onRespond(Object object) {
//                                        databaseHelper.addScoutItems(itemsVisible,ChatbotAdapterRV.MODE_MAP);
//                                        loadMap();
//                                    }
//                                },
//                                "No Name", 0);
                }
                else {
                    upload_complete=true;
                    progressDialog.hide();
                    myApplication.showInternetError(getActivity());
                    btnRetry.setVisibility(View.VISIBLE);
                }

            }
        else{
            loadMap();
        }
    }

    private double metersToAcres(long meterSquared){
        return Math.round((meterSquared*0.00024711) * 1000.0) / 1000.0;
       // return (long) Math.round();
    }

    private double metersToHectares(long mettersSquared){
        return mettersSquared*0.0001;
    }

    private double metersToKilometers(long metersSquared){
        return metersSquared*0.000001;
    }

    private double metersToKilo(long metersSquared){
        return metersSquared*0.001;
    }

    private void loadMap(){
        btnRetry.setVisibility(View.GONE);
        upload_complete=true;
        if(progressDialog.isShowing())
            progressDialog.hide();

        messages=new ArrayList<>();

        databaseHelper.addScoutItems(itemsVisible, ChatbotAdapterRV.MODE_MAP);
        progressBar.setVisibility(View.VISIBLE);
          webView = rootView.findViewById(R.id.webview);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);


        webView.loadUrl(Keys.AGRO_BASE+"faw-api/get-map/?mapping_id="+section_id);
        Log.e(TAG, "Section id: " + section_id);
        webView.setWebViewClient(new WebViewClient() {


            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
            }

            @TargetApi(Build.VERSION_CODES.LOLLIPOP)


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.getUrl().toString());
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                Log.d(TAG,"error");
                btnRetry.setVisibility(View.VISIBLE);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                Log.d(TAG,"ssl error");
               // btnRetry.setVisibility(View.VISIBLE);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                Log.d(TAG,"http error");
                //btnRetry.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onTaskAccepted(String id) {

    }

    @Override
    public void onTaskStarted(String id) {

    }

    @Override
    public void onTaskStopped(String id, boolean complete) {
        Log.d(TAG,"task complete "+complete);
        if(complete){
            InstructionScout instructionScout=databaseHelper.getInstructScout(id,ChatbotAdapterRV.MODE_SCOUT);
            if(instructionScout!=null){
                int index= Scout.getIndex(itemsVisible,instructionScout);
                if(index>=0){
                    unprocessed--;
                    updateUI();
                }

                   // adapter.updateItem(index,instructionScout);
            }
        }
    }

    private void updateUI(){
        if(unprocessed>0){
            //some points are yet to show
        }
        else {
            //every point is showing perfectly
        }
    }

    @Override
    public void onTaskCancelled(String id) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MyApplication.shutdownLive();
        MainActivityAutomated.clearMappingReference();
    }


    //Show textbox to edit username
    private void showPop(String caption){

        if(getActivity()!=null) {
            final Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.map_save_menu);

            // set the custom dialog components - text, image and button
              TextView tvCaption = dialog.findViewById(R.id.tv_caption);
            tvCaption.setText(caption);
            dialog.findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    submitMapping();
                    dialog.cancel();
                }
            });

//            dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    btnRetry.setVisibility(View.VISIBLE);
//                    progressBar.setVisibility(View.GONE);
//                    dialog.cancel();
//                }
//            });

            dialog.show();
        }
    }

    private void submitMapping(){
        Log.d(TAG,"messages size: "+messages.size());
        if (messages!=null&&messages.size() > 0) {
            if(myApplication.hasNetworkConnection(getActivity())) {
                progressDialog.show();
//                    networkRequest.sendFAWMappingBulk(createForm.buildJSON_Mapping(
//                            createForm.buildPOI_Mapping(messages)),
//                            section_id,mapModel.type,this, "No Name", 0);
            }
            else {
                progressDialog.hide();
                showPop("You don't have internet connectivity now. " +
                        "You can activate your internet and try again, or attempt save later");
            }
        } else {
            loadMap();
        }
    }

    @Override
    public void onRespond(Object object) {
                progressDialog.hide();
                Log.d(TAG,"object: "+object);
                if(object!=null&&getActivity()!=null) {
                    MyApplication.showToast(getActivity(),
                            "Map saved successfully",
                            Gravity.CENTER);
                    loadMap();
                }
                else {
                    showPop("Couldn't connect to server. Try again.");
                }
    }
}
