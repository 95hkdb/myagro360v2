package com.hensongeodata.myagro360v2.fragment;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.Html;
import android.text.InputType;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.MyRadio;
import com.hensongeodata.myagro360v2.controller.MyState;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.CheckboxField;
import com.hensongeodata.myagro360v2.model.DateField;
import com.hensongeodata.myagro360v2.model.DropdownField;
import com.hensongeodata.myagro360v2.model.EmailField;
import com.hensongeodata.myagro360v2.model.FieldFragPair;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.TownField;
import com.hensongeodata.myagro360v2.model.Validation;
import com.hensongeodata.myagro360v2.model.Field;
import com.hensongeodata.myagro360v2.model.FileField;
import com.hensongeodata.myagro360v2.model.NumericField;
import com.hensongeodata.myagro360v2.model.RadioField;
import com.hensongeodata.myagro360v2.model.Section;
import com.hensongeodata.myagro360v2.model.TextAreaField;
import com.hensongeodata.myagro360v2.model.TextField;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.view.FixedActivity;


/**
 * Created by user1 on 8/16/2017.
 */



public class BaseFragment extends Fragment {
    private static String TAG=BaseFragment.class.getSimpleName();
    protected CreateForm createForm;
    protected MyApplication myApplication;
    protected DatabaseHelper databaseHelper;
    protected User user;
    protected MyState myState;
    protected NetworkRequest networkRequest;
    private LayoutInflater inflater;

    private ArrayList<FieldFragPair> fieldFragListScan;//cache
    private ArrayList<FieldFragPair> fieldFragListImage;//cache
    private ArrayList<FieldFragPair> fieldFragListDate;//cache
    private ArrayList<FieldFragPair> fieldFragListVideo;
    private ArrayList<FieldFragPair> fieldFragListMultiImage;
    private ArrayList<FieldFragPair> fieldMapping;
    private ArrayList<FieldFragPair> fieldFragListTown;

    private Fragment currentFragmentScan;//image fragment being inflated during addField
    private Fragment currentFragmentImage;//image fragment being inflated during addField
    private Fragment currentFragmentDate;//date fragment being inflated during addField
    private Fragment currentFragmentVideo;
    private Fragment currentFragmentMultiImage;
    private Fragment currentFragmentMapping;
    private Fragment currentFragmentTown;

    private int fragIdImage =100;
    private int fragIdDate=1000;
    private int fragIdVideo=2000;
    private int fragIdMultiImage=3000;
    private int fragIdScan=4000;
    private int fragIdMapping=5000;
    private int fragIdTown=6000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        inflater = LayoutInflater.from(getActivity());

        createForm =CreateForm.getInstance(getActivity());
        myApplication=new MyApplication();
        databaseHelper=new DatabaseHelper(getActivity());
        user=new User(getActivity());
        myState=new MyState(getActivity());
        networkRequest=new NetworkRequest(getActivity());

        fieldFragListImage =new ArrayList<>();
        fieldFragListDate=new ArrayList<>();
        fieldFragListVideo=new ArrayList<>();
        fieldFragListMultiImage=new ArrayList<>();
        fieldFragListScan=new ArrayList<>();
        fieldMapping=new ArrayList<>();
        fieldFragListTown=new ArrayList<>();
    }


    protected int contains(List<Field> fields, final String field_id) {
        Field field=null;
        return Collections.binarySearch(fields, field, new Comparator<Field>() {

            @Override
            public int compare(Field o1, Field o2) {
                return o1.getId().compareTo(field_id);
            }
        });
    }

    protected static String convertDate(String date){
        String day=date.substring(0,date.indexOf("/"));
        String month=date.substring(date.indexOf("/")+1,date.lastIndexOf("/"));
        String year=date.substring(date.lastIndexOf("/")+1);

        String newMonth;
        if(month.equalsIgnoreCase("01")){
            newMonth="Jan";
        }
        else if(month.equalsIgnoreCase("02")){
            newMonth="Feb";
        }
        else if(month.equalsIgnoreCase("03")){
            newMonth="Mar";
        }
        else if(month.equalsIgnoreCase("04")){
            newMonth="Apr";
        }
        else if(month.equalsIgnoreCase("05")){
            newMonth="May";
        }
        else if(month.equalsIgnoreCase("06")){
            newMonth="Jun";
        }
        else if(month.equalsIgnoreCase("07")){
            newMonth="Jul";
        }
        else if(month.equalsIgnoreCase("08")){
            newMonth="Aug";
        }
        else if(month.equalsIgnoreCase("09")){
            newMonth="Sep";
        }
        else if(month.equalsIgnoreCase("10")){
            newMonth="Oct";
        }
        else if(month.equalsIgnoreCase("11")){
            newMonth="Nov";
        }
        else if(month.equalsIgnoreCase("12")){
            newMonth="Dec";
        }
        else{
            newMonth=month;
        }

        return day+" "+newMonth+", "+year;
    }

    protected void generateForm(LinearLayout parent,int sectionIndex){
        if(CreateForm.getInstance(getActivity()).sections!=null&&CreateForm.getInstance(getActivity()).sections.size()>sectionIndex) {
            ArrayList<Object> fields = CreateForm.getInstance(getActivity()).sections.get(sectionIndex);
            // Log.d(TAG,"index: "+sectionIndex+" fields: "+fields.size());
            for (int a = 0; a < fields.size(); a++) {
                Object fieldObject = fields.get(a);
                LinearLayout baseView;
                if (fieldObject instanceof Section) {
                    Section section = (Section) fieldObject;
                    baseView = (LinearLayout) inflater.inflate(R.layout.model_section, null);
                    ((TextView) baseView.findViewById(R.id.tv_title)).setText(section.getTitle());
                    if (section.getDesc() != null && !section.getDesc().trim().equalsIgnoreCase("description of section")
                            && !section.getDesc().trim().isEmpty())
                        ((TextView) baseView.findViewById(R.id.tv_description)).setText(section.getDesc());
                    else
                          baseView.findViewById(R.id.tv_description).setVisibility(View.GONE);
                } else {
                    baseView = (LinearLayout) inflater.inflate((a + 1 == fields.size()) ? R.layout.question_extended : R.layout.question, null);
                    baseView.setTag(((Field) fieldObject).getTag());

                    Field field = (Field) fieldObject;
                    Log.d(TAG, "question: " + field.getQuestion());
                    if (field.isRequired())
                        ((TextView) baseView.findViewById(R.id.tv_question)).setText(required(field.getQuestion()));
                    else
                        ((TextView) baseView.findViewById(R.id.tv_question)).setText(field.getQuestion());

                    if(FixedActivity.scannerActive){
                        //scanner form opened hence first option should be a scanner
                        FileField fileField=new FileField();
                        if(fieldObject instanceof TextField) {
                            fileField.setId(((TextField) fieldObject).getId());
                            fileField.setType(FileField.FILE_SCAN);
                            baseView = addFileField(baseView, fileField);
                        }

                        CreateForm.getInstance(getActivity()).sections.get(sectionIndex).set(a,fileField);
                        //replace original textfield with filefield(scanner type)

                        FixedActivity.scannerActive=false;
                    }
                    else if (fieldObject instanceof RadioField) {
                        baseView = addRadioField(baseView, (RadioField) fieldObject);
                    } else if (fieldObject instanceof DropdownField) {
                        baseView = addDropdownField(baseView, (DropdownField) fieldObject);
                    } else if (fieldObject instanceof CheckboxField) {
                        baseView = addCheckboxField(baseView, (CheckboxField) fieldObject);
                    } else if (fieldObject instanceof FileField) {
                        baseView = addFileField(baseView, (FileField) fieldObject);
                    } else if (fieldObject instanceof TextAreaField) {
                        baseView = addTextAreaField(baseView, (TextAreaField) fieldObject);
                    } else if (fieldObject instanceof EmailField) {
                        baseView = addEmailField(baseView, (EmailField) fieldObject);
                    } else if (fieldObject instanceof DateField) {
                        baseView = addDateField(baseView, (DateField) fieldObject);
                    } else if (fieldObject instanceof NumericField) {
                        baseView = addNumericField(baseView, (NumericField) fieldObject);
                    } else if (fieldObject instanceof TownField) {
                        Log.d(TAG,"adding town");
                        baseView = addTownField(baseView, (TownField) fieldObject);
                    }
                    else if (fieldObject instanceof TextField) {
                        Log.d(TAG,"adding text field");
                        baseView = addTextField(baseView, (TextField) fieldObject);
                    }
                }
                parent.addView(baseView);//add newly created baseView(Question field) to form

                if (currentFragmentScan != null) {
                    //means a fragment item is added
                    String fragTag = "fragment" + fragIdScan;
                    Field fragField = (Field) fieldObject;
                    String answer = null;
                    if (fragField.getAnswers().size() > 0)
                        answer = fragField.getAnswers().size() > 0 ? fragField.getAnswers().get(0) : "";//in case an answer exists
                    Bundle bundle = new Bundle();
                    bundle.putString("answer", answer);
                    bundle.putString("field_id", ((Field) fieldObject).getId());
                    currentFragmentScan.setArguments(bundle);
                    FragmentManager fm = getChildFragmentManager();
                    fm.beginTransaction().add(fragIdScan, currentFragmentScan, fragTag).commit();
                    fragIdScan++;

                    fieldFragListScan.add(new FieldFragPair(fragField.getId(), fragTag));
                    currentFragmentScan = null; //remove fragment from currentFragmentImage instance
                   }

                if (currentFragmentImage != null) {
                    //means a fragment item is added
                    String fragTag = "fragment" + fragIdImage;
                    Field fragField = (Field) fieldObject;
                    String answer = null;
                    if (fragField.getAnswers().size() > 0)
                        answer = fragField.getAnswers().size() > 0 ? fragField.getAnswers().get(0) : "";//in case an answer exists
                    Bundle bundle = new Bundle();
                    bundle.putString("answer", answer);
                    bundle.putString("field_id", ((Field) fieldObject).getId());
                    currentFragmentImage.setArguments(bundle);
                    FragmentManager fm = getChildFragmentManager();
                    fm.beginTransaction().add(fragIdImage, currentFragmentImage, fragTag).commit();
                    fragIdImage++;

                    fieldFragListImage.add(new FieldFragPair(fragField.getId(), fragTag));
                    currentFragmentImage = null; //remove fragment from currentFragmentImage instance
                    Log.d(TAG, fragTag + " | " + answer);
                }

                if (currentFragmentVideo != null) {
                    //means a fragment item is added
                    String fragTag = "fragment" + fragIdVideo;
                    Field fragField = (Field) fieldObject;
                    String answer = null;
                    if (fragField.getAnswers().size() > 0)
                        answer = fragField.getAnswers().size() > 0 ? fragField.getAnswers().get(0) : "";//in case an answer exists
                    Bundle bundle = new Bundle();
                    bundle.putString("answer", answer);
                    bundle.putString("field_id", ((Field) fieldObject).getId());
                    currentFragmentVideo.setArguments(bundle);
                    FragmentManager fm = getChildFragmentManager();
                    fm.beginTransaction().add(fragIdVideo, currentFragmentVideo, fragTag).commit();
                    fragIdVideo++;

                    fieldFragListVideo.add(new FieldFragPair(fragField.getId(), fragTag));
                    currentFragmentVideo = null; //remove fragment from currentFragmentImage instance
                    Log.d(TAG, fragTag + " | " + answer);
                }

                if (currentFragmentDate != null) {
                    //means a fragment item is added
                    String fragTag = "fragment" + fragIdDate;
                    Field fragField = (Field) fieldObject;
                    String answer = null;
                    if (fragField.getAnswers().size() > 0)
                        answer = fragField.getAnswers().size() > 0 ? fragField.getAnswers().get(0) : "";//in case an answer exists
                    Bundle bundle = new Bundle();
                    bundle.putString("answer", answer);
                    bundle.putString("field_id", ((Field) fieldObject).getId());
                    currentFragmentDate.setArguments(bundle);
                    FragmentManager fm = getChildFragmentManager();
                    fm.beginTransaction().add(fragIdDate, currentFragmentDate, fragTag).commit();
                    fragIdDate++;

                    fieldFragListDate.add(new FieldFragPair(fragField.getId(), fragTag));
                    currentFragmentDate = null; //remove fragment from currentFragmentDate instance
                    Log.d(TAG, fragTag + " | " + answer);
                }

                if (currentFragmentMultiImage != null) {
                    //means a fragment item is added
                    String fragTag = "fragment" + fragIdMultiImage;
                    Field fragField = (Field) fieldObject;
                    String answer = null;
                    if (fragField.getAnswers().size() > 0)
                        answer = fragField.getAnswers().size() > 0 ? fragField.getAnswers().get(0) : "";//in case an answer exists
                    Bundle bundle = new Bundle();
                    bundle.putString("answer", answer);
                    bundle.putString("field_id", ((Field) fieldObject).getId());
                    currentFragmentMultiImage.setArguments(bundle);
                    FragmentManager fm = getChildFragmentManager();
                    fm.beginTransaction().add(fragIdMultiImage, currentFragmentMultiImage, fragTag).commit();
                    fragIdMultiImage++;

                    fieldFragListMultiImage.add(new FieldFragPair(fragField.getId(), fragTag));
                    currentFragmentMultiImage = null; //remove fragment from currentFragmentDate instance
                    Log.d(TAG, fragTag + " | " + answer);
                }



                if (currentFragmentMapping != null) {
                    //means a fragment item is added
                    String fragTag = "fragment" + fragIdMapping;
                    Field fragField = (Field) fieldObject;
                    String answer = null;
                    if (fragField.getAnswers().size() > 0)
                        answer = fragField.getAnswers().size() > 0 ? fragField.getAnswers().get(0) : "";//in case an answer exists
                    Bundle bundle = new Bundle();
                    bundle.putString("answer", answer);
                    bundle.putString("field_id", ((Field) fieldObject).getId());
                    currentFragmentMapping.setArguments(bundle);
                    FragmentManager fm = getChildFragmentManager();
                    fm.beginTransaction().add(fragIdMapping, currentFragmentMapping, fragTag).commit();
                    fragIdMapping++;

                    fieldMapping.add(new FieldFragPair(fragField.getId(), fragTag));
                    currentFragmentMapping = null; //remove fragment from currentFragmentDate instance
                    Log.d(TAG, fragTag + " | " + answer);
                }


                if (currentFragmentTown != null) {
                    //means a fragment item is added
                    String fragTag = "fragment" + fragIdTown;
                    Field fragField = (Field) fieldObject;
                    String answer = null;
                    if (fragField.getAnswers().size() > 0)
                        answer = fragField.getAnswers().size() > 0 ? fragField.getAnswers().get(0) : "";//in case an answer exists
                    Bundle bundle = new Bundle();
                    bundle.putString("answer", answer);
                    bundle.putString("field_id", ((Field) fieldObject).getId());
                    currentFragmentTown.setArguments(bundle);
                    FragmentManager fm = getChildFragmentManager();
                    fm.beginTransaction().add(fragIdTown, currentFragmentTown, fragTag).commit();
                    fragIdTown++;

                    fieldFragListTown.add(new FieldFragPair(fragField.getId(), fragTag));
                    currentFragmentTown = null; //remove fragment from currentFragmentDate instance
                    Log.d(TAG, fragTag + " | " + answer);
                }

            }
        }
    }

    private LinearLayout addTextField(LinearLayout baseView,TextField field){

        View optionsView = inflater.inflate(R.layout.model_text,null);
        ((EditText)optionsView.findViewById(R.id.et_answer)).setHint(field.getHint());

        if(field.getAnswers().size()>0)
            ((EditText)optionsView.findViewById(R.id.et_answer)).setText(field.getAnswers().get(0));

        String etValue=((EditText)optionsView.findViewById(R.id.et_answer)).getText().toString().trim();
        ((LinearLayout)baseView.findViewById(R.id.options)).addView(optionsView);

        return baseView;
    }

    private LinearLayout addRadioField(LinearLayout baseView,RadioField field){
Log.d(TAG,"radio type: "+field.getType());
        View optionsView = inflater.inflate(R.layout.model_radio,null);

          RadioGroup radioGroup = optionsView.findViewById(R.id.radio_group);
        ArrayList<String>options=field.getOptions();
        for(int a=0;a<options.size();a++){
            RadioButton radioButton=new RadioButton(getActivity());
            radioButton.setText(options.get(a));
            if(field.getAnswers().size()>0&&field.getAnswers().get(0)!=null&&field.getAnswers().get(0).trim().equalsIgnoreCase(radioButton.getText().toString())){
                radioButton.setChecked(true);
            }
          //  radioButton.setOnCheckedChangeListener(new MyRadioListener(otherRadio));
            radioGroup.addView(radioButton);
        }

        //Now, add other view as last item of radio group
        //radioGroup.addView(other);


        ((LinearLayout)baseView.findViewById(R.id.options)).addView(optionsView);

        return baseView;
    }

    private LinearLayout addTextAreaField(LinearLayout baseView,TextAreaField field){

        View optionsView = inflater.inflate(R.layout.model_textarea,null);
        ((EditText)optionsView.findViewById(R.id.et_answer)).setHint(field.getHint());
        if(field.getAnswers().size()>0)
        ((EditText)optionsView.findViewById(R.id.et_answer)).setText(field.getAnswers().get(0));

        ((LinearLayout)baseView.findViewById(R.id.options)).addView(optionsView);

        return baseView;
    }

    private LinearLayout addCheckboxField(LinearLayout baseView,CheckboxField field){

        LinearLayout optionsView = (LinearLayout) inflater.inflate(R.layout.model_checkbox,null);

        ArrayList<String>options=field.getOptions();
        ArrayList<String>answers=field.getAnswers();

        for(int a=0;a<options.size();a++){
            CheckBox checkBox=new CheckBox(getActivity());
            checkBox.setText(options.get(a));

            if(answers!=null&&answers.contains(checkBox.getText().toString())){
                checkBox.setChecked(true);
            }
            else {
                checkBox.setChecked(false);
            }
            optionsView.addView(checkBox);
        }

        ((LinearLayout)baseView.findViewById(R.id.options)).addView(optionsView);


        return baseView;
    }

    private LinearLayout addEmailField(LinearLayout baseView,EmailField field){

        View optionsView = inflater.inflate(R.layout.model_text,null);
        ((EditText)optionsView.findViewById(R.id.et_answer)).setHint(field.getHint());
      if(field.getAnswers().size()>0)
        ((EditText)optionsView.findViewById(R.id.et_answer)).setText(field.getAnswers().get(0));

        ((EditText)optionsView.findViewById(R.id.et_answer)).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        ((LinearLayout)baseView.findViewById(R.id.options)).addView(optionsView);

        return baseView;
    }

    private LinearLayout addDateField(LinearLayout baseView,DateField field){
        View DateView = inflater.inflate(R.layout.model_date,null);
        DateView.setId(fragIdDate); //this id makes it easy for fragment to be set to this specific layout
        ((LinearLayout)baseView.findViewById(R.id.options)).addView(DateView);

        currentFragmentDate = new DateFragment();
        return baseView;
    }


    private LinearLayout addTownField(LinearLayout baseView,TownField field){
        View DateView = inflater.inflate(R.layout.model_town,null);
        DateView.setId(fragIdTown); //this id makes it easy for fragment to be set to this specific layout
        ((LinearLayout)baseView.findViewById(R.id.options)).addView(DateView);

        currentFragmentTown = new TownFragment();
        return baseView;
    }


    private LinearLayout addMappingField(LinearLayout baseView,DateField field){
        View DateView = inflater.inflate(R.layout.model_mapping,null);
        DateView.setId(fragIdMapping); //this id makes it easy for fragment to be set to this specific layout
        ((LinearLayout)baseView.findViewById(R.id.options)).addView(DateView);
        currentFragmentMapping = new DateFragment();
        return baseView;
    }

    private LinearLayout addNumericField(LinearLayout baseView,NumericField field){

        View optionsView = inflater.inflate(R.layout.model_text,null);
        ((EditText)optionsView.findViewById(R.id.et_answer)).setHint(field.getHint());
        ((EditText)optionsView.findViewById(R.id.et_answer)).setText(field.getAnswers().get(0));
        ((EditText)optionsView.findViewById(R.id.et_answer)).setInputType(InputType.TYPE_CLASS_NUMBER);

        ((LinearLayout)baseView.findViewById(R.id.options)).addView(optionsView);

        return baseView;
    }

    private LinearLayout addDropdownField(LinearLayout baseView,DropdownField field){

        View optionsView = inflater.inflate(R.layout.model_dropdown,null);
          Spinner spinner = optionsView.findViewById(R.id.sp_answer);

        List<String> options=field.getOptions();
        if (field.getHint() == null||field.getHint().trim().equalsIgnoreCase(""))
            field.setHint("Choose one item");

        options.add(field.getHint());

        if(options.size()>1) {
            final int listsize = options.size() - 1;
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,options) {
                @Override
                public int getCount() {
                    return (listsize); // Truncate the list
                }
            };

            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapter);
            spinner.setSelection(listsize);
            spinner.setPrompt(field.getHint());
            spinner.setFocusable(true);

            if (field.getAnswers().size()>0&&field.getAnswers().get(0) != null&&!field.getAnswers().get(0).trim().equalsIgnoreCase(""))
                spinner.setSelection(options.indexOf(field.getAnswers().get(0)));
        }

        ((LinearLayout)baseView.findViewById(R.id.options)).addView(optionsView);

        return baseView;
    }

    private LinearLayout addFileField(LinearLayout baseView,FileField field){
        Log.d(TAG,"add filefield: "+field.getId());
        if(field.getType().equals(FileField.FILE_IMAGE)) {
         //field type is image
            View ImageView = inflater.inflate(R.layout.model_file,null);
            ImageView.setId(fragIdImage); //this id makes it easy for fragment to be set to this specific layout
            ((LinearLayout)baseView.findViewById(R.id.options)).addView(ImageView);

            currentFragmentImage = new ImageFragment();
        }
        else if(field.getType().equals(FileField.FILE_VIDEO)) {
            //field type is image
            View videoView = inflater.inflate(R.layout.model_file,null);
            videoView.setId(fragIdVideo); //this id makes it easy for fragment to be set to this specific layout
            ((LinearLayout)baseView.findViewById(R.id.options)).addView(videoView);

            currentFragmentVideo = new VideoFragment();
        }
        else if(field.getType().equals(FileField.FILE_MULTI_IMAGE)) {
            //field type is image
            View vMultiImage = inflater.inflate(R.layout.model_file,null);
            vMultiImage.setId(fragIdMultiImage); //this id makes it easy for fragment to be set to this specific layout
            ((LinearLayout)baseView.findViewById(R.id.options)).addView(vMultiImage);

            currentFragmentMultiImage = new MultiImageFragment();
        }
        if(field.getType().equals(FileField.FILE_SCAN)) {
            //field type is image
            View scanView = inflater.inflate(R.layout.model_file,null);
            scanView.setId(fragIdScan); //this id makes it easy for fragment to be set to this specific layout
            ((LinearLayout)baseView.findViewById(R.id.options)).addView(scanView);

            currentFragmentScan = new ScanFragment();
        }

        return baseView;
    }


    protected boolean validateForm(LinearLayout parent,int sectionIndex){
        boolean isValid=true; //this particular form is valid until proven otherwise
        int firstErrorTag=-1;//-1 implies no error tag has been set yet
        ArrayList result;
        ArrayList<Object> fields= CreateForm.getInstance(getActivity()).sections.get(sectionIndex);
        Validation validation =new Validation();
        for(int a=0;a<fields.size();a++){
            //Get our base view with tag specified during generateForm()

            if(fields.get(a) instanceof Section){
                continue;
            }
              LinearLayout baseView = parent.findViewWithTag(((Field) fields.get(a)).getTag());
            Object field=fields.get(a);
            result=new ArrayList();

            if(fields.get(a) instanceof RadioField){
                validation =validateRadioField(baseView, (RadioField) field);
                result=validation.getResults();
            }
            else if(field instanceof DropdownField){
                validation =validateDropDown(baseView, (DropdownField) field);
                result=validation.getResults();
            }
            else if(field instanceof CheckboxField){
                validation =validateCheckboxField(baseView, (CheckboxField) field);
                result=validation.getResults();
            }
            else if(field instanceof FileField){
                validation =validateFileField(baseView, (FileField) field);
                result=validation.getResults();
            }
            else if(field instanceof TextAreaField){
                validation =validateTextAreaField(baseView, (TextAreaField) field);
                result=validation.getResults();
            }
            else if(field instanceof EmailField){
                validation =validateEmailField(baseView, (EmailField) field);
                result=validation.getResults();
            }
            else if(field instanceof DateField){
                validation =validateDateField(baseView, (DateField) field);
                result=validation.getResults();
            }
            else if(field instanceof NumericField){
                validation =validateNumericField(baseView, (NumericField) field);
                result=validation.getResults();
            }

            else if(fields.get(a) instanceof TextField){
                validation =validateTextField(baseView, (TextField) field);
                result=validation.getResults();
            }
            else if(field instanceof TownField){
                validation =validateTownField(baseView, (TownField) field);
                result=validation.getResults();
            }

            //add answerList to field
            if(result!=null)
                ((Field)field).setAnswers(result);

            if(validation.isError()){
                ((TextView)baseView.findViewById(R.id.tv_error)).setText(validation.getMessage());
                baseView.findViewById(R.id.tv_error).setVisibility(View.VISIBLE);

                if(isValid)
                    firstErrorTag=((Field)field).getTag();//for the first error found, cache the view tag in order to focus it first
                     isValid=false;//an error found hence form not valid
                 }

                //Log.d(TAG,"replacing with updated field "+((TextField)field).getAnswers().get(0));
                fields.set(a,field);//replacing with updated field (now with answer)
        }
        if(firstErrorTag!=-1){
            //Log.d(TAG,"firstErrorTag: "+firstErrorTag);
            //(parent.findViewWithTag(firstErrorTag)).requestFocus();//scroll to first view on page with error
            (parent.findViewWithTag(firstErrorTag)).getParent()
                    .requestChildFocus((parent.findViewWithTag(firstErrorTag)),(parent.findViewWithTag(firstErrorTag)));
        }

        CreateForm.getInstance(getActivity()).sections.set(sectionIndex,fields); //replace old section with updated section
        return isValid;
    }

    protected void clearAllErrors(LinearLayout parent,int sectionIndex){
        ArrayList<Object> fields= CreateForm.getInstance(getActivity()).sections.get(sectionIndex);
        for(int a=0;a<fields.size();a++){
            //Get our base view with tag specified during generateForm()
            if(fields.get(a) instanceof Section){
                continue;
            }
              LinearLayout baseView = parent.findViewWithTag(((Field) fields.get(a)).getTag());

            ((TextView)(baseView).findViewById(R.id.tv_error)).setText(null);
            ((baseView).findViewById(R.id.tv_error)).setVisibility(View.GONE);
        }
    }

    private Validation validateTextField(LinearLayout baseView, TextField field){
        Validation validation =new Validation();
        String answer=((EditText) baseView.findViewById(R.id.et_answer)).getText().toString().trim();
        ArrayList<String> answers=new ArrayList<>();
        if(field.isRequired()&&(answer.trim().isEmpty())) {
                validation.setError(true);
                validation.setMessage(getActivity().getResources().getString(R.string.error_field_required));
        }
        answers.add(answer);
        validation.setResults(answers);

        return validation;
    }

    private Validation validateRadioField(LinearLayout baseView, RadioField field) {

        Validation validation = new Validation();
        ArrayList answers=new ArrayList();

          RadioGroup radioGroup = baseView.findViewById(R.id.radio_group);

             for (int a = 0; a < radioGroup.getChildCount(); a++) {
                View v = radioGroup.getChildAt(a);
                if(v instanceof RadioButton){
                    if(((RadioButton)v).isChecked()){
                        //since one radio button is checked, clear error and break outta loop
                        validation.setError(false);
                        validation.setMessage(null);
                        answers.add(((RadioButton)v).getText().toString().trim());//pass answer back to answers arrayList
                        break;
                    }
                }
             }
                 if (field.isRequired()&&answers.size()==0)//since there were no answers selected although field is required
                 {
                     validation.setError(true);//until proven otherwise, there is an error (field has not been filled)
                     validation.setMessage(getActivity().getResources().getString(R.string.error_field_required));
                 }

        validation.setResults(answers);
        return validation;
    }


    private Validation validateDropDown(LinearLayout baseView, DropdownField field) {
        Validation validation =new Validation();
        String answer=((Spinner) baseView.findViewById(R.id.sp_answer)).getSelectedItem().toString().trim();
        ArrayList<String> answers=new ArrayList<>();
        if(field.isRequired()&&(answer.trim().isEmpty()||field.getHint().equalsIgnoreCase(answer))) {
            validation.setError(true);
            validation.setMessage(getActivity().getResources().getString(R.string.error_field_required));
        }
        if(!field.getHint().equalsIgnoreCase(answer))
                answers.add(answer);

        validation.setResults(answers);


        return validation;
    }

    private Validation validateTextAreaField(LinearLayout baseView, TextAreaField field){
        Validation validation =new Validation();
        String answer=((EditText) baseView.findViewById(R.id.et_answer)).getText().toString().trim();
        ArrayList<String> answers=new ArrayList<>();
        if(field.isRequired()&&(answer.trim().isEmpty())) {
                validation.setError(true);
                validation.setMessage(getActivity().getResources().getString(R.string.error_field_required));
        }

        answers.add(answer);
        validation.setResults(answers);

        return validation;
    }

    private Validation validateCheckboxField(LinearLayout baseView, CheckboxField field) {

        Validation validation = new Validation();
          LinearLayout options = baseView.findViewById(R.id.options);
          LinearLayout checkboxGroup = options.findViewById(R.id.checkbox_group);
        ArrayList<String>answers=new ArrayList<>();

            for (int a = 0; a < checkboxGroup.getChildCount(); a++) {
             //   Log.d(TAG,"options: "+a);
                View v = checkboxGroup.getChildAt(a);
                if(v instanceof CheckBox){
                    //Log.d(TAG,"checkbox instance: checked "+((CheckBox)v).isChecked());
                    if(((CheckBox)v).isChecked()){
                        //since one radio button is checked, clear error and break outta loop
                        validation.setError(false);
                        validation.setMessage(null);
                        answers.add(((CheckBox)v).getText().toString());
                    }
                }
            }

        if (field.isRequired()&&answers.size()==0) {
            validation.setError(true);//until proven otherwise, there is an error (field has not been filled)
            validation.setMessage(getActivity().getResources().getString(R.string.error_field_required));
        }

        validation.setResults(answers);//send list of user options (answers) back to the calling method
        return validation;
    }

    private Validation validateDateField(LinearLayout baseView, DateField field){
        Validation validation =new Validation();
        String answer=null;
        String fragTag=null;
        String field_id=field.getId();
        for(int a = 0; a< fieldFragListDate.size(); a++){
            if(field_id.equalsIgnoreCase(fieldFragListDate.get(a).getField_id())){
                fragTag= fieldFragListDate.get(a).getFrag_tag();//frag tag has been found in cached list
                break;
            }
        }
        if(fragTag!=null){
            FragmentManager fm = getChildFragmentManager();
            Fragment frag = fm.findFragmentByTag(fragTag);
            if(frag instanceof DateFragment){
                DateFragment dateFragment= (DateFragment) frag;
                answer=dateFragment.getDate();//get base64 string from specific fragment
            }
        }

        ArrayList answers=new ArrayList();
        if(field.isRequired()&&(answer==null||answer.trim().isEmpty())) {
            validation.setError(true);
            validation.setMessage(getActivity().getResources().getString(R.string.error_field_required));
        }
        // Log.d(TAG,"base64String: "+answer);
        answers.add(answer);
        validation.setResults(answers);
        return validation;
    }

    private Validation validateTownField(LinearLayout baseView, TownField field){
        Validation validation =new Validation();
        String answer=null;
        String fragTag=null;
        String field_id=field.getId();
        for(int a = 0; a< fieldFragListTown.size(); a++){
            if(field_id.equalsIgnoreCase(fieldFragListTown.get(a).getField_id())){
                fragTag= fieldFragListTown.get(a).getFrag_tag();//frag tag has been found in cached list
                break;
            }
        }
        if(fragTag!=null){
            FragmentManager fm = getChildFragmentManager();
            Fragment frag = fm.findFragmentByTag(fragTag);
            if(frag instanceof TownFragment){
                TownFragment townFragment= (TownFragment) frag;
                answer=townFragment.getTown();//get Town string from specific fragment
            }
        }

        ArrayList answers=new ArrayList();
        if(field.isRequired()&&(answer==null||answer.trim().isEmpty())) {
            validation.setError(true);
            validation.setMessage(getActivity().getResources().getString(R.string.error_field_required));
        }

        answers.add(answer);
        validation.setResults(answers);
        return validation;
    }

    private Validation validateNumericField(LinearLayout baseView, NumericField field){
        Validation validation =new Validation();
        String answer=((EditText) baseView.findViewById(R.id.et_answer)).getText().toString().trim();
        ArrayList answers=new ArrayList();
        if(field.isRequired()&&(answer.trim().isEmpty())) {
                validation.setError(true);
                validation.setMessage(getActivity().getResources().getString(R.string.error_field_required));
        }

        answers.add(answer);
        validation.setResults(answers);
        return validation;
    }

    private Validation validateEmailField(LinearLayout baseView, EmailField field){
        Validation validation =new Validation();
        String answer=((EditText) baseView.findViewById(R.id.et_answer)).getText().toString().trim();
        ArrayList answers=new ArrayList();

        if(field.isRequired()&&(answer.trim().isEmpty())) {
            validation.setError(true);
            validation.setMessage(getActivity().getResources().getString(R.string.error_field_required));
        }
        if(!myApplication.isEmailValid(answer)&&!validation.isError()){
            validation.setError(true);
            validation.setMessage(getActivity().getResources().getString(R.string.error_invalid_email));
        }

        answers.add(answer);
        validation.setResults(answers);

        return validation;
    }

    private Validation validateFileField(LinearLayout baseView, FileField field){
        Validation validation =new Validation();
        validation.setError(false);
        String answer=null;
        String fragTag=null;
        String field_id=field.getId();
        if(field.getType().equals(FileField.FILE_IMAGE))
            return validateImage(field);
        else  if(field.getType().equals(FileField.FILE_VIDEO))
            return validateVideo(field);
        else if(field.getType().equals(FileField.FILE_MULTI_IMAGE))
            return validateMultiImage(field);
        else if(field.getType().equals(FileField.FILE_SCAN))
            return validateScan(field);

        return validation;
    }


    private Validation validateImage(FileField field){
        Validation validation =new Validation();
        String answer=null;
        String fragTag=null;
        String field_id=field.getId();
            for (int a = 0; a < fieldFragListImage.size(); a++) {
                if (field_id.equalsIgnoreCase(fieldFragListImage.get(a).getField_id())) {
                    fragTag = fieldFragListImage.get(a).getFrag_tag();//frag tag has been found in cached list
                    break;
                }
            }
            if (fragTag != null) {
                FragmentManager fm = getChildFragmentManager();
                Fragment frag = fm.findFragmentByTag(fragTag);
                if (frag instanceof ImageFragment) {
                    ImageFragment imageFragment = (ImageFragment) frag;
                    answer=imageFragment.getPath();
                }
            }

            ArrayList answers = new ArrayList();
            if (field.isRequired() && (answer == null || answer.trim().isEmpty())) {
                validation.setError(true);
                validation.setMessage(getActivity().getResources().getString(R.string.error_field_required));
            }

            answers.add(answer);
            validation.setResults(answers);
            return validation;
    }

    private Validation validateVideo(FileField field){
        Validation validation =new Validation();
        String answer=null;
        String fragTag=null;
        String field_id=field.getId();
        for (int a = 0; a < fieldFragListVideo.size(); a++) {
            if (field_id.equalsIgnoreCase(fieldFragListVideo.get(a).getField_id())) {
                fragTag = fieldFragListVideo.get(a).getFrag_tag();//frag tag has been found in cached list
                break;
            }
        }
        if (fragTag != null) {
            FragmentManager fm = getChildFragmentManager();
            Fragment frag = fm.findFragmentByTag(fragTag);
            if (frag instanceof VideoFragment) {
                VideoFragment videoFragment = (VideoFragment) frag;
                answer=videoFragment.getPath();
               }
        }
        ArrayList answers = new ArrayList();
        if (field.isRequired() && (answer == null || answer.trim().isEmpty())) {
            validation.setError(true);
            validation.setMessage(getActivity().getResources().getString(R.string.error_field_required));
        }
        // Log.d(TAG,"base64String: "+answer);
        answers.add(answer);
        validation.setResults(answers);
        return validation;
    }

    private Validation validateMultiImage(FileField field){
        Validation validation =new Validation();
        ArrayList answers = new ArrayList();
        String fragTag=null;
        String field_id=field.getId();
        for (int a = 0; a < fieldFragListMultiImage.size(); a++) {
            if (field_id.equalsIgnoreCase(fieldFragListMultiImage.get(a).getField_id())) {
                fragTag = fieldFragListMultiImage.get(a).getFrag_tag();//frag tag has been found in cached list
                break;
            }
        }
        if (fragTag != null) {
            FragmentManager fm = getChildFragmentManager();
            Fragment frag = fm.findFragmentByTag(fragTag);
            if (frag instanceof MultiImageFragment) {
                MultiImageFragment imageFragment = (MultiImageFragment) frag;

                ArrayList<Image> images=imageFragment.getAllUri();
                if(images!=null&&images.size()!=0) {
                    for (int a = 0; a < images.size(); a++) {
                        Uri uri=images.get(a).getUri();
                        if(uri!=null)
                            answers.add(uri.getPath());
                    }

                    if(answers.size()==0){
                        answers.add(null);
                    }
                }
                else {
                    answers.add(null);
                }
            }
        }
        if (field.isRequired() && (answers.size()==0 ||answers.get(0)==null)) {
            validation.setError(true);
            validation.setMessage(getActivity().getResources().getString(R.string.error_field_required));
        }
        validation.setResults(answers);
        return validation;
    }

    private Validation validateScan(FileField field){
        Validation validation =new Validation();
        String answer=null;
        String fragTag=null;
        String field_id=field.getId();
        for (int a = 0; a < fieldFragListScan.size(); a++) {
            if (field_id.equalsIgnoreCase(fieldFragListScan.get(a).getField_id())) {
                fragTag = fieldFragListScan.get(a).getFrag_tag();//frag tag has been found in cached list
                break;
            }
        }
        if (fragTag != null) {
            FragmentManager fm = getChildFragmentManager();
            Fragment frag = fm.findFragmentByTag(fragTag);
            if (frag instanceof ScanFragment) {
                ScanFragment scanFragment = (ScanFragment) frag;
                answer=scanFragment.getPath();
            }
        }

        ArrayList answers = new ArrayList();
        if (field.isRequired() && (answer == null || answer.trim().isEmpty())) {
            validation.setError(true);
            validation.setMessage(getActivity().getResources().getString(R.string.error_field_required));
        }

        answers.add(answer);
        validation.setResults(answers);
        return validation;
    }


    @SuppressWarnings("deprecation")
    private Spanned required(String question){
        String asterisk = "<font color='#EE0000'> *</font>";
       // Spanned formatted=Html.fromHtml(asteriks);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(question+asterisk, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(question+asterisk);
        }
       // return formatted;
    }

    private class MyRadioListener implements CompoundButton.OnCheckedChangeListener{
        MyRadio otherRadio;
        RadioGroup radioGroup;
        public MyRadioListener(MyRadio otherRadio){
            this.otherRadio=otherRadio;
        }

        public MyRadioListener(MyRadio otherRadio,RadioGroup radioGroup){
            this.otherRadio=otherRadio;
            this.radioGroup=radioGroup;
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
           if(compoundButton instanceof MyRadio&&b){
               otherRadio.etEnabled(true);
               otherRadio.etRequestFocus();

               for(int a =0;a<radioGroup.getChildCount();a++){
                   if(radioGroup.getChildAt(a) instanceof RadioButton)
                            ((RadioButton)radioGroup.getChildAt(a)).setChecked(false);
               }
           }
           else if(compoundButton instanceof RadioButton&&b){
               otherRadio.setChecked(false);
               otherRadio.etClear();
               otherRadio.etSetHint("Other");
               //otherRadio.etEnabled(false);
           }
        }
    }
}
