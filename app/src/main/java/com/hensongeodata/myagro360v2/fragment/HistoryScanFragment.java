package com.hensongeodata.myagro360v2.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hensongeodata.myagro360v2.Interface.TaskListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.adapter.HistoryScanAdapterRV;
import com.hensongeodata.myagro360v2.controller.LinearLayoutManagerWithSmoothScroller;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.view.Scout;

import java.util.ArrayList;


/**
 * Created by user1 on 8/16/2017.
 */



public class HistoryScanFragment extends BaseFragment implements TaskListener{
    private static String TAG= HistoryScanFragment.class.getSimpleName();

    private String section_id;
    private View rootView=null;
    private ArrayList<InstructionScout> itemsVisible;
    private RecyclerView recyclerView;
    private HistoryScanAdapterRV adapter;
    public static int scan_total;
    public static int scan_detected;

    public HistoryScanFragment newInstance(int position) {
        HistoryScanFragment f = new HistoryScanFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        //Log.d(TAG,"instance: "+position );
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        section_id = getArguments() != null ? getArguments().getString("section_id") : "";
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         rootView = inflater.inflate(R.layout.history_scan_fragment, container, false);

         MyApplication.initLive(getActivity(),this);

          recyclerView = rootView.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(getActivity()));

        itemsVisible=new ArrayList<>();
        itemsVisible.add(new InstructionScout());
        itemsVisible.addAll(databaseHelper.getScoutList(section_id, ChatbotAdapterRV.MODE_SCOUT));

        scan_total=itemsVisible.size()-1;

        int index=0;
        for(InstructionScout instructionScout:itemsVisible){
            String message=instructionScout.getMessage();
            if(message==null||message.trim().isEmpty()&&index!=0) {
                MyApplication.itemsLive.add(instructionScout);
                MyApplication.itemsLivePosition.add(index);
            }

            index++;
        }

        updateDetection(false);

        adapter=new HistoryScanAdapterRV(getActivity(),itemsVisible);
        adapter.setMode(ChatbotAdapterRV.MODE_SCOUT);
        recyclerView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onTaskAccepted(String id) {

    }

    @Override
    public void onTaskStarted(String id) {

    }

    @Override
    public void onTaskStopped(String id, boolean complete) {
        Log.d(TAG,"task complete "+complete);
        if(complete){
            InstructionScout instructionScout=databaseHelper.getInstructScout(id,ChatbotAdapterRV.MODE_SCOUT);
            Log.d(TAG,"instruction scout "+instructionScout);
            if(instructionScout!=null){
                int index= Scout.getIndex(itemsVisible,instructionScout);
                Log.d(TAG,"index "+index);
                if(index>=0)
                    adapter.updateItem(index,instructionScout);
            }

            updateDetection(true);
        }
    }

    @Override
    public void onTaskCancelled(String id) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MyApplication.shutdownLive();
    }

    private void updateDetection(boolean notifyChange){
        ArrayList<InstructionScout> scouts=databaseHelper.getScoutList(section_id, ChatbotAdapterRV.MODE_SCOUT);
        scan_detected=0;
        for(InstructionScout scout: scouts) {
            String message=scout.getMessage();
            if (message != null && !message.trim().isEmpty()
                    && !message.equalsIgnoreCase("No label found")) {
                scan_detected++;
            }
        }

        if (notifyChange)
            adapter.notifyItemChanged(0);
    }
}
