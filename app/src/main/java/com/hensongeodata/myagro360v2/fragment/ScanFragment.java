package com.hensongeodata.myagro360v2.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.Interface.PercentageListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.LocsmmanProAPI;
import com.hensongeodata.myagro360v2.api.response.ScanResponse;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.controller.ProgressAnimation;
import com.hensongeodata.myagro360v2.controller.RetrofitHelper;
import com.hensongeodata.myagro360v2.controller.Utility;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.ScanFAW;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.view.CameraScan;
import com.hensongeodata.myagro360v2.view.MyCamera;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.hensongeodata.myagro360v2.controller.RetrofitHelper.createPartFromString;

/**
 * Created by user1 on 9/12/2017.
 */

public class ScanFragment extends MediaFragment{
    private static String TAG=ScanFragment.class.getSimpleName();
    private static int REQUEST_SCANNER=100;
    private ScanFragmentReceiver receiver;
    private IntentFilter filter;
    private View vNotice;
    private ImageView ivNotice;
    private TextView tvHeader;
    private TextView tvMessage;
    private ScanFAW scanFAW;
    SharePrefManager sharePrefManager;
    private String NOTICE_HEADER="ScanActivity";
    private String NOTICE_MESSAGE="";
    public static int FAW_DETECTION_TRUE=1;
    public static int FAW_DETECTION_FALSE=0;
    private int index=0;
    private TextView tvLocation;

    private TextView tvPercentage;
    private TextView tvScanning;
    private ProgressBar vProgress;

    private Handler handler;
    private Runnable runnable;
    private Vibrator vibrator;
    private boolean isCancelled=false;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       super.onCreateView(inflater,container,savedInstanceState);
        receiver=new ScanFragmentReceiver();
        filter=new IntentFilter(BroadcastCall.BSFRAGMENT);
        getActivity().registerReceiver(receiver,filter);

        scanFAW=new ScanFAW(getActivity());
        vibrator = (Vibrator)getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        sharePrefManager = new SharePrefManager(getActivity());

        MyApplication.scanFAWList.add(scanFAW);
        index=MyApplication.scanFAWList.size()-1;

          tvLocation = baseView.findViewById(R.id.tv_location);
        fabCapture.setOnClickListener(scanImageListener);
        Log.e(TAG, "fab clicked");
        fabCapture.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.ic_scan_white));

        vNotice=baseView.findViewById(R.id.v_faw_notice);
        vNotice.setVisibility(View.VISIBLE);
          ivNotice = vNotice.findViewById(R.id.iv_notice);
        ivNotice.setImageResource(R.drawable.ic_information);
          tvHeader = vNotice.findViewById(R.id.tv_header);
          tvMessage = vNotice.findViewById(R.id.tv_message);
        toggleView(ACTION_HIDE_ALL);
        setNotice();

        if(MyCamera.autoStart)
            //Toast.makeText(getActivity(), "Yes", Toast.LENGTH_SHORT).show();
            openScanner();

        MyCamera.autoStart=false;

        updateScannerStatus();

       // imageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.placeholder));
        Picasso.get().load(R.drawable.placeholder).resize(500,500).into(imageView);
        handler=new Handler();
        runnable=new Runnable() {
            @Override
            public void run() {
                onScanImageResult();
            }
        };

          vProgress = baseView.findViewById(R.id.v_progress);
        vProgress.setProgress(0);
        vProgress.setMax(10000);
          tvPercentage = baseView.findViewById(R.id.tv_percentage);
          tvScanning = baseView.findViewById(R.id.tv_tag);

        return baseView;
    }

    View.OnClickListener scanImageListener =new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            openScanner();
        }
    };


    public String getPath(){
        Log.d(TAG,"JoinCommunityResponse: "+scanFAW.getResponse());
        switch (scanFAW.getResponse()) {
            case -1:
                return "No scan made";
            case 0:
                return "Pest not detected";
            case 1:
                return "Pest detected";
            default:
                return "No scan made";
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.d(TAG,"status: "+Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        openScanner();
                } else {
                    //code for deny
                    Toast.makeText(getActivity(),"Permission required to continue.",Toast.LENGTH_SHORT).show();
                }
                break;
            default: return;
        }
    }

    private void openScanner(){
        if(MyApplication.hasLocation()) {
            MyCamera.action = MyCamera.ACTION_SCAN;
            Intent i = new Intent(getActivity(), CameraScan.class);
            i.putExtra("index", index);
            getActivity().startActivityForResult(i, REQUEST_SCANNER);
        }
        else {
            MyApplication.showToast(getActivity(),"Generating your location. " +
                    "Please try again after a few seconds",Gravity.CENTER);
        }
    }

    private void onScanImageResult(String path) {
        if(path!=null&&!path.trim().isEmpty()) setImage(path);
        else Toast.makeText(getActivity(),"Couldn't load image",Toast.LENGTH_SHORT).show();

        setNotice();
    }

    private void onScanImageResult() {
        toggleView(ACTION_SHOW_ALL);
        scanFAW=MyApplication.scanFAWList.get(index);
        Image image=scanFAW.getImage();
        if(image!=null&&image.getUri()!=null&&image.getUri().getPath()!=null
                &&!image.getUri().getPath().trim().isEmpty())
            setImage(image.getUri().getPath());
        else {
            //toggleView(ACTION_HIDE_ALL);
            //MyApplication.showToast(getActivity(),"No image attached",Gravity.CENTER);
        }

        if(getActivity()!=null&&getActivity().getResources()!=null)
                setNotice();
    }

    private void setNotice(){
        MyApplication.scanFAWList.set(index,scanFAW);
        tvHeader.setText(scanFAW.getHeader());
        tvMessage.setText( scanFAW.getMessage());
        updateNoticeTheme();
    }

    private void updateNoticeTheme(){
        switch (scanFAW.getResponse()){
            case -1: setNoticeTheme(R.color.disabledBlack);tvLocation.setVisibility(View.VISIBLE); break;
            case 0: setNoticeTheme(R.color.colorAccent); tvLocation.setVisibility(View.GONE); break;
            case 1: setNoticeTheme(R.color.red); tvLocation.setVisibility(View.GONE); break;
            default: setNoticeTheme(R.color.disabledBlack); tvLocation.setVisibility(View.GONE); break;
        }
    }

    private void setNoticeTheme(int color){
        tvHeader.setTextColor(getActivity().getResources().getColor(color));
        tvMessage.setTextColor(getActivity().getResources().getColor(color));
        ivNotice.setColorFilter(getActivity().getResources().getColor(color));
    }

    private void setImage(String path){
        Image image=new Image();
        image.setImageBitmap(myApplication.bitmapCompressLarge(path));
        if(image.getImageBitmap()!=null) {
              image.setImageString(MyApplication.encodeBitmap(image.getImageBitmap()));
            imageView.setImageBitmap(image.getImageBitmap());
            image.setUri(Uri.parse(path));
            scanFAW.setImage(image);
        }

    }

    private void updateScannerStatus(){
        if(MyApplication.hasLocation()){
            tvLocation.setText("Scanner Ready!");
            tvLocation.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== Activity.RESULT_OK){
            if(requestCode==REQUEST_SCANNER){
                String path=data.getStringExtra("path");
                Log.d(TAG,"path: "+path);
                if(path!=null&&!path.trim().isEmpty()) {
                    processImage(path);
                }
            }
        }
    }

    private void processImage(String path){
        setImage(path);
        MultipartBody.Part file = RetrofitHelper.prepareFilePart(getActivity(),"image",path);
        sendFile(file);
    }

    public void sendFile(MultipartBody.Part file){
        isCancelled=false;
        Retrofit.Builder builder=new Retrofit.Builder()
                .baseUrl(Keys.AGRO_BASE)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit=builder.build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

        String gps="";
        if(MyApplication.poi!=null)
            gps=MyApplication.poi.getLat()+","+MyApplication.poi.getLon();


        Log.d(TAG,"gps: "+gps);
        Call<ScanResponse> call= service.scanFAW(
                createPartFromString(Keys.APP_ID),
                createPartFromString(sharePrefManager.getOrganisationDetails().get(0)),
                createPartFromString(gps),
                createPartFromString((new User(getActivity())).getId()!=null?
                        (new User(getActivity())).getId():""), file,Keys.API_KEY);

        call.enqueue(new Callback<ScanResponse>() {
            @Override
            public void onResponse(Call<ScanResponse> call, Response<ScanResponse> response) {

                ScanResponse responseBody=response.body();
                String message=null;
                try {
                    if(responseBody!=null)
                        message=responseBody.getMsg();

                    Log.d(TAG,"JoinCommunityResponse: "+message+" body: "+responseBody
                            +" code: "+response.code()+" error: "+response.errorBody());
                } catch (Throwable e) {
                    e.printStackTrace();
                    processResult(NetworkRequest.STATUS_FAIL,message);
                }

                Log.d(TAG,"date: "+message);
                if(message==null||message.trim().equalsIgnoreCase("null"))
                    processResult(NetworkRequest.STATUS_FAIL,message);
                else
                    processResult(NetworkRequest.STATUS_SUCCESS,message);
            }

            @Override
            public void onFailure(Call<ScanResponse> call, Throwable t) {
                String message=t.getMessage();
                Log.d(TAG,"throwable ScanFragment: "+t);
                processResult(NetworkRequest.STATUS_FAIL,message);
            }
        });

        animateProgress();
    }

    private void processResult(int status,String response){
        if(!isCancelled) {
            if (status == NetworkRequest.STATUS_SUCCESS) {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.has("success")) {
                        int success = jsonObject.getInt("success");
                        Log.d(TAG, "success: " + success);
                        if (success == 1) {
                            scanFAW.setMessage(jsonObject.optString("date"));
                            scanFAW.setResponse(jsonObject.optInt("detected"));

                            if (scanFAW.getResponse() == FAW_DETECTION_TRUE) {
                                vibrator.vibrate(1000);
                                  scanFAW.setHeader("Pest detected");
                            } else
                                  scanFAW.setHeader("Pest not detected");
                        }
                    }

                    setNotice();

                    Log.d(TAG, "date: " + scanFAW.getMessage());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                onFailed();
            }
        }
    }

    public void onFailed(){
        isCancelled=false;
        setNotice();
    }
    @Override
    public void onResume() {
        super.onResume();
//        Log.d(TAG,"onResume()");
//        toggleView(ACTION_SHOW_PROGRESS);
//        if(handler!=null&&runnable!=null)
//        handler.postDelayed(runnable,5000);

    }


    private void animateProgress(){
        vProgress.setProgress(0);
        ProgressAnimation anim = new ProgressAnimation(vProgress, 0, vProgress.getMax());
        anim.setDuration(60000);
        anim.setPercentageListener(new PercentageListener() {
            @Override
            public void onUpdate(int percentage) {
                tvPercentage.setText(percentage+" %");
                if(percentage>=100)
                    onFailed();
            }
        });

        vProgress.startAnimation(anim);
        tvScanning.setText("Scanning...");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(handler!=null)handler.removeCallbacks(runnable);
        if(receiver!=null)getActivity().unregisterReceiver(receiver);
    }


    class ScanFragmentReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
           updateScannerStatus();
        }
    }
}