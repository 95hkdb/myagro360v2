//package com.hensongeodata.myagro360.fragment;
//
//import android.app.Activity;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.database.Cursor;
//import android.net.Uri;
//import android.os.Bundle;
//import android.provider.MediaStore;
//import android.support.annotation.Nullable;
//import android.support.v4.content.ContextCompat;
//import android.support.v7.app.AlertDialog;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.Toast;
//
//import com.hensongeodata.myagro360.R;
//import com.hensongeodata.myagro360.controller.Utility;
//import com.hensongeodata.myagro360.model.Image;
//import com.hensongeodata.myagro360.view.PreviewImageLarge;
//import com.squareup.picasso.Picasso;
//
///**
// * Created by user1 on 9/12/2017.
// */
//
//public class AudioFragment extends MediaFragment {
//    private static String TAG=AudioFragment.class.getSimpleName();
//    //View rootView;
//    private static int REQUEST_RECORDER =100;
//    private static int SELECT_FILE=200;
//    private static int PREVIEW_AUDIO =300;
//    private String userChoosenTask;
//    private Image imageModel;
//    static Uri uri;
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//       super.onCreateView(inflater,container,savedInstanceState);
//        // rootView = inflater.inflate(R.layout.frag_image, container, false);
//        imageModel=new Image();
//
//        fabCapture.setOnClickListener(addImageListener);
//        fabCapture.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.ic_image_white));
//
//        ibAction.setImageResource(R.drawable.ic_eye_white);
//        imageView.setOnClickListener(previewListener);
//
//        if(answer!=null&&!answer.trim().isEmpty()) {
//            setAudio(answer);
//        }
//        else {
//            toggleView(ACTION_HIDE_ALL);
//        }
//
//        return baseView;
//    }
//
//    View.OnClickListener addImageListener=new View.OnClickListener() {
//        @Override
//        public void onAddMapButtonClicked(View view) {
//            selectAudio();
//        }
//    };
//
//    View.OnClickListener previewListener=new View.OnClickListener() {
//        @Override
//        public void onAddMapButtonClicked(View view) {
//            previewAudio();
//        }
//    };
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if(resultCode== Activity.RESULT_OK){
//            if(requestCode== REQUEST_RECORDER){
//                toggleView(ACTION_SHOW_PROGRESS);
//                onRecordAudioResult(data);
//            }
//            else if(requestCode==SELECT_FILE){
//                toggleView(ACTION_SHOW_PROGRESS);
//                onSelectFromGalleryResult(data);
//            }
//            else if(requestCode== PREVIEW_AUDIO){
//                Log.d(TAG,"delete: "+data.getExtras().getBoolean("delete"));
//                if(data.getExtras().getBoolean("delete")){
//                    toggleView(ACTION_SHOW_PROGRESS);
//                    deleteAudio();
//                }
//            }
//        }
//    }
//
////    public String getBase64(){
////        String image_string=null;
////        if(imageModel.getImageBitmap()!=null)
////            image_string=myApplication.encodeBitmap(imageModel.getImageBitmap());
////
////        return image_string;
////    }
//
//    public String getPath(){
//        String path=null;
//        if(imageModel!=null&&imageModel.getUri()!=null)
//            path=imageModel.getUri().getPath();
//
//        return path;
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        Log.d(TAG,"status: "+Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
//        switch (requestCode) {
//            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    if(userChoosenTask.equals("Take Photo"))
//                        playerIntent();
//                    else if(userChoosenTask.equals("Choose from Library"))
//                        galleryIntent();
//                } else {
//                    //code for deny
//                    Toast.makeText(getActivity(),"Permission required to continue.",Toast.LENGTH_SHORT).show();
//                }
//                break;
//            default: return;
//        }
//    }
//
//    private void previewAudio(){
//        if(imageModel!=null) {
//            myApplication.setImageModel(imageModel);//because PreviewImageLarge class uses the global variable
//            Intent intent=new Intent(getActivity(), PreviewImageLarge.class);
//            intent.putExtra("path",imageModel.getUri().getPath());
//            startActivityForResult(intent, PREVIEW_AUDIO);
//        }
//        else
//            deleteAudio();
//    }
//
//    public void deleteAudio(){
//
//        if(imageModel!=null)
//            imageModel.setImageBitmap(null);
//
//        if(imageView!=null)
//            imageView.setVisibility(View.GONE);
//
//        toggleView(ACTION_HIDE_ALL);
//
//        uri =null;
//
//        answer=null;
////        tvBase64.setText(answer);
//        }
//
//    private void selectAudio() {
//        userChoosenTask="cancel";
//        final CharSequence[] items = { "Take Photo", "Choose from Library",
//                "Cancel" };
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle("Add Photo!");
//        builder.setItems(items, new DialogInterface.OnClickListener() {
//            @Override
//            public void onAddMapButtonClicked(DialogInterface dialog, int item) {
//                //boolean result= Utility.checkPermission(getActivity());
//                boolean result=Utility.checkPermission(getActivity());
//                if (items[item].equals("Take Photo")) {
//                    userChoosenTask="Take Photo";
//                    if(result)
//                        playerIntent();
//                } else if (items[item].equals("Choose from Library")) {
//                    userChoosenTask="Choose from Library";
//                    if(result)
//                        galleryIntent();
//                } else if (items[item].equals("Cancel")) {
//                    userChoosenTask="Cancel";
//                    dialog.dismiss();
//                }
//            }
//        });
//        builder.show();
//    }
//
//    private void playerIntent() {
//        Intent intent = new Intent(MediaStore.);
//        uri = Uri.fromFile(getOutputMediaFile());
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
//        startActivityForResult(intent, REQUEST_RECORDER);
//    }
//
//    private void galleryIntent() {
//       Intent intent= new Intent(Intent.ACTION_PICK,
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        intent.setType("audio/*");
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Audio"),SELECT_FILE);
//    }
//
//    private void setAudio(Uri uri, int width, int height, ImageView imageView){
//        Picasso.with(getActivity()).load(uri).resize(width, height).centerInside().into(imageView);
//    }
//
//
//    private void onSelectFromGalleryResult(Intent data) {
//        if (data != null) {
//            try {
//                Uri URI = data.getData();
//                String[] FILE = { MediaStore.Images.Media.DATA };
//
//                Cursor cursor = getActivity().getContentResolver().query(URI,
//                        FILE, null, null, null);
//                cursor.moveToFirst();
//                int columnIndex = cursor.getColumnIndex(FILE[0]);
//                String ImageDecode = cursor.getString(columnIndex);
//
//                cursor.close();
//                setAudio(ImageDecode);
//
//            }   catch (Exception e) {
//                Log.d(TAG,"error: "+e);
//                myApplication.showMessage(getActivity(),"An error occured",
//                        "Something went wrong whiles uploading image. Please try again");
//            }
//        }
//    }
//
//    private void onRecordAudioResult(Intent data) {
//
//        if(uri!=null) {
//            setAudio(uri.getPath());
//        }
//        else{
//            Toast.makeText(getActivity(),"Couldn't load image",Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    private void setAudio(String path){
//        imageModel=new Image();
//        imageModel.setImageBitmap(myApplication.bitmapCompressLarge(path));
//        imageModel.setImageString(myApplication.encodeBitmap(imageModel.getImageBitmap()));
//        imageView.setImageBitmap(imageModel.getImageBitmap());
//        imageModel.setUri(Uri.parse(path));
//        toggleView(ACTION_SHOW_ALL);
//    }
//}
