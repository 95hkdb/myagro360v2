//package com.faw.myagro360.fragment;
//
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.content.pm.PackageManager;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Bundle;
//import android.os.Handler;
//import android.support.annotation.Nullable;
//import android.support.annotation.RequiresApi;
//import android.support.v4.content.ContextCompat;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.faw.myagro360.MyApplication;
//import com.faw.myagro360.R;
//import com.faw.myagro360.controller.Utility;
//import com.faw.myagro360.model.Image;
//import com.faw.myagro360.model.ScanFAW;
//import com.faw.myagro360.view.CameraScan;
//import com.faw.myagro360.view.MyCamera;
//import com.squareup.picasso.Picasso;
//
///**
// * Created by user1 on 9/12/2017.
// */
//
//public class ScanFragment_backup extends MediaFragment {
//    private static String TAG=ScanFragment_backup.class.getSimpleName();
//    private static int REQUEST_SCANNER=100;
//    private View vNotice;
//    private ImageView ivNotice;
//    private TextView tvHeader;
//    private TextView tvMessage;
//
//    static Uri uri;
//    private ScanFAW scanFAW;
//    private String NOTICE_HEADER="ScanActivity";
//    private String NOTICE_MESSAGE="";
//    private int index=0;
//    private TextView tvLocation;
//
//    private IntentFilter filter;
//    private Handler handler;
//    private Runnable runnable;
//
//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//       super.onCreateView(inflater,container,savedInstanceState);
//        scanFAW=new ScanFAW(getActivity());
//        MyApplication.scanFAWList.add(scanFAW);
//        index=MyApplication.scanFAWList.size()-1;
//
//        tvLocation= (TextView) baseView.findViewById(R.id.tv_location);
//        fabCapture.setOnClickListener(scanImageListener);
//        fabCapture.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.ic_scan_white));
//
//        vNotice=baseView.findViewById(R.id.v_faw_notice);
//        vNotice.setVisibility(View.VISIBLE);
//        ivNotice= (ImageView) vNotice.findViewById(R.id.iv_notice);
//        ivNotice.setImageResource(R.drawable.ic_information);
//        tvHeader= (TextView) vNotice.findViewById(R.id.tv_header);
//        tvMessage= (TextView) vNotice.findViewById(R.id.tv_message);
//        toggleView(ACTION_HIDE_ALL);
//        setNotice();
//
//        if(MyCamera.autoStart)
//            openScanner();
//
//        MyCamera.autoStart=false;
//
//        updateScannerStatus();
//
//       // imageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.placeholder));
//        Picasso.with(getActivity()).load(R.drawable.placeholder).resize(500,500).into(imageView);
//        handler=new Handler();
//        runnable=new Runnable() {
//            @Override
//            public void run() {
//                onScanImageResult();
//            }
//        };
//
//        return baseView;
//    }
//
//    View.OnClickListener scanImageListener =new View.OnClickListener() {
//        @Override
//        public void onAddMapButtonClicked(View view) {
//            openScanner();
//        }
//    };
//
//
//    public String getPath(){
//        Log.d(TAG,"JoinCommunityResponse: "+scanFAW.getResponse());
//        switch (scanFAW.getResponse()) {
//            case -1:
//                return "No scan made";
//            case 0:
//                return "Fall army worm not detected";
//            case 1:
//                return "Fall army worm detected";
//            default:
//                return "No scan made";
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        Log.d(TAG,"status: "+Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
//        switch (requestCode) {
//            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                        openScanner();
//                } else {
//                    //code for deny
//                    Toast.makeText(getActivity(),"Permission required to continue.",Toast.LENGTH_SHORT).show();
//                }
//                break;
//            default: return;
//        }
//    }
//
//    private void openScanner(){
//        if(MyApplication.hasLocation()) {
//            MyCamera.action = MyCamera.ACTION_SCAN;
//            Intent i = new Intent(getActivity(), CameraScan.class);
//            i.putExtra("index", index);
//            getActivity().startActivityForResult(i, REQUEST_SCANNER);
//        }
//        else {
//            MyApplication.showToast(getActivity(),"Generating your location. " +
//                    "Please try again after a few seconds",Gravity.CENTER);
//        }
//    }
//
//    private void onScanImageResult(String path) {
//        if(path!=null&&!path.trim().isEmpty()) setImage(path);
//        else Toast.makeText(getActivity(),"Couldn't load image",Toast.LENGTH_SHORT).show();
//
//        setNotice();
//    }
//
//    private void onScanImageResult() {
//        toggleView(ACTION_SHOW_ALL);
//        scanFAW=MyApplication.scanFAWList.get(index);
//        Image image=scanFAW.getImage();
//        if(image!=null&&image.getUri()!=null&&image.getUri().getPath()!=null
//                &&!image.getUri().getPath().trim().isEmpty())
//            setImage(image.getUri().getPath());
//        else {
//            //toggleView(ACTION_HIDE_ALL);
//            //MyApplication.showToast(getActivity(),"No image attached",Gravity.CENTER);
//        }
//
//        if(getActivity()!=null&&getActivity().getResources()!=null)
//                setNotice();
//    }
//
//    private void setNotice(){
//        scanFAW=MyApplication.scanFAWList.get(index);
//       // Log.d(TAG,"setNotice()");
//        tvHeader.setText(scanFAW.getHeader());
//        tvMessage.setText( scanFAW.getMessage());
//        updateNoticeTheme();
//    }
//
//    private void updateNoticeTheme(){
//        switch (scanFAW.getResponse()){
//            case -1: setNoticeTheme(R.color.disabledBlack);tvLocation.setVisibility(View.VISIBLE); break;
//            case 0: setNoticeTheme(R.color.colorAccent); tvLocation.setVisibility(View.GONE); break;
//            case 1: setNoticeTheme(R.color.red); tvLocation.setVisibility(View.GONE); break;
//            default: setNoticeTheme(R.color.disabledBlack); tvLocation.setVisibility(View.GONE); break;
//        }
//    }
//
//    private void setNoticeTheme(int color){
//        tvHeader.setTextColor(getActivity().getResources().getColor(color));
//        tvMessage.setTextColor(getActivity().getResources().getColor(color));
//        ivNotice.setColorFilter(getActivity().getResources().getColor(color));
//    }
//
//    private void setImage(String path){
//        Image image=new Image();
//        image.setImageBitmap(myApplication.bitmapCompressLarge(path));
//        if(image.getImageBitmap()!=null) {
//            image.setImageString(myApplication.encodeBitmap(image.getImageBitmap()));
//            imageView.setImageBitmap(image.getImageBitmap());
//            image.setUri(Uri.parse(path));
//            scanFAW.setImage(image);
//        }
//
//    }
//
//    private void updateScannerStatus(){
//        if(MyApplication.hasLocation()){
//            tvLocation.setText("Scanner Ready!");
//            tvLocation.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPrimary));
//        }
//    }
//    @Override
//    public void onResume() {
//        super.onResume();
//        Log.d(TAG,"onResume()");
//        toggleView(ACTION_SHOW_PROGRESS);
//        if(handler!=null&&runnable!=null)
//        handler.postDelayed(runnable,5000);
//
//    }
//
//
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        if(handler!=null)handler.removeCallbacks(runnable);
//    }
//}