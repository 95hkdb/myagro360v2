package com.hensongeodata.myagro360v2.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Guide;


/**
 * Created by user1 on 8/16/2017.
 */



public class GuideFragment extends BaseFragment {
    private static String TAG= GuideFragment.class.getSimpleName();

    private int guideIndex;
    private NestedScrollView rootView=null;
    private LinearLayout baseView;
    private Guide guide;
    private ImageView imageView;
    private TextView tvHeader;
    private TextView tvContent;

    public GuideFragment newInstance(int position) {
        GuideFragment f = new GuideFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        //Log.d(TAG,"instance: "+position );
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        guideIndex = getArguments() != null ? getArguments().getInt("position") : 1;

        if(MyApplication.guides!=null&&guideIndex<MyApplication.guides.size())
            guide= MyApplication.guides.get(guideIndex);

        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         rootView = (NestedScrollView) inflater.inflate(R.layout.guide_item, container, false);
          imageView = rootView.findViewById(R.id.iv_image);
          tvHeader = rootView.findViewById(R.id.tv_header);
          tvContent = rootView.findViewById(R.id.tv_description);

        if (guide != null) {
        imageView.setImageResource(guide.getImage());
        tvHeader.setText(guide.getHeader());
        tvContent.setText(guide.getContent());
        }

        return rootView;
    }
}
