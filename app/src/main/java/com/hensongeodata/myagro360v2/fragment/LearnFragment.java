package com.hensongeodata.myagro360v2.fragment;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.KnowledgeBase;
import com.halilibo.bettervideoplayer.BetterVideoCallback;
import com.halilibo.bettervideoplayer.BetterVideoPlayer;
import com.hensongeodata.myagro360v2.view.LibraryActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.wang.avi.AVLoadingIndicatorView;


/**
 * Created by user1 on 8/16/2017.
 */



public class LearnFragment extends BaseFragment {
    private static String TAG= LearnFragment.class.getSimpleName();

    private int sectionIndex;
    private View rootView=null;
    private LinearLayout baseView;
    private KnowledgeBase knowledgeBase;
    private LinearLayout vMedia;
    private TextView tvContent;
    private int id;
    private BetterVideoPlayer player;
    private LayoutInflater mInflater;
    private Bitmap mBitmap;
    private Dialog dialogImage;
    private View vEye;

    public LearnFragment newInstance(int position) {
        LearnFragment f = new LearnFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        //Log.d(TAG,"instance: "+position );
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sectionIndex = getArguments() != null ? getArguments().getInt("position") : 1;
          if (LibraryActivity.knowledgeBases != null)
                knowledgeBase = LibraryActivity.knowledgeBases.get(sectionIndex);
        else if(getActivity()!=null)
            getActivity().finish();

        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         rootView =inflater.inflate(R.layout.kb_content, container, false);
         mInflater = LayoutInflater.from(getActivity());

         if(knowledgeBase!=null) {
               vMedia = rootView.findViewById(R.id.v_media);
               tvContent = rootView.findViewById(R.id.tv_content);

             setMedia();


             tvContent.setText(Html.fromHtml(knowledgeBase.getContent()));

             Log.d(TAG, "content: " + knowledgeBase.getContent());

         }

        return rootView;
    }


    private void setMedia(){
        vMedia.setVisibility(View.VISIBLE);

        if(knowledgeBase.getUrl()!=null
                &&!knowledgeBase.getUrl().trim().isEmpty()) {
            if (knowledgeBase.getMedia_type() != null) {
                if (knowledgeBase.getMedia_type().equalsIgnoreCase(KnowledgeBase.MEDIA_VIDEO))
                    setVideo();
                else if (knowledgeBase.getMedia_type().equalsIgnoreCase(KnowledgeBase.MEDIA_IMAGE))
                    setImage();
                else
                    setDefault();
            }
        }
        else
            setDefault();

    }

    private void setVideo(){
        View v=mInflater.inflate(R.layout.kb_video_player_item,null);
        vMedia.addView(v);

          player = rootView.findViewById(R.id.player);
        // player.setAutoPlay(true);
        player.setBottomProgressBarVisibility(true);
        player.setHideControlsOnPlay(false);
        // Sets the callback to this Activity, since it inherits EasyVideoCallback
        player.setCallback(new BetterVideoCallback() {
            @Override
            public void onStarted(BetterVideoPlayer player) {

            }

            @Override
            public void onPaused(BetterVideoPlayer player) {

            }

            @Override
            public void onPreparing(BetterVideoPlayer player) {

            }

            @Override
            public void onPrepared(BetterVideoPlayer player) {

            }

            @Override
            public void onBuffering(int percent) {

            }

            @Override
            public void onError(BetterVideoPlayer player, Exception e) {

            }

            @Override
            public void onCompletion(BetterVideoPlayer player) {

            }

            @Override
            public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {

            }
        });

        // Sets the source to the HTTP URL held in the TEST_URL variable.
        // To play files, you can use Uri.fromFile(new File("..."))
        //player.setSource(Uri.parse(TEST_URL));


        player.setSource(Uri.parse(knowledgeBase.getUrl()));

        player.showControls();
    }

    private void setImage(){
        View v=mInflater.inflate(R.layout.kb_image_item,null);
        vMedia.addView(v);
        vEye=v.findViewById(R.id.iv_eye);

          final ImageView mImage = rootView.findViewById(R.id.image);
          final AVLoadingIndicatorView progress = rootView.findViewById(R.id.av);
        progress.show();

        Picasso.get().load(knowledgeBase.getUrl()).resize(900,900).placeholder(R.drawable.placeholder).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                mBitmap=bitmap;
                mImage.setImageBitmap(bitmap);
                progress.hide();
                mImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        initImageDialog();
                        dialogImage.show();
                    }
                });
                vEye.setVisibility(View.VISIBLE);
                Log.d(TAG,"loaded");
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                progress.hide();
                mImage.setOnClickListener(null);

                Log.d(TAG,"failed");
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                mImage.setImageDrawable(placeHolderDrawable);
                mImage.setOnClickListener(null);
                vEye.setVisibility(View.GONE);
                Log.d(TAG,"prepare");
            }
        });

    }

    private void setFile(){
        View v=mInflater.inflate(R.layout.kb_video_player_item,null);
        vMedia.addView(v);

          player = rootView.findViewById(R.id.player);
        player.setBottomProgressBarVisibility(true);
        player.setHideControlsOnPlay(false);
        player.setCallback(new BetterVideoCallback() {
            @Override
            public void onStarted(BetterVideoPlayer player) {

            }

            @Override
            public void onPaused(BetterVideoPlayer player) {

            }

            @Override
            public void onPreparing(BetterVideoPlayer player) {

            }

            @Override
            public void onPrepared(BetterVideoPlayer player) {

            }

            @Override
            public void onBuffering(int percent) {

            }

            @Override
            public void onError(BetterVideoPlayer player, Exception e) {

            }

            @Override
            public void onCompletion(BetterVideoPlayer player) {

            }

            @Override
            public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {

            }
        });

        // Sets the source to the HTTP URL held in the TEST_URL variable.
        // To play files, you can use Uri.fromFile(new File("..."))
        //player.setSource(Uri.parse(TEST_URL));


        player.setSource(Uri.parse(knowledgeBase.getUrl()));

        player.showControls();
    }

    private void setDefault(){
        View v=mInflater.inflate(R.layout.kb_default_item,null);
        vMedia.addView(v);
        Log.d(TAG,"setDefault");
    }

    //Show textbox to edit username
    private void initImageDialog(){

        dialogImage = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialogImage.setContentView(R.layout.kb_zoomimage);

          SubsamplingScaleImageView mImage = dialogImage.findViewById(R.id.image);
        mImage.setImage(ImageSource.bitmap(mBitmap));

        dialogImage.findViewById(R.id.ib_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogImage.cancel();
            }
        });

        ((TextView)dialogImage.findViewById(R.id.tv_caption)).setText(knowledgeBase.getTitle());


    }

}
