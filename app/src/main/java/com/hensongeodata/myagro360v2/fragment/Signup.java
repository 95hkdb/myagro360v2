package com.hensongeodata.myagro360v2.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.hensongeodata.myagro360v2.Interface.AuthListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SettingsModel;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.view.Chat;
import com.hensongeodata.myagro360v2.view.District;
import com.hensongeodata.myagro360v2.view.PrivacyPolicy;
import com.hensongeodata.myagro360v2.view.SigninSignup;
import com.hensongeodata.myagro360v2.view.VideoPlayerPreview;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by user1 on 10/14/2017.
 */

 public class Signup extends BaseFragmentSimple {
    View rootView;
    private static final String TAG = Signup.class.getSimpleName();

    private MyApplication myApplication;
    private ProgressDialog progressDialog;
    private IntentFilter filter;
    private SignupBroadcastReceiver receiver;
    private String prompt;
    private List<String> types;
    private Spinner spType;

    TextInputEditText etFirstname;
    TextInputEditText etLastname;
    TextInputEditText etEmail;
    TextInputEditText etPassword;
    TextInputEditText etPasswordConfirm;
    TextInputEditText etPhone;
    TextInputEditText etTown;
    TextInputEditText etOrg;
    Button btnSignup;
    CheckBox cbPrivacy;
    TextView tvError;
    String type;
    User mUser;
    private AgroWebAPI mAgroWebAPI;

    NetworkRequest networkRequest;
      private HashMap<Integer, String> activitiesHashMap;

      @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.signup, container, false);
        networkRequest=new NetworkRequest(getActivity());
        networkRequest.setAuthListener(authListener);

        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage(resolveString(R.string.progress_creating_account));
        progressDialog.setCancelable(false);

        mAgroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);
        myApplication=new MyApplication();
        receiver=new SignupBroadcastReceiver();
        filter=new IntentFilter(BroadcastCall.SIGN_UP);
        getActivity().registerReceiver(receiver,filter);

          getRolesFromAPI();

          etFirstname = rootView.findViewById(R.id.et_firstname);
          etLastname = rootView.findViewById(R.id.et_lastname);
          etEmail = rootView.findViewById(R.id.et_email);
          etPassword = rootView.findViewById(R.id.et_password);
          etPasswordConfirm = rootView.findViewById(R.id.et_password_confirm);
          etPhone = rootView.findViewById(R.id.et_phone);
//          etTown = rootView.findViewById(R.id.et_town);
//        etTown.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onAddMapButtonClicked(View view) {
//              chooseTown();
//            }
//        });
          etOrg = rootView.findViewById(R.id.et_organization);

          cbPrivacy = rootView.findViewById(R.id.cb_privacy);
          tvError = rootView.findViewById(R.id.tv_error);

        cbPrivacy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    tvError.setVisibility(View.GONE);
            }
        });

          btnSignup = rootView.findViewById(R.id.btn_signup);
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String firstname = etFirstname.getText().toString().trim();
                String lastname = etLastname.getText().toString().trim();
                String password = etPassword.getText().toString().trim();
                String passwordConfirm = etPasswordConfirm.getText().toString().trim();
                String email = etEmail.getText().toString().trim();
                String phone=etPhone.getText().toString().trim();
//                String town=etTown.getText().toString().trim();

                if (TextUtils.isEmpty(firstname)) {
                    etFirstname.setError(resolveString(R.string.error_field_required));
                    etFirstname.requestFocus();
                    return;
                }

                if (!isNameValid(firstname)) {
                    etFirstname.setError(resolveString(R.string.first_name));
                    etFirstname.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(lastname)) {
                    etLastname.setError(resolveString(R.string.error_field_required));
                    etLastname.requestFocus();
                    return;
                }
                if (!isNameValid(lastname)) {
                    etFirstname.setError("Enter a valid Last Name.");
                    etFirstname.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(email)) {
                    etEmail.setError(resolveString(R.string.error_field_required));
                    etEmail.requestFocus();
                    return;
                }
//                if (TextUtils.isEmpty(etOrg.getText().toString().trim())) {
//                    etOrg.setError("Enter your organization name here.");
//                    etOrg.requestFocus();
//                    return;
//                }
                if (!isEmailValid(email)) {
                    etEmail.setError("Enter a valid Email Address.");
                    etEmail.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    etPassword.setError(resolveString(R.string.error_field_required));
                    etPassword.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(passwordConfirm)) {
                    etPasswordConfirm.setError(resolveString(R.string.error_field_required));
                    etPasswordConfirm.requestFocus();
                    return;
                }

                if (!password.equals(passwordConfirm)) {
                    etPasswordConfirm.setError(resolveString(R.string.error_incorrect_password));
                    etPasswordConfirm.requestFocus();
                    return;
                }


                if (TextUtils.isEmpty(phone)) {
                    etPhone.setError(resolveString(R.string.error_field_required));
                    etPhone.requestFocus();
                    return;
                }
                if (!isPhoneValid(phone)||phone.length()<10) {
                    etPhone.setError("Enter a valid Phone Number.");
                    etPhone.requestFocus();
                    return;
                }

                if (type==null||type.isEmpty()||type.equalsIgnoreCase(prompt)) {
                    MyApplication.showToast(getActivity(),
                            resolveString(R.string.profession)+", "+resolveString(R.string.error_field_required),
                            Gravity.CENTER);
                    spType.requestFocus();
                    return;
                }

//                if (TextUtils.isEmpty(etTown.getText().toString())) {
//                    MyApplication.showToast(getActivity(),
//                            resolveString(R.string.town)+", "+getActivity().getResources().getString(R.string.error_field_required),
//                            Gravity.CENTER);
//                    etTown.setError(resolveString(R.string.error_field_required));
//                    etTown.requestFocus();
//                    return;
//                }

              User user=new User(null);//null implies data wont be saved to sharedprefs
                user.setFirstname(firstname);
                user.setLastname(lastname);
                user.setPassword(password);
                user.setEmail(email);
                user.setPhone(phone);
                user.setType(type);

                user.setOrganization(etOrg.getText().toString().trim());


                if (cbPrivacy.isChecked())
                    doSignup(user);
                else
                    tvError.setVisibility(View.VISIBLE);
            }

        });

        rootView.findViewById(R.id.bg_privacy_policy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                etTown.setError(null);
                startActivity(new Intent(getActivity(),PrivacyPolicy.class));
            }
        });


          spType = rootView.findViewById(R.id.spinner_type);
        rootView.findViewById(R.id.bg_spinner_type).setBackgroundResource(R.drawable.search_bg_dark);
        prompt=getActivity().getResources().getString(R.string.user_type);
//        loadTypes();
//        setSpinner(spType,types);
        spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //Log.d(TAG,"position: "+i);
                if(i!=types.size()-1)
                      type = spType.getItemAtPosition(i).toString().toLowerCase();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        rootView.findViewById(R.id.v_tutorial).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), VideoPlayerPreview.class);
                intent.putExtra("path", Keys.VIDEO_URL_IGEZA);
                intent.putExtra("caption",getActivity().getResources().getString(R.string.watch_tutorial));
                Toast.makeText(getActivity(),getActivity().getResources().getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
               // getActivity().startActivity(intent);
            }
        });

        return rootView;
    }

    private boolean isNameValid(String name) {
        return name.matches( "[a-zA-z]+([ '-][a-zA-Z]+)*" );
    }

    private boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isPhoneValid(String phone) {
        return PhoneNumberUtils.isGlobalPhoneNumber(phone);
    }

    public void doSignup(User user){

        if(myApplication.hasNetworkConnection(getActivity())){
            progressDialog.show();
            networkRequest.signUp(user);
        }
        else {
            if(progressDialog.isShowing())
                progressDialog.hide();

            myApplication.showInternetError(getActivity());
        }
    }


    public void doAuthentication(User user){

        if(myApplication.hasNetworkConnection(getActivity())){
           // progressDialog.setMessage(resolveString(R.string.progress_authenticate_account));
            //networkRequest.registerPush(user);
            authListener.onAuthenticate(NetworkRequest.STATUS_SUCCESS,1);

        }
        else {
            myApplication.showInternetError(getActivity());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== Activity.RESULT_OK){
            if(requestCode==100){//Town name requested
//                etTown.setText(data.getExtras().getString("town_name"));
            }
        }
    }


    class SignupBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            String message=intent.getExtras().getString("date");
            int status=intent.getExtras().getInt("status");

                if(status==NetworkRequest.STATUS_SUCCESS){
                      if (myApplication.hasSession(getActivity())) {
                            doAuthentication(new User(getActivity()));
                            ((SigninSignup)getActivity()).navigateLogin();
                      }
                    else{
                        if(progressDialog.isShowing())
                            progressDialog.cancel();

                        myApplication.showMessage(getActivity(),"No session created. Try again");
                    }

                    }
                else if(status==NetworkRequest.STATUS_FAIL){
                    if(message==null||message.trim().isEmpty())
                    myApplication.showMessage(getActivity(),"No response from server. Try again");
                else
                    myApplication.showMessage(getActivity(),message);

                    if(progressDialog.isShowing())
                        progressDialog.cancel();
                }
        }
    }


    AuthListener authListener=new AuthListener() {
        @Override
        public void onAuthenticate(int status, int officer) {
            if(status==NetworkRequest.STATUS_SUCCESS){
                  Intent i = new Intent(getActivity(), SigninSignup.class);
//                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (progressDialog.isShowing())
                    progressDialog.cancel();

                if(officer!=-1) {
                    (new User(getActivity())).setOfficer(officer==1);
                    Toast.makeText(getActivity(), resolveString(R.string.success), Toast.LENGTH_SHORT).show();
                    (new SettingsModel(getActivity())).setArchive_mode(false);

                    User user=new User(getActivity());
                    user.setChatroomActive_id(null);
                    user.setProfession_id(null);
                    String profession_id=Chat.getChatroom_id(getActivity(),type);
//                    Log.d("Signup","type: "+type+" id: "+profession_id);
                    user.setProfession_id(profession_id);
//                    Log.d("Signup: ", "Profes_id: "+user.getProfession_id());
                      myApplication.logout(getActivity());
                    startActivity(i);
                    getActivity().finish();
                }
                else {
                    myApplication.logout(getActivity());
                    myApplication.showMessage(getActivity(),resolveString(R.string.failed));
                }
            }
            else {
                myApplication.logout(getActivity());
                myApplication.showMessage(getActivity(),resolveString(R.string.failed));
                if(progressDialog.isShowing())
                    progressDialog.cancel();
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(receiver!=null)
            getActivity().unregisterReceiver(receiver);
        if(networkRequest!=null)
        networkRequest.close();
    }

    private void loadTypes(){
//        types=new ArrayList<>();
//        types.add(getActivity().getResources().getString(R.string.coordinator));
//        types.add(getActivity().getResources().getString(R.string.farmer));
//        types.add(getActivity().getResources().getString(R.string.seed_dealer));
//        types.add(getActivity().getResources().getString(R.string.fertilizer_dealer));
//        types.add(getActivity().getResources().getString(R.string.fbo));
//        types.add(getActivity().getResources().getString(R.string.insurance));
//        types.add(getActivity().getResources().getString(R.string.funder));
//        types.add(getActivity().getResources().getString(R.string.agronomist));
//        types.add(getActivity().getResources().getString(R.string.soil_scientist));
//        types.add(getActivity().getResources().getString(R.string.pathologist));
//        types.add(getActivity().getResources().getString(R.string.entomologist));
//        types.add(getActivity().getResources().getString(R.string.food_scientist));
//        types.add(getActivity().getResources().getString(R.string.post_harvest_tech));
//        types.add(getActivity().getResources().getString(R.string.nematologist));
//        types.add(getActivity().getResources().getString(R.string.wholesaler));
//        types.add(getActivity().getResources().getString(R.string.retailer));
//        types.add(getActivity().getResources().getString(R.string.mofa));
//        types.add(getActivity().getResources().getString(R.string.tech_support));

        types.add(prompt);
    }

    private void chooseTown(){
        startActivity(new Intent(getActivity(), District.class));
    }

    private void setSpinner(Spinner spinner, List<String> list){
        if(list!=null) {
            final int listsize = list.size() - 1;
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list) {
                @Override
                public int getCount() {
                    return (listsize); // Truncate the list
                }
            };

            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapter);
            spinner.setPrompt(prompt);
            spinner.setFocusable(true);
            spinner.setSelection(listsize);
            spinner.setEnabled(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        etTown.setText(District.town);
    }

      private void getRolesFromAPI() {

            mAgroWebAPI.fetchRolesCall()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JsonElement>() {
                          @Override
                          public void onSubscribe(Disposable d) {

                          }

                          @Override
                          public void onNext(JsonElement jsonElement) {

                                JsonArray bodyJsonArray =  jsonElement.getAsJsonObject().
                                        getAsJsonArray("scans");

                                types=new ArrayList<>();

                                for( int i = 0; i <  bodyJsonArray.size(); i++) {
                                      String label =  bodyJsonArray.get(i).getAsJsonObject().get("label").getAsString();
                                      Log.i(TAG, "onNext: " + label);
                                      types.add(label);
                                }

                                types.add(prompt);
                          }

                          @Override
                          public void onError(Throwable e) {

                          }

                          @Override
                          public void onComplete() {
                                setSpinner(spType,types);
                          }
                    });

      }


}