package com.hensongeodata.myagro360v2.fragment;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.view.MainActivity;


/**
 * Created by user1 on 8/16/2017.
 */



public class FormFragment extends BaseFragment {
    private static String TAG= FormFragment.class.getSimpleName();

    private int sectionIndex;
    private NestedScrollView rootView=null;
    private LinearLayout baseView;
    private Runnable runnable;
    private Handler handler;
    private Button btnNext;


    public FormFragment newInstance(int position) {
        FormFragment f = new FormFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        //Log.d(TAG,"instance: "+position );
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sectionIndex = getArguments() != null ? getArguments().getInt("position") : 1;
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         rootView = (NestedScrollView) inflater.inflate(R.layout.form, container, false);
          baseView = rootView.findViewById(R.id.container);
          btnNext = rootView.findViewById(R.id.btn_next);
        //if there are no children attached to rootview yet
        if(baseView.getChildCount()==0)
            generateForm(baseView, sectionIndex);

        btnNext.setText(((MainActivity)getActivity()).getBtnText(sectionIndex));
//        btnNext.setText(((FormActivity)getActivity()).getBtnText(sectionIndex));
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).next(sectionIndex);
//                ((FormActivity)getActivity()).next(sectionIndex);
            }
        });
        return rootView;
    }



   public void setBtnNextListener(View.OnClickListener listener){
        btnNext.setOnClickListener(listener);
   }

   public void setBtnNextText(String action){
       btnNext.setText(action);
   }


    public boolean validate(){
        clearAllErrors(baseView,sectionIndex);//clear all error texts displayed on the form
       return validateForm(baseView,sectionIndex);
      }
}
