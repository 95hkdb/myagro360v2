package com.hensongeodata.myagro360v2.fragment;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.MyState;

/**
 * Created by user1 on 9/12/2017.
 */

public class CligisFragment extends Fragment {
    View rootView;
    DatabaseHelper databaseHelper;
    MyState myState;
    MyApplication myApplication;

    WebView webView;
    View maploading;
    View mapRetry;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.cligis, container, false);
        databaseHelper=new DatabaseHelper(getActivity());
        myState=new MyState(getActivity());
        myApplication=new MyApplication();

        maploading=rootView.findViewById(R.id.bg_loading);
        maploading.setAnimation(myApplication.blinkAnim());

        mapRetry=rootView.findViewById(R.id.bg_retry);
        mapRetry.setVisibility(View.GONE);
        mapRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reloadMap();
            }
        });

          webView = rootView.findViewById(R.id.webview);
        webView.setVisibility(View.GONE);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);

        webView.loadUrl("http://www.silapha.com");
        webView.setWebViewClient(new WebViewClient() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.getUrl().toString());
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                maploading.clearAnimation();
                maploading.setVisibility(View.GONE);
                mapRetry.setVisibility(View.GONE);
                webView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                mapRetry.setVisibility(View.VISIBLE);
                webView.setVisibility(View.GONE);
                maploading.setVisibility(View.GONE);
                maploading.setAnimation(myApplication.blinkAnim());
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                mapRetry.setVisibility(View.VISIBLE);
                webView.setVisibility(View.GONE);
                maploading.setVisibility(View.GONE);
                maploading.setAnimation(myApplication.blinkAnim());
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                mapRetry.setVisibility(View.VISIBLE);
                webView.setVisibility(View.GONE);
                maploading.setVisibility(View.GONE);
                maploading.setAnimation(myApplication.blinkAnim());
            }
        });


        return rootView;
    }

    private void reloadMap(){
        mapRetry.setVisibility(View.GONE);
        webView.setVisibility(View.GONE);
        maploading.setVisibility(View.VISIBLE);
        webView.reload();
    }

}
