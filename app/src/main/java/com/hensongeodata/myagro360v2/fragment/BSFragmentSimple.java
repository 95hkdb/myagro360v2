package com.hensongeodata.myagro360v2.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.view.BaseActivity;

/**
 * Created by user1 on 9/12/2017.
 */

public class BSFragmentSimple extends BaseFragment{
    private static String TAG=BSFragmentSimple.class.getSimpleName();
    public static String ACTION_LOCATION="location";


    View rootView;
    TextView bsAccuracy;
    TextView bsLongitude;
    TextView bsLatitude;



    private BSFragmentReceiver receiver;
    private IntentFilter filter;

    private BottomSheetBehavior mBottomSheetBehavior;
    private ImageButton ibBSHandle;
    private boolean bsOpened=false;
    private POI poi;
    private static POI cachedPOI;
    private static POI currentPOI;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.bottomsheet_item_simple, container, false);

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseActivity.startLocationOffline(getActivity());
            }
        });
        receiver=new BSFragmentReceiver();
        filter=new IntentFilter(BroadcastCall.BSFRAGMENT);
        getActivity().registerReceiver(receiver,filter);

        initPOIBootomSheetView(MyApplication.poi);

//        (rootView.findViewById(R.id.ib_addpoint)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onAddMapButtonClicked(View view) {
//               // MyApplication.poi=MyApplication.getFakePOI();
//                POI poi=MyApplication.poi;
//                if(poi.getAccuracy()!=null) {
//                    if(!existsPOI(poi)) {
//                        int maxAccuracy = (new SettingsModel(getActivity())).getAccuracy_max();
//                        if (Integer.parseInt(poi.getAccuracy()) <= maxAccuracy) {
//                            poi = new POI(null, poi.getLon(), poi.getLat(), poi.getAlt(), poi.getAccuracy());
//                            showPop(poi, ACTION_ADD_POI);
//                        } else
//                            Toast.makeText(getActivity(), "Please ensure your accuracy is less or equal to " + maxAccuracy, Toast.LENGTH_SHORT).show();
//                    }
//                    else
//                        Toast.makeText(getActivity(), "Way point instance already saved.", Toast.LENGTH_SHORT).show();
//                }
//                    else {
//                    Toast.makeText(getActivity(),"No Way point available.",Toast.LENGTH_SHORT).show();
//                }
//            }
//        });


        return rootView;
    }

    public POI getPoi(){
        //BaseActivity.startLocationOffline(getActivity());
        if(currentPOI!=null){
            if(!existsPOISimple(currentPOI))
                return currentPOI;
            else
                MyApplication.showToast(getActivity(),"Way point instance saved already.", Gravity.CENTER);

            cachedPOI=currentPOI;
        }
        else {
            MyApplication.showToast(getActivity(),"No way point recorded yet.",Gravity.CENTER);
        }

        return  null;
    }

    private boolean existsPOISimple(POI poi){
    boolean exist=false;
        if(cachedPOI!=null){
            if(cachedPOI.getAccuracy().equalsIgnoreCase(poi.getAccuracy())
                    &&cachedPOI.getLat().equalsIgnoreCase(poi.getLat())
                    &&cachedPOI.getLon().equalsIgnoreCase(poi.getLon())
                    &&cachedPOI.getAlt().equalsIgnoreCase(poi.getAlt()))
                exist=true;
        }
        else exist=false;

        return exist;
    }

    private boolean existsPOI(POI poi){
        boolean exist=false;
        if(MyApplication.poiGroup.getPoiList()!=null
                &&MyApplication.poiGroup.getPoiList().size()==0){
            cachedPOI=null;
            return false;
        }

        if(cachedPOI!=null){
            if(cachedPOI.getAccuracy().equalsIgnoreCase(poi.getAccuracy())
                    &&cachedPOI.getLat().equalsIgnoreCase(poi.getLat())
                    &&cachedPOI.getLon().equalsIgnoreCase(poi.getLon())
                    &&cachedPOI.getAlt().equalsIgnoreCase(poi.getAlt()))
                exist=true;
        }
        else exist=false;

        return exist;
    }

    private void initPOIBootomSheetView(POI poi){

          bsAccuracy = rootView.findViewById(R.id.tv_value);
        bsAccuracy.setTextColor(ContextCompat.getColor(getActivity(),R.color.primaryWhite));

          bsLongitude = rootView.findViewById(R.id.tv_longitude);
        bsLongitude.setTextColor(ContextCompat.getColor(getActivity(),R.color.primaryWhite));

          bsLatitude = rootView.findViewById(R.id.tv_latitude);
        bsLatitude.setTextColor(ContextCompat.getColor(getActivity(),R.color.primaryWhite));

        setBottomSheetPOIValues(poi);//demo purposes
    }

    private void setBottomSheetPOIValues(POI poi){
        Log.d(TAG,"BS lon: "+poi.getLon()+" lat: "+poi.getLat()+" alt: "+poi.getAlt());
        String accuracy=poi.getAccuracy();
        if(accuracy!=null&&!accuracy.trim().equalsIgnoreCase("null")) {
            bsAccuracy.setText(accuracy+"m");
            bsAccuracy.setTextColor((new BaseActivity()).getAccuracyColor(getActivity(),Integer.parseInt(accuracy)));
            bsLongitude.setText(poi.getLon());
            bsLatitude.setText(poi.getLat());
        }

        currentPOI=poi;

    }

    class BSFragmentReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
                String action = intent.getExtras().getString("action");
                     int status = intent.getExtras().getInt("status");
            Log.d(TAG,"action: "+action);
            if(action.equals(ACTION_LOCATION)) {
                if (status == NetworkRequest.STATUS_SUCCESS) {
                    setBottomSheetPOIValues(MyApplication.poi);
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(receiver!=null)
                getActivity().unregisterReceiver(receiver);
    }

}
