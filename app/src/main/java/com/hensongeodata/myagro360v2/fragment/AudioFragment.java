package com.hensongeodata.myagro360v2.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.hensongeodata.myagro360v2.R;


/**
 * Created by user1 on 8/16/2017.
 */



public class AudioFragment extends BaseFragment {
    private static String TAG= AudioFragment.class.getSimpleName();
    private int sectionIndex;
    private View rootView=null;
    private LinearLayout baseView;
    private int id;
    //private JcPlayerView audioPlayer;


    public AudioFragment newInstance(int position) {
        AudioFragment f = new AudioFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        //Log.d(TAG,"instance: "+position );
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sectionIndex = getArguments() != null ? getArguments().getInt("position") : 1;
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         rootView =inflater.inflate(R.layout.frag_audio, container, false);
       // audioPlayer= (JcPlayerView) rootView.findViewById(R.id.player_audio);

        return rootView;
    }

    public void playAudio(String title,String url){
        if(url==null)
            url="http://www.noiseaddicts.com/samples_1w72b820/2558.mp3";

       // audioPlayer.playAudio(JcAudio.createFromURL(title,url));

    }

}
