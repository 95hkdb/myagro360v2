package com.hensongeodata.myagro360v2.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.view.BaseActivity;

/**
 * Created by user1 on 9/12/2017.
 */

public class BSFragmentFixed extends BaseFragment{
    private static String TAG=BSFragmentFixed.class.getSimpleName();
    private static String ACTION_DIMENSION="dimension";
    private static String ACTION_LOCATION="location";

    private View rootView;
    private TextView bsAccuracy;

    private BSFragmentReceiver receiver;
    private IntentFilter filter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.bottomsheet_item_fixed, container, false);

        receiver=new BSFragmentReceiver();
        filter=new IntentFilter(BroadcastCall.BSFRAGMENT);
        getActivity().registerReceiver(receiver,filter);

        initPOIBootomSheetView(MyApplication.poi);
        myApplication.selectPOIGroup(getActivity(),databaseHelper);
        return rootView;
    }



    private void initPOIBootomSheetView(POI poi){

          bsAccuracy = rootView.findViewById(R.id.tv_value);
        ((TextView)(rootView.findViewById(R.id.tv_tag))).setText("ACCURACY");
        bsAccuracy.setTextColor(ContextCompat.getColor(getActivity(),R.color.primaryWhite));
        ((TextView)(rootView.findViewById(R.id.tv_tag))).setTextColor(ContextCompat.getColor(getActivity(),R.color.secondaryWhite));

        setBottomSheetPOIValues(poi);//demo purposes
    }

    private void setBottomSheetPOIValues(POI poi){
        String accuracy=poi.getAccuracy();
        if(accuracy!=null&&!accuracy.trim().equalsIgnoreCase("null")) {
            bsAccuracy.setText(accuracy+"m");
            bsAccuracy.setTextColor((new BaseActivity()).getAccuracyColor(getActivity(),Integer.parseInt(accuracy)));
        }
    }

    class BSFragmentReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
                String action = intent.getExtras().getString("action");
                     int status = intent.getExtras().getInt("status");
            Log.d(TAG,"action: "+action);
            if(action.equals(ACTION_LOCATION)) {
                if (status == NetworkRequest.STATUS_SUCCESS) {
                    setBottomSheetPOIValues(MyApplication.poi);
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(receiver!=null)
                getActivity().unregisterReceiver(receiver);
    }

}
