package com.hensongeodata.myagro360v2.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.hensongeodata.myagro360v2.Interface.LocalListener;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.SettingsModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 10/14/2017.
 */
 public class LangFragment extends Fragment {
    private static String TAG=LangFragment.class.getSimpleName();
    View rootView;
    private SettingsModel settings;
    private String prompt;
    private Spinner spLang;
    private List<String> languages;
    public static LocalListener localListener;
    private static int mPosition=-1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.language, container, false);
        settings=new SettingsModel(getActivity());
          spLang = rootView.findViewById(R.id.spinner);
        languages=new ArrayList<>();

        prompt=getActivity().getResources().getString(R.string.prompt);
        loadLanguage();
        setSpinner(spLang,languages);

        spLang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG,"position: "+i);
                String selection=spLang.getItemAtPosition(i).toString().toLowerCase();
                Log.d(TAG,"selection: "+selection);

                    if (i != languages.size() - 1) {
                        if(mPosition!=i) {
                            settings.setLang(selection);
                            Log.d(TAG, "set language");
                            if (localListener != null) {
                                Log.d(TAG, "update localization");
                                mPosition = i;
                                localListener.onLanguageChanged(settings.getLang());
                            }
                        }
//                        else
//                            mPosition=languages.size()-1;
                    }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return rootView;
    }

    private void loadLanguage(){
       for(String language: getLangList()){
           languages.add(language.toUpperCase());
       }

       languages.add(prompt);
    }

    private void setSpinner(Spinner spinner, List<String>list){
            if(list!=null) {
                 final int listsize = list.size() - 1;
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list) {
                @Override
                public int getCount() {
                    return (listsize); // Truncate the list
                }
                };

                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(dataAdapter);
                spinner.setPrompt(prompt);
                spinner.setFocusable(true);
                spinner.setSelection(listsize);
                spinner.setEnabled(true);
            }
        }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private ArrayList<String> getLangList(){
            ArrayList<String> langs=new ArrayList<>();
            langs.add(getActivity().getResources().getString(R.string.english));
//            langs.add(getActivity().getResources().getString(R.string.french));
//            langs.add(getActivity().getResources().getString(R.string.swahili));

            return langs;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
}