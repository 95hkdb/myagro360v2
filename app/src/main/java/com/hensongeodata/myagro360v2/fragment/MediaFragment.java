package com.hensongeodata.myagro360v2.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.hensongeodata.myagro360v2.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by user1 on 9/12/2017.
 */

public class MediaFragment extends BaseFragment {
    private static String TAG=MediaFragment.class.getSimpleName();
    protected View baseView;
    private String userChoosenTask;
    public static String ACTION_HIDE_ALL="hide_all";
    public static String ACTION_SHOW_ALL="show_all";
    public static String ACTION_SHOW_PROGRESS="show_progress";
    protected int globalMediaIndex;
    protected View wrapper;
    protected FloatingActionButton fabCapture;
    protected ImageButton ibAction;

    protected ImageView imageView;
    protected AVLoadingIndicatorView progressBar;
    protected String answer;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle=this.getArguments();
        if(bundle!=null) {
            answer = bundle.getString("answer", null);
        }

        Log.d(TAG,"onCreate");
    }

//    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        baseView = inflater.inflate(R.layout.frag_media, container, false);
        //checkPermission(getActivity());
        wrapper=findView(wrapper,R.id.bg_wrapper);
        fabCapture= (FloatingActionButton) findView(fabCapture,R.id.fab_capture);
        ibAction= (ImageButton) findView(ibAction,R.id.ib_action);
        imageView= (ImageView) findView(imageView,R.id.iv_image);
        progressBar= (AVLoadingIndicatorView) findView(progressBar,R.id.avl);

        return baseView;
    }

    private View findView(View v,int id){
        v=baseView.findViewById(id);
        return v;
    }


    protected void toggleView(String action){
        if(action!=null) {
            if (action.equalsIgnoreCase(ACTION_SHOW_PROGRESS)) {
                wrapper.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                progressBar.show();
            } else if (action.equalsIgnoreCase(ACTION_SHOW_ALL)) {
                wrapper.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.VISIBLE);
                ibAction.setVisibility(View.VISIBLE);
                progressBar.hide();
                progressBar.setVisibility(View.GONE);
            } else if (action.equalsIgnoreCase(ACTION_HIDE_ALL)) {
                wrapper.setVisibility(View.GONE);
                ibAction.setVisibility(View.GONE);
                imageView.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                progressBar.hide();
            }
        }
    }

    protected Bitmap createThumbnailFromPath(String filePath){
        Log.d(TAG,"filePath: "+filePath+" thumbnail:"+ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Images.Thumbnails.MICRO_KIND));
        return  BitmapFactory.decodeFile(filePath);
        //return ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Images.Thumbnails.MICRO_KIND);
    }

    protected static File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Locsmman Pro");

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");
    }


//    public static boolean checkPermission(final Activity activity){
//        //Check for read permissions
//        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);
//                alertBuilder.setCancelable(true);
//                alertBuilder.setTitle("Permission necessary");
//                alertBuilder.setMessage("External storage READ permission is necessary");
//                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onAddMapButtonClicked(DialogInterface dialog, int which) {
//                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
//                    }
//                });
//                AlertDialog alert = alertBuilder.create();
//                alert.show();
//            } else {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
//            }
//        }
//        //Check for write permissions
//        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);
//                alertBuilder.setCancelable(true);
//                alertBuilder.setTitle("Permission necessary");
//                alertBuilder.setMessage("External storage WRITE permission is necessary");
//                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onAddMapButtonClicked(DialogInterface dialog, int which) {
//                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Utility.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
//                    }
//                });
//                AlertDialog alert = alertBuilder.create();
//                alert.show();
//            } else {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Utility.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
//            }
//
//            return false;
//        }
//        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
//                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);
//                alertBuilder.setCancelable(true);
//                alertBuilder.setTitle("Permission necessary");
//                alertBuilder.setMessage("Camera access required");
//                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onAddMapButtonClicked(DialogInterface dialog, int which) {
//                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, Utility.MY_PERMISSIONS_REQUEST_CAMERA);
//                    }
//                });
//                AlertDialog alert = alertBuilder.create();
//                alert.show();
//            } else {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, Utility.MY_PERMISSIONS_REQUEST_CAMERA);
//            }
//
//            return false;
//        }
//        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.RECORD_AUDIO)) {
//                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);
//                alertBuilder.setCancelable(true);
//                alertBuilder.setTitle("Permission necessary");
//                alertBuilder.setMessage("Grant audio recording capabilities");
//                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onAddMapButtonClicked(DialogInterface dialog, int which) {
//                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.RECORD_AUDIO}, Utility.MY_PERMISSIONS_RECORD_AUDIO);
//                    }
//                });
//                AlertDialog alert = alertBuilder.create();
//                alert.show();
//            } else {
//                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.RECORD_AUDIO}, Utility.MY_PERMISSIONS_RECORD_AUDIO);
//            }
//
//            return false;
//        }
//        else {
//            return true;
//        }
//    }


    public static Bitmap uriToBitmap(Context context,Uri selectedFileUri) {
        Bitmap bitmap=null;
        if(selectedFileUri==null||selectedFileUri.toString().trim().isEmpty())
            return null;

        try {
            ParcelFileDescriptor parcelFileDescriptor =
                    context.getContentResolver().openFileDescriptor(selectedFileUri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            bitmap= BitmapFactory.decodeFileDescriptor(fileDescriptor);

            parcelFileDescriptor.close();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return bitmap;
    }

}
