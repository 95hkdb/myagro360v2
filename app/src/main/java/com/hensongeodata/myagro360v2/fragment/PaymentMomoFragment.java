package com.hensongeodata.myagro360v2.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Pay;

import java.util.UUID;

import gh.com.payswitch.thetellerandroid.ghmobilemoney.GhMobileMoneyFragment;
import gh.com.payswitch.thetellerandroid.ghmobilemoney.GhMobileMoneyPresenter;
import gh.com.payswitch.thetellerandroid.thetellerManager;


public class PaymentMomoFragment extends BaseFragment {
    private static String TAG= PaymentMomoFragment.class.getSimpleName();
    private View rootView;
    GhMobileMoneyPresenter ghMobileMoneyPresenter;
    Pay pay;
    public PaymentMomoFragment newInstance(int position) {
        PaymentMomoFragment f = new PaymentMomoFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        //Log.d(TAG,"instance: "+position );
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         rootView = inflater.inflate(R.layout.payment_momo, container, false);

         ghMobileMoneyPresenter = new GhMobileMoneyPresenter(getActivity(), new GhMobileMoneyFragment());

         new thetellerManager(getActivity()).setAmount(Double.parseDouble(String.valueOf(pay.amountDue)))
                 .setEmail(user.getEmail())
                .setfName(user.getFirstname())
                .setlName(user.getLastname())
                .setApiKey("MWVjZDQ4MDA3M2VlYzQ1N2I5MWM2YzViMmZkZjZkMWM=")
                .setTxRef(generateTxRef())
                .acceptGHMobileMoneyPayments(true)
                .allowSaveCardFeature(true)
                .initialize();

        return rootView;
    }

    private String generateTxRef(){
        return String.valueOf(UUID.randomUUID()).substring(0,10);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
