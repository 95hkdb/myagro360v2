package com.hensongeodata.myagro360v2.newmodel;

public class PostContent {

      private int id;
      private String date;
      private String title;
      private String content;

      public PostContent() {
      }

      public PostContent(int id, String date, String title, String content) {
            this.id = id;
            this.date = date;
            this.title = title;
            this.content = content;
      }

      public int getId() {
            return id;
      }

      public String getDate() {
            return date;
      }

      public String getTitle() {
            return title;
      }

      public String getContent() {
            return content;
      }
}
