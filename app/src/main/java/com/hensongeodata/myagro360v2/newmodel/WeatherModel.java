package com.hensongeodata.myagro360v2.newmodel;

import com.google.gson.annotations.SerializedName;

public class WeatherModel {

      @SerializedName("LocationName")
      private String locationName;
      @SerializedName("WeatherMain")
      private String weatherMain;
      @SerializedName("WeatherDescription")
      private String weatherDescription;
      @SerializedName("Temperature")
      private double temperature;
      @SerializedName("Pressure")
      private String pressure;
      @SerializedName("Humidity")
      private String humidity;

      public WeatherModel() {
      }

      public WeatherModel(String locationName, String weatherMain, String weatherDescription, double temperature, String pressure, String humidity) {
            this.locationName = locationName;
            this.weatherMain = weatherMain;
            this.weatherDescription = weatherDescription;
            this.temperature = temperature;
            this.pressure = pressure;
            this.humidity = humidity;
      }


      public String getLocationName() {
            return locationName;
      }

      public void setLocationName(String locationName) {
            this.locationName = locationName;
      }

      public String getWeatherMain() {
            return weatherMain;
      }

      public void setWeatherMain(String weatherMain) {
            this.weatherMain = weatherMain;
      }

      public String getWeatherDescription() {
            return weatherDescription;
      }

      public void setWeatherDescription(String weatherDescription) {
            this.weatherDescription = weatherDescription;
      }

      public double getTemperature() {
            return temperature;
      }

      public void setTemperature(double temperature) {
            this.temperature = temperature;
      }

      public String getPressure() {
            return pressure;
      }

      public void setPressure(String pressure) {
            this.pressure = pressure;
      }

      public String getHumidity() {
            return humidity;
      }

      public void setHumidity(String humidity) {
            this.humidity = humidity;
      }


}
