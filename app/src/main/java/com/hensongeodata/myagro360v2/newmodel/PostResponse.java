package com.hensongeodata.myagro360v2.newmodel;

import com.google.gson.annotations.SerializedName;

public class PostResponse {

      @SerializedName("image")
      private String post_image;

      @SerializedName("creator")
      private String post_creator;

      @SerializedName("content")
      private PostContent postContent;

      public PostResponse() {
      }

      public PostResponse(String post_image, String post_creator, PostContent postContent) {
            this.post_image = post_image;
            this.post_creator = post_creator;
            this.postContent = postContent;
      }

      public String getPost_image() {
            return post_image;
      }

      public String getPost_creator() {
            return post_creator;
      }

      public PostContent getPostContent() {
            return postContent;
      }
}
