package com.hensongeodata.myagro360v2.newmodel;

public class ActivityModel {

      private boolean error;
      private String error_msg;

      public ActivityModel() {
      }

      public ActivityModel(boolean error, String error_msg) {
            this.error = error;
            this.error_msg = error_msg;
      }

      public boolean isError() {
            return error;
      }

      public String getError_msg() {
            return error_msg;
      }
}
