package com.hensongeodata.myagro360v2.model;

import org.json.JSONArray;

import java.util.ArrayList;

public class FormFarmMappingResponse {
    public JSONArray jsonArray;
    public ArrayList<String> headers;
}
