package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 1/19/2018.
 */

public class POI {
    private int id;
    public String id_str;
    public String name="Untitled";
    public String lon;
    public String lat;
    public String alt;
    public String accuracy;
    public String transaction_id;

    public POI(){}

    public POI(String name, String lon, String lat, String alt, String accuracy) {
        this.name = name;
        this.lon = lon;
        this.lat = lat;
        this.alt=alt;
        this.accuracy = accuracy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }
}
