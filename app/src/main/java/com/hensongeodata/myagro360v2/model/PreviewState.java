package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 2/13/2018.
 */

public class PreviewState {
    //private int index;
    private boolean viewed=false;


    public PreviewState(boolean viewed) {
        //this.index = index;
        this.viewed = viewed;
    }

    public boolean isViewed() {
        return viewed;
    }

    public void setViewed(boolean viewed) {
        this.viewed = viewed;
    }
}
