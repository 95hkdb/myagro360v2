package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 9/27/2017.
 */

public class Town {
    private String id;
    private String name;
    private String district_id;

    public Town(){}

    public Town(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(String district_id) {
        this.district_id = district_id;
    }
}
