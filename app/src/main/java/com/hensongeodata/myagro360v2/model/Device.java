package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 3/2/2018.
 */

public class Device {
    private String name;
    private String androidVersion;
    private String serial;
    private String model;

    public Device(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
