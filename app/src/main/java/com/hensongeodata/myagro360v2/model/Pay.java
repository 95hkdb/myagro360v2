package com.hensongeodata.myagro360v2.model;

import org.json.JSONObject;

public class Pay {
    public long amountDue;
    public User user;
    public String cardName;
    public String cardNumber;
    public String cardType;
    public String yearExpiration;
    public String monthExpiration;
    public String cvv;
    public JSONObject products;
}
