package com.hensongeodata.myagro360v2.model;

import java.util.ArrayList;

/**
 * Created by user1 on 1/25/2018.
 */

public class DropdownField extends Field {

    private ArrayList<String> options;


    public ArrayList<String> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<String> options) {
        this.options = options;
    }
}
