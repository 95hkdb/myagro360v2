package com.hensongeodata.myagro360v2.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.view.Chat;

import java.util.ArrayList;

/**
 * Created by user1 on 10/30/2017.
 */

public class User {
    private String id;
    private String hgt_user_id;
    private String orgID;
    private String firstname;
    private String lastname;
    private String push_id;
    private String email;
    private String phone;
    private String password;
    private SharedPreferences sharedPreferences;
    private String workarea;
    private String neighbourhood;
    private Boolean firstTime;
    private String town_id;//town id of most recent selected town/workarea
    private boolean officer;
    private String type;
    private String organization;
    private String chatroomActive_id;
    private String profession_id;
    private ArrayList<String> chatRoom_ids;
    Context context;

    public User(Context context){
        if(context!=null)
            sharedPreferences=context.getSharedPreferences("MyPrefs",0);

        this.context=context;
    }

    public User(Context context, String id, String firstname, String lastname, String push_id, String email, String phone, String password, boolean verified) {
        sharedPreferences=context.getSharedPreferences(Keys.PREFS,0);
        this.id = id;
        this.firstname = firstname;
        this.lastname=lastname;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.push_id=push_id;
    }

    public String getType() {
        if(context!=null)
            type=getString("user_type");

        return type;
    }

    public void setType(String type) {
        if(context!=null)
            saveString("user_type",type);

        this.type = type;
    }


    public String getProfession_id() {
        Log.d("User","getProf");
        if(context!=null)
           profession_id=getString("profession_id");

        return profession_id;
    }

    public void setProfession_id(String profession_id) {

        if(context!=null) {
            Log.d("User","pro_id: "+getString("profession_id")+" context: "+context);

            saveString("profession_id", profession_id);
        }

        this.profession_id = profession_id;
    }

    public ArrayList<String> getChatRoom_ids() {
       chatRoom_ids =new ArrayList<>();
        if(context!=null){
            chatRoom_ids.add(Chat.getChatroom_id(context,context.getResources().getString(R.string.community_title)));

            if(getProfession_id()!=null&&!getProfession_id().trim().isEmpty())
                chatRoom_ids.add(getProfession_id());
        }
        return chatRoom_ids;
    }

   /* public void setChatRooms(ArrayList<String> chatRoom_ids) {
        for(String chatRoom: chatRoom_ids){
            saveString("chatroom1",chatRoom);
        }
        this.chatRoom_ids = chatRoom_ids;
    }*/

    public String getOrganization() {
        if(context!=null)
            organization=getString("organization");
        return organization;
    }

    public void setOrganization(String organization) {
        if(context!=null)
            saveString("organization",organization);

        this.organization = organization;
    }


    public String getId() {
        if(context!=null)
            id=getString("id");

        return id;
    }

    public void setId(String id) {
        if(context!=null)
            saveString("id",id);

        this.id = id;
    }


    public String getChatroomActive_id() {
        if(context!=null)
            chatroomActive_id =getString("chatroomActive_id");
/*
        if(chatroomActive_id==null||chatroomActive_id.trim().isEmpty())
            chatroomActive_id=MessageGroup.NAME_IGEZA_COMMUNITY;*/

        return chatroomActive_id;
    }

    public void setChatroomActive_id(String id) {
        if(context!=null)
            saveString("chatroomActive_id",id);

        this.chatroomActive_id = id;
    }

    public String getHgt_user_id() {
        if(context!=null)
            hgt_user_id=getString("hgt_id");

        return hgt_user_id;
    }

    public void setHgt_user_id(String hgt_user_id) {
        if(context!=null)
            saveString("ghgt_id",hgt_user_id);

        this.hgt_user_id = hgt_user_id;
    }


    public String getPush_id() {
        if(context!=null)
            push_id=getString("token");

        return push_id;
    }

    public void setPush_id(String token) {
        if(context!=null)
            saveString("token",token);

        this.push_id = token;
    }


    public String getOrgID() {
        if(context!=null)
            orgID=getString("orgID");

        return orgID;
    }

    public void setOrgID(String orgID) {
        if(context!=null)
            saveString("orgID",orgID);

        this.orgID = orgID;
    }

    public String getFirstname() {
        if(context!=null)
            firstname =getString("firstname");

        return firstname;
    }

    public void setFirstname(String firstname) {
        if(context!=null)
            saveString("firstname", firstname);

        this.firstname = firstname;
    }


    public String getLastname() {
        if(context!=null)
            lastname =getString("lastname");

        return lastname;
    }

    public void setLastname(String lastname) {
        if(context!=null)
            saveString("lastname", lastname);

        this.lastname = lastname;
    }

    public String getEmail() {
        if(context!=null)
        email=getString("email");
        return email;
    }

    public void setEmail(String email) {
        if(context!=null)
        saveString("email",email);
        this.email = email;
    }

    public String getPhone() {
        if(context!=null)
        phone=getString("phone");
        return phone;
    }

    public void setPhone(String phone) {
        if(context!=null)
        saveString("phone",phone);
        this.phone = phone;
    }

    public String getPassword() {
        if(context!=null)
        password =getString("password");
        return password;
    }

    public void setPassword(String password) {
        if(context!=null)
        saveString("password", password);
        this.password = password;
    }


    public String getWorkarea() {
        if(context!=null)
            workarea =getString("workarea");
        return workarea;
    }

    public void setWorkarea(String workarea) {
        if(context!=null)
            saveString("workarea", workarea);
        this.workarea = workarea;
    }

    public String getNeighbourhood() {
        if(context!=null)
            neighbourhood =getString("neighbourhood");
        return neighbourhood;
    }

    public void setNeighbourhood(String neighbourhood) {
        if(context!=null)
            saveString("neighbourhood", neighbourhood);
        this.neighbourhood = neighbourhood;
    }

    public String getTown_id() {
        if(context!=null)
            town_id =getString("town_id");
        return town_id;
    }

    public void setTown_id(String town_id) {
        if(context!=null)
            saveString("town_id", town_id);
        this.town_id = town_id;
    }

    public Boolean getFirstTime() {
        if(context!=null)
            firstTime =getBoolean("first_time");
        return firstTime;
    }

    public void setFirstTime(Boolean firstTime) {
        if(context!=null)
            saveBoolean("first_time", firstTime);
        this.firstTime = firstTime;
    }

    public boolean isOfficer() {
        if(context!=null)
            officer =getBoolean("officer");
        return officer;
    }

    public void setOfficer(boolean officer) {
        if(context!=null)
            saveBoolean("officer", officer);
        this.officer = officer;
    }

    private void saveString(String key, String value){
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(key, value);
        edit.commit();
    }

    private String getString(String key){
        return sharedPreferences.getString(key,null);
    }

    private void saveInt(String key,int value){
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt(key, value);
        edit.commit();
    }

    private int getInt(String key){
        return sharedPreferences.getInt(key,0);
    }


    private void saveBoolean(String key,boolean value){
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean(key, value);
        edit.commit();
    }


    private boolean getBoolean(String key){
        return sharedPreferences.getBoolean(key,true);
    }


}
