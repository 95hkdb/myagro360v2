package com.hensongeodata.myagro360v2.model;

import android.content.Context;

import com.hensongeodata.myagro360v2.R;

public class MessageGroup {
    private String id;
    private String name;
    public static String NAME_IGEZA_COMMUNITY="igeza_community";

    private MessageGroup(Context context){
        if(context!=null){
            NAME_IGEZA_COMMUNITY=context.getResources().getString(R.string.community_title);
        }
    }
    public MessageGroup(Context context,String id, String name) {
        this.id = id;
        this.name = name;

        if(context!=null){
            NAME_IGEZA_COMMUNITY=context.getResources().getString(R.string.community_title);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
