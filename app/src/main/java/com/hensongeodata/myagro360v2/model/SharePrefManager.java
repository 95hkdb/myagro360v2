package com.hensongeodata.myagro360v2.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.hensongeodata.myagro360v2.newmodel.PostContent;

import java.util.ArrayList;
import java.util.List;

public class SharePrefManager {

    private static final String SHARED_PREF_NAME = "hgtagro";
    private static final String KEY_USER_FIRSTNAME = "userfirstname";
    private static final String KEY_USER_LASTNAME = "userlasttname";
    private static final String KEY_USER_EMAIL = "useremail";
    private static final String KEY_USER_PROFESSION = "userprofession";
    private static final String KEY_USER_ACCURACY_MAX = "maxaccuracy";
    private static final String KEY_USER_ORGANISATION_CODE = "userorgcode";
    private static final String KEY_USER_ORGANISATION_NAME = "userorgname";
    private static final String KEY_USER_ORGANISATION_ADDRESS = "userorgphone";
    private static final String KEY_USER_ORGANISATION_LOGO_URL = "userorglogourl";
    private static final String KEY_USER_ORGANISATION_EMAIL = "userorgemail";
    private static final String KEY_USER_PUSH_TOKEN = "userpushtoken";

//    Getting Post into shared preferences
private static final String KEY_POST_ID = "postid";
private static final String KEY_POST_URL = "posturl";
private static final String KEY_POST_CREATOR = "postcreator";
private static final String KEY_POST_DATE = "postdate";
private static final String KEY_POST_TITLE = "postitle";
private static final String KEY_POST_CONTENT = "postcontent";

//  Getting weather into shared pref manager
private static final String KEY_WEATHER_LOCATION = "weatherlocation";
private static final String KEY_WEATHER_MAIN= "weathermain";
private static final String KEY_WEATHER_DESCRIP = "weatherdescript";
private static final String KEY_WEATHER_TEMP= "weathertemperature";
private static final String KEY_WEATHER_HUMID= "weatherhumidity";
private static final String KEY_WEATHER_PRESS= "weatherpressure";

//  Getting activity
private static final String KEY_ACTIVITY_NAME = "activityname";
private static final String KEY_ACTIVITY_DURATION = "activityduration";
private static final String KEY_ACTIVITY_COMMENT= "activitycomment";

      //  Form before Mapping
      private static final String KEY_FORM_LABEL = "formname";
      private static final String KEY_FORM_COMMENT = "formduration";

    private static SharePrefManager mInstance;
    private static Context sharedContext;

    private static final String PREF_COORD_LAT = "coord_lat";
    private static final String PREF_COORD_LNG = "coord_long";

    private static final String PREF_DESTINATION = "destination";

    private static final String PREF_PLOT_SORT = "plot_sort";
    public static final String SORT_BY_PLOT_SIZE_DESC = "sort_by_plot_desc";
    public static final String SORT_BY_PLOT_ALL = "sort_by_plot_all";
    public static final String SORT_BY_PLOT_SIZE_ASC   = "sort_by_plot_asc";
    public static final String SORT_BY_PLOT_NAME_DESC         = "sort_by_plot_name_desc";
    public static final String SORT_BY_PLOT_NAME_ASC            = "sort_by_plot_name_asc";
    public static final String SORT_BY_DISTINCT = "sort_by_distinct";
    public static final String SORT_BY_LAST_TO_FIRST = "sort_by_last_to_first";
    public static final String SORT_BY_CROP = "sort_by_crop";
    public static final String SORT_BY_CROP_TYPE = "sort_by_crop_type";
    public static final String CURRENT_CROP_SELECTION  = "current_crop_selection";

    public static final String PREF_ACTIVITIES_SORT = "activities_sort";
    public static final String SORT_BY_PLOT_ACTIVITY = "sort_by_plot_activity";
    public static final String SORT_BY_FARM_ACTIVITY = "sort_by_farm_activity";
    public static final String SORT_BY_COMPLETED_ACTIVITY = "sort_by_completed_activity";
    public static final String SORT_BY_PENDING_ACTIVITY = "sort_by_pending_activity";
    public static final String SORT_BY_TYPE_ACTIVITY = "sort_by_type_activity";
    public static final String SORT_BY_DATE_CREATED_ACTIVITY = "sort_by_date_created_activity";
    public static final String SORT_BY_CROP_ACTIVITY= "sort_by_crop_activity";
    public static final String SORT_BY_ALL_ACTIVITY= "sort_by_all_activity";
    public static final String SORT_BY_ACTIVITY_TYPE = "sort_by_activity_type";
    public static final String CURRENT_ACTIVITY_SELECTION= "sort_by_activity_name";

    public static final String SORT_BY_PLOT_TYPE_ACTIVITY = "sort_by_plot_type_activity";
    public static final String SORT_BY_FARM_TYPE_ACTIVITY = "sort_by_farm_type_activity";
    public static final String SORT_BY_CROP_TYPE_ACTIVITY = "sort_by_crop_type_activity";
    public static final String SORT_BY_FROM_DATE_TYPE_ACTIVITY = "sort_by_from_date_type_activity";
    public static final String SORT_BY_END_DATE_TYPE_ACTIVITY = "sort_by_end_date_type_activity";

    private static final String PREF_MAP_SORT = "map_sort";
    public static final String SORT_BY_ALL_MAP = "sort_by_all_map";
    public static final String SORT_BY_RECENT_MAP = "sort_by_recent_map";
    public static final String SORT_BY_SIZE_HIGH_TO_LOW_MAP = "sort_by_size_high_to_low_map";
    public static final String SORT_BY_SIZE_LOW_TO_HIGH_MAP = "sort_by_size_low_to_high_map";

    public static final String CURRENT_PLOT_SELECTION_ACTIVITY = "current_plot_selection_activity";
    public static final String CURRENT_FARM_SELECTION_ACTIVITY = "current_farm_selection_activity";

    public static final String PREF_SECONDARY_USER = "secondary_user";
    public static final String CURRENT_SECONDARY_USER_SELECTION = "current_secondary_user_selection";

    public SharePrefManager(Context context) {
        sharedContext = context;
    }

    public static synchronized SharePrefManager getInstance(Context context){
        if (mInstance == null) {
            mInstance = new SharePrefManager(context);
        }
        return mInstance;
    }

    public boolean maxacurracy(int max){
        SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(KEY_USER_ACCURACY_MAX, max);
        editor.apply();

        return true;

    }

    public  boolean newpost(String url, String creator, PostContent postContent){

        SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(KEY_POST_URL, url);
        editor.putString(KEY_POST_CREATOR, creator);
        editor.putInt(KEY_POST_ID, postContent.getId());
        editor.putString(KEY_POST_TITLE, postContent.getTitle());
        editor.putString(KEY_POST_CONTENT, postContent.getContent());
        editor.putString(KEY_POST_DATE , postContent.getDate());

        editor.apply();

        return  true;
    }

      public  boolean newActivity(String act_name, String duration, String comment){

            SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            editor.putString(KEY_ACTIVITY_NAME, act_name);
            editor.putString(KEY_ACTIVITY_DURATION, duration);
            editor.putString(KEY_ACTIVITY_COMMENT , comment);

            editor.apply();

            return true;
      }

      public boolean formMapping(String label, String comment) {

            SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            editor.putString(KEY_FORM_LABEL, label);
            editor.putString(KEY_FORM_COMMENT, comment);

            editor.apply();

            return  true;
      }

    public boolean newWeather(String location,String type, String description, String temperature, String humidity, String pressure){
        SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(KEY_WEATHER_LOCATION, location);
        editor.putString(KEY_WEATHER_MAIN, type);
        editor.putString(KEY_WEATHER_DESCRIP, description);
        editor.putString(KEY_WEATHER_TEMP, temperature);
        editor.putString(KEY_WEATHER_HUMID, humidity);
        editor.putString(KEY_WEATHER_PRESS, pressure);

        editor.apply();

        return  true;
    }

    public List<String> getWeather(){

        SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        List<String> weather = new ArrayList<>();
        weather.add(sharedPreferences.getString(KEY_WEATHER_LOCATION, null));
        weather.add(sharedPreferences.getString(KEY_WEATHER_MAIN, null));
        weather.add(sharedPreferences.getString(KEY_WEATHER_DESCRIP, null));
        weather.add(sharedPreferences.getString(KEY_WEATHER_TEMP, null));
        weather.add(sharedPreferences.getString(KEY_WEATHER_HUMID, null));
        weather.add(sharedPreferences.getString(KEY_WEATHER_PRESS, null));
//        post.add(sharedPreferences.getString(KEY_POST_CREATOR, null));

        return weather;

    }

      public List<String> getFormMapping() {

            SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
            List<String> mapping = new ArrayList<>();
            mapping.add(sharedPreferences.getString(KEY_FORM_LABEL, null));
            mapping.add(sharedPreferences.getString(KEY_FORM_COMMENT, null));

            return mapping;

      }

    public List<String> getRecentActivity(){
        SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        List<String> activity = new ArrayList<>();
        activity.add(sharedPreferences.getString(KEY_ACTIVITY_NAME, null));
        activity.add(sharedPreferences.getString(KEY_ACTIVITY_DURATION, null));
        activity.add(sharedPreferences.getString(KEY_ACTIVITY_COMMENT, null));

        return activity;
    }

    public List<String> getPostInitial(){

        SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        List<String> post = new ArrayList<>();
        post.add(sharedPreferences.getString(KEY_POST_URL, null));
        post.add(sharedPreferences.getString(KEY_POST_CREATOR, null));

        return post;
    }

    public PostContent getPostContent(){
        SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);

        return new  PostContent(
                sharedPreferences.getInt(KEY_POST_ID, 0),
                sharedPreferences.getString(KEY_POST_DATE, null),
                sharedPreferences.getString(KEY_POST_TITLE, null),
                sharedPreferences.getString(KEY_POST_CONTENT, null)
        );
    }

    public boolean organisation(String org_code, String org_name,String org_email,String org_phone, String logo_url) {
        SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(KEY_USER_ORGANISATION_CODE, org_code);
        editor.putString(KEY_USER_ORGANISATION_NAME, org_name);
        editor.putString(KEY_USER_ORGANISATION_EMAIL, org_email);
        editor.putString(KEY_USER_ORGANISATION_ADDRESS, org_phone);
        editor.putString(KEY_USER_ORGANISATION_LOGO_URL, logo_url);

        editor.apply();
        return true;
    }

    public boolean userLogin(String email,String fname,String lname,String profs) {
        SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_USER_FIRSTNAME, fname);
        editor.putString(KEY_USER_EMAIL, email);
        editor.putString(KEY_USER_LASTNAME, lname);
        editor.putString(KEY_USER_PROFESSION, profs);
        editor.apply();

        //String u_email = sharedPreferences.getString(KEY_USER_FIRSTNAME, null);

        //Toast.makeText(sharedContext, "Editor has " + u_email, Toast.LENGTH_LONG).show();
        return true;
    }

    public boolean savePushToken(String push){
        SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_USER_PUSH_TOKEN, push);
        editor.apply();

        return true;
    }

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
          return sharedPreferences.getString(KEY_USER_EMAIL, null) != null
                  && sharedPreferences.getString(KEY_USER_ORGANISATION_NAME, null) != null;
    }

    public List<String> getUserDetails(){
        SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(sharedContext);
        List<String> user = new ArrayList<>();
        user.add(sharedPreferences.getString(KEY_USER_EMAIL, null));
        user.add(sharedPreferences.getString(KEY_USER_FIRSTNAME, null));
        user.add(sharedPreferences.getString(KEY_USER_LASTNAME, null));
        user.add(sharedPreferences.getString(KEY_USER_PUSH_TOKEN, null));
        user.add(sharedPreferences.getString(KEY_USER_PROFESSION, null));
          user.add(sharedPreferences.getString(KEY_USER_ORGANISATION_LOGO_URL, null));

        return user;
    }

    public List<String> getOrganisationDetails(){

        SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        List<String> organisation = new ArrayList<>();
        organisation.add(sharedPreferences.getString(KEY_USER_ORGANISATION_CODE, null));
        organisation.add(sharedPreferences.getString(KEY_USER_ORGANISATION_NAME, null));
        organisation.add(sharedPreferences.getString(KEY_USER_ORGANISATION_EMAIL, null));
        organisation.add(sharedPreferences.getString(KEY_USER_ORGANISATION_ADDRESS, null));
        organisation.add(sharedPreferences.getString(KEY_USER_ORGANISATION_LOGO_URL, null));

        return organisation;
    }


    public static void setLocationDetails(Context context, double lat, double lon){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putLong(PREF_COORD_LAT, Double.doubleToRawLongBits(lat));
        editor.putLong(PREF_COORD_LNG, Double.doubleToRawLongBits(lon));
        editor.apply();
    }


    public static void resetLocationCoordinates(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();


        editor.remove(PREF_COORD_LAT);
        editor.remove(PREF_COORD_LNG);
        editor.apply();
    }

    public static double[] getLocationCoordinates(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        double[] preferredCoordinates = new double[2];

        preferredCoordinates[0] = Double
                .longBitsToDouble(sp.getLong(PREF_COORD_LAT, Double.doubleToRawLongBits(0.0)));
        preferredCoordinates[1] = Double
                .longBitsToDouble(sp.getLong(PREF_COORD_LNG, Double.doubleToRawLongBits(0.0)));

        return preferredCoordinates;
    }

    public static boolean isLocationLatLonAvailable(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        boolean spContainLatitude = sp.contains(PREF_COORD_LAT);
        boolean spContainLongitude = sp.contains(PREF_COORD_LNG);

        boolean spContainBothLatitudeAndLongitude = false;
        if (spContainLatitude && spContainLongitude) {
            spContainBothLatitudeAndLongitude = true;
        }

        return spContainBothLatitudeAndLongitude;
    }

    public boolean logout() {
        SharedPreferences sharedPreferences = sharedContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        return true;
    }

    public static void setDestination(Context context, String destination){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_DESTINATION ,  destination);
        editor.apply();
    }

    public static boolean isDestinationAvailable(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        boolean spContainsDestination = sp.contains(PREF_DESTINATION);

        if (spContainsDestination){
            return true;
        }

        return false;
    }

    public static void clearDestination(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PREF_DESTINATION);
        editor.apply();
    }

    public static void setSortPlotsPreference(Context context, String sortPreference){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_PLOT_SORT ,  sortPreference);
        editor.apply();
    }

    public static boolean isSortPreferenceAvailable(Context context){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.contains(PREF_PLOT_SORT);
    }

    public static String getSortPlotsPreference(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_PLOT_SORT,"");
    }

    public static void clearSortPlotsPreference(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PREF_PLOT_SORT);
        editor.apply();
    }

    // Crop Preference
    public static void setSortCropPreference(Context context, String sortPreference){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SORT_BY_CROP_TYPE ,  sortPreference);
        editor.apply();
    }

    public static boolean isCropTypePreferenceAvailable(Context context){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.contains(SORT_BY_CROP_TYPE);
    }

    public static void clearCropTypePreference(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(SORT_BY_CROP_TYPE);
        editor.apply();
    }

    public static String getCropSortPreference(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(SORT_BY_CROP_TYPE,"");
    }

    public static void setCurrentCropSelection(Context context, long currentSelection){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        if (sharedPreferences.contains(CURRENT_CROP_SELECTION)){
            editor.remove(CURRENT_CROP_SELECTION);
            editor.apply();
        }

        editor.putLong(CURRENT_CROP_SELECTION ,  currentSelection);
        editor.apply();
    }

    public static long getCurrentCropSelection(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getLong(CURRENT_CROP_SELECTION,0);
    }


    //Farm Activity Preference
    public static void setSortFarmActivitiesPreference(Context context, String sortPreference){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_ACTIVITIES_SORT ,  sortPreference);
        editor.apply();
    }

    public static boolean isActivitiesSortPreferenceAvailable(Context context){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.contains(PREF_ACTIVITIES_SORT);
    }

    public static String getSortActivitiesPreference(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_ACTIVITIES_SORT,"");
    }

    public static void clearSortActivitiesPreference(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PREF_ACTIVITIES_SORT);
        editor.apply();
    }

    public static void setCurrentActivitySelection(Context context, long currentSelection){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        if (sharedPreferences.contains(CURRENT_ACTIVITY_SELECTION)){
            editor.remove(CURRENT_ACTIVITY_SELECTION);
            editor.apply();
        }

        editor.putLong(CURRENT_ACTIVITY_SELECTION ,  currentSelection);
        editor.apply();
    }

    public static long getCurrentActivitySelection(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getLong(CURRENT_ACTIVITY_SELECTION,0);
    }

    // Activity Type Preference
    public static void setSortActivityTypePreference(Context context, String sortPreference){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SORT_BY_ACTIVITY_TYPE ,  sortPreference);
        editor.apply();
    }

    public static boolean isActivityTypePreferenceAvailable(Context context){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.contains(SORT_BY_ACTIVITY_TYPE);
    }

    public static void clearActivityTypePreference(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(SORT_BY_ACTIVITY_TYPE);
        editor.apply();
    }

    public static String getActivityTypeSortPreference(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(SORT_BY_ACTIVITY_TYPE,"");
    }

    // Plot  Type Preference
    public static void setSortParameterPreference(Context context, String sortPreference, String sortType){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

//        String sort_type = "";
//
//        switch(sortType){
//            case SORT_BY_PLOT_TYPE_ACTIVITY:
//                sort_type = SORT_BY_PLOT_TYPE_ACTIVITY;
//                break;
//
//            case SORT_BY_FARM_TYPE_ACTIVITY:
//                sort_type = SORT_BY_FARM_TYPE_ACTIVITY;
//                break;
//
//            case SORT_BY_DATE_TYPE_ACTIVITY:
//                sort_type = SORT_BY_DATE_TYPE_ACTIVITY;
//                break;
//        }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(sortType,  sortPreference);
        editor.apply();
    }

    public static String getSortParameterPreference(Context context, String sortType) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

            return sp.getString(sortType,"");

    }

    public static boolean isSortParameterAvailable(Context context, String sortType){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.contains(sortType);
    }

    public static void clearSortParameter(Context context, String sortType){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(sortType);
        editor.apply();
    }

    public static void setSelectedPreference(Context context, long currentSelection, String owner){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        if (sharedPreferences.contains(owner)){
            editor.remove(owner);
            editor.apply();
        }

        editor.putLong(owner ,  currentSelection);
        editor.apply();
    }

    public static long getSelectedPreference(Context context, String owner) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getLong(owner,0);
    }

    public static String getActivityTypeSortPreference(Context context, String sortType) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(sortType,"");
    }


//    public static void setCurrentCropSelection(Context context, long currentSelection){
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//
//        if (sharedPreferences.contains(CURRENT_CROP_SELECTION)){
//            editor.remove(CURRENT_CROP_SELECTION);
//            editor.apply();
//        }
//
//        editor.putLong(CURRENT_CROP_SELECTION ,  currentSelection);
//        editor.apply();
//    }

//    public static long getCurrentCropSelection(Context context) {
//        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
//        return sp.getLong(CURRENT_CROP_SELECTION,0);
//    }


    public static void setSecondaryUserPref(Context context, String userId){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_SECONDARY_USER ,  userId);
        editor.apply();
    }

    public static String getSecondaryUserPref(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_SECONDARY_USER, "");
    }

    public static boolean isSecondaryUserAvailable(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        boolean spContainsSecondaryUser = sp.contains(PREF_SECONDARY_USER);

        if (spContainsSecondaryUser){
            return true;
        }

        return false;
    }

    public static void clearSecondaryUserPref(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PREF_SECONDARY_USER);
        editor.apply();
    }

    public static void setCurrentSecondaryUserSelection(Context context, long currentSelection){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        if (sharedPreferences.contains(CURRENT_SECONDARY_USER_SELECTION)){
            editor.remove(CURRENT_SECONDARY_USER_SELECTION);
            editor.apply();
        }

        editor.putLong(CURRENT_SECONDARY_USER_SELECTION ,  currentSelection);
        editor.apply();
    }

    public static long getCurrentSecondaryUserSelection(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getLong(CURRENT_SECONDARY_USER_SELECTION,0);
    }

    public static boolean isSecondaryUserSelectionAvailable(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        boolean spContainsSecondaryUser = sp.contains(CURRENT_SECONDARY_USER_SELECTION);

        if (spContainsSecondaryUser){
            return true;
        }

        return false;
    }


    public static void setMapSortPreference(Context context, String sortPreference){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_MAP_SORT ,  sortPreference);
        editor.apply();
    }

    public static boolean isMapSortPreferenceAvailable(Context context){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.contains(PREF_MAP_SORT);
    }

    public static String getMapSortPreference(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(PREF_MAP_SORT,"");
    }

    public static void clearMapSortPreference(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PREF_MAP_SORT);
        editor.apply();
    }

}

