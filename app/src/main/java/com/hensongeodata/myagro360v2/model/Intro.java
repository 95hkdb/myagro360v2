package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 3/11/2018.
 */

public class Intro {
    private String caption;
    private String message;
    private int illustration;

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getIllustration() {
        return illustration;
    }

    public void setIllustration(int illustration) {
        this.illustration = illustration;
    }
}
