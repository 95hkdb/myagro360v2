package com.hensongeodata.myagro360v2.model;

import android.net.Uri;

/**
 * Created by user1 on 11/6/2017.
 */

public class Video {
    private String id;
//    private String path;
//    private String previewImage;
//    private String thumbnailPath;
//    private String videoString;
//    private Bitmap videoBitmap;
    private Uri uri;
    private Image image;

    public Video(){

    }

//    public String getPath() {
//        return path;
//    }
//
//    public void setPath(String path) {
//        this.path = path;
//    }
//
//    public String getPreviewImage() {
//        return previewImage;
//    }
//
//    public void setPreviewImage(String previewImage) {
//        this.previewImage = previewImage;
//    }
//
//    public String getThumbnailPath() {
//        return thumbnailPath;
//    }
//
//    public void setThumbnailPath(String thumbnailPath) {
//        this.thumbnailPath = thumbnailPath;
//    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

//    public String getVideoString() {
//        return videoString;
//    }
//
//    public void setVideoString(String videoString) {
//        this.videoString = videoString;
//    }
//
//    public Bitmap getVideoBitmap() {
//        return videoBitmap;
//    }
//
//    public void setVideoBitmap(Bitmap videoBitmap) {
//        this.videoBitmap = videoBitmap;
//    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
