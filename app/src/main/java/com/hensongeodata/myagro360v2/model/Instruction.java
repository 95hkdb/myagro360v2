package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 7/16/2018.
 */

public class Instruction {
    protected String message;
    protected Image image;
    protected int action;
    protected int status;
    protected String id;

    public static int ACTION_INFO=100;
    public static int ACTION_INSTRUCT=200;
    public static int ACTION_TASK=300;

    public static int STATE_PROCCESSED=400;
    public static int STATE_UNPROCCESSED=500;

    public Instruction(String message, Image image, int action, int status) {
        this.message = message;
        this.image = image;
        this.action = action;
        this.status=status;
    }

    public Instruction(){}


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
