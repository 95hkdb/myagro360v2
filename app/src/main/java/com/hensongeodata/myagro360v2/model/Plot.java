package com.hensongeodata.myagro360v2.model;

import com.google.gson.annotations.SerializedName;

public class Plot {

      @SerializedName("id")
      private int id;

      @SerializedName("name")
      private String name;

      @SerializedName("farm_id")
      private String farmId;

      @SerializedName("farm_name")
      private String farmName;

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      public String getFarmId() {
            return farmId;
      }

      public void setFarmId(String farmId) {
            this.farmId = farmId;
      }

      public String getFarmName() {
            return farmName;
      }

      public void setFarmName(String farmName) {
            this.farmName = farmName;
      }
}
