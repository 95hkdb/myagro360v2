package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 8/7/2018.
 */

public class Pesticide {
    private String title;
    private String description;
    private String providerName;
    private String providerPhone;
    private String activeIngredients;

    public Pesticide(String title, String description, String providerName, String providerPhone, String activeIngredients) {
        this.title = title;
        this.description = description;
        this.providerName = providerName;
        this.providerPhone = providerPhone;
        this.activeIngredients = activeIngredients;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getProviderPhone() {
        return providerPhone;
    }

    public void setProviderPhone(String providerPhone) {
        this.providerPhone = providerPhone;
    }

    public String getActiveIngredients() {
        return activeIngredients;
    }

    public void setActiveIngredients(String activeIngredients) {
        this.activeIngredients = activeIngredients;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
