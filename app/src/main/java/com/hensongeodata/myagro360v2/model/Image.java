package com.hensongeodata.myagro360v2.model;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by user1 on 11/6/2017.
 */

public class Image {
    private String id;
    private String imageString;
    private Bitmap imageBitmap;
    private Uri uri;
    private String image_url;
    private int src;
    //private OnImageReady onImageReady;

    public Image(){

    }
    public Image(String imageString, Bitmap imageBitmap) {
        this.imageString = imageString;
        this.imageBitmap = imageBitmap;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public Image(int src) {
        this.src = src;
    }

    public int getSrc() {
        return src;
    }

    public void setSrc(int src) {
        this.src = src;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageString() {
        return imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }

    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }
}
