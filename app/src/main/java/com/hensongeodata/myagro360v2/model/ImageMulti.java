package com.hensongeodata.myagro360v2.model;

import com.hensongeodata.myagro360v2.Interface.OnImageReady;

import java.util.ArrayList;

/**
 * Created by user1 on 5/15/2018.
 */

public class ImageMulti {
    private String id;
    private ArrayList<Image> images;
    private OnImageReady onImageReady;

    public ImageMulti(){
        images=new ArrayList<>();
    }

    public ImageMulti(ArrayList<Image> images) {
        this.images = images;
    }

    public OnImageReady getOnImageReady() {
        return onImageReady;
    }

    public void setOnImageReady(OnImageReady onImageReady) {
        this.onImageReady = onImageReady;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    public void addImage(Image image){
        images.add(image);
        if(onImageReady!=null)
                    onImageReady.onReady();
    }

    public boolean removeImage(int index){
       if(images.size()>index) {
           images.remove(index);
       return true;
       }

       return false;
    }
}
