package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 7/16/2018.
 */

public class Information {
    protected String message;
    protected Image image;
    protected int action;

    public static int ACTION_INFO=100;
    public static int ACTION_TASK=200;

    public Information(String message, Image image, int action) {
        this.message = message;
        this.image = image;
        this.action = action;
    }

    public Information(String message) {
        this.message = message;
    }

    public Information(String message, int action) {
        this.message = message;
        this.action = action;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }
}
