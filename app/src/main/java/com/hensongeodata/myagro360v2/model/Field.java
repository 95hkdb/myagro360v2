package com.hensongeodata.myagro360v2.model;

import java.util.ArrayList;

/**
 * Created by user1 on 1/24/2018.
 */

public class Field {
    //Question types
    public static String TEXT="text";
    public static String RADIO="radio";
    public static String TEXTAREA="textarea";
    public static String DROPDOWN="dropdown";
    public static String CHECKBOX="checkbox";
    public static String EMAIL="email";
    public static String DATE="date";
    public static String NUMERIC="numeric";
    public static String FILE="file";
    public static String MAPPING="mapping";
    public static String PHONE="phone";
    public static String TEXT_MASK="text_mask";
    public static String TOWN="town";

    protected String id;//id of field from server
    protected String type;
    protected String question;
    protected String description;
    protected String hint;
    protected int tag; //id of view in layout
    protected boolean required=false;
    protected String section_id;//section a field belongs to. MAIN section don't have section ids.SUB sections use MAIN section's ids
    protected ArrayList<String> answers;

    public Field(){
        //an empty constructor
        answers=new ArrayList<>();
        answers.add(null);
         }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<String> answers) {
        this.answers = answers;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public void addFields(Field field){
       //weird implementation but this adds a fields detail to a new field (usually when casting)
        this.id=field.getId();
        this.type=field.getType();
        this.question=field.getQuestion();
        this.description=field.getDescription();
        this.hint=field.getHint();
        this.tag=field.getTag();
        this.required=field.isRequired();
        this.section_id=field.getSection_id();
        this.answers=field.getAnswers();
    }
}
