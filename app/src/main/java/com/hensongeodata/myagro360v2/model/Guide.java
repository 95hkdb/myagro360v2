package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 4/4/2018.
 */

public class Guide {
    private int image;
    private String header;
    private String content;

    public Guide(int image, String header, String content) {
        this.image = image;
        this.header = header;
        this.content = content;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
