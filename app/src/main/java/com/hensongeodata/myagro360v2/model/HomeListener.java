package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 9/13/2017.
 */

public class HomeListener {

    public interface HomeFabListener{
        void onAction(int option);
    }

    private static HomeListener mInstance;
    private HomeFabListener homeFabListener;
    private int mOption;

    private HomeListener(){}

    public static HomeListener getmInstance() {
        if (mInstance == null) {
            mInstance = new HomeListener();
        }
        return mInstance;
    }

    public void setListener(HomeFabListener fabListener){
        homeFabListener=fabListener;
    }


    public void changeOption(int option){
        if(mInstance!=null){
            mOption=option;
            notifyOptionChange();
        }
    }

    public int getOption(){
        return mOption;
    }

    private void notifyOptionChange(){
        homeFabListener.onAction(mOption);
    }


}
