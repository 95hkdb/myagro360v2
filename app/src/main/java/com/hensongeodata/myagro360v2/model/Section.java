package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 1/24/2018.
 */

public class Section {
    public static String SUB="sub";
    public static String MAIN="main";

    private String id;
    private String title;
    private String desc;
    private String form_id;
    private String type;


    public Section(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getForm_id() {
        return form_id;
    }

    public void setForm_id(String form_id) {
        this.form_id = form_id;
    }
}
