package com.hensongeodata.myagro360v2.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 7/16/2018.
 */

public class Chatbot {
    protected String name;
    protected List<Object> instructions;

    public Chatbot() {
        instructions=new ArrayList<>();
    }

    public Chatbot(String name, List<Object> instructions) {
        this.name = name;
        this.instructions = instructions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Object> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<Object> instructions) {
        this.instructions = instructions;
    }
}
