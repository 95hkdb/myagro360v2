package com.hensongeodata.myagro360v2.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user1 on 7/25/2018.
 */

@Entity(tableName = KnowledgeBase.TABLE_NAME)
public class KnowledgeBase{

    public static final String TABLE_NAME = "knowledgeBase";

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo( name = "id")
    private int id;

    @ColumnInfo( name = "title")
    private String title;

    @ColumnInfo( name = "content")
    private String content;

    @ColumnInfo( name = "url")
    private String url;

    @ColumnInfo( name = "media_url")
    @SerializedName("media_url")
    private String mediaUrl;

    @ColumnInfo( name = "audio")
    private String audio;

    @SerializedName("media_type")
    private String media_type;

    @ColumnInfo( name = "language")
    private String language;

    public static String MEDIA_VIDEO="video";
    public static String MEDIA_IMAGE="image";
    public static String MEDIA_AUDIO="audio";
    public static String MEDIA_FILE="file";


    public KnowledgeBase(){}

    @Ignore
    public KnowledgeBase(int id, String title, String content, String url, String audio, String media_type, String language) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.url = url;
        this.audio=audio;
        this.media_type = media_type;
        this.language = language;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }
}
