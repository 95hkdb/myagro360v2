package com.hensongeodata.myagro360v2.model;

public class PaymentReceipt {
    public String id;
    public String order_number;
    public String date;
    public String order_status;
    public String amount_paid;
}
