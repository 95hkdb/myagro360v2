package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 10/27/2017.
 */

public class SigninModel {
    private String email;
    private String password;
    private String hgtOrgID;

    public SigninModel(){}

    public SigninModel(String email, String password, String hgtOrgID) {
        this.email = email;
        this.password = password;
        this.hgtOrgID = hgtOrgID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHgtOrgID() {
        return hgtOrgID;
    }

    public void setHgtOrgID(String hgtOrgID) {
        this.hgtOrgID = hgtOrgID;
    }
}
