package com.hensongeodata.myagro360v2.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.hensongeodata.myagro360v2.MyApplication;

/**
 * Created by user1 on 10/30/2017.
 */

public class SettingsModel {
    private String id;
    private String org_id;
    private String form_id;
    private String transaction_id;
    private int accuracy_max;
    private boolean previewForm;
    private String town;
    private String workarea;
    private String country;
    private String region;
    private String district;
    private SharedPreferences sharedPreferences;
    private Boolean firstTime;
    private long scan_batch;
    private long scan_item;
    private long map_batch;
    private long map_item;
    private boolean archive_mode;
    private String town_id;//town id of most recent selected town/workarea
    private int file_counter;
    private String lang;

    public static String LAN_ENG="english";
    public static String LAN_EWE="ewe";
    public static String LAN_FR="french";
    public static String LAN_SWAHILI="swahili";
    public static String LAN_TWI="twi";

    public static String CODE_ENG="en";
    public static String CODE_EWE="ewe";
    public static String CODE_FR="fr";
    public static String CODE_SWAHILI="sw";
    public static String CODE_TWI="twi";

    Context context;

    public SettingsModel(Context context){
        if(context!=null)
            sharedPreferences=context.getSharedPreferences("MyPrefs",0);

        this.context=context;
    }

    public SettingsModel(Context context, String id, int accuracy_max, String town, String firstname, String lastname, String workarea, String phone, String password, boolean verified) {
        sharedPreferences=context.getSharedPreferences(Keys.PREFS,0);
        this.id = id;
        this.accuracy_max = accuracy_max;
        this.town = town;
        this.workarea = workarea;
    }

    public String getId() {
        if(context!=null)
            id=getString("id");

        return id;
    }

    public void setId(String id) {
        if(context!=null)
            saveString("id",id);

        this.id = id;
    }

    public long getScan_batch() {
        if(context!=null)
            scan_batch =getLong("scan_batch");

        return scan_batch;
    }

    public void setScan_batch(long scan_batch) {
        if(context!=null)
            saveLong("scan_batch", scan_batch);

        this.scan_batch = scan_batch;
    }


    public long getScan_item() {
        if(context!=null)
            scan_item =getLong("scan_item");

        return scan_item;
    }

    public void setScan_item(long scan_item) {
        if(context!=null)
            saveLong("scan_item", scan_item);

        this.scan_item = scan_item;
    }


    public long getMap_batch() {
        if(context!=null)
            map_batch =getLong("map_batch");

        return map_batch;
    }

    public void setMap_batch(long map_batch) {
        if(context!=null)
            saveLong("map_batch", map_batch);

        this.map_batch = this.map_batch;
    }


    public long getMap_item() {
        if(context!=null)
            map_item =getLong("map_item");

        return map_item;
    }

    public void setMap_item(long map_item) {
        if(context!=null)
            saveLong("map_item", map_item);

        this.map_item = map_item;
    }


    public String getForm_id() {
        if(context!=null)
            form_id =getString("form_id");

        return form_id;
    }

    public void setFile_counter(int file_counter) {
        if(context!=null)
            saveInt("file_counter", file_counter);

        this.file_counter = file_counter;
    }

    public int getFile_counter() {
        if(context!=null)
            file_counter =getInt("file_counter");
        return file_counter;
    }


    public void setForm_id(String form_id) {
        if(context!=null)
            saveString("form_id", form_id);

        this.form_id = form_id;
    }

    public String getTransaction_id() {
        if(context!=null)
            transaction_id =getString("transaction_id");
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        if(context!=null)
            saveString("transaction_id", transaction_id);

        this.transaction_id = transaction_id;
    }

    public String getOrg_id() {
        if(context!=null)
            org_id =getString("org_id");
        return org_id;
    }

    public void setOrg_id(String org_id) {
        if(context!=null)
            saveString("org_id", org_id);
        this.org_id = org_id;
    }

    public int getAccuracy_max() {
        if(context!=null)
            accuracy_max =getInt("accuracy_max");
        if(accuracy_max<=0)
            accuracy_max= MyApplication.ACCURACY_DEFAULT;

        return accuracy_max;
    }

    public void setAccuracy_max(int accuracy_max) {
        if(context!=null)
            saveInt("accuracy_max", accuracy_max);

        this.accuracy_max = accuracy_max;
    }

    public String getTown() {
        if(context!=null)
            town =getString("town");

        return town;
    }

    public void setTown(String town) {
        if(context!=null)
        saveString("town", town);

        this.town = town;
    }

    public String getWorkarea() {
        if(context!=null)
            workarea =getString("workarea");
        return workarea;
    }

    public void setWorkarea(String workarea) {
        if(context!=null)
            saveString("workarea", workarea);
        this.workarea = workarea;
    }

    public String getTown_id() {
        if(context!=null)
            town_id =getString("town_id");
        return town_id;
    }

    public void setTown_id(String town_id) {
        if(context!=null)
            saveString("town_id", town_id);
        this.town_id = town_id;
    }

    public Boolean getFirstTime() {
        if(context!=null)
            firstTime =getBoolean("first_time");
        return firstTime;
    }

    public void setFirstTime(Boolean firstTime) {
        if(context!=null)
            saveBoolean("first_time", firstTime);

        this.firstTime = firstTime;
    }

    public String getLang() {
        if(context!=null)
            lang=getString("lang");

        if(lang==null||lang.trim().isEmpty())
            lang=LAN_ENG;

        return lang;
    }

    public void setLang(String lang) {
        if(context!=null)
            saveString("lang",lang);

        this.lang = lang;
    }

    public boolean isPreviewForm() {
        if(context!=null)
            previewForm =getBoolean("preview_form");
        return previewForm;
    }

    public void setPreviewForm(boolean previewForm) {
        if(context!=null)
            saveBoolean("preview_form", previewForm);
        this.previewForm = previewForm;
    }

    public boolean isArchive_mode() {
        if(context!=null)
            archive_mode =getBoolean("archive_mode");
        return archive_mode;
    }

    public void setArchive_mode(boolean archive_mode) {
        if(context!=null)
            saveBoolean("archive_mode", archive_mode);
        this.archive_mode = archive_mode;
    }

    private void saveString(String key, String value){
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(key, value);
        edit.commit();
    }

    private String getString(String key){
        return sharedPreferences.getString(key,null);
    }

    private void saveInt(String key,int value){
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt(key, value);
        edit.commit();
    }

    private void saveLong(String key,long value){
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putLong(key, value);
        edit.commit();
    }

    private long getLong(String key){
        long defaultValue=0;
        return sharedPreferences.getLong(key,defaultValue);
    }

    private int getInt(String key){
        return sharedPreferences.getInt(key,0);
    }


    private void saveBoolean(String key,boolean value){
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    private boolean getBoolean(String key){
        return sharedPreferences.getBoolean(key,true);
    }

    public String getCountry() {
    if(context!=null)
        country=getString("country");
        return country;
    }

    public void setCountry(String country) {
        if(context!=null)
            saveString("country",country);
        this.country = country;
    }

    public String getRegion() {
        if(context!=null)
            region=getString("region");
        return region;
    }

    public void setRegion(String region) {
        if(context!=null)
            saveString("region",region);
        this.region = region;
    }

    public String getDistrict() {
        if(context!=null)
            district=getString("district");
        return district;
    }

    public void setDistrict(String district) {
        if(context!=null)
            saveString("district",district);
        this.district = district;
    }
}
