package com.hensongeodata.myagro360v2.model;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by user1 on 1/25/2018.
 */

public class FileField extends Field {
    public static String FILE_IMAGE="image";
    public static String FILE_MULTI_IMAGE="multi_image";
    public static String FILE_VIDEO="video";
    public static String FILE_SCAN="scan";
    public static String FILE_OTHER="other";
    private String fileType;
    private String uri;
    private MultipartBody.Part file;
    private RequestBody rb_question_id;
    private RequestBody rb_type;


    public FileField(){}

    public FileField(MultipartBody.Part file, RequestBody rb_question_id, RequestBody rb_type) {
        this.file = file;
        this.rb_question_id = rb_question_id;
        this.rb_type = rb_type;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public MultipartBody.Part getFile() {
        return file;
    }

    public void setFile(MultipartBody.Part file) {
        this.file = file;
    }

    public RequestBody getRb_question_id() {
        return rb_question_id;
    }

    public void setRb_question_id(RequestBody rb_question_id) {
        this.rb_question_id = rb_question_id;
    }

    public RequestBody getRb_type() {
        return rb_type;
    }

    public void setRb_type(RequestBody rb_type) {
        this.rb_type = rb_type;
    }
}
