package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 9/27/2017.
 */

public class Neighbourhood {
    private String id;
    private String name;
    private String town_id;

    public Neighbourhood(){}

    public Neighbourhood(String id, String name, String town_id) {
        this.id = id;
        this.name = name;
        this.town_id = town_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTown_id() {
        return town_id;
    }

    public void setTown_id(String town_id) {
        this.town_id = town_id;
    }
}
