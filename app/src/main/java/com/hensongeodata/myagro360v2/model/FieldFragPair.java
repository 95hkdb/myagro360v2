package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 2/18/2018.
 */

public class FieldFragPair {
    private String field_id;
    private String frag_tag;

    public FieldFragPair(String field_id, String frag_tag) {
        this.field_id = field_id;
        this.frag_tag = frag_tag;
    }

    public String getField_id() {
        return field_id;
    }

    public void setField_id(String field_id) {
        this.field_id = field_id;
    }

    public String getFrag_tag() {
        return frag_tag;
    }

    public void setFrag_tag(String frag_tag) {
        this.frag_tag = frag_tag;
    }
}
