package com.hensongeodata.myagro360v2.model;

import java.util.ArrayList;

/**
 * Created by user1 on 1/27/2018.
 */

public class Validation {
    private boolean error =false;//by default, no error is error
    private String message;
    private ArrayList results;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList getResults() {
        return results;
    }

    public void setResults(ArrayList results) {
        this.results = results;
    }
}
