package com.hensongeodata.myagro360v2.model;

import android.content.Context;

import com.hensongeodata.myagro360v2.Interface.TaskListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.CreateForm;

import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by user1 on 7/16/2018.
 */

public class Pauline extends Chatbot {
    //Assists users in scanning FAW
    TaskListener taskListener;
    Context context;
    public static String name="Wence";
    String batch_id;

    public Pauline(Context context){
        this.context=context;
    }

    public TaskListener getTaskListener() {
        return taskListener;
    }

    public void setTaskListener(TaskListener taskListener) {
        this.taskListener = taskListener;
    }

    public ArrayList<InstructionScout> getChatScout(){

        ArrayList<InstructionScout> items=new ArrayList<>();
        int defaultState=InstructionScout.STATE_UNPROCCESSED;

        String user=(new User(context)).getFirstname();
        if(user==null||user.trim().isEmpty())
            user=(new User(context)).getLastname();

        if(user==null||user.trim().isEmpty())
            user="";

        String lang=(new SettingsModel(context)).getLang();
        if(lang.equalsIgnoreCase(SettingsModel.LAN_FR)) {

            if(MyApplication.tts!=null) {
                MyApplication.tts.setLanguage(Locale.FRENCH);
              //  MyApplication.tts.setSpeechRate(0.7f);
            }

            items.add(new InstructionScout(resolveString(R.string.hello_fr) + " " + user + ", " +
                    resolveString(R.string.i_am_fr)
                    + " " + name + " " + resolveString(R.string.ai_scout_txt1_fr),
                    new Image(R.drawable.scout_image), InstructionScout.ACTION_INSTRUCT, defaultState));

            items.add(new InstructionScout(resolveString(R.string.ai_scout_txt2_fr),
                    new Image(R.drawable.scout_image), InstructionScout.ACTION_INSTRUCT, defaultState));

            for (int a = 0; a < 5; a++) {
                items.add(new InstructionScout(resolveString(R.string.ai_scout_txt3_fr)+ " " + (a + 1),
                        new Image(R.drawable.scout_image), InstructionScout.ACTION_INFO, defaultState));
                for (int b = 0; b < 10; b++) {
                    items.add(new InstructionScout(resolveString(R.string.are_you_at_fr) + " "
                            + resolveString(R.string.station_fr) + " " + (a + 1) + " " + resolveString(R.string.plant_fr) + " " + (b + 1),
                            new Image(R.drawable.scout_image), InstructionScout.ACTION_TASK, defaultState));

                    InstructionScout instruction=new InstructionScout("",
                            new Image(R.drawable.scout_image), InstructionScout.ACTION_SCAN, defaultState);
                    instruction.setPlant_number(b+1);
                    instruction.setPoint_number(a+1);
                    items.add(instruction);
                }
            }

        }
        else {
            if(MyApplication.tts!=null)
                MyApplication.tts.setLanguage(Locale.ENGLISH);

            items.add(new InstructionScout(resolveString(R.string.hello) + " " + user + ", " +
                    resolveString(R.string.i_am)
                    + " " + name + " " + resolveString(R.string.ai_scout_txt1),
                    new Image(R.drawable.scout_image), InstructionScout.ACTION_INSTRUCT, defaultState));

            items.add(new InstructionScout(resolveString(R.string.ai_scout_txt2),
                    new Image(R.drawable.scout_image), InstructionScout.ACTION_INSTRUCT, defaultState));

            for (int a = 0; a < 5; a++) {
                items.add(new InstructionScout(resolveString(R.string.ai_scout_txt3)+ " " + (a + 1),
                        new Image(R.drawable.scout_image), InstructionScout.ACTION_INFO, defaultState));
                for (int b = 0; b < 10; b++) {
                    items.add(new InstructionScout(resolveString(R.string.are_you_at) + " "
                            + resolveString(R.string.station) + " " + (a + 1) + " " + resolveString(R.string.plant) + " " + (b + 1),
                            new Image(R.drawable.scout_image), InstructionScout.ACTION_TASK, defaultState));

                    InstructionScout instruction=new InstructionScout("",
                            new Image(R.drawable.scout_image), InstructionScout.ACTION_SCAN, defaultState);
                    instruction.setPlant_number(b+1);
                    instruction.setPoint_number(a+1);
                    items.add(instruction);
                }
            }
        }

       SettingsModel settings=new SettingsModel(context);
        long saved_id=settings.getScan_batch();
        saved_id++;
        batch_id=validate(saved_id+""+(new CreateForm(context)).getCurrentTime());
        long lastItem_id=settings.getScan_item();

        int a=0;
        for(InstructionScout instructionScout: items){
            instructionScout.setId(lastItem_id+"");
            instructionScout.setScout_id(batch_id);
            lastItem_id++;

            items.set(a,instructionScout);
            a++;
        }

        settings.setScan_item(lastItem_id);
        settings.setScan_batch(saved_id);

        return items;
    }

    public ArrayList<InstructionScout> getChatMap(){

        ArrayList<InstructionScout> items=new ArrayList<>();
        int defaultState=InstructionScout.STATE_UNPROCCESSED;
        String user=(new User(context)).getFirstname();
        if(user==null||user.trim().isEmpty())
            user=(new User(context)).getLastname();

        if(user==null||user.trim().isEmpty())
            user="";


        String lang=(new SettingsModel(context)).getLang();
        if(lang.equalsIgnoreCase(SettingsModel.LAN_FR)) {

            if (MyApplication.tts != null) {
                MyApplication.tts.setLanguage(Locale.FRENCH);
              //  MyApplication.tts.setSpeechRate(0.7f);
            }

            items.add(new InstructionScout(resolveString(R.string.hello_fr) + " " + user + ", " +
                    resolveString(R.string.i_am_fr) + " " + name + " " + resolveString(R.string.ai_map_txt1_fr),
                    new Image(R.drawable.scout_image), InstructionScout.ACTION_INSTRUCT, defaultState));

            items.add(new InstructionScout(resolveString(R.string.ai_map_txt2_fr)+" spot 1",
                    new Image(R.drawable.scout_image), InstructionScout.ACTION_INSTRUCT, defaultState));

            for (int b = 0; b < 15; b++) {
                items.add(new InstructionScout(resolveString(R.string.ai_map_txt4_fr) + " spot " + (b + 1),
                        new Image(R.drawable.scout_image), InstructionScout.ACTION_INSTRUCT, defaultState));

                items.add(new InstructionScout(resolveString(R.string.are_you_at_fr) + " spot " + (b + 1),
                        new Image(R.drawable.scout_image), InstructionScout.ACTION_TASK, defaultState));

                items.add(new InstructionScout("",
                        new Image(R.drawable.scout_image), InstructionScout.ACTION_SCAN, defaultState));
            }
        }
        else {
            if (MyApplication.tts != null)
                MyApplication.tts.setLanguage(Locale.ENGLISH);

            items.add(new InstructionScout(resolveString(R.string.hello) + " " + user + ", " +
                    resolveString(R.string.i_am) + " " + name + " " + resolveString(R.string.ai_map_txt1),
                    new Image(R.drawable.scout_image), InstructionScout.ACTION_INSTRUCT, defaultState));

            items.add(new InstructionScout(resolveString(R.string.ai_map_txt2)+" spot 1",
                    new Image(R.drawable.scout_image), InstructionScout.ACTION_INSTRUCT, defaultState));

                items.add(new InstructionScout(resolveString(R.string.ai_map_txt4) + " spot " + (1),
                        new Image(R.drawable.scout_image), InstructionScout.ACTION_INSTRUCT, defaultState));

                items.add(new InstructionScout("Are you at spot 1",
                    new Image(R.drawable.scout_image), InstructionScout.ACTION_TASK, defaultState));

            items.add(new InstructionScout("",
                    new Image(R.drawable.scout_image), InstructionScout.ACTION_SCAN, defaultState));

                items.add(new InstructionScout("Now, keep walking and press \'done\' when you get to the end",
                    new Image(R.drawable.scout_image), InstructionScout.ACTION_TASK, defaultState));

            items.add(new InstructionScout("",
                    new Image(R.drawable.scout_image), InstructionScout.ACTION_SCAN, defaultState));

            for (int b = 0; b < 500; b++) {
//                items.add(new InstructionScout(resolveString(R.string.ai_map_txt4) + " spot " + (b + 1),
//                        new Image(R.drawable.scout_image), InstructionScout.ACTION_INSTRUCT, defaultState));
//
//                items.add(new InstructionScout(resolveString(R.string.are_you_at) + " spot " + (b + 1),
//                        new Image(R.drawable.scout_image), InstructionScout.ACTION_TASK, defaultState));

                items.add(new InstructionScout("Capturing next spot.",
                        new Image(R.drawable.scout_image), InstructionScout.ACTION_TASK, defaultState));

                items.add(new InstructionScout("",
                        new Image(R.drawable.scout_image), InstructionScout.ACTION_SCAN, defaultState));
            }

        }
        SettingsModel settings=new SettingsModel(context);
        long saved_id=settings.getScan_batch();
        saved_id++;

        batch_id=saved_id+""+String.valueOf(UUID.randomUUID()).substring(0,10);

             //   validate(saved_id+""+(new CreateForm(context)).getCurrentTime());
        long lastItem_id=settings.getMap_item();

        int a=0;
        for(InstructionScout instructionMap: items){
            instructionMap.setId(lastItem_id+"");

            instructionMap.setScout_id(batch_id);
            lastItem_id++;

            items.set(a,instructionMap);
            a++;
        }

        settings.setMap_item(lastItem_id);
        settings.setMap_batch(saved_id);

        return items;
    }


    public String getScoutID(){
        return batch_id;
    }

    protected String resolveString(int id){
        return  context.getResources().getString(id);
    }

    private String validate(String fileName){
        fileName=fileName.replaceAll("\\s","");
        fileName=fileName.replaceAll(":","");
        fileName=fileName.trim();

        return fileName;
    }
}