package com.hensongeodata.myagro360v2.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity( tableName = MapModel.TABLE_NAME)
public class MapModel {

    public static final String TABLE_NAME = "maps";

    public static final String COLUMN_ID   = "id";
    public static final String COLUMN_MAP_ID  = "map_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_TYPE= "type";
    public static final String COLUMN_SYNCED= "synced";
    public static final String COLUMN_USER_ID= "user_id";
    public static final String COLUMN_ORG_ID= "org_id";

    public static final String COLUMN_SIZE_AC= "size_in_acres";
    public static final String COLUMN_SIZE_KM= "size_in_km";
    public static final String COLUMN_SIZE_M= "size_in_m";
    public static final String COLUMN_CREATED_DATE= "created_date";

    public static String TYPE_POINT="point";
    public static String TYPE_LINE="line";
    public static String TYPE_POLYGON="polygon";

    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo( name = COLUMN_MAP_ID)
    public long mapId;

    @ColumnInfo( name = COLUMN_ID)
    public String id;

    @ColumnInfo( name = COLUMN_NAME)
    public String name;

    @ColumnInfo(name = COLUMN_TYPE)
    public String type;

    @ColumnInfo( name = COLUMN_SIZE_M)
    private double sizeM;

    @ColumnInfo( name = COLUMN_SIZE_AC)
    private double sizeAc;

    @ColumnInfo( name = COLUMN_SIZE_KM)
    private double sizeKm;

    @ColumnInfo(name = COLUMN_SYNCED)
    public boolean synced;

    @ColumnInfo(name = COLUMN_USER_ID)
    public String userId;

    @ColumnInfo(name = COLUMN_ORG_ID)
    public String orgId;

    @SerializedName("created_date")
    @ColumnInfo(name = COLUMN_CREATED_DATE)
    public String createdDate;

    public long getMapId() {
        return mapId;
    }

    public void setMapId(long mapId) {
        this.mapId = mapId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public double getSizeM() {
        return sizeM;
    }

    public void setSizeM(double sizeM) {
        this.sizeM = sizeM;
    }

    public double getSizeAc() {
        return sizeAc;
    }

    public void setSizeAc(double sizeAc) {
        this.sizeAc = sizeAc;
    }

    public double getSizeKm() {
        return sizeKm;
    }

    public void setSizeKm(double sizeKm) {
        this.sizeKm = sizeKm;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
