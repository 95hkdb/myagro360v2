package com.hensongeodata.myagro360v2.model;

import java.util.ArrayList;

/**
 * Created by user1 on 8/7/2018.
 */

public class ProvidersPesticide {
    private String title;
    private String type;
    private String phone;
    private String location;
    private ArrayList<Pesticide> pesticides;


    public ProvidersPesticide(String title, String type, String phone, String location, ArrayList<Pesticide> pesticides) {
        this.title = title;
        this.type = type;
        this.phone = phone;
        this.location = location;
        this.pesticides = pesticides;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ArrayList<Pesticide> getPesticides() {
        return pesticides;
    }

    public void setPesticides(ArrayList<Pesticide> pesticides) {
        this.pesticides = pesticides;
    }
}
