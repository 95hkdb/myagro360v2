package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 1/25/2018.
 */

public class Organization {
    private String id;
    private String name;
    private String description;
    private String formCount;
    private String color;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFormCount() {
        return formCount;
    }

    public void setFormCount(String formCount) {
        this.formCount = formCount;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
