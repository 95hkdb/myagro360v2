package com.hensongeodata.myagro360v2.model;

import java.util.ArrayList;

/**
 * Created by user1 on 1/24/2018.
 */

public class RadioField extends Field {
    private ArrayList<String>options;

    public RadioField(){}

    public ArrayList<String> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<String> options) {
        this.options = options;
    }

}
