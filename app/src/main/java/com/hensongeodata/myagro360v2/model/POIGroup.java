package com.hensongeodata.myagro360v2.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 3/7/2018.
 */

public class POIGroup {
    private List<POI> poiList;
    private boolean select;
    private String id;
    private String name;

    public POIGroup(){
        poiList=new ArrayList<>();
        select=false;
        name="";
        id="";
    }

    public List<POI> getPoiList() {
        return poiList;
    }

    public void setPoiList(List<POI> poiList) {
        this.poiList = poiList;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
