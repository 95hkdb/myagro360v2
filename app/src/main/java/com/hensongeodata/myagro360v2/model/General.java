package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 9/26/2017.
 */

public class General {
    private int id;
    private String town;
    private String neighbourhood;
    private String place;
    private String facility_type;
    private String service_type;

    public General(){}

    public General(String town, String neighbourhood, String place, String facility_type, String service_type) {
        this.town = town;
        this.neighbourhood = neighbourhood;
        this.place = place;
        this.facility_type = facility_type;
        this.service_type = service_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getNeighbourhood() {
        return neighbourhood;
    }

    public void setNeighbourhood(String neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getFacility_type() {
        return facility_type;
    }

    public void setFacility_type(String facility_type) {
        this.facility_type = facility_type;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }
}
