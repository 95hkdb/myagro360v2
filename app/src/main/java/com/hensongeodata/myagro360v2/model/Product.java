package com.hensongeodata.myagro360v2.model;

import java.util.ArrayList;
import java.util.List;

public class Product {
    public String id;
    public String name;
    public long price;
    public String description;
    public String owner_name;
    public String image;
    public List<Image> images;
    public int quantity=1;
    public int status=STATUS_NON;
    public String category;
    public POI poi;
    public int max=20;
    public String order_number;

    public static int STATUS_NON=100;
    public static int STATUS_BOUGHT=200;
    public static int STATUS_CART=300;

    public Product(){
        images=new ArrayList<>();
    }


}
