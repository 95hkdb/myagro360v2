package com.hensongeodata.myagro360v2.model;

import android.content.Context;

import com.hensongeodata.myagro360v2.R;

public class ScanFAW {
    private String id;
    private String header;
    private String message;
    private int response=-1;
    private Image image;
    private Context context;
    public ScanFAW(Context context){
        this.context=context;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        if(message==null&&context!=null)
            message=context.getResources().getString(R.string.faw_msg_default);
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getResponse() {
        return response;
    }

    public void setResponse(int response) {
        this.response = response;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getHeader() {
        if(header==null&&context!=null)
            header=context.getResources().getString(R.string.scan);
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }
}
