package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 1/25/2018.
 */

public class Form {
    private String id;
    private Organization organization;
    private String title;
    private String description;
    private String status;
    private String access_code;
    public int mapping;
    public static int MAPPING_ENABLED=1;
    public static int MAPPING_DISABLED=0;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccess_code() {
        return access_code;
    }

    public void setAccess_code(String access_code) {
        this.access_code = access_code;
    }
}
