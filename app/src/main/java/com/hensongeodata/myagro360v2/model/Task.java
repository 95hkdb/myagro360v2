package com.hensongeodata.myagro360v2.model;

import com.hensongeodata.myagro360v2.Interface.TaskListener;

/**
 * Created by user1 on 7/16/2018.
 */

public class Task extends Information {
    TaskListener taskListener;
    int id; //essentially the position of the task in the list of executable infos, instructions and tasks
    public Task(String message, int action) {
        super(message, action);
    }

    public TaskListener getTaskListener() {
        return taskListener;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTaskListener(TaskListener taskListener) {
        this.taskListener = taskListener;
    }
}
