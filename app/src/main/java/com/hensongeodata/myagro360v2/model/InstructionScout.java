package com.hensongeodata.myagro360v2.model;

/**
 * Created by user1 on 7/16/2018.
 */

public class InstructionScout extends Instruction{
    protected String scout_id;
    private int plant_number;
    private int point_number;

    public static int ACTION_SCAN =400;

    public InstructionScout(String message, Image image, int action, int status) {
        super(message, image, action, status);
    }

    public InstructionScout(String message, Image image, int action, int status, String id, String scout_id) {
        this.message = message;
        this.image = image;
        this.action = action;
        this.status = status;
        this.id = id;
        this.scout_id=scout_id;
    }

    public int getPlant_number() {
        return plant_number;
    }

    public void setPlant_number(int plant_number) {
        this.plant_number = plant_number;
    }

    public int getPoint_number() {
        return point_number;
    }

    public void setPoint_number(int point_number) {
        this.point_number = point_number;
    }

    public String getScout_id() {
        return scout_id;
    }

    public void setScout_id(String scout_id) {
        this.scout_id = scout_id;
    }

    public int getId_Integer(){
        if(id!=null&&!id.trim().isEmpty())
                return Integer.parseInt(id);
        else
            return -1;
    }

    public InstructionScout(){}

    public InstructionScout(String message) {
        this.message = message;
    }

    public InstructionScout(String message, int action) {
        this.message = message;
        this.action = action;
    }
}
