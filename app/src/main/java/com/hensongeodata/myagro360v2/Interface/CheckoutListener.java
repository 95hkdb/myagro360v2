package com.hensongeodata.myagro360v2.Interface;

public interface CheckoutListener {
    void add(int atIndex);
    void subtract(int atIndex);
    void remove(int atIndex);
}
