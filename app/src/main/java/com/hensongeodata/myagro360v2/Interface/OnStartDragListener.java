package com.hensongeodata.myagro360v2.Interface;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by user1 on 3/2/2018.
 */

public interface OnStartDragListener {


    void onStartDrag(RecyclerView.ViewHolder viewHolder);

}