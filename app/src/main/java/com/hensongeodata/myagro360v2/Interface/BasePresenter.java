package com.hensongeodata.myagro360v2.Interface;

public interface BasePresenter {
      void subscribe();

      void unsubscribe();
}
