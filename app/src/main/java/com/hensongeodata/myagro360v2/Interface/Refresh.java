package com.hensongeodata.myagro360v2.Interface;

public interface Refresh {
    void reload(String scout_id);

    void moreClicked(String title, String sectionId);

    void shareButtonClicked(String section_id);

    void onSyncButtonClicked();

}
