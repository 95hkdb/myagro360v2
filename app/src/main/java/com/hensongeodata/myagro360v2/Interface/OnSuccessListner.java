package com.hensongeodata.myagro360v2.Interface;

/**
 * Created by user1 on 3/2/2018.
 */

public interface OnSuccessListner {

      void onSuccess(boolean flag);

      void onError();

      void onPrepare();
}