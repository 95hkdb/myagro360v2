package com.hensongeodata.myagro360v2.Interface;

/**
 * Created by user1 on 3/2/2018.
 */

public interface ItemTouchHelperViewHolder {

    void onItemSelected();

    void onItemClear();
}