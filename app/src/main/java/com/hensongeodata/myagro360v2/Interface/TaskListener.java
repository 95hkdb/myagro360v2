package com.hensongeodata.myagro360v2.Interface;

/**
 * Created by user1 on 8/6/2017.
 */

public interface TaskListener {
    void onTaskAccepted(String id);
    void onTaskStarted(String id);
    void onTaskStopped(String id,boolean complete);
    void onTaskCancelled(String id);
}
