package com.hensongeodata.myagro360v2.Interface;

import com.hensongeodata.myagro360v2.model.Product;

import java.util.List;

/**
 * Created by user1 on 6/19/2018.
 */

public interface ProductsListener {
      void loaded(int status, List<Product> products);
}
