package com.hensongeodata.myagro360v2.version2_0.Events;

public  class KBDownloadCompleted {
      private boolean failed;

      public KBDownloadCompleted(boolean failed) {
            this.failed = failed;
      }

      public boolean isFailed() {
            return failed;
      }

      public void setFailed(boolean failed) {
            this.failed = failed;
      }
}
