package com.hensongeodata.myagro360v2.version2_0.Fragment;


import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.DialogFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.response.MapStatusResponse;
import com.hensongeodata.myagro360v2.api.response.PlotResponse;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.POI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.hensongeodata.myagro360v2.controller.DatabaseHelper.SECTION_ID;


public class AttachMapDialogFragment extends DialogFragment {

      private static final String TAG = AttachMapDialogFragment.class.getSimpleName();
      private static final String MAP_NAME = "map_name";

      private DatabaseHelper databaseHelper;

      private AgroWebAPI mAgroWebAPI;
      private AppCompatSpinner plots_spinner;
      private String sectionId;
      private WebView webView;
      private ImageButton btnClose;
      private Button btnSave;
      private EditText acreageEditTtext;
      private LinearLayout choosePlotContainer;
      HashMap<Integer, String> spinnerMap;
      private EditText nameEditTtext;
      private EditText attachedToEditText;
      private ImageButton imageButton;

      private String plotId;

      public AttachMapDialogFragment() {
      }

      public static AttachMapDialogFragment newInstance(String sectionId) {
            AttachMapDialogFragment fragment = new AttachMapDialogFragment();
            Bundle args = new Bundle();
            args.putString(SECTION_ID, sectionId);
            fragment.setArguments(args);
            return fragment;
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            mAgroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);
            View view = inflater.inflate(R.layout.fragment_attach_map_dialog, container, false);

            databaseHelper = new DatabaseHelper(getContext());
            plots_spinner = view.findViewById(R.id.spn_plots);
            webView = view.findViewById(R.id.webview);
            btnSave = view.findViewById(R.id.bt_save);
            btnClose = view.findViewById(R.id.bt_close);
            nameEditTtext = view.findViewById(R.id.map_name);
            acreageEditTtext = view.findViewById(R.id.acreage);
            attachedToEditText = view.findViewById(R.id.attached_to);
            imageButton              = view.findViewById(R.id.attached_to_edit);
            choosePlotContainer = view.findViewById(R.id.choose_plot_container);

            final boolean[] editActive = {false};

            btnClose.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        dismiss();
                  }
            });

            btnSave.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        attachMapToPlot(plotId);
                  }
            });

            imageButton.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        if (!editActive[0]){
                              imageButton.setImageResource(R.drawable.ic_edit_white_24dp);
                              imageButton.setBackgroundColor(getActivity().getResources().getColor(R.color.colorGreen005));
                              choosePlotContainer.setVisibility(View.VISIBLE);
                              editActive[0] = true;

                              plots_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                          plotId = spinnerMap.get(plots_spinner.getSelectedItemPosition());
                                          attachedToEditText.setTag(plotId);
                                          Log.d(TAG, "onItemSelected:  " + plotId);
                                          Log.d(TAG, "onItemSelected:  " + plots_spinner.getSelectedItem().toString());
                                          Log.d(TAG, "onItemSelected:  " + sectionId);
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {}
                              });

                        }else{
                              imageButton.setImageResource(R.drawable.ic_edit_black_24dp);
                              imageButton.setBackgroundColor(getActivity().getResources().getColor(R.color.grey));
                              choosePlotContainer.setVisibility(View.GONE);
                              editActive[0] = false;
                        }
                  }
            });

            getPlotsFromAPI();

            loadMap(sectionId);

            checkMapStatus(sectionId);

            showPerimeter();

            setMapName();


            return view;
      }

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            if (getArguments() != null) {
                  sectionId = getArguments().getString(SECTION_ID);
            }

      }

      private void getPlotsFromAPI() {

            mAgroWebAPI.getPlots()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<PlotResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                          }

                          @Override
                          public void onNext(PlotResponse plotResponse) {

                                String[] spinnerArray = new String[plotResponse.getPlots().size()];

                                try {

                                      spinnerMap = new HashMap<Integer, String>();
                                      for (int i = 0; i < plotResponse.getPlots().size(); i++) {
                                            spinnerMap.put(i, String.valueOf(plotResponse.getPlots().get(i).getId()));
                                            spinnerArray[i] = plotResponse.getPlots().get(i).getName();
                                      }

                                      setupArrayAdapter(spinnerArray, plots_spinner);

                                } catch (Exception e) {
                                      Log.e(TAG, "onNext:  " + e.getMessage());
                                }

                          }

                          @Override
                          public void onError(Throwable e) {

                          }

                          @Override
                          public void onComplete() {

                          }
                    });
      }

      private void setupArrayAdapter(String[] stringArray, AppCompatSpinner spinner) {
            ArrayAdapter<String> array = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_item, stringArray);
            array.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(array);
//        spinner.setSelection(0);
      }

      private void loadMap(String section_id) {

            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setDisplayZoomControls(false);
            webView.loadUrl(Keys.AGRO_BASE + "faw-api/get-map/?mapping_id=" + section_id);
            webView.setWebViewClient(new WebViewClient() {

                  @Override
                  public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
//                        progressBar.setVisibility(View.GONE);
                  }

                  @TargetApi(Build.VERSION_CODES.LOLLIPOP)


                  @Override
                  public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                        view.loadUrl(request.getUrl().toString());
                        return false;
                  }

                  @Override
                  public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);
                  }

                  @Override
                  public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                        super.onReceivedError(view, request, error);
//                        Log.d(TAG,"error");
//                        btnRetry.setVisibility(View.VISIBLE);
                  }

                  @Override
                  public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                        super.onReceivedSslError(view, handler, error);
//                        Log.d(TAG,"ssl error");
                        // btnRetry.setVisibility(View.VISIBLE);
                  }

                  @Override
                  public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                        super.onReceivedHttpError(view, request, errorResponse);
//                        Log.d(TAG,"http error");
                        //btnRetry.setVisibility(View.VISIBLE);
                  }
            });
      }

      private void attachMapToPlot(String plot_id) {

//            String plot_id = spinnerMap.get(plots_spinner.getSelectedItemPosition());

            Call<JsonElement> call = mAgroWebAPI.attachPlotToMap(sectionId, String.valueOf(plot_id));

            call.enqueue(new Callback<JsonElement>() {
                  @Override
                  public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        Log.i(TAG, "onResponse:  " + response.body());
                        Toast.makeText(getContext(), "Sucessfully attached map to plot!", Toast.LENGTH_SHORT).show();
                        dismiss();
                  }

                  @Override
                  public void onFailure(Call<JsonElement> call, Throwable t) {
                        Log.e(TAG, "onFailure:  " + t.getMessage());


                  }
            });
      }

      private void checkMapStatus(String mapping_id){
            mAgroWebAPI.checkMapStatus(mapping_id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<MapStatusResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {

                          }

                          @Override
                          public void onNext(MapStatusResponse mapStatusResponse) {
                                if (mapStatusResponse.getStatus() != 0 ){
                                      attachedToEditText.setText(mapStatusResponse.getPlot_name());
                                      attachedToEditText.setTag(mapStatusResponse.getPlot_id());
                                }
                          }

                          @Override
                          public void onError(Throwable e) {

                          }

                          @Override
                          public void onComplete() {

                          }
                    });
      }

      private void showPerimeter() {

            List<InstructionScout> getAllItems = databaseHelper.getScoutListAllStates(sectionId, ChatbotAdapterRV.MODE_MAP);
            ArrayList<String> strPois = new ArrayList<>();

            int index2 = 0;
            for (InstructionScout instructionScout : getAllItems) {
                  if (instructionScout != null) {
                        String message = instructionScout.getMessage();
                        if (message != null && !message.trim().isEmpty())
                              strPois.add(message);
                  }
                  index2++;
            }

            ArrayList<POI> pois = CreateForm.getInstance(getContext()).buildPOI_Mapping(strPois);
            int area = MyApplication.getArea(pois);
            long perimeter = MyApplication.getPerimeter(pois);
            double inKilometers = metersToKilo(perimeter);

            acreageEditTtext.setText(String.valueOf(metersToAcres(perimeter)));
      }

      private double metersToAcres(long meterSquared) {
            return Math.round((meterSquared * 0.00024711) * 1000.0) / 1000.0;
      }

      private double metersToHectares(long mettersSquared) {
            return mettersSquared * 0.0001;
      }

      private double metersToKilometers(long metersSquared) {
            return metersSquared * 0.000001;
      }

      private double metersToKilo(long metersSquared) {
            return metersSquared * 0.001;
      }

      private void setMapName() {
            if (databaseHelper.getMapping(sectionId) != null) {
                  String name = databaseHelper.getMapping(sectionId).getName();
                  if (name != null) {
                        nameEditTtext.setText(name);
                  }
            }
      }
}
