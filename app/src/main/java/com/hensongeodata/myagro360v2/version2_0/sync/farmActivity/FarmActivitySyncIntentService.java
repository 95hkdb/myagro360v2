package com.hensongeodata.myagro360v2.version2_0.sync.farmActivity;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

import org.json.JSONException;

public class FarmActivitySyncIntentService extends IntentService {

      public FarmActivitySyncIntentService() {
            super("MapSyncIntentService");
      }

      @Override
      protected void onHandleIntent(@Nullable Intent intent) {
            try {
                  FarmActivitySyncTask.pushAllFarmActivities(this);
            } catch (JSONException e) {
                  e.printStackTrace();
            }
      }
}
