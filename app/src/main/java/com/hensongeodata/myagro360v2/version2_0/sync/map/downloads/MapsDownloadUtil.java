package com.hensongeodata.myagro360v2.version2_0.sync.map.downloads;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.work.BackoffPolicy;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import java.util.concurrent.TimeUnit;

public class MapsDownloadUtil {
      private static final int SYNC_INTERVAL_HOURS = 1;
      private static final int SYNC_INTERVAL_SECONDS = (int) TimeUnit.HOURS.toSeconds(SYNC_INTERVAL_HOURS);
      private static final int SYNC_FLEXTIME_SECONDS = SYNC_INTERVAL_SECONDS / 3;

      private static boolean sInitialized;

      static void ScheduleWorker(@NonNull final Context context){

            PeriodicWorkRequest request = new PeriodicWorkRequest.Builder(MapsDownloadWorker.class, 30, TimeUnit.MINUTES)
                    .setInitialDelay(5, TimeUnit.SECONDS)
                    .setBackoffCriteria(BackoffPolicy.EXPONENTIAL, 1, TimeUnit.MINUTES)
                    .build();

            WorkManager.getInstance(context).enqueueUniquePeriodicWork("MyAgro360MapsDownload", ExistingPeriodicWorkPolicy.KEEP, request);
      }

      synchronized public static void initialize(@NonNull final Context context){

            if (sInitialized) return;

            sInitialized = true;

            ScheduleWorker(context);

//            new Thread(() -> {
//                  startImmediateSync(context);
//            }).start();
      }

      public static void startImmediateSync(@NonNull final Context context){
            Intent intentToSyncImmediately = new Intent(context, MapsDownloadIntentService.class);
            context.startService(intentToSyncImmediately);
      }
}
