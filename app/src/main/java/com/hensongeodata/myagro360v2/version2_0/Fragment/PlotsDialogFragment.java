package com.hensongeodata.myagro360v2.version2_0.Fragment;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hensongeodata.myagro360v2.Interface.HistoryTabListener;
import com.hensongeodata.myagro360v2.Interface.Refresh;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Adapter.PlotsRecyclerviewAdapter;
import com.hensongeodata.myagro360v2.version2_0.Listeners.PlotsRvListener;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class PlotsDialogFragment extends DialogFragment implements HistoryTabListener,Refresh {

      private static final String TAG = PlotsDialogFragment.class.getSimpleName();
      private MapDatabase mapDatabase;
      private MainViewModel mMainViewModel;
      private RecyclerView mRecyclerView;
      private Button addNewPlotBtn;
      private FloatingActionButton fabButton;
      private LinearLayout linearLayout;
      private PlotsRecyclerviewAdapter adapter;
      private TextView numberOfPlotsTextView;
      private TextView totalAcreageTextView;
      private ImageView sortImageButton;
      private LinearLayout configureLayout;
      private Button configureFilterButton;
      private ImageView noActivityImageView;
      private TextView     briefTextView;
      private TextView     titleTextView;
      private ImageButton btnClose;

      private OnPlotsFragmentListener onPlotsFragmentListener;

      public PlotsDialogFragment() {}

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
      }

      @NonNull
      @Override
      public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
            return super.onCreateDialog(savedInstanceState);
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_plots_dialog, container, false);

            mapDatabase         = MapDatabase.getInstance(getContext());
            mMainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            mRecyclerView = view.findViewById(R.id.plot_history_recyclerview);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

            btnClose = view.findViewById(R.id.bt_close);
            fabButton = view.findViewById(R.id.fab_add);
            linearLayout = view.findViewById(R.id.no_plots_linear_layout);
            addNewPlotBtn = view.findViewById(R.id.add_new_plot_btn);
            numberOfPlotsTextView = view.findViewById(R.id.number_of_plots);
            totalAcreageTextView = view.findViewById(R.id.total_acreage);
            sortImageButton          = view.findViewById(R.id.sort_button);
            configureLayout           = view.findViewById(R.id.configure_layout);
            configureFilterButton  = view.findViewById(R.id.configure_filter);
            noActivityImageView = view.findViewById(R.id.image);
            briefTextView                = view.findViewById(R.id.brief);
            titleTextView                 = view.findViewById(R.id.title);

            fabButton.setOnClickListener(v -> addNewPlot());

            addNewPlotBtn.setOnClickListener(v -> addNewPlot());

            configureFilterButton.setOnClickListener(v -> PlotsFragmentSortButtonClicked());

            sortImageButton.setOnClickListener(v -> PlotsFragmentSortButtonClicked());

            loadPlots();
            calculateTotalAcreage();

            btnClose.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        dismiss();
                  }
            });

            return view;
      }

      private void loadPlots() {
            if (mMainViewModel != null){
                  mMainViewModel = null;
                  mMainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            }

            if (SharePrefManager.isSortPreferenceAvailable(getContext())){

                  switch (SharePrefManager.getSortPlotsPreference(getContext())){

                        case SharePrefManager.SORT_BY_CROP:
                              populatePlotsByCropType();
                              break;

                        case SharePrefManager.SORT_BY_PLOT_SIZE_DESC:
                              populatePlotsBySizeDesc();
                              break;

                        case SharePrefManager.SORT_BY_PLOT_SIZE_ASC:
                              populatePlotsBySizeAsc();
                              break;

                        case SharePrefManager.SORT_BY_PLOT_NAME_DESC:
                              populatePlotsByPlotNameDesc();
                              break;

                        case SharePrefManager.SORT_BY_PLOT_NAME_ASC:
                              populatePlotsByPlotNameAsc();
                              break;

                        case SharePrefManager.SORT_BY_DISTINCT:
                              populateDistinctPlots();
                              break;

                        case SharePrefManager.SORT_BY_LAST_TO_FIRST:

                        default:
                              populatePlotsByLastToFirst();

                  }
            }else {
                  populateAllPlots();
            }
      }

      private void addNewPlot(){
            onPlotsFragmentListener.onAddNewPlot();
      }

      private void plotsFragmentsMoreClicked(long id, String name, String plotId, String mapId){
            onPlotsFragmentListener.onPlotsFragmentMoreClicked( id, name, plotId, mapId );
      }

      private void PlotsFragmentSortButtonClicked(){
            onPlotsFragmentListener.onPlotsFragmentSortButtonClicked();
      }

      @Override
      public void onDetach() {
            super.onDetach();
            onPlotsFragmentListener = null;
      }

      @Override
      public void onSelected(String item) {}

      @Override
      public void reload(String scout_id) {

      }

      @Override
      public void moreClicked(String title, String sectionId) {

      }

      @Override
      public void shareButtonClicked(String section_id) {

      }

      @Override
      public void onSyncButtonClicked() {

      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
//            if (context instanceof PlotsDialogFragment.OnPlotsFragmentListener) {
//                  onPlotsFragmentListener = (PlotsDialogFragment.OnPlotsFragmentListener) context;
//            } else {
//                  throw new RuntimeException(context.toString()
//                          + " must implement OnPlotsFragmentListener");
//            }
      }

      private void populatePlotsByCropType() {
            String orgId = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);
            String cropId = SharePrefManager.getCropSortPreference(getContext());
            mMainViewModel.setPlotsByCrop(mapDatabase.plotDaoAccess().fetchAllPlotsByCropId(orgId,cropId));
            setMainViewModel(mMainViewModel.getPlotsByCrop());
      }

      private void populateAllPlots(){
            setMainViewModel(mMainViewModel.getPlots());
      }

      private void populatePlotsBySizeDesc(){
            setMainViewModel(mMainViewModel.getPlotsBySizeDesc());
      }

      private void populatePlotsBySizeAsc(){
            setMainViewModel(mMainViewModel.getPlotsBySizeAsc());
      }

      private void populatePlotsByPlotNameDesc(){
            setMainViewModel(mMainViewModel.getPlotsByNameDesc());
      }

      private void populatePlotsByPlotNameAsc(){
            setMainViewModel(mMainViewModel.getPlotsByNameAsc());
      }

      private void populatePlotsByLastToFirst(){
            setMainViewModel(mMainViewModel.getPlotsByDescOrder());
      }

      private void populateDistinctPlots(){
            setMainViewModel(mMainViewModel.getDistinctPlots());
      }

      private void setMainViewModel(LiveData<List<FarmPlot>> plotsBySizeDesc) {
            plotsBySizeDesc.observe(getActivity(), new Observer<List<FarmPlot>>() {
                  @SuppressLint("RestrictedApi")
                  @Override
                  public void onChanged(List<FarmPlot> farmPlotList) {

                        if (farmPlotList.size() != 0) {

                              numberOfPlotsTextView.setText(String.valueOf(farmPlotList.size()));
                              linearLayout.setVisibility(View.GONE);
                              fabButton.setVisibility(View.VISIBLE);
                              configureLayout.setVisibility(View.VISIBLE);

                              adapter = new PlotsRecyclerviewAdapter(getContext(), farmPlotList, PlotsDialogFragment.this, new PlotsRvListener() {
                                    @Override
                                    public void moreClicked(long id, String name, String plotId, String mapId) {
                                          plotsFragmentsMoreClicked(id, name, plotId, mapId);
                                    }

                                    @Override
                                    public void shareButtonClicked(long id, String name, String plotId, String mapId) {
//                                          Toast.makeText(getContext(), "Plots Share", Toast.LENGTH_SHORT).show();
                                    }
                              });

                              mRecyclerView.setAdapter(adapter);

                        } else {
                              configureLayout.setVisibility(View.GONE);
                              linearLayout.setVisibility(View.VISIBLE);
                              fabButton.setVisibility(View.GONE);


                              if (SharePrefManager.isSortPreferenceAvailable(getContext())){
                                    noActivityImageView.setVisibility(View.GONE);
                                    briefTextView.setText("No plot matches your filter. ");
                                    configureLayout.setVisibility(View.VISIBLE);
                                    addNewPlotBtn.setVisibility(View.GONE);
                                    configureFilterButton.setVisibility(View.VISIBLE);
                              }else {
                                    noActivityImageView.setVisibility(View.GONE);
                                    configureLayout.setVisibility(View.GONE);
                                    addNewPlotBtn.setVisibility(View.VISIBLE);
                                    configureFilterButton.setVisibility(View.GONE);

                              }
                        }
                  }
            });
      }

      private void calculateTotalAcreage(){

//            String userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);

            String userId = "";

            if (SharePrefManager.isSecondaryUserAvailable(getContext())){
                  userId = SharePrefManager.getSecondaryUserPref(getContext());
            }else {
                  userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
            }

            String orgId = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);
            AtomicLong totalSize = new AtomicLong();

            AppExecutors.getInstance().getDiskIO().execute(() -> totalSize.set(mapDatabase.plotDaoAccess().plotSizeSum( orgId)));

            new Handler().postDelayed(() -> totalAcreageTextView.setText(String.format("%s ac", String.valueOf(totalSize.get()))),2000);

      }

      public interface OnPlotsFragmentListener {

            void onAddNewPlot();

            void onPlotsFragmentMoreClicked(long id, String name, String plotId, String mapId);

            void onPlotsFragmentSortButtonClicked();

      }

      @Override
      public void onResume() {
            super.onResume();
            Log.i(TAG, "onResume:  PlotsFragment Restarted.." );
            loadPlots();
      }

      @Override
      public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
            Log.i(TAG, "onViewStateRestored:  PlotsFragment"  );
      }

      @Override
      public void onInflate(Context context, AttributeSet attrs, Bundle savedInstanceState) {
            super.onInflate(context, attrs, savedInstanceState);
            Log.i(TAG, "onInflate:  PlotsFragment");
      }

      private void sharePlotMap(String sectionId){
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, Keys.AGRO_BASE+"faw-api/get-map/?mapping_id="+sectionId);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
      }

}


