package com.hensongeodata.myagro360v2.version2_0.sync;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import java.util.concurrent.TimeUnit;

public class DBSyncUtil {
      private static final int SYNC_INTERVAL_HOURS = 1;
      private static final int SYNC_INTERVAL_SECONDS = (int) TimeUnit.HOURS.toSeconds(SYNC_INTERVAL_HOURS);
      private static final int SYNC_FLEXTIME_SECONDS = SYNC_INTERVAL_SECONDS / 3;

      private static boolean sInitialized;

      static void ScheduleWorker(@NonNull final Context context){

            Constraints constraints = new Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build();

            OneTimeWorkRequest dbSyncWork =
                    new OneTimeWorkRequest.Builder(DBSyncWorker.class)
                            .setConstraints(constraints)
                            .build();

            WorkManager.getInstance(context).enqueueUniqueWork("DBSYNC", ExistingWorkPolicy.KEEP,dbSyncWork);

      }

      synchronized public static void initialize(@NonNull final Context context){

            if (sInitialized) return;

            sInitialized = true;

            ScheduleWorker(context);

//            new Thread(() -> {
//                  startImmediateSync(context);
//            }).start();
      }

      public static void startImmediateSync(@NonNull final Context context){
            Intent intentToSyncImmediately = new Intent(context, DBSyncIntentService.class);
            context.startService(intentToSyncImmediately);
      }
}
