package com.hensongeodata.myagro360v2.version2_0.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.helper.ItemAnimation;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.version2_0.Adapter.AppNotificationAdapter;
import com.hensongeodata.myagro360v2.version2_0.Model.AppNotification;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;

/**
 * A simple {@link Fragment} subclass.
 */
public class AppNotificationFragment extends Fragment {

      private static final String TAG = AppNotification.class.getSimpleName();

      private CompositeDisposable disposable = new CompositeDisposable();
      private RecyclerView recyclerView;
      private LinearLayout progressLayout;
      private AppNotificationAdapter mAdapter;
      private List<AppNotification> items = new ArrayList<>();
      private int animation_type = ItemAnimation.BOTTOM_UP;
      private View parent_view;
      private AgroWebAPI mAgroWebAPI;
      private MainViewModel mainViewModel;
      private LinearLayout noNotificationsLinearLayout;
      private MapDatabase mapDatabase;

      public AppNotificationFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View view = inflater.inflate(R.layout.fragment_app_notification, container, false);

            parent_view = view.findViewById(android.R.id.content);
            progressLayout = view.findViewById(R.id.lyt_progress);
            mAgroWebAPI = NetworkRequest.getRetrofit(Keys.FAW_API).create(AgroWebAPI.class);
            mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            noNotificationsLinearLayout = view.findViewById(R.id.no_notifications_linear_layout);
            mapDatabase = MapDatabase.getInstance(getContext());
            initComponent(view);


            AppExecutors.getInstance().getDiskIO().execute(() -> {

                  AppNotification appNotification = new AppNotification();
                  appNotification.setName(AppNotification.NOTIFICATION_PLOTS);
                  appNotification.setMessage("Plots have been downloaded to your device");
                  appNotification.setOwner(AppNotification.NOTIFICATION_PLOTS);
                  appNotification.setDateCreated(System.currentTimeMillis());

//                  mapDatabase.notificationDaoAccess().insert(appNotification);

            });

            getNotificationsFromDatabase();



            return view;
      }

      private void initComponent(View view) {

            Context context = getContext();

            recyclerView = view.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setHasFixedSize(true);

            animation_type = ItemAnimation.BOTTOM_UP;
      }


      private void getNotificationsFromDatabase(){
            mainViewModel.getAppNotifications().observe(getActivity(), new Observer<List<AppNotification>>() {
                  @Override
                  public void onChanged(List<AppNotification> appNotifications) {
                        items = appNotifications;

                        Log.i(TAG, "onChanged:  App Notifications  Size " + appNotifications.size());


                        if (appNotifications.size() != 0){

                              setAdapter();
                              noNotificationsLinearLayout.setVisibility(View.GONE);

                              BottomNavigationView bottomNavigationViewMain  =  getActivity().findViewById(R.id.nav_view);
//        navView.getOrCreateBadge(R.id.action_add).setNumber(2);

                              final int[] unreadAlertsCount = new int[1];

                              AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                    @Override
                                    public void run() {
                                          unreadAlertsCount[0] = mapDatabase.notificationDaoAccess().notificationsUnReadCount();
                                    }
                              });

                              new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                          try {

                                                HelperClass.showBadge(getContext(), bottomNavigationViewMain, R.id.navigation_alert, String.valueOf(unreadAlertsCount[0]));

                                          }catch (Exception e){
                                                Log.e(TAG, "run:  " + e.getMessage() );
                                          }
                                    }
                              },2000);

                        }else {
                              progressLayout.setVisibility(View.GONE);
                              noNotificationsLinearLayout.setVisibility(View.VISIBLE);

                        }
                  }
            });
      }

      private void setAdapter() {
            //set data and list adapter
            try{
                  mAdapter = new AppNotificationAdapter(getContext(), items, animation_type);
                  recyclerView.setAdapter(mAdapter);
            }catch (Exception e){
                  if (mAdapter != null ){
//                        mAdapter = new AppNotificationAdapter(getContext(), items, animation_type);
//                        recyclerView.setAdapter(mAdapter);
                  }
            }
      }



      private DisposableObserver<TextViewTextChangeEvent> searchItems() {
            return new DisposableObserver<TextViewTextChangeEvent>() {
                  @Override
                  public void onNext(TextViewTextChangeEvent textViewTextChangeEvent) {
                        Log.d(TAG, "Search query: " + textViewTextChangeEvent.text());
                        mAdapter.getFilter().filter(textViewTextChangeEvent.text());
                  }

                  @Override
                  public void onError(Throwable e) {
                        Log.e(TAG, "onError: " + e.getMessage());
                  }

                  @Override
                  public void onComplete() {

                  }
            };
      }


}
