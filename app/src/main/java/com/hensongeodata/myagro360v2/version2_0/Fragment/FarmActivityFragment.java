package com.hensongeodata.myagro360v2.version2_0.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hensongeodata.myagro360v2.Interface.HistoryTabListener;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.response.FarmResponse;
import com.hensongeodata.myagro360v2.api.response.UserActivityResponse;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Adapter.ActivitiesRecyclerviewAdapter;
import com.hensongeodata.myagro360v2.version2_0.Listeners.ActivitiesRvListener;
import com.hensongeodata.myagro360v2.version2_0.Model.Farm;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmActivity;
import com.hensongeodata.myagro360v2.version2_0.Model.Member;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FarmActivityFragment extends Fragment implements HistoryTabListener {

      private static final String TAG = FarmActivityFragment.class.getSimpleName();
      private String orgId = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);
      private String userId = "";

      private FarmHomeFragmentListener farmHomeFragmentListener;
      private RecyclerView mRecyclerView;
      private ActivitiesRecyclerviewAdapter mActivitiesRecyclerviewAdapter;
      private MainViewModel mMainViewModel;
      private LinearLayout linearLayout;
      private FloatingActionButton fabButton;
      private AgroWebAPI mAgroWebApi;
      //      private LinearLayout buttonsContainer;
      private LinearLayout configureLayout;
      private ImageView sortButton;
      private TextView numberOfActivities;
      private TextView numberOfPlots;
      private ImageView noActivityImageView;
      private TextView     titleTextView;
      private TextView     briefTextView;
      private Button addNewActivityButton;
      private Button configureFilterButton;
      private List<FarmActivity> mFarmActivities;
      private MapDatabase mMapDatabase;
      private AppCompatSpinner spn_staff;
      private HashMap<Integer, String> staffSpinnerHashMap;

      @Nullable
      @Override
      public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_farm_activity, container, false);

            mMapDatabase = MapDatabase.getInstance(getContext());
            mMainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            mRecyclerView = view.findViewById(R.id.activities_recyclerview);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
            linearLayout = view.findViewById(R.id.no_activities_layout);
            fabButton = view.findViewById(R.id.fab_add);
            mAgroWebApi = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);
            configureLayout = view.findViewById(R.id.configure_layout);
            sortButton            = view.findViewById(R.id.sort_button);
            numberOfActivities = view.findViewById(R.id.number_of_activities);
            numberOfPlots           = view.findViewById(R.id.number_of_plots);
            noActivityImageView = view.findViewById(R.id.image);
            titleTextView                 = view.findViewById(R.id.title);
            briefTextView                = view.findViewById(R.id.brief);
            addNewActivityButton = view.findViewById(R.id.add_new_activity_button);
            configureFilterButton     = view.findViewById(R.id.configure_filter);
            spn_staff                        = view.findViewById(R.id.spn_staff);
//            buttonsContainer = view.findViewById(R.id.buttons_container);
            mFarmActivities = new ArrayList<>();

            fabButton.setOnClickListener(v -> onAddFarmActivityButton());

            sortButton.setOnClickListener(v -> PlotsFragmentSortButtonClicked());

            if (SharePrefManager.isSecondaryUserAvailable(getContext())){
                  userId = SharePrefManager.getSecondaryUserPref(getContext());
            }else {
                  userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
            }

            initComponent(view);

//            populateActivities();

            loadActivities();

            new Handler().postDelayed(() -> {
                  Log.i(TAG, "onCreateView:  Size: " + mFarmActivities.size());
                  setAdapter();
            }, 3000);

            fetchMembersFromDB();
            return view;
      }


      private void initComponent(View view) {
            addNewActivityButton.setOnClickListener(v -> onAddFarmActivityButton());
            configureFilterButton.setOnClickListener(v -> PlotsFragmentSortButtonClicked());
      }

      public void onAddFarmActivityButton(){
            if (farmHomeFragmentListener != null){
                  farmHomeFragmentListener.onAddNewFarmActivityButton();
            }
      }

      public void onFarmActivityPlotClicked(long plotId){
            if (farmHomeFragmentListener != null){
                  farmHomeFragmentListener.onActivityPlotClicked(plotId);
            }
      }

      public void onFarmActivityMoreClicked(String activityId, String name, long id){
            if (farmHomeFragmentListener != null){
                  farmHomeFragmentListener.onActivityMoreClicked(activityId, name, id);
            }
      }

      private void PlotsFragmentSortButtonClicked(){
            farmHomeFragmentListener.onFarmActivitiesFragmentSortButtonClicked();
      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof  FarmHomeFragmentListener){
                  farmHomeFragmentListener = (FarmHomeFragmentListener)context;
            }else{
                  throw new RuntimeException(context.toString()
                          + " must implement FarmHomeFragmentListener");
            }
      }

      @Override
      public void onSelected(String item) {}

      public interface FarmHomeFragmentListener{
            void onAddNewFarmActivityButton();
            void  onActivityPlotClicked(long plotId);
            void   onActivityMoreClicked(String activityId, String name, long id);
            void onFarmActivitiesFragmentSortButtonClicked();
      }

      private void loadActivities(){
            if (mMainViewModel != null){
                  mMainViewModel = null;
                  mMainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            }

            if (SharePrefManager.isActivitiesSortPreferenceAvailable(getContext())){

                  switch (SharePrefManager.getSortActivitiesPreference(getContext())){

                        case SharePrefManager.SORT_BY_PLOT_ACTIVITY:
                              populateActivitiesByPlot();
                              break;

                        case SharePrefManager.SORT_BY_FARM_ACTIVITY:
                              populateActivitiesByFarm();
                              break;

                        case SharePrefManager.SORT_BY_COMPLETED_ACTIVITY:
                              populateActivitiesByCompleted();
                              break;

                        case SharePrefManager.SORT_BY_PENDING_ACTIVITY:
                              populateActivitiesByPending();
                              break;

                        case SharePrefManager.SORT_BY_TYPE_ACTIVITY:
                              populateActivitiesByActivityType();
                              break;

                        case SharePrefManager.SORT_BY_DATE_CREATED_ACTIVITY:
//                              populateActivitiesByDateCreated();
                              populateActivitiesByRecent();
                              break;

                        case SharePrefManager.SORT_BY_ALL_ACTIVITY:

                        default:
                              populateActivities();
                  }
            }else {
                  populateActivities();
            }
      }

      private void populateActivitiesByCrop(){
            setMainViewModel(mMainViewModel.getFarmActivities());
      }

      private void populateActivitiesByDateCreated(){
            String startDDate = SharePrefManager.getSortParameterPreference(getContext(), SharePrefManager.SORT_BY_FROM_DATE_TYPE_ACTIVITY);
            String endDate = SharePrefManager.getSortParameterPreference(getContext(), SharePrefManager.SORT_BY_END_DATE_TYPE_ACTIVITY);
            String initialStartDateValue = startDDate.isEmpty() ? "0" : startDDate;
            String initialendDateValue = endDate.isEmpty() ? "0" : endDate;
            Log.i(TAG, "start Date:  " + startDDate);
            Log.i(TAG, "end Date:  " + startDDate);
            mMainViewModel.setFarmActivitiesByPlot(mMapDatabase.farmActivityDaoAccess().
                    fetchActivitiesByStartDate(userId,orgId, Long.valueOf(initialStartDateValue), Long.valueOf(initialendDateValue)));
            setMainViewModel(mMainViewModel.getFarmActivitiesByStartDate());
      }

      private void populateActivitiesByRecent(){
            mMainViewModel.setFarmActivitiesByPlot(mMapDatabase.farmActivityDaoAccess().fetchActivitiesByRecent(userId, orgId));
            setMainViewModel(mMainViewModel.getFarmActivitiesByRecent());

            AtomicInteger plotCount = new AtomicInteger();

            AppExecutors.getInstance().getDiskIO().execute(() -> {
                  plotCount.set(mMapDatabase.farmActivityDaoAccess().fetchActivitiesPlotCount(userId, orgId));
                  Log.i(TAG, "Plots Count " + plotCount);
            });

            new Handler().postDelayed(() -> setNumberOfPlots(plotCount.get()),1000);
      }

      private void populateActivitiesByActivityType(){
            String activityId = SharePrefManager.getActivityTypeSortPreference(getContext());
            mMainViewModel.setFarmActivitiesByActivityTpe(mMapDatabase.farmActivityDaoAccess().fetchActivitiesByActivityType(userId,orgId,activityId));
            setMainViewModel(mMainViewModel.getFarmActivitiesByActivityTpe());

            AtomicInteger plotCount = new AtomicInteger();

            AppExecutors.getInstance().getDiskIO().execute(() -> {
                  plotCount.set(mMapDatabase.farmActivityDaoAccess().fetchActivitiesByActivityPlotCount(userId, orgId, activityId));
                  Log.i(TAG, "Plots Count " + plotCount);
            });

            new Handler().postDelayed(() -> setNumberOfPlots(plotCount.get()),800);
      }

      private void populateActivitiesByPending(){
            setMainViewModel(mMainViewModel.getFarmActivitiesByPending());
            AtomicInteger plotCount = new AtomicInteger();

            AppExecutors.getInstance().getDiskIO().execute(() -> {
                  plotCount.set(mMapDatabase.farmActivityDaoAccess().fetchActivitiesByCompletedPlotCount(userId, orgId, 0));
                  Log.i(TAG, "Plots Count " + plotCount);
            });

            new Handler().postDelayed(() -> setNumberOfPlots(plotCount.get()),800);
      }

      private void populateActivitiesByCompleted(){
            setMainViewModel(mMainViewModel.getFarmActivitiesByCompleted());
            AtomicInteger plotCount = new AtomicInteger();

            AppExecutors.getInstance().getDiskIO().execute(() -> {
                  plotCount.set(mMapDatabase.farmActivityDaoAccess().fetchActivitiesByCompletedPlotCount(userId, orgId, 1));
                  Log.i(TAG, "Plots Count " + plotCount);
            });

            new Handler().postDelayed(() -> setNumberOfPlots(plotCount.get()),800);
      }

      private void populateActivitiesByFarm(){
            String farmId = SharePrefManager.getSortParameterPreference(getContext(), SharePrefManager.SORT_BY_FARM_TYPE_ACTIVITY);
            mMainViewModel.setFarmActivitiesByFarm(mMapDatabase.farmActivityDaoAccess().fetchActivitiesByFarm(userId,orgId, farmId));
            setMainViewModel(mMainViewModel.getFarmActivitiesByFarm());

            AtomicInteger plotCount = new AtomicInteger();

            AppExecutors.getInstance().getDiskIO().execute(() -> {
                  plotCount.set(mMapDatabase.farmActivityDaoAccess().fetchActivitiesByFarmPlotCount(userId, orgId, farmId));
                  Log.i(TAG, "Plots Count " + plotCount);
            });

            new Handler().postDelayed(new Runnable() {
                  @Override
                  public void run() {
                        setNumberOfPlots(plotCount.get());
                  }
            },800);
      }

      private void populateActivitiesByPlot(){

            String plotId = SharePrefManager.getSortParameterPreference(getContext(), SharePrefManager.SORT_BY_PLOT_TYPE_ACTIVITY);
            String initialValue = plotId.isEmpty() ? "0" : plotId;
            Log.i(TAG, "populateActivitiesByPlot:  " + plotId);
            mMainViewModel.setFarmActivitiesByPlot(mMapDatabase.farmActivityDaoAccess().fetchActivitiesByPlot(userId,orgId, Long.valueOf(initialValue)));
            setMainViewModel(mMainViewModel.getFarmActivitiesByPlot());

            AtomicInteger plotCount = new AtomicInteger();

            AppExecutors.getInstance().getDiskIO().execute(() -> {
                  plotCount.set(mMapDatabase.farmActivityDaoAccess().fetchActivitiesByPlotPlotCount(userId, orgId, Long.valueOf(initialValue)));
                  Log.i(TAG, "By Plots Count " + plotCount);
            });

            new Handler().postDelayed(new Runnable() {
                  @Override
                  public void run() {
                        setNumberOfPlots(plotCount.get());
                  }
            },800);
      }

      private void populateActivities(){
            setMainViewModel(mMainViewModel.getFarmActivities());

            AtomicInteger plotCount = new AtomicInteger();

            AppExecutors.getInstance().getDiskIO().execute(() -> {
                  plotCount.set(mMapDatabase.farmActivityDaoAccess().fetchActivitiesPlotCount(userId, orgId));
                  Log.i(TAG, "Plots Count " + plotCount);
            });

            new Handler().postDelayed(() -> setNumberOfPlots(plotCount.get()),1000);
      }

      private void setMainViewModel(LiveData<List<FarmActivity>> farmActivities) {
            farmActivities.observe(getActivity(), farmActivities1 -> {
                  mFarmActivities = farmActivities1;

                  if (mFarmActivities.size() != 0){
                        linearLayout.setVisibility(View.GONE);

                        numberOfActivities.setText(String.valueOf(mFarmActivities.size()));

//                  buttonsContainer.setVisibility(View.VISIBLE);
                        configureLayout.setVisibility(View.VISIBLE);
                        fabButton.setVisibility(View.VISIBLE);
                        mActivitiesRecyclerviewAdapter = new ActivitiesRecyclerviewAdapter(getContext(), mFarmActivities,
                                FarmActivityFragment.this, new ActivitiesRvListener() {
                              @Override
                              public void onPlotsClicked(long plotId) { onFarmActivityPlotClicked(plotId); }

                              @Override
                              public void onMoreClicked(String activityId, String name, long id) {
                                    onFarmActivityMoreClicked(activityId, name, id);
                              }
                        });

                        mRecyclerView.setAdapter(mActivitiesRecyclerviewAdapter);

                        if (SharePrefManager.isActivitiesSortPreferenceAvailable(getContext())){
                              noActivityImageView.setVisibility(View.GONE);
                              briefTextView.setText("No activity matches your filter. ");
                              configureLayout.setVisibility(View.VISIBLE);
                              addNewActivityButton.setVisibility(View.GONE);
                              configureFilterButton.setVisibility(View.VISIBLE);
                        }else {
                              noActivityImageView.setVisibility(View.GONE);
                              configureLayout.setVisibility(View.GONE);
                              addNewActivityButton.setVisibility(View.VISIBLE);
                              configureFilterButton.setVisibility(View.GONE);
                        }

                  }else {
                        linearLayout.setVisibility(View.VISIBLE);
                        fabButton.setVisibility(View.GONE);
//                  buttonsContainer.setVisibility(View.GONE);
                        configureLayout.setVisibility(View.GONE);
                  }
            });
      }

      @SuppressLint("RestrictedApi")
      private void setAdapter() {
            Log.i(TAG, "onChanged:  " + mFarmActivities.size());
            if (mFarmActivities.size() != 0){
                  linearLayout.setVisibility(View.GONE);

//                  buttonsContainer.setVisibility(View.VISIBLE);
                  configureLayout.setVisibility(View.VISIBLE);
                  fabButton.setVisibility(View.VISIBLE);
                  mActivitiesRecyclerviewAdapter = new ActivitiesRecyclerviewAdapter(getContext(), mFarmActivities,
                          FarmActivityFragment.this, new ActivitiesRvListener() {
                        @Override
                        public void onPlotsClicked(long plotId) {
                              onFarmActivityPlotClicked(plotId);
                        }

                        @Override
                        public void onMoreClicked(String activityId, String name, long id) {
                              onFarmActivityMoreClicked(activityId, name, id);
                        }
                  });

                  mRecyclerView.setAdapter(mActivitiesRecyclerviewAdapter);
            }else {
                  linearLayout.setVisibility(View.VISIBLE);
                  fabButton.setVisibility(View.GONE);
//                  buttonsContainer.setVisibility(View.GONE);
                  configureLayout.setVisibility(View.GONE);
            }
      }

      private void getFarms(){
            mAgroWebApi.fetchFarms(userId, orgId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new io.reactivex.Observer<FarmResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(FarmResponse farmResponse) {

                                int paddingLeft = (int)getActivity().getResources().getDimension(R.dimen.spacing_xlarge);
                                int paddingRight = (int)getActivity().getResources().getDimension(R.dimen.spacing_xlarge);

                                for (Farm farm: farmResponse.getFarms()){
                                      Button addButton = new Button(getContext());
                                      addButton.setText( farm.getName());
                                      Drawable tabRoundedAccentDrawable = getActivity().getResources().getDrawable(R.drawable.tab_rounded_accent);
                                      addButton.setPadding(paddingLeft,0,paddingRight,0);
                                      addButton.setTextColor(getActivity().getResources().getColor(R.color.white));
                                      addButton.setBackground(tabRoundedAccentDrawable);

                                      addButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                  AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                              mFarmActivities = mMapDatabase.farmActivityDaoAccess().fetchFarmActivitiesByFarmId(farm.getId());
                                                        }
                                                  });
                                                  Toast.makeText(getContext(), "Clicked!", Toast.LENGTH_SHORT).show();
                                            }
                                      });

//                                            buttonsContainer.addView(addButton);
                                }
                          }

                          @Override
                          public void onError(Throwable e) {
                                Log.e(TAG, "onError:  get Farms " + e.getMessage() );
                          }

                          @Override
                          public void onComplete() {}

                    });
      }

      private void getFarmActivities(){
            mAgroWebApi.fetchUserActivities(userId, orgId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<List<UserActivityResponse>>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(List<UserActivityResponse> userActivityResponses) {
                                for (UserActivityResponse userActivityResponse : userActivityResponses){
                                      FarmActivity farmActivity = new FarmActivity();
                                      farmActivity.setName(userActivityResponse.getName());
                                      farmActivity.setAdditionalInfo(userActivityResponse.getAdditionalInfo());
                                      farmActivity.setPlotId(userActivityResponse.getPlotId());
                                      farmActivity.setStartDateTimeString(userActivityResponse.getStartDate());
                                      farmActivity.setEndDateTimeString(userActivityResponse.getStartDate());
                                      farmActivity.setCompleted(userActivityResponse.isCompleted());
                                      farmActivity.setType(userActivityResponse.getType());
                                      farmActivity.setId(userActivityResponse.getId());
                                }
                          }

                          @Override
                          public void onError(Throwable e) {}

                          @Override
                          public void onComplete() {

                                if (mFarmActivities.size() != 0){
                                      linearLayout.setVisibility(View.GONE);

                                      numberOfActivities.setText(String.valueOf(mFarmActivities.size()));

//                  buttonsContainer.setVisibility(View.VISIBLE);
                                      configureLayout.setVisibility(View.VISIBLE);
                                      fabButton.setVisibility(View.VISIBLE);
                                      mActivitiesRecyclerviewAdapter = new ActivitiesRecyclerviewAdapter(getContext(), mFarmActivities,
                                              FarmActivityFragment.this, new ActivitiesRvListener() {
                                            @Override
                                            public void onPlotsClicked(long plotId) { onFarmActivityPlotClicked(plotId); }

                                            @Override
                                            public void onMoreClicked(String activityId, String name, long id) {
                                                  onFarmActivityMoreClicked(activityId, name, id);
                                            }
                                      });

                                      mRecyclerView.setAdapter(mActivitiesRecyclerviewAdapter);

                                      if (SharePrefManager.isActivitiesSortPreferenceAvailable(getContext())){
                                            noActivityImageView.setVisibility(View.GONE);
                                            briefTextView.setText("No activity matches your filter. ");
                                            configureLayout.setVisibility(View.VISIBLE);
                                            addNewActivityButton.setVisibility(View.GONE);
                                            configureFilterButton.setVisibility(View.VISIBLE);
                                      }else {
                                            noActivityImageView.setVisibility(View.GONE);
                                            configureLayout.setVisibility(View.GONE);
                                            addNewActivityButton.setVisibility(View.VISIBLE);
                                            configureFilterButton.setVisibility(View.GONE);
                                      }

                                }else {
                                      linearLayout.setVisibility(View.VISIBLE);
                                      fabButton.setVisibility(View.GONE);
//                  buttonsContainer.setVisibility(View.GONE);
                                      configureLayout.setVisibility(View.GONE);
                                }

                          }
                    });
      }

      private void fetchMembersFromDB(){

            mMainViewModel.getMembers().observe(getActivity(), new androidx.lifecycle.Observer<List<Member>>() {
                  @Override
                  public void onChanged(List<Member> members) {

                        String[] spinnerArray = new String[members.size()];

                        Log.i(TAG, "PlotsFragment " + members.size());

                        try {

                              staffSpinnerHashMap = new HashMap<Integer, String>();
                              for ( int i =0; i < members.size(); i++ ){
                                    staffSpinnerHashMap.put(i, String.valueOf(members.get(i).getId()));
                                    spinnerArray[i] = members.get(i).getFirstName() + " " + members.get(i).getLastName() + " ( " + members.get(i).getEmail();
                                    Log.i(TAG, "onChanged:  " + members.get(i).getId());
                              }
                              HelperClass.setupArrayAdapter(getActivity(),spinnerArray, spn_staff);
//                              setupArrayAdapter(spinnerArray, spn_staff);
                        } catch (Exception e) {
                              Log.e(TAG, "onNext:  " + e.getMessage());
                        }
                  }
            });

      }

      private void setNumberOfPlots(int plotCount){
            numberOfPlots.setText(String.valueOf(plotCount));
      }

}

