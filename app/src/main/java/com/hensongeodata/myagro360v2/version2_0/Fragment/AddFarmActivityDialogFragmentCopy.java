package com.hensongeodata.myagro360v2.version2_0.Fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.DialogFragment;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.response.FarmActivityResponse;
import com.hensongeodata.myagro360v2.api.response.PlotResponse;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.helper.Tools;
import com.hensongeodata.myagro360v2.model.FarmActivityModel;
import com.hensongeodata.myagro360v2.model.Keys;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AddFarmActivityDialogFragmentCopy extends DialogFragment implements AdapterView.OnItemSelectedListener {

    private static final String TAG = AddFarmActivityDialogFragmentCopy.class.getSimpleName();

    private View root_view;
    private Button spn_from_date, spn_from_time;
    private Button spn_to_date, spn_to_time;
    private TextView tv_email;
    private EditText et_name, et_location;
    private CheckBox cb_allday;
    private AppCompatSpinner plots_spinner;
    private AppCompatSpinner spn_activities;
    private AppCompatSpinner done_by_spinner;
    public CallbackResult callbackResult;
    private int request_code = 0;
    private AgroWebAPI mAgroWebAPI;
    LinearLayout activityPerformerLayout;
    private TextView activity_performer_text;

    public void setOnCallbackResult(final CallbackResult callbackResult) {
        this.callbackResult = callbackResult;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        root_view = inflater.inflate(R.layout.fragment_choose_plot_dialog, container, false);
//        spn_from_date  = root_view.findViewById(R.id.spn_from_date);
//        spn_from_time  = root_view.findViewById(R.id.spn_from_time);
//        spn_to_date       = root_view.findViewById(R.id.spn_to_date);
//        spn_to_time       = root_view.findViewById(R.id.spn_to_time);
//        tv_email               = root_view.findViewById(R.id.tv_email);
        cb_allday             = root_view.findViewById(R.id.cb_allday);
        plots_spinner    = root_view.findViewById(R.id.spn_plots);
        spn_activities = root_view.findViewById(R.id.spn_activities);
        done_by_spinner = root_view.findViewById(R.id.done_by_spinner);

        ((ImageButton) root_view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

//        spn_from_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialogDatePickerLight((Button) v);
//            }
//        });
//
//        spn_from_time.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialogTimePickerLight((Button) v);
//            }
//        });
//
//        spn_to_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialogDatePickerLight((Button) v);
//            }
//        });
//
//        spn_to_time.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialogTimePickerLight((Button) v);
//            }
//        });

          mAgroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);
//          getActivitiesFromAPI();
          getPlotsFromAPI();

          populateDoneBySpinner();

//          done_by_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//              @Override
//              public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                  switch( parent.getItemAtPosition(position).toString()){
//                      case "Outsourced":
//                          showStaffTextView("Outsourced to");
//                          break;
//
//                      case "Staff":
//                          showStaffTextView(parent.getItemAtPosition(position).toString());
//                          break;
//
//                  }
//              }
//
//              @Override
//              public void onNothingSelected(AdapterView<?> parent) {
//
//              }
//          });

          return root_view;
    }

    private void showStaffTextView(String performerText) {
    }

    private void populateDoneBySpinner() {
        String[] done_by_string_array = new String[2];
        done_by_string_array[0] = "Staff";
        done_by_string_array[1] = "Outsourced";

//        setupArrayAdapter(done_by_string_array, done_by_spinner);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) { }

    @Override
    public void onNothingSelected(AdapterView<?> parent) { }

    public interface CallbackResult {
        void sendResult(int requestCode, Object obj);
    }


    public void setRequestCode(int request_code) {
        this.request_code = request_code;
    }

    private void dialogDatePickerLight(final Button bt) {
        Calendar cur_calender = Calendar.getInstance();
        DatePickerDialog datePicker = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        long date_ship_millis = calendar.getTimeInMillis();
                        bt.setText(Tools.getFormattedDateEvent(date_ship_millis));
                    }
                },
                cur_calender.get(Calendar.YEAR),
                cur_calender.get(Calendar.MONTH),
                cur_calender.get(Calendar.DAY_OF_MONTH)
        );
        //set dark light
        datePicker.setThemeDark(false);
        datePicker.setAccentColor(getResources().getColor(R.color.colorPrimary));
        datePicker.setMinDate(cur_calender);
        datePicker.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    private void dialogTimePickerLight(final Button bt) {
        Calendar cur_calender = Calendar.getInstance();
        TimePickerDialog datePicker = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                calendar.set(Calendar.AM_PM, calendar.get(Calendar.AM_PM));
                long time_millis = calendar.getTimeInMillis();
                bt.setText(Tools.getFormattedTimeEvent(time_millis));
            }
        }, cur_calender.get(Calendar.HOUR_OF_DAY), cur_calender.get(Calendar.MINUTE), true);
        datePicker.setThemeDark(false);
        datePicker.setAccentColor(getResources().getColor(R.color.colorPrimary));
        datePicker.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }

    private void setupArrayAdapter(String[] stringArray, AppCompatSpinner spinner){
        ArrayAdapter<String> array = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_item, stringArray);
        array.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(array);
//        spinner.setSelection(0);
    }

    private void getActivitiesFromAPI() {

        mAgroWebAPI.getActivities()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<FarmActivityResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(FarmActivityResponse farmActivityResponse) {
                          String[] farmActivitiesArray = new String[farmActivityResponse.getFarmActivityModelList().size()];
                          List<FarmActivityModel> activities = farmActivityResponse.getFarmActivityModelList();

                          for ( int i =0; i < activities.size(); i++ ){
                                farmActivitiesArray[i] = activities.get(i).getName();
                          }
                          setupArrayAdapter(farmActivitiesArray, spn_activities);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError:  " + e.getMessage() );
                    }

                    @Override
                    public void onComplete() { }
                });
    }

    private void getPlotsFromAPI() {

        mAgroWebAPI.getPlots()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PlotResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(PlotResponse plotResponse) {

                        String[] plotsArray = new String[plotResponse.getPlots().size()];

                        for ( int i =0; i < plotResponse.getPlots().size(); i++ ){
                            plotsArray[i] = plotResponse.getPlots().get(i).getName();
                        }
                        setupArrayAdapter(plotsArray, plots_spinner);

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}