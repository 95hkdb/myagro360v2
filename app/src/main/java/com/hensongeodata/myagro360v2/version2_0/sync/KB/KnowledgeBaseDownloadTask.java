package com.hensongeodata.myagro360v2.version2_0.sync.KB;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.response.KnowledgeBaseResponse;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.KnowledgeBase;
import com.hensongeodata.myagro360v2.version2_0.Events.KBDownloadCompleted;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KnowledgeBaseDownloadTask {

      private static final String TAG = KnowledgeBaseDownloadTask.class.getSimpleName();
      private static MapDatabase mapDatabase;
      private static final long[] DEFAULT_VIBRATION_PATTERN =  {0, 250, 250, 250};
      protected static MyApplication myApplication;
      private static final int PLOTS_NOTIFICATION_ID = 1010;
      private static final int KB_NOTIFICATION_ID = 1015;


      public static void DownloadKnowledgeBase(Context context){

            String channelName = "syncKB";
            String channelId = "DBKBSyncChannelId";

            mapDatabase = MapDatabase.getInstance(context);

            AgroWebAPI AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.FAW_API).create(AgroWebAPI.class);

            Log.i(TAG, "SyncKnowledgeBase:  entered" );

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            Log.i(TAG, "SyncKnowledgeBase:  entered Network Constraint" );


            int importance = NotificationManager.IMPORTANCE_LOW;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.drawable.download)
                    .setContentTitle("Downloading knowledge Base..")
                    .setContentText("Download in progress");


            AppExecutors.getInstance().getDiskIO().execute(() -> {

                  try {

                        mapDatabase.knowledgeBaseDaoAccess().deleteKB();

                        AGRO_WEB_API.fetchKnowledgeBaseCall("english", "app", Keys.API_KEY)
                                .enqueue(new Callback<KnowledgeBaseResponse>() {
                                      @Override
                                      public void onResponse(Call<KnowledgeBaseResponse> call, Response<KnowledgeBaseResponse> response) {

                                            Log.i(TAG, "SyncKnowledgeBase:  entered API CALL RESPONSE" );

                                            int progressCounter = 0;
                                            if (response.body() != null) {
                                                  for (KnowledgeBase knowledgeBase : response.body().getTopics()){
                                                        progressCounter++;
                                                        int finalProgressCounter = progressCounter;
                                                        AppExecutors.getInstance().getDiskIO().execute(() -> {
                                                              Log.i(TAG, "SyncKnowledgeBase:  entered Insert and Notifications" );

                                                              mapDatabase.knowledgeBaseDaoAccess().insert(knowledgeBase);

                                                              mBuilder.setProgress(response.body().getTotal(), finalProgressCounter, false);

                                                              notificationManager.notify(KB_NOTIFICATION_ID, mBuilder.build());

                                                              if (response.body().getTopics().size() == mapDatabase.knowledgeBaseDaoAccess().knowledgeBaseCount()){
                                                                    EventBus.getDefault().post(new KBDownloadCompleted(false));
                                                              }
                                                        });
                                                  }
                                            }
                                      }

                                      @Override
                                      public void onFailure(Call<KnowledgeBaseResponse> call, Throwable t) {
                                            Log.i(TAG, "onResponse:  KB Error" + t.getMessage());
                                            Log.i(TAG, "SyncKnowledgeBase:  entered Error " + t.getMessage()  );
                                            EventBus.getDefault().post(new KBDownloadCompleted(true));
                                      }
                                });

                  }catch (Exception e){
                        Log.e(TAG, "SyncKnowledgeBase :  " + e.getMessage() );
                        EventBus.getDefault().post(new KBDownloadCompleted(true));
                  }
            });
      }

      public static void showNotification(int notificationId, String channelId,
                                          String channelName, Context context, String body, NotificationCompat.Style notificationStyle, Intent intent) {

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            int importance = NotificationManager.IMPORTANCE_HIGH;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setVibrate(DEFAULT_VIBRATION_PATTERN)
                    .setContentText(body)
                    .setStyle(notificationStyle)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setNumber(1)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                    .setAutoCancel(true);

            if (intent != null){
                  TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                  stackBuilder.addNextIntent(intent);
                  PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                          0,
                          PendingIntent.FLAG_UPDATE_CURRENT
                  );
                  mBuilder.setContentIntent(resultPendingIntent);
            }

            notificationManager.notify(notificationId, mBuilder.build());
      }

}
