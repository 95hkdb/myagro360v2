package com.hensongeodata.myagro360v2.version2_0.Events;

public  class AEAItemClickedEvent {

      private long memberId;
      private String id;
      private String name;

      public AEAItemClickedEvent(long memberId, String name, String id) {
            this.memberId = memberId;
            this.id = id;
            this.name = name;
      }

      public String getId() {
            return id;
      }

      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      public long getMemberId() {
            return memberId;
      }

      public void setMemberId(long memberId) {
            this.memberId = memberId;
      }
}
