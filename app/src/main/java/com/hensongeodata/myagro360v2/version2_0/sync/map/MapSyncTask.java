package com.hensongeodata.myagro360v2.version2_0.sync.map;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.version2_0.Model.POISModel;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class MapSyncTask {

      private static final String TAG = MapSyncTask.class.getSimpleName();
      private static MapDatabase mapDatabase;
      private static final long[] DEFAULT_VIBRATION_PATTERN =  {0, 250, 250, 250};
      protected static MyApplication myApplication;


      synchronized public static  void syncAllMaps(Context context) throws JSONException {
            myApplication=new MyApplication();
            mapDatabase = MapDatabase.getInstance(context);
            NetworkRequest networkRequest = new NetworkRequest(context);
            CreateForm createForm = CreateForm.getInstance(context);
             ArrayList<String> strPOIS = new ArrayList<>();

           List<MapModel> notSyncedMaps =  mapDatabase.daoAccess().fetchNotSyncedMaps();

           if (myApplication.hasNetworkConnection(context) && notSyncedMaps.size() != 0 ){

                 int notificationlId = 1003;
                 String channelName = "mapUpdatesChannel";
                 String channelId = "mapUpdatesChannelId";

                 NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Map sync is in progress..."));
                 showNotification(notificationlId, channelId, channelName,context,  "Map sync is in progress.. ", notificationStyle,null );

                 for (MapModel map: notSyncedMaps){
                       List<POISModel> poisModels = mapDatabase.poisDaoAccess().fetchPOISByMapId(map.getId());
                       for (POISModel poisModel: poisModels){
                             strPOIS.add(poisModel.getInfo());

                             Log.i(TAG, "syncAllMaps:  " + poisModel.getInfo());
                       }

//                       networkRequest.sendFAWMappingBulk(
//                               createForm.buildJSON_Mapping(
//                                       createForm.buildPOI_Mapping(strPOIS)),
//                               map.getId(), map.getType(), new NetworkResponse() {
//                                     @Override
//                                     public void onRespond(Object object) {
//
//                                           AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
//                                                 @Override
//                                                 public void run() {
//                                                       mapDatabase.daoAccess().updateSyncedMapByMapId(map.getId());
//
//                                                       NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Map syncing is complete."));
//                                                       showNotification(notificationlId, channelId, channelName,context,  "Map syncing is complete.", notificationStyle,null );
//                                                 }
//                                           });
//
//                                     }
//                               }, map.name, 0);
                 }
           }
      }


      synchronized public static  void syncMap(Context context, String mapId) throws JSONException {

            AppExecutors.getInstance().getDiskIO().execute(() -> {
            mapDatabase = MapDatabase.getInstance(context);
            NetworkRequest networkRequest = new NetworkRequest(context);
            CreateForm createForm = CreateForm.getInstance(context);
            ArrayList<String> strPOIS = new ArrayList<>();

                        MapModel notSyncedMap=  mapDatabase.daoAccess().fetchNotSyncedMap(mapId);
                        List<POISModel> poisModels = mapDatabase.poisDaoAccess().fetchPOISByMapId(mapId);

                        for (POISModel poisModel: poisModels){
                              strPOIS.add(poisModel.getInfo());
                        }

                  //                                networkRequest.sendFAWMappingBulk(
//                                        createForm.buildJSON_Mapping(
//                                                createForm.buildPOI_Mapping(strPOIS)),
//                                        notSyncedMap.getId(), notSyncedMap.getType(), object -> {
//
//
//                                        }, notSyncedMap.name, 0);

                  mapDatabase.daoAccess().updateSyncedMapByMapId(mapId);
                    });
            }

      public static void showNotification(int notificationId, String channelId,
                                          String channelName, Context context, String body, NotificationCompat.Style notificationStyle, Intent intent) {

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            int importance = NotificationManager.IMPORTANCE_HIGH;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            androidx.core.app.NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setVibrate(DEFAULT_VIBRATION_PATTERN)
                    .setContentText(body)
                    .setStyle(notificationStyle)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setNumber(1)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                    .setAutoCancel(true);

            if (intent != null){
                  TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                  stackBuilder.addNextIntent(intent);
                  PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                          0,
                          PendingIntent.FLAG_UPDATE_CURRENT
                  );
                  mBuilder.setContentIntent(resultPendingIntent);
            }

            notificationManager.notify(notificationId, mBuilder.build());
      }

}
