package com.hensongeodata.myagro360v2.version2_0.Fragment;


import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.response.FarmActivityResponse;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.helper.Tools;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Adapter.ActivitiesRecyclerviewAdapter;
import com.hensongeodata.myagro360v2.version2_0.Broadcast.MyBroadcastReceiver;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmActivity;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;
import com.hensongeodata.myagro360v2.version2_0.Model.Member;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;
import com.hensongeodata.myagro360v2.version2_0.sync.farmActivity.FarmActivitySyncTask;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.ALARM_SERVICE;

public class ChooseActivityDialogFragment extends DialogFragment {

      private static final String TAG = ChooseActivityDialogFragment.class.getSimpleName();
      private static final String ARG_PLOT_ID = "plot_id" ;
      private AppCompatImageButton btnClose;
      private Button btn_save;
      private Button saveButton;
      private Button spn_from_date, spn_from_time;
      private Button spn_to_date, spn_to_time;
      private AppCompatSpinner spn_activities;
      private AppCompatSpinner done_by_spinner;
      private TextView tv_email;
      private TextView performer_type_text;
      private AppCompatSpinner spn_staff;
      private  EditText outsourced_name;
      private  EditText additional_info;
      private boolean outSourced;
      private AgroWebAPI mAgroWebAPI;
      private HashMap<Integer, String> activitiesHashMap;
      private long mPlotId;
      private MapDatabase mapDatabase;
      private MainViewModel mainViewModel;
      private ActivitiesRecyclerviewAdapter activitiesRecyclerviewAdapter;
      private HashMap<Integer, String> staffSpinnerHashMap;
      private ChooseActivityDialogFragment.OnChooseActivityDialogFragmentListener mListener;
      private Handler handler;

      public ChooseActivityDialogFragment() {}

      public static ChooseActivityDialogFragment newInstance(long plotId){
            ChooseActivityDialogFragment chooseActivityDialogFragment = new ChooseActivityDialogFragment();
            Bundle args = new Bundle();
            args.putLong(ARG_PLOT_ID, plotId);
            chooseActivityDialogFragment.setArguments(args);

            return chooseActivityDialogFragment;
      }

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            if (getArguments() != null) {
                  mPlotId = getArguments().getLong(ARG_PLOT_ID);
            }
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View root_view = inflater.inflate(R.layout.fragment_choose_activity, container, false);

            initViewComponents(root_view);

            handler = new Handler();

            btnClose = root_view.findViewById(R.id.bt_close);

            btnClose.setOnClickListener(v -> dismiss());

            btn_save.setOnClickListener(v -> {
                  processActivityForm();
            });

            saveButton.setOnClickListener(v -> {
                  processActivityForm();
            });

            spn_from_date.setOnClickListener(v -> dialogDatePickerLight((Button) v));

            spn_from_time.setOnClickListener(v -> dialogTimePickerLight((Button) v));

            spn_to_date.setOnClickListener(v -> dialogDatePickerLight((Button) v));

            spn_to_time.setOnClickListener(v -> dialogTimePickerLight((Button) v));

            populateDoneBySpinner();
            fetchMembersFromDB();

          done_by_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                      switch( parent.getItemAtPosition(position).toString()){
                      case "external":
                          showStaffTextView("Outsourced to");
                          spn_staff.setVisibility(View.GONE);
                          outsourced_name.setVisibility(View.VISIBLE);
                          outSourced = true;
                          break;

                      case "staff":
                          showStaffTextView(parent.getItemAtPosition(position).toString());
                            spn_staff.setVisibility(View.VISIBLE);
                            outsourced_name.setVisibility(View.GONE);
                            outSourced  = false;
                            break;

                  }

                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {}
          });

            getActivitiesFromAPI();

            return root_view;
      }

      private void initViewComponents(View root_view) {
            mapDatabase  = MapDatabase.getInstance(getContext());
            mAgroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);
            mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            btn_save             = root_view.findViewById(R.id.bt_save);
            saveButton         = root_view.findViewById(R.id.save_activity_btn);
            spn_from_date  = root_view.findViewById(R.id.spn_from_date);
            spn_from_time  = root_view.findViewById(R.id.spn_from_time);
            spn_to_date       = root_view.findViewById(R.id.spn_to_date);
            spn_to_time       = root_view.findViewById(R.id.spn_to_time);
            tv_email               = root_view.findViewById(R.id.tv_email);
            spn_activities = root_view.findViewById(R.id.spn_activities);
            done_by_spinner = root_view.findViewById(R.id.done_by_spinner);
            performer_type_text = root_view.findViewById(R.id.performer_type_text);
            spn_staff                        = root_view.findViewById(R.id.spn_staff);
            outsourced_name                        = root_view.findViewById(R.id.outsourced_name);
            additional_info                        = root_view.findViewById(R.id.additional_info);
            outSourced                                    =  false;
      }

      private void processActivityForm() {
            String fromDate = spn_from_date.getText().toString();
            String fromTime = spn_from_time.getText().toString();
            String toDate = spn_to_date.getText().toString();
            String toTime = spn_to_time.getText().toString();

            if (fromDate.isEmpty()) {
                  spn_from_date.setError("Error");
            }

            if (toDate.isEmpty()) {
                  spn_to_date.setError("Error");
            }

            if (fromTime.isEmpty()) {
                  spn_from_time.setError("Error");
            }

            if (toTime.isEmpty()) {
                  spn_to_time.setError("Error");
            }

            if (outSourced && outsourced_name.getText().toString().isEmpty()) {
                  outsourced_name.setError("name of outsourced required!");
            }

            if (!fromDate.isEmpty() && !toDate.isEmpty()) {

                  String outsourcedNameText = "";
                  String fromDateText = spn_from_date.getText().toString();
                  String toDateText = spn_to_date.getText().toString();
                  String fromTimeText = spn_from_time.getText().toString();
                  String toTimeText = spn_to_time.getText().toString();
                  String activityName = spn_activities.getSelectedItem().toString();
                  String activityId = activitiesHashMap.get(spn_activities.getSelectedItemPosition());
                  String staffId = staffSpinnerHashMap.get(spn_staff.getSelectedItemPosition());
                  long plotId = mPlotId;
                  boolean activityOutSourced = outSourced;
                  String comment = additional_info.getText().toString();

                  if (outSourced && !outsourced_name.getText().toString().isEmpty()) {
                        outsourcedNameText = outsourced_name.getText().toString();
                  }

                  String userId = "";

                  if (SharePrefManager.isSecondaryUserAvailable(getContext())){
                        userId = SharePrefManager.getSecondaryUserPref(getContext());
                  }else {
                        userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
                  }

                  FarmActivity farmActivity = new FarmActivity();
                  farmActivity.setId(activityId);
                  farmActivity.setName(activityName);
                  farmActivity.setStartDateTimeString(fromDateText + " " + fromTimeText);
                  farmActivity.setEndDateTimeString(toDateText + " " + toTimeText);
//                  farmActivity.setPlotId(String.valueOf(plotId));
                  farmActivity.setPrimaryPlotId(plotId);
                  farmActivity.setOutSourced(activityOutSourced);
                  farmActivity.setOutSourcedName(outsourcedNameText);
                  farmActivity.setType(performer_type_text.getText().toString());
                  farmActivity.setAdditionalInfo(comment);
                  farmActivity.setCompleted(false);
                  farmActivity.setOrgId(SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0));
//                  farmActivity.setUserId(SharePrefManager.getInstance(getContext()).getUserDetails().get(0));
                  farmActivity.setUserId(userId);
                  farmActivity.setSynced(false);
                  farmActivity.setCreatedAt(System.currentTimeMillis());
                  farmActivity.setUpdatedAt(System.currentTimeMillis());

                  String extractFromMonth = fromDateText.substring(4, 8);
                  String extractFromDay = fromDateText.substring(9, 11);
                  String extractFromYear = fromDateText.substring(12, 16);
                  String reformatedFromDateString = extractFromDay + "-" + getMonthNumber(extractFromMonth) + "-" + extractFromYear+" " +fromTimeText;
                  long fromDateMilliseconds = convertDateToMilliseconds(reformatedFromDateString);
//                              double timeDateMillisecondsOne = convertToMilliseconds(extractFromYear, extractFromMonth, extractFromDay, )

                  String extractToMonth = toDateText.substring(4, 8);
                  String extractToDay = toDateText.substring(9, 11);
                  String extractToYear = toDateText.substring(12, 16);
                  String reformatedToDateString = extractToDay + "-" + getMonthNumber(extractToMonth) + "-" + extractToYear +" "+ toTimeText;
                  long toDateMilliseconds = convertDateToMilliseconds(reformatedToDateString);

                  farmActivity.setStartDate(HelperClass.convertDateOnlyToMilliseconds(extractFromYear + "-" + getMonthNumber(extractFromMonth) + "-" + extractFromDay ));
                  farmActivity.setEndDate(HelperClass.convertDateOnlyToMilliseconds(extractToYear + "-" + getMonthNumber(extractToMonth) + "-" + extractToDay ));
//                  farmActivity.setEndDate(toDateMilliseconds);

                  if (outSourced) {
                        farmActivity.setStaffId("");
                  } else {
                        farmActivity.setStaffId(staffId);
                  }

                  AppExecutors.getInstance().getDiskIO().execute(() -> {
                        FarmPlot farmPlot = mapDatabase.plotDaoAccess().fetchPlotByPlotId(plotId);
                        farmActivity.setFarmId(farmPlot.getFarmId());
                        farmActivity.setPlotId(farmPlot.getId());
                       long farmActivityId =  mapDatabase.farmActivityDaoAccess().insert(farmActivity);

                        FarmActivitySyncTask.syncFarmActivity(handler, getContext(), farmActivityId );

                  });


                  String text = "Successfully added a new activity!";
                  Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
                  onChooseActivityDialogSave();
                  onChooseActivityDialogDismiss();
                  dismiss();
            }
      }

      private void showStaffTextView(String performerText) {
            performer_type_text.setText(performerText);
      }

      private void populateDoneBySpinner() {
            String[] done_by_string_array = new String[2];
            done_by_string_array[0] = "staff";
            done_by_string_array[1] = "external";

        setupArrayAdapter(done_by_string_array, done_by_spinner);
      }

      private void dialogDatePickerLight(final Button bt) {
            Calendar cur_calender = Calendar.getInstance();

            DatePickerDialog datePicker = DatePickerDialog.newInstance(
                    new DatePickerDialog.OnDateSetListener() {
                          @Override
                          public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                long date_ship_millis = calendar.getTimeInMillis();
                                bt.setText(Tools.getFormattedDateEvent(date_ship_millis));
                          }
                    },
                    cur_calender.get(Calendar.YEAR),
                    cur_calender.get(Calendar.MONTH),
                    cur_calender.get(Calendar.DAY_OF_MONTH)


            );
            //set dark light
            datePicker.setThemeDark(false);
            datePicker.setAccentColor(getResources().getColor(R.color.colorPrimary));
            datePicker.setMinDate(cur_calender);
            datePicker.show(getActivity().getFragmentManager(), "Datepickerdialog");



            Log.d(TAG, "dialogDatePickerLight:  " + cur_calender.get(0));
      }

      private void dialogTimePickerLight(final Button bt) {
            Calendar cur_calender = Calendar.getInstance();
            TimePickerDialog datePicker = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                  @Override
                  public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        calendar.set(Calendar.AM_PM, calendar.get(Calendar.AM_PM));
                        long time_millis = calendar.getTimeInMillis();
                        bt.setText(Tools.getFormattedTimeEvent(time_millis));
                  }
            }, cur_calender.get(Calendar.HOUR_OF_DAY), cur_calender.get(Calendar.MINUTE), true);
            datePicker.setThemeDark(false);
            datePicker.setAccentColor(getResources().getColor(R.color.colorPrimary));
            datePicker.show(getActivity().getFragmentManager(), "Timepickerdialog");
      }

      private void getActivitiesFromAPI() {

            mAgroWebAPI.getActivities()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<FarmActivityResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                          }

                          @Override
                          public void onNext(FarmActivityResponse farmActivityResponse) {
                                String[] farmActivitiesArray = new String[farmActivityResponse.getFarmActivityModelList().size()];

                                try {

                                      activitiesHashMap = new HashMap<Integer, String>();
                                      for ( int i =0; i < farmActivityResponse.getFarmActivityModelList().size(); i++ ){
                                            activitiesHashMap.put(i, String.valueOf(farmActivityResponse.getFarmActivityModelList().get(i).getId()));
                                            farmActivitiesArray[i] = farmActivityResponse.getFarmActivityModelList().get(i).getName();
                                      }
                                      setupArrayAdapter(farmActivitiesArray, spn_activities);
                                } catch (Exception e) {
                                }
                          }

                          @Override
                          public void onError(Throwable e) {
//                                Log.e(TAG, "onError:  " + e.getMessage() );
                          }

                          @Override
                          public void onComplete() { }
                    });
      }

      private void setupArrayAdapter(String[] stringArray, AppCompatSpinner spinner){
            ArrayAdapter<String> array = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_item, stringArray);
            array.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(array);
      }

      private long convertDateToMilliseconds(String dateString) {

            SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm", Locale.ENGLISH);
            Date date = null;

            long dateConverted = 0;

            try {
                  date = sdf.parse(dateString.trim());
                  dateConverted =  (date.getTime());
                  Log.i(TAG, "testDate: Date - Time in milliseconds : " + dateConverted);

            } catch (ParseException e) {
                  e.printStackTrace();
            }

            return dateConverted;
      }

      private double convertToMilliseconds(int year, int month,  int day,int hour,  int minute,  int second){
            Calendar calendar = Calendar.getInstance();
//            calendar.set(2019, 8, 2, 20, 35, 0 );
            calendar.set(year, month, day, hour, minute, second );

            return Double.longBitsToDouble(calendar.getTimeInMillis());
      }

      private int getMonthNumber(String month){
            int i = 0;
            switch (month.trim()){
                  case "Jan":
                        i = 1;
                        return i;

                  case "Feb":
                        i = 2;
                        return i;

                  case "Mar":
                        i =  3;
                        return i;

                  case "Apr":
                        i = 4;
                        return i;

                  case "May":
                        i =  5;
                        return i;

                  case "Jun":
                        i =  6;
                        return i;

                  case "Jul":
                        i = 7;
                        return i;

                  case "Aug":
                        i = 8;
                        return i;

                  case "Sep":
                        i = 9;
                        return i;

                  case "Oct":
                        i = 10;
                        return i;

                  case "Nov":
                        i = 11;
                        return i;

                  case "Dec":
                        i = 12;
                        return i;

            }

            return i;
      }

      @SuppressLint("DefaultLocale")
      private static String millisecondsToDate(double timeStamp){
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis((long)timeStamp);

            return String.format("%d-%d-%d",
                    calendar.get(Calendar.DAY_OF_MONTH),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.YEAR));
      }

      private void fetchMembersFromDB(){

            mainViewModel.getMembers().observe(getActivity(), new androidx.lifecycle.Observer<List<Member>>() {
                  @Override
                  public void onChanged(List<Member> members) {

                        String[] spinnerArray = new String[members.size()];

                        Log.i(TAG, "ChooseActivityDialogFragment " + members.size());

                        try {

                              staffSpinnerHashMap = new HashMap<Integer, String>();
                              for ( int i =0; i < members.size(); i++ ){
                                    staffSpinnerHashMap.put(i, String.valueOf(members.get(i).getId()));
                                    spinnerArray[i] = members.get(i).getFirstName() + " " + members.get(i).getLastName() + " ( " + members.get(i).getEmail();
                                    Log.i(TAG, "onChanged:  " + members.get(i).getId());
                              }
                              setupArrayAdapter(spinnerArray, spn_staff);
                        } catch (Exception e) {
                              Log.e(TAG, "onNext:  " + e.getMessage());
                        }
                  }
            });

      }

      public void startAlert() {
//        EditText text = findViewById(R.id.time);
            int i = 10;
            Intent intent = new Intent(getActivity(), MyBroadcastReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getContext(), 234324243, intent, 0);
            AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (i * 1000), pendingIntent);
            Toast.makeText(getContext(), "Alarm set in " + i + " seconds", Toast.LENGTH_LONG).show();
      }

      private void onChooseActivityDialogDismiss(){
            if (mListener != null){
                  mListener.onDismiss();
            }
      }

      private void onChooseActivityDialogSave(){
            if (mListener != null){
                  mListener.onChooseActivityDialogFragmentSave();
            }
      }

      public interface OnChooseActivityDialogFragmentListener{
            void onDismiss();
            void onChooseActivityDialogFragmentSave();
      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof ChooseActivityDialogFragment.OnChooseActivityDialogFragmentListener) {
                  mListener = (ChooseActivityDialogFragment.OnChooseActivityDialogFragmentListener) context;
            } else {
                  throw new RuntimeException(context.toString()
                          + " must implement OnChooseActivityDialogFragmentListener");
            }
      }

      @Override
      public void onDetach() {
            super.onDetach();
            mListener = null;
      }

//      @Override
//      public void onStart() {
//            super.onStart();
//            EventBus.getDefault().register(this);
//      }
//
//      @Override
//      public void onStop() {
//            super.onStop();
//            EventBus.getDefault().unregister(this);
//      }
}
