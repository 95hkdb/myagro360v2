package com.hensongeodata.myagro360v2.version2_0.sync.KB;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

public class KnowledgeBaseDownloadIntentService extends IntentService {

      public KnowledgeBaseDownloadIntentService() {
            super("PlotSyncIntentService");
      }

      @Override
      protected void onHandleIntent(@Nullable Intent intent) {
            KnowledgeBaseDownloadTask.DownloadKnowledgeBase(this);
      }
}
