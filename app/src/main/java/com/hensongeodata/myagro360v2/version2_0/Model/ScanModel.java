package com.hensongeodata.myagro360v2.version2_0.Model;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity( tableName =  ScanModel.TABLE_NAME)
public class ScanModel {

      public static final String TABLE_NAME = "scans";

      public static final String COLUMN_ID = "id";
      public static final String COLUMN_ITEM_ID= "item_id";
      public static final String COLUMN_BATCH_ID= "batch_id";
      public static final String COLUMN_IMAGE_PATH = "image_path";
      public static final String COLUMN_STATUS = "status";
      public static final String COLUMN_MESSAGE = "message";
      public static final String COLUMN_SCOUT_ID = "scout_id";
      public static final String COLUMN_TYPE = "type";
      public static final String COLUMN_PLANT_NUMBER = "plant_number";
      public static final String COLUMN_POINT_NUMBER = "point_number";
      public static final String COLUMN_PLOT_ID = "plot_id";
      public static final String COLUMN_DATE_CREATED = "date_created";
      public static final String COLUMN_ORG_ID = "org_id";
      public static final String COLUMN_USER_ID = "user_id";
      public static final String COLUMN_ACTION = "action";
      private static final String COLUMN_PEST_IDENTIFIED = "pest_identified" ;
      private static final String COLUMN_PEST_FOUND = "pest_found";
      private static final String COLUMN_SCAN_REPORT_TEXT = "scan_report_text";
      private static final String COLUMN_SYNCED = "synced";

      public static int STATE_PROCESSED=400;
      public static int STATE_UNPROCCESSED=500;


      @PrimaryKey(autoGenerate = true)
      @ColumnInfo( name = COLUMN_ID)
      private long id;

      @ColumnInfo( name = COLUMN_ITEM_ID)
      private String itemId;

      @Nullable
      @ColumnInfo( name = COLUMN_BATCH_ID)
      private String batchId;

      @ColumnInfo( name = COLUMN_IMAGE_PATH)
      private String imagePath;

      @ColumnInfo( name = COLUMN_STATUS)
      private int status;

      @ColumnInfo( name = COLUMN_ACTION)
      private int action;

      @ColumnInfo( name = COLUMN_MESSAGE)
      private String message;

      @ColumnInfo( name = COLUMN_PEST_IDENTIFIED)
      private String pestIdentified;

      @ColumnInfo( name = COLUMN_PEST_FOUND)
      private boolean pestFound;

      @ColumnInfo( name = COLUMN_SCOUT_ID)
      private long scoutId;

      @ColumnInfo( name = COLUMN_TYPE)
      private String type;

      @ColumnInfo( name = COLUMN_PLANT_NUMBER)
      private int plantNumber;

      @ColumnInfo( name = COLUMN_POINT_NUMBER)
      private int pointNumber;

      @ColumnInfo( name = COLUMN_PLOT_ID)
      private long plotId;

      @ColumnInfo( name = COLUMN_DATE_CREATED)
      private long dateCreated;

      @ColumnInfo( name = COLUMN_ORG_ID)
      private String orgId;

      @ColumnInfo( name = COLUMN_USER_ID)
      private String userId;

      @ColumnInfo( name = COLUMN_SCAN_REPORT_TEXT)
      private String scanReportText;

      @ColumnInfo( name = COLUMN_SYNCED)
      private boolean synced;


      public long getId() {
            return id;
      }

      public String getItemId() {
            return itemId;
      }

      public void setItemId(String itemId) {
            this.itemId = itemId;
      }

      @Nullable
      public String getBatchId() {
            return batchId;
      }

      public void setBatchId(@Nullable String batchId) {
            this.batchId = batchId;
      }

      public void setId(long id) {
            this.id = id;
      }

      public String getImagePath() {
            return imagePath;
      }

      public void setImagePath(String imagePath) {
            this.imagePath = imagePath;
      }

      public int getStatus() {
            return status;
      }

      public int getAction() {
            return action;
      }

      public void setAction(int action) {
            this.action = action;
      }

      public void setStatus(int status) {
            this.status = status;
      }

      public String getMessage() {
            return message;
      }

      public void setMessage(String message) {
            this.message = message;
      }

      public String getPestIdentified() {
            return pestIdentified;
      }

      public void setPestIdentified(String pestIdentified) {
            this.pestIdentified = pestIdentified;
      }

      public long getScoutId() {
            return scoutId;
      }

      public void setScoutId(long scoutId) {
            this.scoutId = scoutId;
      }

      public String getType() {
            return type;
      }

      public void setType(String type) {
            this.type = type;
      }

      public int getPlantNumber() {
            return plantNumber;
      }

      public void setPlantNumber(int plantNumber) {
            this.plantNumber = plantNumber;
      }

      public int getPointNumber() {
            return pointNumber;
      }

      public void setPointNumber(int pointNumber) {
            this.pointNumber = pointNumber;
      }

      public long getPlotId() {
            return plotId;
      }

      public void setPlotId(long plotId) {
            this.plotId = plotId;
      }

      public long getDateCreated() {
            return dateCreated;
      }

      public void setDateCreated(long dateCreated) {
            this.dateCreated = dateCreated;
      }

      public String getOrgId() {
            return orgId;
      }

      public void setOrgId(String orgId) {
            this.orgId = orgId;
      }

      public String getUserId() {
            return userId;
      }

      public void setUserId(String userId) {
            this.userId = userId;
      }

      public boolean isPestFound() {
            return pestFound;
      }

      public void setPestFound(boolean pestFound) {
            this.pestFound = pestFound;
      }

      public String getScanReportText() {
            return scanReportText;
      }

      public void setScanReportText(String scanReportText) {
            this.scanReportText = scanReportText;
      }

      public boolean isSynced() {
            return synced;
      }

      public void setSynced(boolean synced) {
            this.synced = synced;
      }
}
