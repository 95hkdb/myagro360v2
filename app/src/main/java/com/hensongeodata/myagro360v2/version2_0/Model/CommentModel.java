package com.hensongeodata.myagro360v2.version2_0.Model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity( tableName =  CommentModel.TABLE_NAME)
public class CommentModel implements Parcelable {

      public static final String TABLE_NAME = "comments";

      public static final String COLUMN_COMMENT_ID= "comment_id";
      public static final String COLUMN_ID = "id";
      public static final String COLUMN_CONTENT= "content";
      public static final String COLUMN_CREATOR_NAME= "creator_name";
      public static final String COLUMN_DATE_CREATED = "date_created";
      private static final String COLUMN_SYNCED = "synced";

      @PrimaryKey(autoGenerate = true)
      @ColumnInfo( name = COLUMN_COMMENT_ID)
      private long commentId;

      @ColumnInfo( name = COLUMN_ID)
      private String id;

      @ColumnInfo( name = COLUMN_CONTENT)
      private String content;

      @SerializedName("creator_name")
      @ColumnInfo( name = COLUMN_CREATOR_NAME)
      private String creatorName;

      @SerializedName("created")
      @ColumnInfo( name = COLUMN_DATE_CREATED)
      private String dateCreated;

      @ColumnInfo( name = COLUMN_SYNCED)
      private boolean synced;

      public CommentModel() {
      }

      public String getId() {
            return id;
      }

      public void setId(String id) {
            this.id = id;
      }

      public String getContent() {
            return content;
      }

      public void setContent(String content) {
            this.content = content;
      }

      public String getCreatorName() {
            return creatorName;
      }

      public void setCreatorName(String creatorName) {
            this.creatorName = creatorName;
      }

      public String getDateCreated() {
            return dateCreated;
      }

      public void setDateCreated(String dateCreated) {
            this.dateCreated = dateCreated;
      }

      public boolean isSynced() {
            return synced;
      }

      public void setSynced(boolean synced) {
            this.synced = synced;
      }

      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(this.commentId);
            dest.writeString(this.id);
            dest.writeString(this.content);
            dest.writeString(this.creatorName);
            dest.writeString(this.dateCreated);
            dest.writeByte(this.synced ? (byte) 1 : (byte) 0);
      }

      protected CommentModel(Parcel in) {
            this.commentId = in.readLong();
            this.id = in.readString();
            this.content = in.readString();
            this.creatorName = in.readString();
            this.dateCreated = in.readString();
            this.synced = in.readByte() != 0;
      }

      public static final Parcelable.Creator<CommentModel> CREATOR = new Parcelable.Creator<CommentModel>() {
            @Override
            public CommentModel createFromParcel(Parcel source) {
                  return new CommentModel(source);
            }

            @Override
            public CommentModel[] newArray(int size) {
                  return new CommentModel[size];
            }
      };
}
