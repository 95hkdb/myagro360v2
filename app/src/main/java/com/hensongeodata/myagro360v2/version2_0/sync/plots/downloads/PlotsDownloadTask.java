package com.hensongeodata.myagro360v2.version2_0.sync.plots.downloads;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.google.gson.JsonElement;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.response.FarmResponse;
import com.hensongeodata.myagro360v2.api.response.PlotResponse;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Events.PlotProgressEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.PlotsDownloadCompleted;
import com.hensongeodata.myagro360v2.version2_0.Model.AppNotification;
import com.hensongeodata.myagro360v2.version2_0.Model.Farm;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlotsDownloadTask {

      private static final String TAG = PlotsDownloadTask.class.getSimpleName();
      private static MapDatabase mapDatabase;
      private static final long[] DEFAULT_VIBRATION_PATTERN =  {0, 250, 250, 250};
      protected static MyApplication myApplication;
      private static final int PLOTS_NOTIFICATION_ID = 1010;
      public static final int UPDATE_PLOTS_NOTIFICATION_ID = 1014;

      synchronized public static  void syncAllPlots(Context context) throws JSONException {
            myApplication=new MyApplication();
            mapDatabase = MapDatabase.getInstance(context);
            AgroWebAPI agroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            List<FarmPlot> unSyncedPlots = mapDatabase.plotDaoAccess().fetchAllUnSyncedPlots();
            if (myApplication.hasNetworkConnection(context) && unSyncedPlots.size() != 0 ){

                 int notificationlId = 1004;
                 String channelName = "plotUpdatesChannel";
                 String channelId         = "plotUpdatesChannelId";

                 NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Plot sync is in progress..."));
                 showNotification(notificationlId, channelId, channelName,context,  "Plot sync is in progress.. ", notificationStyle,null );

                                     AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                           @Override
                                           public void run() {

                                                 for(FarmPlot plot: unSyncedPlots){

                                                       long id        = plot.getPlotId();
                                                       String name = plot.getName();
                                                       String description = plot.getDescription();
                                                       double plotSize       = plot.getSizeAc();
                                                       String mapId =           plot.getMapId();
                                                       String cropId  = plot.getCropId();
                                                       String orgId      = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);
                                                       String userId     = SharePrefManager.getInstance(context).getUserDetails().get(0);
                                                       String farmId     = plot.getFarmId();

                                                       Log.d(TAG, "Plots loop:  " + "id " + id + " name  " + name + " org id  " + orgId  + " user id " + userId   );

                                                       agroWebAPI.createFarmPlot(name, description, plotSize,  mapId, cropId, orgId, userId, farmId).enqueue(new Callback<JsonElement>() {
                                                             @Override
                                                             public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                                                                   if (response.isSuccessful()){
                                                                         AppExecutors.getInstance().getDiskIO().execute(() -> {
                                                                               mapDatabase.plotDaoAccess().updateSyncStatusById(id);
                                                                               showNotification(notificationlId, channelId, channelName,context,  "Plot sync is now complete!", notificationStyle,null );
                                                                         });

                                                                   }else if (response.code() == 500){
                                                                         Log.d(TAG, "onResponse:  " + response.body());
                                                                         Log.d(TAG, "onResponse:  " + response.body());
                                                                         showNotification(notificationlId, channelId, channelName,context,  "Plot sync failed ", notificationStyle,null );
                                                                   }
                                                             }

                                                             @Override
                                                             public void onFailure(Call<JsonElement> call, Throwable t) {

                                                             }
                                                       });
                                                 }
                                           }
                                     });
                 }
      }

      synchronized public static  void fetchAllPlots(Context context) throws JSONException {
            myApplication=new MyApplication();
            mapDatabase = MapDatabase.getInstance(context);
            AgroWebAPI agroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            List<FarmPlot> unSyncedPlots = mapDatabase.plotDaoAccess().fetchAllUnSyncedPlots();
            if (myApplication.hasNetworkConnection(context) && unSyncedPlots.size() != 0 ){

                  int notificationlId = 23894;
                  String channelName = "plotsPullChannel";
                  String channelId         = "plotsPullChannelId";

                  NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Plot sync is in progress..."));
                  showNotification(notificationlId, channelId, channelName,context,  "Plot sync is in progress.. ", notificationStyle,null );

                  AppExecutors.getInstance().getDiskIO().execute(() -> {

//
                        String orgId = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);
                        String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);

                        agroWebAPI.getPlotsCall(orgId, userId).enqueue(new Callback<PlotResponse>() {
                              @Override
                              public void onResponse(Call<PlotResponse> call, Response<PlotResponse> response) {

                                    if (response.isSuccessful()){
                                          List<FarmPlot> farmPlots = null;
                                          if (response.body() != null) {
                                                farmPlots = response.body().getPlots();

                                                for (FarmPlot farmPlot : farmPlots){

                                                      if (mapDatabase.plotDaoAccess().plotExist(farmPlot.getId()) == null){

                                                            farmPlot.setOrgId(orgId);
                                                            farmPlot.setUserId(userId);

                                                            AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                                                  @Override
                                                                  public void run() {
                                                                        mapDatabase.plotDaoAccess().insert(farmPlot);
                                                                  }
                                                            });
                                                      }
                                                }

                                                showNotification(notificationlId, channelId, channelName, context, "Plots are syncing!", notificationStyle, null);
                                          }
                                    }
                              }

                              @Override
                              public void onFailure(Call<PlotResponse> call, Throwable t) {
                                    Log.e(TAG, "onFailure:  " + t.getMessage() );
                              }
                        });
//
                  });
            }
      }

      public static void DownloadAllPlots(Context context){

            mapDatabase = MapDatabase.getInstance(context);

            String channelName = "synPcChannel";
            String channelId = "DBSyncChannelId";

            AgroWebAPI AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            String userId = "";
            if (SharePrefManager.isSecondaryUserAvailable(context)){
                  userId = SharePrefManager.getSecondaryUserPref(context);
            }else {
                  userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
            }
            String orgId   = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

            if (mapDatabase.plotDaoAccess().plotCountByOrgId(orgId) != 0){
                  AppExecutors.getInstance().getDiskIO().execute(() -> mapDatabase.plotDaoAccess().deleteSyncedPlots());
            }

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            int importance = NotificationManager.IMPORTANCE_LOW;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.drawable.download)
                    .setContentTitle("Downloading your plots..")
                    .setContentText("Download in progress");

            String finalUserId = userId;
            AGRO_WEB_API.getPlotsCall(orgId, userId).enqueue(new Callback<PlotResponse>() {
                  @Override
                  public void onResponse(Call<PlotResponse> call, Response<PlotResponse> response) {

                        int totalLengthOfPlots = response.body().getPlots().size();
                        int count = 0;
                        final int[] dbPlotCounts = {0};

                        try {

                              int progressCounter = 0;

                              for (FarmPlot plot : response.body().getPlots()) {

                                    progressCounter++;

                                    FarmPlot farmPlotModel = new FarmPlot();

                                    int finalProgressCounter = progressCounter;
                                    int finalProgressCounter1 = progressCounter;
                                    AppExecutors.getInstance().getDiskIO().execute(() -> {
                                          try {

                                                farmPlotModel.setName(plot.getName());

                                                Log.i(TAG, "onResponse:  Sync Plot" + plot.getName());
                                                Farm farm = mapDatabase.farmDaoAccess().fetchFarmByFarmId(plot.getFarmId());
                                                Log.i(TAG, "Count:  Farm " + farm);
                                                if (farm != null) {
                                                      farmPlotModel.setPrimaryFarmId(farm.getFarmId());
                                                }

                                                farmPlotModel.setId(plot.getId());
                                                farmPlotModel.setFarmId(plot.getFarmId());
                                                farmPlotModel.setFarmName(plot.getFarmName());
                                                farmPlotModel.setCropName(plot.getCropName());
                                                farmPlotModel.setCropId(plot.getCropId());
                                                farmPlotModel.setMapId(plot.getMapId());
                                                farmPlotModel.setUserId(finalUserId);
                                                farmPlotModel.setOrgId(orgId);
                                                farmPlotModel.setSynced(true);

                                                if (plot.getCreatedDateString() != null){
                                                      long milliseconds = HelperClass.convertDateTimeToMilliseconds(plot.getCreatedDateString());
                                                      farmPlotModel.setCreatedAt(milliseconds);
                                                      farmPlotModel.setUpdatedAt(milliseconds);
                                                }

                                                mapDatabase.plotDaoAccess().insert(farmPlotModel);
                                                dbPlotCounts[0] = mapDatabase.plotDaoAccess().plotCountByOrgId(orgId);

                                                Log.i(TAG, "Plots Download DB Count " + dbPlotCounts[0]);
                                                Log.i(TAG, "Plots Download API Plot length " + totalLengthOfPlots);

                                                // Sets the progress indicator to a max value, the current completion percentage and "determinate" state
                                                mBuilder.setProgress(totalLengthOfPlots, finalProgressCounter, false);
                                                // Displays the progress bar for the first time.

                                                notificationManager.notify(PLOTS_NOTIFICATION_ID, mBuilder.build());

                                                EventBus.getDefault().post(new PlotProgressEvent(finalProgressCounter1, totalLengthOfPlots));

                                                if (dbPlotCounts[0] == totalLengthOfPlots) {
                                                      // When the loop is finished, updates the notification
                                                      mBuilder.setContentText("Download completed")
                                                              // Removes the progress bar
                                                              .setProgress(0,0,false);
                                                      notificationManager.notify(PLOTS_NOTIFICATION_ID, mBuilder.build());
                                                      EventBus.getDefault().post(new PlotsDownloadCompleted(false));
                                                }

                                          } catch (Exception e) {

                                                Log.e(TAG, "run: SyncPlots loop  " + e.getMessage());
                                          }
                                    });

                                    count++;

                              }

                        } catch (Exception e) {

                              NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Plots DB Syncing failed.."));
                              showNotification(PLOTS_NOTIFICATION_ID, channelId, channelName, context, "Syncing failed!", notificationStyle, null);
                              EventBus.getDefault().post(new PlotsDownloadCompleted(true));

                        }

                        AppExecutors.getInstance().getDiskIO().execute(() -> {
                              AppNotification appNotification = new AppNotification();
                              appNotification.setName(AppNotification.NOTIFICATION_PLOTS);
                              appNotification.setMessage("Plots have been downloaded to your device");
                              appNotification.setOwner(AppNotification.NOTIFICATION_PLOTS);
                              appNotification.setDateCreated(System.currentTimeMillis());
                              mapDatabase.notificationDaoAccess().insert(appNotification);
                        });

                        EventBus.getDefault().post(new PlotsDownloadCompleted(true));
                  }

                  @Override
                  public void onFailure(Call<PlotResponse> call, Throwable t) {
                        EventBus.getDefault().post(new PlotsDownloadCompleted(true));
                  }
            });
      }

      public  static void pullFarms(Context context){
            int notificationlId = 1008;
            String channelName = "syncFarmsChannel";
            String channelId         = "syncFarmUpdatesChannelId";
            mapDatabase  = MapDatabase.getInstance(context);
            AgroWebAPI AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
            String orgId   = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

            // if there is any record of farm then delete all first.
            if (mapDatabase.farmDaoAccess().fetchAllFarms(userId, orgId).size() != 0) {
                  mapDatabase.farmDaoAccess().deleteFarmsByUser(userId, orgId);
            }

            AGRO_WEB_API.fetchFarmsCall(userId, orgId).enqueue(new Callback<FarmResponse>() {
                  @Override
                  public void onResponse(Call<FarmResponse> call, Response<FarmResponse> response) {

                        Log.d(TAG, "onResponse:  Farm : " + response.code());

                        if (response.isSuccessful()) {

                              for (Farm farm : response.body().getFarms()) {

                                    Farm farmModel = new Farm();
                                    farmModel.setName(farm.getName());
                                    farmModel.setId(farm.getId());
                                    farmModel.setOrgId(orgId);
                                    farmModel.setUserId(userId);
                                    farm.setSynced(true);

                                    Log.d(TAG, "Farm " + farm.getName());
                                    Log.d(TAG, "Farm " + farm.getId());
                                    Log.d(TAG, "Farm " + farm.getSynced());

                                    AppExecutors.getInstance().getDiskIO().execute(() -> mapDatabase.farmDaoAccess().insert(farmModel));
                              }

                                    NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Downloading farms .."));
                                    showNotification(notificationlId, channelId, channelName, context, "Downloading farms ..", notificationStyle, null);
                        }
                  }

                  @Override
                  public void onFailure(Call<FarmResponse> call, Throwable t) {
                        Log.e(TAG, "onFailure:  " + t.getMessage());
                  }
            });
      }

      public static void UpdatePlotSizes( Context context){

            MapDatabase mapDatabase = MapDatabase.getInstance(context);

            String channelName = "UpdatePlotSizesSyncChannel";
            String channelId = "UpdatePlotSizesChannelId";

            String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
            String orgId   = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

//            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
//                    .setSmallIcon(R.drawable.download)
//                    .setContentTitle("Updating plot sizes..")
//                    .setContentText("Updates in progress");

            if (mapDatabase.plotDaoAccess().plotCountByOrgId(orgId) != 0) {
//                  mapDatabase.plotDaoAccess().deletePlots();
            }

            AppExecutors.getInstance().getDiskIO().execute(() -> {

                  try {

                        List<MapModel> mapModelList = mapDatabase.daoAccess().fetchAllMapsList(userId, orgId);

                        int progressCounter = 0;

                        for (MapModel mapModel : mapModelList){

                              progressCounter++;

                              List<FarmPlot> plots =  mapDatabase.plotDaoAccess().fetchPlotsByMapId(mapModel.getId());

                              for (FarmPlot farmPlot : plots){
                                    farmPlot.setSizeAc(mapModel.getSizeAc());
                                    farmPlot.setSizeKm(mapModel.getSizeKm());
                                    farmPlot.setSizeM(mapModel.getSizeM());
                                    mapDatabase.plotDaoAccess().updatePlot(farmPlot);
                              }

//                              mBuilder.setProgress(mapModelList.size(), progressCounter, false);
                              // Displays the progress bar for the first time.

//                              notificationManager.notify(UPDATE_PLOTS_NOTIFICATION_ID, mBuilder.build());

                              Log.d(TAG, "run: show Perimeter:  km " + mapModel.getSizeKm() + "  acreage " + mapModel.getSizeAc());
                        }

                        NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Updating plot sizes..."));
                        showNotification(UPDATE_PLOTS_NOTIFICATION_ID, channelId, channelName,context,  "Updating plot sizes... ", notificationStyle,null );

                  }catch (Exception e){
                        Log.e(TAG, "UpdatePlotSizes Error " + e.getMessage() );
                  }
            });

      }

      public static void showNotification(int notificationId, String channelId,
                                          String channelName, Context context, String body, NotificationCompat.Style notificationStyle, Intent intent) {

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            int importance = NotificationManager.IMPORTANCE_HIGH;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setVibrate(DEFAULT_VIBRATION_PATTERN)
                    .setContentText(body)
                    .setStyle(notificationStyle)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setNumber(1)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                    .setAutoCancel(true);

            if (intent != null){
                  TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                  stackBuilder.addNextIntent(intent);
                  PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                          0,
                          PendingIntent.FLAG_UPDATE_CURRENT
                  );
                  mBuilder.setContentIntent(resultPendingIntent);
            }

            notificationManager.notify(notificationId, mBuilder.build());
      }

}
