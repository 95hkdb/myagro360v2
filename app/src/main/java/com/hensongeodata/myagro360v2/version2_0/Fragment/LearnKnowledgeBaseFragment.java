package com.hensongeodata.myagro360v2.version2_0.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.response.KnowledgeBaseResponse;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.helper.ItemAnimation;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.KnowledgeBase;
import com.hensongeodata.myagro360v2.version2_0.Adapter.KnowledgeBaseAdapter;
import com.hensongeodata.myagro360v2.version2_0.Events.KBDownloadCompleted;
import com.hensongeodata.myagro360v2.version2_0.KnowledgeBaseDetailFragment;
import com.hensongeodata.myagro360v2.version2_0.Preference.DownloadsSharePref;
import com.hensongeodata.myagro360v2.version2_0.sync.KB.KnowledgeBaseDownloadIntentService;
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class LearnKnowledgeBaseFragment extends Fragment {

      private static final String TAG = LearnKnowledgeBaseFragment.class.getSimpleName();

      private static final String ARG_PARAM1 = "param1";
      private static final String ARG_PARAM2 = "param2";

      private CompositeDisposable disposable = new CompositeDisposable();
      private RecyclerView recyclerView;
      private LinearLayout progressLayout;
      private KnowledgeBaseAdapter mAdapter;
      private List<KnowledgeBase> items = new ArrayList<>();
      private int animation_type = ItemAnimation.BOTTOM_UP;
      private Unbinder unbinder;
      private MapDatabase mapDatabase;
      private int KbDBSize;
      private Dialog progressDialog;

      private String mParam1;
      private String mParam2;

//      @BindView(R.id.input_search)
      EditText inputSearch;

//      private OnPlotActivitiesFragmentListener mListener;
      private View parent_view;
      private AgroWebAPI mAgroWebAPI;
      private MainViewModel  mainViewModel;
      private MyApplication myApplication;


      public LearnKnowledgeBaseFragment() { }

      public static LearnKnowledgeBaseFragment newInstance(String param1, String param2) {
            LearnKnowledgeBaseFragment fragment = new LearnKnowledgeBaseFragment();
            Bundle args = new Bundle();
            args.putString(ARG_PARAM1, param1);
            args.putString(ARG_PARAM2, param2);
            fragment.setArguments(args);
            return fragment;
      }

      @Override
      public void onCreate(Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                  mParam1 = getArguments().getString(ARG_PARAM1);
                  mParam2 = getArguments().getString(ARG_PARAM2);
            }
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            View view  = inflater.inflate(R.layout.fragment_learn_knowledge_base, container, false);
            unbinder = ButterKnife.bind(getActivity(), view);

            parent_view = view.findViewById(android.R.id.content);
            progressLayout = view.findViewById(R.id.lyt_progress);
            mAgroWebAPI = NetworkRequest.getRetrofit(Keys.FAW_API).create(AgroWebAPI.class);
            mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            mapDatabase      = MapDatabase.getInstance(getContext());
            myApplication    = new MyApplication();

            getMainViewModel();

            initComponent(view);

            new Handler().postDelayed(() -> {
                  setAdapter();
                  progressLayout.setVisibility(View.GONE);
            },2000);

            if (myApplication.hasNetworkConnection(getContext())){

                  Log.i(TAG, "onCreateView:  Downloads " + DownloadsSharePref.isPlotsDownloadPreferenceAvailable(getContext()));
                  Log.i(TAG, "onCreateView:  Downloads " + DownloadsSharePref.getPlotsDownloadedPreference(getContext()));

                  if (!DownloadsSharePref.isKBDownloadPreferenceAvailable(getContext()) || !DownloadsSharePref.getKBDownloadedPreference(getContext())){
//                        showKBDownloadDialog();
                        Intent intentToSyncImmediately = new Intent(getContext(), KnowledgeBaseDownloadIntentService.class);
                        getContext().startService(intentToSyncImmediately);
                        new Handler().postDelayed(this::showProgressDialog, 10);
                  }
            }

            return view;
      }

      @Override
      public void onDetach() {
            super.onDetach();
//            mListener = null;
      }


      private void initComponent(View view) {

            Context context = getContext();

            recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setHasFixedSize(true);

            animation_type = ItemAnimation.BOTTOM_UP;
      }

      private void setAdapter() {
            //set data and list adapter
                  mAdapter = new KnowledgeBaseAdapter(getContext(), items, animation_type);
                  recyclerView.setAdapter(mAdapter);


            // on item list clicked
            mAdapter.setOnItemClickListener(new KnowledgeBaseAdapter.OnItemClickListener() {
                  @Override
                  public void onItemClick(View view, KnowledgeBase obj, int position) {

                        KnowledgeBaseDetailFragment fragment = new KnowledgeBaseDetailFragment();
                        fragment.setData(obj);
                        fragment.show(getChildFragmentManager(), fragment.getTag());
                  }
            });
      }

      private void getMainViewModel(){
            mainViewModel.getKnowledgeBaseList().observe(getActivity(), new androidx.lifecycle.Observer<List<KnowledgeBase>>() {
                  @Override
                  public void onChanged(List<KnowledgeBase> knowledgeBases) {

                        if (knowledgeBases.size() != 0){

//                              items.clear();
                              items.addAll(knowledgeBases);
                        }

                        Log.i(TAG, "onChanged:  kb db size " + KbDBSize);
                  }
            });
      }

      private void getKnowledgeBaseFromAPI() {

            mAgroWebAPI.fetchKnowledgeBase("english", "app",Keys.API_KEY)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<KnowledgeBaseResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {

                          }

                          @Override
                          public void onNext(KnowledgeBaseResponse knowledgeBaseResponse) {
                                items.clear();
                                items.addAll(knowledgeBaseResponse.getTopics());
                                setAdapter();
                          }

                          @Override
                          public void onError(Throwable e) {

                          }

                          @Override
                          public void onComplete() {
                                progressLayout.setVisibility(View.GONE);
                          }
                    });
      }

      private void checkKBSize(){
            AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                  @Override
                  public void run() {
                        KbDBSize = mapDatabase.knowledgeBaseDaoAccess().knowledgeBaseCount();
                  }
            });
      }

      private DisposableObserver<TextViewTextChangeEvent> searchItems() {
            return new DisposableObserver<TextViewTextChangeEvent>() {
                  @Override
                  public void onNext(TextViewTextChangeEvent textViewTextChangeEvent) {
                        Log.d(TAG, "Search query: " + textViewTextChangeEvent.text());
                        mAdapter.getFilter().filter(textViewTextChangeEvent.text());
                  }

                  @Override
                  public void onError(Throwable e) {
                        Log.e(TAG, "onError: " + e.getMessage());
                  }

                  @Override
                  public void onComplete() {

                  }
            };
      }

      private void showKBDownloadDialog() {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Download Knowledgebase to offline?");
            builder.setMessage(R.string.download_plots_string);
            builder.setPositiveButton("OK, Download!", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intentToSyncImmediately = new Intent(getContext(), KnowledgeBaseDownloadIntentService.class);
                        getContext().startService(intentToSyncImmediately);

                        new Handler().postDelayed(() -> showProgressDialog(), 10);
                  }
            });
            builder.setNegativeButton("NO", null);
            builder.show();
      }

      private void showProgressDialog() {
            progressDialog = new Dialog(getContext());
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            progressDialog.setContentView(R.layout.dialog_kb_progress);
            progressDialog.setCancelable(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(progressDialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            progressDialog.show();
            progressDialog.getWindow().setAttributes(lp);
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onKBFragmentDownloadCompleted(KBDownloadCompleted event) {

            if (progressDialog != null && progressDialog.isShowing()) {
                  progressDialog.dismiss();
            }

//            progressDialog.dismiss();
//            Toast.makeText(getContext(), "Event from Plots", Toast.LENGTH_LONG).show();

            if (!event.isFailed()){

                  if (DownloadsSharePref.isKBDownloadPreferenceAvailable(getContext())){
                        DownloadsSharePref.clearKBDownloadedPreference(getContext());
                  }
                  DownloadsSharePref.setKBDownloadedPreference(getContext(), true);
            }else {
                  Toast.makeText(getContext(), "Plots failed to download!", Toast.LENGTH_SHORT).show();
            }

      }

      @Override
      public void onStart() {
            super.onStart();
            EventBus.getDefault().register(this);
      }

      @Override
      public void onStop() {
            EventBus.getDefault().unregister(this);
            super.onStop();
      }

}
