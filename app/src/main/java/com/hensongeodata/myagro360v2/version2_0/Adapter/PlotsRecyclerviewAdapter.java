package com.hensongeodata.myagro360v2.version2_0.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hensongeodata.myagro360v2.Interface.HistoryTabListener;
import com.hensongeodata.myagro360v2.Interface.ItemTouchHelperViewHolder;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.version2_0.Listeners.PlotsRvListener;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;
import com.hensongeodata.myagro360v2.version2_0.Model.POISModel;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user1 on 8/2/2016.
 */

public class PlotsRecyclerviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG = PlotsRecyclerviewAdapter.class.getSimpleName();
    private List<FarmPlot> items;
    private Context mContext;
    private HistoryTabListener listener;
    private PlotsRvListener plotsRvListener;
    private int mode;
    private ArrayList<String> strPois;
    protected DatabaseHelper databaseHelper;
    private MapDatabase mapDatabase;
    private long perimeter;
    private double inKilometers;
    private double acreage;
    private MyApplication myApplication;

    public PlotsRecyclerviewAdapter(Context context, List<FarmPlot> items, HistoryTabListener listener, PlotsRvListener plotsRvListener) {
       this.plotsRvListener=plotsRvListener;
        mContext=context;
        this.items =items;
        this.listener=listener;
        mapDatabase = MapDatabase.getInstance(context);
        myApplication = new MyApplication();
    }


    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.plots_list, parent, false);
            return new TabHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof TabHolder) {

            FarmPlot farmPlot = items.get(position);

            Log.i(TAG, "onBindViewHolder:  " + farmPlot.getName());

            final TabHolder tabHolder = (TabHolder) holder;
            final String plotId = farmPlot.getId();
            final long mainId = farmPlot.getPlotId();
            final String mapId = farmPlot.getMapId();
            final String name = farmPlot.getName();
            final String cropId = farmPlot.getCropId();
            final String cropName = farmPlot.getCropName();
            final double plotSize = farmPlot.getSizeAc();
            final String    farmName = farmPlot.getFarmName();
            final boolean synced = farmPlot.isSynced();

            if (!synced){

            }

            if (name != null ){

                tabHolder.mapTitleTextView.setText(String.format("%s - %s", name, farmName));
                tabHolder.cropTextView.setText(cropName);

                String cropAndSizeText = plotSize + " acres ";
                tabHolder.mapSizeTextView.setText(String.valueOf(cropAndSizeText ));

                tabHolder.moreImageView.setOnClickListener(v -> {
                    plotsRvListener.moreClicked(mainId, name, plotId, mapId);
                });

                if (farmPlot.getCreatedAt() != null) {
                    String dateString = new SimpleDateFormat("dd/MM/yyyy hh:mm").format(new Date(farmPlot.getCreatedAt()));

                    tabHolder.createdAt.setText(dateString);

                    Log.i(TAG, "onBindViewHolder:  Date Created " + dateString);
                    Log.i(TAG, "onBindViewHolder:  Date Created " + farmPlot.getCreatedAt());
                }
            }

            AppExecutors.getInstance().getNetworkIO().execute(() -> {
                HelperClass.LoadWebView(tabHolder.webView, mapId, tabHolder.noPreviewAvailableImageView, mContext);
            });

            tabHolder.moreImageView.setOnClickListener(v -> {
                plotsRvListener.moreClicked(mainId, name, plotId, mapId);
            });

            tabHolder.shareButton.setOnClickListener(v -> {
                plotsRvListener.shareButtonClicked(mainId, name, plotId, mapId);
            });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class TabHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
        public TextView mapTitleTextView;
        public TextView cropTextView;
        public WebView webView;
        public ProgressBar progressBar;
        public TextView mapSizeTextView;
        public ImageView moreImageView;
        public Button shareButton;
        public ImageView noPreviewAvailableImageView;
        public TextView createdAt;

        public TabHolder(View itemView) {
            super(itemView);

            mapTitleTextView = itemView.findViewById(R.id.map_title);
            webView                  = itemView.findViewById(R.id.webview);
            progressBar           = itemView.findViewById(R.id.progress_bar);
            mapSizeTextView = itemView.findViewById(R.id.map_size);
            moreImageView   = itemView.findViewById(R.id.more);
            shareButton          = itemView.findViewById(R.id.share_map);
            cropTextView         = itemView.findViewById(R.id.crops);
            noPreviewAvailableImageView = itemView.findViewById(R.id.no_preview_available);
            createdAt                                           = itemView.findViewById(R.id.created_at);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

    private double metersToKilo(long metersSquared){
        return metersSquared*0.001;
    }

    private double metersToAcres(long meterSquared){
        return Math.round((meterSquared*0.00024711) * 1000.0) / 1000.0;
    }


    private void showPerimeter(PlotsRecyclerviewAdapter.TabHolder tabHolder, String section_id){

        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
            @Override
            public void run() {

                List<POISModel> poisModelList = mapDatabase.poisDaoAccess().fetchPOISByMapId(section_id);
                strPois =new ArrayList<>();

                for (POISModel poisModel : poisModelList) {
                    if(poisModel!=null) {
                        String message = poisModel.getInfo();
                        if (message != null && !message.trim().isEmpty())
                            strPois.add(message);
                        Log.d(TAG, "run: show Perimeter:  " + message);
                    }
                }

                ArrayList<POI> pois= CreateForm.getInstance(mContext).buildPOI_Mapping(strPois);
                int area= MyApplication.getArea(pois);
                perimeter = MyApplication.getPerimeter(pois);
                inKilometers = metersToKilo(perimeter);
                acreage = metersToAcres(perimeter);

                Log.d(TAG, "run: show Perimeter:  km" + inKilometers + "  acreage" + acreage);
            }
        });


        tabHolder.mapSizeTextView.setText(String.format("%sm | %skm |  %s acres", String.valueOf(perimeter), inKilometers, acreage));

    }

}