package com.hensongeodata.myagro360v2.version2_0.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;

import java.util.HashMap;

public class ChoosePlotDialogFragment extends DialogFragment implements AdapterView.OnItemSelectedListener {

    private static final String TAG = ChoosePlotDialogFragment.class.getSimpleName();

    private View root_view;
    private AppCompatSpinner plots_spinner;
    private AppCompatSpinner spn_activities;
    private AgroWebAPI mAgroWebAPI;
    private String mMapId;
    private MainViewModel mMainViewModel;
    private HashMap<Integer, String> mapSpinnerHashMap;
    private MapDatabase mMapDatabase;
    private WebView webView;
    private String mapId;
    private String mPlotCrop;
    private double mPlotSize;
    private TextView plotCropTextView;
    private TextView plotSizeTextView;
    private ProgressBar mWebViewProgressBar;
    private Button btnContinue;
    private ImageView noPreviewAvailableImageView;
    private int plot_id;

      private OnAddFarmActivityDialogFragmentListener onAddFarmActivityDialogFragmentListener;
      private OnChoosePlotDialogFragmentListener onChoosePlotDialogFragmentListener;

      @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            mMapDatabase = MapDatabase.getInstance(getContext());
            mMainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            root_view = inflater.inflate(R.layout.fragment_choose_plot_dialog, container, false);
            plots_spinner    = root_view.findViewById(R.id.spn_plots);
            spn_activities = root_view.findViewById(R.id.spn_activities);
            webView          = root_view.findViewById(R.id.plot_web_view);
            plotCropTextView = root_view.findViewById(R.id.plot_crop);
            plotSizeTextView  = root_view.findViewById(R.id.plot_size);
            mWebViewProgressBar = root_view.findViewById(R.id.webview_progress);
            btnContinue                      = root_view.findViewById(R.id.bt_continue);
            noPreviewAvailableImageView = root_view.findViewById(R.id.no_preview_available);

            btnContinue.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        onContinueButtonClicked(plot_id);
                  }
            });

          root_view.findViewById(R.id.bt_close).setOnClickListener(v -> {
                onDismissButtonClicked();
                dismiss();

                Log.i(TAG, "onCreateView:  Dismiss from ChoosePlotDialogFragment" );
          });

          mAgroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            fetchPlotsFromDatabase();

            plots_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                      plot_id = Integer.valueOf(mapSpinnerHashMap.get(plots_spinner.getSelectedItemPosition()));
                      Log.i(TAG, "onItemSelected:  PLOT ID " + plot_id);

                      AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                  if (plot_id != 0){
                                        FarmPlot plotObject  = mMapDatabase.plotDaoAccess().fetchPlotByPlotId((plot_id));
                                        Log.i(TAG, "onItemSelected:  PLOT ID " + plot_id);
                                        mapId = plotObject.getMapId();
                                        mPlotCrop = plotObject.getCropName();
                                        mPlotSize    = plotObject.getSizeAc();
                                  }

                            }
                      });

                      final Handler handler = new Handler();
                      handler.postDelayed(() -> {
                            HelperClass.LoadWebView(webView, mapId, noPreviewAvailableImageView, getContext());
                            plotCropTextView.setText(mPlotCrop);
                            plotSizeTextView.setText(String.format("%s acre(s)", String.valueOf(mPlotSize)));
                            mWebViewProgressBar.setVisibility(View.GONE);
                      }, 1500);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {}
          });

          return root_view;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) { }

    @Override
    public void onNothingSelected(AdapterView<?> parent) { }


    private void setupArrayAdapter(String[] stringArray, AppCompatSpinner spinner){
        ArrayAdapter<String> array = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_item, stringArray);
        array.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(array);
    }

      private void fetchPlotsFromDatabase(){
            mMainViewModel.getPlots().observe(getActivity(), farmPlotList -> {
                  String[] spinnerArray = new String[farmPlotList.size()];
                  try {

                        mapSpinnerHashMap = new HashMap<Integer, String>();
                        for ( int i =0; i < farmPlotList.size(); i++ ){
//                              mapSpinnerHashMap.put(i, String.valueOf(farmPlotList.get(i).getId()));
                              mapSpinnerHashMap.put(i, String.valueOf(farmPlotList.get(i).getPlotId()));
                              spinnerArray[i] = farmPlotList.get(i).getName();
                              Log.i(TAG, "onChanged:  Plots Spinner " + farmPlotList.get(i).getPlotId());
                              Log.i(TAG, "onChanged:  Plots Spinner " + farmPlotList.get(i).getName());
                        }
                        setupArrayAdapter(spinnerArray, plots_spinner);
                  } catch (Exception e) {
                        Log.e(TAG, "onNext:  " + e.getMessage());
                  }
            });
      }

      private void onContinueButtonClicked(long plotId){
            if (onChoosePlotDialogFragmentListener != null) {
                  onChoosePlotDialogFragmentListener.onChoosePlotDialogFragmentListenerContinue(plotId);
            }
      }

      private void onDismissButtonClicked(){
            if (onChoosePlotDialogFragmentListener != null) {
                  onChoosePlotDialogFragmentListener.onChoosePlotDialogFragmentDismiss();
            }
      }


      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof ChoosePlotDialogFragment.OnChoosePlotDialogFragmentListener) {
                  onChoosePlotDialogFragmentListener = (ChoosePlotDialogFragment.OnChoosePlotDialogFragmentListener) context;
            } else {
                  throw new RuntimeException(context.toString()
                          + " must implement OnChoosePlotDialogFragmentListener");
            }
      }

      @Override
      public void onDetach() {
            super.onDetach();
            onChoosePlotDialogFragmentListener = null;
      }

      public interface OnAddFarmActivityDialogFragmentListener{
            void onAddFarmActivityDialogContinueButton(long plotId);
      }

      public interface OnChoosePlotDialogFragmentListener{
          void onChoosePlotDialogFragmentDismiss();
          void onChoosePlotDialogFragmentListenerContinue(long plotId);

      }
}