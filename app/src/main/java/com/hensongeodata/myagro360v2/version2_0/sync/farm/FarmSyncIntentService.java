package com.hensongeodata.myagro360v2.version2_0.sync.farm;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

public class FarmSyncIntentService extends IntentService {

      public FarmSyncIntentService() {
            super("FarmSyncIntentService");
      }

      @Override
      protected void onHandleIntent(@Nullable Intent intent) {
            FarmSyncTask.pullFarms(this);
      }
}
