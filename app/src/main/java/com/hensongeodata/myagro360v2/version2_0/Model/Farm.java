package com.hensongeodata.myagro360v2.version2_0.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity( tableName =  Farm.TABLE_NAME)
public class Farm {

      public static final String TABLE_NAME = "farms";

      public static final String COLUMN_ID = "id";
      public static final String COLUMN_FARM_ID = "farm_id";
      public static final String COLUMN_NAME = "name";
      public static final String COLUMN_USER_ID = "user_id";
      public static final String COLUMN_ORG_ID = "org_id";
      public static final String COLUMN_SYNCED = "synced";


      @PrimaryKey(autoGenerate = true)
      @ColumnInfo( name = COLUMN_FARM_ID)
      private long farmId;

      @SerializedName("id")
      @ColumnInfo( name = COLUMN_ID)
      private String id;

      @SerializedName("name")
      @ColumnInfo( name = COLUMN_NAME)
      private String name;

      @ColumnInfo( name = COLUMN_ORG_ID)
      private String orgId;

      @ColumnInfo( name = COLUMN_USER_ID)
      private String userId;

      @ColumnInfo( name = COLUMN_SYNCED)
      private boolean synced;


      public long getFarmId() {
            return farmId;
      }

      public void setFarmId(long farmId) {
            this.farmId = farmId;
      }

      public String getId() {
            return id;
      }

      public void setId(String id) {
            this.id = id;
      }

      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      public String getOrgId() {
            return orgId;
      }

      public void setOrgId(String orgId) {
            this.orgId = orgId;
      }

      public String getUserId() {
            return userId;
      }

      public void setUserId(String userId) {
            this.userId = userId;
      }

      public boolean getSynced() {
            return synced;
      }

      public void setSynced(boolean synced) {
            this.synced = synced;
      }
}
