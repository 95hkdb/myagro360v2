package com.hensongeodata.myagro360v2.version2_0.sync.map.downloads;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

public class MapsDownloadIntentService extends IntentService {

      public MapsDownloadIntentService() {
            super("MapSyncIntentService");
      }

      @Override
      protected void onHandleIntent(@Nullable Intent intent) {
            MapsDownloadTask.DownloadMaps(this);
      }
}
