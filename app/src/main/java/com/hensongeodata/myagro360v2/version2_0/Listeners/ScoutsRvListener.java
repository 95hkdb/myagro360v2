package com.hensongeodata.myagro360v2.version2_0.Listeners;

public interface ScoutsRvListener {

      void onScoutingFragmentMoreClicked(long id, long plotId);

      void  onReadButtonClicked(String name);
}