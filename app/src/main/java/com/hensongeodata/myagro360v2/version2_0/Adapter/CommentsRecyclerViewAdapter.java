package com.hensongeodata.myagro360v2.version2_0.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hensongeodata.myagro360v2.Interface.ItemTouchHelperViewHolder;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.version2_0.Model.CommentModel;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;
import com.hensongeodata.myagro360v2.version2_0.Util.TimeAgo;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class CommentsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG = CommentsRecyclerViewAdapter.class.getSimpleName();
    private List<CommentModel> items;
    private Context mContext;
    private int mode;
    protected DatabaseHelper databaseHelper;
    private MapDatabase mapDatabase;
    private MyApplication myApplication;

    public CommentsRecyclerViewAdapter(Context context, List<CommentModel> items) {
        mContext=context;
        this.items =items;
        mapDatabase = MapDatabase.getInstance(context);
    }


    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.comment_history, parent, false);
            return new TabHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof TabHolder) {

            CommentModel commentModel = items.get(position);

            Log.i(TAG, "onBindViewHolder:  Image Path" + commentModel.getCreatorName());

            final TabHolder tabHolder = (TabHolder) holder;

            Spannable word = new SpannableString(commentModel.getCreatorName());
            word.setSpan(new StyleSpan(Typeface.BOLD), 0, word.length(), 0);
            ((TabHolder) holder).commentContentTextView.setText(word);

            Spannable wordTwo = new SpannableString(" " + commentModel.getContent());
            ((TabHolder) holder).commentContentTextView.append(wordTwo);

            String dateCreatedString = commentModel.getDateCreated();

            long milliseconds = HelperClass.convertDateTimeToMilliseconds(dateCreatedString);

            String timeAgo = TimeAgo.getTimeAgo(milliseconds);

            tabHolder.timeAgoTextView.setText(timeAgo);

        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class TabHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
//        public TextView scoutTitleTextView;
        public TextView commentContentTextView;
        public TextView commentCreatorNameTextView;
        public TextView commentCommentsCountTextView;
        public TextView timeAgoTextView;


        public TabHolder(View itemView) {
            super(itemView);

            commentContentTextView = itemView.findViewById(R.id.textComment);
            timeAgoTextView                   = itemView.findViewById(R.id.time_ago);
//            commentCreatorNameTextView = itemView.findViewById(R.id.creator_name_text_view);
//            commentCommentsCountTextView = itemView.findViewById(R.id.comment_comments_count_text_view);

        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

    private double metersToAcres(long meterSquared){
        return Math.round((meterSquared*0.00024711) * 1000.0) / 1000.0;
    }

    private double metersToKilo(long metersSquared){
        return metersSquared*0.001;
    }

    private void showPerimeter(TabHolder tabHolder, String section_id){


//        tabHolder.mapSizeTextView.setText(String.format("%sm | %skm |  %s acres", String.valueOf(perimeter), inKilometers, acreage));

    }

}