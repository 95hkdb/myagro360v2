package com.hensongeodata.myagro360v2.version2_0.sync.plots;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

import org.json.JSONException;

public class PlotSyncIntentService extends IntentService {

      public PlotSyncIntentService() {
            super("PlotSyncIntentService");
      }

      @Override
      protected void onHandleIntent(@Nullable Intent intent) {
            try {
                  PlotSyncTask.syncAllPlots(this);
            } catch (JSONException e) {
                  e.printStackTrace();
            }
      }
}
