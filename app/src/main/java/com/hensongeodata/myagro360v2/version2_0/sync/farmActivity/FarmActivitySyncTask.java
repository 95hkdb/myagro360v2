package com.hensongeodata.myagro360v2.version2_0.sync.farmActivity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Events.PushedToServerEvent;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmActivity;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FarmActivitySyncTask {

      private static final String TAG = FarmActivitySyncTask.class.getSimpleName();
      private static MapDatabase mapDatabase;
      private static final long[] DEFAULT_VIBRATION_PATTERN =  {0, 250, 250, 250};
      protected static MyApplication myApplication;
      private static AgroWebAPI agroWebAPI;

      synchronized public static  void pushAllFarmActivities(Context context) throws JSONException {
            myApplication=new MyApplication();
            mapDatabase = MapDatabase.getInstance(context);
            String orgId = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);
            String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);

            agroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            List<FarmActivity> farmActivityList = mapDatabase.farmActivityDaoAccess().fetchNotSyncedActivities(userId, orgId);

            for( FarmActivity farmActivity : farmActivityList){
                  agroWebAPI.createFarmActivity(userId, orgId, farmActivity.getName(), farmActivity.getAdditionalInfo(),
                          farmActivity.getPlotId(), farmActivity.getStartDateTimeString(), farmActivity.getEndDateTimeString(),
                          farmActivity.getStaffId(), farmActivity.getType(), farmActivity.getOutSourcedName(), farmActivity.getId(),
                          false).enqueue(new Callback<FarmActivity>() {
                        @Override
                        public void onResponse(Call<FarmActivity> call, Response<FarmActivity> response) {
                              if (response.isSuccessful()){
                                    farmActivity.setSynced(true);
                                    mapDatabase.farmActivityDaoAccess().update(farmActivity);
                              }
                        }

                        @Override
                        public void onFailure(Call<FarmActivity> call, Throwable t) {
                              Log.e(TAG, "onFailure:  " + t.getMessage() );
                        }
                  });

            }
      }


      synchronized public static  void syncFarmActivity(Handler handler, Context context, long farmActivityId) {

            myApplication=new MyApplication();
            mapDatabase = MapDatabase.getInstance(context);
            String orgId = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);
            String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);

            agroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            final FarmActivity[] farmActivity = new FarmActivity[1];

            AppExecutors.getInstance().getDiskIO().execute(() -> {
                  farmActivity[0] = mapDatabase.farmActivityDaoAccess().fetchActivityByFarmActivityId(farmActivityId);
            });

            handler.postDelayed(() -> {

                  String startDateString = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(new Date(farmActivity[0].getStartDate()));
                  String endDateString = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(new Date(farmActivity[0].getEndDate()));

                  agroWebAPI.createFarmActivity(
                    userId,
                    orgId,
                    farmActivity[0].getName(),
                    farmActivity[0].getAdditionalInfo(),
                    farmActivity[0].getPlotId(),
                    startDateString,
                    endDateString,
                    farmActivity[0].getStaffId(),
                    farmActivity[0].getType(),
                    farmActivity[0].getOutSourcedName(),
                    farmActivity[0].getId(),
                    false).enqueue(new Callback<FarmActivity>() {
                  @Override
                  public void onResponse(Call<FarmActivity> call, Response<FarmActivity> response) {

                        if (response.isSuccessful()) {
                              AppExecutors.getInstance().getDiskIO().execute(() -> {
                                    farmActivity[0].setSynced(true);
                                    mapDatabase.farmActivityDaoAccess().update(farmActivity[0]);
                                    Log.i(TAG, "onResponse Farm Activity Code :  " + response.code());

                                    EventBus.getDefault().post(new PushedToServerEvent("Activity"));

                              });
                        }
                  }

                  @Override
                  public void onFailure(Call<FarmActivity> call, Throwable t) {
                        Log.e(TAG, "onFailure:  Farm Activity " + t.getMessage());
                  }
            });

            },2000);

      }

      public static void showNotification(int notificationId, String channelId,
                                          String channelName, Context context, String body, NotificationCompat.Style notificationStyle, Intent intent) {

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            int importance = NotificationManager.IMPORTANCE_HIGH;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setVibrate(DEFAULT_VIBRATION_PATTERN)
                    .setContentText(body)
                    .setStyle(notificationStyle)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setNumber(1)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                    .setAutoCancel(true);

            if (intent != null){
                  TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                  stackBuilder.addNextIntent(intent);
                  PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                          0,
                          PendingIntent.FLAG_UPDATE_CURRENT
                  );
                  mBuilder.setContentIntent(resultPendingIntent);
            }

            notificationManager.notify(notificationId, mBuilder.build());
      }

}
