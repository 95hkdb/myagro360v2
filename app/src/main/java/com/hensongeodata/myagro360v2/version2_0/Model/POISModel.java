package com.hensongeodata.myagro360v2.version2_0.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;


@Entity( tableName = POISModel.TABLE_NAME)
public class POISModel {

      public static final String TABLE_NAME = "pois";

      public static final String COLUMN_ID   = "id";
      public static final String COLUMN_INFO   = "info";
      public static final String COLUMN_MAP_ID= "map_id";


      @PrimaryKey(autoGenerate = true)
      @ColumnInfo( name = COLUMN_ID)
      @SerializedName("id")
      private long id;

      @ColumnInfo( name = COLUMN_INFO)
      private String info;

      @ColumnInfo( name = COLUMN_MAP_ID)
      @SerializedName("map_id")
      private String mapId;

      public long getId() {
            return id;
      }

      public void setId(long id) {
            this.id = id;
      }

      public String getInfo() {
            return info;
      }

      public void setInfo(String info) {
            this.info = info;
      }

      public String getMapId() {
            return mapId;
      }

      public void setMapId(String mapId) {
            this.mapId = mapId;
      }
}
