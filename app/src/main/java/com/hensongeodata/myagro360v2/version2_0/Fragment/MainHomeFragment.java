package com.hensongeodata.myagro360v2.version2_0.Fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.GridCategoryAdapter;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.helper.SpacingItemDecoration;
import com.hensongeodata.myagro360v2.helper.Tools;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.newmodel.WeatherModel;
import java.util.ArrayList;
import java.util.List;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainHomeFragment extends Fragment implements GridCategoryAdapter.OnItemClickListener{

      private static final String TAG = MainHomeFragment.class.getSimpleName();
      private RecyclerView mRecyclerView;
      private GridCategoryAdapter mGridCategoryAdapter;
      private AgroWebAPI mAgroWebAPI;
      private TextView mTextView;
      private MenuItemClickListener menuItemClickListener;
      private MyApplication myApplication;

      public MainHomeFragment() { }

      @Override
      public void onCreate(Bundle savedInstanceState) {
            mAgroWebAPI = NetworkRequest.getRetrofit(Keys.WEATHER_BASE_URI).create(AgroWebAPI.class);
            getWeatherInfoFromAPI();
            myApplication = new MyApplication();

            super.onCreate(savedInstanceState);
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_main_home, container, false);

            String firstName = "";

            if (SharePrefManager.getInstance(getContext()).getUserDetails() != null ){
                  firstName = (SharePrefManager.getInstance(getContext()).getUserDetails()).get(1)+"!";
            }

            TextView firstNameTextView = view.findViewById(R.id.first_name);
            firstNameTextView.setText(firstName);
            mTextView = view.findViewById(R.id.weather_temp_text);

            mRecyclerView = view.findViewById(R.id.recyclerView);
            mRecyclerView.setLayoutManager( new GridLayoutManager(getContext(), 1) );
            mRecyclerView.addItemDecoration(new SpacingItemDecoration(3, Tools.dip2px(getContext(), 8), true));
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setNestedScrollingEnabled(false);

            List<String> list = new ArrayList<>();
            list.add("RECORD AN ACTIVITY");
            list.add("SCAN FOR PESTS");
            list.add("MAP YOUR FARM");

            mGridCategoryAdapter = new GridCategoryAdapter(getContext(), list, new GridCategoryAdapter.OnItemClickListener() {
                  @Override
                  public void onItemClick(View view, String obj, int position) {
                        menuItemClickListener.onClicked(position);
                  }
            });
            mRecyclerView.setAdapter(mGridCategoryAdapter);

            return view;
      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);

            if (context instanceof MenuItemClickListener) {
                  menuItemClickListener = (MenuItemClickListener) context;
            } else {
                  throw new RuntimeException(context.toString()
                          + " must implement MenuItemClickListener");
            }
      }

      private void getWeatherInfoFromAPI() {

                  mAgroWebAPI.getWeather(Keys.WEATHER_API_KEY, "5.7218383", "-0.1658684")
                          .subscribeOn(Schedulers.io())
                          .observeOn(AndroidSchedulers.mainThread())
                          .subscribe(new Observer<WeatherModel>() {
                                Disposable disposable;

                                @Override
                                public void onSubscribe(Disposable d) {
                                      disposable = d;
                                }

                                @Override
                                public void onNext(WeatherModel weatherModel) {
                                           double temp = (weatherModel.getTemperature())-273.15;
                                            mTextView.setText( String.valueOf((int) temp));
                                            Log.i(TAG, "onNext:  " + temp );
                                }

                                @Override
                                public void onError(Throwable e) {
                                      Log.e(TAG, "onError:  " + e.getMessage() );
                                }

                                @Override
                                public void onComplete() {
                                      if (!disposable.isDisposed()) {
                                            disposable.dispose();
                                      }
                                }
                          });
//            }
      }

      @Override
      public void onItemClick(View view, String obj, int position) {

      }

      public interface MenuItemClickListener{

            void onClicked(int position);
      }
}
