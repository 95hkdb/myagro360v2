package com.hensongeodata.myagro360v2.version2_0.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hensongeodata.myagro360v2.Interface.HistoryTabListener;
import com.hensongeodata.myagro360v2.Interface.ItemTouchHelperViewHolder;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.version2_0.Listeners.ScansRvListener;
import com.hensongeodata.myagro360v2.version2_0.Model.ScanModel;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user1 on 8/2/2016.
 */

public class ScanHistoryRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG = ScanHistoryRecyclerViewAdapter.class.getSimpleName();
    private List<ScanModel> items;
    private Context mContext;
    private HistoryTabListener listener;
    private ScansRvListener scansRvListener;
    private LocsmmanEngine locsmmanEngine;
    private int mode;
    private ArrayList<String> strPois;
    protected DatabaseHelper databaseHelper;
    private MapDatabase mapDatabase;
    private MyApplication myApplication;

    public ScanHistoryRecyclerViewAdapter(Context context, List<ScanModel> items, ScansRvListener scansRvListener) {
        this.scansRvListener = scansRvListener;
        mContext=context;
        this.items =items;
        locsmmanEngine=new LocsmmanEngine(context);
        databaseHelper=new DatabaseHelper(mContext);
        mapDatabase = MapDatabase.getInstance(context);
        myApplication = new MyApplication();
    }


    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.scan_history, parent, false);
            return new TabHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof TabHolder) {

            ScanModel scanModel = items.get(position);

            Log.i(TAG, "onBindViewHolder:  Image Path" + scanModel.getImagePath());

            final TabHolder tabHolder = (TabHolder) holder;
            final long id                             = scanModel.getId();
            final int plantNumber           = scanModel.getPlantNumber();
            final String message              = scanModel.getMessage();
            final String imageUrl             = scanModel.getImagePath();
            final String pestIdentified    = scanModel.getPestIdentified();
            final long dateCreated      = scanModel.getDateCreated();

            tabHolder.scoutTitleTextView.setText(message);

            if (message != null){
                if (message.toLowerCase().equals("pest detected")){
                    tabHolder.scoutTitleTextView.setTextColor(mContext.getResources().getColor(R.color.red));
                }else {
                    tabHolder.scoutTitleTextView.setTextColor(mContext.getResources().getColor(R.color.colorGreen100));
                }
            }

            Log.i(TAG, "onBindViewHolder: Batch ID  " + scanModel.getBatchId());
            Log.i(TAG, "onBindViewHolder: Scout ID  " + scanModel.getScoutId());


            if (imageUrl != null){
                if (imageUrl.substring(0,5).equals("https")) {
                    Log.i(TAG, "onBindViewHolder: Image Url  Https" + imageUrl);
                    Picasso.get().load(imageUrl).into( tabHolder.noPreviewAvailableImageView);
                }else {
                    tabHolder.noPreviewAvailableImageView.setImageURI(Uri.parse(imageUrl));
                }
            }



            tabHolder.pestIdentifiedTextView.setText(pestIdentified);

            if (scanModel.isPestFound()){
                tabHolder.readMoreTextView.setPaintFlags(tabHolder.readMoreTextView .getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                tabHolder.readMoreTextView.setVisibility(View.VISIBLE);

                tabHolder.readMoreTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final int[] count = new int[1];


                        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                count[0] = mapDatabase.knowledgeBaseDaoAccess().knowledgeBaseCount();
                            }
                        });

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (count[0]== 0){

                                    Toast.makeText(mContext, "You will need to download Knowledgebase to access this!", Toast.LENGTH_SHORT).show();

                                }else {
                                    scansRvListener.onReadButtonClicked(pestIdentified);
                                }
                            }
                        }, 1000);

                    }
                });
            }

            tabHolder.moreImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    scansRvListener.moreClicked(id);
                }
            });

            String dateString = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(new Date(dateCreated));

            tabHolder.dateCreatedTextView.setText(String.valueOf(dateString));
//            tabHolder.dateCreatedTextView.setText(String.valueOf(dateCreated));

//            Log.i(TAG, "onBindViewHolder:  Image " + imageUrl);

        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class TabHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
        public TextView scoutTitleTextView;
        public WebView webView;
        public ProgressBar progressBar;
        public TextView pestIdentifiedTextView;
        public ImageView moreImageView;
        public Button shareButton;
        public ImageView syncImageView;
        public ImageView noPreviewAvailableImageView;
        public TextView readMoreTextView;
        public TextView dateCreatedTextView;

        public TabHolder(View itemView) {
            super(itemView);

            scoutTitleTextView = itemView.findViewById(R.id.scout_title);
            progressBar           = itemView.findViewById(R.id.progress_bar);
            pestIdentifiedTextView = itemView.findViewById(R.id.pest_identified);
            moreImageView   = itemView.findViewById(R.id.more);
            shareButton           = itemView.findViewById(R.id.share_map);
            syncImageView      = itemView.findViewById(R.id.sync_image);
            noPreviewAvailableImageView = itemView.findViewById(R.id.no_preview_available);
            readMoreTextView  = itemView.findViewById(R.id.read_textview);
            dateCreatedTextView = itemView.findViewById(R.id.date_created);

        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

    private double metersToAcres(long meterSquared){
        return Math.round((meterSquared*0.00024711) * 1000.0) / 1000.0;
    }

    private double metersToKilo(long metersSquared){
        return metersSquared*0.001;
    }

    private void showPerimeter(TabHolder tabHolder, String section_id){


//        tabHolder.mapSizeTextView.setText(String.format("%sm | %skm |  %s acres", String.valueOf(perimeter), inKilometers, acreage));

    }

}