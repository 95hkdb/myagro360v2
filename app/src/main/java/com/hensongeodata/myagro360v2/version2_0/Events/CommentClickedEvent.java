package com.hensongeodata.myagro360v2.version2_0.Events;

import com.hensongeodata.myagro360v2.version2_0.Model.ThreadModel;

public  class CommentClickedEvent {

      private ThreadModel threadModel;

      public CommentClickedEvent(ThreadModel threadModel) {
            this.threadModel = threadModel;
      }

      public ThreadModel getThreadModel() {
            return threadModel;
      }

      public void setThreadModel(ThreadModel threadModel) {
            this.threadModel = threadModel;
      }
}
