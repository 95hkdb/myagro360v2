package com.hensongeodata.myagro360v2.version2_0.Fragment;


import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonElement;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.response.ThreadResponse;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Adapter.ThreadRecyclerViewAdapter;
import com.hensongeodata.myagro360v2.version2_0.Events.DBSyncingComplete;
import com.hensongeodata.myagro360v2.version2_0.Events.ThreadCreatedEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForumFragment extends Fragment {

      private AgroWebAPI mAgroWebAPI;
      private RecyclerView recyclerView;
      private ThreadRecyclerViewAdapter threadRecyclerViewAdapter;
      private static final String TAG = AgroWebAPI.class.getSimpleName();
      private ProgressBar progressBar;
      private LinearLayout noConnectionLayout;
      private FloatingActionButton floatingActionButton;
      private Dialog progressDialog;

      public ForumFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            // Inflate the layout for this fragment

            View view =  inflater.inflate(R.layout.fragment_forum, container, false);


            recyclerView = view.findViewById(R.id.thread_history_recyclerview);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
            progressBar      = view.findViewById(R.id.progress_bar);
            noConnectionLayout = view.findViewById(R.id.lyt_no_connection);
            floatingActionButton = view.findViewById(R.id.fab_add);

            mAgroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_FORUM).create(AgroWebAPI.class);
            MyApplication myApplication = new MyApplication();

           if( myApplication.hasNetworkConnection(getContext())){
                 recyclerView.setVisibility(View.VISIBLE);
                 noConnectionLayout.setVisibility(View.GONE);
                  getThreadsFromAPI();
            }else{
                 recyclerView.setVisibility(View.GONE);
                 noConnectionLayout.setVisibility(View.VISIBLE);
            }

           floatingActionButton.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                       showCustomDialog();
                 }
           });


            return view;

      }

      private void getThreadsFromAPI() {

            mAgroWebAPI.fetchThreads()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ThreadResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(ThreadResponse threadResponse) {
                                Log.i(TAG, "onNext:  Thread Response " + threadResponse.getData().getThreads().get(0).getContent());
                                Log.i(TAG, "onNext:  Thread Response " + threadResponse.getData().getThreads().get(0).getTitle());

                                threadRecyclerViewAdapter = new ThreadRecyclerViewAdapter(getContext(), threadResponse.getData().getThreads());
                          }

                          @Override
                          public void onError(Throwable e) {
                                Log.i(TAG, "onError:  Thread Response " + e.getMessage());
                                Log.i(TAG, "onError:  Thread Response " + e.getCause());
                                Log.i(TAG, "onError:  Thread Response " + e.getStackTrace());

                          }

                          @Override
                          public void onComplete() {
                                recyclerView.setAdapter(threadRecyclerViewAdapter);
                                progressBar.setVisibility(View.GONE);

                          }
                    });
      }

      @Override
      public void onStart() {
            super.onStart();
            EventBus.getDefault().register(this);
      }

      @Override
      public void onStop() {
            EventBus.getDefault().unregister(this);
            super.onStop();
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onMessage(DBSyncingComplete event) {
//            Toast.makeText(getContext(), "Comment Event Received " + event.getThreadModel().getContent(), Toast.LENGTH_SHORT).show();
      }

      private void showCustomDialog() {
            final Dialog dialog = new Dialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            dialog.setContentView(R.layout.dialog_add_thread);
            dialog.setCancelable(true);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            String email = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
            String first_name = SharePrefManager.getInstance(getContext()).getUserDetails().get(1);
            String last_name = SharePrefManager.getInstance(getContext()).getUserDetails().get(2);

            final EditText et_post = dialog.findViewById(R.id.et_post);
            final EditText title        = dialog.findViewById(R.id.title);

            TextView nameTextView = dialog.findViewById(R.id.name);
            TextView emailTextView = dialog.findViewById(R.id.email);

            emailTextView.setText(email);

            nameTextView.setText(String.format("%s %s", first_name, last_name));

            dialog.findViewById(R.id.bt_cancel).setOnClickListener(v -> dialog.dismiss());

            dialog.findViewById(R.id.bt_submit).setOnClickListener(v -> {

                  String content = et_post.getText().toString().trim();
                  String titleText = title.getText().toString().trim();


                  if (titleText.isEmpty()){
                        title.setError("Title field is required!");
                  }

                  if (content.isEmpty()){
                        et_post.setError("Content field is required!");
                  }

                  
                  if (!content.isEmpty() && !titleText.isEmpty()){
//                        Toast.makeText(getContext(), "Successfully submitted!", Toast.LENGTH_SHORT).show();

                        List<String> tags = new ArrayList<>();
                        tags.add("tag");

                        createThread(titleText, content, first_name, email, tags);

                        dialog.dismiss();

                        showProgressDialog();

                  }
                  
            });

            dialog.show();
            dialog.getWindow().setAttributes(lp);
      }

      private void createThread(String title, String content, String firstName, String email, List<String> tags){
            mAgroWebAPI.createThread(title, content, firstName, email, tags)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JsonElement>() {
                          @Override
                          public void onSubscribe(Disposable d) {

                          }

                          @Override
                          public void onNext(JsonElement jsonElement) {

                          }

                          @Override
                          public void onError(Throwable e) {
                                Log.e(TAG, "onError:  Create Thread " + e.getMessage() );
//                                Toast.makeText(getContext(),  e.getMessage(), Toast.LENGTH_SHORT).show();
                                EventBus.getDefault().post(new ThreadCreatedEvent());
                                progressDialog.dismiss();

                          }

                          @Override
                          public void onComplete() {
                                EventBus.getDefault().post(new ThreadCreatedEvent());
                                progressDialog.dismiss();

                          }
                    });
      }

      private void showProgressDialog() {
            progressDialog = new Dialog(getContext());
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            progressDialog.setContentView(R.layout.dialog_forum_progress);
            progressDialog.setCancelable(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(progressDialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            progressDialog.show();
            progressDialog.getWindow().setAttributes(lp);
      }

}
