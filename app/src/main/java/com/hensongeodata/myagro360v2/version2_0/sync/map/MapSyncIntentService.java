package com.hensongeodata.myagro360v2.version2_0.sync.map;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

import org.json.JSONException;

public class MapSyncIntentService extends IntentService {

      public MapSyncIntentService() {
            super("MapSyncIntentService");
      }

      @Override
      protected void onHandleIntent(@Nullable Intent intent) {
            try {
                  MapSyncTask.syncAllMaps(this);
            } catch (JSONException e) {
                  e.printStackTrace();
            }
      }
}
