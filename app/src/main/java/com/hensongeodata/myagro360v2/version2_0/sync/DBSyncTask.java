package com.hensongeodata.myagro360v2.version2_0.sync;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.response.CropResponse;
import com.hensongeodata.myagro360v2.api.response.KnowledgeBaseResponse;
import com.hensongeodata.myagro360v2.api.response.MapResponse;
import com.hensongeodata.myagro360v2.api.response.MemberResponse;
import com.hensongeodata.myagro360v2.api.response.UserMapResponse;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.KnowledgeBase;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Model.Crop;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;
import com.hensongeodata.myagro360v2.version2_0.Model.Member;
import com.hensongeodata.myagro360v2.version2_0.Model.POISModel;
import com.hensongeodata.myagro360v2.version2_0.Model.Point;
import com.hensongeodata.myagro360v2.version2_0.Model.ScanModel;
import com.hensongeodata.myagro360v2.version2_0.Model.ScoutModel;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DBSyncTask {

      private static final String TAG = DBSyncTask.class.getSimpleName();
      private static MapDatabase mapDatabase;
      private static final long[] DEFAULT_VIBRATION_PATTERN =  {0, 250, 250, 250};
      protected static MyApplication myApplication;
      private static AgroWebAPI AGRO_WEB_API;

      public static final int PLOTS_NOTIFICATION_ID = 1010;
      public static final int MAPS_NOTIFICATION_ID = 1012;
      public static final int MEMBERS_NOTIFICATION_ID = 1013;
      public static final int UPDATE_PLOTS_NOTIFICATION_ID = 1014;
      public static final int KB_NOTIFICATION_ID = 1015;
//      private static int SCANS_NOTIFICATION_ID;
      private static int SCANS_NOTIFICATION_ID = 1030;


      synchronized public static  void syncDB(Context context) throws JSONException {
            myApplication=new MyApplication();
            mapDatabase = MapDatabase.getInstance(context);

//            syncFarms(context);
            SyncAllMaps(context);
//            SyncAllMembers(context);
//            SyncAllPlots(context);
            SyncKnowledgebase(context);
//            SyncScans(context);
            SyncScouts(context);
            Log.i(TAG, "run:  Sync Scouts Started");
      }


      public static void showNotification(int notificationId, String channelId,
                                          String channelName, Context context, String body, NotificationCompat.Style notificationStyle, Intent intent) {

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            int importance = NotificationManager.IMPORTANCE_LOW;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setContentText(body)
                    .setStyle(notificationStyle)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setNumber(1)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                    .setAutoCancel(true);

            if (intent != null){
                  TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                  stackBuilder.addNextIntent(intent);
                  PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                          0,
                          PendingIntent.FLAG_UPDATE_CURRENT
                  );
                  mBuilder.setContentIntent(resultPendingIntent);
            }

            notificationManager.notify(notificationId, mBuilder.build());
      }

      public static void progressNotification(int notificationId, String channelId,
                                          String channelName, Context context) {

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            int importance = NotificationManager.IMPORTANCE_HIGH;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.drawable.download)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setContentText("Download in progress");

            notificationManager.notify(notificationId, mBuilder.build());
      }

//      private static void SyncAllPlots(Context context){
//
//            String channelName = "synPcChannel";
//            String channelId = "DBSyncChannelId";
//
//            AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);
//
//            String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
//            String orgId   = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);
//
////            if (myApplication.hasNetworkConnection(context)){
//
//                  if (mapDatabase.plotDaoAccess().plotCountByOrgId(orgId) == 0){
//                        AppExecutors.getInstance().getDiskIO().execute(() -> mapDatabase.plotDaoAccess().deleteSyncedPlots());
//                  }
//
//                  NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//
//                  int importance = NotificationManager.IMPORTANCE_LOW;
//
//                  if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//                        NotificationChannel mChannel = new NotificationChannel(
//                                channelId, channelName, importance);
//                        notificationManager.createNotificationChannel(mChannel);
//                  }
//
//                  NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
//                          .setSmallIcon(R.drawable.download)
//                          .setContentTitle("Downloading your plots..")
//                          .setContentText("Download in progress");
//
//
//                  AGRO_WEB_API.getPlotsCall(orgId, userId).enqueue(new Callback<PlotResponse>() {
//                              @Override
//                              public void onResponse(Call<PlotResponse> call, Response<PlotResponse> response) {
//
//                                    int totalLengthOfPlots = response.body().getPlots().size();
//                                    int count = 0;
//                                    final int[] dbPlotCounts = {0};
//
//                                    try {
//
//                                          int progressCounter = 0;
//
//                                          for (FarmPlot plot : response.body().getPlots()) {
//
//                                                progressCounter++;
//
//                                                FarmPlot farmPlotModel = new FarmPlot();
//
//                                                int finalProgressCounter = progressCounter;
//                                                AppExecutors.getInstance().getDiskIO().execute(() -> {
//                                                      try {
//
//                                                            farmPlotModel.setName(plot.getName());
//
//                                                            Log.i(TAG, "onResponse:  Sync Plot" + plot.getName());
//                                                            Farm farm = mapDatabase.farmDaoAccess().fetchFarmByFarmId(plot.getFarmId());
//                                                            Log.i(TAG, "Count:  Farm " + farm);
//                                                            if (farm != null) {
//                                                                  farmPlotModel.setPrimaryFarmId(farm.getFarmId());
//                                                            }
//
//                                                            farmPlotModel.setId(plot.getId());
//                                                            farmPlotModel.setFarmId(plot.getFarmId());
//                                                            farmPlotModel.setFarmName(plot.getFarmName());
//                                                            farmPlotModel.setCropName(plot.getCropName());
//                                                            farmPlotModel.setCropId(plot.getCropId());
//                                                            farmPlotModel.setMapId(plot.getMapId());
//                                                            farmPlotModel.setUserId(userId);
//                                                            farmPlotModel.setOrgId(orgId);
//                                                            farmPlotModel.setSynced(true);
//                                                            farmPlotModel.setCreatedAt(System.currentTimeMillis());
//                                                            farmPlotModel.setUpdatedAt(System.currentTimeMillis());
//
//                                                            mapDatabase.plotDaoAccess().insert(farmPlotModel);
//                                                            dbPlotCounts[0] = mapDatabase.plotDaoAccess().plotCountByOrgId(orgId);
//
//                                                            // Sets the progress indicator to a max value, the current completion percentage and "determinate" state
//                                                            mBuilder.setProgress(totalLengthOfPlots, finalProgressCounter, false);
//                                                            // Displays the progress bar for the first time.
//
//                                                            notificationManager.notify(PLOTS_NOTIFICATION_ID, mBuilder.build());
//
//                                                            if (dbPlotCounts[0] == totalLengthOfPlots) {
////                                                                  NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Plots DB Syncing is complete!"));
////                                                                  showNotification(PLOTS_NOTIFICATION_ID, channelId, channelName, context, "Plots DB Syncing is complete!", notificationStyle, null);
//                                                                  // When the loop is finished, updates the notification
//                                                                  mBuilder.setContentText("Download completed")
//                                                                          // Removes the progress bar
//                                                                          .setProgress(0,0,false);
//                                                                  notificationManager.notify(PLOTS_NOTIFICATION_ID, mBuilder.build());
//                                                            }
//
//                                                      } catch (Exception e) {
//
//                                                            Log.e(TAG, "run: SyncPlots loop  " + e.getMessage());
//                                                      }
//                                                });
//
//                                                count++;
//
//                                          }
//
//                                    } catch (Exception e) {
//
//                                          NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Plots DB Syncing failed.."));
//                                          showNotification(PLOTS_NOTIFICATION_ID, channelId, channelName, context, "Syncing failed!", notificationStyle, null);
//                                    }
//
//                                    AppExecutors.getInstance().getDiskIO().execute(() -> {
//                                          AppNotification appNotification = new AppNotification();
//                                          appNotification.setName(AppNotification.NOTIFICATION_PLOTS);
//                                          appNotification.setMessage("Plots have been downloaded to your device");
//                                          appNotification.setOwner(AppNotification.NOTIFICATION_PLOTS);
//                                          appNotification.setDateCreated(System.currentTimeMillis());
//
//                                          mapDatabase.notificationDaoAccess().insert(appNotification);
//                                    });
//                              }
//
//                              @Override
//                              public void onFailure(Call<PlotResponse> call, Throwable t) {
//                              }
//                        });
//
//            }



      public static void SyncAllMaps(Context context){

            String channelName = "syncMapsChannel";
            String channelId = "syncMapUpdatesChannelId";

            AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
            String orgId   = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

            Log.i(TAG, "onResponse:  Sync Map" + userId);

            AppExecutors.getInstance().getDiskIO().execute(() -> {

                  if (myApplication.hasNetworkConnection(context)){

                        try{
                              if (  mapDatabase.daoAccess().mapsByOrgId(orgId) != 0){
                                    AppExecutors.getInstance().getDiskIO().execute(() -> mapDatabase.daoAccess().deleteSyncedMaps());
                              }

                              NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                              NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                                      .setSmallIcon(R.drawable.download)
                                      .setContentTitle("Downloading your maps..")
                                      .setContentText("Download in progress");


                              AGRO_WEB_API.getUserMap(userId, orgId).enqueue(new Callback<UserMapResponse>() {
                                    @Override
                                    public void onResponse(Call<UserMapResponse> call, Response<UserMapResponse> response) {

                                          try {

                                                int progressCounter = 0;

                                                for (MapResponse mapResponse : response.body().getMaps()) {

                                                      progressCounter++;

                                                      MapModel mapModel = new MapModel();
                                                      mapModel.setId(mapResponse.getMappingCode());
                                                      mapModel.setName(mapResponse.getName());
                                                      mapModel.setType(mapResponse.getType());
                                                      mapModel.setSynced(true);
                                                      mapModel.setOrgId(orgId);
                                                      mapModel.setUserId(userId);

                                                      for (Point point : mapResponse.getPoints()) {
                                                            POISModel poisModel = new POISModel();
                                                            poisModel.setMapId(mapResponse.getMappingCode());
                                                            String coordinatesInfo = point.getLatitude() + "#" + point.getLongitude() + "#" + point.getAccuracy();
                                                            poisModel.setInfo(coordinatesInfo);

                                                            AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                                                  @Override
                                                                  public void run() {
                                                                        mapDatabase.poisDaoAccess().insert(poisModel);
                                                                  }
                                                            });
                                                      }

                                                      int finalProgressCounter = progressCounter;
                                                      AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                  try {

                                                                        List<POISModel> poisModelList = mapDatabase.poisDaoAccess().fetchPOISByMapId(mapModel.getId());
                                                                        ArrayList<String> strPois = new ArrayList<>();

                                                                        for (POISModel poisModel : poisModelList) {
                                                                              if (poisModel != null) {
                                                                                    String message = poisModel.getInfo();
                                                                                    if (message != null && !message.trim().isEmpty())
                                                                                          strPois.add(message);

                                                                                    Log.d(TAG, "run: show Perimeter:  " + message);
                                                                              }
                                                                        }

                                                                        ArrayList<POI> pois = CreateForm.getInstance(context).buildPOI_Mapping(strPois);
                                                                        int area = MyApplication.getArea(pois);

                                                                        long perimeter = MyApplication.getPerimeter(pois);
                                                                        double inKilometers = metersToKilo(perimeter);
                                                                        double acreage = metersToAcres(perimeter);

                                                                        mapModel.setSizeAc(acreage);
                                                                        mapModel.setSizeM(perimeter);
                                                                        mapModel.setSizeKm(inKilometers);

                                                                        AppExecutors.getInstance().getDiskIO().execute(() -> {
                                                                              mapDatabase.daoAccess().insert(mapModel);
                                                                        });

                                                                        Log.i(TAG, "onResponse:  Sync Map" + mapResponse.getName());

                                                                        mBuilder.setProgress(response.body().getMaps().size(), finalProgressCounter, false);
                                                                        // Displays the progress bar for the first time.

                                                                        notificationManager.notify(MAPS_NOTIFICATION_ID, mBuilder.build());

                                                                  } catch (Exception e) {

                                                                        Log.e(TAG, "run: SyncMaps loop  " + e.getMessage());
                                                                  }
                                                            }
                                                      });
                                                }

//                                                NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Downloading your maps to device.."));
//                                                showNotification(MAPS_NOTIFICATION_ID, channelId, channelName, context, "Downloading your maps to device..", notificationStyle, null);

//                                                AppNotification appNotification = new AppNotification();
//                                                appNotification.setName(AppNotification.NOTIFICATION_PLOTS);
//                                                appNotification.setMessage("Your plots have been downloaded to your device");
//                                                appNotification.setOwner(AppNotification.NOTIFICATION_PLOTS);
//                                                appNotification.setDateCreated(System.currentTimeMillis());
//
//                                                mapDatabase.notificationDaoAccess().insert(appNotification);

                                          } catch (Exception e) {

                                                NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Downloading failed!"));
                                                showNotification(MAPS_NOTIFICATION_ID, channelId, channelName, context, "Downloading failed!", notificationStyle, null);

                                                Log.e(TAG, "Map Syncing Failed:  " + e.getMessage());
                                          }

                                          AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                                @Override
                                                public void run() {
//                                                      AppNotification appNotification = new AppNotification();
//                                                      appNotification.setName(AppNotification.NOTIFICATION_MAPS);
//                                                      appNotification.setMessage("Your maps have been downloaded to your device");
//                                                      appNotification.setOwner(AppNotification.NOTIFICATION_MAPS);
//                                                      appNotification.setDateCreated(System.currentTimeMillis());
//
//                                                      mapDatabase.notificationDaoAccess().insert(appNotification);
                                                }
                                          });

                                    }

                                    @Override
                                    public void onFailure(Call<UserMapResponse> call, Throwable t) {
                                          Log.e(TAG, "onFailure:  " + t.getMessage());
                                    }
                              });
                        }catch (Exception e){
                              Log.e(TAG, "SyncAllMaps Error :  "  + e.getMessage() );
                        }
                  }

            });

      }

      public static void syncCrops(Context context){
            mapDatabase  = MapDatabase.getInstance(context);
            AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            AGRO_WEB_API.fetchCropsCall().enqueue(new Callback<CropResponse>() {
                  @Override
                  public void onResponse(Call<CropResponse> call, Response<CropResponse> response) {
                        if (response.isSuccessful()){
                              AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                    @Override
                                    public void run() {

                                          try {

                                                if (response.body() != null && response.body().getCrops().size() != mapDatabase.cropDaoAccess().getCropsCount()) {

                                                      mapDatabase.cropDaoAccess().deleteCrops();

                                                      for (Crop crop : response.body().getCrops()) {
                                                            mapDatabase.cropDaoAccess().insert(crop);
                                                      }
                                                }

                                          }catch (Exception e){

                                                Log.e(TAG, "Sync Crops Error  " + e.getMessage()    );
                                          }

                                    }
                              });
                        }

//                        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
//                              @Override
//                              public void run() {
//                                    AppNotification appNotification = new AppNotification();
//                                    appNotification.setName(AppNotification.NOTIFICATION_CROPS);
//                                    appNotification.setMessage("Crops have been downloaded to your device");
//                                    appNotification.setOwner(AppNotification.NOTIFICATION_CROPS);
//                                    appNotification.setDateCreated(System.currentTimeMillis());
//
//                                    mapDatabase.notificationDaoAccess().insert(appNotification);
//                              }
//                        });

                  }

                  @Override
                  public void onFailure(Call<CropResponse> call, Throwable t) {}
            });

      }

      public static void UpdatePlotSizes( Context context){

            MapDatabase mapDatabase = MapDatabase.getInstance(context);

            String channelName = "UpdatePlotSizesSyncChannel";
            String channelId = "UpdatePlotSizesChannelId";

//            String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);


            String userId = "";

            if (SharePrefManager.isSecondaryUserAvailable(context)){
                  userId = SharePrefManager.getSecondaryUserPref(context);
            }else {
                  userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
            }

            String orgId   = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

//            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
//                    .setSmallIcon(R.drawable.download)
//                    .setContentTitle("Updating plot sizes..")
//                    .setContentText("Updates in progress");

            if (mapDatabase.plotDaoAccess().plotCountByOrgId(orgId) != 0) {
                  mapDatabase.plotDaoAccess().deletePlots();
            }

            String finalUserId = userId;
            AppExecutors.getInstance().getDiskIO().execute(() -> {

                        try {

                              List<MapModel> mapModelList = mapDatabase.daoAccess().fetchAllMapsList(finalUserId, orgId);

                              int progressCounter = 0;

                              for (MapModel mapModel : mapModelList){

                                    progressCounter++;

                                    List<FarmPlot> plots =  mapDatabase.plotDaoAccess().fetchPlotsByMapId(mapModel.getId());

                                    for (FarmPlot farmPlot : plots){
                                          farmPlot.setSizeAc(mapModel.getSizeAc());
                                          farmPlot.setSizeKm(mapModel.getSizeKm());
                                          farmPlot.setSizeM(mapModel.getSizeM());
                                          mapDatabase.plotDaoAccess().updatePlot(farmPlot);
                                    }

//                              mBuilder.setProgress(mapModelList.size(), progressCounter, false);
                                    // Displays the progress bar for the first time.

//                              notificationManager.notify(UPDATE_PLOTS_NOTIFICATION_ID, mBuilder.build());

                                    Log.d(TAG, "run: show Perimeter:  km " + mapModel.getSizeKm() + "  acreage " + mapModel.getSizeAc());
                              }

                              NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Updating plot sizes..."));
                              showNotification(UPDATE_PLOTS_NOTIFICATION_ID, channelId, channelName,context,  "Updating plot sizes... ", notificationStyle,null );

                        }catch (Exception e){
                              Log.e(TAG, "UpdatePlotSizes Error " + e.getMessage() );
                        }
                  });

      }

      public static void SyncAllMembers(Context context){

            String channelName = "synPcChannel";
            String channelId = "DBSyncChannelId";

            mapDatabase = MapDatabase.getInstance(context);
            AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
            String orgId   = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

                  AppExecutors.getInstance().getDiskIO().execute(() -> {

                        if (mapDatabase.memberDaoAccess().memberCount() != 0) {
                              mapDatabase.memberDaoAccess().deleteMembers();
                        }

                        AGRO_WEB_API.getMembers(userId, orgId).enqueue(new Callback<MemberResponse>() {
                              @Override
                              public void onResponse(Call<MemberResponse> call, Response<MemberResponse> memberResponse) {
                                    if (memberResponse.isSuccessful()) {

                                          try{

                                                if (memberResponse.body() != null) {
                                                      for (Member member : memberResponse.body().getMembers()) {
                                                            member.setOrgId(orgId);
                                                            AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                                                  @Override
                                                                  public void run() {
                                                                        mapDatabase.memberDaoAccess().insert(member);
                                                                  }
                                                            });
                                                      }
                                                }

                                          }catch (Exception e){
                                                Log.e(TAG, "onResponse:  " + e.getMessage() );
                                          }
                                    }
                              }

                              @Override
                              public void onFailure(Call<MemberResponse> call, Throwable t) {

                                    NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Member Downloading failed!"));
                                    showNotification(MEMBERS_NOTIFICATION_ID, channelId, channelName, context, "Member Downloading failed!", notificationStyle, null);
                              }
                        });
                  });

      }

      private static void SyncKnowledgebase(Context context){

            String channelName = "syncKB";
            String channelId = "DBKBSyncChannelId";

            AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.FAW_API).create(AgroWebAPI.class);

            Log.i(TAG, "SyncKnowledgebase:  entered" );

                  NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                  Log.i(TAG, "SyncKnowledgebase:  entered Network Constraint" );


                  int importance = NotificationManager.IMPORTANCE_LOW;

                  if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        NotificationChannel mChannel = new NotificationChannel(
                                channelId, channelName, importance);
                        notificationManager.createNotificationChannel(mChannel);
                  }

                  NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                          .setSmallIcon(R.drawable.download)
                          .setContentTitle("Downloading knowledge Base..")
                          .setContentText("Download in progress");


                  AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                        @Override
                        public void run() {

                              try {

                                    mapDatabase.knowledgeBaseDaoAccess().deleteKB();

                                    AGRO_WEB_API.fetchKnowledgeBaseCall("english", "app", Keys.API_KEY)
                                            .enqueue(new Callback<KnowledgeBaseResponse>() {
                                                  @Override
                                                  public void onResponse(Call<KnowledgeBaseResponse> call, Response<KnowledgeBaseResponse> response) {

                                                        Log.i(TAG, "SyncKnowledgebase:  entered API CALL RESPONSE" );

                                                        int progressCounter = 0;
//                                                Log.i(TAG, "onResponse:  KB Total " + response.body().getTopics());
                                                        if (response.body() != null) {
                                                              for (KnowledgeBase knowledgeBase : response.body().getTopics()){
                                                                    progressCounter++;
                                                                    int finalProgressCounter = progressCounter;
                                                                    AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                                                          @Override
                                                                          public void run() {
                                                                                Log.i(TAG, "SyncKnowledgebase:  entered Insert and Notifications" );

                                                                                mapDatabase.knowledgeBaseDaoAccess().insert(knowledgeBase);

                                                                                mBuilder.setProgress(response.body().getTotal(), finalProgressCounter, false);

                                                                                notificationManager.notify(KB_NOTIFICATION_ID, mBuilder.build());
                                                                          }
                                                                    });

                                                              }
                                                        }
                                                  }

                                                  @Override
                                                  public void onFailure(Call<KnowledgeBaseResponse> call, Throwable t) {

                                                        Log.i(TAG, "onResponse:  KB Error" + t.getMessage());

                                                        Log.i(TAG, "SyncKnowledgebase:  entered Error " + t.getMessage()  );


                                                  }
                                            });

                              }catch (Exception e){
                                    Log.e(TAG, "SyncKnowledgebase :  " + e.getMessage() );
                              }
                        }
                  });
            }


      public static void SyncScans(Context context){

            String channelName = "syncKB";
            String channelId = "DBKBSyncChannelId";

            AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            Log.i(TAG, "SyncKnowledgebase:  entered" );

//            if (myApplication.hasNetworkConnection(context)) {

                  NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                  Log.i(TAG, "SyncKnowledgebase:  entered Network Constraint" );


                  int importance = NotificationManager.IMPORTANCE_LOW;

                  if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        NotificationChannel mChannel = new NotificationChannel(
                                channelId, channelName, importance);
                        notificationManager.createNotificationChannel(mChannel);
                  }

                  NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                          .setSmallIcon(R.drawable.download)
                          .setContentTitle("Downloading Scans...")
                          .setContentText("Download in progress");


                  AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                        @Override
                        public void run() {

//                              mapDatabase = MapDatabase.getInstance(context);

                              String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
                              String orgId   = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

                              if (mapDatabase.scanDaoAccess().scanCount(orgId) != 0) {
                                    Log.i(TAG, "SyncScans:  Scans COUNT" );
                                    mapDatabase.scanDaoAccess().deleteScans(orgId);
                              }

                              AGRO_WEB_API.getUserScans(orgId, userId)
                                      .enqueue(new Callback<JsonElement>() {
                                            @SuppressLint("NewApi")
                                            @Override
                                            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                                                  int progressCounter = 0;

                                                  if (response.body() != null) {

                                                        JsonArray bodyJsonArray =  response.body().getAsJsonObject().
                                                                getAsJsonArray("scans");

//
                                                        for( int i = 0; i <  bodyJsonArray.size(); i++){

                                                              progressCounter++;
                                                              int finalProgressCounter = progressCounter;

                                                             String name =  bodyJsonArray.get(i).getAsJsonObject().get("name").getAsString();
                                                             String image =  bodyJsonArray.get(i).getAsJsonObject().get("image").getAsString();
                                                             String scoutId =  bodyJsonArray.get(i).getAsJsonObject().get("scout_id").getAsString();
                                                              String strippedName = "";

                                                              ScanModel scanModel = new ScanModel();

                                                              if (name.toLowerCase().contains("--label not found--")){
                                                                   String firstStrip = name.replace("--","");
                                                                   String secondStrip = firstStrip.replace("[", "").replace("]","").replace("\"", "");
                                                                    strippedName = secondStrip.replaceAll(", $", "");
//                                                                    string.Replace("\"", "")
                                                                    scanModel.setPestFound(false);
                                                              }else {
                                                                   String firstStrip = name.replace("[","");
                                                                   String secondStrip = firstStrip.replace("]", "").replace("\"", "");
                                                                   strippedName = secondStrip.replaceAll(", $", "");
                                                                    scanModel.setPestFound(true);
                                                              }

                                                              Log.i(TAG, "onResponse:  Scan Response  Name : " + name);
                                                              Log.i(TAG, "onResponse:  Scan Response  Name : " + strippedName);
                                                              Log.i(TAG, "onResponse:  Scan Response  Name : " + image);
//
                                                              scanModel.setMessage(strippedName);
                                                              scanModel.setPestIdentified(strippedName);
                                                              scanModel.setImagePath(image);
                                                              scanModel.setSynced(true);
                                                              scanModel.setOrgId(orgId);
                                                              scanModel.setUserId(userId);
                                                              scanModel.setScoutId(Long.valueOf(scoutId));
                                                              scanModel.setItemId((scoutId));
                                                              scanModel.setDateCreated(System.currentTimeMillis());
                                                              scanModel.setStatus(ScanModel.STATE_PROCESSED);

                                                              AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                          mapDatabase.scanDaoAccess().insert(scanModel);
                                                                          mBuilder.setProgress(bodyJsonArray.size(), finalProgressCounter, false);

                                                                          notificationManager.notify(SCANS_NOTIFICATION_ID, mBuilder.build());
                                                                    }
                                                              });
                                                        }
                                                  }
                                            }

                                            @Override
                                            public void onFailure(Call<JsonElement> call, Throwable t) {
                                                  Log.i(TAG, "Scan Response:  Error" + t.getMessage());
                                            }
                                      });
                        }
                  });
//            }

      }

      public static void SyncScouts(Context context){

            String channelName = "syncScouts";
            String channelId = "DBScoutsSyncChannelId";

            AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            Log.i(TAG, "Sync Scouts :  entered" );

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            int importance = NotificationManager.IMPORTANCE_LOW;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.drawable.download)
                    .setContentTitle("Downloading Scans...")
                    .setContentText("Download in progress");


            AppExecutors.getInstance().getDiskIO().execute(() -> {

                  String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
                  String orgId   = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

                  try{

                        if (mapDatabase.scoutDaoAccess().scoutCount(orgId) != 0) {
                              Log.i(TAG, "SyncScouts:  Scouts COUNT" );
                              mapDatabase.scoutDaoAccess().deleteScouts(orgId);
                        }

                        if (mapDatabase.scanDaoAccess().scoutCount(orgId) != 0) {
                              Log.i(TAG, "SyncScouts:  Scouts COUNT" );
                              mapDatabase.scanDaoAccess().deleteScans(orgId);
                        }

                        AGRO_WEB_API.getUserScouts(orgId, userId)
                                .enqueue(new Callback<JsonElement>() {
                                      @SuppressLint("NewApi")
                                      @Override
                                      public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                                            int progressCounter = 0;

                                            if (response.body() != null) {

                                                  Log.i(TAG, "Get User Scouts " + response.body().toString());

                                                  if( response.body().isJsonObject()){

                                                        JsonArray bodyJsonArray =  response.body().getAsJsonObject().
                                                                getAsJsonArray("scouts");

                                                        for( int i = 0; i <  bodyJsonArray.size(); i++){

                                                              progressCounter++;
                                                              int finalProgressCounter = progressCounter;

                                                              String ScoutCode =  bodyJsonArray.get(i).getAsJsonObject().get("ScoutCode").getAsString();
                                                              String ScoutID =  bodyJsonArray.get(i).getAsJsonObject().get("ScoutID").getAsString();
                                                              JsonArray scansJsonArray =  bodyJsonArray.get(i).getAsJsonObject().getAsJsonArray("scans");

                                                              ScoutModel scoutModel = new ScoutModel();
                                                              scoutModel.setOrgId(orgId);
                                                              scoutModel.setUserId(userId);
//                                                        scoutModel.setPlotId(plotId);
                                                              scoutModel.setBatchId(ScoutCode);
                                                              Log.i(TAG, "run:  Scout Model " + ScoutCode);
                                                              scoutModel.setDateCreated(System.currentTimeMillis());

                                                              AppExecutors.getInstance().getDiskIO().execute(() -> {
                                                                    mapDatabase.scoutDaoAccess().insert(scoutModel);

//                                                                    if (bodyJsonArray.size() == mapDatabase.scoutDaoAccess().countUserMaps(userId)){
//                                                                          EventBus.getDefault().post(new MapsDownloadCompleted(false));
//                                                                    }
//                                                                    mBuilder.setProgress(bodyJsonArray.size(), finalProgressCounter, false);
//                                                                    notificationManager.notify(SCANS_NOTIFICATION_ID, mBuilder.build());
                                                              });

                                                              for(int j =0;  j< scansJsonArray.size(); j++){

                                                                    String name =  scansJsonArray.get(j).getAsJsonObject().get("Labels").getAsString();
//                                                        String image =  scansJsonArray.get(j).getAsJsonObject().get("image").getAsString();
//                                                        String scoutId =  scansJsonArray.get(j).getAsJsonObject().get("scout_id").getAsString();
                                                                    String strippedName = "";

                                                                    ScanModel scanModel = new ScanModel();

                                                                    if (name.toLowerCase().contains("--label not found--")){
                                                                          String firstStrip = name.replace("--","");
                                                                          String secondStrip = firstStrip.replace("[", "").replace("]","").replace("\"", "");
                                                                          strippedName = secondStrip.replaceAll(", $", "");
//                                                                    string.Replace("\"", "")
                                                                          scanModel.setPestFound(false);

                                                                    }else {
                                                                          String firstStrip = name.replace("[","");
                                                                          String secondStrip = firstStrip.replace("]", "").replace("\"", "");
                                                                          strippedName = secondStrip.replaceAll(", $", "");
                                                                          scanModel.setPestFound(true);
                                                                    }

                                                                    Log.i(TAG, "onResponse:  Scan Response  Name : " + name);
                                                                    Log.i(TAG, "onResponse:  Scan Response  Name : " + strippedName);

                                                                    scanModel.setMessage(strippedName);
                                                                    scanModel.setPestIdentified(strippedName);
//                                                        scanModel.setImagePath(image);
                                                                    scanModel.setSynced(true);
                                                                    scanModel.setOrgId(orgId);
                                                                    scanModel.setUserId(userId);
                                                                    scanModel.setScoutId(Long.valueOf(ScoutCode));
                                                                    scanModel.setItemId((ScoutCode));
                                                                    scanModel.setDateCreated(System.currentTimeMillis());
                                                                    scanModel.setStatus(ScanModel.STATE_PROCESSED);

                                                                    Log.i(TAG, "onResponse:  Scan Response  scout code : " + ScoutCode);
                                                                    Log.i(TAG, "onResponse:  Scan Response  scout id : " + ScoutID);

                                                                    AppExecutors.getInstance().getDiskIO().execute(() -> {
                                                                          mapDatabase.scanDaoAccess().insert(scanModel);
                                                                    });
                                                              }


                                                        }

                                                  }
                                            }
                                      }

                                      @Override
                                      public void onFailure(Call<JsonElement> call, Throwable t) {
                                            Log.i(TAG, "Scan Response:  Error" + t.getMessage());
                                      }
                                });

                  }catch (Exception e){
                        Log.e(TAG, "SyncScouts Error : " + e.getMessage() );
                  }
            });
      }

      public static void UpdateScouts(Context context){

            String channelName = "syncKB";
            String channelId = "DBKBSyncChannelId";

            AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            Log.i(TAG, "SyncKnowledgebase:  entered" );

//            if (myApplication.hasNetworkConnection(context)) {

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            Log.i(TAG, "SyncKnowledgebase:  entered Network Constraint" );


            int importance = NotificationManager.IMPORTANCE_LOW;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.drawable.download)
                    .setContentTitle("Downloading Scans...")
                    .setContentText("Download in progress");


            AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                  @Override
                  public void run() {

//                              mapDatabase = MapDatabase.getInstance(context);

                        String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
                        String orgId   = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

                        List<ScoutModel> scoutModelList = mapDatabase.scoutDaoAccess().fetchAllScoutList(orgId);
                        List<ScanModel> scanModelList = mapDatabase.scanDaoAccess().fetchAllScanList(orgId);

                              for (ScanModel scanModel : scanModelList){

                              }

                  }
            });
//            }

      }


      private static double metersToAcres(long meterSquared){
            return Math.round((meterSquared*0.00024711) * 1000.0) / 1000.0;
      }

      private static double metersToKilo(long metersSquared){
            return metersSquared*0.001;
      }

      private static StringBuilder generateScanReportText(ScanModel scanModel){

            StringBuilder stringBuilder = new StringBuilder();

            if (scanModel != null ){
                  long id = scanModel.getId();
                  int status = scanModel.getStatus();
                  String message = scanModel.getMessage();
                  long scoutId = scanModel.getScoutId();
                  String type = scanModel.getType();
                  int plantNumber = scanModel.getPlantNumber();
                  int plantPoint = scanModel.getPointNumber();
                  String pestIdentified = scanModel.getPestIdentified();
                  String spacing = "     ";

                  stringBuilder.append(id);
                  stringBuilder.append(spacing);
                  stringBuilder.append(status);
                  stringBuilder.append(spacing);
                  stringBuilder.append(message);
                  stringBuilder.append(spacing);
                  stringBuilder.append(scoutId);
                  stringBuilder.append(spacing);
                  stringBuilder.append(type);
                  stringBuilder.append(spacing);
                  stringBuilder.append(plantNumber);
                  stringBuilder.append(spacing);
                  stringBuilder.append(plantPoint);
                  stringBuilder.append(spacing);
                  stringBuilder.append(pestIdentified);
            }

            return stringBuilder;

      }

}
