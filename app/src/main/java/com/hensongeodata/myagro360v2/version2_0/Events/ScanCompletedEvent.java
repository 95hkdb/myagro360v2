package com.hensongeodata.myagro360v2.version2_0.Events;

public  class ScanCompletedEvent {

      private long id;

      public ScanCompletedEvent(long id) {
            this.id = id;
      }

      public long getId() {
            return id;
      }
}
