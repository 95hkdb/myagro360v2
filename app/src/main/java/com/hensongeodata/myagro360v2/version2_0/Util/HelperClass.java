package com.hensongeodata.myagro360v2.version2_0.Util;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.pdf.PdfDocument;
import android.net.http.SslError;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Keys;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class HelperClass {
      private static final String TAG = HelperClass.class.getSimpleName();

//      private static final int  = 58;

      public static final  int  ALL_PLOTS_SORT = 0;
      public static final int BY_CROP_PLOTS_SORT = 1;
      public static final int BY_NAME_A_Z_PLOTS_SORT_ = 2;
      public static final int BY_NAME_Z_A_PLOTS_SORT_ = 3;
      public static final int BY_SIZE_HIGH_LOW_PLOTS_SORT_ = 4;
      public static final int BY_SIZE_LOW_HIGH_PLOTS_SORT_ = 5;
      public static final int BY_SEARCH_PLOTS_SORT_ = 6;
      public static final int BY_DATE_PLOTS_SORT_ = 7;
      public static final int BY_DISTINCT_PLOTS_SORT_ = 8;
      public static final int BY_LAST_FIRST_PLOTS_SORT_ = 9;


      public static void LoadWebView(WebView webView, String sectionId,
                                     ImageView noPreviewAvailableImageView, Context context){

            MyApplication myApplication = new MyApplication();

            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setDisplayZoomControls(false);

            if (myApplication.hasNetworkConnection(context)){
//                  webView.loadUrl(Keys.AGRO_BASE+"faw-api/get-map/?mapping_id="+sectionId);
//                  webView.loadUrl(Keys.FAW_API+"faw-api/get-map/?mapping_id="+sectionId);

                  webView.loadUrl(Keys.FAW_API+"get-map/?mapping_id="+sectionId);

                  Log.e(TAG, "Section id: " + sectionId);

                  noPreviewAvailableImageView.setVisibility(View.GONE);
                  webView.setVisibility(View.VISIBLE);
                  webView.setWebViewClient(new WebViewClient() {

                        @Override
                        public void onPageFinished(WebView view, String url) {
                              super.onPageFinished(view, url);
                        }

                        @TargetApi(Build.VERSION_CODES.LOLLIPOP)

                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                              view.loadUrl(request.getUrl().toString());
                              return false;
                        }

                        @Override
                        public void onPageStarted(WebView view, String url, Bitmap favicon) {
                              super.onPageStarted(view, url, favicon);
                        }

                        @Override
                        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                              super.onReceivedError(view, request, error);
                              Log.d(TAG,"error");
                        }

                        @Override
                        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                              super.onReceivedSslError(view, handler, error);
                              Log.d(TAG,"ssl error");
                        }

                        @Override
                        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                              super.onReceivedHttpError(view, request, errorResponse);
                              Log.d(TAG,"http error");
                        }
                  });
            }else  {
                  webView.setVisibility(View.GONE);
                  noPreviewAvailableImageView.setVisibility(View.VISIBLE);
            }
      }

      public static double metersToKilo(long metersSquared){
            return metersSquared*0.001;
      }

      public static double metersToAcres(long meterSquared){
            return Math.round((meterSquared*0.00024711) * 1000.0) / 1000.0;
      }

      public static double round(double value, int places) {
            if (places < 0) throw new IllegalArgumentException();

            BigDecimal bd = BigDecimal.valueOf(value);
            bd = bd.setScale(places, RoundingMode.HALF_UP);
            return bd.doubleValue();
      }

      public static int compareDates(long firstDateTimeInMillis, long secondDateTimeInMillis){
            Calendar firstCalendar = Calendar.getInstance();
            firstCalendar.setTimeInMillis(firstDateTimeInMillis);

            Calendar secondCalendar = Calendar.getInstance();
            secondCalendar.setTimeInMillis(secondDateTimeInMillis);

            return firstCalendar.compareTo(secondCalendar);
      }

      public static long convertDateToMilliseconds(String dateString) {

            SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm", Locale.ENGLISH);
            Date date = null;

            long dateConverted = 0;

            try {
                  date = sdf.parse(dateString.trim());
                  dateConverted =  (date.getTime());
                  Log.i(TAG, "testDate: Date - Time in milliseconds : " + dateConverted);

            } catch (ParseException e) {
                  e.printStackTrace();
            }

            return dateConverted;
      }

      public static long convertDateTimeToMilliseconds(String dateString) {

//            SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss", Locale.ENGLISH);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
            Date date = null;

            long dateConverted = 0;

            try {
                  date = sdf.parse(dateString.trim());
                  dateConverted =  (date.getTime());
                  Log.i(TAG, "testDate: Date - Time in milliseconds : " + dateConverted);

            } catch (ParseException e) {
                  e.printStackTrace();
            }

            return dateConverted;
      }

      public static long convertDateOnlyToMilliseconds(String dateString) {

//            SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss", Locale.ENGLISH);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date date = null;

            long dateConverted = 0;

            try {
                  date = sdf.parse(dateString.trim());
                  dateConverted =  (date.getTime());
                  Log.i(TAG, "testDate: Date - Time in milliseconds : " + dateConverted);

            } catch (ParseException e) {
                  e.printStackTrace();
            }

            return dateConverted;
      }


      public static int getMonthNumber(String month){
            int i = 0;
            switch (month.trim()){
                  case "Jan":
                        i = 1;
                        return i;

                  case "Feb":
                        i = 2;
                        return i;

                  case "Mar":
                        i =  3;
                        return i;

                  case "Apr":
                        i = 4;
                        return i;

                  case "May":
                        i =  5;
                        return i;

                  case "Jun":
                        i =  6;
                        return i;

                  case "Jul":
                        i = 7;
                        return i;

                  case "Aug":
                        i = 8;
                        return i;

                  case "Sep":
                        i = 9;
                        return i;

                  case "Oct":
                        i = 10;
                        return i;

                  case "Nov":
                        i = 11;
                        return i;

                  case "Dec":
                        i = 12;
                        return i;

            }

            return i;
      }

      public static void createPdf(Context context, String text){
            // create a new document
            PdfDocument document = new PdfDocument();
            // crate a page description
            PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(1000, 600, 1).create();
            // start a page
            PdfDocument.Page page = document.startPage(pageInfo);
            Canvas canvas = page.getCanvas();
            Paint paint = new Paint();
//            paint.setColor(Color.RED);
//            canvas.drawCircle(50, 50, 30, paint);
            paint.setColor(Color.BLACK);
            canvas.drawText(text, 20, 50, paint);
            //canvas.drawt
            // finish the page
            document.finishPage(page);
// draw text on the graphics object of the page
            // Create Page 2
            pageInfo = new PdfDocument.PageInfo.Builder(300, 600, 2).create();
            page = document.startPage(pageInfo);
            canvas = page.getCanvas();
            paint = new Paint();
            paint.setColor(Color.BLUE);
            canvas.drawCircle(100, 100, 100, paint);
            document.finishPage(page);
            // write the document content
            String directory_path = Environment.getExternalStorageDirectory().getPath() + "/mypdf/";
            Log.i(TAG, "createPdf:  " + directory_path);
            File file = new File(directory_path);
            if (!file.exists()) {
                  file.mkdirs();
            }
            String targetPdf = directory_path+"test-2.pdf";
            File filePath = new File(targetPdf);
            try {
                  document.writeTo(new FileOutputStream(filePath));
//                  Toast.makeText(context, "Done", Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                  Log.e("main", "error "+e.toString());
//                  Toast.makeText(context, "Something wrong: " + e.toString(),  Toast.LENGTH_LONG).show();
            }
            // close the document
            document.close();
      }

      public static void showNotification(int notificationId, String channelId,
                                          String channelName, Context context, String body, NotificationCompat.Style notificationStyle, Intent intent) {

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  int importance = NotificationManager.IMPORTANCE_HIGH;
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setContentText(body)
                    .setStyle(notificationStyle)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setNumber(1)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                    .setAutoCancel(true);

            if (intent != null){
                  TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                  stackBuilder.addNextIntent(intent);
                  PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                          0,
                          PendingIntent.FLAG_UPDATE_CURRENT
                  );
                  mBuilder.setContentIntent(resultPendingIntent);
            }

            notificationManager.notify(notificationId, mBuilder.build());
      }

      public static void showDialogPolygon(Context context) {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            dialog.setContentView(R.layout.dialog_header_polygon);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setCancelable(true);

            ((Button) dialog.findViewById(R.id.bt_join)).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        Toast.makeText(context, "Button Join Clicked", Toast.LENGTH_SHORT).show();
                  }
            });

            ((Button) dialog.findViewById(R.id.bt_decline)).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        Toast.makeText(context, "Button Decline Clicked", Toast.LENGTH_SHORT).show();
                  }
            });


            dialog.show();
      }

      public static void setupArrayAdapter(Context context,String[] stringArray, AppCompatSpinner spinner){
            ArrayAdapter<String> array = new ArrayAdapter<>(context, R.layout.simple_spinner_item, stringArray);
            array.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(array);
      }

      public static void setupArrayAdapterWithSpinner(Context context,String[] stringArray, Spinner spinner){
            ArrayAdapter<String> array = new ArrayAdapter<>(context, R.layout.simple_spinner_item, stringArray);
            array.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(array);
      }

      public static void showBadge(Context context, BottomNavigationView
              bottomNavigationView, @IdRes int itemId, String value) {
            removeBadge(bottomNavigationView, itemId);
            BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
            View badge = LayoutInflater.from(context).inflate(R.layout.layout_badge, bottomNavigationView, false);

            TextView text = badge.findViewById(R.id.badge_text_view);
            text.setText(value);
            itemView.addView(badge);
      }

      public static void removeBadge(BottomNavigationView bottomNavigationView, @IdRes int itemId) {
            BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
            if (itemView.getChildCount() == 3) {
                  itemView.removeViewAt(2);
            }
      }

}
