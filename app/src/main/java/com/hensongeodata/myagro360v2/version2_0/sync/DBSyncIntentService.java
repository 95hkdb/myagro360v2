package com.hensongeodata.myagro360v2.version2_0.sync;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

import org.json.JSONException;

public class DBSyncIntentService extends IntentService {

      public DBSyncIntentService() {
            super("DBSyncIntentService");
      }

      @Override
      protected void onHandleIntent(@Nullable Intent intent) {
            try {
                  DBSyncTask.syncDB(this);
            } catch (JSONException e) {
                  e.printStackTrace();
            }
      }
}
