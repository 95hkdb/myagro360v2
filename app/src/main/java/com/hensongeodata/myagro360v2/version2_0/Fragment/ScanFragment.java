package com.hensongeodata.myagro360v2.version2_0.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.request.LocsmmanProAPI;
import com.hensongeodata.myagro360v2.api.response.KnowledgeBaseResponse;
import com.hensongeodata.myagro360v2.api.response.ScanResponse;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.controller.RetrofitHelper;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.KnowledgeBase;
import com.hensongeodata.myagro360v2.model.ScanFAW;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Adapter.ScanHistoryRecyclerViewAdapter;
import com.hensongeodata.myagro360v2.version2_0.Events.ScanCompletedEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.ScanDeletedEvent;
import com.hensongeodata.myagro360v2.version2_0.KnowledgeBaseDetailFragment;
import com.hensongeodata.myagro360v2.version2_0.Listeners.ScansRvListener;
import com.hensongeodata.myagro360v2.version2_0.Model.ScanModel;
import com.hensongeodata.myagro360v2.view.CameraScan;
import com.hensongeodata.myagro360v2.view.MyCamera;
import com.hensongeodata.myagro360v2.view.ScoutAutomated;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.hensongeodata.myagro360v2.controller.RetrofitHelper.createPartFromString;

public class ScanFragment extends Fragment implements ScansRvListener {

      private static final String TAG = ScanFragment.class.getSimpleName();
      private static final int REQUEST_SCANNER = 100;
      private static int SCOUT=100;
      ArrayList<String> headers;
//      ArrayList<HashMap<String,String>>items;
      private List<ScanModel> items;
      private int mode;
      private String form_id;
      protected RecyclerView recyclerView;
      protected ImageView imageView;
      private static final String SECTION_ID = "SCOUT";
      private static final String ARG_PARAM2 = "param2";
      private int section_id;
      private String mParam2;
      private TextView numberOfScansTextView;
      private TextView numberOfPestDetectedTextView;

      private FloatingActionButton fabButton;
      private LinearLayout linearLayout;
      private LinearLayout configureLayout;
      private MainViewModel mainViewModel;
      private MapDatabase mapDatabase;
      private String orgId;
      private String userId;
      private int index = 0;
      private String itemId;

      private boolean isCancelled = false;

      private OnScanFragmentListener mListener;
      private ScanHistoryRecyclerViewAdapter adapter;
      protected MyApplication myApplication;
      private ScanFAW scanFAW;
      private List<KnowledgeBase> knowledgeBaseList;
      private AgroWebAPI mAgroWebAPI;
      private String scout_id;

      public ScanFragment() {}

      public static ScanFragment newInstance(int sectionId) {
            ScanFragment fragment = new ScanFragment();
            Bundle args = new Bundle();
            args.putInt(SECTION_ID, sectionId);
            fragment.setArguments(args);
            return fragment;
      }

      public static ScanFragment newInstance(String scoutId) {
            ScanFragment fragment = new ScanFragment();
            Bundle args = new Bundle();
            args.putString("scout_id", scoutId);
            fragment.setArguments(args);
            return fragment;
      }

      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                  section_id = getArguments().getInt(SECTION_ID);
                  scout_id     =  getArguments().getString("scout_id");
            }
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_scan_basic, container, false);

            myApplication=new MyApplication();
            orgId  = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);
//            userId  = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);

            if (SharePrefManager.isSecondaryUserAvailable(getContext())){
                  userId = SharePrefManager.getSecondaryUserPref(getContext());
            }else {
                  userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
            }

            recyclerView = view.findViewById(R.id.fragment_scouting_recyclerview);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
            mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            mapDatabase      = MapDatabase.getInstance(getContext());
            mAgroWebAPI = NetworkRequest.getRetrofit(Keys.FAW_API).create(AgroWebAPI.class);

            linearLayout = view.findViewById(R.id.no_mapping_linear_layout);
            configureLayout = view.findViewById(R.id.configure_layout);
            fabButton = view.findViewById(R.id.fab_add);
            numberOfScansTextView = view.findViewById(R.id.number_of_scans);
            numberOfPestDetectedTextView = view.findViewById(R.id.pests_detected);
            Button addNewScoutButton = view.findViewById(R.id.add_new_scout_button);
            knowledgeBaseList = new ArrayList<>();

            scanFAW = new ScanFAW(getActivity());

            MyApplication.scanFAWList.add(scanFAW);
            index = MyApplication.scanFAWList.size() - 1;

            fabButton.setOnClickListener(v -> openScanner());
            addNewScoutButton.setOnClickListener(v->openScanner());

            getAllScansFromViewModel();

            loadKnowledgeBaseList();

//            getKnowledgeBaseFromAPI();

            return view;
      }

      public void onMoreIconClicked(long id) {
            if (mListener != null) {
                  mListener.onScanFragmentMoreIconClicked(id);
            }
      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof OnScanFragmentListener) {
                  mListener = (OnScanFragmentListener) context;
            } else {
                  throw new RuntimeException(context.toString()
                          + " must implement OnPlotActivitiesFragmentListener");
            }
      }

      @Override
      public void onDetach() {
            super.onDetach();
            mListener = null;
      }

      @Override
      public void moreClicked(long id) {
            onMoreIconClicked(id);
//            Toast.makeText(getContext(), " More Clicked", Toast.LENGTH_SHORT).show();
      }

      @Override
      public void onReadButtonClicked(String name) {

            String shortenedResults = "";

//            if (!name.startsWith("Possible pest identified-")){
            if (!name.trim().startsWith("Possible")){
                  shortenedResults = name.substring(0,10);
            }else {
                  shortenedResults = name.trim().substring(25,32);
            }

            KnowledgeBase knowledgeBase = searchKB(shortenedResults);

            Toast.makeText(getActivity(), shortenedResults, Toast.LENGTH_SHORT).show();
//            Toast.makeText(getActivity(), name, Toast.LENGTH_SHORT).show();

            try {

//                  KnowledgeBase knowledgeBasemodel = new KnowledgeBase();
//                  knowledgeBasemodel.setTitle("Thrips Adult");
//                  knowledgeBasemodel.setContent("Thrips (order Thysanoptera) are minute (most are 1 mm long or less), slender insects with fringed wings and " +
//                          "unique asymmetrical mouthparts. Different thrips species feed mostly on plants by puncturing and sucking up the contents, although a few are predators. " +
//                          "Approximately 6,000 species have been described. They fly only weakly and their feathery wings are unsuitable for conventional flight; instead, " +
//                          "thrips exploit an unusual mechanism, clap " +
//                          "and fling, to create lift using an unsteady circulation pattern with transient vortices near the wings.");
//                  knowledgeBasemodel.setMediaUrl("https://upload.wikimedia.org/wikipedia/commons/f/f5/Frankadult.jpg");
//                  knowledgeBasemodel.setUrl("https://upload.wikimedia.org/wikipedia/commons/f/f5/Frankadult.jpg");
//                  knowledgeBasemodel.setLanguage("english");

                  KnowledgeBaseDetailFragment fragment = new KnowledgeBaseDetailFragment();
                  fragment.setData(knowledgeBase);
//                  fragment.setData(knowledgeBase);
                  fragment.show(getChildFragmentManager(), fragment.getTag());
            }catch(Exception e){
                  Toast.makeText(getActivity(), "Sorry, we can not fetch this pest information at this time! ", Toast.LENGTH_SHORT).show();
            }
      }

      public interface OnScanFragmentListener {
            void onScanFragmentMoreIconClicked(long id);
      }

      private void openScouting(){
            Intent scouting=new Intent(getContext(), ScoutAutomated.class);
            scouting.putExtra("mode", ChatbotAdapterRV.MODE_SCOUT);
            startActivityForResult(scouting,SCOUT);
      }


      private void openScanner() {
            if (MyApplication.hasLocation()) {
                  if (myApplication.hasNetworkConnection(getActivity())) {
                        MyCamera.action = MyCamera.ACTION_SCAN;
                        Intent i = new Intent(getActivity(), CameraScan.class);
//                        Intent i = new Intent(getActivity(), CameraScanSimple.class);
                        i.putExtra("index", index);
                        startActivityForResult(i, 100);
                  } else {
                        myApplication.showInternetError(getActivity());
                  }
            } else {
                  MyApplication.showToast(getActivity(), "Generating your location. Please try again later", Gravity.CENTER);
            }
      }

      private void getAllScansFromViewModel(){

                  mainViewModel.getScoutModelList().observe(getActivity(), scoutModels -> {
                        items = scoutModels;

                        if ( scoutModels.size() != 0 ){

                              Log.i(TAG, "getAllScansFromViewModel:  " + scoutModels.size());

                              AtomicInteger detectedCounts = new AtomicInteger();

                              AppExecutors.getInstance().getDiskIO().execute(() -> detectedCounts.set(mapDatabase.scanDaoAccess().pestDetectedCount(orgId)));
                              numberOfScansTextView.setText(String.valueOf(scoutModels.size()));

                              new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                          numberOfPestDetectedTextView.setText(String.valueOf(detectedCounts.get()));
                                    }
                              }, 1000);

                              setAdapter();

                              linearLayout.setVisibility(View.GONE);
                              fabButton.setVisibility(View.VISIBLE);
                              configureLayout.setVisibility(View.VISIBLE);
                        }else {

                              Log.i(TAG, "getAllScansFromViewModel:  " + scoutModels.size());

                              linearLayout.setVisibility(View.VISIBLE);
                              fabButton.setVisibility(View.GONE);
                              configureLayout.setVisibility(View.GONE);
                        }
                  });

      }

      private void setAdapter(){

            adapter=new ScanHistoryRecyclerViewAdapter(getContext(), items, this) ;
            adapter.setMode(mode);
            recyclerView.setAdapter(adapter);
      }

      @Override
      public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == Activity.RESULT_OK) {
                  if (requestCode == REQUEST_SCANNER) {
                        String path = data.getStringExtra("path");
                        Log.d(TAG, "Image path: " + path);
                        if (path != null && !path.trim().isEmpty()) {
                              processImage(path);

                              AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                    @Override
                                    public void run() {

                                          int count = mapDatabase.scanDaoAccess().scoutCount(orgId);
                                          itemId = String.valueOf(count+1);

                                          ScanModel scanModel = new ScanModel();
                                          scanModel.setItemId(itemId);
                                          scanModel.setImagePath(path != null ? path : null);
                                          scanModel.setStatus(ScanModel.STATE_UNPROCCESSED);
                                          scanModel.setMessage("LOADING...");
                                          scanModel.setPestIdentified("loading...");
                                          scanModel.setUserId(userId);
                                          scanModel.setOrgId(orgId);
                                          scanModel.setDateCreated(System.currentTimeMillis());
                                          mapDatabase.scanDaoAccess().insert(scanModel);
                                    }
                              });

                        } else {
//                              MyApplication.showToast(this, resolveString(R.string.image_failed_process), Gravity.CENTER);
                              MyApplication.showToast(getActivity(), getActivity().getString(R.string.image_failed_process), Gravity.CENTER);
                        }
                  }
            }
      }

      private void processImage(String path) {
            Log.d(TAG, "process image: " + path);
            setImage(path);
            MultipartBody.Part file = RetrofitHelper.prepareFilePart(getActivity(), "image", path);
            sendFile(file);
//            animateProgress();
      }

      public void sendFile(MultipartBody.Part file) {
            isCancelled = false;
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(Keys.AGRO_BASE)
                    .addConverterFactory(GsonConverterFactory.create());

            Retrofit retrofit = builder.build();

            LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

            String gps = "";
            String email = SharePrefManager.getInstance(getActivity()).getUserDetails().get(0);
            if (MyApplication.poi != null)
                  gps = MyApplication.poi.getLat() + "," + MyApplication.poi.getLon();

            Log.d(TAG, "gps: " + gps);

            Call<ScanResponse> call = service.scanFAW(
                    createPartFromString(Keys.APP_ID),
                    createPartFromString(SharePrefManager.getInstance(getActivity()).getOrganisationDetails().get(0)),
                    createPartFromString(gps),
                    createPartFromString(email),
                    file, Keys.API_KEY);

            Log.e(TAG, "ext_code: " + SharePrefManager.getInstance(getActivity()).getOrganisationDetails().get(0));

            Log.e(TAG, "Details: " + Keys.APP_ID + ", " + gps + ", " + email + ", " + file + ", " + Keys.API_KEY);

            call.enqueue(new Callback<ScanResponse>() {
                  @Override
                  public void onResponse(Call<ScanResponse> call, Response<ScanResponse> response) {

                        ScanResponse responseBody = response.body();

                        String message = null;
                        String success = null;
//                        try {
                        if (responseBody != null) {
                              message = responseBody.getMsg();
                              success = responseBody.getSuccess();
                        }

//                    Toast.makeText(ScanActivity.this,"Success: " + success +", ScanActivity response: "+message+" body: "+responseBody
//                            +" code: "+response.code()+" resp message: "+response.message(), Toast.LENGTH_LONG).show();

                        Log.e(TAG, "Success: " + success + ", ScanActivity response: " + message + " body: " + responseBody
                                + " code: " + response.code() + " resp message: " + response.message());

                        try {
                              processResult(NetworkRequest.STATUS_SUCCESS, message, responseBody);
                        } catch (JSONException e) {
                              e.printStackTrace();
                              Log.e(TAG, "onResponse:  " + e.getMessage());
                        }
                  }

                  @Override
                  public void onFailure(Call<ScanResponse> call, Throwable t) {
                        String message = t.getMessage();
                        Log.e(TAG, "throwable ScanActivity: " + t);
                        Toast.makeText(getActivity(), "Scan Failed: " + message, Toast.LENGTH_LONG).show();
                  }
            });

            //animateProgress();
      }

      private void processResult (int status, String response, ScanResponse scanResponse) throws JSONException {

                  if (status == NetworkRequest.STATUS_SUCCESS) {
                        try {
                                    String success= scanResponse.getSuccess();
                                    String message= scanResponse.getMsg();
                                    String detected   = scanResponse.getDetected();
                                    Log.d(TAG,"success: "+success+" message: "+message);
                                    Log.d(TAG, "ProcessResult item Id " + itemId);

                                    String message1 =Integer.valueOf( detected) == 1 ? "Pest detected" : message != null ? message : "Pest not detected";
                                    Log.d(TAG,"success: "+success+" message: "+message + "message:" + message1);

                              AppExecutors.getInstance().getDiskIO().execute(() -> {
                                          ScanModel scanModel = mapDatabase.scanDaoAccess().fetchScoutByItemId(itemId);
                                    Log.d(TAG, "ProcessResult item Id " + itemId);
                                    scanModel.setStatus(ScanModel.STATE_PROCESSED);
                                          scanModel.setMessage(message1);
                                          scanModel.setPestIdentified(message);

                                          if (message != null && !message.equals("No label found")) {
                                                Log.e(TAG, "Not Detected: " + scanFAW.getHeader());
                                                scanFAW.setHeader("Pest Not Detected");
                                                scanModel.setPestFound(true);
                                          } else {
                                                scanModel.setPestFound(false);
                                          }

                                          mapDatabase.scanDaoAccess().updateScout(scanModel);

                                          EventBus.getDefault().post(new ScanCompletedEvent(scanModel.getId()));

                                    Log.i(TAG, "processResult Scan :  " + message);
                                          Log.i(TAG, "processResult Scan :  " + detected);
                                    });
//                              }

                        } catch (Exception e) {
                              e.printStackTrace();
                              Log.e(TAG, "processResult:  " + e.getMessage() );
                        }
                  } else {
//                        onFailed();
//                  }
//            } else {
//
////                  onFailed();
            }
      }

      private void setImage(String path) {
//            if (btnShare != null) btnShare.setVisibility(View.VISIBLE);
            Log.d(TAG, "path: " + path);
            Image image = new Image();
            image.setImageBitmap(myApplication.bitmapCompressLarge(path));
            if (image.getImageBitmap() != null) {
                  image.setImageString(MyApplication.encodeBitmap(image.getImageBitmap()));
//                  imageView.setImageBitmap(image.getImageBitmap());
                  image.setImage_url(path);
                  image.setUri(Uri.parse(path));
                  scanFAW.setImage(image);
            }

      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onMessageEvent(ScanDeletedEvent event) {
            Toast.makeText(getContext(), "Event from Scan Fragment", Toast.LENGTH_SHORT).show();
      }

      @Override
      public void onStart() {
            super.onStart();
            EventBus.getDefault().register(this);
      }

      @Override
      public void onStop() {
            EventBus.getDefault().unregister(this);
            super.onStop();
      }

      private KnowledgeBase searchKB(String name){

             KnowledgeBase knowledgeBaseModel = new KnowledgeBase();

             try {

                   for (KnowledgeBase knowledgeBase : knowledgeBaseList){
                         Log.i(TAG, "KNOWLEDGE BASE searchKB:  Title : " + knowledgeBase.getTitle().toLowerCase());
                         Log.i(TAG, "KNOWLEDGE BASE searchKB:  Title : " + name.toLowerCase());
//                  if(knowledgeBase.getTitle().toLowerCase().contains(name.toLowerCase())){
                         int nameLength = name.length();
                         int cutOffPoint = (int) (nameLength * (0.8));
                         Log.i(TAG, "KNOWLEDGE BASE Cut off:  : " + cutOffPoint );

                         if (name.contains("thrips adult") || name.contains("thrips")) {
                               if (knowledgeBase.getTitle().toLowerCase().contains(name.toLowerCase().substring(0, 5))) {
                                     knowledgeBaseModel = knowledgeBase;
                                     Log.i(TAG, "onChanged:  KNOWLEDGE BASE Item found " + knowledgeBase.getTitle());
                               }

                         }else if (name.contains("fruitworm") || name.contains("fruitwo")) {
//                        if (knowledgeBase.getTitle().toLowerCase().contains(name.toLowerCase().substring(0, 4))) {
                               if (knowledgeBase.getTitle().toLowerCase().contains("fruit worm")) {
                                     knowledgeBaseModel = knowledgeBase;
                                     Log.i(TAG, "onChanged:  KNOWLEDGE BASE Item found " + knowledgeBase.getTitle());
                               }

                         }else if (name.contains("fruit fly")){

                               if (knowledgeBase.getTitle().toLowerCase().contains("fruit fly")) {
                                     knowledgeBaseModel = knowledgeBase;
                                     Log.i(TAG, "onChanged:  KNOWLEDGE BASE Item found " + knowledgeBase.getTitle());
                               }

                         }else {
                               if(knowledgeBase.getTitle().toLowerCase().contains(name.toLowerCase().substring(0,10))){
                                     knowledgeBaseModel = knowledgeBase;
                                     Log.i(TAG, "onChanged:  KNOWLEDGE BASE Item found " + knowledgeBase.getTitle());
                               }
                         }

                   }

             }catch (Exception e){

                   Log.e(TAG, "searchKB:  " +e.getMessage()  );
             }

            return knowledgeBaseModel;
      }

      private void loadKnowledgeBaseList(){
            mainViewModel.getKnowledgeBaseList().observe(getActivity(), new Observer<List<KnowledgeBase>>() {
                  @Override
                  public void onChanged(List<KnowledgeBase> knowledgeBases) {
                        knowledgeBaseList = knowledgeBases;

                        Log.i(TAG, "onChanged:  KNOWLEDGE BASE " + knowledgeBases.size());
                        Log.i(TAG, "onChanged: KNOWLEDGE BASE  " + knowledgeBaseList.size());
                  }
            });
      }

      private void getKnowledgeBaseFromAPI() {

            mAgroWebAPI.fetchKnowledgeBase("english", "app",Keys.API_KEY)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new io.reactivex.Observer<KnowledgeBaseResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(KnowledgeBaseResponse knowledgeBaseResponse) {
//
                                List<KnowledgeBase> topics = knowledgeBaseResponse.getTopics();

                                Log.i(TAG, "onNext:  KnowledgeBase API " + topics.size() );

                                if (topics.contains("Adult Thr ")){
                                      Log.i(TAG, "onNext:  ");
                                      Log.i(TAG, "onNext:  KnowledgeBase API : pest found!");
                                }

//                                KnowledgeBase knowledgeBase = searchKB(shortenedResults);
                          }

                          @Override
                          public void onError(Throwable e) {}

                          @Override
                          public void onComplete() {
//                                progressLayout.setVisibility(View.GONE);
                          }
                    });
      }


}
