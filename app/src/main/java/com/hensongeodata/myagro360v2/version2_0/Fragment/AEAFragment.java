package com.hensongeodata.myagro360v2.version2_0.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.version2_0.Adapter.AdapterListBasic;
import com.hensongeodata.myagro360v2.version2_0.Events.AEAItemClickedEvent;
import com.hensongeodata.myagro360v2.version2_0.Model.Member;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AEAFragment extends Fragment {

      private RecyclerView recyclerView;
      private TextView usersTextView;
      private ProgressBar progressBar;
      private AdapterListBasic mAdapter;
      private MainViewModel mainViewModel;
      private List<Member> memberItems;
      private View parent_view;
      private AEAFragmentListener aeaFragmentListener;



      public AEAFragment() {
            // Required empty public constructor
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            View view =  inflater.inflate(R.layout.fragment_aea, container, false);

            // Inflate the layout for this fragment
            parent_view = view.findViewById(android.R.id.content);
            usersTextView = view.findViewById(R.id.number_of_users);
            progressBar      = view.findViewById(R.id.progress_bar);
            mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            memberItems = new ArrayList<>();

            getMembersViewModel();

//            initToolbar();

            new Handler().postDelayed(new Runnable() {
                  @Override
                  public void run() {
                        initComponent(view);
                        progressBar.setVisibility(View.GONE);
                  }
            },1000);

            return view;

      }


      private void initToolbar(View view) {
      }

      private void initComponent(View view) {
            recyclerView = view.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setHasFixedSize(true);

            //set data and list adapter
            mAdapter = new AdapterListBasic(getActivity(), memberItems);
            recyclerView.setAdapter(mAdapter);

            // on item list clicked
            mAdapter.setOnItemClickListener((view1, obj, position) -> {
                  EventBus.getDefault().post(new AEAItemClickedEvent(obj.getMemberId(),obj.getFirstName() + " " + obj.getLastName(), obj.getId()));
            });

      }

      private void getMembersViewModel(){
            mainViewModel.getMembers().observe(getActivity(), members -> {
                  usersTextView.setText(String.valueOf(members.size()));
                  memberItems = members;
            });
      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
      }


      public interface AEAFragmentListener{
            void onAEAFragmentItemSelected(String name, long id);
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onItemClicked(AEAItemClickedEvent event) {
      }

      @Override
      public void onStart() {
            super.onStart();
            EventBus.getDefault().register(this);
      }

      @Override
      public void onStop() {
            EventBus.getDefault().unregister(this);
            super.onStop();
      }

}
