package com.hensongeodata.myagro360v2.version2_0.Adapter;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hensongeodata.myagro360v2.Interface.HistoryTabListener;
import com.hensongeodata.myagro360v2.Interface.ItemTouchHelperViewHolder;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.version2_0.Listeners.ScoutsRvListener;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;
import com.hensongeodata.myagro360v2.version2_0.Model.ScoutModel;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by user1 on 8/2/2016.
 */

public class ScoutHistoryRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG = ScoutHistoryRecyclerViewAdapter.class.getSimpleName();
    private List<ScoutModel> items;
    private Context mContext;
    private HistoryTabListener listener;
    private ScoutsRvListener mScoutsRVListener;
    private LocsmmanEngine locsmmanEngine;
    private int mode;
    private ArrayList<String> strPois;
    protected DatabaseHelper databaseHelper;
    private MapDatabase mapDatabase;
    private long perimeter;
    private double inKilometers;
    private double acreage;
    private MyApplication myApplication;

    User mUser;

    public ScoutHistoryRecyclerViewAdapter(Context context, List<ScoutModel> items, ScoutsRvListener scoutsRvListener) {
        mScoutsRVListener = scoutsRvListener;
        mContext=context;
        this.items =items;
        locsmmanEngine=new LocsmmanEngine(context);
        databaseHelper=new DatabaseHelper(mContext);
        mapDatabase = MapDatabase.getInstance(context);
        myApplication = new MyApplication();
    }


    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.scout_history, parent, false);
            return new TabHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final RecyclerView.ViewHolder holder, final int position) {


        if (holder instanceof TabHolder) {

            ScoutModel scoutModel = items.get(position);

            final long plotId = scoutModel.getPlotId();
            final String batchId = scoutModel.getBatchId();
            final long scoutId = scoutModel.getId();
            final String orgId = SharePrefManager.getInstance(mContext).getOrganisationDetails().get(0);
            final long dateCreated = scoutModel.getDateCreated();


            Log.i(TAG, "onBindViewHolder:  Plot Id" + scoutModel.getPlotId());

//            tabHolder.scoutTitleTextView.setText(message);

//            tabHolder.noPreviewAvailableImageView.setImageURI(Uri.parse(imageUrl));

            AtomicReference<FarmPlot> farmPlot = new AtomicReference<>();
            final int[] batchCount = {0};
            final int[] pestDetectedCount = {0};

            AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                @Override
                public void run() {
                    try{

                        farmPlot.set(mapDatabase.plotDaoAccess().fetchPlotByPlotId(plotId));
                        batchCount[0] = mapDatabase.scanDaoAccess().scansCountByBatchId(orgId, Long.valueOf(batchId));
                        pestDetectedCount[0] = mapDatabase.scanDaoAccess().pestDetectedCountByBatchId(orgId,  Long.valueOf(batchId));

                    }catch (Exception e){
                        Log.e(TAG, "Scout History  "  + e.getMessage() );
                    }

                }
            });

            new Handler().postDelayed(() -> {
//                if (farmPlot.get() != null){

                    String dateString = new SimpleDateFormat("dd/MM/yyyy hh:mm").format(new Date(dateCreated));

                    ((TabHolder) holder).dateCreatedTextView.setText(String.format("%s", String.valueOf(dateString)));

                    ((TabHolder) holder).plotNameTextView.setText(String.format("%s", String.valueOf(farmPlot.get()== null ? "No plot associated " : farmPlot.get().getName())));

                    ((TabHolder) holder).scoutTitleTextView.setText(String.format("Scout %s", String.valueOf(position + 1)));

                    ((TabHolder) holder).totalScansTextView.setText(String.format(" %s", String.valueOf(batchCount[0])));

                    if (pestDetectedCount[0] >0){
                        ((TabHolder) holder).pestDetectedTextView.setTextColor(mContext.getResources().getColor(R.color.red));
                    }else {
                        ((TabHolder) holder).pestDetectedTextView.setTextColor(mContext.getResources().getColor(R.color.colorGreen100));
                    }

                    ((TabHolder) holder).pestDetectedTextView.setText(String.format(" %s", String.valueOf(pestDetectedCount[0])));
//                }


                ((TabHolder) holder).moreImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mScoutsRVListener.onScoutingFragmentMoreClicked(scoutId, plotId);
                    }
                });


            },600);


            AppExecutors.getInstance().getNetworkIO().execute(() -> {
//                    HelperClass.LoadWebView(tabHolder.webView, sectionId, tabHolder.noPreviewAvailableImageView, mContext);
            });

        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class TabHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
        public TextView scoutTitleTextView;
        public TextView totalScansTextView;
        public WebView webView;
        public ProgressBar progressBar;
        public TextView pestIdentifiedTextView;
        public TextView pestDetectedTextView;
        public TextView plotNameTextView;
        public TextView dateCreatedTextView;
        public ImageView moreImageView;
        public Button shareButton;
        public ImageView syncImageView;
        public ImageView noPreviewAvailableImageView;

        public TabHolder(View itemView) {
            super(itemView);

            scoutTitleTextView = itemView.findViewById(R.id.scout_title);
            totalScansTextView = itemView.findViewById(R.id.total_scans);
            progressBar           = itemView.findViewById(R.id.progress_bar);
            pestIdentifiedTextView = itemView.findViewById(R.id.pest_identified);
            pestDetectedTextView = itemView.findViewById(R.id.pests_detected);
            plotNameTextView        = itemView.findViewById(R.id.plot_name);
            dateCreatedTextView   = itemView.findViewById(R.id.date_created);
            moreImageView   = itemView.findViewById(R.id.more);
            shareButton           = itemView.findViewById(R.id.share_map);
            syncImageView      = itemView.findViewById(R.id.sync_image);
            noPreviewAvailableImageView = itemView.findViewById(R.id.no_preview_available);

        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

    private double metersToAcres(long meterSquared){
        return Math.round((meterSquared*0.00024711) * 1000.0) / 1000.0;
    }

    private double metersToKilo(long metersSquared){
        return metersSquared*0.001;
    }

    private void showPerimeter(TabHolder tabHolder, String section_id){


//        tabHolder.mapSizeTextView.setText(String.format("%sm | %skm |  %s acres", String.valueOf(perimeter), inKilometers, acreage));

    }

}