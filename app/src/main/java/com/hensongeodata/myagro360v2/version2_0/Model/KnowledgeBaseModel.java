package com.hensongeodata.myagro360v2.version2_0.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity( tableName =  KnowledgeBaseModel.TABLE_NAME)
public class KnowledgeBaseModel {

      public static final String TABLE_NAME = "knowledgebase";

      public static final String NOTIFICATION_MAPS = "Maps";
      public static final String NOTIFICATION_PLOTS = "Plots";
      public static final String NOTIFICATION_CROPS = "Crops";
      public static final String NOTIFICATION_SCAN = "Scans";
      public static final String NOTIFICATION_SCOUT = "ScanModel";
      public static final String NOTIFICATION_ACTIVITIES = "Activities";
      public static final String NOTIFICATION_MEMBERS = "Members";

      public static final String COLUMN_ID = "id";
      public static final String COLUMN_NAME= "name";
      public static final String COLUMN_MESSAGE = "message";
      public static final String COLUMN_OWNER = "owner";
      public static final String COLUMN_DATE_CREATED = "date_created";


      @PrimaryKey(autoGenerate = true)
      @ColumnInfo( name = COLUMN_ID)
      private long id;

      @SerializedName("name")
      @ColumnInfo( name = COLUMN_NAME)
      private String name;

      @ColumnInfo( name = COLUMN_MESSAGE)
      private String message;

      @ColumnInfo( name = COLUMN_OWNER)
      private String owner;

      @ColumnInfo( name = COLUMN_DATE_CREATED)
      private long dateCreated;

      public long getId() {
            return id;
      }

      public void setId(long id) {
            this.id = id;
      }

      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      public String getMessage() {
            return message;
      }

      public void setMessage(String message) {
            this.message = message;
      }

      public String getOwner() {
            return owner;
      }

      public void setOwner(String owner) {
            this.owner = owner;
      }

      public long getDateCreated() {
            return dateCreated;
      }

      public void setDateCreated(long dateCreated) {
            this.dateCreated = dateCreated;
      }
}
