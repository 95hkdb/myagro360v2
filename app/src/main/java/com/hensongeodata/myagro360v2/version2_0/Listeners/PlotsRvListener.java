package com.hensongeodata.myagro360v2.version2_0.Listeners;

public interface PlotsRvListener {

      void moreClicked(long id, String name, String plotId, String mapId);

      void shareButtonClicked(long id, String name, String plotId, String mapId);

}
