package com.hensongeodata.myagro360v2.version2_0.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hensongeodata.myagro360v2.R;

public class ProfileDialogFragment extends DialogFragment {

      private OnFragmentInteractionListener mListener;

      public ProfileDialogFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_profile_dialog, container, false);
      }

      public void onButtonPressed(Uri uri) {
            if (mListener != null) {
                  mListener.onFragmentInteraction(uri);
            }
      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof OnFragmentInteractionListener) {
                  mListener = (OnFragmentInteractionListener) context;
            } else {
                  throw new RuntimeException(context.toString()
                          + " must implement OnPlotActivitiesFragmentListener");
            }
      }

      @Override
      public void onDetach() {
            super.onDetach();
            mListener = null;
      }

      public interface OnFragmentInteractionListener {
            void onFragmentInteraction(Uri uri);
      }
}
