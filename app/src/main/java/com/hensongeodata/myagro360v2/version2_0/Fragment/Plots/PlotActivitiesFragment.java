package com.hensongeodata.myagro360v2.version2_0.Fragment.Plots;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.helper.LineItemDecoration;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Adapter.AdapterListInbox;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmActivity;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class PlotActivitiesFragment extends DialogFragment {
      private static final String TAG = PlotActivitiesFragment.class.getSimpleName();
      private static final String PLOT_ID = "plot_id";
      private static final String ARG_PARAM2 = "param2";
      private View parent_view;
      private TextView plotNameTextView;
      private ProgressBar progressBar;
      private LinearLayout noLinearLayout;
      private Button addNewActivityButton;
      private AppCompatImageButton btnClose;
      private AppCompatButton btnSave;
      private ImageButton btnAddNew;
      private RecyclerView recyclerView;
      private AdapterListInbox mAdapter;
      private ActionMode actionModeCallback;
      private ActionMode actionMode;
      private MapDatabase mapDatabase;

      private long mParam1;
      private String mParam2;

      private OnPlotActivitiesFragmentListener mListener;

      public PlotActivitiesFragment() {
            // Required empty public constructor
      }

      public static PlotActivitiesFragment newInstance(long plotId) {
            PlotActivitiesFragment fragment = new PlotActivitiesFragment();
            Bundle args = new Bundle();
            args.putLong(PLOT_ID, plotId);
            fragment.setArguments(args);
            return fragment;
      }

      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                  mParam1 = getArguments().getLong(PLOT_ID);
                  mParam2 = getArguments().getString(ARG_PARAM2);
            }
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            parent_view = inflater.inflate(R.layout.fragment_plot_activities, container, false);

            mapDatabase = MapDatabase.getInstance(getContext());
            btnClose    = parent_view.findViewById(R.id.bt_close);
            btnSave    = parent_view.findViewById(R.id.bt_save);
            plotNameTextView = parent_view.findViewById(R.id.plot_name);
            progressBar = parent_view.findViewById(R.id.progress_bar);
            noLinearLayout = parent_view.findViewById(R.id.no_activities_layout);
            addNewActivityButton = parent_view.findViewById(R.id.add_new_activity_button);
            btnAddNew                       = parent_view.findViewById(R.id.bt_add_new);

            addNewActivityButton.setOnClickListener(v -> onAddNewActivityButtonClicked(mParam1));

            btnAddNew.setOnClickListener(v -> onAddNewActivityButtonClicked(mParam1));

            btnClose.setOnClickListener(v -> {
                  onDismiss();
                  dismiss();
            });

            initComponent();
            return parent_view;
      }

      public void onDismiss() {
            if (mListener != null) {
                  mListener.onPlotActivitiesDialogDismiss();
            }
      }

      public void onAddNewActivityButtonClicked(long plotId) {
            if (mListener != null) {
                  mListener.onNewPlotActivitiesFragmentButtonClicked(plotId);
            }
      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof OnPlotActivitiesFragmentListener) {
                  mListener = (OnPlotActivitiesFragmentListener) context;
            } else {
                  throw new RuntimeException(context.toString()
                          + " must implement OnPlotActivitiesFragmentListener");
            }
      }

      @Override
      public void onDetach() {
            super.onDetach();
            mListener = null;
      }

      private void initComponent() {
            recyclerView =  parent_view.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.addItemDecoration(new LineItemDecoration(getContext(), LinearLayout.VERTICAL));
            recyclerView.setHasFixedSize(true);

            String userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
            String orgId   = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);
            AtomicReference<List<FarmActivity>> items = new AtomicReference<>();
            AtomicReference<FarmPlot> farmPlot = new AtomicReference<>();

            AppExecutors.getInstance().getDiskIO().execute(() -> {

                  farmPlot.set(mapDatabase.plotDaoAccess().fetchPlotByPlotId(mParam1));

                  items.set(mapDatabase.farmActivityDaoAccess().fetchActivitiesByPrimaryPlotId(userId, orgId, mParam1));
            });


            new Handler().postDelayed(() -> {

                  if ( farmPlot.get() != null ){
                        plotNameTextView.setText(farmPlot.get().getName());
                  }

                  mAdapter = new AdapterListInbox(getContext(), items.get());

                  if (mAdapter.getItemCount() != 0){
                        recyclerView.setAdapter(mAdapter);
                        mAdapter.setOnClickListener(new AdapterListInbox.OnClickListener() {
                              @Override
                              public void onItemClick(View view, FarmActivity obj, int pos) {
                                    if (mAdapter.getSelectedItemCount() > 0) {
//                              enableActionMode(pos);
                                    } else {
                                          // read the inbox which removes bold from the row
                                          FarmActivity inbox = mAdapter.getItem(pos);
                                          Toast.makeText(getContext(), "Read: " + inbox.getName(), Toast.LENGTH_SHORT).show();
                                    }
                              }

                              @Override
                              public void onItemLongClick(View view, FarmActivity obj, int pos) {
//                        enableActionMode(pos);
                              }
                        });

                        noLinearLayout.setVisibility(View.GONE);

                  }else if (mAdapter.getItemCount() == 0){
                        noLinearLayout.setVisibility(View.VISIBLE);
                  }

                  progressBar.setVisibility(View.GONE);
            }, 3000);
      }


      public interface OnPlotActivitiesFragmentListener {
            void onPlotActivitiesDialogDismiss();

            void onNewPlotActivitiesFragmentButtonClicked(long plotId);
      }

}
