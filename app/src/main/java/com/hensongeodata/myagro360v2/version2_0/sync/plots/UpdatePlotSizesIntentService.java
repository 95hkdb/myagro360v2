package com.hensongeodata.myagro360v2.version2_0.sync.plots;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

import com.hensongeodata.myagro360v2.version2_0.sync.plots.downloads.PlotsDownloadTask;

public class UpdatePlotSizesIntentService extends IntentService {

      public UpdatePlotSizesIntentService() {
            super("UpdatePlotSizesIntentService");
      }

      @Override
      protected void onHandleIntent(@Nullable Intent intent) {
            PlotsDownloadTask.UpdatePlotSizes(this);
      }
}
