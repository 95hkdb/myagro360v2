package com.hensongeodata.myagro360v2.version2_0;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.GridCategoryAdapter;
import com.hensongeodata.myagro360v2.helper.SpacingItemDecoration;
import com.hensongeodata.myagro360v2.helper.Tools;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends AppCompatActivity {

      private RecyclerView mRecyclerView;
      private GridCategoryAdapter mGridCategoryAdapter;

      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.fragment_main_home);


            mRecyclerView = findViewById(R.id.recyclerView);
            mRecyclerView.setLayoutManager( new GridLayoutManager(this, 1) );
            mRecyclerView.addItemDecoration(new SpacingItemDecoration(4, Tools.dip2px(this, 8), true));
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setNestedScrollingEnabled(false);

            List<String> list = new ArrayList<>();
            list.add("COMMUNITY");
            list.add("COMMUNITY");
//            list.add("MAPPING");
//            list.add("SCOUT");
//            list.add("SCAN");
//            list.add("FORMS");

            mGridCategoryAdapter = new GridCategoryAdapter(this,  list);
            mRecyclerView.setAdapter(mGridCategoryAdapter);
      }
}
