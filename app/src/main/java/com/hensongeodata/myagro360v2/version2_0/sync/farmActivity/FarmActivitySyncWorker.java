package com.hensongeodata.myagro360v2.version2_0.sync.farmActivity;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import org.json.JSONException;

public class FarmActivitySyncWorker extends Worker {
      private static final String TAG = FarmActivitySyncWorker.class.getSimpleName();

      public FarmActivitySyncWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
            super(context, workerParams);
      }

      @NonNull
      @Override
      public Result doWork() {
            try {
                  FarmActivitySyncTask.pushAllFarmActivities(getApplicationContext());
            } catch (JSONException e) {
                  Log.e(TAG, "doWork:  Sync Farm Activities" + e.getMessage() );
                  e.printStackTrace();
            }
            return null;
      }

}
