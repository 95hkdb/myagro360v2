package com.hensongeodata.myagro360v2.version2_0.sync.plots.downloads;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class PlotsDownloadWorker extends Worker {
      private static final String TAG = PlotsDownloadWorker.class.getSimpleName();

      public PlotsDownloadWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
            super(context, workerParams);
      }

      @NonNull
      @Override
      public Result doWork() {


              try {

                    PlotsDownloadTask.DownloadAllPlots(getApplicationContext());

            } catch (Exception e) {
                  Log.e(TAG, "doWork:  Plots Download " + e.getMessage() );
                  e.printStackTrace();
                  return Result.retry();
            }
                  return Result.success();
      }

}
