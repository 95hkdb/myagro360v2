package com.hensongeodata.myagro360v2.version2_0;


import android.app.Dialog;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.KnowledgeBase;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class KnowledgeBaseDetailFragment extends BottomSheetDialogFragment {

      private BottomSheetBehavior mBehavior;
      private AppBarLayout app_bar_layout;
      private LinearLayout lyt_profile;

      private KnowledgeBase knowledgeBase;

      public void setData(KnowledgeBase knowledgeBase) {
            this.knowledgeBase = knowledgeBase;
      }


      public KnowledgeBaseDetailFragment() {}



      public Dialog onCreateDialog(Bundle savedInstanceState) {
            final BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
            final View view = View.inflate(getContext(), R.layout.fragment_knowledge_base_detail, null);

            dialog.setContentView(view);
            mBehavior = BottomSheetBehavior.from((View) view.getParent());
            mBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);

            app_bar_layout = (AppBarLayout) view.findViewById(R.id.app_bar_layout);
            lyt_profile = (LinearLayout) view.findViewById(R.id.lyt_profile);

            try{

                  if (knowledgeBase.getMediaUrl() != null){
                        if (!knowledgeBase.getMediaUrl().isEmpty() ){
                              ImageView image = view.findViewById(R.id.kb_detail_image);
                              Picasso.get().load(knowledgeBase.getMediaUrl()).into(image);
                        }
                  }

                  ((TextView) view.findViewById(R.id.name)).setText(knowledgeBase.getTitle());

                  String content = Html.fromHtml(knowledgeBase.getContent()).toString();

                  ((TextView) view.findViewById(R.id.kb_content)).setText(content);
                  ((TextView) view.findViewById(R.id.name_toolbar)).setText(knowledgeBase.getTitle());


                  hideView(app_bar_layout);

                  mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                        @Override
                        public void onStateChanged(@NonNull View bottomSheet, int newState) {
                              if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                                    showView(app_bar_layout, getActionBarSize());
                                    hideView(lyt_profile);
                              }
                              if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                                    hideView(app_bar_layout);
                                    showView(lyt_profile, getActionBarSize());
                              }

                              if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                                    dismiss();
                              }
                        }

                        @Override
                        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                        }
                  });

            }catch (Exception e){
                  ((TextView) view.findViewById(R.id.name_toolbar)).setText("NO PEST INFORMATION!");

                  ((TextView) view.findViewById(R.id.kb_content)).setText("Sorry, we can not fetch this pest information at this time!");

//                  Toast.makeText(getActivity(), "Sorry, we can not fetch this pest information at this time! ", Toast.LENGTH_SHORT).show();
            }

//            ((View) view.findViewById(R.id.lyt_spacer)).setMinimumHeight(Tools.getScreenHeight() / 2);

            ((ImageButton) view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        dismiss();
                  }
            });

            return dialog;
      }

      private void hideView(View view) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = 0;
            view.setLayoutParams(params);
      }

      private void showView(View view, int size) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = size;
            view.setLayoutParams(params);
      }

      private int getActionBarSize() {
            final TypedArray styledAttributes = getContext().getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
            int size = (int) styledAttributes.getDimension(0, 0);
            return size;
      }

}
