package com.hensongeodata.myagro360v2.version2_0.Fragment.Map;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Events.MapSortClosed;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapSortFragment extends DialogFragment {

      private static final String TAG = MapSortFragment.class.getSimpleName();
      private AppCompatSpinner spn_sortOptions;
      private HashMap<Integer, String> spinnerSortsMap;
      private AppCompatImageButton btnClose;


      public MapSortFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            View view = inflater.inflate(R.layout.fragment_map_sort, container, false);
            spn_sortOptions = view.findViewById(R.id.spn_farms);
            btnClose    = view.findViewById(R.id.bt_close);

            getSortOptions();

            btnClose.setOnClickListener(v -> {
                  dismiss();
                  EventBus.getDefault().post(new MapSortClosed());
            });

            spn_sortOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        switch ((int) id){
                              case 0:
                                    defaultMapSort();
                                    break;

                              case 1:
                                    byRecentSort();
                                    break;

                              case 2:
                                    byMapSizeHighToLow();
                                    break;

                              case 3:
                                    byMapSizeLowToHigh();
                                    break;
                        }

                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) {}
            });


            return view;
      }

      private void byMapSizeLowToHigh() {

            if (SharePrefManager.isMapSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearMapSortPreference(getContext());
                  SharePrefManager.setMapSortPreference(getContext(), SharePrefManager.SORT_BY_SIZE_LOW_TO_HIGH_MAP);
            }
            SharePrefManager.setMapSortPreference(getContext(), SharePrefManager.SORT_BY_SIZE_LOW_TO_HIGH_MAP);

      }

      private void byMapSizeHighToLow() {

            if (SharePrefManager.isMapSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearMapSortPreference(getContext());
                  SharePrefManager.setMapSortPreference(getContext(), SharePrefManager.SORT_BY_SIZE_HIGH_TO_LOW_MAP);
            }
            SharePrefManager.setMapSortPreference(getContext(), SharePrefManager.SORT_BY_SIZE_HIGH_TO_LOW_MAP);
      }

      private void byRecentSort() {
            if (SharePrefManager.isMapSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearMapSortPreference(getContext());
                  SharePrefManager.setMapSortPreference(getContext(), SharePrefManager.SORT_BY_RECENT_MAP);
            }
            SharePrefManager.setMapSortPreference(getContext(), SharePrefManager.SORT_BY_RECENT_MAP);
      }

      private void defaultMapSort() {

            if (SharePrefManager.isMapSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearMapSortPreference(getContext());
                  SharePrefManager.setMapSortPreference(getContext(), SharePrefManager.SORT_BY_ALL_MAP);
            }
            SharePrefManager.setMapSortPreference(getContext(), SharePrefManager.SORT_BY_ALL_MAP);
      }

      private void getSortOptions() {

            ArrayList<String> stringArrayList = new ArrayList<>();
            stringArrayList.add( "All");
            stringArrayList.add( "By Recent");
            stringArrayList.add( "By Map Size( Highest to Lowest )");
            stringArrayList.add("By Map Size( Lowest to Highest )");

            String[] spinnerArray = new String[stringArrayList.size()];

            try {

                  spinnerSortsMap = new HashMap<Integer, String>();
                  for ( int i =0; i < stringArrayList.size(); i++ ){
                        spinnerSortsMap.put(i, stringArrayList.get(i));
                        spinnerArray[i] = stringArrayList.get(i);
                  }

                  HelperClass.setupArrayAdapter(getContext(),spinnerArray, spn_sortOptions);

                  Log.i(TAG, "onChanged:  Farms " + stringArrayList.size());

            } catch (Exception e) {
                  Log.e(TAG, "onNext:  " + e.getMessage());
            }

      }

}
