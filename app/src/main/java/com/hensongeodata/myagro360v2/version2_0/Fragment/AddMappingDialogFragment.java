package com.hensongeodata.myagro360v2.version2_0.Fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddMappingDialogFragment extends DialogFragment {


      private DatabaseHelper databaseHelper;
      public AddMappingDialogFragment() { }


      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            databaseHelper = new DatabaseHelper(getContext());
            View view = inflater.inflate(R.layout.fragment_add_mapping_dialog, container, false);
            return view;
      }

      
}
