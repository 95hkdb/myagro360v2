package com.hensongeodata.myagro360v2.version2_0.Fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.google.android.material.tabs.TabLayout;
import com.hensongeodata.myagro360v2.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopFragment extends Fragment {

      private ViewPager view_pager;
      private TabLayout tab_layout;
      private EditText et_search;
      private ImageButton bt_clear;
      private ProgressBar progress_bar;


      public ShopFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View view = inflater.inflate(R.layout.fragment_shop_fragement, container, false);

            return view;
      }

}
