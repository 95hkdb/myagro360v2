package com.hensongeodata.myagro360v2.version2_0.Model;

import android.graphics.drawable.Drawable;

public class People {

      public int image;
      public Drawable imageDrw;
      public String name;
      public String email;
      public boolean section = false;

      public People() {
      }

      public People(String name, boolean section) {
            this.name = name;
            this.section = section;
      }

}
