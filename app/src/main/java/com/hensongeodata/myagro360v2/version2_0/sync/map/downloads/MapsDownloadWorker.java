package com.hensongeodata.myagro360v2.version2_0.sync.map.downloads;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class MapsDownloadWorker extends Worker {
      private static final String TAG = Worker.class.getSimpleName();

      public MapsDownloadWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
            super(context, workerParams);
      }

      @NonNull
      @Override
      public Result doWork() {
            MapsDownloadTask.DownloadMaps(getApplicationContext());
            return null;
      }

}
