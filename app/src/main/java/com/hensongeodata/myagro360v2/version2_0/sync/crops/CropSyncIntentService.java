package com.hensongeodata.myagro360v2.version2_0.sync.crops;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

import com.hensongeodata.myagro360v2.version2_0.sync.DBSyncTask;

public class CropSyncIntentService extends IntentService {

      public CropSyncIntentService() {
            super("CropSyncIntentService");
      }

      @Override
      protected void onHandleIntent(@Nullable Intent intent) {
            DBSyncTask.syncCrops(this);
      }
}
