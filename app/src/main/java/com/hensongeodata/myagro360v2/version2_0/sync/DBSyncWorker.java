package com.hensongeodata.myagro360v2.version2_0.sync;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import org.json.JSONException;

public class DBSyncWorker extends Worker {
      private static final String TAG = DBSyncWorker.class.getSimpleName();

      public DBSyncWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
            super(context, workerParams);
      }

      @NonNull
      @Override
      public Result doWork() {
            try {
                  DBSyncTask.syncDB(getApplicationContext());
            } catch (JSONException e) {
                  Log.e(TAG, "doWork:  Sync DB" + e.getMessage() );
                  e.printStackTrace();
            }
            return null;
      }

}
