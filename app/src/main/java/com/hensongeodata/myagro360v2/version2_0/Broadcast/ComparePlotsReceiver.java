package com.hensongeodata.myagro360v2.version2_0.Broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.response.PlotResponse;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.sync.plots.downloads.PlotsDownloadIntentService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComparePlotsReceiver extends BroadcastReceiver {

      MediaPlayer mp;
      @Override
      public void onReceive(Context context, Intent intent) {

            AgroWebAPI agroWebAPI  = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            MapDatabase mapDatabase = MapDatabase.getInstance(context);
            String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
            String orgId = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

            agroWebAPI.getPlotsCall(orgId, userId).enqueue(new Callback<PlotResponse>() {
                  @Override
                  public void onResponse(Call<PlotResponse> call, Response<PlotResponse> response) {

                        AppExecutors.getInstance().getDiskIO().execute(() -> {
                              int plotsCount = mapDatabase.plotDaoAccess().plotCountByOrgId(orgId);

                              if (plotsCount != response.body().getPlots().size()){
                                    showPlotsDownloadDialog(context);
                              }
                        });

                  }

                  @Override
                  public void onFailure(Call<PlotResponse> call, Throwable t) {

                  }
            });

            Toast.makeText(context, "Alarm....", Toast.LENGTH_LONG).show();
      }

      private void showPlotsDownloadDialog(Context context) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Download plots?");
            builder.setMessage(R.string.download_plots_string);
            builder.setPositiveButton("OK, Download!", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intentToSyncImmediately = new Intent(context, PlotsDownloadIntentService.class);
                        context.startService(intentToSyncImmediately);
                  }
            });
            builder.setNegativeButton("NO", null);
            builder.show();
      }
}
