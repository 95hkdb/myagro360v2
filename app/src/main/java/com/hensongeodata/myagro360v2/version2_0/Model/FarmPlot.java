package com.hensongeodata.myagro360v2.version2_0.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import com.google.gson.annotations.SerializedName;


@Entity( tableName = FarmPlot.TABLE_NAME)
public class FarmPlot {

      public static final String TABLE_NAME = "plots";

      public static final String COLUMN_ID   = "id";
      public static final String COLUMN_PLOT_ID  = "plot_id";
      public static final String COLUMN_NAME = "name";
      public static final String COLUMN_DESCRIPTION= "description";
      public static final String COLUMN_SIZE= "size";
      public static final String COLUMN_SIZE_AC= "size_in_acres";
      public static final String COLUMN_SIZE_KM= "size_in_km";
      public static final String COLUMN_SIZE_M= "size_in_m";
      public static final String COLUMN_CROP_ID= "crop_id";
      public static final String COLUMN_CROP_NAME= "crop_name";
      public static final String COLUMN_MAP_ID= "map_id";
      public static final String COLUMN_ORG_ID= "org_id";
      public static final String COLUMN_FARM_ID= "farm_id";
      public static final String COLUMN_PRIMARY_FARM_ID= "primary_farm_id";
      public static final String COLUMN_FARM_NAME = "farm_name";
      private static final String COLUMN_SYNCED = "synced";
      public static final String COLUMN_USER_ID= "user_id";
      public static final String COLUMN_CREATED_AT= "created_at";
      public static final String COLUMN_CREATED_DATE_STRING= "created_date_string";
      public static final String COLUMN_UPDATED_AT= "updated_at";

      @PrimaryKey(autoGenerate = true)
      @ColumnInfo( name = COLUMN_PLOT_ID)
      private long plotId;

      @ColumnInfo( name = COLUMN_ID)
      @SerializedName("id")
      private String id;

      @ColumnInfo( name = COLUMN_NAME)
      @SerializedName("name")
      private String name;

      @ColumnInfo( name = COLUMN_DESCRIPTION)
      @SerializedName("plot_description")
      private String description;

      @ColumnInfo( name = COLUMN_SIZE)
      @SerializedName("plot_size")
      private double size;

      @ColumnInfo( name = COLUMN_SIZE_M)
      private double sizeM;

      @ColumnInfo( name = COLUMN_SIZE_AC)
      private double sizeAc;

      @ColumnInfo( name = COLUMN_SIZE_KM)
      private double sizeKm;

      @ColumnInfo( name = COLUMN_CROP_ID)
      @SerializedName("crop_id")
      private String cropId;

      @ColumnInfo( name = COLUMN_CROP_NAME)
      @SerializedName("crop_name")
      private String cropName;

      @ColumnInfo( name = COLUMN_MAP_ID)
      @SerializedName("map_id")
      private String mapId;

      @ColumnInfo( name = COLUMN_ORG_ID)
      @SerializedName("org_id")
      private String orgId;

      @ColumnInfo( name = COLUMN_USER_ID)
      @SerializedName("user_id")
      private String userId;

      @ColumnInfo( name = COLUMN_FARM_ID)
      @SerializedName("farm_id")
      private String farmId;

      @ColumnInfo( name = COLUMN_PRIMARY_FARM_ID)
      private long primaryFarmId;

      @ColumnInfo( name = COLUMN_FARM_NAME)
      @SerializedName("farm_name")
      private String farmName;

      @ColumnInfo( name = COLUMN_SYNCED)
      @SerializedName("synced")
      private boolean synced;

      @ColumnInfo( name = COLUMN_UPDATED_AT)
      private Long updatedAt;

      @ColumnInfo( name = COLUMN_CREATED_AT)
      private Long createdAt;

      @ColumnInfo( name = COLUMN_CREATED_DATE_STRING)
      @SerializedName("created_date")
      private String createdDateString;

      public long getPlotId() {
            return plotId;
      }

      public void setPlotId(long plotId) {
            this.plotId = plotId;
      }

      public String getId() {
            return id;
      }

      public void setId(String id) {
            this.id = id;
      }

      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      public String getDescription() {
            return description;
      }

      public void setDescription(String description) {
            this.description = description;
      }

      public double getSize() {
            return size;
      }

      public void setSize(double size) {
            this.size = size;
      }

      public double getSizeM() {
            return sizeM;
      }

      public void setSizeM(double sizeM) {
            this.sizeM = sizeM;
      }

      public double getSizeAc() {
            return sizeAc;
      }

      public void setSizeAc(double sizeAc) {
            this.sizeAc = sizeAc;
      }

      public double getSizeKm() {
            return sizeKm;
      }

      public void setSizeKm(double sizeKm) {
            this.sizeKm = sizeKm;
      }

      public String getCropId() {
            return cropId;
      }

      public void setCropId(String cropId) {
            this.cropId = cropId;
      }

      public String getCropName() {
            return cropName;
      }

      public void setCropName(String cropName) {
            this.cropName = cropName;
      }

      public String getMapId() {
            return mapId;
      }

      public void setMapId(String mapId) {
            this.mapId = mapId;
      }

      public String getOrgId() {
            return orgId;
      }

      public void setOrgId(String orgId) {
            this.orgId = orgId;
      }

      public String getUserId() {
            return userId;
      }

      public void setUserId(String userId) {
            this.userId = userId;
      }

      public String getFarmId() {
            return farmId;
      }

      public void setFarmId(String farmId) {
            this.farmId = farmId;
      }

      public long getPrimaryFarmId() {
            return primaryFarmId;
      }

      public void setPrimaryFarmId(long primaryFarmId) {
            this.primaryFarmId = primaryFarmId;
      }

      public String getFarmName() {
            return farmName;
      }

      public void setFarmName(String farmName) {
            this.farmName = farmName;
      }

      public boolean isSynced() {
            return synced;
      }

      public void setSynced(boolean synced) {
            this.synced = synced;
      }

      public Long getUpdatedAt() {
            return updatedAt;
      }

      public void setUpdatedAt(Long updatedAt) {
            this.updatedAt = updatedAt;
      }

      public Long getCreatedAt() {
            return createdAt;
      }

      public void setCreatedAt(Long createdAt) {
            this.createdAt = createdAt;
      }

      public String getCreatedDateString() {
            return createdDateString;
      }

      public void setCreatedDateString(String createdDateString) {
            this.createdDateString = createdDateString;
      }
}
