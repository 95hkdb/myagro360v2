package com.hensongeodata.myagro360v2.version2_0.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity( tableName = FarmActivity.TABLE_NAME)
public class FarmActivity  {

      public static final String TABLE_NAME = "activities";

      public static final String COLUMN_ID   = "id";
      public static final String COLUMN_FARM_ACTIVITY_ID  = "farm_activity_id";
      public static final String COLUMN_NAME = "name";
      public static final String COLUMN_ADDITIONAL_INFO = "additional_info";
      public static final String COLUMN_START_DATE_TIME_STRING = "start_date_time_string";
      public static final String COLUMN_START_DATE = "start_date";
      public static final String COLUMN_END_DATE= "end_date";
      public static final String COLUMN_END_DATE_TIME_STRING = "start_end_date_time_string";
      public static final String COLUMN_PLOT_ID = "plot_id";
      public static final String COLUMN_PRIMARY_PLOT_ID = "primary_plot_id";
      public static final String COLUMN_FARM_ID = "farm_id";
      public static final String COLUMN_OUTSOURCED = "outsourced";
      public static final String COLUMN_OUTSOURCED_NAME = "outsourced_name";
      public static final String COLUMN_STAFF_ID = "staff_id";
      public static final String COLUMN_USER_ID = "user_id";
      public static final String COLUMN_TYPE = "type";
      public static final String COLUMN_ORG_ID = "org_id";
      public static final String COLUMN_COMPLETED= "completed";
      public static final String COLUMN_SYNCED= "synced";
      public static final String COLUMN_CREATED_AT= "created_at";
      public static final String COLUMN_UPDATED_AT= "updated_at";

      @PrimaryKey( autoGenerate = true)
      @ColumnInfo( name = COLUMN_FARM_ACTIVITY_ID)
      @SerializedName("activity_id")
      private long activityId;

      @ColumnInfo( name = COLUMN_ID )
      @SerializedName("id")
      private String id;

      @ColumnInfo( name = COLUMN_NAME)
      @SerializedName("name")
      private String name;

      @ColumnInfo( name = COLUMN_ADDITIONAL_INFO)
      @SerializedName("comment")
      private String additionalInfo;

      @ColumnInfo( name = COLUMN_START_DATE_TIME_STRING)
      private String startDateTimeString;

      @ColumnInfo( name = COLUMN_END_DATE_TIME_STRING)
      private String endDateTimeString;

      @ColumnInfo( name = COLUMN_START_DATE)
      @SerializedName("start_date")
      private long startDate;

      @ColumnInfo( name = COLUMN_END_DATE)
      @SerializedName("end_date")
      private long endDate;

      @ColumnInfo( name = COLUMN_PLOT_ID)
      @SerializedName("plot_id")
      private String plotId;

      @ColumnInfo( name = COLUMN_PRIMARY_PLOT_ID)
      private long primaryPlotId;

      @ColumnInfo( name = COLUMN_FARM_ID)
      private String farmId;

      @ColumnInfo( name = COLUMN_OUTSOURCED)
      private boolean outSourced;

      @ColumnInfo( name = COLUMN_STAFF_ID)
      private String staffId;

      @ColumnInfo( name = COLUMN_OUTSOURCED_NAME)
      private String outSourcedName;

      @ColumnInfo( name = COLUMN_USER_ID)
      private String userId;

      @ColumnInfo( name = COLUMN_TYPE)
      private String type;

      @ColumnInfo( name = COLUMN_ORG_ID)
      private String orgId;

      @ColumnInfo( name = COLUMN_COMPLETED)
      private boolean completed;

      @ColumnInfo( name = COLUMN_SYNCED)
      private boolean synced;

      @ColumnInfo( name = COLUMN_CREATED_AT)
      private long createdAt;

      @ColumnInfo( name = COLUMN_UPDATED_AT)
      private long updatedAt;

      public long getActivityId() {
            return activityId;
      }

      public void setActivityId(long activityId) {
            this.activityId = activityId;
      }

      public String getId() {
            return id;
      }

      public void setId(String id) {
            this.id = id;
      }

      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      public String getAdditionalInfo() {
            return additionalInfo;
      }

      public void setAdditionalInfo(String additionalInfo) {
            this.additionalInfo = additionalInfo;
      }

      public String getStartDateTimeString() {
            return startDateTimeString;
      }

      public void setStartDateTimeString(String startDateTimeString) {
            this.startDateTimeString = startDateTimeString;
      }

      public String getEndDateTimeString() {
            return endDateTimeString;
      }

      public void setEndDateTimeString(String endDateTimeString) {
            this.endDateTimeString = endDateTimeString;
      }

      public long getStartDate() {
            return startDate;
      }

      public void setStartDate(long startDate) {
            this.startDate = startDate;
      }

      public long getEndDate() {
            return endDate;
      }

      public void setEndDate(long endDate) {
            this.endDate = endDate;
      }

      public String getPlotId() {
            return plotId;
      }

      public void setPlotId(String plotId) {
            this.plotId = plotId;
      }


      public long getPrimaryPlotId() {
            return primaryPlotId;
      }

      public void setPrimaryPlotId(long primaryPlotId) {
            this.primaryPlotId = primaryPlotId;
      }

      public String getFarmId() {
            return farmId;
      }

      public void setFarmId(String farmId) {
            this.farmId = farmId;
      }

      public boolean isOutSourced() {
            return outSourced;
      }

      public void setOutSourced(boolean outSourced) {
            this.outSourced = outSourced;
      }

      public String getStaffId() {
            return staffId;
      }

      public void setStaffId(String staffId) {
            this.staffId = staffId;
      }

      public String getOutSourcedName() {
            return outSourcedName;
      }

      public void setOutSourcedName(String outSourcedName) {
            this.outSourcedName = outSourcedName;
      }

      public String getUserId() {
            return userId;
      }

      public void setUserId(String userId) {
            this.userId = userId;
      }

      public String getOrgId() {
            return orgId;
      }

      public void setOrgId(String orgId) {
            this.orgId = orgId;
      }

      public String getType() {
            return type;
      }

      public void setType(String type) {
            this.type = type;
      }

      public boolean isCompleted() {
            return completed;
      }

      public void setCompleted(boolean completed) {
            this.completed = completed;
      }

      public boolean isSynced() {
            return synced;
      }

      public void setSynced(boolean synced) {
            this.synced = synced;
      }

      public long getCreatedAt() {
            return createdAt;
      }

      public void setCreatedAt(long createdAt) {
            this.createdAt = createdAt;
      }

      public long getUpdatedAt() {
            return updatedAt;
      }

      public void setUpdatedAt(long updatedAt) {
            this.updatedAt = updatedAt;
      }
}
