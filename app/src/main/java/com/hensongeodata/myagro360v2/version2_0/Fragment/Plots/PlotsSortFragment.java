package com.hensongeodata.myagro360v2.version2_0.Fragment.Plots;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Model.Crop;
import com.hensongeodata.myagro360v2.version2_0.Model.Farm;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static io.fabric.sdk.android.Fabric.TAG;

public class PlotsSortFragment extends DialogFragment {
      private static final String MAP_ID = "map_id" ;
      private static final String PLOT_SIZE = "plot_size" ;
      private MapDatabase mapDatabase;
      private MainViewModel mainViewModel;
      private AgroWebAPI mAgroWebAPI;
      private AppCompatSpinner spn_crops;
      private AppCompatSpinner spn_maps;
      private AppCompatSpinner spn_sortOptions;
      private RadioGroup cropRadioGroup;
      private HashMap<Integer, String> spinnerCropsMap;
      private HashMap<Integer, String> spinnerSortsMap;
      private LinearLayout cropSpinnerLinearLayout;
      private TextView cropTypeTitleTextView;
      private AppCompatImageButton btnClose;
      private AppCompatButton btnSave;
      ArrayList<HashMap<String,String>> maps;
      String mMapId;
      double mPlotSize;
      private String mCropId;
      private String mFarmId;
      private int currentSortSelection;
      private LinearLayout cropRadioLayout;
      private RadioButton radioCropButton;
      private String cropType;
      private PlotsSortFragment.OnPlotSortFragmentListener onPlotSortFragmentListener;

      public PlotsSortFragment() { }

      public static PlotsSortFragment newInstance(String mapId, double plotSize){
            PlotsSortFragment fragment = new PlotsSortFragment();
            Bundle args = new Bundle();
            args.putString(MAP_ID, mapId);
            args.putDouble(PLOT_SIZE, plotSize);
            fragment.setArguments(args);
            return fragment;
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            mAgroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);
            mapDatabase = MapDatabase.getInstance(getContext());
            View view =  inflater.inflate(R.layout.fragment_plots_sort, container, false);
            cropSpinnerLinearLayout = view.findViewById(R.id.crop_spinner_linear_layout);
            cropTypeTitleTextView = view.findViewById(R.id.crop_type_title);
            spn_crops = view.findViewById(R.id.spn_crops);
            spn_sortOptions = view.findViewById(R.id.spn_farms);
            spn_maps  = view.findViewById(R.id.spn_maps);
            cropRadioGroup = view.findViewById(R.id.cropRadioGroup);
            cropRadioLayout = view.findViewById(R.id.crop_radio_layout);
            radioCropButton   = view.findViewById(R.id.radio_crop);
            btnClose    = view.findViewById(R.id.bt_close);
            btnSave    = view.findViewById(R.id.bt_save);

            Log.i(TAG, "onCreateView:  Plot Size " + mPlotSize);

            final Farm[] farm = new Farm[1];

            setCurrentSortSelection();

            spn_sortOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        Log.i(TAG, "onItemSelected:  " + id);

                        switch((int) id){

                              case 0:
                                    sortByLastToFirst();
                                    cropRadioLayout.setVisibility(View.GONE);
                                    break;

                              case 1:
                                    showCropOptions();
                                    break;

                              case 2:
                                    sortByPlotNameAsc();
                                    cropRadioLayout.setVisibility(View.GONE);
                                    break;

                              case 3:
                                    sortByPlotNameDesc();
                                    cropRadioLayout.setVisibility(View.GONE);
                                    break;

                              case 4:
                                    sortByPlotSizeDescending();
                                    cropRadioLayout.setVisibility(View.GONE);
                                    break;

                              case 5:
                                    sortByPlotSizeAscending();
                                    cropRadioLayout.setVisibility(View.GONE);
                                    break;


                              case 6:
                                    distinctPlots();
                                    cropRadioLayout.setVisibility(View.GONE);
                                    break;

                              case 7:
                                    sortByLastToFirst();
                                    cropRadioLayout.setVisibility(View.GONE);
                                    break;
                        }

                        mFarmId = spinnerSortsMap.get(spn_sortOptions.getSelectedItemPosition());

                        Log.i(TAG, "onItemSelected:  Farm ID " + mFarmId );

                        AppExecutors.getInstance().getDiskIO().execute(() -> {
                              farm[0] = mapDatabase.farmDaoAccess().fetchFarmByFarmId(mFarmId);
                              Log.i(TAG, "run:  Plot " + farm[0] + " Farm ID " + mFarmId);
                        });
                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) {}
            });

            spn_crops.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mCropId = spinnerCropsMap.get(spn_crops.getSelectedItemPosition());

                        SharePrefManager.setCurrentCropSelection(getContext(), id);

                        if (SharePrefManager.isCropTypePreferenceAvailable(getContext())) {
                              SharePrefManager.clearCropTypePreference(getContext());
                              SharePrefManager.setSortCropPreference(getContext(), mCropId);
                        }

                        SharePrefManager.setSortCropPreference(getContext(), mCropId);

                        Log.i(TAG, "onItemSelected:  Crop Id " + mCropId  );

                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) { }
            });

            cropSpinnerLinearLayout.setVisibility(View.GONE);

            cropRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                  Log.i(TAG, "onCheckedChanged:  " + checkedId);

                  switch(checkedId) {
                        case R.id.radio_crop:
                              cropType = "crop";
                              cropSpinnerLinearLayout.setVisibility(View.VISIBLE);
                              cropTypeTitleTextView.setText("Crop");
                              break;
                        case R.id.radio_livestock:
                              cropType = "livestock";
                              cropSpinnerLinearLayout.setVisibility(View.VISIBLE);
                              cropTypeTitleTextView.setText("Livestock");
                              break;
                  }
            });

            btnClose.setOnClickListener(v -> {
                  onDismiss();
                  dismiss();
            });

//            btnSave.setOnClickListener(v -> {
//                  onSave();
//                  dismiss();
//            });

            getCropsFromDatabase();
            getSortOptions();

            spn_sortOptions.setSelection(currentSortSelection, true);
            spn_crops.setSelection((int) SharePrefManager.getCurrentCropSelection(getContext()));
            return view;
      }

      private void setCurrentSortSelection() {
            switch (SharePrefManager.getSortPlotsPreference(getContext())){

                  case SharePrefManager.SORT_BY_PLOT_ALL:
                        currentSortSelection = 0;
                        break;

                  case SharePrefManager.SORT_BY_CROP:
                        currentSortSelection = 1;
                        break;

                  case SharePrefManager.SORT_BY_PLOT_NAME_ASC:
                        currentSortSelection = 2;
                        break;

                  case SharePrefManager.SORT_BY_PLOT_NAME_DESC:
                        currentSortSelection = 3;
                        break;

                  case SharePrefManager.SORT_BY_PLOT_SIZE_ASC:
                        currentSortSelection = 4;
                        break;

                  case SharePrefManager.SORT_BY_PLOT_SIZE_DESC:
                        currentSortSelection = 5;
                        break;

                  case SharePrefManager.SORT_BY_DISTINCT:
                        currentSortSelection = 6;
                        break;

                  case SharePrefManager.SORT_BY_LAST_TO_FIRST:
                        currentSortSelection = 7;
                        break;
            }
      }

      private void distinctPlots() {
            if (SharePrefManager.isSortPreferenceAvailable(getContext()))
                  SharePrefManager.clearSortPlotsPreference(getContext());
            SharePrefManager.setSortPlotsPreference(getContext(), SharePrefManager.SORT_BY_DISTINCT);
      }

      private void showSearch() {}

      private void sortByPlotSizeAscending() {
            if (SharePrefManager.isSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearSortPlotsPreference(getContext());
                  SharePrefManager.setSortPlotsPreference(getContext(), SharePrefManager.SORT_BY_PLOT_SIZE_ASC);
            }
            SharePrefManager.setSortPlotsPreference(getContext(), SharePrefManager.SORT_BY_PLOT_SIZE_DESC);

      }

      private void sortByPlotSizeDescending() {
            if (SharePrefManager.isSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearSortPlotsPreference(getContext());
                  SharePrefManager.setSortPlotsPreference(getContext(), SharePrefManager.SORT_BY_PLOT_SIZE_DESC);
            }
            SharePrefManager.setSortPlotsPreference(getContext(), SharePrefManager.SORT_BY_PLOT_SIZE_DESC);
      }

      private void sortByPlotNameDesc() {
            if (SharePrefManager.isSortPreferenceAvailable(getContext())){
                  SharePrefManager.clearSortPlotsPreference(getContext());
                  SharePrefManager.setSortPlotsPreference(getContext(), SharePrefManager.SORT_BY_PLOT_NAME_DESC);
            }
            SharePrefManager.setSortPlotsPreference(getContext(), SharePrefManager.SORT_BY_PLOT_NAME_DESC);
      }

      private void sortByPlotNameAsc() {
            if (SharePrefManager.isSortPreferenceAvailable(getContext()))
                  SharePrefManager.clearSortPlotsPreference(getContext());
            SharePrefManager.setSortPlotsPreference(getContext(), SharePrefManager.SORT_BY_PLOT_NAME_ASC);
      }

      private void showCropOptions() {
            if (SharePrefManager.isSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearSortPlotsPreference(getContext());
                  SharePrefManager.setSortPlotsPreference(getContext(), SharePrefManager.SORT_BY_CROP);
            }
            SharePrefManager.setSortPlotsPreference(getContext(), SharePrefManager.SORT_BY_CROP);

            cropRadioLayout.setVisibility(View.VISIBLE);
            radioCropButton.performClick();
      }

      private void sortByLastToFirst() {
            if (SharePrefManager.isSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearSortPlotsPreference(getContext());
                  SharePrefManager.setSortPlotsPreference(getContext(), SharePrefManager.SORT_BY_LAST_TO_FIRST);
            }
            SharePrefManager.setSortPlotsPreference(getContext(), SharePrefManager.SORT_BY_LAST_TO_FIRST);

      }


      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                  mMapId = getArguments().getString(MAP_ID);
                  mPlotSize = getArguments().getDouble(PLOT_SIZE);
            }
      }

      @NotNull
      @Override
      public Dialog onCreateDialog(Bundle savedInstanceState) {
            Dialog dialog = super.onCreateDialog(savedInstanceState);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            return dialog;
      }

      public void onSave() {
            if (onPlotSortFragmentListener != null) {
                  onPlotSortFragmentListener.onPlotSortSave();
            }
      }

      public void onDismiss() {
            if (onPlotSortFragmentListener != null) {
                  onPlotSortFragmentListener.onPlotsSortDismiss();
            }
      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof PlotsSortFragment.OnPlotSortFragmentListener) {
                  onPlotSortFragmentListener = (PlotsSortFragment.OnPlotSortFragmentListener) context;
            }
      }

      @Override
      public void onDetach() {
            super.onDetach();
            onPlotSortFragmentListener = null;
      }

      private void getSortOptions() {

            ArrayList<String> stringArrayList = new ArrayList<>();
            stringArrayList.add( "All");
            stringArrayList.add( "By Crop");
            stringArrayList.add("By Plot Name ( A-Z )");
            stringArrayList.add( "By Plot Name ( Z-A )");
            stringArrayList.add( "By Plot Size( Highest to Lowest )");
            stringArrayList.add("By Plot Size( Lowest to Highest )");
//            stringArrayList.add(HelperClass.BY_SEARCH_PLOTS_SORT_, "Search Plots by Name ");
//            stringArrayList.add(HelperClass.BY_DATE_PLOTS_SORT_, "By Date Created ");
            stringArrayList.add( "DISTINCT Plots ");
            stringArrayList.add( "By Recent ");

            String[] spinnerArray = new String[stringArrayList.size()];
            try {

                  spinnerSortsMap = new HashMap<Integer, String>();
                  for ( int i =0; i < stringArrayList.size(); i++ ){
                        spinnerSortsMap.put(i, stringArrayList.get(i));
                        spinnerArray[i] = stringArrayList.get(i);
                  }

                  setupArrayAdapter(spinnerArray, spn_sortOptions);

                  Log.i(TAG, "onChanged:  Farms " + stringArrayList.size());

            } catch (Exception e) {
                  Log.e(TAG, "onNext:  " + e.getMessage());
            }

      }

      private void getCropsFromDatabase(){
            mainViewModel.getCrops().observe(getActivity(), new androidx.lifecycle.Observer<List<Crop>>() {
                  @Override
                  public void onChanged(List<Crop> crops) {

                        String[] spinnerArray = new String[crops.size()];
                        try {

                              spinnerCropsMap = new HashMap<Integer, String>();
                              for ( int i =0; i < crops.size(); i++ ){
                                    spinnerCropsMap.put(i, String.valueOf(crops.get(i).getId()));
                                    spinnerArray[i] = crops.get(i).getName();
                              }

                              setupArrayAdapter(spinnerArray, spn_crops);

                        } catch (Exception e) {
                              Log.e(TAG, "onNext:  " + e.getMessage());
                        }
                  }
            });
      }

      private void setupArrayAdapter(String[] stringArray, AppCompatSpinner spinner){
            ArrayAdapter<String> array = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_item, stringArray);
            array.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(array);
      }

      public interface OnPlotSortFragmentListener{
            void onPlotSortSave();

            void onPlotsSortDismiss();
      }
}
