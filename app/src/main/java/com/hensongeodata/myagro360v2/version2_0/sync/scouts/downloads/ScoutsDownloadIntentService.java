package com.hensongeodata.myagro360v2.version2_0.sync.scouts.downloads;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

public class ScoutsDownloadIntentService extends IntentService {

      public ScoutsDownloadIntentService() {
            super("ScoutsDownloadIntentService");
      }

      @Override
      protected void onHandleIntent(@Nullable Intent intent) {
            ScoutsDownloadTask.DownloadScouts(this);
//            ScoutsDownloadTask.DownloadScans(this);
      }
}
