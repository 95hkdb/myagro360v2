package com.hensongeodata.myagro360v2.version2_0.Events;

public  class PushedToServerEvent {

      private String eventType;

      public PushedToServerEvent(String eventType) {
            this.eventType = eventType;
      }

      public String getEventType() {
            return eventType;
      }

      public void setEventType(String eventType) {
            this.eventType = eventType;
      }
}
