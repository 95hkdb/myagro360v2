package com.hensongeodata.myagro360v2.version2_0.Fragment.Plots;


import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hensongeodata.myagro360v2.Interface.HistoryTabListener;
import com.hensongeodata.myagro360v2.Interface.Refresh;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Adapter.PlotsRecyclerviewAdapter;
import com.hensongeodata.myagro360v2.version2_0.Broadcast.ComparePlotsReceiver;
import com.hensongeodata.myagro360v2.version2_0.Events.PlotsDownloadCompleted;
import com.hensongeodata.myagro360v2.version2_0.Fragment.Map.MappingFragment;
import com.hensongeodata.myagro360v2.version2_0.Listeners.PlotsRvListener;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;
import com.hensongeodata.myagro360v2.version2_0.Model.Member;
import com.hensongeodata.myagro360v2.version2_0.Preference.DownloadsSharePref;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;
import com.hensongeodata.myagro360v2.version2_0.sync.plots.UpdatePlotSizesIntentService;
import com.hensongeodata.myagro360v2.version2_0.sync.plots.downloads.PlotsDownloadIntentService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class PlotsFragment extends Fragment implements HistoryTabListener,Refresh {

      private static final String TAG = PlotsFragment.class.getSimpleName();
      private MapDatabase mapDatabase;
      private MainViewModel mMainViewModel;
      private RecyclerView mRecyclerView;
      private Button addNewPlotBtn;
      private FloatingActionButton fabButton;
      private LinearLayout linearLayout;
      private PlotsRecyclerviewAdapter adapter;
      private TextView numberOfPlotsTextView;
      private TextView totalAcreageTextView;
      private ImageView sortImageButton;
      private LinearLayout configureLayout;
      private Button configureFilterButton;
      private ImageView noActivityImageView;
      private TextView     briefTextView;
      private TextView     titleTextView;
      private TextView     accountNameTextView;
      private AppCompatSpinner spn_staff;
      private HashMap<Integer, String> staffSpinnerHashMap;
      private HashMap<Integer, String> staffIDHashMap;
      private String[] spinnerArray;
      private ProgressBar progressBar;
      private Dialog progressDialog;
      private MyApplication myApplication;


      private OnPlotsFragmentListener onPlotsFragmentListener;

      public PlotsFragment() {}

      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_plots, container, false);

//            accountNameTextView = view.findViewById(R.id.account_name);

            mapDatabase         = MapDatabase.getInstance(getContext());
            mMainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            mRecyclerView = view.findViewById(R.id.plot_history_recyclerview);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

            fabButton = view.findViewById(R.id.fab_add);
            linearLayout = view.findViewById(R.id.no_plots_linear_layout);
            addNewPlotBtn = view.findViewById(R.id.add_new_plot_btn);
            numberOfPlotsTextView = view.findViewById(R.id.number_of_plots);
            totalAcreageTextView = view.findViewById(R.id.total_acreage);
            sortImageButton          = view.findViewById(R.id.sort_button);
            configureLayout           = view.findViewById(R.id.configure_layout);
            configureFilterButton  = view.findViewById(R.id.configure_filter);
            noActivityImageView = view.findViewById(R.id.image);
            briefTextView                = view.findViewById(R.id.brief);
            titleTextView                 = view.findViewById(R.id.title);
            spn_staff                        = view.findViewById(R.id.spn_staff);
            myApplication             = new MyApplication();

            fabButton.setOnClickListener(v -> addNewPlot());

            addNewPlotBtn.setOnClickListener(v -> addNewPlot());

            configureFilterButton.setOnClickListener(v -> PlotsFragmentSortButtonClicked());

            sortImageButton.setOnClickListener(v -> PlotsFragmentSortButtonClicked());

            loadPlots();
            calculateTotalAcreage();
            fetchMembersFromDB();
            DownloadsSharePref.getInstance(getContext());

            populatePlotsBySizeAsc();


//            DownloadPlots();

            if (SharePrefManager.isSecondaryUserAvailable(getContext())){

                  new Handler().postDelayed(() -> {
                        if (SharePrefManager.isSecondaryUserSelectionAvailable(getContext())){
                              String selectedId = (staffSpinnerHashMap.get((int) SharePrefManager.getCurrentSecondaryUserSelection(getContext())));
//                              int selectedId = Integer.valueOf(staffSpinnerHashMap.get((int) SharePrefManager.getCurrentSecondaryUserSelection(getContext())));

                              if (selectedId != null){
                                    Log.i(TAG, "run:  Secondary User " + selectedId);
//                                    spn_staff.setSelection(Integer.valueOf(selectedId));
                              }

//                              loadPlots();
//                              if (SharePrefManager.isSecondaryUserAvailable(getContext())){
//                                    populatePlotsByUser();
//                              }else {
//                                    populatePlotsBySizeAsc();
//                              }

                              Log.i(TAG, "onCreateView:  Secondary User  " + SharePrefManager.getSecondaryUserPref(getContext()));
                        }
                  }, 1000);

//                  spn_staff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                        @Override
//                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//                                    String spinnerIndex =
//                                            ( staffSpinnerHashMap.get((int) SharePrefManager.getCurrentSecondaryUserSelection(getContext())));
//
//                              String memberId = null;
//                              if (spinnerIndex != null) {
//                                    memberId = staffIDHashMap.get(Integer.valueOf(spinnerIndex));
//                              }
//
//                              Log.i(TAG, "onItemSelected:  Secondary  Spinner index  " + spinnerIndex);
//                              Log.i(TAG, "onItemSelected:  Secondary  Member id " + memberId);
//
//                              String finalMemberId = memberId;
//                              new Handler().postDelayed(() -> {
//                                          setUserSelection(id, finalMemberId);
//                                          SharePrefManager.getCurrentSecondaryUserSelection(getContext());
//
//                                    }, 2000);
//                        }
//
//                        @Override
//                        public void onNothingSelected(AdapterView<?> parent) {}
//                  });

            }

            String anotherName = "";

            if (SharePrefManager.isSecondaryUserAvailable(getContext()) ){
                  anotherName = SharePrefManager.getSecondaryUserPref(getContext());
            } else {
                  anotherName = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
            }

            String userId = "";

            if (SharePrefManager.isSecondaryUserAvailable(getContext())){
                  userId = SharePrefManager.getSecondaryUserPref(getContext());
            }else {
                  userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
            }

//            accountNameTextView.setText(userId);

            return view;
      }

      private void DownloadPlots() {
            if (myApplication.hasNetworkConnection(getContext())){

                  Log.i(TAG, "onCreateView:  Downloads " + DownloadsSharePref.isPlotsDownloadPreferenceAvailable(getContext()));
                  Log.i(TAG, "onCreateView:  Downloads " + DownloadsSharePref.getPlotsDownloadedPreference(getContext()));

                  if (!DownloadsSharePref.isPlotsDownloadPreferenceAvailable(getContext()) || !DownloadsSharePref.getPlotsDownloadedPreference(getContext())){
                        showPlotsDownloadDialog();
                  }
            }
      }

      private void setUserSelection(long id, String memberId) {

            Log.i(TAG, "Secondary setUserSelection:  Id " + id);
            Log.i(TAG, "Secondary setUserSelection:  " + memberId);
            if (memberId != null){
                  AppExecutors.getInstance().getDiskIO().execute(() -> {
                        Member member = mapDatabase.memberDaoAccess().fetchMemberById(Long.valueOf(memberId));
                        SharePrefManager.clearSecondaryUserPref(getActivity());
                        SharePrefManager.setSecondaryUserPref(getActivity(), member.getEmail());
                  });
                  SharePrefManager.setCurrentSecondaryUserSelection(getActivity(), Long.valueOf(memberId));
            }
      }

      private void loadPlots() {
            if (mMainViewModel != null){
                  mMainViewModel = null;
                  mMainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            }

            if (SharePrefManager.isSortPreferenceAvailable(getContext())){

                  switch (SharePrefManager.getSortPlotsPreference(getContext())){

                        case SharePrefManager.SORT_BY_CROP:
                              populatePlotsByCropType();
                              break;

                        case SharePrefManager.SORT_BY_PLOT_SIZE_DESC:
                              populatePlotsBySizeDesc();
                              break;

                        case SharePrefManager.SORT_BY_PLOT_SIZE_ASC:
                              populatePlotsBySizeAsc();
                              break;

                        case SharePrefManager.SORT_BY_PLOT_NAME_DESC:
                              populatePlotsByPlotNameDesc();
                              break;

                        case SharePrefManager.SORT_BY_PLOT_NAME_ASC:
                              populatePlotsByPlotNameAsc();
                              break;

                        case SharePrefManager.SORT_BY_DISTINCT:
                              populateDistinctPlots();
                              break;

                        case SharePrefManager.SORT_BY_LAST_TO_FIRST:

                        default:
//                              if (SharePrefManager.isSecondaryUserAvailable(getContext())) {
//                                    populatePlotsByUser();
//                              } else {
                                    populatePlotsByLastToFirst();
//                              }

                  }
            }else {
                  populateAllPlots();
//                  populatePlotsByUser();
            }
      }

      private void addNewPlot(){
            onPlotsFragmentListener.onAddNewPlot();
      }

      private void plotsFragmentsMoreClicked(long id, String name, String plotId, String mapId){
            onPlotsFragmentListener.onPlotsFragmentMoreClicked( id, name, plotId, mapId );
      }

      private void PlotsFragmentSortButtonClicked(){
            onPlotsFragmentListener.onPlotsFragmentSortButtonClicked();
      }

      @Override
      public void onDetach() {
            super.onDetach();
            onPlotsFragmentListener = null;
      }

      @Override
      public void onSelected(String item) {}

      @Override
      public void reload(String scout_id) {

      }

      @Override
      public void moreClicked(String title, String sectionId) {

      }

      @Override
      public void shareButtonClicked(String section_id) {

      }

      @Override
      public void onSyncButtonClicked() {

      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof MappingFragment.OnAddMapButtonListener) {
                  onPlotsFragmentListener = (PlotsFragment.OnPlotsFragmentListener) context;
            } else {
                  throw new RuntimeException(context.toString()
                          + " must implement OnPlotsFragmentListener");
            }
      }

      private void populatePlotsByCropType() {
            String orgId = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);
            String cropId = SharePrefManager.getCropSortPreference(getContext());

            mMainViewModel.setPlotsByCrop(mapDatabase.plotDaoAccess().fetchAllPlotsByCropId(orgId,cropId));
            setMainViewModel(mMainViewModel.getPlotsByCrop());
      }

      private void populatePlotsByUser() {

            if (!DownloadsSharePref.isSecondaryUserPlotsDownloadPreferenceAvailable(getContext()) ||
                    !DownloadsSharePref.getSecondaryUserPlotsDownloadPreference(getContext())) {

                  showPlotsDownloadDialog();
            }

            String orgId = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);
//            String userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
            String userId =  SharePrefManager.getSecondaryUserPref(getContext());

            mMainViewModel.setPlotsByUser(mapDatabase.plotDaoAccess().fetchAllPlotsByUserId(orgId,userId));
            setMainViewModel(mMainViewModel.getPlotsByUser());

      }

      private void populateAllPlots(){
            setMainViewModel(mMainViewModel.getPlots());
      }

      private void populatePlotsBySizeDesc(){
            setMainViewModel(mMainViewModel.getPlotsBySizeDesc());
      }

      private void populatePlotsBySizeAsc(){
            setMainViewModel(mMainViewModel.getPlotsBySizeAsc());
      }

      private void populatePlotsByPlotNameDesc(){
            setMainViewModel(mMainViewModel.getPlotsByNameDesc());
      }

      private void populatePlotsByPlotNameAsc(){
            setMainViewModel(mMainViewModel.getPlotsByNameAsc());
      }

      private void populatePlotsByLastToFirst(){
            setMainViewModel(mMainViewModel.getPlotsByDescOrder());
      }

      private void populateDistinctPlots(){
            setMainViewModel(mMainViewModel.getDistinctPlots());
      }

      private void setMainViewModel(LiveData<List<FarmPlot>> plotsBySizeDesc) {
            plotsBySizeDesc.observe(getActivity(), new Observer<List<FarmPlot>>() {
                  @SuppressLint("RestrictedApi")
                  @Override
                  public void onChanged(List<FarmPlot> farmPlotList) {

                        if (farmPlotList.size() != 0) {

                              Log.i(TAG, "onChanged:  Plots Size " + farmPlotList.size());

                              numberOfPlotsTextView.setText(String.valueOf(farmPlotList.size()));
                              linearLayout.setVisibility(View.GONE);
                              fabButton.setVisibility(View.VISIBLE);
                              configureLayout.setVisibility(View.VISIBLE);

                              adapter = new PlotsRecyclerviewAdapter(getContext(), farmPlotList, PlotsFragment.this, new PlotsRvListener() {
                                    @Override
                                    public void moreClicked(long id, String name, String plotId, String mapId) {
                                          plotsFragmentsMoreClicked(id, name, plotId, mapId);
                                    }

                                    @Override
                                    public void shareButtonClicked(long id, String name, String plotId, String mapId) {
//                                          Toast.makeText(getContext(), "Plots Share", Toast.LENGTH_SHORT).show();
                                    }
                              });

                              mRecyclerView.setAdapter(adapter);

                        } else {
                              configureLayout.setVisibility(View.GONE);
                              linearLayout.setVisibility(View.VISIBLE);
                              fabButton.setVisibility(View.GONE);


//                              if (SharePrefManager.isSortPreferenceAvailable(getContext())){
//                                    noActivityImageView.setVisibility(View.GONE);
//                                    briefTextView.setText("No plot matches your filter. ");
//                                    configureLayout.setVisibility(View.VISIBLE);
//                                    addNewPlotBtn.setVisibility(View.GONE);
//                                    configureFilterButton.setVisibility(View.VISIBLE);
//                              }else {
//                                    noActivityImageView.setVisibility(View.GONE);
//                                    configureLayout.setVisibility(View.GONE);
//                                    addNewPlotBtn.setVisibility(View.VISIBLE);
//                                    configureFilterButton.setVisibility(View.GONE);
//
//                              }
                        }
                  }
            });
      }

      private void calculateTotalAcreage(){

//            String userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
            String orgId = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);
            AtomicLong totalSize = new AtomicLong();

            AppExecutors.getInstance().getDiskIO().execute(() -> totalSize.set(mapDatabase.plotDaoAccess().plotSizeSum( orgId)));

            new Handler().postDelayed(() -> totalAcreageTextView.setText(String.format("%s ac", String.valueOf(totalSize.get()))),2000);

      }

      public interface OnPlotsFragmentListener {

            void onAddNewPlot();

            void onPlotsFragmentMoreClicked(long id, String name, String plotId, String mapId);

            void onPlotsFragmentSortButtonClicked();

      }

      @Override
      public void onResume() {
            super.onResume();
            Log.i(TAG, "onResume:  PlotsFragment Restarted.." );
            loadPlots();
      }

      @Override
      public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
            Log.i(TAG, "onViewStateRestored:  PlotsFragment"  );
      }

      @Override
      public void onInflate(Context context, AttributeSet attrs, Bundle savedInstanceState) {
            super.onInflate(context, attrs, savedInstanceState);
            Log.i(TAG, "onInflate:  PlotsFragment");
      }

      private void sharePlotMap(String sectionId){
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, Keys.AGRO_BASE+"faw-api/get-map/?mapping_id="+sectionId);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
      }

      private void fetchMembersFromDB(){

            mMainViewModel.getMembers().observe(getActivity(), new androidx.lifecycle.Observer<List<Member>>() {
                  @Override
                  public void onChanged(List<Member> members) {

                        spinnerArray = new String[members.size()];

                        Log.i(TAG, "PlotsFragment " + members.size());

                        try {

                              staffSpinnerHashMap = new HashMap<Integer, String>();
                              staffIDHashMap            = new HashMap<Integer, String>();

                              for ( int i =0; i < members.size(); i++ ){
                                    long memberId = members.get(i).getMemberId();
//                                    staffSpinnerHashMap.put((int)(memberId), String.valueOf(members.get(i).getId()));

                                    staffIDHashMap.put(i, String.valueOf(memberId));
                                    staffSpinnerHashMap.put((int)(memberId),  String.valueOf(i));
                                    spinnerArray[i] = "( id " + members.get(i).getMemberId() + ") " + " index " + i + " "
                                            +members.get(i).getFirstName() + " "
                                            + members.get(i).getLastName() + " ( " + members.get(i).getEmail();

                                    Log.i(TAG, "onChanged:  " + members.get(i).getId());
                              }
                              HelperClass.setupArrayAdapter(getActivity(),spinnerArray, spn_staff);
                        } catch (Exception e) {
                              Log.e(TAG, "onNext:  " + e.getMessage());
                        }
                  }
            });

      }

      private void showPlotsDownloadDialog() {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Download plots?");
            builder.setMessage(R.string.download_plots_string);
            builder.setPositiveButton("OK, Download!", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intentToSyncImmediately = new Intent(getContext(), PlotsDownloadIntentService.class);
                        getContext().startService(intentToSyncImmediately);

                        new Handler().postDelayed(new Runnable() {
                              @Override
                              public void run() {
                                    showProgressDialog();
//                                    DownloadsSharePref.setPlotsDownloadedPreference(getContext(), true);
                              }
                        }, 10);

                  }
            });
            builder.setNegativeButton("NO", null);
            builder.show();
      }

      private void showProgressDialog() {
             progressDialog = new Dialog(getContext());
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            progressDialog.setContentView(R.layout.dialog_plots_progress);
            progressDialog.setCancelable(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(progressDialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            progressDialog.show();
            progressDialog.getWindow().setAttributes(lp);
      }

      private void showPlotsDownloadAlarmTrigger(Context context){
            AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, ComparePlotsReceiver.class);
            PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
            alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + 1000*60,
                    1000*60, alarmIntent);
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onPlotsFragmentDownloadCompleted(PlotsDownloadCompleted event) {

            if (progressDialog != null && progressDialog.isShowing()) {
                  progressDialog.dismiss();
            }

            if (!event.isFailed()){

//                  if (DownloadsSharePref.isSecondaryUserPlotsDownloadPreferenceAvailable(getContext())){
//                        DownloadsSharePref.clearSecondaryUserPlotsDownloadPreference(getContext());
//                        DownloadsSharePref.setSecondaryUserPlotsDownloadedPreference(getContext(), true);
//                  }

                  if (DownloadsSharePref.isPlotsDownloadPreferenceAvailable(getContext())){
                        DownloadsSharePref.clearPlotsDownloadedPreference(getContext());
                        DownloadsSharePref.setPlotsDownloadedPreference(getContext(), true);
                  }
                  DownloadsSharePref.setPlotsDownloadedPreference(getContext(), true);

                  if((DownloadsSharePref.isPlotsDownloadPreferenceAvailable(getContext())  )
                          && (DownloadsSharePref.isMapsDownloadPreferenceAvailable(getContext()))){
                        startUpdatePlotsService();
                  }

            }else {
                  Toast.makeText(getContext(), "Plots failed to download!", Toast.LENGTH_SHORT).show();
            }

      }

      private void startUpdatePlotsService() {
            Intent intentToSyncImmediately = new Intent(getContext(), UpdatePlotSizesIntentService.class);
            getContext().startService(intentToSyncImmediately);

            Toast.makeText(getContext(), "Plots Sizes updating in the background", Toast.LENGTH_SHORT).show();
      }

      @Override
      public void onStart() {
            super.onStart();
            EventBus.getDefault().register(this);
      }

      @Override
      public void onStop() {
            EventBus.getDefault().unregister(this);
            super.onStop();
      }

}


