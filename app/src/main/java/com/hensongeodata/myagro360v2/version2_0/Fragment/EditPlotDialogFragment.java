package com.hensongeodata.myagro360v2.version2_0.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Model.Crop;
import com.hensongeodata.myagro360v2.version2_0.Model.Farm;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;
import com.hensongeodata.myagro360v2.version2_0.sync.plots.PlotSyncUtil;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static io.fabric.sdk.android.Fabric.TAG;

public class EditPlotDialogFragment extends DialogFragment {

      private static final String MAP_ID = "map_id" ;
      private static final String PLOT_SIZE = "plot_size" ;
      private static final String PLOT_ID = "plot_id";
      private DatabaseHelper databaseHelper;
      private MapDatabase mapDatabase;
      private MainViewModel mainViewModel;
      private AgroWebAPI mAgroWebAPI;
      private AppCompatSpinner spn_crops;
      private AppCompatSpinner spn_maps;
      private AppCompatSpinner spn_farms;
      private ImageButton addNewfarm;
      private HashMap<Integer, String> spinnerCropsMap;
      private HashMap<Integer, String> spinnerFarmsMap;
      private HashMap<Integer, String> mapSpinnerHashMap;
      private AppCompatImageButton btnClose;
      private AppCompatButton btnSave;
      ArrayList<HashMap<String,String>> maps;
      String mMapId;
      double mPlotSize;
      private String mCropId;
      private String mFarmId;
      private LinearLayout cropRadioLayout;
      private RadioButton radioCropButton;
      private LinearLayout cropSpinnerLinearLayout;
      private RadioGroup cropRadioGroup;
      private TextView cropTypeTitleTextView;
      private int currentMapSelection;
      private int currentFarmSelection;
      private int currentCropSelection;
      private String[] mapSpinnerArray;
      private String[] farmSpinnerArray;
      private String[] cropSpinnerArray;
      private String cropType;

      private static final String ARG_PARAM1 = "param1";

      private String mParam1;
      private long  mPlotId;

      private OnEditDialogFragmentListener mListener;

      public EditPlotDialogFragment() {}

      public static EditPlotDialogFragment newInstance(String mapId, double plotSize, long plotId) {
            EditPlotDialogFragment fragment = new EditPlotDialogFragment();
            Bundle args = new Bundle();
            args.putString(MAP_ID, mapId);
            args.putDouble(PLOT_SIZE, plotSize);
            args.putLong(PLOT_ID, plotId);
            fragment.setArguments(args);
            return fragment;
      }

      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                  mParam1 = getArguments().getString(ARG_PARAM1);
                  mPlotId = getArguments().getLong(PLOT_ID);
            }
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            databaseHelper = new DatabaseHelper(getContext());
            mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            mAgroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);
            mapDatabase = MapDatabase.getInstance(getContext());
            View view =  inflater.inflate(R.layout.fragment_edit_plot_dialog, container, false);
            spn_farms = view.findViewById(R.id.spn_farms);
            spn_maps  = view.findViewById(R.id.spn_maps);
            addNewfarm = view.findViewById(R.id.add_new_farm);
            btnClose    = view.findViewById(R.id.bt_close);
            btnSave    = view.findViewById(R.id.bt_save);
            spn_crops = view.findViewById(R.id.spn_crops);
            cropSpinnerLinearLayout = view.findViewById(R.id.crop_spinner_linear_layout);
            cropTypeTitleTextView = view.findViewById(R.id.crop_type_title);
            cropRadioGroup = view.findViewById(R.id.cropRadioGroup);
            cropRadioLayout = view.findViewById(R.id.crop_radio_layout);
            radioCropButton   = view.findViewById(R.id.radio_crop);

            AtomicReference<FarmPlot> oldFarmPlot = new AtomicReference<>();

            AppExecutors.getInstance().getDiskIO().execute(() -> {
                  oldFarmPlot.set(mapDatabase.plotDaoAccess().fetchPlotByPlotId(mPlotId));
            });

            EditText nameEditText = view.findViewById(R.id.plot_name);
            EditText descriptionEditText = view.findViewById(R.id.description);
            EditText sizeEditText = view.findViewById(R.id.size);

            new Handler().postDelayed(() -> {
                  nameEditText.setText(oldFarmPlot.get().getName());
                  descriptionEditText.setText(oldFarmPlot.get().getDescription());
                  sizeEditText.setText(String.valueOf(Double.valueOf(oldFarmPlot.get().getSizeAc())));

                  Log.i(TAG, "Current Farm Selection :  " + currentFarmSelection);


            }, 1000);

            sizeEditText.setEnabled(false);

            spn_maps.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mMapId = mapSpinnerHashMap.get(spn_maps.getSelectedItemPosition());

                        AppExecutors.getInstance().getDiskIO().execute(() -> {
                              MapModel mapModel = mapDatabase.daoAccess().fetchMapById(mMapId);

                              if (mapModel != null){
                                    sizeEditText.setText(String.valueOf(mapModel.getSizeAc()));
                              }
                        });
                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) {

                  }
            });

            spn_farms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mFarmId = spinnerFarmsMap.get(spn_farms.getSelectedItemPosition());
                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) {}
            });

            spn_crops.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mCropId = spinnerCropsMap.get(spn_crops.getSelectedItemPosition());
                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) { }
            });

            addNewfarm.setOnClickListener(v -> {
                  showAddNewFarmFragment("");
            });

            cropRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                  Log.i(TAG, "onCheckedChanged:  " + checkedId);

                  switch(checkedId) {
                        case R.id.radio_crop:
                              cropType = "crop";
                              cropSpinnerLinearLayout.setVisibility(View.VISIBLE);
                              cropTypeTitleTextView.setText("Crop");
                              break;
                        case R.id.radio_livestock:
                              cropType = "livestock";
                              cropSpinnerLinearLayout.setVisibility(View.VISIBLE);
                              cropTypeTitleTextView.setText("Livestock");
                              break;
                  }
            });

            radioCropButton.performClick();

            btnClose.setOnClickListener(v -> dismiss());

            btnSave.setOnClickListener(v -> {

                  final String name = nameEditText.getText().toString();
                  final String description = descriptionEditText.getText().toString();
                  final String mapId = mMapId;
                  final String cropId  = mCropId;
                  final String farmId   = mFarmId;
                  String orgId = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);
                  String userId = "";

                  if (SharePrefManager.isSecondaryUserAvailable(getContext())){
                        userId = SharePrefManager.getSecondaryUserPref(getContext());
                  }else {
                        userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
                  }

                  Log.i(TAG, "onCreateView:  Org Id: " + orgId);

                  FarmPlot farmPlot = new FarmPlot();
                  farmPlot.setPlotId(mPlotId);
                  farmPlot.setName(name);
                  farmPlot.setDescription(description);
                  farmPlot.setMapId(mapId);
                  farmPlot.setCropName(spn_crops.getSelectedItem().toString());
                  farmPlot.setCropId(cropId);
                  farmPlot.setSynced(false);
                  farmPlot.setFarmId(farmId);
                  farmPlot.setFarmName(spn_farms.getSelectedItem().toString());
                  farmPlot.setOrgId(orgId);
                  farmPlot.setUserId(userId);
                  farmPlot.setSizeAc(Double.valueOf(sizeEditText.getText().toString()));

                  if (oldFarmPlot.get().getCreatedAt() == null){
                        farmPlot.setCreatedAt(System.currentTimeMillis());
                  }else {
                        farmPlot.setCreatedAt(oldFarmPlot.get().getCreatedAt());
                  }

                  farmPlot.setUpdatedAt(System.currentTimeMillis());

                  if (name.isEmpty()){
                        nameEditText.setError("Field required!");
                  }

                  if (description.isEmpty()){
                        descriptionEditText.setError("Field required!");
                  }

                  if (!name.isEmpty() && !description.isEmpty()){

                        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                              @Override
                              public void run() {
                                    try{
                                          mapDatabase.plotDaoAccess().updatePlot(farmPlot);
                                          PlotSyncUtil.initialize(getContext());

                                    }catch (Exception e){
                                          Log.e(TAG, "run:  Update Plot Error" + e.getMessage() );
                                    }
                              }
                        });

                        onSave();
                        Toast.makeText(getContext(), "Successfully updated plot!", Toast.LENGTH_SHORT).show();
                        dismiss();
                  }
            });

            fetchMapsFromDatabase();
            getCropsFromDatabase();
            getFarmsFromDB();


            setCurrentMapCurrentSelection(oldFarmPlot.get());

            if (oldFarmPlot.get() != null){
                  setCurrentFarmCurrentSelection(oldFarmPlot.get().getFarmName());
            }
            if (oldFarmPlot.get() != null){
                  setCurrentCropSelection(oldFarmPlot.get().getCropName());
            }
            return view;
      }

      private void setCurrentCropSelection(String item) {
            new Handler().postDelayed(() -> {
                  currentCropSelection =  getSpinnerArrayValuePosition(cropSpinnerArray,item);
                  spn_crops.setSelection(currentCropSelection);
            },1200);
      }

      private void setCurrentFarmCurrentSelection(String item) {
            new Handler().postDelayed(() -> {
                  currentFarmSelection = getSpinnerArrayValuePosition(farmSpinnerArray ,item);
                  Log.i(TAG, "setCurrentFarmCurrentSelection:  " + currentFarmSelection);
                  spn_farms.setSelection(currentFarmSelection);
            },1200);
      }

      private void setCurrentMapCurrentSelection(FarmPlot farmPlot) {

            AtomicReference<MapModel> mapModel = new AtomicReference<>();
            AppExecutors.getInstance().getDiskIO().execute(() -> {
                  if (farmPlot != null ){
                        mapModel.set(mapDatabase.daoAccess().fetchMapById(farmPlot.getMapId()));
                  }
            });

            new Handler().postDelayed(() -> {

                  if (mapModel.get() != null){
                        currentMapSelection = getSpinnerArrayValuePosition(mapSpinnerArray ,mapModel.get().getName());
                        spn_maps.setSelection(currentMapSelection);

                  }else {
                        currentMapSelection = 0;
                  }
            },1200);
      }

      @NotNull
      @Override
      public Dialog onCreateDialog(Bundle savedInstanceState) {
            Dialog dialog = super.onCreateDialog(savedInstanceState);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            return dialog;
      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof ChooseMapDialogFragment.OnChooseMapFragmentListener) {
                  mListener = (EditPlotDialogFragment.OnEditDialogFragmentListener) context;
            } else {
                  throw new RuntimeException(context.toString()
                          + " must implement OnEditDialogFragmentListener");
            }
      }

      public void onSave() {
            if (mListener != null) {
                  mListener.onSave();
            }
      }

      private void showAddNewFarmFragment(String origination) {
            final Dialog dialog = new Dialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            dialog.setContentView(R.layout.fragment_add_farm_dialog);
            dialog.setCancelable(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


            dialog.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        dialog.dismiss();
                  }
            });

            dialog.findViewById(R.id.continue_button).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {

                        EditText editTextName = dialog.findViewById(R.id.farm_name);

                        final String name = editTextName.getText().toString();

                        if (name.isEmpty()){
                              editTextName.setError("Field required!");
                        }


                        if (!name.isEmpty()){
                              Toast.makeText(getContext(), "adding new farm..", Toast.LENGTH_SHORT).show();
                              dialog.findViewById(R.id.continue_button).setVisibility(View.GONE);
                              dialog.findViewById(R.id.loading_indicator).setVisibility(View.VISIBLE);

                              new Handler().postDelayed(() -> {

                                    String userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
                                    String orgId = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);

                                    Farm farmModel = new Farm();
                                    farmModel.setName(name);
                                    farmModel.setUserId(userId);
                                    farmModel.setOrgId(orgId);

                                    AppExecutors.getInstance().getDiskIO().execute(() -> mapDatabase.farmDaoAccess().insert(farmModel));
                                    dialog.dismiss();
                              }, 2500);
                        }
                  }
            });

            dialog.show();
            dialog.getWindow().setAttributes(lp);
      }

      private void getFarmsFromDB() {

            mainViewModel.getFarms().observe(getActivity(), new androidx.lifecycle.Observer<List<Farm>>() {
                  @Override
                  public void onChanged(List<Farm> farms) {

                         farmSpinnerArray = new String[farms.size()];
                        try {

                              spinnerFarmsMap = new HashMap<Integer, String>();
                              for ( int i =0; i < farms.size(); i++ ){
                                    spinnerFarmsMap.put(i, String.valueOf(farms.get(i).getId()));
                                    farmSpinnerArray[i] = farms.get(i).getName();
                              }
                              setupArrayAdapter(farmSpinnerArray, spn_farms);
                        } catch (Exception e) {
                              Log.e(TAG, "onNext:  " + e.getMessage());
                        }

                  }
            });
      }

      private void getCropsFromDatabase(){
            mainViewModel.getCrops().observe(getActivity(), new androidx.lifecycle.Observer<List<Crop>>() {
                  @Override
                  public void onChanged(List<Crop> crops) {

                        cropSpinnerArray = new String[crops.size()];
                        try {

                              spinnerCropsMap = new HashMap<Integer, String>();
                              for ( int i =0; i < crops.size(); i++ ){
                                    spinnerCropsMap.put(i, String.valueOf(crops.get(i).getId()));
                                    cropSpinnerArray[i] = crops.get(i).getName();
                              }

                              setupArrayAdapter(cropSpinnerArray, spn_crops);

                        } catch (Exception e) {
                              Log.e(TAG, "onNext:  " + e.getMessage());
                        }
                  }
            });
      }

      private boolean searchSpinnerArray(String item){
            return Arrays.asList(cropSpinnerArray)
                    .contains(item);
      }

      private int getSpinnerArrayValuePosition(String[] spinnerArray, String item){
            int position = 0;

            try {
                  for (int i = 0; i < spinnerArray.length; i++) {
                        if (spinnerArray[i].equals(item)) {
                              position = i;
                        }
                  }
            }catch (Exception e){
                  for (int i = 0; i < spinnerArray.length-1; i++) {
                        if (spinnerArray[i].equals(item)) {
                              position = i;
                        }
                  }
            }

            return position;
      }

      private void fetchMapsFromDatabase(){
            mainViewModel.getMaps().observe(getActivity(), mapModelList -> {
                  mapSpinnerArray = new String[mapModelList.size()];
                  try {

                        mapSpinnerHashMap = new HashMap<Integer, String>();
                        for ( int i =0; i < mapModelList.size(); i++ ){
                              mapSpinnerHashMap.put(i, String.valueOf(mapModelList.get(i).getId()));
                              mapSpinnerArray[i] = mapModelList.get(i).getName();
                        }

                        setupArrayAdapter(mapSpinnerArray, spn_maps);
                  } catch (Exception e) {
                        Log.e(TAG, "onNext:  " + e.getMessage());
                  }
            });
      }

      private void setupArrayAdapter(String[] stringArray, AppCompatSpinner spinner){
            ArrayAdapter<String> array = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_item, stringArray);
            array.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(array);
      }


      public interface OnEditDialogFragmentListener{
            void onSave();
      }
}
