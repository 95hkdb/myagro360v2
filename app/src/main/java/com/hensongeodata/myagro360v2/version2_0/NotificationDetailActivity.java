package com.hensongeodata.myagro360v2.version2_0;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.helper.Tools;
import com.hensongeodata.myagro360v2.view.MainHomePageActivity;
import com.squareup.picasso.Picasso;


public class NotificationDetailActivity extends AppCompatActivity {

      private TextView titleTextView;
      private TextView contentTextView;
      private ImageView imageView;
      private TextView toolbarTitleTextView;
      private Button readMoreButton;
      private String title;
      private MapDatabase mapDatabase;


      @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_detail);

        titleTextView = findViewById(R.id.notification_detail_title);
        contentTextView = findViewById(R.id.notification_detail_content);
        imageView = findViewById(R.id.notification_detail_image);
        toolbarTitleTextView = findViewById(R.id.toolbar_title);
        readMoreButton  = findViewById(R.id.read_more_button);
        mapDatabase = MapDatabase.getInstance(this);

            initToolbar();

            if (getIntent() != null){

              long id = getIntent().getLongExtra("id",0);
              title = getIntent().getStringExtra("title");
              String content = getIntent().getStringExtra("content");
              String image = getIntent().getStringExtra("image");
              String link = getIntent().getStringExtra("link");
              boolean  hasImage = getIntent().getBooleanExtra("has_image", false);
              boolean  hasLink = getIntent().getBooleanExtra("has_link", false);

              titleTextView.setText(title);
              contentTextView.setText(content);
              toolbarTitleTextView.setText(title);

              if (hasImage){
                    Picasso.get().load(image).into( imageView);
              }else {
                    imageView.setImageResource((R.drawable.no_image_available));
              }

              if (hasLink){
                    readMoreButton.setVisibility(View.VISIBLE);

                    readMoreButton.setOnClickListener(v -> {
                          Intent i = new Intent(Intent.ACTION_VIEW);
                          i.setData(Uri.parse(link));
                          startActivity(i);
                    });
              }

                  AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                              mapDatabase.notificationDaoAccess().setReadNotification(id);
                        }
                  });
        }
    }

    private void initToolbar() {
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_search, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.grey_60));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
//            finish();
              startActivity(new Intent(getApplicationContext(), MainHomePageActivity.class));

        } else {
//            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

      @Override
      public void onBackPressed() {
//            super.onBackPressed();

//            startActivity(new Intent(getApplicationContext(), MainHomePageActivity.class));

//            Log.d("CDA", "onBackPressed Called");
//            Intent setIntent = new Intent(Intent.ACTION_MAIN);
//            setIntent.addCategory(Intent.CATEGORY_HOME);
//            setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(setIntent);

            moveTaskToBack(true);
      }
}
