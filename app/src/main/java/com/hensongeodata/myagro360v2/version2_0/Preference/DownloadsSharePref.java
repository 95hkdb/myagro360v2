package com.hensongeodata.myagro360v2.version2_0.Preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class DownloadsSharePref {

    private static DownloadsSharePref mInstance;
    private static Context sharedContext;

    private static final String PREF_PLOTS_DOWNLOADED     = "plots_downloaded";
    private static final String PREF_SECONDARY_USER_PLOTS_DOWNLOADED     = "secondary_user_plots_downloaded";
    private static final String PREF_MAPS_DOWNLOADED      = "maps_downloaded";
    private static final String PREF_SCOUTS_DOWNLOADED  = "scouts_downloaded";
    private static final String PREF_KB_DOWNLOADED             = "kb_downloaded";


    public DownloadsSharePref(Context context) {
        sharedContext = context;
    }

    public static synchronized DownloadsSharePref getInstance(Context context){
        if (mInstance == null) {
            mInstance = new DownloadsSharePref(context);
        }
        return mInstance;
    }

    public static void setSecondaryUserPlotsDownloadedPreference(Context context, boolean downloaded){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREF_SECONDARY_USER_PLOTS_DOWNLOADED ,  downloaded);
        editor.apply();
    }

    public static boolean isSecondaryUserPlotsDownloadPreferenceAvailable(Context context){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.contains(PREF_SECONDARY_USER_PLOTS_DOWNLOADED);
    }

    public static boolean getSecondaryUserPlotsDownloadPreference(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PREF_SECONDARY_USER_PLOTS_DOWNLOADED, false);
    }

    public static void clearSecondaryUserPlotsDownloadPreference(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PREF_SECONDARY_USER_PLOTS_DOWNLOADED);
        editor.apply();
    }

    public static void setPlotsDownloadedPreference(Context context, boolean downloaded){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREF_PLOTS_DOWNLOADED ,  downloaded);
        editor.apply();
    }

    public static boolean isPlotsDownloadPreferenceAvailable(Context context){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.contains(PREF_PLOTS_DOWNLOADED);
    }

    public static boolean getPlotsDownloadedPreference(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PREF_PLOTS_DOWNLOADED, false);
    }

    public static void clearPlotsDownloadedPreference(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PREF_PLOTS_DOWNLOADED);
        editor.apply();
    }


    public static void setMapsDownloadedPreference(Context context, boolean downloaded){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREF_MAPS_DOWNLOADED ,  downloaded);
        editor.apply();
    }

    public static boolean isMapsDownloadPreferenceAvailable(Context context){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.contains(PREF_MAPS_DOWNLOADED);
    }

    public static boolean getMapsDownloadedPreference(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PREF_MAPS_DOWNLOADED, false);
    }

    public static void clearMapsDownloadedPreference(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PREF_MAPS_DOWNLOADED);
        editor.apply();
    }


    public static void setScoutsDownloadedPreference(Context context, boolean downloaded){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREF_SCOUTS_DOWNLOADED ,  downloaded);
        editor.apply();
    }

    public static boolean isScoutsDownloadPreferenceAvailable(Context context){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.contains(PREF_SCOUTS_DOWNLOADED);
    }

    public static boolean getScoutsDownloadedPreference(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PREF_SCOUTS_DOWNLOADED, false);
    }

    public static void clearScoutsDownloadedPreference(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PREF_SCOUTS_DOWNLOADED);
        editor.apply();
    }


    public static void setKBDownloadedPreference(Context context, boolean downloaded){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PREF_KB_DOWNLOADED ,  downloaded);
        editor.apply();
    }

    public static boolean isKBDownloadPreferenceAvailable(Context context){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.contains(PREF_KB_DOWNLOADED);
    }

    public static boolean getKBDownloadedPreference(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PREF_KB_DOWNLOADED, false);
    }

    public static void clearKBDownloadedPreference(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PREF_KB_DOWNLOADED);
        editor.apply();
    }

}

