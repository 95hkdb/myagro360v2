package com.hensongeodata.myagro360v2.version2_0.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity( tableName = Member.TABLE_NAME)
public class Member {

      public static final String COLUMN_ID = "id";
      public static final String COLUMN_MEMBER_ID = "member_id";
      public static final String COLUMN_FIRST_NAME = "first_name";
      public static final String COLUMN_LAST_NAME = "last_name";
      public static final String COLUMN_EMAIL = "email";
      public static final String COLUMN_ORG_ID= "org_id";

      public static final String TABLE_NAME = "members";

      @PrimaryKey(autoGenerate = true)
      @ColumnInfo(name = COLUMN_MEMBER_ID)
      private long memberId;

      @SerializedName("id")
      @ColumnInfo(name = COLUMN_ID)
      private String id;

      @SerializedName("firstname")
      @ColumnInfo(name = COLUMN_FIRST_NAME)
      private String firstName;

      @SerializedName("lastname")
      @ColumnInfo(name = COLUMN_LAST_NAME)
      private String lastName;

      @ColumnInfo(name = COLUMN_EMAIL)
      private String email;

      @ColumnInfo(name = COLUMN_ORG_ID)
      private String orgId;


      public long getMemberId() {
            return memberId;
      }

      public void setMemberId(long memberId) {
            this.memberId = memberId;
      }

      public String getId() {
            return id;
      }

      public void setId(String id) {
            this.id = id;
      }

      public String getFirstName() {
            return firstName;
      }

      public void setFirstName(String firstName) {
            this.firstName = firstName;
      }

      public String getLastName() {
            return lastName;
      }

      public void setLastName(String lastName) {
            this.lastName = lastName;
      }

      public String getEmail() {
            return email;
      }

      public void setEmail(String email) {
            this.email = email;
      }

      public String getOrgId() {
            return orgId;
      }

      public void setOrgId(String orgId) {
            this.orgId = orgId;
      }
}
