package com.hensongeodata.myagro360v2.version2_0.sync.scouts.downloads;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Events.ScanProgressEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.ScoutProgressEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.ScoutsDownloadCompleted;
import com.hensongeodata.myagro360v2.version2_0.Model.ScanModel;
import com.hensongeodata.myagro360v2.version2_0.Model.ScoutModel;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScoutsDownloadTask {

      private static final String TAG = ScoutsDownloadTask.class.getSimpleName();
      private static MapDatabase mapDatabase;
      private static final long[] DEFAULT_VIBRATION_PATTERN =  {0, 250, 250, 250};
      protected static MyApplication myApplication;
      private static final int MAPS_NOTIFICATION_ID = 1012;
      private static int SCANS_NOTIFICATION_ID = 1030;


      public static void DownloadScouts(Context context){

            String channelName = "syncScouts";
            String channelId = "DBScoutsSyncChannelId";

            AgroWebAPI AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);
            mapDatabase = MapDatabase.getInstance(context);

//            AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            Log.i(TAG, "Sync Scouts :  entered" );

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            int importance = NotificationManager.IMPORTANCE_LOW;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.drawable.download)
                    .setContentTitle("Downloading Scans...")
                    .setContentText("Download in progress");


            AgroWebAPI finalAGRO_WEB_API = AGRO_WEB_API;
            AppExecutors.getInstance().getDiskIO().execute(() -> {

                  String userId = "";

                  if (SharePrefManager.isSecondaryUserAvailable(context)){
                        userId = SharePrefManager.getSecondaryUserPref(context);
                  }else {
                        userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
                  }

                  String orgId   = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

                  try{

                        if (mapDatabase.scoutDaoAccess().scoutCount(orgId) != 0) {
                              Log.i(TAG, "SyncScouts:  Scouts COUNT" );
                              mapDatabase.scoutDaoAccess().deleteScouts(orgId);
                        }

                        if (mapDatabase.scanDaoAccess().scoutCount(orgId) != 0) {
                              Log.i(TAG, "SyncScouts:  Scouts COUNT" );
                              mapDatabase.scanDaoAccess().deleteScans(orgId);
                        }

                        String finalUserId = userId;
                        finalAGRO_WEB_API.getUserScouts(orgId, userId)
                                .enqueue(new Callback<JsonElement>() {
                                      @SuppressLint("NewApi")
                                      @Override
                                      public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                                            int progressCounter = 0;
                                            int scanProgressCounter = 0;

                                            if (response.body() != null) {

                                                  Log.i(TAG, "Get User Scouts " + response.body().toString());

                                                  if( response.body().isJsonObject()){

                                                        JsonArray bodyJsonArray =  response.body().getAsJsonObject().
                                                                getAsJsonArray("scouts");

                                                        for( int i = 0; i <  bodyJsonArray.size(); i++){

                                                              progressCounter++;
                                                              int finalProgressCounter = progressCounter;

                                                              String ScoutCode =  bodyJsonArray.get(i).getAsJsonObject().get("ScoutCode").getAsString();
                                                              String ScoutID =  bodyJsonArray.get(i).getAsJsonObject().get("ScoutID").getAsString();
                                                              JsonArray scansJsonArray =  bodyJsonArray.get(i).getAsJsonObject().getAsJsonArray("scans");

                                                              ScoutModel scoutModel = new ScoutModel();
                                                              scoutModel.setOrgId(orgId);
                                                              scoutModel.setUserId(finalUserId);
//                                                        scoutModel.setPlotId(plotId);
                                                              scoutModel.setBatchId(ScoutCode);
                                                              Log.i(TAG, "run:  Scout Model " + ScoutCode);
                                                              scoutModel.setDateCreated(System.currentTimeMillis());

                                                              AppExecutors.getInstance().getDiskIO().execute(() -> {
                                                                    mapDatabase.scoutDaoAccess().insert(scoutModel);

                                                                    Log.i(TAG, "onResponse: Scouts Download json Array size"  + bodyJsonArray.size() );
                                                                    Log.i(TAG, "onResponse: Scouts Download DB Size "  + mapDatabase.scoutDaoAccess().scoutCountyUser(orgId, finalUserId) );

                                                                    EventBus.getDefault().post(new ScoutProgressEvent(finalProgressCounter, bodyJsonArray.size()));

                                                                    if (bodyJsonArray.size() == mapDatabase.scoutDaoAccess().scoutCountyUser(orgId, finalUserId)){
                                                                          EventBus.getDefault().post(new ScoutsDownloadCompleted(false));
                                                                    }
//                                                                    mBuilder.setProgress(bodyJsonArray.size(), finalProgressCounter, false);
//                                                                    notificationManager.notify(SCANS_NOTIFICATION_ID, mBuilder.build());
                                                              });

                                                              for(int j =0;  j< scansJsonArray.size(); j++){

                                                                    scanProgressCounter++;

                                                                    String name =  scansJsonArray.get(j).getAsJsonObject().get("Labels").getAsString();
                                                                    String dateCreated = scansJsonArray.get(j).getAsJsonObject().get("Created").getAsString();
                                                                    String detected = scansJsonArray.get(j).getAsJsonObject().get("Detected").getAsString();

//                                                        String image =  scansJsonArray.get(j).getAsJsonObject().get("image").getAsString();
//                                                        String scoutId =  scansJsonArray.get(j).getAsJsonObject().get("scout_id").getAsString();
                                                                    String strippedName = "";

                                                                    ScanModel scanModel = new ScanModel();

                                                                    if (name.toLowerCase().contains("--label not found--")){
                                                                          String firstStrip = name.replace("--","");
                                                                          String secondStrip = firstStrip.replace("[", "").replace("]","").replace("\"", "");
                                                                          strippedName = secondStrip.replaceAll(", $", "");
//                                                                    string.Replace("\"", "")
                                                                          scanModel.setPestFound(false);

                                                                    }else {
                                                                          String firstStrip = name.replace("[","");
                                                                          String secondStrip = firstStrip.replace("]", "").replace("\"", "");
                                                                          strippedName = secondStrip.replaceAll(", $", "");
                                                                    }

                                                                    Log.i(TAG, "onResponse:  Scan Response  Name : " + name);
                                                                    Log.i(TAG, "onResponse:  Scan Response  Name : " + strippedName);

                                                                    try{
                                                                          scanModel.setScoutId(Long.valueOf(ScoutCode));
                                                                          scanModel.setItemId((ScoutCode));
                                                                    }catch (Exception e){
                                                                          Log.e(TAG, "onResponse:  ScoutsDownloadTask " + e.getMessage()  );
                                                                    }

                                                                    scanModel.setMessage(strippedName);
                                                                    scanModel.setPestIdentified(strippedName);
//                                                        scanModel.setImagePath(image);
                                                                    scanModel.setSynced(true);
                                                                    scanModel.setOrgId(orgId);
                                                                    scanModel.setUserId(finalUserId);
                                                                    scanModel.setPestFound(Boolean.valueOf(detected));
//                                                                    scanModel.setDateCreated(System.currentTimeMillis());
                                                                    scanModel.setDateCreated(HelperClass.convertDateTimeToMilliseconds(dateCreated));
                                                                    scanModel.setStatus(ScanModel.STATE_PROCESSED);

                                                                    Log.i(TAG, "onResponse:  Scan Response  scout code : " + ScoutCode);
                                                                    Log.i(TAG, "onResponse:  Scan Response  scout id : " + ScoutID);

                                                                    EventBus.getDefault().post(new ScanProgressEvent(scanProgressCounter, scansJsonArray.size()));

                                                                    AppExecutors.getInstance().getDiskIO().execute(() -> {
                                                                          mapDatabase.scanDaoAccess().insert(scanModel);
                                                                    });
                                                              }
                                                        }

                                                  }else {
                                                        EventBus.getDefault().post(new ScoutsDownloadCompleted(false));
                                                  }
                                            }
                                      }

                                      @Override
                                      public void onFailure(Call<JsonElement> call, Throwable t) {
                                            Log.i(TAG, "Scan Response:  Error" + t.getMessage());
                                            EventBus.getDefault().post(new ScoutsDownloadCompleted(true));
                                      }
                                });

                  }catch (Exception e){
                        Log.e(TAG, "SyncScouts Error : " + e.getMessage() );
                  }
            });
      }

      public static void DownloadScans(Context context){

            String channelName = "syncKB";
            String channelId = "DBKBSyncChannelId";

           AgroWebAPI AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            Log.i(TAG, "SyncKnowledgebase:  entered" );

//            if (myApplication.hasNetworkConnection(context)) {

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            Log.i(TAG, "SyncKnowledgebase:  entered Network Constraint" );


            int importance = NotificationManager.IMPORTANCE_LOW;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.drawable.download)
                    .setContentTitle("Downloading Scans...")
                    .setContentText("Download in progress");


            AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                  @Override
                  public void run() {

                        mapDatabase = MapDatabase.getInstance(context);

                        String userId = "";

                        if (SharePrefManager.isSecondaryUserAvailable(context)){
                              userId = SharePrefManager.getSecondaryUserPref(context);
                        }else {
                              userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
                        }

                        String orgId   = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

                        if (mapDatabase.scanDaoAccess().scanCount(orgId) != 0) {
                              Log.i(TAG, "SyncScans:  Scans COUNT" );
                              mapDatabase.scanDaoAccess().deleteScans(orgId);
                        }

                        String finalUserId = userId;
                        AGRO_WEB_API.getUserScans(orgId, userId)
                                .enqueue(new Callback<JsonElement>() {
                                      @SuppressLint("NewApi")
                                      @Override
                                      public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                                            int progressCounter = 0;

                                            if (response.body() != null) {

                                                  JsonArray bodyJsonArray =  response.body().getAsJsonObject().
                                                          getAsJsonArray("scans");

//
                                                  for( int i = 0; i <  bodyJsonArray.size(); i++){

                                                        progressCounter++;
                                                        int finalProgressCounter = progressCounter;

                                                        String name =  bodyJsonArray.get(i).getAsJsonObject().get("name").getAsString();
                                                        String image =  bodyJsonArray.get(i).getAsJsonObject().get("image").getAsString();
                                                        String scoutId =  bodyJsonArray.get(i).getAsJsonObject().get("scout_id").getAsString();
                                                        String strippedName = "";

                                                        ScanModel scanModel = new ScanModel();

                                                        if (name.toLowerCase().contains("--label not found--")){
                                                              String firstStrip = name.replace("--","");
                                                              String secondStrip = firstStrip.replace("[", "").replace("]","").replace("\"", "");
                                                              strippedName = secondStrip.replaceAll(", $", "");
//                                                                    string.Replace("\"", "")
                                                              scanModel.setPestFound(false);
                                                        }else {
                                                              String firstStrip = name.replace("[","");
                                                              String secondStrip = firstStrip.replace("]", "").replace("\"", "");
                                                              strippedName = secondStrip.replaceAll(", $", "");
                                                              scanModel.setPestFound(true);
                                                        }

                                                        Log.i(TAG, "onResponse:  Scan Response  Name : " + name);
                                                        Log.i(TAG, "onResponse:  Scan Response  Name : " + strippedName);
                                                        Log.i(TAG, "onResponse:  Scan Response  Name : " + image);
//
                                                        scanModel.setMessage(strippedName);
                                                        scanModel.setPestIdentified(strippedName);
                                                        scanModel.setImagePath(image);
                                                        scanModel.setSynced(true);
                                                        scanModel.setOrgId(orgId);
                                                        scanModel.setUserId(finalUserId);
                                                        scanModel.setScoutId(Long.valueOf(scoutId));
                                                        scanModel.setItemId((scoutId));
                                                        scanModel.setDateCreated(System.currentTimeMillis());
                                                        scanModel.setStatus(ScanModel.STATE_PROCESSED);

                                                        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                                              @Override
                                                              public void run() {
                                                                    mapDatabase.scanDaoAccess().insert(scanModel);
                                                                    mBuilder.setProgress(bodyJsonArray.size(), finalProgressCounter, false);

                                                                    notificationManager.notify(SCANS_NOTIFICATION_ID, mBuilder.build());
                                                              }
                                                        });
                                                  }
                                            }
                                      }

                                      @Override
                                      public void onFailure(Call<JsonElement> call, Throwable t) {
                                            Log.i(TAG, "Scan Response:  Error" + t.getMessage());
                                      }
                                });
                  }
            });
//            }

      }


      public static void DownloadScout(Context context){

            String channelName = "syncMapsChannel";
            String channelId         = "syncMapUpdatesChannelId";

            AgroWebAPI AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);
             mapDatabase = MapDatabase.getInstance(context);

            String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
            String orgId   = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

            Log.i(TAG, "onResponse:  Sync Map" + userId);



      }

      public static void showNotification(int notificationId, String channelId,
                                          String channelName, Context context, String body, NotificationCompat.Style notificationStyle, Intent intent) {

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            int importance = NotificationManager.IMPORTANCE_HIGH;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setVibrate(DEFAULT_VIBRATION_PATTERN)
                    .setContentText(body)
                    .setStyle(notificationStyle)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setNumber(1)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                    .setAutoCancel(true);

            if (intent != null){
                  TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                  stackBuilder.addNextIntent(intent);
                  PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                          0,
                          PendingIntent.FLAG_UPDATE_CURRENT
                  );
                  mBuilder.setContentIntent(resultPendingIntent);
            }

            notificationManager.notify(notificationId, mBuilder.build());
      }

}
