package com.hensongeodata.myagro360v2.version2_0.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.hensongeodata.myagro360v2.R;

public class MySpinnerAdapter extends BaseAdapter {
      @Override
      public int getCount() {
            return 0;
      }

      @Override
      public Object getItem(int position) {
            return null;
      }

      @Override
      public long getItemId(int position) {
            return 0;
      }

      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
            Context context = parent.getContext();

            View view = LayoutInflater.from(context).inflate(R.layout.simple_spinner_dropdown_item, null);

            view.setTag("");

            return view;
      }
}
