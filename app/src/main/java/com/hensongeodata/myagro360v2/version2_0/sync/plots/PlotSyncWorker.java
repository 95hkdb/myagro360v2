package com.hensongeodata.myagro360v2.version2_0.sync.plots;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class PlotSyncWorker extends Worker {
      private static final String TAG = PlotSyncWorker.class.getSimpleName();

      public PlotSyncWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
            super(context, workerParams);
      }

      @NonNull
      @Override
      public Result doWork() {

//            try {
//                  PlotSyncTask.syncAllPlots(getApplicationContext());
////                  PlotSyncTask.fetchAllPlots(getApplicationContext());
//
//            } catch (JSONException e) {
//                  Log.e(TAG, "doWork:  Sync All Plots" + e.getMessage() );
//                  e.printStackTrace();
//                  return Result.retry();
//            }
//            return Result.success();
//      }

              try {
                  PlotSyncTask.syncAllPlots(getApplicationContext());
      //            PlotSyncTask.fetchAllPlots(getApplicationContext());
                    Log.i(TAG, "doWork:  sync all plots" );

            } catch (Exception e) {
                  Log.e(TAG, "doWork:  Sync All Plots" + e.getMessage() );
                  e.printStackTrace();
                  return Result.retry();
            }
                  return Result.success();
      }

}
