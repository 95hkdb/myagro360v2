package com.hensongeodata.myagro360v2.version2_0.Fragment.Map;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hensongeodata.myagro360v2.Interface.HistoryTabListener;
import com.hensongeodata.myagro360v2.Interface.Refresh;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Adapter.MapHistoryRV;
import com.hensongeodata.myagro360v2.version2_0.Events.MapSortButtonClicked;
import com.hensongeodata.myagro360v2.version2_0.Events.MapsDownloadCompleted;
import com.hensongeodata.myagro360v2.version2_0.Model.Member;
import com.hensongeodata.myagro360v2.version2_0.Preference.DownloadsSharePref;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;
import com.hensongeodata.myagro360v2.version2_0.sync.map.MapSyncUtil;
import com.hensongeodata.myagro360v2.version2_0.sync.map.downloads.MapsDownloadIntentService;
import com.hensongeodata.myagro360v2.version2_0.sync.plots.UpdatePlotSizesIntentService;
import com.hensongeodata.myagro360v2.view.ScoutAutomated;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class MappingFragment extends Fragment implements HistoryTabListener,Refresh{

      private static final String TAG = MappingFragment.class.getSimpleName() ;
      private Button mAddButton;
      private RecyclerView recyclerView;
      private FloatingActionButton fabButton;
      protected DatabaseHelper databaseHelper;
      private MapDatabase mapDatabase;
      private MainViewModel mainViewModel;
      protected MapHistoryRV adapter;
      ArrayList<HashMap<String,String>>items;
      private LinearLayout linearLayout;
      private TextView numberOfMapsTextView;
      private TextView totalAcreageTextView;
      private LinearLayout mapsInfo;
      private View rootView;
      private ArrayList<InstructionScout> itemsVisible;
      private static final String SECTION_ID = "section_id";
      private int sectionId;
      private OnAddMapButtonListener mListener;
      private SwipeRefreshLayout swipe_refresh;
      private AppCompatSpinner spn_staff;
      private HashMap<Integer, String> staffSpinnerHashMap;
      private Dialog progressDialog;
      private MyApplication myApplication;
      private ImageView sortImageButton;
      private TextView     accountNameTextView;
      private Context mContext;


      public MappingFragment() {}

      public static MappingFragment newInstance(int sectionId) {
            MappingFragment fragment = new MappingFragment();
            Bundle args = new Bundle();
            args.putInt(SECTION_ID, sectionId);
            fragment.setArguments(args);
            return fragment;
      }

      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

//            databaseHelper=new DatabaseHelper(getContext());


            if (getArguments() != null) {
                  sectionId = getArguments().getInt(SECTION_ID);
            }

      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view =  inflater.inflate(R.layout.fragment_mapping, container, false);
            rootView = view;
            mapDatabase = MapDatabase.getInstance(getContext());
            mAddButton  =  view.findViewById(R.id.add_new_map_button);
            recyclerView = view.findViewById(R.id.mapping_history_recyclerview);
            swipe_refresh =  view.findViewById(R.id.swipe_refresh_layout);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
            linearLayout = view.findViewById(R.id.no_mapping_linear_layout);
            fabButton = view.findViewById(R.id.fab_add);
            numberOfMapsTextView = view.findViewById(R.id.number_of_maps);
            mapsInfo = view.findViewById(R.id.maps_info);
            totalAcreageTextView = view.findViewById(R.id.total_acreage);
            sortImageButton          = view.findViewById(R.id.sort_button);
            spn_staff                        = view.findViewById(R.id.spn_staff);
            myApplication  = new MyApplication();
            progressDialog = new Dialog(getContext());
            mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            mContext = getContext();
//            accountNameTextView = view.findViewById(R.id.account_name);

            // on swipe list
            swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                  @Override
                  public void onRefresh() {
                        pullAndRefresh();
                  }
            });

            fabButton.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        onButtonPressed();
                  }
            });
            mAddButton.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        onButtonPressed();
                  }
            });
//            init();
            calculateTotalAcreage();
//            populateMap();
//            fetchMembersFromDB();
            loadMaps();

            if (myApplication.hasNetworkConnection(getContext())){

                  DownloadsSharePref.clearMapsDownloadedPreference(getContext());

//                  if (!DownloadsSharePref.isMapsDownloadPreferenceAvailable(getContext())
//                          || !DownloadsSharePref.getMapsDownloadedPreference(getContext())){
//                        showMapsDownloadDialog();
//                  }
            }

            sortImageButton.setOnClickListener(v -> EventBus.getDefault().post(new MapSortButtonClicked()));

            Log.i(TAG, "onCreateView:  Map Preference " + SharePrefManager.getMapSortPreference(getContext()));

            String userId = "";

            if (SharePrefManager.isSecondaryUserAvailable(getContext())){
                  userId = SharePrefManager.getSecondaryUserPref(getContext());
            }else {
                  userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
            }

//            accountNameTextView.setText(userId);


            return view;
      }

      private void openMapping() {
            Intent mappingIntent = new Intent(getContext(), ScoutAutomated.class);
            mappingIntent.putExtra("mode", 0);
            startActivity(mappingIntent);
      }

      public void onButtonPressed() {
            if (mListener != null) {
                  mListener.onAddMapButtonClicked();
            }
      }

      public void onMoreClicked(String value, String sectionId){
            if (mListener != null) {
                  mListener.onMoreIconClicked(value, sectionId);
            }
      }

      public void onShareButtonClicked(String sectionId){
            if (mListener != null) {
                  mListener.shareButtonClicked( sectionId);
            }
      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof OnAddMapButtonListener) {
                  mListener = (OnAddMapButtonListener) context;
            } else {
                  throw new RuntimeException(context.toString()
                          + " must implement OnPlotActivitiesFragmentListener");
            }
      }

      @Override
      public void onDetach() {
            super.onDetach();
            mListener = null;
      }

      @Override
      public void onSelected(String item) { }

      @Override
      public void reload(String scout_id) { }

      @Override
      public void moreClicked(String value, String sectionId) { }

      @Override
      public void shareButtonClicked(String section_id) { }

      @Override
      public void onSyncButtonClicked() {}

      public interface OnAddMapButtonListener {
            void onAddMapButtonClicked();

            void onMoreIconClicked(String value, String sectionId);

            void shareButtonClicked(String sectionId);
      }

      private void loadMaps() {
            if (mainViewModel != null){
                  mainViewModel = null;
                  mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            }

            if (SharePrefManager.isMapSortPreferenceAvailable(getContext())){

                  switch (SharePrefManager.getMapSortPreference(getContext())){

                        case SharePrefManager.SORT_BY_ALL_MAP:
                              populateMapsByDefault();
                              break;

                        case SharePrefManager.SORT_BY_RECENT_MAP:
                              populateMapsByRecent();
                              break;

                        case SharePrefManager.SORT_BY_SIZE_HIGH_TO_LOW_MAP:
                              populateMapsBySortHighToLow();
                              break;

                        case SharePrefManager.SORT_BY_SIZE_LOW_TO_HIGH_MAP:
                              populateMapsBySortLowToHigh();
                              break;

                        default:
                              populateMapsByDefault();

                  }
            }else {
                  populateMapsByDefault();

            }
      }

      private void populateMapsBySortLowToHigh() {
            setMainViewModel(mainViewModel.getMapsBySizeLowToHigh());
      }

      private void populateMapsBySortHighToLow() {
            setMainViewModel(mainViewModel.getMapsBySizeHighToLow());
      }

      private void populateMapsByRecent() {
            setMainViewModel(mainViewModel.getMapsByRecent());
      }

      private void populateMapsByDefault() {
            setMainViewModel(mainViewModel.getMaps());
      }

      private void setMainViewModel(LiveData<List<MapModel>> viewModel) {

            viewModel.observe(getActivity(), mapModelList -> {

                  if (mapModelList.size() != 0){
                        linearLayout.setVisibility(View.GONE);
                        fabButton.setVisibility(View.VISIBLE);
                        mapsInfo.setVisibility(View.VISIBLE);
                        numberOfMapsTextView.setText(String.valueOf(mapModelList.size()));

                        Log.i(TAG, "onChanged:  Map Size " + mapModelList.size());

                        adapter = new MapHistoryRV(getContext(), mapModelList, MappingFragment.this, new Refresh() {

                              @Override
                              public void reload(String scout_id) {}

                              @Override
                              public void moreClicked(String title, String sectionId) {
                                    onMoreClicked(title, sectionId);
                              }

                              @Override
                              public void shareButtonClicked(String section_id) {
                                    onShareButtonClicked(section_id);
                              }

                              @Override
                              public void onSyncButtonClicked() {
                                    MapSyncUtil.initialize(getActivity());
                              }
                        });

                        recyclerView.setAdapter(adapter);

                  }else if (mapModelList.size() == 0 || mapModelList == null){

                        Log.i(TAG, "onChanged:  Maps Empty " );
                        mapsInfo.setVisibility(View.GONE);
                        linearLayout.setVisibility(View.VISIBLE);
                        fabButton.setVisibility(View.GONE);
                  }
            });

      }


      private void populateMap(){

            mainViewModel.getMaps().observe(getActivity(), new Observer<List<MapModel>>() {
                  @SuppressLint("RestrictedApi")
                  @Override
                  public void onChanged(List<MapModel> mapModelList) {

                        if (mapModelList.size() != 0){
                              linearLayout.setVisibility(View.GONE);
                              fabButton.setVisibility(View.VISIBLE);
                              mapsInfo.setVisibility(View.VISIBLE);
                              numberOfMapsTextView.setText(String.valueOf(mapModelList.size()));

                              Log.i(TAG, "onChanged:  Map Size " + mapModelList.size());

                              adapter = new MapHistoryRV(getContext(), mapModelList, MappingFragment.this, new Refresh() {

                                    @Override
                                    public void reload(String scout_id) {}

                                    @Override
                                    public void moreClicked(String title, String sectionId) {
                                          onMoreClicked(title, sectionId);
                                    }

                                    @Override
                                    public void shareButtonClicked(String section_id) {
                                          onShareButtonClicked(section_id);
                                    }

                                    @Override
                                    public void onSyncButtonClicked() {
                                          MapSyncUtil.initialize(getActivity());
                                    }
                              });

                              recyclerView.setAdapter(adapter);

                        }else if (mapModelList.size() == 0 || mapModelList == null){

                              Log.i(TAG, "onChanged:  Maps Empty " );
                              mapsInfo.setVisibility(View.GONE);
                              linearLayout.setVisibility(View.VISIBLE);
                              fabButton.setVisibility(View.GONE);
                        }
                  }
            });
      }

      private void calculateTotalAcreage(){

            String userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
            String orgId = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);
            AtomicInteger totalSize = new AtomicInteger();


            AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                  @Override
                  public void run() {
//                        totalSize.set(mapDatabase.daoAccess().mapSizeSum(userId, orgId));
                        totalSize.set(mapDatabase.daoAccess().mapSizeSum( orgId));
                  }
            });

            new Handler().postDelayed(() -> totalAcreageTextView.setText(String.valueOf(totalSize.get())),2000);

      }

      @Override
      public void onResume() {
            super.onResume();
//            init();
      }

      private void pullAndRefresh() {
            swipeProgress(true);
            new Handler().postDelayed(new Runnable() {
                  @Override
                  public void run() {
                        adapter.notifyDataSetChanged();
                        swipeProgress(false);
                  }
            }, 3000);
      }

      private void swipeProgress(final boolean show) {
            if (!show) {
                  swipe_refresh.setRefreshing(show);
                  return;
            }
            swipe_refresh.post(new Runnable() {
                  @Override
                  public void run() {
                        swipe_refresh.setRefreshing(show);
                  }
            });
      }

      private void fetchMembersFromDB(){

            mainViewModel.getMembers().observe(getActivity(), new androidx.lifecycle.Observer<List<Member>>() {
                  @Override
                  public void onChanged(List<Member> members) {

                        String[] spinnerArray = new String[members.size()];

                        Log.i(TAG, "PlotsFragment " + members.size());

                        try {

                              staffSpinnerHashMap = new HashMap<Integer, String>();
                              for ( int i =0; i < members.size(); i++ ){
                                    staffSpinnerHashMap.put(i, String.valueOf(members.get(i).getId()));
                                    spinnerArray[i] = members.get(i).getFirstName() + " " + members.get(i).getLastName() + " ( " + members.get(i).getEmail();
                                    Log.i(TAG, "onChanged:  " + members.get(i).getId());
                              }
                              HelperClass.setupArrayAdapter(getActivity(),spinnerArray, spn_staff);
//                              setupArrayAdapter(spinnerArray, spn_staff);
                        } catch (Exception e) {
                              Log.e(TAG, "onNext:  " + e.getMessage());
                        }
                  }
            });

      }

      private void showMapsDownloadDialog() {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle("Download maps?");
            builder.setMessage(R.string.download_maps_string);
            builder.setPositiveButton("OK, Download!", (dialogInterface, i) -> {
                  Intent intentToSyncImmediately = new Intent(mContext, MapsDownloadIntentService.class);
                  mContext.startService(intentToSyncImmediately);

                  new Handler().postDelayed(() -> showProgressDialog(), 10);
            });
            builder.setNegativeButton("NO", null);
            builder.show();
      }

      private void showProgressDialog() {
            progressDialog = new Dialog(getContext());
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            progressDialog.setContentView(R.layout.dialog_maps_progress);
            progressDialog.setCancelable(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(progressDialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            progressDialog.show();
            progressDialog.getWindow().setAttributes(lp);
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onMapsFragmentDownloadCompleted(MapsDownloadCompleted event) {
            progressDialog.dismiss();

            if (!event.isFailed()){

                  if (DownloadsSharePref.isMapsDownloadPreferenceAvailable(getContext())){
                        DownloadsSharePref.clearMapsDownloadedPreference(getContext());
                  }
                  DownloadsSharePref.setMapsDownloadedPreference(getContext(), true);

                  if((DownloadsSharePref.isPlotsDownloadPreferenceAvailable(getContext())  )
                          && (DownloadsSharePref.isMapsDownloadPreferenceAvailable(getContext()))){
                        startUpdatePlotsService();
                  }

            }else if (event.isFailed()) {
                  Toast.makeText(getContext(), "Maps failed to download!", Toast.LENGTH_SHORT).show();
            }

      }

      private void startUpdatePlotsService() {
            Intent intentToSyncImmediately = new Intent(getContext(), UpdatePlotSizesIntentService.class);
            getContext().startService(intentToSyncImmediately);

            Toast.makeText(getContext(), "Plots Sizes updating in the background", Toast.LENGTH_SHORT).show();
      }

      @Override
      public void onStart() {
            super.onStart();
            EventBus.getDefault().register(this);
      }

      @Override
      public void onStop() {
            EventBus.getDefault().unregister(this);
            super.onStop();
      }



}
