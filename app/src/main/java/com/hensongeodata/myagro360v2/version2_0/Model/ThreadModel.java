package com.hensongeodata.myagro360v2.version2_0.Model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

//@Entity( tableName =  ThreadModel.TABLE_NAME)
public class ThreadModel implements Parcelable {

      public static final String TABLE_NAME = "threads";

      public static final String COLUMN_THREAD_ID= "thread_id";
      public static final String COLUMN_ID = "id";
      public static final String COLUMN_UNIQUE= "unique";
      public static final String COLUMN_TITLE= "title";
      public static final String COLUMN_CONTENT= "content";
      public static final String COLUMN_IMAGE_URL= "image_url";
      public static final String COLUMN_CREATOR_NAME= "creator_name";
      public static final String COLUMN_FILE_NAME = "filename";
      public static final String COLUMN_DATE_CREATED = "date_created";
      private static final String COLUMN_SYNCED = "synced";
      private static final String COLUMN_COMMENTS = "comments";

      @PrimaryKey(autoGenerate = true)
      @ColumnInfo( name = COLUMN_THREAD_ID)
      private long threadId;

      @ColumnInfo( name = COLUMN_ID)
      private String id;

      @ColumnInfo( name = COLUMN_UNIQUE)
      private String unique;

      @ColumnInfo( name = COLUMN_TITLE)
      private String title;

      @ColumnInfo( name = COLUMN_CONTENT)
      private String content;

      @SerializedName("image_url")
      @ColumnInfo( name = COLUMN_IMAGE_URL)
      private String imageUrl;

      @SerializedName("creator_name")
      @ColumnInfo( name = COLUMN_CREATOR_NAME)
      private String creatorName;

      @SerializedName("filename")
      @ColumnInfo( name = COLUMN_FILE_NAME)
      private String fileName;

      @SerializedName("created")
      @ColumnInfo( name = COLUMN_DATE_CREATED)
      private String dateCreated;

      @SerializedName("synced")
      @ColumnInfo( name = COLUMN_SYNCED)
      private boolean synced;

      @SerializedName("comments")
      @ColumnInfo( name = COLUMN_COMMENTS)
      private List<CommentModel> comments;

      public ThreadModel() {

      }

      public long getThreadId() {
            return threadId;
      }

      public void setThreadId(long threadId) {
            this.threadId = threadId;
      }

      public String getId() {
            return id;
      }

      public void setId(String id) {
            this.id = id;
      }

      public String getUnique() {
            return unique;
      }

      public void setUnique(String unique) {
            this.unique = unique;
      }

      public String getTitle() {
            return title;
      }

      public void setTitle(String title) {
            this.title = title;
      }

      public String getContent() {
            return content;
      }

      public void setContent(String content) {
            this.content = content;
      }

      public String getImageUrl() {
            return imageUrl;
      }

      public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
      }

      public String getCreatorName() {
            return creatorName;
      }

      public void setCreatorName(String creatorName) {
            this.creatorName = creatorName;
      }

      public String getFileName() {
            return fileName;
      }

      public void setFileName(String fileName) {
            this.fileName = fileName;
      }

      public String getDateCreated() {
            return dateCreated;
      }

      public void setDateCreated(String dateCreated) {
            this.dateCreated = dateCreated;
      }

      public boolean isSynced() {
            return synced;
      }

      public void setSynced(boolean synced) {
            this.synced = synced;
      }

      public List<CommentModel> getComments() {
            return comments;
      }

      public void setComments(List<CommentModel> comments) {
            this.comments = comments;
      }

      @Override
      public int describeContents() {
            return 0;
      }

      @Override
      public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(this.threadId);
            dest.writeString(this.id);
            dest.writeString(this.unique);
            dest.writeString(this.title);
            dest.writeString(this.content);
            dest.writeString(this.imageUrl);
            dest.writeString(this.creatorName);
            dest.writeString(this.fileName);
            dest.writeString(this.dateCreated);
            dest.writeByte(this.synced ? (byte) 1 : (byte) 0);
            dest.writeList(this.comments);
      }

      protected ThreadModel(Parcel in) {
            this.threadId = in.readLong();
            this.id = in.readString();
            this.unique = in.readString();
            this.title = in.readString();
            this.content = in.readString();
            this.imageUrl = in.readString();
            this.creatorName = in.readString();
            this.fileName = in.readString();
            this.dateCreated = in.readString();
            this.synced = in.readByte() != 0;
            this.comments = new ArrayList<CommentModel>();
            in.readList(this.comments, CommentModel.class.getClassLoader());
      }

      public static final Parcelable.Creator<ThreadModel> CREATOR = new Parcelable.Creator<ThreadModel>() {
            @Override
            public ThreadModel createFromParcel(Parcel source) {
                  return new ThreadModel(source);
            }

            @Override
            public ThreadModel[] newArray(int size) {
                  return new ThreadModel[size];
            }
      };
}
