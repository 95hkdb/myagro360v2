package com.hensongeodata.myagro360v2.version2_0.sync.farm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.response.FarmResponse;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Model.Farm;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FarmSyncTask {

      private static final String TAG = FarmSyncTask.class.getSimpleName();
      private static MapDatabase mapDatabase;
      private static final long[] DEFAULT_VIBRATION_PATTERN =  {0, 250, 250, 250};
      protected static MyApplication myApplication;
      private static AgroWebAPI AGRO_WEB_API;

      public static void showNotification(int notificationId, String channelId,
                                          String channelName, Context context, String body, NotificationCompat.Style notificationStyle, Intent intent) {

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            int importance = NotificationManager.IMPORTANCE_HIGH;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setVibrate(DEFAULT_VIBRATION_PATTERN)
                    .setContentText(body)
                    .setStyle(notificationStyle)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setNumber(1)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                    .setAutoCancel(true);

            if (intent != null){
                  TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                  stackBuilder.addNextIntent(intent);
                  PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                          0,
                          PendingIntent.FLAG_UPDATE_CURRENT
                  );
                  mBuilder.setContentIntent(resultPendingIntent);
            }

            notificationManager.notify(notificationId, mBuilder.build());
      }

      public synchronized static void pullFarms(Context context){
            int notificationlId = 1008;
            String channelName = "syncFarmsChannel";
            String channelId         = "syncFarmUpdatesChannelId";
            mapDatabase  = MapDatabase.getInstance(context);
            AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
            String orgId   = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

                  // if there is any record of farm then delete all first.
                  if (mapDatabase.farmDaoAccess().fetchAllFarms(userId, orgId).size() != 0) {
                        mapDatabase.farmDaoAccess().deleteFarms();
                  }

                  AGRO_WEB_API.fetchFarmsCall(userId, orgId).enqueue(new Callback<FarmResponse>() {
                        @Override
                        public void onResponse(Call<FarmResponse> call, Response<FarmResponse> response) {

                              Log.d(TAG, "onResponse:  Farm : " + response.code());

                              if (response.isSuccessful()) {

                                    for (Farm farm : response.body().getFarms()) {

                                          Farm farmModel = new Farm();
                                          farmModel.setName(farm.getName());
                                          farmModel.setId(farm.getId());
                                          farmModel.setOrgId(orgId);
                                          farmModel.setUserId(userId);
                                          farm.setSynced(true);

                                          Log.d(TAG, "Farm " + farm.getName());
                                          Log.d(TAG, "Farm " + farm.getId());
                                          Log.d(TAG, "Farm " + farm.getSynced());

                                          AppExecutors.getInstance().getDiskIO().execute(() -> mapDatabase.farmDaoAccess().insert(farmModel));
                                    }

//                                    NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Downloading farms .."));
//                                    showNotification(notificationlId, channelId, channelName, context, "Downloading farms ..", notificationStyle, null);
                              }
                        }

                        @Override
                        public void onFailure(Call<FarmResponse> call, Throwable t) {
                              Log.e(TAG, "onFailure:  " + t.getMessage());
                        }
                  });
            }
//      }

      public synchronized static void pushFarms(Context context){

      }
}
