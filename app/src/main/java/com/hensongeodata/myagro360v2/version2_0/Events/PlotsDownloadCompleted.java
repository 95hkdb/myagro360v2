package com.hensongeodata.myagro360v2.version2_0.Events;

public  class PlotsDownloadCompleted {
      private boolean failed;

      public PlotsDownloadCompleted(boolean failed) {
            this.failed = failed;
      }

      public boolean isFailed() {
            return failed;
      }

      public void setFailed(boolean failed) {
            this.failed = failed;
      }
}
