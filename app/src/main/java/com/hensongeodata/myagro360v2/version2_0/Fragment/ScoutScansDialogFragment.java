//package com.hensongeodata.myagro360.version2_0.Fragment;
//
//
//import android.os.Bundle;
//
//import androidx.fragment.app.Fragment;
//import androidx.recyclerview.widget.RecyclerView;
//
//import android.os.Handler;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.hensongeodata.myagro360.R;
//import com.hensongeodata.myagro360.data.MapDatabase;
//import com.hensongeodata.myagro360.data.viewModels.MainViewModel;
//import com.hensongeodata.myagro360.helper.AppExecutors;
//import com.hensongeodata.myagro360.version2_0.Adapter.ScanHistoryRecyclerViewAdapter;
//import com.hensongeodata.myagro360.version2_0.Listeners.ScansRvListener;
//import com.hensongeodata.myagro360.version2_0.Model.ScanModel;
//
//import java.util.List;
//import java.util.concurrent.atomic.AtomicInteger;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class ScoutScansDialogFragment extends Fragment implements ScansRvListener {
//
//      private static final String TAG = ScoutScansDialogFragment.class.getSimpleName();
//
//      private ScanHistoryRecyclerViewAdapter adapter;
//      private MainViewModel mainViewModel;
//      private MapDatabase mapDatabase;
//      private List<ScanModel> items;
//      protected RecyclerView recyclerView;
//
//
//      public ScoutScansDialogFragment() {
//            // Required empty public constructor
//      }
//
//
//      @Override
//      public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                               Bundle savedInstanceState) {
//            // Inflate the layout for this fragment
//            return inflater.inflate(R.layout.fragment_scout_scans_dialog, container, false);
//      }
//
//      private void getAllScansFromViewModel(){
//
//            mainViewModel.getScoutModelList().observe(getActivity(), scoutModels -> {
//                  items = scoutModels;
//
//                  if ( scoutModels.size() != 0 ){
//
//                        Log.i(TAG, "getAllScansFromViewModel:  " + scoutModels.size());
//
//                        AtomicInteger detectedCounts = new AtomicInteger();
//
////                        AppExecutors.getInstance().getDiskIO().execute(() -> detectedCounts.set(mapDatabase.scanDaoAccess().pestDetectedCount(orgId)));
////                        numberOfScansTextView.setText(String.valueOf(scoutModels.size()));
//
//                        new Handler().postDelayed(new Runnable() {
//                              @Override
//                              public void run() {
////                                    numberOfPestDetectedTextView.setText(String.valueOf(detectedCounts.get()));
//                              }
//                        }, 1000);
//
//                        setAdapter();
//
////                        linearLayout.setVisibility(View.GONE);
////                        fabButton.setVisibility(View.VISIBLE);
////                        configureLayout.setVisibility(View.VISIBLE);
//                  }else {
//
//                        Log.i(TAG, "getAllScansFromViewModel:  " + scoutModels.size());
//
////                        linearLayout.setVisibility(View.VISIBLE);
////                        fabButton.setVisibility(View.GONE);
////                        configureLayout.setVisibility(View.GONE);
//                  }
//            });
//      }
//
//      private void setAdapter(){
//
//            adapter=new ScanHistoryRecyclerViewAdapter(getContext(), items, this) ;
//            adapter.setMode(mode);
//            recyclerView.setAdapter(adapter);
//      }
//
//      @Override
//      public void moreClicked(long id) {
//
//      }
//
//      @Override
//      public void onReadButtonClicked(String name) {
//
//      }
//}
