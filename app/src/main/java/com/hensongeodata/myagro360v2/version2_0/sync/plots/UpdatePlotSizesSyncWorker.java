package com.hensongeodata.myagro360v2.version2_0.sync.plots;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.hensongeodata.myagro360v2.version2_0.sync.DBSyncTask;

public class UpdatePlotSizesSyncWorker extends Worker {
      private static final String TAG = UpdatePlotSizesSyncWorker.class.getSimpleName();

      public UpdatePlotSizesSyncWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
            super(context, workerParams);
      }

      @NonNull
      @Override
      public Result doWork() {
            DBSyncTask.UpdatePlotSizes(getApplicationContext());
            return Result.success();
      }

}
