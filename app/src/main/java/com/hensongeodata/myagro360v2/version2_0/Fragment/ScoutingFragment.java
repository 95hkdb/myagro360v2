package com.hensongeodata.myagro360v2.version2_0.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hensongeodata.myagro360v2.Interface.HistoryTabListener;
import com.hensongeodata.myagro360v2.Interface.Refresh;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.version2_0.Adapter.ScoutHistoryRecyclerViewAdapter;
import com.hensongeodata.myagro360v2.version2_0.Events.ScoutButtonClickedEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.ScoutsDownloadCompleted;
import com.hensongeodata.myagro360v2.version2_0.Listeners.ScoutsRvListener;
import com.hensongeodata.myagro360v2.version2_0.Model.ScoutModel;
import com.hensongeodata.myagro360v2.version2_0.Preference.DownloadsSharePref;
import com.hensongeodata.myagro360v2.version2_0.sync.scouts.downloads.ScoutsDownloadIntentService;
import com.hensongeodata.myagro360v2.view.ScoutAutomated;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class ScoutingFragment extends Fragment implements HistoryTabListener,Refresh {

      private static final String TAG = ScoutingFragment.class.getSimpleName();
      private static int SCOUT=100;
      private List<ScoutModel> items;
      private int mode;
      private String form_id;
      protected RecyclerView recyclerView;
      private static final String SECTION_ID = "SCOUT";
      private static final String ARG_PARAM2 = "param2";
      private int section_id;
      private String mParam2;

      private FloatingActionButton fabButton;
      private LinearLayout linearLayout;
      private TextView numberOfScoutsTextView;
      private MainViewModel mainViewModel;
      private MapDatabase mapDatabase;


      private ScoutingFragmentListener mListener;
      private ScoutHistoryRecyclerViewAdapter adapter;
      private Dialog progressDialog;
      MyApplication myApplication;

      public ScoutingFragment() { }

      public static ScoutingFragment newInstance(int sectionId) {
            ScoutingFragment fragment = new ScoutingFragment();
            Bundle args = new Bundle();
            args.putInt(SECTION_ID, sectionId);
            fragment.setArguments(args);
            return fragment;
      }

      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                  section_id = getArguments().getInt(SECTION_ID);
            }
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_scouting, container, false);

            recyclerView = view.findViewById(R.id.fragment_scouting_recyclerview);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
            mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            mapDatabase       = MapDatabase.getInstance(getContext());
            myApplication      = new MyApplication();
//            progressDialog = new Dialog(getContext());


            numberOfScoutsTextView = view.findViewById(R.id.number_of_scouts);
            linearLayout = view.findViewById(R.id.no_mapping_linear_layout);
            fabButton = view.findViewById(R.id.fab_add);
            Button addNewScoutButton = view.findViewById(R.id.add_new_scout_button);

            fabButton.setOnClickListener(v -> addNewScoutBButtonClicked());
            addNewScoutButton.setOnClickListener(v->addNewScoutBButtonClicked());

            getAllScoutsFromViewModel();

            if (myApplication.hasNetworkConnection(getContext())){

//                  Log.i(TAG, "onCreateView:  Downloads " + DownloadsSharePref.isScoutsDownloadPreferenceAvailable(getContext()));
//                  Log.i(TAG, "onCreateView:  Downloads " + DownloadsSharePref.getScoutsDownloadedPreference(getContext()));

//                  if (!DownloadsSharePref.isScoutsDownloadPreferenceAvailable(getContext()) || !DownloadsSharePref.getScoutsDownloadedPreference(getContext())){
//                        showScoutsDownloadDialog();
//                  }
            }

            return view;
      }

      private void addNewScoutBButtonClicked(){
//            mListener.scoutingFragmentAddButtonClicked();
            EventBus.getDefault().post(new ScoutButtonClickedEvent());
      }

      @Override
      public void onDetach() {
            super.onDetach();
            mListener = null;
      }

      @Override
      public void onSelected(String item) {}

      @Override
      public void reload(String scout_id) {}

      @Override
      public void moreClicked(String title, String sectionId) {}

      @Override
      public void shareButtonClicked(String section_id) {}

      @Override
      public void onSyncButtonClicked() {}



      private void openScouting(){
            Intent scouting=new Intent(getContext(), ScoutAutomated.class);
            scouting.putExtra("mode", ChatbotAdapterRV.MODE_SCOUT);
            startActivityForResult(scouting,SCOUT);
      }


      private void getAllScoutsFromViewModel(){

            mainViewModel.getScoutModels().observe(getActivity(), scoutModels -> {
                  items = scoutModels;

                  if ( scoutModels.size() != 0 ){

                        Log.i(TAG, "getAllScoutsFromViewModel:  " + scoutModels.size());
                        numberOfScoutsTextView.setText(String.valueOf(scoutModels.size()));

                        setAdapter();

                        linearLayout.setVisibility(View.GONE);
//                        fabButton.setVisibility(View.VISIBLE);
                  }else {

                        Log.i(TAG, "getAllScoutsFromViewModel:  " + scoutModels.size());

                        linearLayout.setVisibility(View.VISIBLE);
//                        fabButton.setVisibility(View.GONE);
                  }
            });
      }

      private void setAdapter(){

            adapter=new ScoutHistoryRecyclerViewAdapter(getContext(), items, new ScoutsRvListener() {


                  @Override
                  public void onScoutingFragmentMoreClicked(long id, long plotId) {
                        scoutingFragmentsMoreClicked(id, plotId);
//                        Toast.makeText(getContext(), "More Clicked!!", Toast.LENGTH_SHORT).show();
                  }

                  @Override
                  public void onReadButtonClicked(String name) {

                  }
            });
            adapter.setMode(mode);
            recyclerView.setAdapter(adapter);
      }

      private void scoutingFragmentsMoreClicked(long id, long plotId){
            mListener.scoutingFragmentMoreClicked( id, plotId );
      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof ScoutingFragment.ScoutingFragmentListener) {
                  mListener = (ScoutingFragment.ScoutingFragmentListener) context;
            } else {
                  throw new RuntimeException(context.toString()
                          + " must implement ScoutingFragmentListener");
            }
      }

      public interface ScoutingFragmentListener{
            void scoutingFragmentMoreClicked(long id, long plotId);

            void scoutingFragmentAddButtonClicked();
      }

      private void showScoutsDownloadDialog() {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Download scouts?");
            builder.setMessage(R.string.download_plots_string);
            builder.setPositiveButton("OK, Download!", (dialogInterface, i) -> {
                  Intent intentToSyncImmediately = new Intent(getContext(), ScoutsDownloadIntentService.class);
                  getContext().startService(intentToSyncImmediately);

                  new Handler().postDelayed(this::showProgressDialog, 10);

            });
            builder.setNegativeButton("NO", null);
            builder.show();
      }

      private void showProgressDialog() {
            progressDialog = new Dialog(getContext());
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            progressDialog.setContentView(R.layout.dialog_scouts_progress);
            progressDialog.setCancelable(false);


            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(progressDialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            progressDialog.show();
            progressDialog.getWindow().setAttributes(lp);
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onScoutsFragmentDownloadCompleted(ScoutsDownloadCompleted event) {
            progressDialog.dismiss();

//            Toast.makeText(getContext(), "Event from Scouts", Toast.LENGTH_LONG).show();

            if (!event.isFailed()){

                  if (DownloadsSharePref.isScoutsDownloadPreferenceAvailable(getContext())){
                        DownloadsSharePref.clearScoutsDownloadedPreference(getContext());
                  }
                  DownloadsSharePref.setScoutsDownloadedPreference(getContext(), true);

            }else {
                  Toast.makeText(getContext(), "Scouts failed to download!", Toast.LENGTH_SHORT).show();
            }


      }


      @Override
      public void onStart() {
            super.onStart();
            EventBus.getDefault().register(this);
      }

      @Override
      public void onStop() {
            EventBus.getDefault().unregister(this);
            super.onStop();
      }

}
