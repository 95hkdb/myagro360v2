package com.hensongeodata.myagro360v2.version2_0.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity( tableName =  ScoutModel.TABLE_NAME)
public class ScoutModel {

      public static final String TABLE_NAME = "scouts";

      public static final String COLUMN_ID            = "id";
      public static final String COLUMN_NAME    = "name";
      public static final String COLUMN_PLOT_ID = "plot_id";
      public static final String COLUMN_BATCH_ID = "batch_id";
      public static final String COLUMN_ORG_ID   = "org_id";
      public static final String COLUMN_USER_ID  = "user_id";
      public static final String COLUMN_INFECTED = "infected";
      public static final String COLUMN_PESTS_DETECTED= "pests_detected";
      public static final String COLUMN_DATE_CREATED = "date_created";


      @PrimaryKey(autoGenerate = true)
      @ColumnInfo( name = COLUMN_ID)
      private long id;

      @ColumnInfo( name = COLUMN_NAME)
      private String name;

      @ColumnInfo( name = COLUMN_PLOT_ID)
      private long plotId;

      @ColumnInfo( name = COLUMN_BATCH_ID)
      private String batchId;

      @ColumnInfo( name = COLUMN_ORG_ID)
      private String orgId;

      @ColumnInfo( name = COLUMN_USER_ID)
      private String userId;

      @ColumnInfo( name = COLUMN_INFECTED)
      private boolean infected;

      @ColumnInfo( name = COLUMN_PESTS_DETECTED)
      private int pestsDetected;

      @ColumnInfo( name = COLUMN_DATE_CREATED)
      private long dateCreated;

      public long getId() {
            return id;
      }

      public void setId(long id) {
            this.id = id;
      }

      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      public long getPlotId() {
            return plotId;
      }

      public void setPlotId(long plotId) {
            this.plotId = plotId;
      }

      public String getBatchId() {
            return batchId;
      }

      public void setBatchId(String batchId) {
            this.batchId = batchId;
      }

      public String getOrgId() {
            return orgId;
      }

      public void setOrgId(String orgId) {
            this.orgId = orgId;
      }

      public String getUserId() {
            return userId;
      }

      public void setUserId(String userId) {
            this.userId = userId;
      }

      public boolean getInfected() {
            return infected;
      }

      public void setInfected(boolean infected) {
            this.infected = infected;
      }

      public int getPestsDetected() {
            return pestsDetected;
      }

      public void setPestsDetected(int pestsDetected) {
            this.pestsDetected = pestsDetected;
      }

      public long getDateCreated() {
            return dateCreated;
      }

      public void setDateCreated(long dateCreated) {
            this.dateCreated = dateCreated;
      }
}
