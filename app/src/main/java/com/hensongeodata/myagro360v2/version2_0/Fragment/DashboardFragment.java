package com.hensongeodata.myagro360v2.version2_0.Fragment;


import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Events.DashboardItemClickedEvent;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmActivity;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {

      private static final String TAG = DashboardFragment.class.getSimpleName();

      private MapDatabase mMapDatabase;
      private TextView plotsCountTextView;
      private TextView mapsCountTextView;
      private TextView activitiesCountTextView;
      private TextView farmsCountTextView;
      private TextView scansCountTextView;
      private LinearLayout recentActivitiesLayout;
      private TextView orgNameTextView;
      private TextView orgNmeTextView;
      private TextView orgCodeTextView;
      private TextView professionTextView;
      private CircularImageView userImageTextView;
      private CircularImageView orgImageView;
      private CardView activitiesCardView;
      private LinearLayout plotsLinearLayout;
      private LinearLayout scansLinearLayout;
      private LinearLayout mapsLinearLayout;
      private LinearLayout farmsLinearLayout;

      public DashboardFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            mMapDatabase             = MapDatabase.getInstance(getContext());
            View view                          = inflater.inflate(R.layout.fragment_dashboard, container, false);
            plotsCountTextView     = view.findViewById(R.id.plots_count);
            mapsCountTextView = view.findViewById(R.id.maps_count);
            activitiesCountTextView = view.findViewById(R.id.activities_count);
            farmsCountTextView = view.findViewById(R.id.farms_count);
            recentActivitiesLayout = view.findViewById(R.id.recent_activities_layout);
            scansCountTextView     = view.findViewById(R.id.scans_count);
            orgNameTextView         = view.findViewById(R.id.org_name);
            orgNmeTextView         = view.findViewById(R.id.org_nme);
            orgCodeTextView         = view.findViewById(R.id.org_code);
            professionTextView        = view.findViewById(R.id.profession_text_view);
            userImageTextView        = view.findViewById(R.id.user_image_view);
            activitiesCardView           = view.findViewById(R.id.activities_card_view);
            plotsLinearLayout            = view.findViewById(R.id.plots_linear_layout);
            scansLinearLayout            = view.findViewById(R.id.scans_linear_layout);
            mapsLinearLayout            = view.findViewById(R.id.maps_linear_layout);
            farmsLinearLayout            = view.findViewById(R.id.farms_linear_layout);
            orgImageView                    = view.findViewById(R.id.org_image_view);

            getCounts();

            populateRecentActivities(view);

            displayUserDetails();

            activitiesCardView.setOnClickListener(v -> EventBus.getDefault().post(new DashboardItemClickedEvent("Activities")));
            plotsLinearLayout.setOnClickListener(v -> EventBus.getDefault().post(new DashboardItemClickedEvent("Plots")));
            scansLinearLayout.setOnClickListener(v -> EventBus.getDefault().post(new DashboardItemClickedEvent("Scans")));
            mapsLinearLayout.setOnClickListener(v -> EventBus.getDefault().post(new DashboardItemClickedEvent("Maps")));
            farmsLinearLayout.setOnClickListener(v -> EventBus.getDefault().post(new DashboardItemClickedEvent("Farms")));

            return view;
      }

      private void getCounts(){

            AtomicInteger plotsCount = new AtomicInteger();
            AtomicInteger mapsCount = new AtomicInteger();
            AtomicInteger activitiesCount = new AtomicInteger();
            AtomicInteger farmsCount = new AtomicInteger();
            AtomicInteger scansCount = new AtomicInteger();

            AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                  @Override
                  public void run() {
                        String orgId = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);
                        String userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);


                        plotsCount.set(mMapDatabase.plotDaoAccess().plotCountByOrgId(orgId));
                        mapsCount.set(mMapDatabase.daoAccess().mapsByOrgId(orgId));
                        activitiesCount .set(mMapDatabase.farmActivityDaoAccess().farmActivitiesCount(userId, orgId));
                        farmsCount .set(mMapDatabase.farmDaoAccess().getFarmsCount(orgId));
                        scansCount.set(mMapDatabase.scanDaoAccess().scoutCount(orgId));
                  }
            });

            new Handler().postDelayed(() -> {

                  plotsCountTextView.setText(String.valueOf(plotsCount.get()));
                  mapsCountTextView.setText(String.valueOf(mapsCount.get()));
                  activitiesCountTextView.setText(String.valueOf(activitiesCount.get()));
                  farmsCountTextView.setText(String.valueOf(farmsCount.get()));
                  scansCountTextView.setText(String.valueOf(scansCount.get()));
            },1000);

      }

      private void displayUserDetails(){
            String orgCode = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);
            String orgName = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(1);
            String firstName  = SharePrefManager.getInstance(getContext()).getUserDetails().get(1);
            String lastName =  SharePrefManager.getInstance(getContext()).getUserDetails().get(2);
            String email =  SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
            String profession =  SharePrefManager.getInstance(getContext()).getUserDetails().get(4);
            String imageUrl = "https://accounts.silapha.com/external/clientDetailsKey/?email="+ email;
            String orgImage =  SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(4);

            Log.i(TAG, "display User Details : " + "org name " + orgName + " first name " + firstName +
                    " last name " + lastName + " email " + email + "profession " + " image Url " + imageUrl + "All " + SharePrefManager.getInstance(getContext()).getOrganisationDetails()) ;

            orgNameTextView.setText(String.format("%s %s", firstName, lastName));
            orgNmeTextView.setText(String.format("%s", orgName));
            orgCodeTextView.setText(String.format("%s", orgCode));
            Picasso.get().load(orgImage).into(orgImageView);

            professionTextView.setText(email);
      }

      private void populateRecentActivities(View view) {
            String orgId = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);
            String userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
            final AtomicReference<List<FarmActivity>>[] farmActivityList = new AtomicReference[]{new AtomicReference<>()};

            AppExecutors.getInstance().getDiskIO().execute(() -> {
                  farmActivityList[0] =
                          new AtomicReference<>(mMapDatabase.farmActivityDaoAccess().fourRecentActivities(userId, orgId));
            });

            new Handler().postDelayed(() -> {

                  if (farmActivityList[0].get() != null) {
                        if (farmActivityList[0].get().size() != 0) {
                              ((TextView) view.findViewById(R.id.activity_one)).setText(farmActivityList[0].get().get(0).getName());
                              ((TextView) view.findViewById(R.id.activity_one_date)).setText(farmActivityList[0].get().get(0).getStartDateTimeString());

                              if (farmActivityList[0].get().size() > 1) {
                                    ((TextView) view.findViewById(R.id.activity_two)).setText(farmActivityList[0].get().get(1).getName());
                                    ((TextView) view.findViewById(R.id.activity_two_date)).setText(farmActivityList[0].get().get(1).getStartDateTimeString());
                              }

                              if (farmActivityList[0].get().size() > 2) {
                                    ((TextView) view.findViewById(R.id.activity_three)).setText(farmActivityList[0].get().get(2).getName());
                                    ((TextView) view.findViewById(R.id.activity_three_date)).setText(farmActivityList[0].get().get(2).getStartDateTimeString());

                              }

                              if (farmActivityList[0].get().size() > 3) {
                                    ((TextView) view.findViewById(R.id.activity_four)).setText(farmActivityList[0].get().get(3).getName());
                                    ((TextView) view.findViewById(R.id.activity_four_date)).setText(farmActivityList[0].get().get(3).getStartDateTimeString());
                              }

                        } else if (farmActivityList[0].get().size() == 0) {

                              ((TextView) view.findViewById(R.id.activity_two)).setText("There are no farm activities recorded!");
                              view.findViewById(R.id.see_more).setVisibility(View.GONE);
                        }
                  }

            }, 1000);


      }

}
