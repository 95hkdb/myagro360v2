package com.hensongeodata.myagro360v2.version2_0.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;

public class ChooseMapDialogFragment extends DialogFragment {

      private static final String TAG = ChooseMapDialogFragment.class.getSimpleName();
      private MainViewModel mainViewModel;
      private DatabaseHelper databaseHelper;
      private AgroWebAPI mAgroWebAPI;
      private AppCompatSpinner spn_maps;
      private HashMap<Integer, String> mapSpinnerHashMap;
      private AppCompatImageButton btnClose;
      private AppCompatButton btnSave;
      private TextView sizeTextView;
      private ProgressBar progressBar;
      private static final String PLOT_NAME = "name";
      private static final String MAP_ID = "map_id";
      private static final String PLOT_DESCRIPTION= "description";
      private static final String PLOT_SIZE = "plot_size";
      String section_id;
      private String mPlotName;
      private double mPlotSize;
      private String mPlotDescription;
      private MapDatabase mapDatabase;
      private double perimeter;
      private double inKilometers;
      private double acreage;


      private OnChooseMapFragmentListener mListener;
      private WebView webView;
      private ImageView noPreviewAvailableImageView;

      private MyApplication myApplication;

      public ChooseMapDialogFragment() {}

      @NonNull
      @Override
      public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
            Dialog dialog = super.onCreateDialog(savedInstanceState);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            return dialog;
      }

      public static ChooseMapDialogFragment newInstance(String mapId, int plotSize) {
            ChooseMapDialogFragment fragment = new ChooseMapDialogFragment();
            Bundle args = new Bundle();
            args.putString(MAP_ID, mapId);
            fragment.setArguments(args);
            return fragment;
      }

      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                  mPlotName = getArguments().getString(PLOT_NAME);
            }
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_choose_map_dialog, container, false);

            myApplication = new MyApplication();
            mapDatabase = MapDatabase.getInstance(getContext());
            databaseHelper = new DatabaseHelper(getContext());
            mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            mAgroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);
            progressBar      = view.findViewById(R.id.progress_bar);
            spn_maps  = view.findViewById(R.id.spn_maps);
            btnClose    = view.findViewById(R.id.bt_close);
            btnSave    = view.findViewById(R.id.bt_save);
            webView   = view.findViewById(R.id.dialog_web_view);
            sizeTextView = view.findViewById(R.id.map_size);
            noPreviewAvailableImageView = view.findViewById(R.id.no_preview_available);

            btnClose.setOnClickListener(v -> dismiss());

            spn_maps.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        sizeTextView.setVisibility(View.GONE);
                        section_id = mapSpinnerHashMap.get(spn_maps.getSelectedItemPosition());
                        HelperClass.LoadWebView(webView, section_id, noPreviewAvailableImageView, getContext());
                        showPerimeter(section_id);
                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) { }
            });

            btnSave.setOnClickListener((View v) -> {
                  onContinueButtonClicked(section_id,  mPlotSize);
            });
            fetchMapsFromDatabase();
            return view;
      }

      public void onContinueButtonClicked(String mapId, double plotSize) {
            if (mListener != null) {
                  mListener.onContinueButtonClicked(mapId, plotSize);
            }
      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof OnChooseMapFragmentListener) {
                  mListener = (OnChooseMapFragmentListener) context;
            } else {
                  throw new RuntimeException(context.toString()
                          + " must implement OnPlotActivitiesFragmentListener");
            }
      }

      @Override
      public void onDetach() {
            super.onDetach();
            mListener = null;
      }


      private void setupArrayAdapter(String[] stringArray, AppCompatSpinner spinner){
            ArrayAdapter<String> array = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_item, stringArray);
            array.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(array);
      }

      private void fetchMapsFromDatabase(){
            mainViewModel.getMaps().observe(getActivity(), mapModelList -> {
                  String[] spinnerArray = new String[mapModelList.size()];
                  try {

                        mapSpinnerHashMap = new HashMap<Integer, String>();
                        for ( int i =0; i < mapModelList.size(); i++ ){
                              mapSpinnerHashMap.put(i, String.valueOf(mapModelList.get(i).getId()));
                              spinnerArray[i] = mapModelList.get(i).getName();
                              Log.i(TAG, "onChanged:  " + mapModelList.get(i).getId());
                        }
                        setupArrayAdapter(spinnerArray, spn_maps);
                  } catch (Exception e) {
                        Log.e(TAG, "onNext:  " + e.getMessage());
                  }
            });
      }

      private void showPerimeter( String mapId){
            sizeTextView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

            AtomicReference<MapModel> mapModel = new AtomicReference<>();

            AppExecutors.getInstance().getDiskIO().execute(() -> mapModel.set(mapDatabase.daoAccess().fetchMapById(mapId)));

            new Handler().postDelayed(() -> {
                  sizeTextView.setVisibility(View.VISIBLE);
                  progressBar.setVisibility(View.GONE);
                  mPlotSize = (mapModel.get().getSizeAc());
                  sizeTextView.setText(String.format("%sac | %skm | %sm", String.valueOf(mapModel.get().getSizeAc()), mapModel.get().getSizeKm(), mapModel.get().getSizeM()));
            }, 2000);
      }


      @Override
      public void onResume() {
            super.onResume();
      }

      public interface OnChooseMapFragmentListener {
            void onContinueButtonClicked(String mapId, double plotSize);
      }
}
