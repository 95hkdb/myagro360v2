package com.hensongeodata.myagro360v2.version2_0;


import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlotDetailDialogFragment extends BottomSheetDialogFragment {

      private BottomSheetBehavior mBehavior;
      private AppBarLayout app_bar_layout;
      private LinearLayout lyt_profile;
      private WebView webView;

      private FarmPlot farmPlot;

      public void setData(FarmPlot farmPlot) {
            this.farmPlot = farmPlot;
      }

      public PlotDetailDialogFragment() {}


      public Dialog onCreateDialog(Bundle savedInstanceState) {
            final BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
            final View view = View.inflate(getContext(), R.layout.fragment_plot_detail, null);
            webView = view.findViewById(R.id.plot_web_view);
            TextView plotNameTextView = view.findViewById(R.id.plot_name);
            TextView plotSizeTextView = view.findViewById(R.id.plot_size);
            TextView plotCropTextView = view.findViewById(R.id.plot_crop);

            dialog.setContentView(view);
            mBehavior = BottomSheetBehavior.from((View) view.getParent());
            mBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);


            app_bar_layout = (AppBarLayout) view.findViewById(R.id.app_bar_layout);
            lyt_profile = (LinearLayout) view.findViewById(R.id.lyt_profile);


            ((TextView) view.findViewById(R.id.name)).setText(farmPlot.getDescription());


//            String content = Html.fromHtml(farmPlot.getDescription()).toString();

//            ((TextView) view.findViewById(R.id.kb_content)).setText(content);
            ((TextView) view.findViewById(R.id.name_toolbar)).setText(farmPlot.getName());
//            ((View) view.findViewById(R.id.lyt_spacer)).setMinimumHeight(Tools.getScreenHeight() / 2);

            hideView(app_bar_layout);

            mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                  @Override
                  public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                              showView(app_bar_layout, getActionBarSize());
                              hideView(lyt_profile);
                        }
                        if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                              hideView(app_bar_layout);
                              showView(lyt_profile, getActionBarSize());
                        }

                        if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                              dismiss();
                        }
                  }

                  @Override
                  public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                  }
            });

            plotNameTextView.setText(farmPlot.getName());
            plotSizeTextView.setText(String.format(" %sm | %skm | %sac", farmPlot.getSizeM(), farmPlot.getSizeKm(), farmPlot.getSizeAc()));
            plotCropTextView.setText(String.format(" Crop :  %s", farmPlot.getCropName()));
            loadMap(farmPlot.getMapId());

            ((ImageButton) view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        dismiss();
                  }
            });

            return dialog;
      }

      private void hideView(View view) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = 0;
            view.setLayoutParams(params);
      }

      private void showView(View view, int size) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = size;
            view.setLayoutParams(params);
      }

      private int getActionBarSize() {
            final TypedArray styledAttributes = getContext().getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
            int size = (int) styledAttributes.getDimension(0, 0);
            return size;
      }

      private void loadMap(String sectionId) {
            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setDisplayZoomControls(false);
            webView.loadUrl(Keys.AGRO_BASE+"faw-api/get-map/?mapping_id="+sectionId);
            webView.setWebViewClient(new WebViewClient() {

                  @Override
                  public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
//                        progressBar.setVisibility(View.GONE);
//                        webView.setVisibility(View.VISIBLE);
                  }

                  @TargetApi(Build.VERSION_CODES.LOLLIPOP)

                  @Override
                  public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                        view.loadUrl(request.getUrl().toString());
                        return false;
                  }

                  @Override
                  public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);
                  }

                  @Override
                  public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                        super.onReceivedError(view, request, error);
//                        Log.d(TAG,"error");
                  }

                  @Override
                  public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                        super.onReceivedSslError(view, handler, error);
//                        Log.d(TAG,"ssl error");
                  }

                  @Override
                  public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                        super.onReceivedHttpError(view, request, errorResponse);
//                        Log.d(TAG,"http error");
                  }
            });
      }

}
