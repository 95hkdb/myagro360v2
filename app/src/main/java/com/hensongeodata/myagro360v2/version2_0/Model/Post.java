package com.hensongeodata.myagro360v2.version2_0.Model;

public class Post {

      private String type;

      private Message message;

      private boolean has_image;

      private String  image;

      private boolean has_link;

      private String link;

      public String getType() {
            return type;
      }

      public void setType(String type) {
            this.type = type;
      }

      public Message getMessage() {
            return message;
      }

      public void setMessage(Message message) {
            this.message = message;
      }

      public boolean isHas_image() {
            return has_image;
      }

      public void setHas_image(boolean has_image) {
            this.has_image = has_image;
      }

      public String getImage() {
            return image;
      }

      public void setImage(String image) {
            this.image = image;
      }

      public boolean isHas_link() {
            return has_link;
      }

      public void setHas_link(boolean has_link) {
            this.has_link = has_link;
      }

      public String getLink() {
            return link;
      }

      public void setLink(String link) {
            this.link = link;
      }
}
