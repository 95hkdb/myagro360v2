package com.hensongeodata.myagro360v2.version2_0.sync.plots;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.google.gson.JsonElement;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.response.PlotResponse;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;

import org.json.JSONException;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlotSyncTask {

      private static final String TAG = PlotSyncTask.class.getSimpleName();
      private static MapDatabase mapDatabase;
      private static final long[] DEFAULT_VIBRATION_PATTERN =  {0, 250, 250, 250};
      protected static MyApplication myApplication;


      synchronized public static  void syncAllPlots(Context context) throws JSONException {
            myApplication=new MyApplication();
            mapDatabase = MapDatabase.getInstance(context);
            AgroWebAPI agroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            List<FarmPlot> unSyncedPlots = mapDatabase.plotDaoAccess().fetchAllUnSyncedPlots();
            if (myApplication.hasNetworkConnection(context) && unSyncedPlots.size() != 0 ){

                 int notificationlId = 1004;
                 String channelName = "plotUpdatesChannel";
                 String channelId         = "plotUpdatesChannelId";

                 NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Plot sync is in progress..."));
                 showNotification(notificationlId, channelId, channelName,context,  "Plot sync is in progress.. ", notificationStyle,null );

                                     AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                           @Override
                                           public void run() {

                                                 for(FarmPlot plot: unSyncedPlots){

                                                       long id        = plot.getPlotId();
                                                       String name = plot.getName();
                                                       String description = plot.getDescription();
                                                       double plotSize       = plot.getSizeAc();
                                                       String mapId =           plot.getMapId();
                                                       String cropId  = plot.getCropId();
                                                       String orgId      = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

//                                                       String userId     = SharePrefManager.getInstance(context).getUserDetails().get(0);

                                                       String userId = "";
                                                       if (SharePrefManager.isSecondaryUserAvailable(context)){
                                                             userId = SharePrefManager.getSecondaryUserPref(context);
                                                       }else {
                                                             userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
                                                       }

                                                       String farmId     = plot.getFarmId();

                                                       Log.d(TAG, "Plots loop:  " + "id " + id + " name  " + name + " org id  " + orgId  + " user id " + userId   );

                                                       agroWebAPI.createFarmPlot(name, description, plotSize,  mapId, cropId, orgId, userId, farmId).enqueue(new Callback<JsonElement>() {
                                                             @Override
                                                             public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                                                                   if (response.isSuccessful()){
                                                                         AppExecutors.getInstance().getDiskIO().execute(() -> {
                                                                               mapDatabase.plotDaoAccess().updateSyncStatusById(id);

                                                                               showNotification(notificationlId, channelId, channelName,context,  "Plot sync is now complete!", notificationStyle,null );
                                                                         });

                                                                   }else if (response.code() == 500){
                                                                         Log.d(TAG, "onResponse:  " + response.body());
                                                                         Log.d(TAG, "onResponse:  " + response.body());
                                                                         showNotification(notificationlId, channelId, channelName,context,  "Plot sync failed ", notificationStyle,null );
                                                                   }
                                                             }

                                                             @Override
                                                             public void onFailure(Call<JsonElement> call, Throwable t) {
                                                                   Log.e(TAG, "onFailure:  " + t.getMessage() );
                                                             }
                                                       });
                                                 }
                                           }
                                     });
                 }
      }

      synchronized public static  void createFarmPlot(Context context, String name, String description, double plotSize, String mapId, String cropId, String orgId, String farmId) {
            myApplication=new MyApplication();
            mapDatabase = MapDatabase.getInstance(context);
            AgroWebAPI agroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

                                    String userId = "";
                                    if (SharePrefManager.isSecondaryUserAvailable(context)){
                                          userId = SharePrefManager.getSecondaryUserPref(context);
                                    }else {
                                          userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
                                    }

                                    agroWebAPI.createFarmPlot(name, description, plotSize,  mapId, cropId, orgId, userId, farmId).enqueue(new Callback<JsonElement>() {
                                          @Override
                                          public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                                                if (response.isSuccessful()){
                                                      AppExecutors.getInstance().getDiskIO().execute(() -> {
//                                                            mapDatabase.plotDaoAccess().updateSyncStatusById(id);

//                                                            showNotification(notificationlId, channelId, channelName,context,  "Plot sync is now complete!", notificationStyle,null );
                                                      });

                                                }else if (response.code() == 500){
                                                      Log.d(TAG, "onResponse:  " + response.body());
                                                      Log.d(TAG, "onResponse:  " + response.body());
//                                                      showNotification(notificationlId, channelId, channelName,context,  "Plot sync failed ", notificationStyle,null );
                                                }
                                          }

                                          @Override
                                          public void onFailure(Call<JsonElement> call, Throwable t) {
                                                Log.e(TAG, "onFailure:  " + t.getMessage() );
                                          }
                                    });

      }

      synchronized public static  void fetchAllPlots(Context context) throws JSONException {
            myApplication=new MyApplication();
            mapDatabase = MapDatabase.getInstance(context);
            AgroWebAPI agroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);

            List<FarmPlot> unSyncedPlots = mapDatabase.plotDaoAccess().fetchAllUnSyncedPlots();
            if (myApplication.hasNetworkConnection(context) && unSyncedPlots.size() != 0 ){

                  int notificationlId = 23894;
                  String channelName = "plotsPullChannel";
                  String channelId         = "plotsPullChannelId";

                  NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Plot sync is in progress..."));
                  showNotification(notificationlId, channelId, channelName,context,  "Plot sync is in progress.. ", notificationStyle,null );

                  AppExecutors.getInstance().getDiskIO().execute(() -> {

//
                        String orgId = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);
                        String userId = SharePrefManager.getInstance(context).getUserDetails().get(0);

                        agroWebAPI.getPlotsCall(orgId, userId).enqueue(new Callback<PlotResponse>() {
                              @Override
                              public void onResponse(Call<PlotResponse> call, Response<PlotResponse> response) {

                                    if (response.isSuccessful()){
                                          List<FarmPlot> farmPlots = null;
                                          if (response.body() != null) {
                                                farmPlots = response.body().getPlots();

                                                for (FarmPlot farmPlot : farmPlots){

                                                      if (mapDatabase.plotDaoAccess().plotExist(farmPlot.getId()) == null){

                                                            farmPlot.setOrgId(orgId);
                                                            farmPlot.setUserId(userId);

                                                            AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                                                  @Override
                                                                  public void run() {
                                                                        mapDatabase.plotDaoAccess().insert(farmPlot);
                                                                  }
                                                            });
                                                      }
                                                }

                                                showNotification(notificationlId, channelId, channelName, context, "Plots are syncing!", notificationStyle, null);
                                          }
                                    }
                              }

                              @Override
                              public void onFailure(Call<PlotResponse> call, Throwable t) {
                                    Log.e(TAG, "onFailure:  " + t.getMessage() );
                              }
                        });
//
                  });
            }
      }

      public static void showNotification(int notificationId, String channelId,
                                          String channelName, Context context, String body, NotificationCompat.Style notificationStyle, Intent intent) {

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            int importance = NotificationManager.IMPORTANCE_HIGH;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setVibrate(DEFAULT_VIBRATION_PATTERN)
                    .setContentText(body)
                    .setStyle(notificationStyle)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setNumber(1)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                    .setAutoCancel(true);

            if (intent != null){
                  TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                  stackBuilder.addNextIntent(intent);
                  PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                          0,
                          PendingIntent.FLAG_UPDATE_CURRENT
                  );
                  mBuilder.setContentIntent(resultPendingIntent);
            }

            notificationManager.notify(notificationId, mBuilder.build());
      }

}
