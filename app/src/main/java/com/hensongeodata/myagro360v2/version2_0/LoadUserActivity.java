package com.hensongeodata.myagro360v2.version2_0;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Events.MapProgressEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.MapsDownloadCompleted;
import com.hensongeodata.myagro360v2.version2_0.Events.PlotProgressEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.PlotsDownloadCompleted;
import com.hensongeodata.myagro360v2.version2_0.Events.ScanProgressEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.ScoutProgressEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.ScoutsDownloadCompleted;
import com.hensongeodata.myagro360v2.version2_0.Fragment.LoadUserPropertiesFragment;
import com.hensongeodata.myagro360v2.version2_0.sync.map.downloads.MapsDownloadIntentService;
import com.hensongeodata.myagro360v2.version2_0.sync.plots.downloads.PlotsDownloadIntentService;
import com.hensongeodata.myagro360v2.version2_0.sync.scouts.downloads.ScoutsDownloadIntentService;
import com.hensongeodata.myagro360v2.view.MainHomePageActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class LoadUserActivity extends AppCompatActivity {

      private static final String TAG = LoadUserPropertiesFragment.class.getSimpleName();
      private ProgressBar mapProgressBar;
      private ProgressBar plotProgressBar;
      private ProgressBar scoutProgressBar;
      private ProgressBar scanProgressBar;
      private Dialog progressDialog;
      private TextView dialogContentTextView;
      private String userId;
      private MainViewModel mainViewModel;

      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
//            setContentView(R.layout.activity_load_user);
            setContentView(R.layout.fragment_load_user_properties);

            if (SharePrefManager.isSecondaryUserAvailable(getApplicationContext())){
                  userId = SharePrefManager.getSecondaryUserPref(getApplicationContext());
            }else {
                  userId = SharePrefManager.getInstance(getApplicationContext()).getUserDetails().get(0);
            }

//            userId = SharePrefManager.getSecondaryUserPref(this);

            showProgressDialog();

            mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
            mapProgressBar = findViewById(R.id.map_progress);
            plotProgressBar = findViewById(R.id.plots_progress);
            scoutProgressBar = findViewById(R.id.scout_progress);
            scanProgressBar = findViewById(R.id.scans_progress);

            Intent intentToSyncImmediately = new Intent(this, MapsDownloadIntentService.class);
            startService(intentToSyncImmediately);
      }


      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onMapDownloadCompleted(MapsDownloadCompleted event) {

            Log.i(TAG, "onMapDownloadCompleted:  Completed" );
            downloadPlots();
      }


      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onPlotDownloadCompleted(PlotsDownloadCompleted event) {
            Log.i(TAG, "onPlotDownloadCompleted: ");
            downloadScouts();
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onScoutsDownloadCompleted(ScoutsDownloadCompleted event) {
            Log.i(TAG, "onScoutsDownloadCompleted: ");
            progressDialog.dismiss();
            showCustomDialog();

            new Handler().postDelayed(new Runnable() {
                  @Override
                  public void run() {
                        startActivity(new Intent(getApplicationContext(), MainHomePageActivity.class));
                  }
            },1000);

      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onScoutProgressEvent(ScoutProgressEvent event) {
            Log.i(TAG, "onScoutProgressEvent:  " + event.getPercentage());
            scoutProgressBar.setMax(event.getTotal());
            scoutProgressBar.setProgress(event.getPercentage(), true);
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onScanProgressEvent(ScanProgressEvent event) {
            Log.i(TAG, "onScanProgressEvent:  " + event.getPercentage());
            scanProgressBar.setMax(event.getTotal());
            scanProgressBar.setProgress(event.getPercentage(), true);
      }


      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onPlotProgressEvent(PlotProgressEvent event) {
            Log.i(TAG, "onPlotProgressEvent:  " + event.getPercentage());
            plotProgressBar.setMax(event.getTotal());
            plotProgressBar.setProgress(event.getPercentage(), true);
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onMapProgressEvent(MapProgressEvent event) {
            Log.i(TAG, "onMapProgressEvent:  " + event.getPercentage());
            mapProgressBar.setMax(event.getTotal());
            mapProgressBar.setProgress(event.getPercentage(), true);
      }

      private void downloadPlots() {
            Intent intentToSyncImmediately = new Intent(this, PlotsDownloadIntentService.class);
            startService(intentToSyncImmediately);
      }

      private void downloadScouts() {
            Intent intentToSyncImmediately = new Intent(this, ScoutsDownloadIntentService.class);
            startService(intentToSyncImmediately);
      }

      @Override
      public void onStart() {
            super.onStart();
            EventBus.getDefault().register(this);
      }

      @Override
      public void onStop() {
            EventBus.getDefault().unregister(this);
            super.onStop();
      }

      private void showProgressDialog() {
            progressDialog = new Dialog(this);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            progressDialog.setContentView(R.layout.dialog_user_downloads_progress);
            progressDialog.setCancelable(false);
            dialogContentTextView = progressDialog.findViewById(R.id.dialog_content_text_view);

            dialogContentTextView.setText(String.format("Downloading %s resources", userId));

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(progressDialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            progressDialog.show();
            progressDialog.getWindow().setAttributes(lp);
      }

      private void showCustomDialog() {
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            dialog.setContentView(R.layout.dialog_info);
            dialog.setCancelable(true);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            dialog.findViewById(R.id.bt_close).setOnClickListener(v -> {
//                  Toast.makeText(getContext(), ((AppCompatButton) v).getText().toString() + " Clicked", Toast.LENGTH_SHORT).show();
                  dialog.dismiss();
            });

            dialog.show();
            dialog.getWindow().setAttributes(lp);
      }

}
