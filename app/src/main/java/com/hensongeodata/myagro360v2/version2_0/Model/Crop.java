package com.hensongeodata.myagro360v2.version2_0.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity( tableName = Crop.TABLE_NAME)
public class Crop {

      public static final String COLUMN_ID   = "id";
      public static final String COLUMN_CROP_ID  = "crop_id";
      public static final String COLUMN_NAME = "name";

      public static final String TABLE_NAME = "crops";

      @PrimaryKey(autoGenerate = true)
      @ColumnInfo( name = COLUMN_CROP_ID)
      private long cropId;

      @ColumnInfo(name = COLUMN_ID)
      private int id;

      @ColumnInfo(name = COLUMN_NAME)
      private String name;

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      public long getCropId() {
            return cropId;
      }

      public void setCropId(long cropId) {
            this.cropId = cropId;
      }
}
