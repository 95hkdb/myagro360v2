package com.hensongeodata.myagro360v2.version2_0.Fragment;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Keys;


public class FullMapDialogFragment extends DialogFragment {
      private static final String ARG_PARAM1 = "param1";

      private String mParam1;
      private WebView webView;


      private FullMapDialogFragmentListener mListener;

      public FullMapDialogFragment() {}

      public static FullMapDialogFragment newInstance(String param1) {
            FullMapDialogFragment fragment = new FullMapDialogFragment();
            Bundle args = new Bundle();
            args.putString(ARG_PARAM1, param1);
            fragment.setArguments(args);
            return fragment;
      }

      @Override
      public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                  mParam1 = getArguments().getString(ARG_PARAM1);
            }
      }

      @NonNull
      @Override
      public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
            Dialog dialog = super.onCreateDialog(savedInstanceState);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            return dialog;
      }


      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_full_map_dialog, container, false);

            webView = view.findViewById(R.id.dialog_web_view);

            loadMap(mParam1);

            ((ImageView )view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        dismiss();
                  }
            });
            return view;
      }

      public void onButtonPressed(Uri uri) {
            if (mListener != null) {
                  mListener.onDismiss();
            }
      }

//      @Override
//      public void onAttach(Context context) {
//            super.onAttach(context);
//            if (context instanceof FullMapDialogFragmentListener) {
//                  mListener = (FullMapDialogFragmentListener) context;
//            } else {
//                  throw new RuntimeException(context.toString()
//                          + " must implement FullMapDialogFragmentListener");
//            }
//      }

//      @Override
//      public void onDetach() {
//            super.onDetach();
//            mListener = null;
//      }


      public interface FullMapDialogFragmentListener {
            void onDismiss();
      }

      private void loadMap(String section_id ){

            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setDisplayZoomControls(false);


            webView.loadUrl(Keys.AGRO_BASE+"faw-api/get-map/?mapping_id="+section_id);
            webView.setWebViewClient(new WebViewClient() {


                  @Override
                  public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
//                        progressBar.setVisibility(View.GONE);
                  }

                  @TargetApi(Build.VERSION_CODES.LOLLIPOP)


                  @Override
                  public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                        view.loadUrl(request.getUrl().toString());
                        return false;
                  }

                  @Override
                  public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);
                  }

                  @Override
                  public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                        super.onReceivedError(view, request, error);
//                        Log.d(TAG,"error");
//                        btnRetry.setVisibility(View.VISIBLE);
                  }

                  @Override
                  public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                        super.onReceivedSslError(view, handler, error);
//                        Log.d(TAG,"ssl error");
                        // btnRetry.setVisibility(View.VISIBLE);
                  }

                  @Override
                  public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                        super.onReceivedHttpError(view, request, errorResponse);
//                        Log.d(TAG,"http error");
                        //btnRetry.setVisibility(View.VISIBLE);
                  }
            });
      }

}
