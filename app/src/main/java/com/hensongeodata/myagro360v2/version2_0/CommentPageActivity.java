package com.hensongeodata.myagro360v2.version2_0;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonElement;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.R.id;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.response.ThreadResponse;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Adapter.CommentsRecyclerViewAdapter;
import com.hensongeodata.myagro360v2.version2_0.Events.CommentClickedEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.CommentPageBackEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CommentPageActivity extends AppCompatActivity {

      private static final String TAG = CommentPageActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private CommentsRecyclerViewAdapter commentsRecyclerViewAdapter;
    private AgroWebAPI mAgroWebAPI;
    private TextView threadCreatorNameTextView;
    private TextView threadTitleTextView;
    private TextView threadContentTextView;
    private Button buttonSend;
    private TextView comment;
    private ProgressBar progressBar;
    private View parent_view;


      @Override
      protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(com.hensongeodata.myagro360v2.R.layout.activity_comment_page);

            mAgroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_FORUM).create(AgroWebAPI.class);
            recyclerView = findViewById(id.comment_history_recyclerview);
            recyclerView.setHasFixedSize(false);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setNestedScrollingEnabled(false);
            threadCreatorNameTextView = findViewById(id.thread_creator_name_text_view);
            threadTitleTextView  = findViewById(R.id.thread_title_text_view);
            threadContentTextView  = findViewById(R.id.thread_content_text_view);
            buttonSend                          = findViewById(R.id.buttonSend);
            comment                                = findViewById(id.comment);
            progressBar                           = findViewById(R.id.progress_bar);
            parent_view = findViewById(android.R.id.content);

            Intent commentIntent = getIntent();
            String threadId = commentIntent.getStringExtra("thread_id");

            getThreadsFromAPI(threadId);
            initToolbar();

            String email = SharePrefManager.getInstance(this).getUserDetails().get(0);
            String firstName = SharePrefManager.getInstance(this).getUserDetails().get(1);

            buttonSend.setOnClickListener(v -> {

                  if (!comment.getText().toString().isEmpty()){
                        String commentText = comment.getText().toString();
                        commentOnThread(threadId, commentText, firstName, email);

                        buttonSend.setEnabled(false);
                        buttonSend.setText("Please wait...");
                        comment.setEnabled(false);

                        new Handler().postDelayed(new Runnable() {
                              @Override
                              public void run() {
                                    getThreadsFromAPI(threadId);
                                    comment.setEnabled(true);
                                    buttonSend.setEnabled(true);
                                    buttonSend.setText("Send");
                                    Snackbar.make(parent_view, "Successfully added a comment!", Snackbar.LENGTH_LONG).show();
                                    comment.setText("");

                              }
                        },3000);

                  }
            });
      }

      private void initToolbar() {
            Toolbar toolbar =  findViewById(R.id.toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(null);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            toolbar.setNavigationOnClickListener(v -> {
//                  Toast.makeText(getApplicationContext(), "Back Pressed!", Toast.LENGTH_SHORT).show();
                  EventBus.getDefault().post(new CommentPageBackEvent());
                  finish();
            });
      }

      @Override
      public void onBackPressed() {
            super.onBackPressed();
            finish();
      }

      @Override
      public void onStart() {
            super.onStart();
            EventBus.getDefault().register(this);
      }

      @Override
      public void onStop() {
            EventBus.getDefault().unregister(this);
            super.onStop();
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onCommentClicked(CommentClickedEvent event) {
            Toast.makeText(this, "Comment Event Received From Comment Page " + event.getThreadModel().getContent(), Toast.LENGTH_SHORT).show();
      }

      private void getThreadsFromAPI(String id) {

            mAgroWebAPI.fetchSingleThread(id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<ThreadResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(ThreadResponse threadResponse) {
                                Log.i(TAG, "onNext:  Thread Response " + threadResponse.getData().getThread().getContent());
//                                Log.i(TAG, "onNext:  Thread Response " + threadResponse.getData().getThreads().get(0).getTitle());

                                threadCreatorNameTextView.setText(threadResponse.getData().getThread().getCreatorName());
                                threadTitleTextView.setText(threadResponse.getData().getThread().getTitle());
                                threadContentTextView.setText(threadResponse.getData().getThread().getContent());
                                commentsRecyclerViewAdapter = new CommentsRecyclerViewAdapter(getApplicationContext(), threadResponse.getData().getThread().getComments());
                          }

                          @Override
                          public void onError(Throwable e) {
                                Log.i(TAG, "onError:  Thread Response " + e.getMessage());
                                Log.i(TAG, "onError:  Thread Response " + e.getCause());
                                Log.i(TAG, "onError:  Thread Response " + e.getStackTrace());

                          }

                          @Override
                          public void onComplete() {
                                recyclerView.setAdapter(commentsRecyclerViewAdapter);
//                                progressBar.setVisibility(View.GONE);
                          }
                    });
      }


      private void commentOnThread(String id, String content, String firstName, String email) {

            mAgroWebAPI.commentThread(id, content, firstName, email)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<JsonElement>() {
                          @Override
                          public void onSubscribe(Disposable d) {}

                          @Override
                          public void onNext(JsonElement threadResponse) {
                          }

                          @Override
                          public void onError(Throwable e) {
                                Log.i(TAG, "onError:  Thread Response " + e.getMessage());
                                Log.i(TAG, "onError:  Thread Response " + e.getCause());
                                Log.i(TAG, "onError:  Thread Response " + e.getStackTrace());
                          }

                          @Override
                          public void onComplete() {
                                commentsRecyclerViewAdapter.notifyDataSetChanged();
                                recyclerView.setAdapter(commentsRecyclerViewAdapter);
//                                progressBar.setVisibility(View.GONE);
                          }
                    });

      }


}
