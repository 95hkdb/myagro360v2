package com.hensongeodata.myagro360v2.version2_0;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.hensongeodata.myagro360v2.BuildConfig;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.version2_0.Fragment.WalkthroughFragment;


public class WalkthroughActivity extends AppCompatActivity implements View.OnClickListener {
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough);

        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        viewPager.addOnPageChangeListener(new WizardPageChangeListener());

        View image = findViewById(R.id.activity_walkthrough_style16);
        String url = BuildConfig.IMAGE_URL + "walkthrough/style-14/Welcome-14-960.jpg";
        String urlThumb = BuildConfig.IMAGE_URL + "walkthrough/style-14/Welcome-14-960-thumb.jpg";
        loadImageRequest(image, url, urlThumb);

    }

    private void loadImageRequest(final View bg, String url, String urlThumb) {
        RequestBuilder<Drawable> requestBuilder = Glide.with(this).load(urlThumb);

        requestBuilder.thumbnail(Glide.with(this).load(url)).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                bg.setBackground(resource);
            }
        });
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private int WIZARD_PAGES_COUNT = 4;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new WalkthroughFragment(position);
        }

        @Override
        public int getCount() {
            return WIZARD_PAGES_COUNT;
        }

    }

    private class WizardPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int position) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageScrolled(int position, float positionOffset,
                                   int positionOffsetPixels) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageSelected(int position) {

        }
    }

    @Override
    public void onClick(View view) {
//        switch (view.getId()){
//            case R.id.btnSignIn:
//                Toast.makeText(this, "Button Sign In clicked!", Toast.LENGTH_SHORT).show();
//                break;
//            case R.id.btnSignUp:
//                Toast.makeText(this, "Button Sign Up clicked!", Toast.LENGTH_SHORT).show();
//                break;
//            default:
//                break;
//        }
    }
}
