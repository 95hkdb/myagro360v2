package com.hensongeodata.myagro360v2.version2_0.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.view.SigninSignup;


@SuppressLint("ValidFragment")
public class WalkthroughFragment extends Fragment {
    int wizard_page_position;

    public WalkthroughFragment(int position) {
        this.wizard_page_position = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        int layout_id = R.layout.walkthrogh_page_1;
        switch (wizard_page_position) {
            case 0:
                layout_id = R.layout.walkthrogh_page_1;
                break;

            case 1:
                layout_id = R.layout.walkthrough_page_2;
                break;

            case 2:
                layout_id = R.layout.walkthrough_page_3;
                break;

            case 3:
                layout_id = R.layout.walkthrough_page_4;
                break;
        }
        View view = inflater.inflate(layout_id, container, false);
        Button button = (Button) view.findViewById(R.id.btnGetStarted);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SigninSignup.class);
                getActivity().startActivity(intent);
            }
        });

        return view;
    }

}
