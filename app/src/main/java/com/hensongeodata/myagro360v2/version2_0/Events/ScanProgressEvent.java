package com.hensongeodata.myagro360v2.version2_0.Events;

public  class ScanProgressEvent {

      private int percentage;
      private int total;

      public ScanProgressEvent(int percentage, int total) {
            this.percentage = percentage;
            this.total = total;
      }

      public int getPercentage() {
            return percentage;
      }

      public void setPercentage(int percentage) {
            this.percentage = percentage;
      }

      public int getTotal() {
            return total;
      }

      public void setTotal(int total) {
            this.total = total;
      }

}
