package com.hensongeodata.myagro360v2.version2_0.sync.map.downloads;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.response.MapResponse;
import com.hensongeodata.myagro360v2.api.response.UserMapResponse;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Events.MapProgressEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.MapsDownloadCompleted;
import com.hensongeodata.myagro360v2.version2_0.Model.AppNotification;
import com.hensongeodata.myagro360v2.version2_0.Model.POISModel;
import com.hensongeodata.myagro360v2.version2_0.Model.Point;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsDownloadTask {

      private static final String TAG = MapsDownloadTask.class.getSimpleName();
      private static MapDatabase mapDatabase;
      private static final long[] DEFAULT_VIBRATION_PATTERN =  {0, 250, 250, 250};
      protected static MyApplication myApplication;
      private static final int MAPS_NOTIFICATION_ID = 1012;


      public static void DownloadMaps(Context context){

            String channelName = "syncMapsChannel";
            String channelId         = "syncMapUpdatesChannelId";

            AgroWebAPI AGRO_WEB_API = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);
             mapDatabase = MapDatabase.getInstance(context);

            String userId;
            if (SharePrefManager.isSecondaryUserAvailable(context)){
                  userId = SharePrefManager.getSecondaryUserPref(context);
            }else {
                  userId = SharePrefManager.getInstance(context).getUserDetails().get(0);
            }

            String orgId   = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

            Log.i(TAG, "onResponse:  Sync Map" + userId);

            AppExecutors.getInstance().getDiskIO().execute(() -> {

                        try{
                              if (  mapDatabase.daoAccess().mapsByOrgId(orgId) != 0){
                                    AppExecutors.getInstance().getDiskIO().execute(() -> {
                                          mapDatabase.daoAccess().deleteSyncedMaps();
                                          mapDatabase.poisDaoAccess().deletePOIS();
                                    });
                              }

                              NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                              int importance = NotificationManager.IMPORTANCE_LOW;

                              if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                                    NotificationChannel mChannel = new NotificationChannel(
                                            channelId, channelName, importance);
                                    notificationManager.createNotificationChannel(mChannel);
                              }

                              NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                                      .setSmallIcon(R.drawable.download)
                                      .setContentTitle("Downloading your maps..")
                                      .setContentText("Download in progress");


                              AGRO_WEB_API.getUserMap(userId, orgId).enqueue(new Callback<UserMapResponse>() {
                                    @Override
                                    public void onResponse(@NotNull Call<UserMapResponse> call, @NotNull Response<UserMapResponse> response) {

                                          try {

                                                int progressCounter = 0;

                                                Log.i(TAG, "onResponse:  Maps Empty Array " + response);

                                                if (response.body() != null) {
                                                      for (MapResponse mapResponse : response.body().getMaps()) {

                                                            Log.d(TAG, "onResponse:  Name " + mapResponse.getName());

                                                            progressCounter++;

                                                            MapModel mapModel = new MapModel();
                                                            mapModel.setId(mapResponse.getMappingCode());
                                                            mapModel.setName(mapResponse.getName());
                                                            mapModel.setType(mapResponse.getType());
                                                            mapModel.setSynced(true);
                                                            mapModel.setOrgId(orgId);
                                                            mapModel.setUserId(userId);
                                                            mapModel.setCreatedDate(mapResponse.getCreatedDate());

                                                            for (Point point : mapResponse.getPoints()) {
                                                                  POISModel poisModel = new POISModel();
                                                                  poisModel.setMapId(mapResponse.getMappingCode());
                                                                  String coordinatesInfo = point.getLatitude() + "#" + point.getLongitude() + "#" + point.getAccuracy();
                                                                  String coordinatesInfo2 = point.getLatitude() + "#" + point.getLongitude() ;
                                                                  poisModel.setInfo(coordinatesInfo);
                                                                  Log.d(TAG, "onResponse:  Name " + mapResponse.getName() + " coordinates " + coordinatesInfo2);
                                                                  AppExecutors.getInstance().getDiskIO().execute(() -> mapDatabase.poisDaoAccess().insert(poisModel));
                                                            }

                                                            int finalProgressCounter = progressCounter;
                                                            int finalProgressCounter1 = progressCounter;
                                                            AppExecutors.getInstance().getDiskIO().execute(() -> {
                                                                  try {

                                                                        List<POISModel> poisModelList = mapDatabase.poisDaoAccess().fetchPOISByMapId(mapModel.getId());
                                                                        ArrayList<String> strPois = new ArrayList<>();

                                                                        for (POISModel poisModel : poisModelList) {
                                                                              if (poisModel != null) {
                                                                                    String message = poisModel.getInfo();
                                                                                    if (message != null && !message.trim().isEmpty())
                                                                                          strPois.add(message);

//                                                                                    Log.d(TAG, "run: show Perimeter:  " + message);
                                                                              }
                                                                        }

                                                                        ArrayList<POI> pois = CreateForm.getInstance(context).buildPOI_Mapping(strPois);
                                                                        int area = MyApplication.getArea(pois);

                                                                        long perimeter = MyApplication.getPerimeter(pois);
                                                                        double inKilometers = HelperClass.metersToKilo(perimeter);
                                                                        double acreage = HelperClass.metersToAcres(perimeter);

                                                                        mapModel.setSizeAc(acreage);
                                                                        mapModel.setSizeM(perimeter);
                                                                        mapModel.setSizeKm(inKilometers);

                                                                        AppExecutors.getInstance().getDiskIO().execute(() -> {
                                                                              mapDatabase.daoAccess().insert(mapModel);

                                                                              EventBus.getDefault().post(new MapProgressEvent(finalProgressCounter1, response.body().getMaps().size() ));

                                                                              if (response.body().getMaps().size() == mapDatabase.daoAccess().countUserMaps(userId)){
                                                                                    EventBus.getDefault().post(new MapsDownloadCompleted(false));
                                                                              }
                                                                        });

//                                                                        Log.i(TAG, "onResponse:  Sync Map" + mapResponse.getName());

                                                                        mBuilder.setProgress(response.body().getMaps().size(), finalProgressCounter, false);
                                                                        // Displays the progress bar for the first time.

                                                                        notificationManager.notify(MAPS_NOTIFICATION_ID, mBuilder.build());

                                                                  } catch (Exception e) {

//                                                                        Log.e(TAG, "run: SyncMaps loop  " + e.getMessage());
                                                                  }
                                                            });
                                                      }
                                                }

                                          } catch (Exception e) {

                                                NotificationCompat.Style notificationStyle = (new NotificationCompat.BigTextStyle().bigText("Downloading failed!"));
                                                showNotification(MAPS_NOTIFICATION_ID, channelId, channelName, context, "Downloading failed!", notificationStyle, null);

                                                Log.e(TAG, "Map Syncing Failed:  " + e.getMessage());

                                                EventBus.getDefault().post(new MapsDownloadCompleted(true));
                                                EventBus.getDefault().post(new MapProgressEvent(100,  100));

                                          }

                                          AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                                @Override
                                                public void run() {
                                                      AppNotification appNotification = new AppNotification();
                                                      appNotification.setName(AppNotification.NOTIFICATION_MAPS);
                                                      appNotification.setMessage("Your maps have been downloaded to your device");
                                                      appNotification.setOwner(AppNotification.NOTIFICATION_MAPS);
                                                      appNotification.setDateCreated(System.currentTimeMillis());

                                                      mapDatabase.notificationDaoAccess().insert(appNotification);
                                                }
                                          });

                                    }

                                    @Override
                                    public void onFailure(Call<UserMapResponse> call, Throwable t) {
                                          Log.e(TAG, "onFailure:  " + t.getMessage());

                                          EventBus.getDefault().post(new MapsDownloadCompleted(false));
                                          EventBus.getDefault().post(new MapProgressEvent(100,  100));
                                    }
                              });
                        }catch (Exception e){
                              Log.e(TAG, "SyncAllMaps Error :  "  + e.getMessage() );
                        }

            });

      }

      public static void showNotification(int notificationId, String channelId,
                                          String channelName, Context context, String body, NotificationCompat.Style notificationStyle, Intent intent) {

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            int importance = NotificationManager.IMPORTANCE_HIGH;

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                  NotificationChannel mChannel = new NotificationChannel(
                          channelId, channelName, importance);
                  notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setVibrate(DEFAULT_VIBRATION_PATTERN)
                    .setContentText(body)
                    .setStyle(notificationStyle)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setNumber(1)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                    .setAutoCancel(true);

            if (intent != null){
                  TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                  stackBuilder.addNextIntent(intent);
                  PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                          0,
                          PendingIntent.FLAG_UPDATE_CURRENT
                  );
                  mBuilder.setContentIntent(resultPendingIntent);
            }

            notificationManager.notify(notificationId, mBuilder.build());
      }

}
