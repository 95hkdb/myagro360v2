package com.hensongeodata.myagro360v2.version2_0.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.hensongeodata.myagro360v2.Interface.HistoryTabListener;
import com.hensongeodata.myagro360v2.Interface.ItemTouchHelperViewHolder;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.version2_0.Listeners.ActivitiesRvListener;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmActivity;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ActivitiesRecyclerviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG = ActivitiesRecyclerviewAdapter.class.getSimpleName();
    private List<FarmActivity> items;
    private Context mContext;
    private HistoryTabListener listener;
    private ActivitiesRvListener activitiesRvListener;
    private int mode;
    protected DatabaseHelper databaseHelper;
    private MapDatabase mapDatabase;

    public ActivitiesRecyclerviewAdapter(Context context, List<FarmActivity> items, HistoryTabListener listener, ActivitiesRvListener activitiesRvListener) {
       this.activitiesRvListener=activitiesRvListener;
        mContext=context;
        this.items =items;
        this.listener=listener;
        mapDatabase = MapDatabase.getInstance(context);
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.activities_list, parent, false);
            return new TabHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof TabHolder) {

            FarmActivity farmActivity = items.get(position);

            Log.i(TAG, "onBindViewHolder:  " + farmActivity.getName());

            final TabHolder tabHolder = (TabHolder) holder;
            final long activityId = farmActivity.getActivityId();
            final String id = farmActivity.getId();
            final long plotId = farmActivity.getPrimaryPlotId();
            final String name = farmActivity.getName();
            final String startTimeText = farmActivity.getStartDateTimeString();
            final long   startDateTime = farmActivity.getStartDate();
            final long   endDateTime = farmActivity.getEndDate();
            final String farmId               = farmActivity.getFarmId();
            final String[] plotName = {""};
            final String[] farmName = {""};
            final boolean completed = farmActivity.isCompleted();

            AppExecutors.getInstance().getDiskIO().execute(() -> {
                FarmPlot farmPlot = mapDatabase.plotDaoAccess().fetchPlotByPlotId(plotId);
                plotName[0] = farmPlot.getName();
                farmName[0] = farmPlot.getFarmName();
            });

            new Handler().postDelayed(() -> {
                tabHolder.plotNameTextView.setText(plotName[0]);

                String[] letters = {};

                try{
                    letters = farmName[0].split(" ");
                }catch (Exception e){ }

                if (farmName[0]  != null){

                    String initialLetters = "";

                    if (letters.length == 1){
                         initialLetters =farmName[0].substring(0,2);
                    }else if (letters.length == 2){
                        String firstLetter =letters[0].substring(0,1);
                        String secondLetter =letters[1].substring(0,1);
                         initialLetters = firstLetter + secondLetter;
                    }else if ( letters.length > 2){
                        String firstLetter =letters[0].substring(0,1);
                        String secondLetter =letters[1].substring(0,1);
                        String thirdLetter =letters[1].substring(0,1);
                         initialLetters = firstLetter + secondLetter + thirdLetter;
                    }else{
                        initialLetters = "";
                    }

                    TextDrawable drawable = TextDrawable.builder()
                            .buildRound(initialLetters.toUpperCase(), Color.RED);
                    tabHolder.initialsImageView.setImageDrawable(drawable);

                }
            }, 2000);

            if (name.length() == 10 || name.length() > 10){
                String title = name.substring(0, 10);
                title+="..";
                tabHolder.titleTextView.setText(title);
            }else {
                tabHolder.titleTextView.setText(name);
            }

            tabHolder.startTimeTextView.setText(String.format("%s %s ", mContext.getResources().getString(R.string.starts_on), startTimeText));

            String dateString = new SimpleDateFormat("dd/MM/yyyy hh:mm").format(new Date(farmActivity.getCreatedAt()));

            Log.i(TAG, "onBindViewHolder:  Start Date  " +
                    new SimpleDateFormat("dd/MM/yyyy hh:mm", Locale.ENGLISH).format(new Date(farmActivity.getStartDate())));;

            Log.i(TAG, "onBindViewHolder:  Compare Date " + HelperClass.compareDates(startDateTime, endDateTime));

            tabHolder.createdAtTextView.setText(String.format("Created on %s", dateString));

            tabHolder.plotNameTextView.setPaintFlags(tabHolder.plotNameTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            tabHolder.plotNameTextView.setOnClickListener(v -> activitiesRvListener.onPlotsClicked(plotId));

            tabHolder.moreImageView.setOnClickListener(v -> activitiesRvListener.onMoreClicked(id, name, activityId));

                if ( completed ){
                    tabHolder.statusTextView.setText(mContext.getResources().getString(R.string.completed));
                }else {
                    tabHolder.statusTextView.setText(mContext.getResources().getString(R.string.pending));
                }
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void reloadData(){
        notifyDataSetChanged();
    }

    private class TabHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
        public ProgressBar progressBar;
        public ImageView moreImageView;
        public Button shareButton;
        public TextView titleTextView;
        public TextView startTimeTextView;
        public TextView createdAtTextView;
        public TextView plotNameTextView;
        public TextView statusTextView;
        public ImageView initialsImageView;
        public CardView cardView;

        public TabHolder(View itemView) {
            super(itemView);

            progressBar              = itemView.findViewById(R.id.progress_bar);
            moreImageView       = itemView.findViewById(R.id.more);
            shareButton              = itemView.findViewById(R.id.share_map);
            titleTextView              = itemView.findViewById(R.id.title);
            startTimeTextView   = itemView.findViewById(R.id.start_time);
            createdAtTextView  = itemView.findViewById(R.id.created_at);
            plotNameTextView  = itemView.findViewById(R.id.plot_name);
            statusTextView          = itemView.findViewById(R.id.status);
            initialsImageView      = itemView.findViewById(R.id.initials_image_view);
            cardView                      = itemView.findViewById(R.id.card_view);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

}