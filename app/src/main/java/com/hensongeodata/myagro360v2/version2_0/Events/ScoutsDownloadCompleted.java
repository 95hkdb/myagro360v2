package com.hensongeodata.myagro360v2.version2_0.Events;

public  class ScoutsDownloadCompleted {
      private boolean failed;

      public ScoutsDownloadCompleted(boolean failed) {
            this.failed = failed;
      }

      public boolean isFailed() {
            return failed;
      }

      public void setFailed(boolean failed) {
            this.failed = failed;
      }
}
