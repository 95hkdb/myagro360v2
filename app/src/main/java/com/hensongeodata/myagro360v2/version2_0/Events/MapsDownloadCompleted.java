package com.hensongeodata.myagro360v2.version2_0.Events;

public  class MapsDownloadCompleted {
      private boolean failed;

      public MapsDownloadCompleted(boolean failed) {
            this.failed = failed;
      }

      public boolean isFailed() {
            return failed;
      }

      public void setFailed(boolean failed) {
            this.failed = failed;
      }
}
