package com.hensongeodata.myagro360v2.version2_0.Model;

import com.google.gson.annotations.SerializedName;

public class Point {

      @SerializedName("ID")
      private String id;

      @SerializedName("Longitude")
      private String longitude;

      @SerializedName("Latitude")
      private String latitude;

      @SerializedName("Accuracy")
      private String accuracy;

      @SerializedName("LastEdited")
      private String lastEdited;

      @SerializedName("Created")
      private String created;

      public Point() {
      }

      public String getId() {
            return id;
      }

      public void setId(String id) {
            this.id = id;
      }

      public String getLongitude() {
            return longitude;
      }

      public void setLongitude(String longitude) {
            this.longitude = longitude;
      }

      public String getLatitude() {
            return latitude;
      }

      public void setLatitude(String latitude) {
            this.latitude = latitude;
      }

      public String getAccuracy() {
            return accuracy;
      }

      public void setAccuracy(String accuracy) {
            this.accuracy = accuracy;
      }

      public String getLastEdited() {
            return lastEdited;
      }

      public void setLastEdited(String lastEdited) {
            this.lastEdited = lastEdited;
      }

      public String getCreated() {
            return created;
      }

      public void setCreated(String created) {
            this.created = created;
      }
}
