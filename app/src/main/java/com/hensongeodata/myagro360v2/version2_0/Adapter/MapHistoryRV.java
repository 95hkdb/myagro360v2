package com.hensongeodata.myagro360v2.version2_0.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hensongeodata.myagro360v2.Interface.HistoryTabListener;
import com.hensongeodata.myagro360v2.Interface.ItemTouchHelperViewHolder;
import com.hensongeodata.myagro360v2.Interface.Refresh;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.version2_0.Model.POISModel;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 8/2/2016.
 */

public class MapHistoryRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG = MapHistoryRV.class.getSimpleName();
    private List<MapModel> items;
    private Context mContext;
    private HistoryTabListener listener;
    private Refresh refresh;
    private LocsmmanEngine locsmmanEngine;
    private int mode;
    private ArrayList<String> strPois;
    protected DatabaseHelper databaseHelper;
    private MapDatabase mapDatabase;
    private long perimeter;
    private double inKilometers;
    private double acreage;
    private MyApplication myApplication;

    User mUser;

    public MapHistoryRV(Context context, List<MapModel> items, HistoryTabListener listener, Refresh refreshListener) {
        refresh=refreshListener;
        mContext=context;
        this.items =items;
        this.listener=listener;
        locsmmanEngine=new LocsmmanEngine(context);
        databaseHelper=new DatabaseHelper(mContext);
        mapDatabase = MapDatabase.getInstance(context);
        myApplication = new MyApplication();
    }


    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.map_history, parent, false);
            return new TabHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof TabHolder) {

            MapModel mapModel = items.get(position);

            Log.i(TAG, "onBindViewHolder:  " + mapModel.getName());

            final TabHolder tabHolder = (TabHolder) holder;
//            final long mainId = mapModel.get();
            final String sectionId = mapModel.getId();
            final String name = mapModel.getName();
            final boolean hasSynced = mapModel.synced;

            if (!hasSynced){
                tabHolder.syncImageView.setVisibility(View.VISIBLE);
                tabHolder.syncImageView.setOnClickListener(v -> {
                    refresh.onSyncButtonClicked();
                });
            }

            if (name != null ){
                tabHolder.mapTitleTextView.setText(name);
                tabHolder.moreImageView.setOnClickListener(v -> refresh.moreClicked(name, sectionId));
            }

//            showPerimeter(tabHolder, sectionId);

            tabHolder.mapSizeTextView.setText(String.format("%sm | %skm |  %s acres", String.valueOf(mapModel.getSizeM()), mapModel.getSizeKm(), mapModel.getSizeAc()));


            AppExecutors.getInstance().getNetworkIO().execute(new Runnable() {
                @Override
                public void run() {
                    HelperClass.LoadWebView(tabHolder.webView, sectionId, tabHolder.noPreviewAvailableImageView, mContext);
                }
            });



            tabHolder.moreImageView.setOnClickListener(v -> refresh.moreClicked(name, sectionId));

            tabHolder.shareButton.setOnClickListener(v -> refresh.shareButtonClicked(sectionId));
        }
    }

    private void removeMapping(final int position){
        MapModel mapModel = items.get(position);

        String id        = mapModel.getId();
        String name = mapModel.getName();

        new AlertDialog.Builder(mContext)
                .setTitle(name)
                .setMessage("Do you want to remove this map?")
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if ((new DatabaseHelper(mContext)).removeScoutListItem(id, ChatbotAdapterRV.MODE_MAP)){
                            MyApplication.showToast(mContext, mContext.getResources().getString(R.string.success), Gravity.CENTER);
//                            items.remove(position);
//                            notifyDataSetChanged();

                            refresh.reload(null);
                        }
                        else
                            MyApplication.showToast(mContext,mContext.getResources().getString(R.string.failed), Gravity.CENTER);
                    }
                })
                .show();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class TabHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
        public TextView mapTitleTextView;
        public WebView webView;
        public ProgressBar progressBar;
        public TextView mapSizeTextView;
        public ImageView moreImageView;
        public Button shareButton;
        public ImageView syncImageView;
        public ImageView noPreviewAvailableImageView;

        public TabHolder(View itemView) {
            super(itemView);

            mapTitleTextView = itemView.findViewById(R.id.map_title);
            webView                  = itemView.findViewById(R.id.webview);
            progressBar           = itemView.findViewById(R.id.progress_bar);
            mapSizeTextView = itemView.findViewById(R.id.map_size);
            moreImageView   = itemView.findViewById(R.id.more);
            shareButton           = itemView.findViewById(R.id.share_map);
            syncImageView      = itemView.findViewById(R.id.sync_image);
            noPreviewAvailableImageView = itemView.findViewById(R.id.no_preview_available);

        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

    private double metersToAcres(long meterSquared){
        return Math.round((meterSquared*0.00024711) * 1000.0) / 1000.0;
    }

    private double metersToKilo(long metersSquared){
        return metersSquared*0.001;
    }

    private void showPerimeter(TabHolder tabHolder, String section_id){

        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
            @Override
            public void run() {

                List<POISModel> poisModelList = mapDatabase.poisDaoAccess().fetchPOISByMapId(section_id);

                List<InstructionScout> getAllItems=databaseHelper.getScoutListAllStates(section_id, ChatbotAdapterRV.MODE_MAP);
                strPois =new ArrayList<>();

//        for (InstructionScout instructionScout : getAllItems) {
                for (POISModel poisModel : poisModelList) {
//            if(instructionScout!=null) {
                    if(poisModel!=null) {
                        String message = poisModel.getInfo();
                        if (message != null && !message.trim().isEmpty())
                            strPois.add(message);

                        Log.d(TAG, "run: show Perimeter:  " + message);
                    }
                }

                ArrayList<POI> pois= CreateForm.getInstance(mContext).buildPOI_Mapping(strPois);
                int area=MyApplication.getArea(pois);
                perimeter = MyApplication.getPerimeter(pois);
                inKilometers = metersToKilo(perimeter);
                acreage = metersToAcres(perimeter);

                Log.d(TAG, "run: show Perimeter:  km" + inKilometers + "  acreage" + acreage);
            }
        });

        tabHolder.mapSizeTextView.setText(String.format("%sm | %skm |  %s acres", String.valueOf(perimeter), inKilometers, acreage));

    }

}