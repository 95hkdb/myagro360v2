package com.hensongeodata.myagro360v2.version2_0.Fragment;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Events.MapProgressEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.MapsDownloadCompleted;
import com.hensongeodata.myagro360v2.version2_0.Events.PlotProgressEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.PlotsDownloadCompleted;
import com.hensongeodata.myagro360v2.version2_0.Events.ScanProgressEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.ScoutProgressEvent;
import com.hensongeodata.myagro360v2.version2_0.Events.ScoutsDownloadCompleted;
import com.hensongeodata.myagro360v2.version2_0.sync.map.downloads.MapsDownloadIntentService;
import com.hensongeodata.myagro360v2.version2_0.sync.plots.downloads.PlotsDownloadIntentService;
import com.hensongeodata.myagro360v2.version2_0.sync.scouts.downloads.ScoutsDownloadIntentService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoadUserPropertiesFragment extends Fragment {

      private static final String TAG = LoadUserPropertiesFragment.class.getSimpleName();
      private ProgressBar mapProgressBar;
      private ProgressBar plotProgressBar;
      private ProgressBar scoutProgressBar;
      private ProgressBar scanProgressBar;
      private Dialog progressDialog;
      private TextView dialogContentTextView;
      private String userId;
      private MainViewModel mainViewModel;

      public LoadUserPropertiesFragment() {
            // Required empty public constructor
      }


      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {

            // Inflate the layout for this fragment
            View view = inflater.inflate(R.layout.fragment_load_user_properties, container, false);

            userId = SharePrefManager.getSecondaryUserPref(getContext());

            showProgressDialog();

            mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            mapProgressBar = view.findViewById(R.id.map_progress);
            plotProgressBar = view.findViewById(R.id.plots_progress);
            scoutProgressBar = view.findViewById(R.id.scout_progress);
            scanProgressBar = view.findViewById(R.id.scans_progress);

            Intent intentToSyncImmediately = new Intent(getContext(), MapsDownloadIntentService.class);
            getContext().startService(intentToSyncImmediately);

            return view;
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onMapDownloadCompleted(MapsDownloadCompleted event) {

            Log.i(TAG, "onMapDownloadCompleted:  Completed" );
            downloadPlots();
      }


      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onPlotDownloadCompleted(PlotsDownloadCompleted event) {
            Log.i(TAG, "onPlotDownloadCompleted: ");
            downloadScouts();
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onScoutsDownloadCompleted(ScoutsDownloadCompleted event) {
            Log.i(TAG, "onScoutsDownloadCompleted: ");
            progressDialog.dismiss();

            showCustomDialog();
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onScoutProgressEvent(ScoutProgressEvent event) {
            Log.i(TAG, "onScoutProgressEvent:  " + event.getPercentage());
            scoutProgressBar.setMax(event.getTotal());
            scoutProgressBar.setProgress(event.getPercentage(), true);
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onScanProgressEvent(ScanProgressEvent event) {
            Log.i(TAG, "onScanProgressEvent:  " + event.getPercentage());
            scanProgressBar.setMax(event.getTotal());
            scanProgressBar.setProgress(event.getPercentage(), true);
      }


      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onPlotProgressEvent(PlotProgressEvent event) {
            Log.i(TAG, "onPlotProgressEvent:  " + event.getPercentage());
            plotProgressBar.setMax(event.getTotal());
            plotProgressBar.setProgress(event.getPercentage(), true);
      }

      @Subscribe(threadMode = ThreadMode.MAIN)
      public void onMapProgressEvent(MapProgressEvent event) {
            Log.i(TAG, "onMapProgressEvent:  " + event.getPercentage());
            mapProgressBar.setMax(event.getTotal());
            mapProgressBar.setProgress(event.getPercentage(), true);
      }

      private void downloadPlots() {
            Intent intentToSyncImmediately = new Intent(getContext(), PlotsDownloadIntentService.class);
            getContext().startService(intentToSyncImmediately);
      }

      private void downloadScouts() {
            Intent intentToSyncImmediately = new Intent(getContext(), ScoutsDownloadIntentService.class);
            getContext().startService(intentToSyncImmediately);
      }

      @Override
      public void onStart() {
            super.onStart();
            EventBus.getDefault().register(this);
      }

      @Override
      public void onStop() {
            EventBus.getDefault().unregister(this);
            super.onStop();
      }

      private void showProgressDialog() {
            progressDialog = new Dialog(getContext());
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            progressDialog.setContentView(R.layout.dialog_user_downloads_progress);
            progressDialog.setCancelable(false);
            dialogContentTextView = progressDialog.findViewById(R.id.dialog_content_text_view);

            dialogContentTextView.setText(String.format("Downloading %s resources", userId));

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(progressDialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            progressDialog.show();
            progressDialog.getWindow().setAttributes(lp);
      }

      private void showCustomDialog() {
            final Dialog dialog = new Dialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            dialog.setContentView(R.layout.dialog_info);
            dialog.setCancelable(true);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            dialog.findViewById(R.id.bt_close).setOnClickListener(v -> {
//                  Toast.makeText(getContext(), ((AppCompatButton) v).getText().toString() + " Clicked", Toast.LENGTH_SHORT).show();
                  dialog.dismiss();
            });

            dialog.show();
            dialog.getWindow().setAttributes(lp);
      }

}
