package com.hensongeodata.myagro360v2.version2_0.Fragment.Plots;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.JsonElement;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Fragment.ChooseMapDialogFragment;
import com.hensongeodata.myagro360v2.version2_0.Model.Crop;
import com.hensongeodata.myagro360v2.version2_0.Model.Farm;
import com.hensongeodata.myagro360v2.version2_0.Model.FarmPlot;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static io.fabric.sdk.android.Fabric.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddPlotDialogFragment extends DialogFragment {

      private static final String MAP_ID = "map_id" ;
      private static final String PLOT_SIZE = "plot_size" ;
      private MapDatabase mapDatabase;
      private MainViewModel mainViewModel;
      private AgroWebAPI mAgroWebAPI;
      private AppCompatSpinner spn_crops;
      private AppCompatSpinner spn_maps;
      private AppCompatSpinner spn_farms;
      private RadioGroup cropRadioGroup;
      private HashMap<Integer, String> spinnerCropsMap;
      private HashMap<Integer, String> spinnerFarmsMap;
      private LinearLayout cropSpinnerLinearLayout;
      private TextView cropTypeTitleTextView;
      private AppCompatImageButton btnClose;
      private AppCompatButton btnSave;
      private ImageButton addNewfarm;
      ArrayList<HashMap<String,String>>maps;
      String mMapId;
      double mPlotSize;
      private String mCropId;
      private String mFarmId;
      private String cropType;
      MyApplication myApplication;
      private OnAddDialogFragmentListener mOnAddDialogFragmentListener;

      public AddPlotDialogFragment() { }

      public static AddPlotDialogFragment newInstance(String mapId, double plotSize){
            AddPlotDialogFragment fragment = new AddPlotDialogFragment();
            Bundle args = new Bundle();
            args.putString(MAP_ID, mapId);
            args.putDouble(PLOT_SIZE, plotSize);
            fragment.setArguments(args);
            return fragment;
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            myApplication = new MyApplication();
            mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            mAgroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);
            mapDatabase = MapDatabase.getInstance(getContext());
            View view =  inflater.inflate(R.layout.fragment_add_plot_dialog, container, false);
            cropSpinnerLinearLayout = view.findViewById(R.id.crop_spinner_linear_layout);
            cropTypeTitleTextView = view.findViewById(R.id.crop_type_title);
            spn_crops = view.findViewById(R.id.spn_crops);
            spn_farms = view.findViewById(R.id.spn_farms);
            spn_maps  = view.findViewById(R.id.spn_maps);
            cropRadioGroup = view.findViewById(R.id.cropRadioGroup);
            btnClose    = view.findViewById(R.id.bt_close);
            btnSave    = view.findViewById(R.id.bt_save);
            addNewfarm = view.findViewById(R.id.add_new_farm);


            EditText nameEditText = view.findViewById(R.id.plot_name);
            EditText descriptionEditText = view.findViewById(R.id.description);
            EditText sizeEditText = view.findViewById(R.id.size);
            sizeEditText.setText(String.valueOf(mPlotSize));
            Log.i(TAG, "onCreateView:  Plot Size " + mPlotSize);

            sizeEditText.setEnabled(false);

//            mFarmId = spinnerFarmsMap.get(spn_farms.getSelectedItemPosition());

            final Farm[] farm = new Farm[1];

            spn_farms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mFarmId = spinnerFarmsMap.get(spn_farms.getSelectedItemPosition());

                        Log.i(TAG, "onItemSelected:  Farm ID " + mFarmId );

                        AppExecutors.getInstance().getDiskIO().execute(() -> {
                              farm[0] = mapDatabase.farmDaoAccess().fetchFarmByFarmId(mFarmId);
                              Log.i(TAG, "run:  Plot " + farm[0] + " Farm ID " + mFarmId);
                        });
                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) {}
            });

            spn_crops.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mCropId = spinnerCropsMap.get(spn_crops.getSelectedItemPosition());
                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) { }
            });

            cropSpinnerLinearLayout.setVisibility(View.GONE);

            cropRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                  Log.i(TAG, "onCheckedChanged:  " + checkedId);

                  switch(checkedId) {
                        case R.id.radio_crop:
                              cropType = "crop";
                              cropSpinnerLinearLayout.setVisibility(View.VISIBLE);
                              cropTypeTitleTextView.setText("Crop");
                              break;
                        case R.id.radio_livestock:
                              cropType = "livestock";
                              cropSpinnerLinearLayout.setVisibility(View.VISIBLE);
                              cropTypeTitleTextView.setText("Livestock");
                              break;
                  }
            });

            btnClose.setOnClickListener(v -> dismiss());

            btnSave.setOnClickListener(v -> {

                  final String name = nameEditText.getText().toString();
                  final String description = descriptionEditText.getText().toString();
                  final String mapId = mMapId;
                  final double plotSize = mPlotSize;
                  final String cropId  = mCropId;
                  final String farmId   = mFarmId;

                  final long   farmPrimaryId = farm[0] != null ? farm[0].getFarmId() : 0;

                  String orgId = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);

                  String userId = "";

                  if (SharePrefManager.isSecondaryUserAvailable(getContext())){
                        userId = SharePrefManager.getSecondaryUserPref(getContext());
                  }else {
                        userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
                  }

//              String userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);

                  FarmPlot farmPlot = new FarmPlot();
                  farmPlot.setName(name);
                  farmPlot.setDescription(description);
                  farmPlot.setSize(0);
                  farmPlot.setSizeM(0);
                  farmPlot.setSizeKm(0);
                  farmPlot.setSizeAc(plotSize);
                  farmPlot.setMapId(mapId);
                  farmPlot.setCropName(spn_crops.getSelectedItem().toString());
                  farmPlot.setCropId(cropId);
                  farmPlot.setOrgId(orgId);
                  farmPlot.setUserId(userId);
                  farmPlot.setSynced(false);
                  farmPlot.setFarmId(farmId);
                  farmPlot.setPrimaryFarmId(farmPrimaryId);
                  farmPlot.setFarmName(spn_farms.getSelectedItem().toString());
                  farmPlot.setCreatedAt(System.currentTimeMillis());
                  farmPlot.setUpdatedAt(System.currentTimeMillis());

                  Log.i(TAG, "AddPlot Name :  " + name);
                  Log.i(TAG, "AddPlot Description :  " + description);
                  Log.i(TAG, "AddPlot Size :  " + plotSize);
                  Log.i(TAG, "AddPlot Kilo :  " + "");
                  Log.i(TAG, "AddPlot Acres:  " + (plotSize));
                  Log.i(TAG, "AddPlot Crop Name:  " + spn_crops.getSelectedItem().toString());
                  Log.i(TAG, "AddPlot Crop ID:  " + cropId);
                  Log.i(TAG, "AddPlot Org ID:  " + orgId);
                  Log.i(TAG, "AddPlot Farm ID:  " + farmId);
                  Log.i(TAG, "AddPlot Farm Name:  " + spn_farms.getSelectedItem().toString());

                  if (name.isEmpty()){
                        nameEditText.setError("Field required!");
                  }

                  if (description.isEmpty()){
                        descriptionEditText.setError("Field required!");
                  }

                  if (!name.isEmpty() && !description.isEmpty()){

                        String finalUserId = userId;
                        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                              @Override
                              public void run() {
                                    try{
//                                          PlotSyncUtil.initialize(getContext());

                                          mAgroWebAPI.createFarmPlot(name, description, plotSize,mapId,cropId,orgId, finalUserId,farmId).enqueue(new Callback<JsonElement>() {
                                                @Override
                                                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                                                      String plotId = null;
                                                      if (response.body() != null) {
                                                            plotId = response.body().getAsJsonObject().get("plot_id").getAsString();
                                                      }

                                                      Log.i(TAG, "onResponse:  Plot ID " + plotId);

                                                      farmPlot.setId(plotId);

                                                      AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                  mapDatabase.plotDaoAccess().insert(farmPlot);
                                                            }
                                                      });

                                                      Log.i(TAG, "onResponse:  Create Farm Plot " + plotId);
                                                }

                                                @Override
                                                public void onFailure(Call<JsonElement> call, Throwable t) {

                                                      Log.e(TAG, "onFailure:  Create Farm Plot " + t.getMessage() );

                                                }
                                          });

//                                          Intent intentToSyncImmediately = new Intent(getContext(), PlotSyncIntentService.class);
//                                          getContext().startService(intentToSyncImmediately);

                                    }catch (Exception e){
                                          Log.e(TAG, "run:  Insert Plot Error" + e.getMessage() );
                                    }
                              }
                        });

                        onSave();
                        Toast.makeText(getContext(), "Successfully added a new plot!", Toast.LENGTH_SHORT).show();
                        dismiss();
                  }
            });


            addNewfarm.setOnClickListener(v -> {
                  showAddNewFarmFragment("");
            });

            getCropsFromDatabase();
            getFarmsFromDB();
            return view;
      }


      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                  mMapId = getArguments().getString(MAP_ID);
                  mPlotSize = getArguments().getDouble(PLOT_SIZE);
            }
      }

      @NotNull
      @Override
      public Dialog onCreateDialog(Bundle savedInstanceState) {
            Dialog dialog = super.onCreateDialog(savedInstanceState);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            return dialog;
      }

      public void onSave() {
            if (mOnAddDialogFragmentListener != null) {
                  mOnAddDialogFragmentListener.onSave();
            }
      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof ChooseMapDialogFragment.OnChooseMapFragmentListener) {
                  mOnAddDialogFragmentListener = (AddPlotDialogFragment.OnAddDialogFragmentListener) context;
            } else {
                  throw new RuntimeException(context.toString()
                          + " must implement OnAddDialogFragmentListener");
            }
      }

      @Override
      public void onDetach() {
            super.onDetach();
            mOnAddDialogFragmentListener = null;
      }

      private void getFarmsFromDB() {

            mainViewModel.getFarms().observe(getActivity(), new androidx.lifecycle.Observer<List<Farm>>() {
                  @Override
                  public void onChanged(List<Farm> farms) {

                        String[] spinnerArray = new String[farms.size()];
                        try {

                              spinnerFarmsMap = new HashMap<Integer, String>();
                              for ( int i =0; i < farms.size(); i++ ){
                                    spinnerFarmsMap.put(i, String.valueOf(farms.get(i).getId()));
                                    spinnerArray[i] = farms.get(i).getName();
                              }

                              setupArrayAdapter(spinnerArray, spn_farms);

                              Log.i(TAG, "onChanged:  Farms " + farms.size());

                        } catch (Exception e) {
                              Log.e(TAG, "onNext:  " + e.getMessage());
                        }

                  }
            });
      }

      private void getCropsFromDatabase(){
            mainViewModel.getCrops().observe(getActivity(), new androidx.lifecycle.Observer<List<Crop>>() {
                  @Override
                  public void onChanged(List<Crop> crops) {

                        String[] spinnerArray = new String[crops.size()];
                        try {

                              spinnerCropsMap = new HashMap<Integer, String>();
                              for ( int i =0; i < crops.size(); i++ ){
                                    spinnerCropsMap.put(i, String.valueOf(crops.get(i).getId()));
                                    spinnerArray[i] = crops.get(i).getName();
                              }

                              setupArrayAdapter(spinnerArray, spn_crops);

                        } catch (Exception e) {
                              Log.e(TAG, "onNext:  " + e.getMessage());
                        }
                  }
            });
      }

      private void showAddNewFarmFragment(String origination) {
            final Dialog dialog = new Dialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            dialog.setContentView(R.layout.fragment_add_farm_dialog);
            dialog.setCancelable(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


            dialog.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                        dialog.dismiss();
                  }
            });

            dialog.findViewById(R.id.continue_button).setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {

                        EditText editTextName = dialog.findViewById(R.id.farm_name);

                        final String name = editTextName.getText().toString();

                        if (name.isEmpty()){
                              editTextName.setError("Field required!");
                        }


                        if (!name.isEmpty()){
                              Toast.makeText(getContext(), "adding new farm..", Toast.LENGTH_SHORT).show();
                              dialog.findViewById(R.id.continue_button).setVisibility(View.GONE);
                              dialog.findViewById(R.id.loading_indicator).setVisibility(View.VISIBLE);

                              new Handler().postDelayed(() -> {

                                    String userId = "";

                                    if (SharePrefManager.isSecondaryUserAvailable(getContext())){
                                          userId = SharePrefManager.getSecondaryUserPref(getContext());
                                    }else {
                                          userId = SharePrefManager.getInstance(getContext()).getUserDetails().get(0);
                                    }

                                    String orgId = SharePrefManager.getInstance(getContext()).getOrganisationDetails().get(0);

                                    Farm farmModel = new Farm();
                                    farmModel.setName(name);
                                    farmModel.setUserId(userId);
                                    farmModel.setOrgId(orgId);

                                    AppExecutors.getInstance().getDiskIO().execute(() -> mapDatabase.farmDaoAccess().insert(farmModel));
                                    dialog.dismiss();
                              }, 2500);
                        }
                  }
            });

            dialog.show();
            dialog.getWindow().setAttributes(lp);
      }

      private void setupArrayAdapter(String[] stringArray, AppCompatSpinner spinner){
            ArrayAdapter<String> array = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_item, stringArray);
            array.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(array);
      }

      public interface OnAddDialogFragmentListener{
            void onSave();
      }
}
