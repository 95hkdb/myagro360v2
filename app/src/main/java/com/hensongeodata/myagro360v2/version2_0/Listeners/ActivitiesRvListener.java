package com.hensongeodata.myagro360v2.version2_0.Listeners;

public interface ActivitiesRvListener {

      void onPlotsClicked(long plotId);

      void onMoreClicked(String activityId, String name, long id);

}
