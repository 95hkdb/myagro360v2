package com.hensongeodata.myagro360v2.version2_0.Adapter;

import android.content.Context;;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.version2_0.Model.Member;

import java.util.ArrayList;
import java.util.List;

//public class AdapterListBasic extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
public class AdapterListBasic extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

      private List<Member> items = new ArrayList<>();

      private Context ctx;
      private OnItemClickListener mOnItemClickListener;

      public interface OnItemClickListener {
            void onItemClick(View view, Member obj, int position);
      }

      public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
            this.mOnItemClickListener = mItemClickListener;
      }

      public AdapterListBasic(Context context, List<Member> items) {
            this.items = items;
            ctx = context;
      }

      public class OriginalViewHolder extends RecyclerView.ViewHolder {
            public ImageView image;
            public TextView name;
            public TextView email;
            public TextView orgId;
            public View lyt_parent;

            public OriginalViewHolder(View v) {
                  super(v);
                  image = v.findViewById(R.id.image);
                  name = v.findViewById(R.id.name);
                  email = v.findViewById(R.id.email);
                  orgId   = v.findViewById(R.id.org_id);
                  lyt_parent = v.findViewById(R.id.lyt_parent);
            }
      }

      @Override
      public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder vh;
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_people_chat, parent, false);
            vh = new OriginalViewHolder(v);
            return vh;
      }

      // Replace the contents of a view (invoked by the layout manager)
      @Override
      public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            if (holder instanceof OriginalViewHolder) {
                  OriginalViewHolder view = (OriginalViewHolder) holder;

                  Member p = items.get(position);
                  view.name.setText(String.format("%s %s", p.getFirstName(), p.getLastName()));
                  view.email.setText(p.getEmail());
                  view.orgId.setText(String.format("Org ID: %s", p.getOrgId()));
//                  Tools.displayImageRound(ctx, view.image, p.image);
//                  Tools.displayImageOriginal(ctx, view.image, p.);
                  view.lyt_parent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                              if (mOnItemClickListener != null) {
                                    mOnItemClickListener.onItemClick(view, items.get(position), position);
                              }
                        }
                  });
            }
      }

      @Override
      public int getItemCount() {
            return items.size();
      }

}
