package com.hensongeodata.myagro360v2.version2_0.sync.plots.downloads;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

public class PlotsDownloadIntentService extends IntentService {

      public PlotsDownloadIntentService() {
            super("PlotsDownloadIntentService");
      }

      @Override
      protected void onHandleIntent(@Nullable Intent intent) {
            PlotsDownloadTask.DownloadAllPlots(this);
            PlotsDownloadTask.pullFarms(this);
      }
}
