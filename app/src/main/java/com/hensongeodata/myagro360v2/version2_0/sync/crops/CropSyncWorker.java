package com.hensongeodata.myagro360v2.version2_0.sync.crops;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.hensongeodata.myagro360v2.version2_0.sync.DBSyncTask;

public class CropSyncWorker extends Worker {
      private static final String TAG = Worker.class.getSimpleName();

      public CropSyncWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
            super(context, workerParams);
      }

      @NonNull
      @Override
      public Result doWork() {
            DBSyncTask.syncCrops(getApplicationContext());
            return null;
      }

}
