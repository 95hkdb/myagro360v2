package com.hensongeodata.myagro360v2.version2_0.sync.map;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import org.json.JSONException;

public class MapSyncWorker  extends Worker {
      private static final String TAG = Worker.class.getSimpleName();

      public MapSyncWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
            super(context, workerParams);
      }

      @NonNull
      @Override
      public Result doWork() {
            try {
                  MapSyncTask.syncAllMaps(getApplicationContext());
            } catch (JSONException e) {
                  Log.e(TAG, "doWork:  Sync All Maps" + e.getMessage() );
                  e.printStackTrace();
            }
            return null;
      }

}
