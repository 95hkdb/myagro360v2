package com.hensongeodata.myagro360v2.version2_0.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hensongeodata.myagro360v2.Interface.ItemTouchHelperViewHolder;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.version2_0.CommentPageActivity;
import com.hensongeodata.myagro360v2.version2_0.Events.CommentClickedEvent;
import com.hensongeodata.myagro360v2.version2_0.Model.ThreadModel;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;
import com.hensongeodata.myagro360v2.version2_0.Util.TimeAgo;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;
import org.ocpsoft.prettytime.PrettyTime;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class ThreadRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG = ThreadRecyclerViewAdapter.class.getSimpleName();
    private List<ThreadModel> items;
    private Context mContext;
    private int mode;
    protected DatabaseHelper databaseHelper;
    private MapDatabase mapDatabase;
    private MyApplication myApplication;

    public ThreadRecyclerViewAdapter(Context context, List<ThreadModel> items) {
        mContext=context;
        this.items =items;
//        databaseHelper=new DatabaseHelper(mContext);
        mapDatabase = MapDatabase.getInstance(context);
    }


    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.thread_history, parent, false);
            return new TabHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof TabHolder) {

            ThreadModel threadModel = items.get(position);

            Log.i(TAG, "onBindViewHolder:  Image Path" + threadModel.getImageUrl());

            final TabHolder tabHolder = (TabHolder) holder;

            tabHolder.threadContentTextView.setText(threadModel.getContent());
            tabHolder.threadCreatorNameTextView.setText(threadModel.getCreatorName());

            if (threadModel.getComments().size()> 1){
                tabHolder.threadCommentsCountTextView.setText(threadModel.getComments().size() + " Comment(s)");
            }else {
                tabHolder.threadCommentsCountTextView.setText("Add your comment");
            }

            PrettyTime p = new PrettyTime();
            System.out.println(p.format(new Date()));

            String dateCreatedString = threadModel.getDateCreated();

            LocalDate localDate = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            }

            long milliseconds = HelperClass.convertDateTimeToMilliseconds(dateCreatedString);

            String timeAgo = TimeAgo.getTimeAgo(milliseconds);

            tabHolder.timeAgoTextView.setText(timeAgo);

            tabHolder.threadCommentsCountTextView.setOnClickListener(v -> {
                EventBus.getDefault().post(new CommentClickedEvent(threadModel));

                Intent intent = new Intent(mContext, CommentPageActivity.class);
                intent.putExtra("thread_id", threadModel.getId());
                mContext.startActivity(intent);
            });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class TabHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
//        public TextView scoutTitleTextView;
        public TextView threadContentTextView;
        public TextView threadCreatorNameTextView;
        public TextView threadCommentsCountTextView;
        public TextView timeAgoTextView;


        public TabHolder(View itemView) {
            super(itemView);

            threadContentTextView = itemView.findViewById(R.id.thread_content_text_view);
            threadCreatorNameTextView = itemView.findViewById(R.id.creator_name_text_view);
            threadCommentsCountTextView = itemView.findViewById(R.id.thread_comments_count_text_view);
            timeAgoTextView                               = itemView.findViewById(R.id.time_ago);

        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

    private double metersToAcres(long meterSquared){
        return Math.round((meterSquared*0.00024711) * 1000.0) / 1000.0;
    }

    private double metersToKilo(long metersSquared){
        return metersSquared*0.001;
    }

    private void showPerimeter(TabHolder tabHolder, String section_id){


//        tabHolder.mapSizeTextView.setText(String.format("%sm | %skm |  %s acres", String.valueOf(perimeter), inKilometers, acreage));

    }

}