package com.hensongeodata.myagro360v2.version2_0.sync.farm;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class FarmSyncWorker extends Worker {
      private static final String TAG = FarmSyncWorker.class.getSimpleName();

      public FarmSyncWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
            super(context, workerParams);
      }

      @NonNull
      @Override
      public Result doWork() {
            FarmSyncTask.pullFarms(getApplicationContext());
            return null;
      }

}
