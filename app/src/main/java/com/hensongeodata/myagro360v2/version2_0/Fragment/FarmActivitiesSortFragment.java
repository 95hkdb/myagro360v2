package com.hensongeodata.myagro360v2.version2_0.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.AgroWebAPI;
import com.hensongeodata.myagro360v2.api.response.FarmActivityResponse;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.data.MapDatabase;
import com.hensongeodata.myagro360v2.data.viewModels.MainViewModel;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.helper.Tools;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.version2_0.Model.Crop;
import com.hensongeodata.myagro360v2.version2_0.Model.Farm;
import com.hensongeodata.myagro360v2.version2_0.Util.HelperClass;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static io.fabric.sdk.android.Fabric.TAG;

public class FarmActivitiesSortFragment extends DialogFragment {
      private static final String MAP_ID = "map_id" ;
      private static final String PLOT_SIZE = "plot_size" ;
      private MapDatabase mapDatabase;
      private MainViewModel mainViewModel;
      private AgroWebAPI mAgroWebAPI;
      private AppCompatSpinner spn_crops;
      private AppCompatSpinner spn_maps;
      private AppCompatSpinner spn_farms;
      private AppCompatSpinner spn_sortOptions;
      private AppCompatSpinner spn_activities;

      private Button spn_from_date, spn_from_time;
      private Button spn_to_date, spn_to_time;

      private RadioGroup cropRadioGroup;
      private HashMap<Integer, String> spinnerCropsMap;
      private HashMap<Integer, String> spinnerSortsMap;
      private LinearLayout cropSpinnerLinearLayout;
      private TextView cropTypeTitleTextView;
      private AppCompatImageButton btnClose;
      private AppCompatButton btnSave;
      ArrayList<HashMap<String,String>> maps;
      private HashMap<Integer, String> activitiesHashMap;
      private HashMap<Integer, String> spinnerFarmsMap;

      String mMapId;
      double mPlotSize;
      private String mCropId;
      private String mFarmId;
      private int currentSortSelection;
      private LinearLayout cropRadioLayout;
      private LinearLayout activityLinearLayout;
      private RadioButton radioCropButton;
      private String cropType;
      private OnFarmActivitiesSortFragmentListener onFarmActivitiesSortFragmentListener;
      private HashMap<Integer, String> plotsSpinnerHashMap;
      private AppCompatSpinner plots_spinner;
      private LinearLayout plotsSpinnerLayout;
      private LinearLayout farmsSpinnerLayout;
      private LinearLayout dateLayout;

      public FarmActivitiesSortFragment() { }

      public static FarmActivitiesSortFragment newInstance(String mapId, double plotSize){
            FarmActivitiesSortFragment fragment = new FarmActivitiesSortFragment();
            Bundle args = new Bundle();
            args.putString(MAP_ID, mapId);
            args.putDouble(PLOT_SIZE, plotSize);
            fragment.setArguments(args);
            return fragment;
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
            mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
            mAgroWebAPI = NetworkRequest.getRetrofit(Keys.AGRO_BASE).create(AgroWebAPI.class);
            mapDatabase = MapDatabase.getInstance(getContext());
            View view =  inflater.inflate(R.layout.fragment_farm_activities_sort, container, false);
            cropSpinnerLinearLayout = view.findViewById(R.id.crop_spinner_linear_layout);
            cropTypeTitleTextView = view.findViewById(R.id.crop_type_title);
            spn_crops = view.findViewById(R.id.spn_crops);
            spn_sortOptions = view.findViewById(R.id.spn_sort_options);
            spn_maps  = view.findViewById(R.id.spn_maps);
            spn_farms = view.findViewById(R.id.spn_farms);
            spn_activities = view.findViewById(R.id.spn_activities);
            plots_spinner    = view.findViewById(R.id.spn_plots);
            cropRadioGroup = view.findViewById(R.id.cropRadioGroup);
            cropRadioLayout = view.findViewById(R.id.crop_radio_layout);
            activityLinearLayout = view.findViewById(R.id.activity_layout);
            plotsSpinnerLayout = view.findViewById(R.id.plots_spinner_layout);
            farmsSpinnerLayout = view.findViewById(R.id.farm_layout);
            dateLayout                   = view.findViewById(R.id.date_layout);
            spn_from_date  = view.findViewById(R.id.spn_from_date);
            spn_from_time  = view.findViewById(R.id.spn_from_time);
            spn_to_date       = view.findViewById(R.id.spn_to_date);
            spn_to_time       = view.findViewById(R.id.spn_to_time);
            radioCropButton   = view.findViewById(R.id.radio_crop);
            btnClose    = view.findViewById(R.id.bt_close);
            btnSave    = view.findViewById(R.id.bt_save);

            Log.i(TAG, "onCreateView:  Plot Size " + mPlotSize);

            final Farm[] farm = new Farm[1];

            setCurrentSortSelection();

            spn_from_date.setOnClickListener(v -> dialogDatePickerLight((Button) v));

            spn_to_date.setOnClickListener(v -> dialogDatePickerLight((Button) v));

            spn_sortOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        Log.i(TAG, "onItemSelected:  " + id);

                        switch((int) id){
                              case 0:
                                    sortByPlot();
                                    break;

                              case 1:
                                    sortByFarm();
                                    break;

                              case 2:
                                    sortByCompleted();
                                    cropRadioLayout.setVisibility(View.GONE);
                                    break;

                              case 3:
                                    sortByPending();
                                    cropRadioLayout.setVisibility(View.GONE);
                                    break;

                              case 4:
                                    sortByActivityType();
                                    cropRadioLayout.setVisibility(View.GONE);
                                    break;

                              case 5:
                                    sortByDateCreated();
                                    cropRadioLayout.setVisibility(View.GONE);
                                    break;

                              case 6:
                                    sortByAll();
                                    cropRadioLayout.setVisibility(View.GONE);
                                    break;
                        }

                        mFarmId = spinnerSortsMap.get(spn_sortOptions.getSelectedItemPosition());

                        Log.i(TAG, "onItemSelected:  Farm ID " + mFarmId );

                        AppExecutors.getInstance().getDiskIO().execute(() -> {
                              farm[0] = mapDatabase.farmDaoAccess().fetchFarmByFarmId(mFarmId);
                              Log.i(TAG, "run:  Plot " + farm[0] + " Farm ID " + mFarmId);
                        });
                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) {}
            });

            spn_crops.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mCropId = spinnerCropsMap.get(spn_crops.getSelectedItemPosition());

                        SharePrefManager.setCurrentCropSelection(getContext(), id);

                        if (SharePrefManager.isCropTypePreferenceAvailable(getContext())) {
                              SharePrefManager.clearCropTypePreference(getContext());
                              SharePrefManager.setSortCropPreference(getContext(), mCropId);
                        }

                        SharePrefManager.setSortCropPreference(getContext(), mCropId);

                        Log.i(TAG, "onItemSelected:  Crop Id " + mCropId  );

                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) { }
            });

            cropSpinnerLinearLayout.setVisibility(View.GONE);

            cropRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                  Log.i(TAG, "onCheckedChanged:  " + checkedId);

                  switch(checkedId) {
                        case R.id.radio_crop:
                              cropType = "crop";
                              cropSpinnerLinearLayout.setVisibility(View.VISIBLE);
                              cropTypeTitleTextView.setText("Crop");
                              break;
                        case R.id.radio_livestock:
                              cropType = "livestock";
                              cropSpinnerLinearLayout.setVisibility(View.VISIBLE);
                              cropTypeTitleTextView.setText("Livestock");
                              break;
                  }
            });

            spn_activities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String activityId = activitiesHashMap.get(spn_activities.getSelectedItemPosition());

                        SharePrefManager.setCurrentActivitySelection(getContext(), id);

                        if (SharePrefManager.isActivityTypePreferenceAvailable(getContext())) {
                              SharePrefManager.clearActivityTypePreference(getContext());
                              SharePrefManager.setSortActivityTypePreference(getContext(), activityId);
                        }

                        SharePrefManager.setSortActivityTypePreference(getContext(), activityId);

                        Log.i(TAG, "onItemSelected:  Activity  Id " + activityId  );
                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) {}
            });

            plots_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String plotId = plotsSpinnerHashMap.get(plots_spinner.getSelectedItemPosition());
                        SharePrefManager.setSelectedPreference(getContext(), id, SharePrefManager.CURRENT_PLOT_SELECTION_ACTIVITY);

                        if (SharePrefManager.isSortParameterAvailable(getContext(), SharePrefManager.SORT_BY_PLOT_TYPE_ACTIVITY)) {
                              SharePrefManager.clearSortParameter(getContext(), SharePrefManager.SORT_BY_PLOT_TYPE_ACTIVITY);
                              SharePrefManager.setSortParameterPreference(getContext(), plotId, SharePrefManager.SORT_BY_PLOT_TYPE_ACTIVITY);
                        }

                        SharePrefManager.setSortParameterPreference(getContext(), plotId, SharePrefManager.SORT_BY_PLOT_TYPE_ACTIVITY);

                        Log.i(TAG, "onItemSelected:  Plot Id " + plotId  );
                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) {}
            });

            spn_farms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String farmId = spinnerFarmsMap.get(spn_farms.getSelectedItemPosition());
                        SharePrefManager.setSelectedPreference(getContext(), id, SharePrefManager.CURRENT_FARM_SELECTION_ACTIVITY);

                        if (SharePrefManager.isSortParameterAvailable(getContext(), SharePrefManager.SORT_BY_FARM_TYPE_ACTIVITY)) {
                              SharePrefManager.clearSortParameter(getContext(), SharePrefManager.SORT_BY_FARM_TYPE_ACTIVITY);
                              SharePrefManager.setSortParameterPreference(getContext(), farmId, SharePrefManager.SORT_BY_FARM_TYPE_ACTIVITY);
                        }

                        SharePrefManager.setSortParameterPreference(getContext(), farmId, SharePrefManager.SORT_BY_FARM_TYPE_ACTIVITY);

                        Log.i(TAG, "onItemSelected:  Farm Id " + farmId  );
                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) {}
            });

            btnClose.setOnClickListener(v -> {
                  onDismiss();
                  dismiss();
            });

            btnSave.setOnClickListener(v -> {

                  String fromDateText = spn_from_date.getText().toString();
                  String toDateText = spn_to_date.getText().toString();

                  String extractFromMonth = fromDateText.substring(4, 8);
                  String extractFromDay = fromDateText.substring(9, 11);
                  String extractFromYear = fromDateText.substring(12, 16);
                  String reformatedFromDateString = extractFromDay + "-" +  HelperClass.getMonthNumber(extractFromMonth) + "-" + extractFromYear;
                  long fromDateMilliseconds = HelperClass.convertDateToMilliseconds(reformatedFromDateString);

                  if (SharePrefManager.isSortParameterAvailable(getContext(), SharePrefManager.SORT_BY_FROM_DATE_TYPE_ACTIVITY)){
                        SharePrefManager.clearSortParameter(getContext(),SharePrefManager.SORT_BY_FROM_DATE_TYPE_ACTIVITY );
                        SharePrefManager.setSortParameterPreference(getContext(), String.valueOf(fromDateMilliseconds), SharePrefManager.SORT_BY_FROM_DATE_TYPE_ACTIVITY);
                  }


                  SharePrefManager.setSortParameterPreference(getContext(), String.valueOf(fromDateMilliseconds), SharePrefManager.SORT_BY_FROM_DATE_TYPE_ACTIVITY);

                  String extractToMonth = toDateText.substring(4, 8);
                  String extractToDay = toDateText.substring(9, 11);
                  String extractToYear = toDateText.substring(12, 16);
                  String reformattedToDateString = extractToDay + "-" + HelperClass.getMonthNumber(extractToMonth) + "-" + extractToYear;
                  long toDateMilliseconds = HelperClass.convertDateToMilliseconds(reformattedToDateString);

                  if (SharePrefManager.isSortParameterAvailable(getContext(), SharePrefManager.SORT_BY_END_DATE_TYPE_ACTIVITY)){
                        SharePrefManager.clearSortParameter(getContext(),SharePrefManager.SORT_BY_END_DATE_TYPE_ACTIVITY );
                        SharePrefManager.setSortParameterPreference(getContext(), String.valueOf(toDateMilliseconds), SharePrefManager.SORT_BY_END_DATE_TYPE_ACTIVITY);
                  }

                  SharePrefManager.setSortParameterPreference(getContext(), String.valueOf(toDateMilliseconds), SharePrefManager.SORT_BY_END_DATE_TYPE_ACTIVITY);

                  onSave();
                  dismiss();
            });

            getCropsFromDatabase();
            getSortOptions();
            getActivitiesFromAPI();
            fetchPlotsFromDatabase();
            getFarmsFromDB();

            spn_sortOptions.setSelection(currentSortSelection, true);
//            spn_crops.setSelection((int) SharePrefManager.getCurrentCropSelection(getContext()));

            spn_activities.setSelection((int) SharePrefManager.getCurrentActivitySelection(getContext()), true);
            plots_spinner.setSelection((int) SharePrefManager.getSelectedPreference(getContext(), SharePrefManager.CURRENT_PLOT_SELECTION_ACTIVITY), true);
            spn_farms.setSelection((int) SharePrefManager.getSelectedPreference(getContext(), SharePrefManager.CURRENT_FARM_SELECTION_ACTIVITY), true);
            return view;
      }

      private void setCurrentSortSelection() {
            switch (SharePrefManager.getSortActivitiesPreference(getContext())){

                  case SharePrefManager.SORT_BY_PLOT_ACTIVITY:
                        currentSortSelection = 0;
                        break;

                  case SharePrefManager.SORT_BY_FARM_ACTIVITY:
                        currentSortSelection = 1;
                        break;

                  case SharePrefManager.SORT_BY_COMPLETED_ACTIVITY:
                        currentSortSelection = 2;
                        break;

                  case SharePrefManager.SORT_BY_PENDING_ACTIVITY:
                        currentSortSelection = 3;
                        break;

                  case SharePrefManager.SORT_BY_TYPE_ACTIVITY:
                        currentSortSelection = 4;
                        break;

                  case SharePrefManager.SORT_BY_DATE_CREATED_ACTIVITY:
                        currentSortSelection = 5;
                        break;

                  case SharePrefManager.SORT_BY_ALL_ACTIVITY:
                        currentSortSelection = 6;
                        break;

            }
      }

      private void sortByCrop() {
            if (SharePrefManager.isActivitiesSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearSortActivitiesPreference(getContext());
                  SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_CROP_ACTIVITY);
            }
            SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_CROP_ACTIVITY);

            activityLinearLayout.setVisibility(View.GONE);
            plotsSpinnerLayout.setVisibility(View.GONE);
            farmsSpinnerLayout.setVisibility(View.GONE);
      }

      private void sortByDateCreated() {
            if (SharePrefManager.isActivitiesSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearSortActivitiesPreference(getContext());
                  SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_DATE_CREATED_ACTIVITY);
            }
            SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_DATE_CREATED_ACTIVITY);

            activityLinearLayout.setVisibility(View.GONE);
            plotsSpinnerLayout.setVisibility(View.GONE);
            farmsSpinnerLayout.setVisibility(View.GONE);
            dateLayout.setVisibility(View.GONE);
      }

      private void sortByActivityType() {
            if (SharePrefManager.isActivitiesSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearSortActivitiesPreference(getContext());
                  SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_TYPE_ACTIVITY);
            }
            SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_TYPE_ACTIVITY);

            activityLinearLayout.setVisibility(View.VISIBLE);
            plotsSpinnerLayout.setVisibility(View.GONE);
            farmsSpinnerLayout.setVisibility(View.GONE);
            dateLayout.setVisibility(View.GONE);
      }

      private void sortByFarm() {
            if (SharePrefManager.isActivitiesSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearSortActivitiesPreference(getContext());
                  SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_FARM_ACTIVITY);
            }
            SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_FARM_ACTIVITY);
            activityLinearLayout.setVisibility(View.GONE);
            plotsSpinnerLayout.setVisibility(View.GONE);
            farmsSpinnerLayout.setVisibility(View.VISIBLE);
            dateLayout.setVisibility(View.GONE);
      }

      private void sortByPlot() {
            if (SharePrefManager.isActivitiesSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearSortActivitiesPreference(getContext());
                  SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_PLOT_ACTIVITY);
            }
            SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_PLOT_ACTIVITY);
            activityLinearLayout.setVisibility(View.GONE);
            plotsSpinnerLayout.setVisibility(View.VISIBLE);
            farmsSpinnerLayout.setVisibility(View.GONE);
            dateLayout.setVisibility(View.GONE);
      }

      private void sortByAll() {
            if (SharePrefManager.isActivitiesSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearSortActivitiesPreference(getContext());
                  SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_ALL_ACTIVITY);
            }
            SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_ALL_ACTIVITY);
            activityLinearLayout.setVisibility(View.GONE);
            plotsSpinnerLayout.setVisibility(View.GONE);
            farmsSpinnerLayout.setVisibility(View.GONE);
            dateLayout.setVisibility(View.GONE);


      }

      private void sortByCompleted() {
            if (SharePrefManager.isActivitiesSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearSortActivitiesPreference(getContext());
                  SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_COMPLETED_ACTIVITY);
            }
            SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_COMPLETED_ACTIVITY);
            activityLinearLayout.setVisibility(View.GONE);
            plotsSpinnerLayout.setVisibility(View.GONE);
            farmsSpinnerLayout.setVisibility(View.GONE);
            dateLayout.setVisibility(View.GONE);
      }

      private void sortByPending() {
            if (SharePrefManager.isActivitiesSortPreferenceAvailable(getContext())) {
                  SharePrefManager.clearSortActivitiesPreference(getContext());
                  SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_PENDING_ACTIVITY);
            }
            SharePrefManager.setSortFarmActivitiesPreference(getContext(), SharePrefManager.SORT_BY_PENDING_ACTIVITY);
            activityLinearLayout.setVisibility(View.GONE);
            plotsSpinnerLayout.setVisibility(View.GONE);
            farmsSpinnerLayout.setVisibility(View.GONE);
            dateLayout.setVisibility(View.GONE);
      }


      @Override
      public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                  mMapId = getArguments().getString(MAP_ID);
                  mPlotSize = getArguments().getDouble(PLOT_SIZE);
            }
      }

      @NotNull
      @Override
      public Dialog onCreateDialog(Bundle savedInstanceState) {
            Dialog dialog = super.onCreateDialog(savedInstanceState);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            return dialog;
      }

      public void onSave() {
            if (onFarmActivitiesSortFragmentListener != null) {
                  onFarmActivitiesSortFragmentListener.onFarmActivitiesSortSave();
            }
      }

      public void onDismiss() {
            if (onFarmActivitiesSortFragmentListener != null) {
                  onFarmActivitiesSortFragmentListener.onFarmActivitiesSortDismiss();
            }
      }

      @Override
      public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof OnFarmActivitiesSortFragmentListener) {
                  onFarmActivitiesSortFragmentListener = (OnFarmActivitiesSortFragmentListener) context;
            }
      }

      @Override
      public void onDetach() {
            super.onDetach();
            onFarmActivitiesSortFragmentListener = null;
      }

      private void getSortOptions() {

            ArrayList<String> stringArrayList = new ArrayList<>();
            stringArrayList.add("By Plot");
            stringArrayList.add("By Farm");
            stringArrayList.add("By Completed");
            stringArrayList.add("By Pending");
            stringArrayList.add("By Activity Type");
            stringArrayList.add("By Recent");
//            stringArrayList.add("By Crop");
            stringArrayList.add("All");

            String[] spinnerArray = new String[stringArrayList.size()];
            try {

                  spinnerSortsMap = new HashMap<Integer, String>();
                  for ( int i =0; i < stringArrayList.size(); i++ ){
                        spinnerSortsMap.put(i, stringArrayList.get(i));
                        spinnerArray[i] = stringArrayList.get(i);
                  }

                  setupArrayAdapter(spinnerArray, spn_sortOptions);

                  Log.i(TAG, "onChanged:  Farms " + stringArrayList.size());

            } catch (Exception e) {
                  Log.e(TAG, "onNext:  " + e.getMessage());
            }

      }

      private void getCropsFromDatabase(){
            mainViewModel.getCrops().observe(getActivity(), new androidx.lifecycle.Observer<List<Crop>>() {
                  @Override
                  public void onChanged(List<Crop> crops) {

                        String[] spinnerArray = new String[crops.size()];
                        try {

                              spinnerCropsMap = new HashMap<Integer, String>();
                              for ( int i =0; i < crops.size(); i++ ){
                                    spinnerCropsMap.put(i, String.valueOf(crops.get(i).getId()));
                                    spinnerArray[i] = crops.get(i).getName();
                              }

                              setupArrayAdapter(spinnerArray, spn_crops);

                        } catch (Exception e) {
                              Log.e(TAG, "onNext:  " + e.getMessage());
                        }
                  }
            });
      }

      private void getActivitiesFromAPI() {

            mAgroWebAPI.getActivities()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<FarmActivityResponse>() {
                          @Override
                          public void onSubscribe(Disposable d) {
                          }

                          @Override
                          public void onNext(FarmActivityResponse farmActivityResponse) {
                                String[] farmActivitiesArray = new String[farmActivityResponse.getFarmActivityModelList().size()];

                                try {

                                      activitiesHashMap = new HashMap<Integer, String>();
                                      for ( int i =0; i < farmActivityResponse.getFarmActivityModelList().size(); i++ ){
                                            activitiesHashMap.put(i, String.valueOf(farmActivityResponse.getFarmActivityModelList().get(i).getId()));
                                            farmActivitiesArray[i] = farmActivityResponse.getFarmActivityModelList().get(i).getName();
//                                            Log.i(TAG, "onChanged:  " + mapModelList.get(i).getId());
                                      }
                                      setupArrayAdapter(farmActivitiesArray, spn_activities);
                                } catch (Exception e) {
//                                      Log.e(TAG, "onNext:  " + e.getMessage());
                                }
                          }

                          @Override
                          public void onError(Throwable e) {
//                                Log.e(TAG, "onError:  " + e.getMessage() );
                          }

                          @Override
                          public void onComplete() {
                                spn_activities.setSelection((int) SharePrefManager.getCurrentActivitySelection(getContext()), true);
                          }
                    });
      }

      private void fetchPlotsFromDatabase(){
            mainViewModel.getPlots().observe(getActivity(), farmPlotList -> {
                  String[] spinnerArray = new String[farmPlotList.size()];
                  try {

                        plotsSpinnerHashMap = new HashMap<Integer, String>();
                        for ( int i =0; i < farmPlotList.size(); i++ ){
//                              mapSpinnerHashMap.put(i, String.valueOf(farmPlotList.get(i).getId()));
                              plotsSpinnerHashMap.put(i, String.valueOf(farmPlotList.get(i).getPlotId()));
                              spinnerArray[i] = farmPlotList.get(i).getName();
                              Log.i(TAG, "onChanged:  Plots Spinner " + farmPlotList.get(i).getPlotId());
                              Log.i(TAG, "onChanged:  Plots Spinner " + farmPlotList.get(i).getName());
                        }
                        setupArrayAdapter(spinnerArray, plots_spinner);
                  } catch (Exception e) {
                        Log.e(TAG, "onNext:  " + e.getMessage());
                  }
            });
      }

      private void getFarmsFromDB() {

            mainViewModel.getFarms().observe(getActivity(), new androidx.lifecycle.Observer<List<Farm>>() {
                  @Override
                  public void onChanged(List<Farm> farms) {

                        String[] spinnerArray = new String[farms.size()];
                        try {

                              spinnerFarmsMap = new HashMap<Integer, String>();
                              for ( int i =0; i < farms.size(); i++ ){
                                    spinnerFarmsMap.put(i, String.valueOf(farms.get(i).getId()));
                                    spinnerArray[i] = farms.get(i).getName();
                              }

                              setupArrayAdapter(spinnerArray, spn_farms);

                              Log.i(TAG, "onChanged:  Farms " + farms.size());

                        } catch (Exception e) {
                              Log.e(TAG, "onNext:  " + e.getMessage());
                        }

                  }
            });
      }

      private void setupArrayAdapter(String[] stringArray, AppCompatSpinner spinner){
            ArrayAdapter<String> array = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_item, stringArray);
            array.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(array);
      }

      private void dialogDatePickerLight(final Button bt) {
            Calendar cur_calender = Calendar.getInstance();

            DatePickerDialog datePicker = DatePickerDialog.newInstance(
                    new DatePickerDialog.OnDateSetListener() {
                          @Override
                          public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                long date_ship_millis = calendar.getTimeInMillis();
                                bt.setText(Tools.getFormattedDateEvent(date_ship_millis));
                          }
                    },
                    cur_calender.get(Calendar.YEAR),
                    cur_calender.get(Calendar.MONTH),
                    cur_calender.get(Calendar.DAY_OF_MONTH)


            );
            //set dark light
            datePicker.setThemeDark(false);
            datePicker.setAccentColor(getResources().getColor(R.color.colorPrimary));
            datePicker.setMinDate(cur_calender);
            datePicker.show(getActivity().getFragmentManager(), "Datepickerdialog");



            Log.d(TAG, "dialogDatePickerLight:  " + cur_calender.get(0));
      }


      public interface OnFarmActivitiesSortFragmentListener {
            void onFarmActivitiesSortSave();

            void onFarmActivitiesSortDismiss();
      }
}
