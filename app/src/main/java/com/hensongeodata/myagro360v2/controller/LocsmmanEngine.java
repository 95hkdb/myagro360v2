package com.hensongeodata.myagro360v2.controller;

import android.animation.Animator;
import android.content.Context;
import android.os.Vibrator;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;

import com.hensongeodata.myagro360v2.model.Pesticide;

import java.util.ArrayList;

/**
 * Created by user1 on 5/16/2018.
 */

public class LocsmmanEngine {
    private Vibrator vibrator;
    private static long hideDuration=0;
    private static long revealDuration=500;

    public LocsmmanEngine(Context context){
        vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    public static AnimationSet bubbleOnceAnim(long duration){

        //Circle animation
        AnimationSet animSet = new AnimationSet(true);

        ScaleAnimation sc=new ScaleAnimation(1.0f, 1.2f,1.0f,1.2f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        sc.setRepeatMode(Animation.REVERSE);
        sc.setRepeatCount(1);

        animSet.addAnimation(sc);
        animSet.setDuration(duration);

        return animSet;
    }

    public static AnimationSet ScaleOnce(long duration, float from, float to){

        //Circle animation
        AnimationSet animSet = new AnimationSet(true);

        ScaleAnimation sc=new ScaleAnimation(from, to,from,to, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animSet.addAnimation(sc);
        animSet.setDuration(duration);
        animSet.setFillAfter(true);

        return animSet;
    }

    public static AnimationSet revealAnim(long duration){

        AnimationSet animSet = new AnimationSet(true);

        ScaleAnimation sc=new ScaleAnimation(1.0f, 1.2f,1.0f,1.2f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        sc.setRepeatMode(Animation.REVERSE);
        sc.setRepeatCount(1);

        animSet.addAnimation(sc);
        animSet.setDuration(duration);

        return animSet;
    }

    public void playVibrator(){
        vibrator.vibrate(100);
    }

    public void hideView(View v){
        if(v!=null) {
            v.animate().alpha(0.0f).setDuration(hideDuration);
            v.setVisibility(View.GONE);
        }
    }

    public void showView(final View v, final boolean vibrate){
        if(v!=null) {
            hideView(v);
            v.animate().alpha(1.0f).setDuration(revealDuration).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    if(vibrate) playVibrator();

                    v.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
        }
    }



    public static ArrayList<Pesticide>getPesticides(){
        ArrayList<Pesticide> pesticides=new ArrayList<>();

        //Pesticide 1
        pesticides.add(new Pesticide("Striker","At a rate of 20-25mls in 20 litres of water depending on stage of caterpillars (Older caterpillars require higher dosage)",
                "","",
                "Thiamethoxam 141g/l  +  Lambda- Cyhalothrin 106g/l"));
        //Pesticide 2
        pesticides.add(new Pesticide("Engeo","At a rate of 20-25mls in 20 litres of water depending on stage of caterpillars (Older caterpillars require higher dosage)",
                "","",
                "Thiamethoxam 141g/l  +  Lambda- Cyhalothrin 106g/l"));
        //Pesticide 3
        pesticides.add(new Pesticide("Bash","At a rate of 20-25mls in 20 litres of water depending on stage of caterpillars (Older caterpillars require higher dosage)",
                "","",
                "Thiamethoxam 141g/l  +  Lambda- Cyhalothrin 106g/l"));
        //Pesticide 4
        pesticides.add(new Pesticide("Rockett","Use 20-30ml per 20 liters of water for small caterpillars on maize less than 1 month and 30-50mls for big caterpillars",
                "","",
                "Profenofos 40% + Cypermethrin 4%"));
        //Pesticide 5
        pesticides.add(new Pesticide("Larvet","Use 20-30ml per 20 liters of water for small caterpillars on maize less than 1 month and 30-50mls for big caterpillars",
                "","",
                "Profenofos 40% + Cypermethrin 4%"));
        //Pesticide 6
        pesticides.add(new Pesticide("Supa Profenofos","Use 20-30ml per 20 liters of water for small caterpillars on maize less than 1 month and 30-50mls for big caterpillars",
                "","",
                "Profenofos 40% + Cypermethrin 4%"));
        //Pesticide 7
        pesticides.add(new Pesticide("Profecron","Use 20-30ml per 20 liters of water for small caterpillars on maize less than 1 month and 30-50mls for big caterpillars",
                "","",
                "Profenofos 40% + Cypermethrin 4%"));
        //Pesticide 8
        pesticides.add(new Pesticide("Agro-Cypro","Use 20-30ml per 20 liters of water for small caterpillars on maize less than 1 month and 30-50mls for big caterpillars",
                "","",
                "Profenofos 40% + Cypermethrin 4%"));
        //Pesticide 9
        pesticides.add(new Pesticide("Hitcell","Use 20-30ml per 20 liters of water for small caterpillars on maize less than 1 month and 30-50mls for big caterpillars",
                "","",
                "Profenofos 40% + Cypermethrin 4%"));
        //Pesticide 10
        pesticides.add(new Pesticide("Dudu Fenos","Use 20-30ml per 20 liters of water for small caterpillars on maize less than 1 month and 30-50mls for big caterpillars",
                "","",
                "Profenofos 40% + Cypermethrin 4%"));
        //Pesticide 11
        pesticides.add(new Pesticide("Extreme","Use 20-30ml per 20 liters of water for small caterpillars on maize less than 1 month and 30-50mls for big caterpillars",
                "","",
                "Profenofos 40% + Cypermethrin 4%"));
        //Pesticide 12
        pesticides.add(new Pesticide("Misile","Use 20-30ml per 20 liters of water for small caterpillars on maize less than 1 month and 30-50mls for big caterpillars",
                "","",
                "Profenofos 40% + Cypermethrin 4%"));
        //Pesticide 13
        pesticides.add(new Pesticide("Cypercal","Use 20-30ml per 20 liters of water for small caterpillars on maize less than 1 month and 30-50mls for big caterpillars",
                "","",
                "Profenofos 40% + Cypermethrin 4%"));
        //Pesticide 14
        pesticides.add(new Pesticide("Socket plus","Use 20-30ml per 20 liters of water for small caterpillars on maize less than 1 month and 30-50mls for big caterpillars",
                "","",
                "Profenofos 40% + Cypermethrin 4%"));
        //Pesticide 15
        pesticides.add(new Pesticide("Profex super","Use 20-30ml per 20 liters of water for small caterpillars on maize less than 1 month and 30-50mls for big caterpillars",
                "","",
                "Profenofos 40% + Cypermethrin 4%"));

        return pesticides;
    }
}
