package com.hensongeodata.myagro360v2.controller;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import androidx.annotation.NonNull;
import android.util.Log;
import android.webkit.MimeTypeMap;

import org.apache.http.entity.mime.content.FileBody;

import java.io.File;
import java.io.IOException;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by user1 on 4/23/2018.
 */

public class RetrofitHelper {

    @NonNull
    public static RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }
    @NonNull
    public static MultipartBody.Part prepareFilePart(Context context,String partName,String path,boolean compress) {
        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        Log.d("RetrofitHelper","path: "+path);
        File file = new File(path);
        Uri fileUri=Uri.fromFile(file);
        Log.d("RetrofitHelper","uri: "+fileUri);
        /*  if(compress)
                fileUri=compressImage(context,fileUri);
*/
        Log.d("RetrofitHelper","uri: "+fileUri+" file: "+file);
        if(fileUri!=null&&file!=null) {
            // create RequestBody instance from file
            String type=getMimeType(context,fileUri);
            Log.d("RetrofitHelper","type: "+type);
            if(type!=null) {
                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse(type),
                                file
                        );
                // MultipartBody.Part is used to send also the actual file name
                return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
            }
            else
                return null;
        }
        else {
            return null;
        }

    }

    @NonNull
    public static MultipartBody.Part prepareFilePart(Context context,String partName,String path) {
        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        File file = new File(path);
        Uri fileUri=Uri.fromFile(file);

        Log.d("RetrofitHelper","uri: "+fileUri+" file: "+file);
        if(fileUri!=null&&file!=null) {
            // create RequestBody instance from file
            String type=getMimeType(context,fileUri);
            Log.d("RetrofitHelper","type: "+type);
            if(type!=null) {
                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse(type),
                                file
                        );
                // MultipartBody.Part is used to send also the actual file name
                return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
            }
            else
                return null;
        }
        else {
            return null;
        }

    }

    public static String getMimeType(Context context, Uri uri) {
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }

    public static FileBody getFileBody(Context context, String path){
       return new FileBody(new File(path), getMimeType(context,Uri.fromFile(new File(path))));
    }

    public static Uri compressImage(Context context,Uri uri){
        Uri mUri = null;
        if(uri!=null) {
            File file = new File(uri.getPath());
            File compressedImageFile = null;
            try {
                if (file != null)
                    compressedImageFile = new Compressor(context).compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }catch (NullPointerException e){
                e.printStackTrace();
                mUri=uri;
            }
            Log.d("RetrofitHelper", "compressed file: " + compressedImageFile);

            if (compressedImageFile != null) {
                mUri = Uri.fromFile(compressedImageFile);
            }
        }

        return mUri;
    }
}
