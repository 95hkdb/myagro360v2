package com.hensongeodata.myagro360v2.controller;

import android.content.Context;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.hensongeodata.myagro360v2.R;

/**
 * Created by user1 on 5/11/2018.
 */

public class MyFABMedia extends FloatingActionButton{
    public MyFABMedia(Context context) {
        super(context);
        setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
    }
}
