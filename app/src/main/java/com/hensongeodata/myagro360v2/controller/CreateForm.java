package com.hensongeodata.myagro360v2.controller;

import android.content.Context;
import android.util.Log;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.model.CheckboxField;
import com.hensongeodata.myagro360v2.model.DropdownField;
import com.hensongeodata.myagro360v2.model.Field;
import com.hensongeodata.myagro360v2.model.FileField;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.model.RadioField;
import com.hensongeodata.myagro360v2.model.Section;
import com.hensongeodata.myagro360v2.model.SettingsModel;
import com.hensongeodata.myagro360v2.model.TextField;
import com.hensongeodata.myagro360v2.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.hensongeodata.myagro360v2.controller.RetrofitHelper.createPartFromString;

/**
 * Created by user1 on 1/24/2018.
 */

public class CreateForm {
    private static String TAG=CreateForm.class.getSimpleName();
    public static String DATA_LIVE="live_data"; //data that is currently being fetched/modified
    public static String DATA_DEAD="dead_data"; //data that was saved and being retrieved
    private Context context;
    private DatabaseHelper databaseHelper;
    private NetworkRequest networkRequest;
    public ArrayList<ArrayList<Object>>sections;
    private static int fieldCount=0;
    private static CreateForm instance;
    private boolean shouldRefreshForm=true;

    public CreateForm(Context context){
        sections=new ArrayList<>();
        databaseHelper=new DatabaseHelper(context);
        networkRequest=new NetworkRequest(context);

        this.context=context;
    }

    public static CreateForm getInstance(Context context){
        if(instance==null)
            instance=new CreateForm(context);

        return instance;
    }

    public int getFieldCount(){
        fieldCount++;
        return fieldCount;
    }

    public boolean isShouldRefreshForm() {
        return shouldRefreshForm;
    }

    public void setShouldRefreshForm(boolean shouldRefreshForm) {
        this.shouldRefreshForm = shouldRefreshForm;
    }

    public void addFields(ArrayList<Object> fields){
        ArrayList<Object> form=new ArrayList<>();

        for(int a=0;a<fields.size();a++){

            if(fields.get(a) instanceof Section &&((Section)fields.get(a)).getType()==Section.MAIN){
                Log.d(TAG,a+" main section");
                    //let's break form to create another fragment
                if(form.size()!=0)
                    sections.add(form);

                form=new ArrayList<>();
                form.add(fields.get(a));
            }
            else if(fields.get(a) instanceof Field||(fields.get(a) instanceof Section &&((Section)fields.get(a)).getType()==Section.SUB)){
                Log.d(TAG,a+" field");
                form.add(fields.get(a));
            }

                if(a==(fields.size()-1)){
                    Log.d(TAG,a+" last entry");
                    //fields are finished hence add form to section
                    sections.add(form);
                }
        }

    }


    public ArrayList getFields(){
      ArrayList<Object> fields=new ArrayList<>();
        for(int a=0;a<sections.size();a++) {
            ArrayList<Object> objectList = sections.get(a);
            for (int b = 0; b < objectList.size(); b++) {
                fields.add(objectList.get(b));
            }
        }
        return fields;
    }


    public ArrayList<Object> buildPreview(ArrayList<ArrayList<Object>> sectionList){
        ArrayList<Object> previewItems=new ArrayList<>();

        //add all items to a single list for preview purposes
        for(int a=0;a<sectionList.size();a++) {
            ArrayList<Object> fields = sectionList.get(a);
            for (int b = 0; b < fields.size(); b++) {
                previewItems.add(fields.get(a));//adding each field item to previewList, including header sections
            }
        }
        return previewItems;
    }

    public JSONObject buildJSON(Context context,String form_id,boolean isFinalBuild) throws JSONException {
        if(form_id==null)
            form_id=(new SettingsModel(context)).getForm_id();

            JSONObject baseObject= new JSONObject();
            JSONObject transactionObject=new JSONObject();
            transactionObject.put(DatabaseHelper.FORM_ID,form_id);
            transactionObject.put(DatabaseHelper.USER_ID,(new User(context)).getId());
            transactionObject.put(DatabaseHelper.DATE,getCurrentDate());
            transactionObject.put(DatabaseHelper.TIME,getCurrentTime());
            transactionObject.put(DatabaseHelper.POI_GROUP_ID,MyApplication.poiGroup.getId());

            baseObject.put(DatabaseHelper.TBL_TRANSACTION,transactionObject);//add transaction properties to base object

            JSONArray answerArray=new JSONArray();
            for(int a=0;a<sections.size();a++){
                ArrayList<Object> fields= sections.get(a);
                ArrayList<JSONObject> answerObjects;

                for(int b=0;b<fields.size();b++) {
                    answerObjects=new ArrayList<>();
                    Object field=fields.get(b);

                    if(field instanceof Section){continue;}

                    else if(field instanceof CheckboxField){
                        CheckboxField checkboxField=((CheckboxField) field);
                        answerObjects=fillJSON(checkboxField.getId(),checkboxField.getAnswers(),checkboxField.getType());
                    }
                    else if(field instanceof DropdownField){
                        DropdownField dropdownField= (DropdownField) field;
                        answerObjects=fillJSON(dropdownField.getId(),dropdownField.getAnswers(),dropdownField.getType());
                    }
                    else if(field instanceof RadioField){
                        RadioField radioField= (RadioField) field;
                        answerObjects=fillJSON(radioField.getId(),radioField.getAnswers(),radioField.getType());
                    }
                    else if(field instanceof FileField){
                        FileField fileField= (FileField) field;
                        if(isFinalBuild){
                            fillFilesToUpload(fileField.getId(), fileField.getType(),fileField.getAnswers());
                            continue;
                        }
                        else
                            answerObjects = fillJSON(fileField.getId(), fileField.getAnswers(), fileField.getType());
                    }
                    else if(field instanceof TextField){
                        TextField textField= (TextField) field;
                        answerObjects=fillJSON(textField.getId(),textField.getAnswers(),textField.getType());
                    }

                        for(int c=0;c<answerObjects.size();c++) {
                            if(answerObjects.get(c)!=null&&!answerObjects.isEmpty())
                                answerArray.put(answerObjects.get(c)); //adding each answer model to answer array
                            }
                    }
            }

                baseObject.put(DatabaseHelper.TBL_ANSWERS,answerArray);//add answer array to base object

        JSONObject poiObject;
        JSONArray poiArray=new JSONArray();
        ArrayList<POI> poiList=new ArrayList<>();
        POI poi;

        if(MyApplication.poiGroup.getPoiList().size()==0){
            MyApplication.poiGroup.getPoiList().add((new MyApplication()).getInstantPOI(context,false));
        }

        for(int a=0;a< MyApplication.poiGroup.getPoiList().size();a++){
                poiObject=new JSONObject();
                poi=MyApplication.poiGroup.getPoiList().get(a);
                poiObject.put(DatabaseHelper.ACCURACY,poi.getAccuracy());
                poiObject.put(DatabaseHelper.LONGITUDE,poi.getLon());
                poiObject.put(DatabaseHelper.LATITUDE,poi.getLat());
                poiObject.put(DatabaseHelper.ALTITUDE,poi.getAlt());
                poiObject.put(DatabaseHelper.NAME,poi.getName());

            poiArray.put(poiObject);

            poiList.add(new POI(poi.getName(),poi.getLon(),poi.getLat(),poi.getAlt(),poi.getAccuracy()));
        }

        baseObject.put(DatabaseHelper.TBL_POIS,poiArray);//adding poi array to base object

        int area=0;
        long distance=0;

        if(poiList.size()>0){
            area=MyApplication.getArea(poiList);
            distance=MyApplication.getPerimeter(poiList);
        }
        JSONObject dimenObject=new JSONObject();
        dimenObject.put(DatabaseHelper.AREA,area);
        dimenObject.put(DatabaseHelper.DISTANCE,distance);

        baseObject.put(DatabaseHelper.TBL_DIMENSIONS,dimenObject);

        if(isFinalBuild)
            MyApplication.form_id_attachment=form_id;

        return baseObject;
    }

//    public JSONObject buildJSON(Context context,boolean isFinalBuild,POI instantPOI,String form_id) throws JSONException {
//
//        if(form_id==null)
//            form_id=(new SettingsModel(context)).getForm_id();
//
//        JSONObject baseObject= new JSONObject();
//
//        JSONObject transactionObject=new JSONObject();
//        transactionObject.put(DatabaseHelper.FORM_ID,form_id);
//        transactionObject.put(DatabaseHelper.USER_ID,(new User(context)).getId());
//        transactionObject.put(DatabaseHelper.DATE,getCurrentDate());
//        transactionObject.put(DatabaseHelper.TIME,getCurrentTime());
//        transactionObject.put(DatabaseHelper.POI_GROUP_ID,MyApplication.poiGroup.getId());
//
//        baseObject.put(DatabaseHelper.TBL_TRANSACTION,transactionObject);//add transaction properties to base object
//
//        JSONArray answerArray=new JSONArray();
//        for(int a=0;a<sections.size();a++){
//            ArrayList<Object> fields= sections.get(a);
//            ArrayList<JSONObject> answerObjects;
//
//            for(int b=0;b<fields.size();b++) {
//                answerObjects=new ArrayList<>();
//                Object field=fields.get(b);
//
//                if(field instanceof Section){continue;}
//
//                else if(field instanceof CheckboxField){
//                    CheckboxField checkboxField=((CheckboxField) field);
//                    answerObjects=fillJSON(checkboxField.getId(),checkboxField.getAnswers(),checkboxField.getType());
//                }
//                else if(field instanceof DropdownField){
//                    DropdownField dropdownField= (DropdownField) field;
//                    answerObjects=fillJSON(dropdownField.getId(),dropdownField.getAnswers(),dropdownField.getType());
//                }
//                else if(field instanceof RadioField){
//                    RadioField radioField= (RadioField) field;
//                    answerObjects=fillJSON(radioField.getId(),radioField.getAnswers(),radioField.getType());
//                }
//                else if(field instanceof FileField){
//                    FileField fileField= (FileField) field;
//                    if(isFinalBuild&&(fileField.getType()!=null
//                            &&fileField.getType().equalsIgnoreCase(FileField.FILE_SCAN))){
//                        fillFilesToUpload(fileField.getId(), fileField.getType(),fileField.getAnswers());
//                        continue;
//                    }
//                    else
//                        answerObjects = fillJSON(fileField.getId(), fileField.getAnswers(), fileField.getType());
//                }
//                else if(field instanceof TextField){
//                    TextField textField= (TextField) field;
//                    answerObjects=fillJSON(textField.getId(),textField.getAnswers(),textField.getType());
//                }
//
//                for(int c=0;c<answerObjects.size();c++) {
//                    if(answerObjects.get(c)!=null&&!answerObjects.isEmpty())
//                        answerArray.put(answerObjects.get(c)); //adding each answer model to answer array
//                }
//            }
//        }
//
//        baseObject.put(DatabaseHelper.TBL_ANSWERS,answerArray);//add answer array to base object
//
//        JSONObject poiObject;
//        JSONArray poiArray=new JSONArray();
//
//        Log.d(TAG,"instant accr: "+instantPOI.getAccuracy()+" lat: "+instantPOI.getLat()+" lon: "+instantPOI.getLon());
//
//            poiObject=new JSONObject();
//            poiObject.put(DatabaseHelper.ACCURACY,instantPOI.getAccuracy());
//            poiObject.put(DatabaseHelper.LONGITUDE,instantPOI.getLon());
//            poiObject.put(DatabaseHelper.LATITUDE,instantPOI.getLat());
//            poiObject.put(DatabaseHelper.ALTITUDE,instantPOI.getAlt());
//            poiObject.put(DatabaseHelper.NAME,instantPOI.getName());
//
//            poiArray.put(poiObject);
//
//        baseObject.put(DatabaseHelper.TBL_POIS,poiArray);//adding poi array to base object
//
//
//        JSONObject dimenObject=new JSONObject();
//        dimenObject.put(DatabaseHelper.AREA,0);
//        dimenObject.put(DatabaseHelper.DISTANCE,0);
//
//        baseObject.put(DatabaseHelper.TBL_DIMENSIONS,dimenObject);
//
//        if(isFinalBuild)
//            MyApplication.form_id_attachment=form_id;
//
//        return baseObject;
//    }

    protected void fillFilesToUpload(String field_id, String type,ArrayList<String> answers){
        RequestBody rbType = createPartFromString(type);
        RequestBody rbField_id = createPartFromString(field_id);
        String media_type="image";
        if(type!=null&&type.equalsIgnoreCase(FileField.FILE_VIDEO))
            media_type="video";

        for(int a=0;a<answers.size();a++) {
            String answer=answers.get(a);
            if(answer!=null&&!answer.trim().isEmpty()) {
                MultipartBody.Part file = RetrofitHelper.prepareFilePart(context,media_type, answer);
                MyApplication.filesToUpload.add(new FileField(file, rbField_id, rbType));
            }
        }
    }

    protected void fillFilesToUpload(String field_id, String type,String answer){
        RequestBody rbType = createPartFromString(type);
        RequestBody rbField_id = createPartFromString(field_id);
        String media_type="image";
        if(type!=null&&type.equalsIgnoreCase(FileField.FILE_VIDEO))
            media_type="video";

            if(answer!=null&&!answer.trim().isEmpty()) {
                MultipartBody.Part file = RetrofitHelper.prepareFilePart(context, media_type, answer);
                MyApplication.filesToUpload.add(new FileField(file, rbField_id, rbType));
            }
    }

    protected ArrayList<JSONObject> fillJSON(String field_id,ArrayList answers,String type){
        JSONObject jsonObject;
        ArrayList<JSONObject>jsonAnswers=new ArrayList<>();
        try {
            if(answers.size()<1){
                jsonObject = new JSONObject();
                jsonObject.put(DatabaseHelper.QUESTION_ID, field_id);
                jsonObject.put(DatabaseHelper.ANSWER, "");
                jsonObject.put(DatabaseHelper.TYPE, type);
                jsonAnswers.add(jsonObject);
            }
            else {
                for (int a = 0; a < answers.size(); a++) {
                    String answer = (String) answers.get(a);
                    jsonObject = new JSONObject();
                    jsonObject.put(DatabaseHelper.QUESTION_ID, field_id);
                    jsonObject.put(DatabaseHelper.ANSWER, answer);
                    jsonObject.put(DatabaseHelper.TYPE, type);

                    jsonAnswers.add(jsonObject);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonAnswers;
    }

    protected JSONObject fillJSON(String field_id,String answer,String type){
        JSONObject jsonObject=new JSONObject();
          try {
                  jsonObject.put(DatabaseHelper.QUESTION_ID, field_id);
                  jsonObject.put(DatabaseHelper.ANSWER, answer);
                  jsonObject.put(DatabaseHelper.TYPE, type);

           } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public String getCurrentTime(){
        TimeZone tz = TimeZone.getTimeZone("GMT+00:00");
        Calendar c = Calendar.getInstance(tz);

        return String.format("%02d" , c.get(Calendar.HOUR_OF_DAY))+" : "+
                String.format("%02d" , c.get(Calendar.MINUTE))+" : "+
                String.format("%02d" , c.get(Calendar.SECOND));
    }

    public String getTime(long millis){
        DateFormat formatter = new SimpleDateFormat("hh:mm aaa", Locale.getDefault());
       // DateFormat formatter = new SimpleDateFormat("hh:mm", Locale.getDefault());
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String time=formatter.format(new Date(millis));
        if(time!=null&&time.charAt(0)=='0')
            time=time.substring(1);
        return time;

    }
    public String getCurrentDate(){

        DateFormat df = new SimpleDateFormat("yyy-MM-dd");
        df.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));

        String date=df.format(new Date());

        return date;
    }

    public String getCurrentDate2(){
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        df.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));

        String date=df.format(new Date());

        return date;
    }

    public JSONArray buildJSON_Mapping(ArrayList<POI> pois) throws JSONException {
        JSONArray jsonArray=new JSONArray();
        for(POI poi:pois){
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("latitude",poi.getLat());
            jsonObject.put("longitude",poi.getLon());
            jsonObject.put("accuracy",poi.getAccuracy());

            jsonArray.put(jsonObject);
        }

        return jsonArray;
    }

    public ArrayList<POI> buildPOI_Mapping(ArrayList<String> messages){
        ArrayList<POI> pois=new ArrayList<>();
        for(String message:messages){
            if(message!=null&&!message.trim().isEmpty()&&message.contains("#")) {
                pois.add(resolvePOI(message));
            }
        }

        return pois;
    }

    public POI resolvePOI(String message){
        POI poi=new POI();
        if(message!=null&&!message.trim().isEmpty()&&message.contains("#")) {
            String[] coordinates = message.split("#");
            if (coordinates.length > 0)
                poi.setLat(coordinates[0]);
            if (coordinates.length > 1)
                poi.setLon(coordinates[1]);
            if (coordinates.length > 2)
                poi.setAccuracy(coordinates[2]);
        }
        return poi;
    }
}
