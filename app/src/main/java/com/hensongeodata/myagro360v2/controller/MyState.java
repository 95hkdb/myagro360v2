package com.hensongeodata.myagro360v2.controller;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by user1 on 9/25/2017.
 */

public class MyState {
    Context context;
    SharedPreferences sharedPreferences;
    private static String PREF_NAME="pref_name";

    public MyState(Context context){
        this.context=context;
        sharedPreferences=context.getSharedPreferences(PREF_NAME,0);
    }

    public void saveString(String key,String value){
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public String getString(String key){
        return sharedPreferences.getString(key,null);
    }

    public void saveInt(String key,int value){
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt(key, value);
        edit.commit();
    }

    public int getInt(String key){
        return sharedPreferences.getInt(key,0);
    }


    public void saveBoolean(String key,boolean value){
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean(key, value);
        edit.commit();

    }


    public boolean getBoolean(String key){
        return sharedPreferences.getBoolean(key,false);
    }

}
