//package com.faw.myagro360.controller;
//
//import android.content.Context;
//import android.net.Uri;
//import android.os.AsyncTask;
//import android.os.Handler;
//import android.util.Log;
//
//import com.faw.myagro360.Interface.AuthListener;
//import com.faw.myagro360.Interface.NetworkResponse;
//import com.faw.myagro360.MyApplication;
//import com.faw.myagro360.api.request.LocsmmanProAPI;
//import com.faw.myagro360.model.Country;
//import com.faw.myagro360.model.Device;
//import com.faw.myagro360.model.District;
//import com.faw.myagro360.model.FileField;
//import com.faw.myagro360.model.Form;
//import com.faw.myagro360.model.Keys;
//import com.faw.myagro360.model.Neighbourhood;
//import com.faw.myagro360.model.POI;
//import com.faw.myagro360.model.Region;
//import com.faw.myagro360.model.ServiceClass;
//import com.faw.myagro360.model.SettingsModel;
//import com.faw.myagro360.model.Town;
//import com.faw.myagro360.model.User;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.BufferedReader;
//import java.io.BufferedWriter;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.io.OutputStream;
//import java.io.OutputStreamWriter;
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.util.ArrayList;
//
//import me.pushy.sdk.Pushy;
//import me.pushy.sdk.util.exceptions.PushyException;
//import okhttp3.ResponseBody;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//import retrofit2.Retrofit;
//import retrofit2.converter.gson.GsonConverterFactory;
//
//import static com.faw.myagro360.controller.RetrofitHelper.createPartFromString;
//
///**
// * Created by user1 on 8/8/2017.
// */
//
//public class NetworkRequest_backup {
//    private String TAG=NetworkRequest_backup.class.getSimpleName();
//    public static int STATUS_SUCCESS=100;
//    public static int STATUS_FAIL=200;
//    public static int STATUS_FAILED_CONNECTION=300;
//    private AuthListener authListener;
//
//    private Context context;
//    DatabaseHelper databaseHelper;
//    public NetworkRequest_backup(Context context){
//        this.context = context;
//        databaseHelper=new DatabaseHelper(context);
//    }
//
//    public void setAuthListener(AuthListener authListener) {
//        this.authListener = authListener;
//    }
//
//    public void close(){
//        databaseHelper.close();
//    }
//
//
//    public void signUp(final User user){
//        final String URL= Keys.CENTRAL+"signup";
//
//        class doSignup extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("appid",Keys.HGT_APP_ID)
//                            .appendQueryParameter("email",user.getEmail())
//                            .appendQueryParameter("password",user.getPassword())
//                            .appendQueryParameter("firstName",user.getFirstname())
//                            .appendQueryParameter("lastName",user.getLastname())
//                            .appendQueryParameter("pushID",user.getPush_id());
//                    String query = builder.build().getEncodedQuery();
//
//                     OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String response) {
//                super.onPostExecute(response);
//                Log.d(TAG,"JoinCommunityResponse "+response);
//
//                if(null != response && !response.isEmpty()) {
//
//                    try {
//                        JSONObject jsonObject=new JSONObject(response);
//                        String error_message=jsonObject.optString("error_msg");
//                        Boolean error=jsonObject.optBoolean("error");
//
//                        if(!error){
//                            String user_id=jsonObject.optString("user_id");
//                            (new MyApplication()).initUser(context,user_id);
//
//                                User mUser=new User(context);
//                                mUser.setFirstname(user.getFirstname());
//                                mUser.setLastname(user.getLastname());
//                                mUser.setEmail(user.getEmail());
//                                mUser.setPassword(user.getPassword());
//                                mUser.setId(user_id);
//                                mUser.setHgt_user_id(jsonObject.optString("hgtuser_id"));
//                                mUser.setPush_id(user.getPush_id());
//                                mUser.setPhone(user.getPhone());
//
//                                BroadcastCall.publishSignup(context, NetworkRequest_backup.STATUS_SUCCESS,null);
//                            }
//                            else {
//                                BroadcastCall.publishSignup(context, NetworkRequest_backup.STATUS_FAIL,error_message);
//                            }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d(TAG,""+e);
//                        BroadcastCall.publishSignup(context, NetworkRequest_backup.STATUS_FAIL,null);
//                    }
//                }
//                else {
//                    BroadcastCall.publishSignup(context, NetworkRequest_backup.STATUS_FAIL,null);
//                }
//            }
//        }
//
//        doSignup doSignup = new doSignup();
//        doSignup.execute();
//    }
//
//    public void signin(final User user){
//        final String URL= Keys.CENTRAL+"login";
//        final Device device=(new MyApplication()).getDevice();
//        class doSignin extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//
//                    //Log.d(TAG,"version: "+device.getAndroidVersion()+" name: "+device.getName()
//                    //+" model: "+device.getModel()+" serial: "+device.getSerial());
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("email",user.getEmail())
//                            .appendQueryParameter("password",user.getPassword())
//                            .appendQueryParameter("device_name",device.getName())
//                            .appendQueryParameter("version",device.getAndroidVersion())
//                            .appendQueryParameter("model",device.getModel())
//                            .appendQueryParameter("serial_number",device.getSerial());
//
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String response) {
//                super.onPostExecute(response);
//                Log.d(TAG,"JoinCommunityResponse "+response);
//
//                if(null != response && !response.isEmpty()) {
//
//                    try {
//                        JSONObject jsonObject=new JSONObject(response);
//                        String error_message=jsonObject.optString("error_msg");
//                        Boolean error=jsonObject.optBoolean("error");
//
//                        if(!error){
//                            String user_id=jsonObject.optString("user_id");
//                            (new MyApplication()).initUser(context,user_id);
//
//                            User mUser=new User(context);
//                               // mUser.setFirstname(user.getFirstname());
//                               // mUser.setLastname(user.getLastname());
//                                mUser.setEmail(user.getEmail());
//                                mUser.setPassword(user.getPassword());
//                                mUser.setId(user_id);
//                                mUser.setHgt_user_id(jsonObject.optString("hgtuser_id"));
//                                mUser.setPush_id(user.getPush_id());
//                               // mUser.setPhone(user.getPhone());
//
//                                BroadcastCall.publishSignin(context, NetworkRequest_backup.STATUS_SUCCESS,error_message);
//                            }
//                            else {
//
//                                BroadcastCall.publishSignin(context, NetworkRequest_backup.STATUS_FAIL,error_message);
//                            }
//
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d(TAG,""+e);
//                        BroadcastCall.publishSignin(context, NetworkRequest_backup.STATUS_FAIL,null);
//                    }
//                }
//                else {
//                    BroadcastCall.publishSignin(context, NetworkRequest_backup.STATUS_FAIL,null);
//                }
//            }
//        }
//
//        doSignin doSignin = new doSignin();
//        doSignin.execute();
//    }
//
//    public void registerPush(final User user){
//        final String URL= Keys.APPS+"authenticateUser";
//        class doAuth extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//
//                    user.setPush_id(Pushy.register(context));
//                    Log.d(TAG,"push_id: "+user.getPush_id());
//
//                    (new User(context)).setPush_id(user.getPush_id());
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("user_id",user.getId())
//                            .appendQueryParameter("push_id", user.getPush_id());
//
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                } catch (PushyException e) {
//                    Log.d(TAG,"Pushy e: "+e);
//                   // authListener.onAuthenticate(NetworkRequest.STATUS_FAIL,-1);
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String response) {
//                super.onPostExecute(response);
//                Log.d(TAG,"JoinCommunityResponse "+response);
//
//                if(null != response && !response.isEmpty()) {
//
//                    try {
//                        JSONObject jsonObject=new JSONObject(response);
//                        authListener.onAuthenticate(NetworkRequest_backup.STATUS_SUCCESS,jsonObject.optInt("officer"));
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d(TAG,""+e);
//                        authListener.onAuthenticate(NetworkRequest_backup.STATUS_FAIL,-1);
//                    }
//                }
//                else {
//                    authListener.onAuthenticate(NetworkRequest_backup.STATUS_FAIL,-1);
//                }
//            }
//        }
//
//        doAuth doAuth = new doAuth();
//        doAuth.execute();
//    }
//
//    public void recover(final String email){
//        final String URL= Keys.CENTRAL+"recoverPassword";
//
//        class doRecovery extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("email",email);
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String response) {
//                super.onPostExecute(response);
//                Log.d(TAG,"JoinCommunityResponse "+response);
//                BroadcastCall.publishRecover(context,response);
//            }
//        }
//
//        doRecovery doRecovery = new doRecovery();
//        doRecovery.execute();
//    }
//
//    public void reset(final String email,final String passwordOld,final String passwordNew){
//        final String URL= Keys.ACCOUNTS_API+"register.php";
//
//        class doReset extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("appid",Keys.HGT_APP_ID)
//                            .appendQueryParameter("email",email)
//                            .appendQueryParameter("password_old",passwordOld)
//                            .appendQueryParameter("password_new",passwordNew);
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String response) {
//                super.onPostExecute(response);
//                Log.d(TAG,"JoinCommunityResponse "+response);
//
//                if(null != response && !response.isEmpty()) {
//
//                    try {
//                        JSONObject jsonObject=new JSONObject(response);
//                        if(jsonObject.has("error")){
//                            String error=jsonObject.getString("error");
//
//                            if(error!=null&&error.trim().equalsIgnoreCase("false")){
//
//                                BroadcastCall.publishReset(context,"success");
//                            }
//                            else {
//
//                                BroadcastCall.publishReset(context,"fail");
//                            }
//                        }
//                        else {
//                            BroadcastCall.publishReset(context,null);
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d(TAG,""+e);
//                        BroadcastCall.publishReset(context,null);
//                    }
//                }
//                else {
//                    BroadcastCall.publishReset(context,null);
//                }
//            }
//        }
//
//        doReset doReset = new doReset();
//        doReset.execute();
//    }
//
//    public void fetchClass(){
//        final String URL= Keys.BASE_URL+"classes";
//
//        class doFetchClass extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("id","");
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String response) {
//                super.onPostExecute(response);
//                Log.d(TAG,"JoinCommunityResponse "+response);
//
//                if(null != response && !response.isEmpty()) {
//
//                    try {
//                        JSONObject jsonObject=new JSONObject(response);
//                        if(jsonObject.has("data")){
//                            databaseHelper=new DatabaseHelper(context);
//                            databaseHelper.mashTable(DatabaseHelper.CLASS_TBL);
//                            databaseHelper=new DatabaseHelper(context);
//
//                            JSONArray jsonArray = jsonObject.getJSONArray("data");
//                            ServiceClass serviceClass=new ServiceClass();
//                            for (int a = 0; a <jsonArray.length(); a++) {
//                                jsonObject = jsonArray.getJSONObject(a);
//                                serviceClass.setId(jsonObject.getString("class_id"));
//                                serviceClass.setName(jsonObject.getString("class_name"));
//                                serviceClass.setDescription(jsonObject.getString("class_description"));
//
//                                          databaseHelper.addItem(DatabaseHelper.CLASS_TBL,serviceClass);
//
//                                    }
//                            // sendCountries(MyApplication.countryList);
//                            BroadcastCall.publishClassLoad(context,"success");
//                        }
//                        else {
//                            BroadcastCall.publishClassLoad(context,null);
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d(TAG,""+e);
//                        BroadcastCall.publishClassLoad(context,null);
//                    }
//                }
//                else {
//                    BroadcastCall.publishClassLoad(context,null);
//                }
//            }
//        }
//
//        doFetchClass doFetchClass = new doFetchClass();
//        doFetchClass.execute();
//    }
//
///*public void fetchDistricts(){
//        final String TOWN_URL= Keys.BASE_URL+"fetch_districts.php";
//
//        class doFetchTowns extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(TOWN_URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("id","");
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String JoinCommunityResponse) {
//                super.onPostExecute(JoinCommunityResponse);
//                Log.d(TAG,"JoinCommunityResponse "+JoinCommunityResponse);
//
//                if(null != JoinCommunityResponse && !JoinCommunityResponse.isEmpty()) {
//
//                    try {
//                        JSONObject jsonObject=new JSONObject(JoinCommunityResponse);
//                        if(jsonObject.has("date")){
//                            BroadcastCall.publishDistrictLoad(context,"success");
//                            databaseHelper.mashTable(DatabaseHelper.DISTRICT_TBL);
//                            databaseHelper=new DatabaseHelper(context);
//
//                            int districtSize=5;//To be replaced with actual number of districts from server
//                            District district=new District();
//                            for(int a=0;a<districtSize;a++){
//                                district.setId(-1);
//                                district.setName("name");
//                                databaseHelper.addMessage(DatabaseHelper.DISTRICT_TBL,district);
//                            }
//
//                        }
//                        else {
//                            BroadcastCall.publishDistrictLoad(context,null);
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d(TAG,""+e);
//                        BroadcastCall.publishDistrictLoad(context,null);
//                    }
//                }
//                else {
//                    BroadcastCall.publishDistrictLoad(context,null);
//                }
//            }
//        }
//
//        doFetchTowns doFetchTowns = new doFetchTowns();
//        doFetchTowns.execute();
//    }
//*/
//    public void sendTransaction(final JSONObject jsonObject){
//        //final String URL= Keys.APPS+"locsinsert";
//       MyApplication.filesToUploadSize=MyApplication.filesToUpload.size();
//
//        Log.d(TAG,"jsonObject: "+jsonObject);
//        Log.d(TAG,"filesSize: "+MyApplication.filesToUploadSize);
//
//        Retrofit.Builder builder=new Retrofit.Builder()
//                .baseUrl(Keys.APPS)
//                .addConverterFactory(GsonConverterFactory.create());
//
//        Retrofit retrofit=builder.build();
//
//        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);
//
//        Call<ResponseBody> call= service.locsinsert(jsonObject);
//
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//
//                ResponseBody responseBody=response.body();
//                String message=null;
//                try {
//                    if(responseBody!=null)
//                        message=responseBody.string();
//
//                    Log.d(TAG,"JoinCommunityResponse: "+message+" body: "+responseBody
//                            +" code: "+response.code()+" error: "+response.errorBody());
//
//                    if(null != message && !message.isEmpty()) {
//
//                        try {
//                            JSONObject jsonObject=new JSONObject(message);
//                            if(jsonObject.has("error")){
//                                boolean error=jsonObject.optBoolean("error");
//                                if(!error) {
//                                    MyApplication.tranx_id_attahcment=jsonObject.optString("tranx_id");
//                                    Log.d(TAG,"tranx_id: "+MyApplication.tranx_id_attahcment);
//                                    BroadcastCall.publishTransaction(context, NetworkRequest_backup.STATUS_SUCCESS, null);
//                                }
//                                else {
//                                    String error_msg=jsonObject.optString("error_msg");
//                                    MyApplication.tranx_id_attahcment=null;
//                                    BroadcastCall.publishTransaction(context, NetworkRequest_backup.STATUS_FAIL, error_msg);
//                                }
//                            }
//                            else {
//                                MyApplication.tranx_id_attahcment=null;
//                                BroadcastCall.publishTransaction(context, NetworkRequest_backup.STATUS_FAIL, null);
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Log.d(TAG,""+e);
//                            MyApplication.tranx_id_attahcment=null;
//                            BroadcastCall.publishTransaction(context, NetworkRequest_backup.STATUS_FAIL, null);
//                        }
//                    }
//                    else {
//                        MyApplication.tranx_id_attahcment=null;
//                        BroadcastCall.publishTransaction(context, NetworkRequest_backup.STATUS_FAIL, null);
//                    }
//
//                    call.cancel();
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    call.cancel();
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                String message=t.getMessage();
//                Log.d(TAG,"throwable: "+t.getMessage()+" class "+t.getClass()+" cause "+t.getCause());
//                call.cancel();
//                BroadcastCall.publishTransaction(context, NetworkRequest_backup.STATUS_FAIL, message);
//            }
//        });
//
////
////        class doSendTransaction extends AsyncTask<String, Void,String> {
////
////
////            @Override
////            protected void onPreExecute() {
////                super.onPreExecute();
////            }
////
////            @Override
////            protected String doInBackground(String... params) {
////                HttpURLConnection connection = null;
////                try {
////                    URL url = new URL(URL);
////                    connection  = (HttpURLConnection) url.openConnection();
////                    connection.setRequestProperty("User-Agent", "");
////                    connection.setRequestMethod("POST");
////                    connection.setDoInput(true);
////                    connection.setChunkedStreamingMode(0);
////                    connection.setConnectTimeout(10000);
////
////                    Uri.Builder builder  = new Uri.Builder()
////                            .appendQueryParameter("payload",""+jsonObject);
////
////                    String query = builder.build().getEncodedQuery();
////
////                    OutputStream os = connection.getOutputStream();
////                    BufferedWriter writer = new BufferedWriter(
////                            new OutputStreamWriter(os, "UTF-8"));
////                    writer.write(query);
////                    writer.flush();
////                    writer.close();
////                    os.close();
////                    connection.connect();
////
////                    InputStream inputStream = connection.getInputStream();
////                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
////                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
////                    StringBuilder stringBuilder = new StringBuilder();
////                    String bufferedStrChunk = null;
////
////                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
////                        stringBuilder.append(bufferedStrChunk);
////                    }
////                    return stringBuilder.toString();
////                }catch  (IOException e) {
////                    e.printStackTrace();
////                }
////                finally {
////                    if(connection!=null)
////                            connection.disconnect();
////                }
////
////                return null;
////            }
////
////            @Override
////            protected void onPostExecute(String JoinCommunityResponse) {
////                super.onPostExecute(JoinCommunityResponse);
////                Log.d(TAG,"JoinCommunityResponse 1 "+JoinCommunityResponse);
////
////               // BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_SUCCESS, null);
////               // MyApplication.tranx_id_attahcment="my_id";
////
////              if(null != JoinCommunityResponse && !JoinCommunityResponse.isEmpty()) {
////
////                    try {
////                        JSONObject jsonObject=new JSONObject(JoinCommunityResponse);
////                        if(jsonObject.has("error")){
////                            boolean error=jsonObject.optBoolean("error");
////                            if(!error) {
////                              MyApplication.tranx_id_attahcment=jsonObject.optString("tranx_id");
////                           Log.d(TAG,"tranx_id: "+MyApplication.tranx_id_attahcment);
////                                BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_SUCCESS, null);
////                            }
////                            else {
////                                String error_msg=jsonObject.optString("error_msg");
////                                MyApplication.tranx_id_attahcment=null;
////                                BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_FAIL, error_msg);
////                            }
////                        }
////                        else {
////                            MyApplication.tranx_id_attahcment=null;
////                            BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_FAIL, null);
////                        }
////
////                    } catch (JSONException e) {
////                        e.printStackTrace();
////                        Log.d(TAG,""+e);
////                        MyApplication.tranx_id_attahcment=null;
////                        BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_FAIL, null);
////                    }
////                }
////                else {
////                  MyApplication.tranx_id_attahcment=null;
////                  BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_FAIL, null);
////              }
////            }
////        }
////
////        doSendTransaction doSendTransaction = new doSendTransaction();
////        doSendTransaction.execute();
//    }
//
//    public void sendTransaction2(JSONObject jsonObject){
//
//        Retrofit.Builder builder=new Retrofit.Builder()
//                .baseUrl(Keys.APPS)
//                .addConverterFactory(GsonConverterFactory.create());
//
//        Retrofit retrofit=builder.build();
////
////        List<MultipartBody.Part> parts = new ArrayList<>();
////
////        if(MyApplication.videoList!=null) {
////            for (int a = 0; a < MyApplication.videoList.size(); a++) {
////                FileField fileField=MyApplication.videoList.get(a);
////            }
////        }
//
//// add the description part within the multipart request
//      //  RequestBody payload = RetrofitHelper.createPartFromString(jsonObject.toString());
//
//// create upload service client
//        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);
//
//// finally, execute the request
//        Call<ResponseBody> call = service.locsinsert(jsonObject);
//
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                Log.d(TAG,"JoinCommunityResponse: "+response);
//                BroadcastCall.publishTransaction(context, NetworkRequest_backup.STATUS_SUCCESS, null);
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Log.d(TAG,"failure: "+call);
//                BroadcastCall.publishTransaction(context, NetworkRequest_backup.STATUS_FAIL, t.getMessage());
//            }
//        });
//    }
//
//    public void sendFile(FileField file){
//        Log.d(TAG,"inside sendFile");
//        final Handler handler=new Handler();
//        Retrofit.Builder builder=new Retrofit.Builder()
//                .baseUrl(Keys.APPS)
//                .addConverterFactory(GsonConverterFactory.create());
//
//        Retrofit retrofit=builder.build();
//
//        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);
//        final Call<ResponseBody> call = service.uploadFile(MyApplication.form_id_attachment==null?
//                        createPartFromString(""):
//                        createPartFromString(MyApplication.form_id_attachment),
//                file.getRb_question_id(),
//                file.getRb_type(), MyApplication.tranx_id_attahcment==null?createPartFromString(""):
//                createPartFromString(MyApplication.tranx_id_attahcment),file.getFile());
//
//
//        final Runnable runnable=new Runnable() {
//            @Override
//            public void run() {
//                Log.d(TAG,"termination check");
//                if(call!=null&&call.isExecuted()) {
//                    call.cancel();
//                    BroadcastCall.publishFileUpload(context, NetworkRequest_backup.STATUS_FAIL,null);
//                }
//            }
//        };
//        //this, after 2 minutes, will check if call is still processing and if so, terminate it
//       // handler.postDelayed(runnable,10000);
//
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                if(runnable!=null&&handler!=null)
//                    handler.removeCallbacks(runnable);
//
//                ResponseBody responseBody=response.body();
//                String message=null;
//                try {
//                    if(responseBody!=null)
//                        message=responseBody.string();
//
//                    Log.d(TAG,"JoinCommunityResponse: "+message+" body: "+responseBody);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    BroadcastCall.publishFileUpload(context, NetworkRequest_backup.STATUS_FAIL,message);
//                }
//
//                if(message==null||message.trim().equalsIgnoreCase("null"))
//                    BroadcastCall.publishFileUpload(context, NetworkRequest_backup.STATUS_FAIL,message);
//                else
//                    BroadcastCall.publishFileUpload(context, NetworkRequest_backup.STATUS_SUCCESS,message);
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                if(runnable!=null&&handler!=null)
//                    handler.removeCallbacks(runnable);
//
//                String message=t.getMessage();
//                Log.d(TAG,"throwable: "+t);
//                BroadcastCall.publishFileUpload(context, NetworkRequest_backup.STATUS_FAIL,message);
//            }
//        });
//    }
//
//    public void fetchCountries(){
//        final String URL= Keys.BASE_URL+"countries";
//
//        class doFetchCountry extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("id","");
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String response) {
//                super.onPostExecute(response);
//                Log.d(TAG,"JoinCommunityResponse "+response);
//
//                if(null != response && !response.isEmpty()) {
//
//                    try {
//                        JSONObject jsonObject=new JSONObject(response);
//                        if(jsonObject.has("data")){
//                            databaseHelper=new DatabaseHelper(context);
//                            databaseHelper.mashTable(DatabaseHelper.COUNTRY_TBL);
//                            databaseHelper=new DatabaseHelper(context);
//
//                            JSONArray jsonArray = jsonObject.getJSONArray("data");
//                            Country country=new Country();
//                            MyApplication.countryList.clear();
//                            MyApplication.sCountryList.clear();
//                            MyApplication.sCountryIDList.clear();
//                            for (int a = 0; a < jsonArray.length(); a++) {
//                                jsonObject = jsonArray.getJSONObject(a);
//                                country.setId(jsonObject.getString("country_id"));
//                                country.setName(jsonObject.getString("country"));
//
//                                MyApplication.countryList.add(country);
//                                MyApplication.sCountryList.add(country.getName());
//                                MyApplication.sCountryIDList.add(country.getId());
//
//                                //if(country.getId().trim().equalsIgnoreCase("b21"))
//                                databaseHelper.addItem(DatabaseHelper.COUNTRY_TBL,country);
//                            }
//                           // sendCountries(MyApplication.countryList);
//                            BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_SUCCESS,"success");
//                        }
//                        else {
//                            BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_FAIL,null);
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d(TAG,""+e);
//                        BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_FAIL,null);
//                    }
//                }
//                else {
//                    BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_FAIL,null);
//                }
//            }
//        }
//
//        doFetchCountry doFetchCountry = new doFetchCountry();
//        doFetchCountry.execute();
//    }
//
//    /**public void fetchCountries(){
//        final String URL= Keys.HGT_API+"countryList";
//
//        class doFetchCountry extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("id","");
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String JoinCommunityResponse) {
//                super.onPostExecute(JoinCommunityResponse);
//                Log.d(TAG,"JoinCommunityResponse "+JoinCommunityResponse);
//
//                if(null != JoinCommunityResponse && !JoinCommunityResponse.isEmpty()) {
//
//                    try {
//                        JSONObject jsonObject=new JSONObject(JoinCommunityResponse);
//                        if(jsonObject.has("status")){
//                            databaseHelper=new DatabaseHelper(context);
//                            databaseHelper.mashTable(DatabaseHelper.COUNTRY_TBL);
//                            databaseHelper=new DatabaseHelper(context);
//
//                            JSONArray jsonArray = jsonObject.getJSONArray("data");
//                            Country country=new Country();
//                            for (int a = 0; a < jsonArray.length(); a++) {
//                                    jsonObject = jsonArray.getJSONObject(a);
//                                    country.setId(jsonObject.getInt("country_id"));
//                                    country.setName(jsonObject.getString("country"));
//
//                                MyApplication.countryList.add(country);
//                                MyApplication.sCountryList.add(country.getName());
//                                MyApplication.sCountryIDList.add(country.getId());
//
//                                databaseHelper.addMessage(DatabaseHelper.COUNTRY_TBL,country);
//                            }
//                            sendCountries(MyApplication.countryList);
//                            BroadcastCall.publishDistrict(context,"success");
//                        }
//                        else {
//                            BroadcastCall.publishDistrict(context,null);
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d(TAG,""+e);
//                        BroadcastCall.publishDistrict(context,null);
//                    }
//                }
//                else {
//                    BroadcastCall.publishDistrict(context,null);
//                }
//            }
//        }
//
//        doFetchCountry doFetchCountry = new doFetchCountry();
//        doFetchCountry.execute();
//    }**/
//
//    public void fetchRegions(final String country_id){
//        final String URL= Keys.BASE_URL+"regions/req";
//
//        class doFetchRegion extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//                    Log.d(TAG,"countryid"+country_id);
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("countryid",""+country_id);
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String response) {
//                super.onPostExecute(response);
//                Log.d(TAG,"JoinCommunityResponse "+response);
//
//                if(null != response && !response.isEmpty()) {
//
//                    try {
//                        JSONObject jsonObject=new JSONObject(response);
//                        if(jsonObject.has("data")){
//                            databaseHelper=new DatabaseHelper(context);
//                            databaseHelper.mashTable(DatabaseHelper.REGION_TBL);
//                            databaseHelper=new DatabaseHelper(context);
//
//                            JSONArray jsonArray = jsonObject.getJSONArray("data");
//                            Region region=new Region();
//                            MyApplication.regionList.clear();
//                            MyApplication.sRegionList.clear();
//                            MyApplication.sRegionIDList.clear();
//                            for (int a = 0; a < jsonArray.length(); a++) {
//                                jsonObject = jsonArray.getJSONObject(a);
//                                region.setId(jsonObject.getString("region_id"));
//                                region.setName(jsonObject.getString("region"));
//                                region.setCountry_id(country_id);
//                               // region.setCountry_id(country_id);
//
//                                MyApplication.regionList.add(region);
//                                MyApplication.sRegionList.add(region.getName());
//                                MyApplication.sRegionIDList.add(region.getId());
//
//                                Log.d(TAG,"country_id "+country_id);
//                              if(country_id.trim().equalsIgnoreCase("b21"))
//                                    databaseHelper.addItem(DatabaseHelper.REGION_TBL,region);
//                            }
//                            BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_SUCCESS,"success");
//                        }
//                        else {
//                            BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_FAIL,null);
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d(TAG,""+e);
//                        BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_FAIL,null);
//                    }
//                }
//                else {
//                    BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_FAIL,null);
//                }
//            }
//        }
//
//        doFetchRegion doFetchRegion = new doFetchRegion();
//        doFetchRegion.execute();
//    }
//
//    public void fetchDistricts(final String region_id){
//        final String URL= Keys.BASE_URL+"districts/req";
//
//        class doFetchCountry extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//
//                    Log.d(TAG,"region_id "+region_id);
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("region_id",""+region_id);
//                            //.appendQueryParameter("country_id",""+country_id);
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String response) {
//                super.onPostExecute(response);
//                Log.d(TAG,"JoinCommunityResponse "+response);
//
//                if(null != response && !response.isEmpty()) {
//
//                    try {
//                        JSONObject jsonObject=new JSONObject(response);
//                        if(jsonObject.has("data")){
//                            databaseHelper=new DatabaseHelper(context);
//
//                            JSONArray jsonArray = jsonObject.getJSONArray("data");
//                            District district=new District();
//                            MyApplication.districtList.clear();
//                            MyApplication.sDistrictList.clear();
//                            MyApplication.sDistrictIDList.clear();
//                            for (int a = 0; a < jsonArray.length(); a++) {
//                                jsonObject = jsonArray.getJSONObject(a);
//                                district.setId(jsonObject.getString("district_id"));
//                                district.setName(jsonObject.getString("district"));
//
//                                MyApplication.districtList.add(district);
//                                MyApplication.sDistrictList.add(district.getName());
//                                MyApplication.sDistrictIDList.add(district.getId());
//                            }
//                            BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_SUCCESS,"success");
//                        }
//                        else {
//                            BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_FAIL,null);
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d(TAG,""+e);
//                        BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_FAIL,null);
//                    }
//                }
//                else {
//                    BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_FAIL,null);
//                }
//            }
//        }
//
//        doFetchCountry doFetchCountry = new doFetchCountry();
//        doFetchCountry.execute();
//    }
//
//    public void fetchTowns(final String district_id){
//        final String TOWN_URL= Keys.BASE_URL+"towns";
//
//        class doFetchTowns extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(TOWN_URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//
//                    Log.d(TAG,"district_id "+district_id);
//                    Uri.Builder builder  = new Uri.Builder()
//                              .appendQueryParameter("district_id",""+district_id);
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//
//            @Override
//            protected void onPostExecute(final String response) {
//                super.onPostExecute(response);
//                Log.d(TAG,"district id "+district_id);
//                Log.d(TAG,"JoinCommunityResponse "+response);
//
//                if(null != response && !response.isEmpty()) {
//                        try {
//                            JSONObject jsonObject=new JSONObject(response);
//                            if(jsonObject.has("data")){
//                                databaseHelper=new DatabaseHelper(context);
//                                databaseHelper.mashTable(DatabaseHelper.TOWN_TBL);
//                                databaseHelper=new DatabaseHelper(context);
//
//                                JSONArray jsonArray = jsonObject.getJSONArray("data");
//                                ArrayList<Town> townList=new ArrayList<>();
//                                Town town;
//                                MyApplication.sTownList.clear();
//                                MyApplication.sTownIDList.clear();
//                                for (int a = 0; a < jsonArray.length(); a++) {
//                                    town=new Town();
//                                    jsonObject = jsonArray.getJSONObject(a);
//                                    town.setId(jsonObject.getString("town_id"));
//                                    town.setName(jsonObject.getString("town"));
//
//                                    townList.add(town);
//
//                                    MyApplication.sTownList.add(town.getName());
//                                    MyApplication.sTownIDList.add(town.getId());
//                                   // databaseHelper.addMessage(DatabaseHelper.TOWN_TBL,town);
//                                }
//                                boolean dbInsert=databaseHelper.addItem(DatabaseHelper.TOWN_TBL,townList);
//                                if(dbInsert)
//                                    BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_SUCCESS,"date");
//                                else
//                                    BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_FAIL,null);
//                            }
//                            else {
//                                BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_FAIL,null);
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Log.d(TAG,""+e);
//                            BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_FAIL,null);
//                        }
//                }
//                else {
//                    BroadcastCall.publishDistrict(context, NetworkRequest_backup.STATUS_FAIL,null);
//                }
//            }
//        }
//
//        doFetchTowns doFetchTowns = new doFetchTowns();
//        doFetchTowns.execute();
//    }
//
//    public void fetchNeighbourhood(final String town_id){
//        final String TOWN_URL= Keys.BASE_URL+"neighbourhood";
//
//        class doFetchNeigh extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(TOWN_URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("town_id",""+town_id);
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//
//            @Override
//            protected void onPostExecute(final String response) {
//                super.onPostExecute(response);
//                Log.d(TAG,"town id "+town_id);
//                Log.d(TAG,"JoinCommunityResponse "+response);
//
//                if(null != response && !response.isEmpty()) {
//                    try {
//                        JSONObject jsonObject=new JSONObject(response);
//                        if(jsonObject.has("data")){
//                            databaseHelper=new DatabaseHelper(context);
//                            databaseHelper.mashTable(DatabaseHelper.NEIGHBOURHOOD_TBL);
//                            databaseHelper=new DatabaseHelper(context);
//
//                            JSONArray jsonArray = jsonObject.getJSONArray("data");
//                            ArrayList<Neighbourhood> neighbourhoodList=new ArrayList<>();
//                            Neighbourhood neighbourhood;
//                            for (int a = 0; a < jsonArray.length(); a++) {
//                                neighbourhood=new Neighbourhood();
//                                jsonObject = jsonArray.getJSONObject(a);
//                                neighbourhood.setTown_id(jsonObject.getString("town_id"));
//                                neighbourhood.setName(jsonObject.getString("neighbourhood"));
//                                neighbourhood.setId(jsonObject.getString("neighbourhood_id"));
//
//                                neighbourhoodList.add(neighbourhood);
//                                // databaseHelper.addMessage(DatabaseHelper.TOWN_TBL,town);
//                            }
//                            boolean dbInsert=databaseHelper.addItem(DatabaseHelper.NEIGHBOURHOOD_TBL,neighbourhoodList);
//                            if(dbInsert)
//                                BroadcastCall.publishNeighbourhood(context, NetworkRequest_backup.STATUS_SUCCESS,null);
//                            else
//                                BroadcastCall.publishNeighbourhood(context, NetworkRequest_backup.STATUS_FAIL,null);
//                        }
//                        else {
//                            BroadcastCall.publishNeighbourhood(context, NetworkRequest_backup.STATUS_FAIL,null);
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d(TAG,""+e);
//                        BroadcastCall.publishNeighbourhood(context, NetworkRequest_backup.STATUS_FAIL,null);
//                    }
//                }
//                else {
//                    BroadcastCall.publishNeighbourhood(context, NetworkRequest_backup.STATUS_FAIL,null);
//                }
//            }
//        }
//
//        doFetchNeigh doFetchNeigh = new doFetchNeigh();
//        doFetchNeigh.execute();
//    }
//
//    public void sendLocsCast(final String longitude, final String latitude,final String accuracy){
//        final String LOCSCAST_URL="http://hgtapi.silapha.com/ehandle/locscast";
//        final User user=new User(context);
//        class doLocsCast extends AsyncTask<String, Void,String> {
//
//
//            @Override
//            protected String doInBackground(String... params) {
//
//                try {
//                    URL url = new URL(LOCSCAST_URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//                    //// TODO: 8/17/2017 remember to change the product code
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("product_code", Keys.HGT_APP_ID)
//                            .appendQueryParameter("product_name", Keys.APP_NAME)
//                            .appendQueryParameter("user_id", user.getHgt_user_id())
//                            .appendQueryParameter("lon", ""+longitude)
//                            .appendQueryParameter("lat", ""+latitude)
//                            .appendQueryParameter("accuracy_m", ""+accuracy);
//
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    //connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String response) {
//                super.onPostExecute(response);
//              //  Log.d(TAG,"JoinCommunityResponse "+JoinCommunityResponse);
//            }
//        }
//
//        doLocsCast doLocsCast = new doLocsCast();
//        doLocsCast.execute();
//    }
//
//    public void fetchForms(final String intentAction){
//        final String FORMS_URL= Keys.APPS+"postFormsApi";
//
//        class doFetchForms extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(FORMS_URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("userid",(new User(context)).getId());
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//
//            @Override
//            protected void onPostExecute(final String response) {
//                super.onPostExecute(response);
//                Log.d(TAG,"fetchForms JoinCommunityResponse "+response);
//
//                if(null != response && !response.isEmpty()) {
//                    try {
//                        JSONObject jsonObject=new JSONObject(response);
//                        JSONObject jsonLocsData=jsonObject.optJSONObject("LocsData");
//                        if(jsonLocsData!=null)
//                        databaseHelper.setup(jsonLocsData);
//
//                        if(intentAction!=null){
//                            if(intentAction.equalsIgnoreCase(BroadcastCall.ORGLIST)){
//                                BroadcastCall.publishOrgUpdate(context, NetworkRequest_backup.STATUS_SUCCESS);
//                            }
//                            else if(intentAction.equalsIgnoreCase(BroadcastCall.FORM_LIST)){
//                                BroadcastCall.publishFormUpdate(context, NetworkRequest_backup.STATUS_SUCCESS);
//                            }
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d(TAG,""+e);
//
//                        if(intentAction!=null) {
//                            if (intentAction.equalsIgnoreCase(BroadcastCall.FORM_LIST))
//                                BroadcastCall.publishFormUpdate(context, NetworkRequest_backup.STATUS_FAIL);
//                            else if (intentAction.equalsIgnoreCase(BroadcastCall.ORGLIST))
//                                BroadcastCall.publishOrgUpdate(context, NetworkRequest_backup.STATUS_SUCCESS);
//                        }
//                    }
//                }
//                else {
//                    if(intentAction!=null) {
//                        if (intentAction.equalsIgnoreCase(BroadcastCall.FORM_LIST))
//                            BroadcastCall.publishFormUpdate(context, NetworkRequest_backup.STATUS_FAIL);
//                        else if (intentAction.equalsIgnoreCase(BroadcastCall.ORGLIST))
//                            BroadcastCall.publishOrgUpdate(context, NetworkRequest_backup.STATUS_SUCCESS);
//                    }
//                    }
//            }
//        }
//
//        doFetchForms doFetchForms = new doFetchForms();
//        doFetchForms.execute();
//    }
//
//    public void fetchForm(final String form_code, final boolean autoLoad){
//        final String FORMS_URL= Keys.APPS+"postFormsCode";
//
//        Log.d(TAG,"fetch by code, autoLoad: "+autoLoad);
//        class doFetchForms extends AsyncTask<String, Void,String> {
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(FORMS_URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("userid",(new User(context)).getId())
//                            .appendQueryParameter("code",form_code);
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//
//            @Override
//            protected void onPostExecute(final String response) {
//                super.onPostExecute(response);
//                Log.d(TAG,"add form JoinCommunityResponse "+response);
//
//                if(null != response && !response.isEmpty()) {
//                    try {
//                        JSONObject jsonObject=new JSONObject(response);
//                        JSONObject jsonLocsData=jsonObject.optJSONObject("LocsData");
//                        Log.d(TAG,"jsonLocsData: "+jsonLocsData);
//                        if(jsonLocsData!=null)
//                            databaseHelper.setup(jsonLocsData);
//
//                        JSONArray orgArray=jsonLocsData.getJSONArray(DatabaseHelper.TBL_ORGANIZATION);
//                        Log.d(TAG,"orgArray: "+orgArray);
//                        if(orgArray!=null&&orgArray.length()>0) {
//                            JSONObject organizationsJSON = orgArray.getJSONObject(0);
//                            JSONObject orgDetails = organizationsJSON.optJSONObject(DatabaseHelper.DETAILS);
//                            String org_id = orgDetails.optString(DatabaseHelper.ITEM_ID);
//
//                            JSONArray formArray = organizationsJSON.optJSONArray(DatabaseHelper.TBL_FORMS);
//                            if(formArray!=null&&formArray.length()>0) {
//                                JSONObject formObject = formArray.optJSONObject(0);
//                                JSONObject formDetails = formObject.optJSONObject(DatabaseHelper.DETAILS);
//                                String form_id = formDetails.optString(DatabaseHelper.ITEM_ID);
//
//                                Log.d(TAG,"form_id: "+form_id+" org_id: "+org_id);
//                                if(form_id!=null&&!form_id.trim().isEmpty()
//                                        &&org_id!=null&&!org_id.trim().isEmpty()) {
//                                    if(autoLoad) {
//                                        (new SettingsModel(context)).setForm_id(form_id);
//                                        (new SettingsModel(context)).setOrg_id(org_id);
//                                        (new SettingsModel(context)).setTransaction_id(null);
//                                        (CreateForm.getInstance(context)).setShouldRefreshForm(true);
//                                    }
//
//                                    Log.d(TAG,"settings form id: "+(new SettingsModel(context)).getForm_id());
//                                     Form form=databaseHelper.getForm(form_id);
//                                    Log.d(TAG,"form: "+form);
//                                     if(form!=null&&form.getId()!=null){
//                                         form.setAccess_code(form_code);
//                                         Log.d(TAG,"form_access_code: "+form.getAccess_code());
//                                        databaseHelper.updateFormCode(form);
//                                     }
////                                    Log.d(TAG,"form: "+form);
//                                    BroadcastCall.publishFormUpdate(context, NetworkRequest_backup.STATUS_SUCCESS);
//                                }
//                                else
//                                    BroadcastCall.publishFormUpdate(context, NetworkRequest_backup.STATUS_FAIL);
//                            }
//                            else
//                                BroadcastCall.publishFormUpdate(context, NetworkRequest_backup.STATUS_FAIL);
//                        }
//                        else
//                            BroadcastCall.publishFormUpdate(context, NetworkRequest_backup.STATUS_FAIL);
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d(TAG,""+e);
//                        BroadcastCall.publishFormUpdate(context, NetworkRequest_backup.STATUS_FAIL);
//                    }
//                }
//                else {
//                    BroadcastCall.publishFormUpdate(context, NetworkRequest_backup.STATUS_FAIL);
//                }
//            }
//        }
//
//        doFetchForms doFetchForms = new doFetchForms();
//        doFetchForms.execute();
//    }
//
//    public void sendFeedback(final String feedback){
//        final String FORMS_URL= Keys.APPS+"feedback";
//
//        class doSendfeedback extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//                try {
//                    URL url = new URL(FORMS_URL);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("userid",(new User(context)).getId())
//                            .appendQueryParameter("feedback",feedback)
//                            .appendQueryParameter("form_id",(new SettingsModel(context)).getForm_id())
//                            .appendQueryParameter("email",(new User(context)).getEmail());
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//
//            @Override
//            protected void onPostExecute(final String response) {
//                super.onPostExecute(response);
//                Log.d(TAG,"JoinCommunityResponse "+response);
//                if(response!=null)
//                    BroadcastCall.publishFeedback(context,Integer.parseInt(response));
//                else
//                    BroadcastCall.publishFeedback(context,STATUS_FAIL);
//            }
//        }
//
//        doSendfeedback doSendfeedback = new doSendfeedback();
//        doSendfeedback.execute();
//    }
//
//    public void sendFAWMappingBulk(final JSONArray jsonArray, final String mapping_id, final NetworkResponse netResponse){
//        final User user=new User(context);
//        Log.d(TAG,"mapping gps_payload: "+jsonArray);
//        Log.d(TAG,"mapping_id: "+mapping_id);
//        Log.d(TAG,"user_id: "+ (new User(context)).getId());
//
//        Retrofit.Builder builder=new Retrofit.Builder()
//                .baseUrl(Keys.FAW_API)
//                .addConverterFactory(GsonConverterFactory.create());
//
//        Retrofit retrofit=builder.build();
//
//        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);
//
//
//        Call<ResponseBody> call= service.sendFAWMappingBulk(createPartFromString(mapping_id),
//                createPartFromString(""+user.getId()),
//                jsonArray);
//
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//
//                ResponseBody responseBody=response.body();
//                String message=null;
//                try {
//                    if(responseBody!=null)
//                        message=responseBody.string();
//
//                    Log.d(TAG,"JoinCommunityResponse: "+message+" body: "+responseBody
//                            +" code: "+response.code()+" error: "+response.errorBody());
//
//                    call.cancel();
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    call.cancel();
//                }
//
//                netResponse.onRespond(message);
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                String message=t.getMessage();
//                Log.d(TAG,"throwable: "+t.getMessage()+" class "+t.getClass()+" cause "+t.getCause());
//                call.cancel();
//                netResponse.onRespond("error");
//            }
//        });
//
////        class doSendFAWMappingBulk extends AsyncTask<String, Void,String> {
////
////            @Override
////            protected String doInBackground(String... params) {
////
////                try {
////                    URL url = new URL(Keys.FAW_API+"process-mapping");
////                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
////                    connection.setRequestProperty("User-Agent", "");
////                    connection.setRequestMethod("POST");
////                    connection.setDoInput(true);
////                    connection.setConnectTimeout(10000);
////                    connection.setReadTimeout(10000);
////
////                    //// TODO: 8/17/2017 remember to change the product code
////                    Uri.Builder builder  = new Uri.Builder()
////                            .appendQueryParameter("mapping_id",""+mapping_id)
////                            .appendQueryParameter("user_id", (new User(context)).getId())
////                            .appendQueryParameter("gps_payload", ""+jsonArray);
////
////                    String query = builder.build().getEncodedQuery();
////
////                    OutputStream os = connection.getOutputStream();
////                    BufferedWriter writer = new BufferedWriter(
////                            new OutputStreamWriter(os, "UTF-8"));
////                    writer.write(query);
////                    writer.flush();
////                    writer.close();
////                    os.close();
////                    connection.connect();
////
////                    InputStream inputStream = connection.getInputStream();
////                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
////                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
////                    StringBuilder stringBuilder = new StringBuilder();
////                    String bufferedStrChunk = null;
////
////                    Log.d(TAG,"connection JoinCommunityResponse: "+connection.getResponseCode());
////
////                    switch (connection.getResponseCode()) {
////                        case HttpURLConnection.HTTP_OK:
////                            Log.d(TAG," **OK**");
////                            //connected = true;
////                            break; // fine, go on
////                        case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
////                            Log.d(TAG," **TIMEOUT**");
////                            onPostExecute(null);
////                            break;// retry
////                        case HttpURLConnection.HTTP_UNAVAILABLE:
////                            Log.d(TAG," **UNAVAILABLE**");
////                            onPostExecute(null);
////                            break;// retry, server is unstable
////                        default:
////                            Log.d(TAG," **NO CODE**");
////                            onPostExecute(null);
////                            break; // abort
////                    }
////
////                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
////                        stringBuilder.append(bufferedStrChunk);
////                    }
////                    return stringBuilder.toString();
////                }
////                catch (java.net.SocketTimeoutException e){
////                    Log.d(TAG,"timeout");
////                    onPostExecute("timeout");
////                }
////                catch  (IOException e) {
////                    e.printStackTrace();
////                    Log.d(TAG,"connection error");
////                    onPostExecute("error");
////                }
////
////                return null;
////            }
////
////            @Override
////            protected void onPostExecute(String JoinCommunityResponse) {
////                super.onPostExecute(JoinCommunityResponse);
////                Log.d(TAG,"JoinCommunityResponse: "+JoinCommunityResponse);
////                netResponse.onRespond(JoinCommunityResponse);
////            }
////        }
////
////        doSendFAWMappingBulk doSendFAWMappingBulk = new doSendFAWMappingBulk();
////        doSendFAWMappingBulk.execute();
//    }
//
//    public void sendFAWMapping(final POI poi, final String mapping_id, final NetworkResponse netResponse){
//        final User user=new User(context);
//        class doSendFAWMapping extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//
//                try {
//                    URL url = new URL(Keys.FAW_API+"process-mapping");
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//                    Log.d(TAG,"mapping_id: "+mapping_id
//                            +" long: "+poi.getLon()
//                            +" lat: "+poi.getLat()+"" +
//                            " accur: "+poi.getAccuracy()
//                            +" user_id: "+(new User(context)).getId());
//                    //// TODO: 8/17/2017 remember to change the product code
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("mapping_id",""+mapping_id)
//                            .appendQueryParameter("user_id", (new User(context)).getId())
//                            .appendQueryParameter("longitude", poi.getLon())
//                            .appendQueryParameter("latitude", poi.getLat())
//                            .appendQueryParameter("accuracy", poi.getAccuracy());
//
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String response) {
//                super.onPostExecute(response);
//                Log.d(TAG,"JoinCommunityResponse: "+response);
//                netResponse.onRespond(response);
//            }
//        }
//
//        doSendFAWMapping doSendFAWMapping = new doSendFAWMapping();
//        doSendFAWMapping.execute();
//    }
//
//    public void fetchKnowledgeBase(final NetworkResponse netResponse){
//        class doKB extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//
//                try {
//                    String platform="app";
//                    String path=Keys.FAW_API
//                            +"kb?lang="+(new SettingsModel(context)).getLang()+""
//                            +"&platform="+platform+"";
//                    Log.d(TAG,"kb path: "+path);
//                    URL url = new URL(path);
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//                    //// TODO: 8/17/2017 remember to change the product code
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("user_id",""+(new User(context)).getHgt_user_id());
//
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }catch  (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String response) {
//                super.onPostExecute(response);
//                netResponse.onRespond(response);
//            }
//        }
//
//        doKB doKB = new doKB();
//        doKB.execute();
//    }
//
//}