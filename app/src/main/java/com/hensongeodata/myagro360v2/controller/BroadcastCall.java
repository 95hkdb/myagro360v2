package com.hensongeodata.myagro360v2.controller;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.fragment.BSFragment;
import com.hensongeodata.myagro360v2.view.FormList;
import com.hensongeodata.myagro360v2.view.OrgList;


/**
 * Created by user1 on 8/2/2017.
 */

public class BroadcastCall {
    public static String TRANSACTION_UPDATE = "android.intent.action.locsmman_transaction";
    public static String FILE_UPDATE = "android.intent.action.locsmman_file_upload";
    public static String SETTINGS_UPDATE = "android.intent.action.locsmman_settings";
    public static String DISTRICT_LOAD_UPDATE = "android.intent.action.locsmman_district";
    public static String CLASS_UPDATE = "android.intent.action.locsmman_class";
    public static String SIGN_IN = "android.intent.action.locsmman_signin";
    public static String SIGN_UP = "android.intent.action.locsmman_signup";
    public static String UPDATE_PROFILE = "android.intent.action.locsmman_update_profile";
    public static String RECOVER = "android.intent.action.locsmman_recover";
    public static String RESET = "android.intent.action.locsmman_reset";
    public static String UPDATE_PASSWORD = "android.intent.action.locsmman_update_password";
    public static String UPDATE_NEIGHBOURHOOD = "android.intent.action.locsmman_update_neighbourhood";
    public static String BSFRAGMENT="android.intent.action.locsmman_update_bsfragment";
    public static String MESSAGE="legendary.com.locsmman.chat";
    public static String FEEDBACK ="legendary.com.locsmman.feedback";
    public static String FORM_LIST ="legendary.com.locsmman.loadform";
    public static String ORGLIST="legendary.com.locsmman.orgadapter.orglist";

      public static String WEATHER = "com.hensongeodata.technologies.weather";
//    public static String ORGLIST="legendary.com.locsmman.orgadapter.orglist";

      public static void publishWeather(Context context, String locationName, String weatherMain, String description, String temperature, String humidity, String pressure) {
            Intent intent = new Intent(WEATHER);
            intent.setAction(WEATHER);
            intent.putExtra("location", locationName);
            intent.putExtra("weather", weatherMain);
            intent.putExtra("description", description);
            intent.putExtra("temperature", temperature);
            intent.putExtra("humidity", humidity);
            intent.putExtra("pressure", pressure);
            context.sendBroadcast(intent);
      }

    public static void publishSignin(Context context,int status,String message){
        Intent intent = new Intent(SIGN_IN);
        intent.setAction(SIGN_IN);
        intent.putExtra("date",message);
        intent.putExtra("status",status);
        context.sendBroadcast(intent);
    }
    public static void publishSignup(Context context,int status,String message){
        Intent intent = new Intent(SIGN_UP);
        intent.setAction(SIGN_UP);
        intent.putExtra("date",message);
        intent.putExtra("status",status);
        context.sendBroadcast(intent);
    }

    public static void publishUpdateProfile(Context context,int status,String message){
        Intent intent = new Intent(UPDATE_PROFILE);
        intent.setAction(UPDATE_PROFILE);
        intent.putExtra("date",message);
        intent.putExtra("status",status);
        context.sendBroadcast(intent);
    }

    public static void publishRecover(Context context,String message){
        Intent intent = new Intent(RECOVER);
        intent.setAction(RECOVER);
        intent.putExtra("date",message);
        context.sendBroadcast(intent);
    }

    public static void publishReset(Context context,String message){
        Intent intent = new Intent(RESET);
        intent.setAction(RESET);
        intent.putExtra("date",message);
        context.sendBroadcast(intent);
    }

    public static void publishUpdatePassword(Context context,String message){
        Intent intent = new Intent(UPDATE_PASSWORD);
        intent.setAction(UPDATE_PASSWORD);
        intent.putExtra("date",message);
        context.sendBroadcast(intent);
    }

    public static void publishTransaction(Context context,int status, String message){
        Intent intent = new Intent(TRANSACTION_UPDATE);
        intent.setAction(TRANSACTION_UPDATE);
        intent.putExtra("date",message);
        intent.putExtra("status",status);
        intent.putExtra("location",false);

        context.sendBroadcast(intent);
    }

    public static void publishFileUpload(Context context,int status, String message){
        Intent intent = new Intent(FILE_UPDATE);
        intent.setAction(FILE_UPDATE);
        intent.putExtra("date",message);
        intent.putExtra("status",status);

        context.sendBroadcast(intent);
        Log.d("BroadcastCall","publishFileUpload");
    }

    public static void publishSettings(Context context,String message){
        Intent intent = new Intent(SETTINGS_UPDATE);
        intent.setAction(SETTINGS_UPDATE);
        intent.putExtra("date",message);
        context.sendBroadcast(intent);
    }

    public static void publishDistrict(Context context,int status,String message){
        Intent intent = new Intent(DISTRICT_LOAD_UPDATE);
        intent.setAction(DISTRICT_LOAD_UPDATE);
        intent.putExtra("date",message);
        intent.putExtra("status",status);
        context.sendBroadcast(intent);
    }

    //Publish service type JoinCommunityResponse to calling receiver
  public static void publishClassLoad(Context context,String message){
        Intent intent = new Intent(CLASS_UPDATE);
        intent.setAction(CLASS_UPDATE);
        intent.putExtra("date",message);
        context.sendBroadcast(intent);
    }

    public static void publishNeighbourhood(Context context,int status,String message){
        Intent intent = new Intent(UPDATE_NEIGHBOURHOOD);
        intent.setAction(UPDATE_NEIGHBOURHOOD);
        intent.putExtra("date",message);
        intent.putExtra("status",status);
        context.sendBroadcast(intent);
    }

    public static void publishLocationUpdate(Context context, int status, String action){
        Intent intent = new Intent(BSFRAGMENT);
        intent.setAction(BSFRAGMENT);

        if(action.equals(BSFragment.ACTION_LOCATION)){
        String strAccur=MyApplication.poi.getAccuracy();
        if(strAccur!=null&&!strAccur.trim().isEmpty()) {

            Double accuracy = Math.ceil(Double.valueOf(strAccur));
            String accur = String.valueOf(accuracy.intValue());
            MyApplication.poi.setAccuracy(accur);

            (new NetworkRequest(context)).sendLocsCast(MyApplication.poi.getLon(),
                    MyApplication.poi.getLat(), accur);
        }
        }

        intent.putExtra("status", status);
        intent.putExtra("action", action);
        context.sendBroadcast(intent);
        Log.d("Broadcast","location broadcast");
    }

    public static void publishFormUpdate(Context context,int status){
        Intent intent=new Intent(FORM_LIST);
        intent.setAction(FORM_LIST);
        intent.putExtra("action",FormList.ACTION_DOWNLOAD);
        intent.putExtra("status",status);
        context.sendBroadcast(intent);
        Log.d("BroadcastCall","formUpdate");
    }

    public static void publishOrgUpdate(Context context,int status){
        Intent intent=new Intent(ORGLIST);
        intent.setAction(ORGLIST);
        intent.putExtra("action", OrgList.ACTION_DOWNLOAD);
        intent.putExtra("status",status);
        context.sendBroadcast(intent);
    }

    public static void publishFeedback(Context context,int status){
        Intent intent=new Intent(FEEDBACK);
        intent.setAction(FEEDBACK);
        intent.putExtra("status",status);
        context.sendBroadcast(intent);
    }

    public static void publishMessage(Context context, String message_id, int status){
        Intent intent=new Intent(MESSAGE);
        intent.setAction(MESSAGE);
        intent.putExtra("msg_id",message_id);

        context.sendBroadcast(intent);
    }
}
