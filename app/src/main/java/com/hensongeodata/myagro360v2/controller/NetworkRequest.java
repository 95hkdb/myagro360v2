package com.hensongeodata.myagro360v2.controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.hensongeodata.myagro360v2.Interface.AuthListener;
import com.hensongeodata.myagro360v2.Interface.JoinCommunityListener;
import com.hensongeodata.myagro360v2.Interface.NetworkResponse;
import com.hensongeodata.myagro360v2.Interface.PaymentListener;
import com.hensongeodata.myagro360v2.Interface.ProductsListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.api.request.LocsmmanProAPI;
import com.hensongeodata.myagro360v2.api.response.AuthenticateResponse;
import com.hensongeodata.myagro360v2.model.Country;
import com.hensongeodata.myagro360v2.model.Device;
import com.hensongeodata.myagro360v2.model.District;
import com.hensongeodata.myagro360v2.model.FileField;
import com.hensongeodata.myagro360v2.model.Form;
import com.hensongeodata.myagro360v2.model.FormFarmMappingResponse;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.Message;
import com.hensongeodata.myagro360v2.model.Neighbourhood;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.model.Pay;
import com.hensongeodata.myagro360v2.model.Product;
import com.hensongeodata.myagro360v2.model.Region;
import com.hensongeodata.myagro360v2.model.ServiceClass;
import com.hensongeodata.myagro360v2.model.SettingsModel;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.model.Town;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.newmodel.ActivityModel;
import com.hensongeodata.myagro360v2.newmodel.PostResponse;
import com.hensongeodata.myagro360v2.newmodel.WeatherModel;
import com.hensongeodata.myagro360v2.view.Chat;
import com.hensongeodata.myagro360v2.view.MainActivityAutomated;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import me.pushy.sdk.Pushy;
import me.pushy.sdk.util.exceptions.PushyException;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.hensongeodata.myagro360v2.controller.RetrofitHelper.createPartFromString;

/**
 * Created by user1 on 8/8/2017.
 */

public class NetworkRequest {
    public static int STATUS_SUCCESS = 100;
    public static int STATUS_FAIL = 200;
    public static int STATUS_FAILED_CONNECTION = 300;
    DatabaseHelper databaseHelper;
    SharePrefManager sharePrefManager;
    private String TAG = NetworkRequest.class.getSimpleName();
    private AuthListener authListener;
    private Context context;

    public NetworkRequest(Context context) {
        this.context = context;
        databaseHelper = new DatabaseHelper(context);
        sharePrefManager = new SharePrefManager(context);
    }

    public void setAuthListener(AuthListener authListener) {
        this.authListener = authListener;
    }

    public void okpptRetrofit(){
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    public void close() {
        databaseHelper.close();
    }

    public void signUp(final User user) {
        final SettingsModel settingsModel = new SettingsModel(context);
        final String URL = Keys.CENTRAL + "signup";
      /*  Log.d(TAG,"country: "+com.faw.myagro360.view.District.country
                +" region: "+com.faw.myagro360.view.District.region
                +" district: "+com.faw.myagro360.view.District.district
                +"town"+com.faw.myagro360.view.District.town);*/
        class doSignup extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);
                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("api_key", Keys.API_KEY)
                            .appendQueryParameter("appid", Keys.HGT_APP_ID)
                            .appendQueryParameter("email", user.getEmail())
                            .appendQueryParameter("password", user.getPassword())
                            .appendQueryParameter("firstName", user.getFirstname())
                            .appendQueryParameter("lastName", user.getLastname())
                            .appendQueryParameter("type", user.getType())
                            .appendQueryParameter("phone", user.getPhone())
                            .appendQueryParameter("country", settingsModel.getCountry())
                            .appendQueryParameter("region", settingsModel.getRegion())
                            .appendQueryParameter("district", settingsModel.getDistrict())
                            .appendQueryParameter("town", settingsModel.getTown())
                            .appendQueryParameter("pushID", user.getPush_id());
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.d(TAG, "response " + response);

                if (null != response && !response.isEmpty()) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String error_message = jsonObject.optString("error_msg");
                        Boolean error = jsonObject.optBoolean("error");

                        if (!error) {
                            String user_id = jsonObject.optString("user_id");
                            (new MyApplication()).initUser(context, user_id);

                            User mUser = new User(context);
                            mUser.setFirstname(user.getFirstname());
                            mUser.setLastname(user.getLastname());
                            mUser.setEmail(user.getEmail());
                            mUser.setPassword(user.getPassword());
                            mUser.setId(user_id);
                            mUser.setHgt_user_id(jsonObject.optString("hgtuser_id"));
                            mUser.setPush_id(user.getPush_id());
                            mUser.setPhone(user.getPhone());
                            mUser.setOrganization(user.getOrganization());
                            mUser.setType(user.getType());
                            BroadcastCall.publishSignup(context, NetworkRequest.STATUS_SUCCESS, null);
                        } else {
                            BroadcastCall.publishSignup(context, NetworkRequest.STATUS_FAIL, error_message);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "" + e);
                        BroadcastCall.publishSignup(context, NetworkRequest.STATUS_FAIL, null);
                    }
                } else {
                    BroadcastCall.publishSignup(context, NetworkRequest.STATUS_FAIL, null);
                }
            }
        }

        doSignup doSignup = new doSignup();
        doSignup.execute();
    }

    public void signUp_back(final User user) {
        Log.d(TAG, "signup");

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Keys.CENTRAL)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

        final Call<ResponseBody> call = service.signup(
                createPartFromString("" + Keys.HGT_APP_ID),
                createPartFromString("" + user.getEmail()),
                createPartFromString("" + user.getPassword()),
                createPartFromString("" + user.getFirstname()),
                createPartFromString("" + user.getLastname()),
                createPartFromString("" + user.getType()),
                createPartFromString("" + com.hensongeodata.myagro360v2.view.District.country),
                createPartFromString("" + com.hensongeodata.myagro360v2.view.District.region),
                createPartFromString("" + com.hensongeodata.myagro360v2.view.District.district),
                createPartFromString("" + com.hensongeodata.myagro360v2.view.District.town),
                createPartFromString("" + user.getOrganization()),
                createPartFromString("" + user.getPush_id()), Keys.API_KEY);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                ResponseBody responseBody = response.body();
                String data = null;
                try {
                    if (responseBody != null)
                        data = responseBody.string();
                    Log.d(TAG, "JoinCommunityResponse " + response);

                    if (null != data && !data.isEmpty()) {

                        try {
                            JSONObject jsonObject = new JSONObject(data);
                            String error_message = jsonObject.optString("error_msg");
                            Boolean error = jsonObject.optBoolean("error");

                            if (!error) {
                                String user_id = jsonObject.optString("user_id");
                                (new MyApplication()).initUser(context, user_id);

                                User mUser = new User(context);
                                mUser.setFirstname(user.getFirstname());
                                mUser.setLastname(user.getLastname());
                                mUser.setEmail(user.getEmail());
                                mUser.setPassword(user.getPassword());
                                mUser.setId(user_id);
                                mUser.setHgt_user_id(jsonObject.optString("hgtuser_id"));
                                mUser.setPush_id(user.getPush_id());
                                mUser.setPhone(user.getPhone());

                                BroadcastCall.publishSignup(context, NetworkRequest.STATUS_SUCCESS, null);
                            } else {
                                BroadcastCall.publishSignup(context, NetworkRequest.STATUS_FAIL, error_message);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "" + e);
                            BroadcastCall.publishSignup(context, NetworkRequest.STATUS_FAIL, null);
                        }
                    } else {
                        BroadcastCall.publishSignup(context, NetworkRequest.STATUS_FAIL, null);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    BroadcastCall.publishSignup(context, NetworkRequest.STATUS_FAIL, null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String date = t.getMessage();
                Log.d(TAG, "throwable: " + t);

                BroadcastCall.publishSignup(context, NetworkRequest.STATUS_FAIL, "" + t);
            }
        });
    }

    public void signin(final User user, final String orgid) {
        final String URL = Keys.CENTRAL + "login";
        final Device device = (new MyApplication()).getDevice();

       if (!checkToken()){
           new RegisterForPush().execute();
       }

        class doSignin extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);

                    //Log.d(TAG,"version: "+device.getAndroidVersion()+" name: "+device.getName()
                    //+" model: "+device.getModel()+" serial: "+device.getSerial());
                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("api_key", Keys.API_KEY)
                            .appendQueryParameter("email", user.getEmail())
                            .appendQueryParameter("password", user.getPassword())
                            .appendQueryParameter("device_name", device.getName())
                            .appendQueryParameter("version", device.getAndroidVersion())
                            .appendQueryParameter("model", device.getModel())
                            .appendQueryParameter("serial_number", device.getSerial());

                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.d(TAG, "response" + response);

                if (null != response && !response.isEmpty()) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String error_message = jsonObject.optString("error_msg");
                        Boolean error = jsonObject.optBoolean("error");

                        if (!error) {
                            String user_id = jsonObject.optString("user_id");
                            String user_fname = jsonObject.optString("first_name");
                            String user_lname = jsonObject.optString("last_name");
                            String user_profession = jsonObject.optString("profession");
                            sharePrefManager.userLogin(user_id,user_fname,user_lname,user_profession);
                              String user_pushToken = sharePrefManager.getUserDetails().get(3);

                            checkOrganisation(orgid);

                        } else {

                            BroadcastCall.publishSignin(context, NetworkRequest.STATUS_FAIL, error_message);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "" + e);
                        BroadcastCall.publishSignin(context, NetworkRequest.STATUS_FAIL, null);
                    }
                } else {
                    BroadcastCall.publishSignin(context, NetworkRequest.STATUS_FAIL, null);
                }
            }
        }

        doSignin doSignin = new doSignin();
        doSignin.execute();
    }

    private boolean checkToken(){
          return SharePrefManager.getInstance(context).getUserDetails().get(3) != null;
    }

    private void checkOrganisation(final String orgid) {

        final String u_email = sharePrefManager.getUserDetails().get(0);
        final String u_fname = sharePrefManager.getUserDetails().get(1);
        final String u_lname = sharePrefManager.getUserDetails().get(2);
        final String u_profession = sharePrefManager.getUserDetails().get(4);
        final String u_push = sharePrefManager.getUserDetails().get(3);

        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Account Validation ...");
        progressDialog.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Keys.AGRO_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        LocsmmanProAPI api = retrofit.create(LocsmmanProAPI.class);
        Call<AuthenticateResponse> call = api.checkOrganisation(orgid, u_email, u_fname,
                u_lname, u_profession, u_push);
        call.enqueue(new Callback<AuthenticateResponse>() {
            @Override
            public void onResponse(Call<AuthenticateResponse> call, Response<AuthenticateResponse> response) {
                  String error_message = response.body().getMessage();
                  progressDialog.dismiss();
                  if (!response.body().getError()) {
                    String org_name = response.body().getOrg_name();
                    String org_email = response.body().getOrg_email();
                    String org_phone = response.body().getOrg_phone();
                    String logo_url = response.body().getLogo_url();
                    SharePrefManager.getInstance(context).organisation(orgid,org_name,org_email,org_phone,logo_url);
                    (new MyApplication()).initUser(context, sharePrefManager.getUserDetails().get(0));

                    User mUser = new User(context);
                    mUser.setFirstname(u_fname);
                    mUser.setLastname(u_lname);
                    mUser.setPhone("");
                    mUser.setType("");
                    mUser.setEmail(u_email);
                    mUser.setPassword("");
                    mUser.setId(u_email);
                    mUser.setHgt_user_id("");
                    Log.d(TAG, "signin pushID: " + u_push);
                    mUser.setPush_id(u_push);

                    if (u_profession != null && !u_profession.trim().isEmpty() && !u_profession.trim().equalsIgnoreCase("null")) {
                        mUser.setProfession_id(Chat.getChatroom_id(context, u_profession));
                    }
                    mUser.setChatroomActive_id(null);//remove active chat so user can rejoin chat
                    BroadcastCall.publishSignin(context, NetworkRequest.STATUS_SUCCESS, error_message);

                } else
                        BroadcastCall.publishSignin(context, NetworkRequest.STATUS_FAIL, error_message);
            }

            @Override
            public void onFailure(Call<AuthenticateResponse> call, Throwable t) {
                progressDialog.dismiss();
                  BroadcastCall.publishSignin(context, NetworkRequest.STATUS_SUCCESS, "Please check your internet. Connect and try again");
                  Log.v("Error: ", t.getMessage());
            }
        });

    }

//    new request starts from here

    /*
    *   Get recent last five recent post from your organisation
    */
    public void recentPost(){
          final String u_email = SharePrefManager.getInstance(context).getUserDetails().get(0);
          final String u_orgcode = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

        //final ProgressDialog progressDialog = new ProgressDialog(context);
        //progressDialog.setMessage("Account Validation ...");
        //progressDialog.show();

        okpptRetrofit();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Keys.AGRO_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        LocsmmanProAPI api = retrofit.create(LocsmmanProAPI.class);
        Call<PostResponse> call = api.newPost(u_email,u_orgcode);
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {

                Log.e(TAG, response.message());
//                if (response.isSuccessful()) {
////                    Log.e(TAG, "Data Response: " + response.body().getPost_creator());
//                    String url = response.body().getPost_image();
//                    String creator = response.body().getPost_creator();
//
////                      Log.e(TAG, "onResponse: " + url  + ", " + creator + ":  " + response.body().getPostContent());
//
////                    SharePrefManager.getInstance(context).newpost(url, creator, response.body().getPostContent());
//
//                } else {
//                    // error case
//                    switch (response.code()) {
//                        case 404:
//                            Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
//                            break;
//                        case 500:
//                            // Toast.makeText(context, "Server Broken", Toast.LENGTH_SHORT).show();
//                            break;
//                        default:
//                            Toast.makeText(context, "Unknown Error", Toast.LENGTH_SHORT).show();
//                            break;
//                    }
//                }

                //Toast.makeText(context, "Data: " + url + ", " + creator, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                Log.e("Error: ", "Big error " + t.getMessage());
                //Toast.makeText(context, "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //  Get recent recorded activity
    public void getRecentActivities(){
        final String u_email = SharePrefManager.getInstance(context).getUserDetails().get(0);
        final String u_orgcode = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);

        okpptRetrofit();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Keys.AGRO_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        LocsmmanProAPI api = retrofit.create(LocsmmanProAPI.class);
        Call<PostResponse> call = api.newPost(u_email, u_orgcode);
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {

            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {

            }
        });
    }

    //  Get activities
    public void getFarmActivity(){
        okpptRetrofit();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Keys.AGRO_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    //  Get weather forecast for the day
    public void getWeather(String api_key, String lon, String lat){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Keys.SILAPHA_WEATHER)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

        final Call<WeatherModel> call = service.weather(api_key, lon, lat);
        call.enqueue(new Callback<WeatherModel>() {
            @Override
            public void onResponse(Call<WeatherModel> call, Response<WeatherModel> response) {
                  if (response.isSuccessful()) {
                        String locationName = response.body().getLocationName();
                        String weatherMain = response.body().getWeatherMain();
                        String description = response.body().getWeatherDescription();
                        String humidity = response.body().getHumidity();
//                        String temperature = response.body().getTemperature();
                        String pressure = response.body().getPressure();

                        Log.e(TAG, "onResponse: " + pressure);
//                        SharePrefManager.getInstance(context).newWeather(locationName, weatherMain, description, temperature, humidity, pressure);
                  }
            }

            @Override
            public void onFailure(Call<WeatherModel> call, Throwable t) {
                Toast.makeText(context, "Weather error" + t.getMessage(), Toast.LENGTH_SHORT).show();
                  Log.v(TAG, "onFailure: " + t.getMessage());
            }
        });

    }

    //  Send recorded activity to server
    public void sendActivity(final String activity_type, final String duration, final String comment, final String worker) {
        Log.d(TAG, "send Activity");
        final String u_email = sharePrefManager.getUserDetails().get(0);
        final String u_orgcode = sharePrefManager.getOrganisationDetails().get(0);

        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Saving Activity ...");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Keys.AGRO_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);
        final Call<ActivityModel> call = service.sendRecentActivity(
                createPartFromString(""+ activity_type),
                createPartFromString(""+ duration),
                createPartFromString(""+ comment),
                createPartFromString("" + worker),
                createPartFromString(""+ u_orgcode),
                createPartFromString(""+ u_email)
        );

        call.enqueue(new Callback<ActivityModel>() {
            @Override
            public void onResponse(Call<ActivityModel> call, Response<ActivityModel> response) {
                progressDialog.dismiss();
                  if (!response.body().isError()) {
                      Log.e(TAG, "Response for activity");
                      SharePrefManager.getInstance(context).newActivity(activity_type, duration, comment);
                      Toast.makeText(context, "Successfully saved", Toast.LENGTH_SHORT).show();
                  }

                  else{
//                        Do something here
                        Toast.makeText(context, "Error occured", Toast.LENGTH_SHORT).show();
                  }
            }

            @Override
            public void onFailure(Call<ActivityModel> call, Throwable t) {
                progressDialog.dismiss();
                  Log.d(TAG, t.getMessage());
                  Toast.makeText(context, "No Internet. Please connect to a WIFI or mobile data. " , Toast.LENGTH_SHORT).show();
            }
        });

    }

//    End

    public void signin_back(final User user) {
        Log.d(TAG, "signin");
        final Device device = (new MyApplication()).getDevice();
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Keys.CENTRAL)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

        final Call<ResponseBody> call = service.signin(
                createPartFromString("" + user.getEmail()),
                createPartFromString("" + user.getPassword()),
                createPartFromString("" + device.getName()),
                createPartFromString("" + device.getAndroidVersion()),
                createPartFromString("" + device.getModel()),
                createPartFromString("" + device.getSerial()), Keys.API_KEY);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                ResponseBody responseBody = response.body();
                String data = null;
                try {
                    if (responseBody != null)
                        data = responseBody.string();
                    Log.d(TAG, "signin response " + data);

                    if (null != data && !data.isEmpty()) {

                        try {
                            JSONObject jsonObject = new JSONObject(data);
                            String error_message = jsonObject.optString("error_msg");
                            Boolean error = jsonObject.optBoolean("error");

                            if (!error) {
                                String user_id = jsonObject.optString("user_id");
                                (new MyApplication()).initUser(context, user_id);

                                User mUser = new User(context);
                                // mUser.setFirstname(user.getFirstname());
                                // mUser.setLastname(user.getLastname());
                                mUser.setEmail(user.getEmail());
                                mUser.setPassword(user.getPassword());
                                mUser.setId(user_id);
                                mUser.setHgt_user_id(jsonObject.optString("hgtuser_id"));
                                mUser.setPush_id(user.getPush_id());
                                // mUser.setPhone(user.getPhone());

                                BroadcastCall.publishSignin(context, NetworkRequest.STATUS_SUCCESS, error_message);
                            } else {

                                BroadcastCall.publishSignin(context, NetworkRequest.STATUS_FAIL, error_message);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "" + e);
                            BroadcastCall.publishSignin(context, NetworkRequest.STATUS_FAIL, null);
                        }
                    } else {
                        BroadcastCall.publishSignin(context, NetworkRequest.STATUS_FAIL, null);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    BroadcastCall.publishSignin(context, NetworkRequest.STATUS_FAIL, null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String date = t.getMessage();
                Log.d(TAG, "throwable: " + t);

                BroadcastCall.publishSignin(context, NetworkRequest.STATUS_FAIL, null);
            }
        });
    }

    public void updateProfile(final User user) {
        final SettingsModel settingsModel = new SettingsModel(context);
        final String URL = Keys.CENTRAL + "update";
        class doUpdateProfile extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);
                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("api_key", Keys.API_KEY)
                            .appendQueryParameter("appid", Keys.HGT_APP_ID)
                            .appendQueryParameter("email", user.getEmail())
                            .appendQueryParameter("firstName", user.getFirstname())
                            .appendQueryParameter("lastName", user.getLastname())
                            .appendQueryParameter("type", user.getType())
                            .appendQueryParameter("phone", user.getPhone())
                            .appendQueryParameter("country", settingsModel.getCountry())
                            .appendQueryParameter("region", settingsModel.getRegion())
                            .appendQueryParameter("district", settingsModel.getDistrict())
                            .appendQueryParameter("town", settingsModel.getTown())
                            .appendQueryParameter("org", user.getOrganization())
                            .appendQueryParameter("pushID", user.getPush_id());
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.d(TAG, "response " + response);

                if (null != response && !response.isEmpty()) {

                    if (response.contains("profileupdated")) {

                        User mUser = new User(context);
                        mUser.setFirstname(user.getFirstname());
                        mUser.setLastname(user.getLastname());
                        mUser.setEmail(user.getEmail());
                        mUser.setPhone(user.getPhone());
                        mUser.setOrganization(user.getOrganization());
                        mUser.setType(user.getType());

                        (new MyApplication()).initUser(context, user.getId());

                        BroadcastCall.publishUpdateProfile(context, NetworkRequest.STATUS_SUCCESS, null);
                    } else {
                        BroadcastCall.publishUpdateProfile(context, NetworkRequest.STATUS_FAIL, null);
                    }
                } else {
                    BroadcastCall.publishUpdateProfile(context, NetworkRequest.STATUS_FAIL, null);
                }
            }
        }

        doUpdateProfile doUpdateProfile = new doUpdateProfile();
        doUpdateProfile.execute();
    }

    public void updateProfile_back(final User user) {
        Log.d(TAG, "signup");

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Keys.CENTRAL)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

        final Call<ResponseBody> call = service.update(
                createPartFromString("" + Keys.APP_ID),
                createPartFromString("" + user.getEmail()),
                createPartFromString("" + user.getPassword()),
                createPartFromString("" + user.getFirstname()),
                createPartFromString("" + user.getLastname()),
                createPartFromString("" + user.getType()),
                createPartFromString("" + com.hensongeodata.myagro360v2.view.District.country),
                createPartFromString("" + com.hensongeodata.myagro360v2.view.District.region),
                createPartFromString("" + com.hensongeodata.myagro360v2.view.District.district),
                createPartFromString("" + com.hensongeodata.myagro360v2.view.District.town),
                createPartFromString("" + user.getOrganization()),
                createPartFromString("" + user.getPush_id()), Keys.API_KEY);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                ResponseBody responseBody = response.body();
                String data = null;
                try {
                    if (responseBody != null)
                        data = responseBody.string();
                    Log.d(TAG, "updateprofile response " + data);

                    if (null != data && !data.isEmpty()) {

                        try {
                            JSONObject jsonObject = new JSONObject(data);
                            String error_message = jsonObject.optString("error_msg");
                            Boolean error = jsonObject.optBoolean("error");

                            if (!error) {
                                String user_id = jsonObject.optString("user_id");
                                (new MyApplication()).initUser(context, user_id);

                                User mUser = new User(context);
                                mUser.setFirstname(user.getFirstname());
                                mUser.setLastname(user.getLastname());
                                mUser.setEmail(user.getEmail());
                                mUser.setPassword(user.getPassword());
                                mUser.setId(user_id);
                                mUser.setHgt_user_id(jsonObject.optString("hgtuser_id"));
                                mUser.setPush_id(user.getPush_id());
                                mUser.setPhone(user.getPhone());

                                BroadcastCall.publishUpdateProfile(context, NetworkRequest.STATUS_SUCCESS, null);
                            } else {
                                BroadcastCall.publishUpdateProfile(context, NetworkRequest.STATUS_FAIL, error_message);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "" + e);
                            BroadcastCall.publishUpdateProfile(context, NetworkRequest.STATUS_FAIL, null);
                        }
                    } else {
                        BroadcastCall.publishUpdateProfile(context, NetworkRequest.STATUS_FAIL, null);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    BroadcastCall.publishUpdateProfile(context, NetworkRequest.STATUS_FAIL, null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String date = t.getMessage();
                Log.d(TAG, "throwable: " + t);

                BroadcastCall.publishUpdateProfile(context, NetworkRequest.STATUS_FAIL, "" + t);
            }
        });
    }

    public void registerPush(final User user) {
        final String URL = Keys.AGRO_BASE + "authenticateUser?api_key=" + Keys.API_KEY;
        class doAuth extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);

                    user.setPush_id(Pushy.register(context));
                    Log.d(TAG, "authenticate pushID: " + user.getPush_id());

                    (new User(context)).setPush_id(user.getPush_id());

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("api_key", Keys.API_KEY)
                            .appendQueryParameter("user_id", user.getId())
                            .appendQueryParameter("push_id", user.getPush_id());

                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (PushyException e) {
                    Log.d(TAG, "Pushy e: " + e);
                    // authListener.onAuthenticate(NetworkRequest.STATUS_FAIL,-1);
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.d(TAG, "Response " + response);

                if (null != response && !response.isEmpty()) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        authListener.onAuthenticate(NetworkRequest.STATUS_SUCCESS, jsonObject.optInt("officer"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "" + e);
                        authListener.onAuthenticate(NetworkRequest.STATUS_FAIL, -1);
                    }
                } else {
                    authListener.onAuthenticate(NetworkRequest.STATUS_FAIL, -1);
                }
            }
        }

        doAuth doAuth = new doAuth();
        doAuth.execute();
    }

    public void registerPush_back(final User user) {
        Log.d(TAG, "signup");

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Keys.AGRO_BASE)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

        final Call<ResponseBody> call = service.registerPush(
                createPartFromString("" + user.getId()),
                createPartFromString("" + user.getPush_id()), Keys.API_KEY);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                ResponseBody responseBody = response.body();
                String data = null;
                try {
                    if (responseBody != null)
                        data = responseBody.string();
                    Log.d(TAG, "updateprofile response " + data);

                    if (null != data && !data.isEmpty()) {

                        try {
                            JSONObject jsonObject = new JSONObject(data);
                            authListener.onAuthenticate(NetworkRequest.STATUS_SUCCESS, jsonObject.optInt("officer"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "" + e);
                            authListener.onAuthenticate(NetworkRequest.STATUS_FAIL, -1);
                        }
                    } else {
                        authListener.onAuthenticate(NetworkRequest.STATUS_FAIL, -1);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    authListener.onAuthenticate(NetworkRequest.STATUS_FAIL, -1);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String date = t.getMessage();
                Log.d(TAG, "throwable: " + t);

                authListener.onAuthenticate(NetworkRequest.STATUS_FAIL, -1);
            }
        });
    }

    public void recover(final String email) {
        final String URL = Keys.CENTRAL + "recoverPassword";

        class doRecovery extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("api_key", Keys.API_KEY)
                            .appendQueryParameter("email", email);
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.d(TAG, "JoinCommunityResponse " + response);
                BroadcastCall.publishRecover(context, response);
            }
        }

        doRecovery doRecovery = new doRecovery();
        doRecovery.execute();
    }

    public void reset(final String email, final String passwordOld, final String passwordNew) {
        final String URL = Keys.ACCOUNTS_API + "register.php";

        class doReset extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("api_key", Keys.API_KEY)
                            .appendQueryParameter("appid", Keys.HGT_APP_ID)
                            .appendQueryParameter("email", email)
                            .appendQueryParameter("password_old", passwordOld)
                            .appendQueryParameter("password_new", passwordNew);
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.d(TAG, "JoinCommunityResponse " + response);

                if (null != response && !response.isEmpty()) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("error")) {
                            String error = jsonObject.getString("error");

                            if (error != null && error.trim().equalsIgnoreCase("false")) {

                                BroadcastCall.publishReset(context, "success");
                            } else {

                                BroadcastCall.publishReset(context, "fail");
                            }
                        } else {
                            BroadcastCall.publishReset(context, null);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "" + e);
                        BroadcastCall.publishReset(context, null);
                    }
                } else {
                    BroadcastCall.publishReset(context, null);
                }
            }
        }

        doReset doReset = new doReset();
        doReset.execute();
    }

    public void fetchClass() {
        final String URL = Keys.BASE_URL + "classes";

        class doFetchClass extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("id", "");
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.d(TAG, "JoinCommunityResponse " + response);

                if (null != response && !response.isEmpty()) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("data")) {
                            databaseHelper = new DatabaseHelper(context);
                            databaseHelper.mashTable(DatabaseHelper.CLASS_TBL);
                            databaseHelper = new DatabaseHelper(context);

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            ServiceClass serviceClass = new ServiceClass();
                            for (int a = 0; a < jsonArray.length(); a++) {
                                jsonObject = jsonArray.getJSONObject(a);
                                serviceClass.setId(jsonObject.getString("class_id"));
                                serviceClass.setName(jsonObject.getString("class_name"));
                                serviceClass.setDescription(jsonObject.getString("class_description"));

                                databaseHelper.addItem(DatabaseHelper.CLASS_TBL, serviceClass);

                            }
                            // sendCountries(MyApplication.countryList);
                            BroadcastCall.publishClassLoad(context, "success");
                        } else {
                            BroadcastCall.publishClassLoad(context, null);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "" + e);
                        BroadcastCall.publishClassLoad(context, null);
                    }
                } else {
                    BroadcastCall.publishClassLoad(context, null);
                }
            }
        }

        doFetchClass doFetchClass = new doFetchClass();
        doFetchClass.execute();
    }

    public void sendTransaction(final JSONObject jsonObject) {
        MyApplication.filesToUploadSize = MyApplication.filesToUpload.size();
        FormFarmMappingResponse responseMapping = null;

        try {
            jsonObject.put("mapping", MainActivityAutomated.getPOIList());
            jsonObject.put("mapping_details", MainActivityAutomated.getMappingDetails());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "jsonObject: " + jsonObject);
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Keys.AGRO_BASE)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

        Call<ResponseBody> call = service.locsinsert(createPartFromString(Keys.APP_ID), jsonObject, Keys.API_KEY);

        final FormFarmMappingResponse finalResponseMapping = responseMapping;
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response != null) {
                    ResponseBody responseBody = response.body();
                    String message = null;
                    try {
                        if (responseBody != null)
                            message = responseBody.string();

                        Log.d(TAG, "response: " + message);

                        if (null != message) {
                            try {
                                JSONObject jsonObject = new JSONObject(message);
                                if (jsonObject != null && jsonObject.has("error")) {
                                    boolean error = jsonObject.optBoolean("error");
                                    if (!error) {
                                        MainActivityAutomated.removeMap(context);
//                                        if (finalResponseMapping != null && finalResponseMapping.headers != null) {
//                                            for (String header : finalResponseMapping.headers) {
//                                                boolean result = databaseHelper.removeScoutListItem(header, ChatbotAdapterRV.MODE_MAP);
//                                                Log.d(TAG, "remove header result result: " + result);
//                                            }
//                                        }

                                        MyApplication.tranx_id_attahcment = jsonObject.optString("tranx_id");
                                        // Log.d(TAG, "tranx_id: " + MyApplication.tranx_id_attahcment);
                                        BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_SUCCESS, null);
                                    } else {
                                        String error_msg = jsonObject.optString("error_msg");
                                        MyApplication.tranx_id_attahcment = null;
                                        BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_FAIL, error_msg);
                                    }
                                } else {
                                    MyApplication.tranx_id_attahcment = null;
                                    BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_FAIL, null);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                MyApplication.tranx_id_attahcment = null;
                                BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_FAIL, null);
                            }
                        } else {
                            MyApplication.tranx_id_attahcment = null;
                            BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_FAIL, null);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                        MyApplication.tranx_id_attahcment = null;
                        BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_FAIL, null);
                    }
                } else {
                    MyApplication.tranx_id_attahcment = null;
                    BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_FAIL, null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_FAIL, null);

            }
        });

    }

    public void sendTransaction2(JSONObject jsonObject) {
//
//        Retrofit.Builder builder=new Retrofit.Builder()
//                .baseUrl(Keys.APPS)
//                .addConverterFactory(GsonConverterFactory.create());
//
//        Retrofit retrofit=builder.build();
////
////        List<MultipartBody.Part> parts = new ArrayList<>();
////
////        if(MyApplication.videoList!=null) {
////            for (int a = 0; a < MyApplication.videoList.size(); a++) {
////                FileField fileField=MyApplication.videoList.get(a);
////            }
////        }
//
//// add the description part within the multipart request
//      //  RequestBody payload = RetrofitHelper.createPartFromString(jsonObject.toString());
//
//// create upload service client
//        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);
//
//// finally, execute the request
//        Call<ResponseBody> call = service.locsinsert(jsonObject);
//
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                Log.d(TAG,"JoinCommunityResponse: "+response);
//                BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_SUCCESS, null);
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Log.d(TAG,"failure: "+call);
//                BroadcastCall.publishTransaction(context, NetworkRequest.STATUS_FAIL, t.getMessage());
//            }
//        });
    }

    public void sendFile(FileField file) {
        Log.d(TAG, "inside sendFile");
        final Handler handler = new Handler();
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Keys.AGRO_BASE)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);
        final Call<ResponseBody> call = service.uploadFile(Keys.API_KEY,
                MyApplication.form_id_attachment == null ? createPartFromString("") : createPartFromString(MyApplication.form_id_attachment),
                file.getRb_question_id(),
                file.getRb_type(), MyApplication.tranx_id_attahcment == null ? createPartFromString("") :
                        createPartFromString(MyApplication.tranx_id_attahcment), file.getFile());


        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "termination check");
                if (call != null && call.isExecuted()) {
                    call.cancel();
                    BroadcastCall.publishFileUpload(context, NetworkRequest.STATUS_FAIL, null);
                }
            }
        };
        //this, after 2 minutes, will check if call is still processing and if so, terminate it
        // handler.postDelayed(runnable,10000);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (runnable != null && handler != null)
                    handler.removeCallbacks(runnable);

                ResponseBody responseBody = response.body();
                String message = null;
                try {
                    if (responseBody != null)
                        message = responseBody.string();

                    Log.d(TAG, "JoinCommunityResponse: " + message + " body: " + responseBody);
                } catch (IOException e) {
                    e.printStackTrace();
                    BroadcastCall.publishFileUpload(context, NetworkRequest.STATUS_FAIL, message);
                }

                if (message == null || message.trim().equalsIgnoreCase("null"))
                    BroadcastCall.publishFileUpload(context, NetworkRequest.STATUS_FAIL, message);
                else
                    BroadcastCall.publishFileUpload(context, NetworkRequest.STATUS_SUCCESS, message);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (runnable != null && handler != null)
                    handler.removeCallbacks(runnable);

                String message = t.getMessage();
                Log.d(TAG, "throwable: " + t);
                BroadcastCall.publishFileUpload(context, NetworkRequest.STATUS_FAIL, message);
            }
        });
    }

    public void fetchCountries() {
        final String URL = Keys.BASE_URL + "countries";

        class doFetchCountry extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("api_key", Keys.API_KEY)
                            .appendQueryParameter("id", "");
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.d(TAG, "JoinCommunityResponse " + response);

                if (null != response && !response.isEmpty()) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("data")) {
//                            databaseHelper=new DatabaseHelper(context);
//                            databaseHelper.mashTable(DatabaseHelper.COUNTRY_TBL);
//                            databaseHelper=new DatabaseHelper(context);

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            Country country = new Country();
                            MyApplication.countryList.clear();
                            MyApplication.sCountryList.clear();
                            MyApplication.sCountryIDList.clear();
                            for (int a = 0; a < jsonArray.length(); a++) {
                                jsonObject = jsonArray.getJSONObject(a);
                                country.setId(jsonObject.getString("country_id"));
                                country.setName(jsonObject.getString("country"));

                                MyApplication.countryList.add(country);
                                MyApplication.sCountryList.add(country.getName());
                                MyApplication.sCountryIDList.add(country.getId());

                                //if(country.getId().trim().equalsIgnoreCase("b21"))
                                //databaseHelper.addMessage(DatabaseHelper.COUNTRY_TBL,country);
                            }
                            // sendCountries(MyApplication.countryList);
                            BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_SUCCESS, "success");
                        } else {
                            BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_FAIL, null);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "" + e);
                        BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_FAIL, null);
                    }
                } else {
                    BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_FAIL, null);
                }
            }
        }

        doFetchCountry doFetchCountry = new doFetchCountry();
        doFetchCountry.execute();
    }

    /**
     * public void fetchCountries(){
     * final String URL= Keys.HGT_API+"countryList";
     * <p>
     * class doFetchCountry extends AsyncTask<String, Void,String> {
     *
     * @Override protected String doInBackground(String... params) {
     * try {
     * URL url = new URL(URL);
     * HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
     * connection.setRequestProperty("User-Agent", "");
     * connection.setRequestMethod("POST");
     * connection.setDoInput(true);
     * <p>
     * Uri.Builder builder  = new Uri.Builder()
     * .appendQueryParameter("id","");
     * String query = builder.build().getEncodedQuery();
     * <p>
     * OutputStream os = connection.getOutputStream();
     * BufferedWriter writer = new BufferedWriter(
     * new OutputStreamWriter(os, "UTF-8"));
     * writer.write(query);
     * writer.flush();
     * writer.close();
     * os.close();
     * connection.connect();
     * <p>
     * InputStream inputStream = connection.getInputStream();
     * InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
     * BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
     * StringBuilder stringBuilder = new StringBuilder();
     * String bufferedStrChunk = null;
     * <p>
     * while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
     * stringBuilder.append(bufferedStrChunk);
     * }
     * return stringBuilder.toString();
     * }catch  (IOException e) {
     * e.printStackTrace();
     * }
     * <p>
     * return null;
     * }
     * @Override protected void onPostExecute(String JoinCommunityResponse) {
     * super.onPostExecute(JoinCommunityResponse);
     * Log.d(TAG,"JoinCommunityResponse "+JoinCommunityResponse);
     * <p>
     * if(null != JoinCommunityResponse && !JoinCommunityResponse.isEmpty()) {
     * <p>
     * try {
     * JSONObject jsonObject=new JSONObject(JoinCommunityResponse);
     * if(jsonObject.has("status")){
     * databaseHelper=new DatabaseHelper(context);
     * databaseHelper.mashTable(DatabaseHelper.COUNTRY_TBL);
     * databaseHelper=new DatabaseHelper(context);
     * <p>
     * JSONArray jsonArray = jsonObject.getJSONArray("data");
     * Country country=new Country();
     * for (int a = 0; a < jsonArray.length(); a++) {
     * jsonObject = jsonArray.getJSONObject(a);
     * country.setId(jsonObject.getInt("country_id"));
     * country.setName(jsonObject.getString("country"));
     * <p>
     * MyApplication.countryList.add(country);
     * MyApplication.sCountryList.add(country.getName());
     * MyApplication.sCountryIDList.add(country.getId());
     * <p>
     * databaseHelper.addMessage(DatabaseHelper.COUNTRY_TBL,country);
     * }
     * sendCountries(MyApplication.countryList);
     * BroadcastCall.publishDistrict(context,"success");
     * }
     * else {
     * BroadcastCall.publishDistrict(context,null);
     * }
     * <p>
     * } catch (JSONException e) {
     * e.printStackTrace();
     * Log.d(TAG,""+e);
     * BroadcastCall.publishDistrict(context,null);
     * }
     * }
     * else {
     * BroadcastCall.publishDistrict(context,null);
     * }
     * }
     * }
     * <p>
     * doFetchCountry doFetchCountry = new doFetchCountry();
     * doFetchCountry.execute();
     * }
     **/

    public void fetchRegions(final String country_id) {
        final String URL = Keys.BASE_URL + "regions/req";

        class doFetchRegion extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);
                    Log.d(TAG, "countryid" + country_id);
                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("api_key", Keys.API_KEY)
                            .appendQueryParameter("countryid", "" + country_id);
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.d(TAG, "JoinCommunityResponse " + response);

                if (null != response && !response.isEmpty()) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("data")) {
//                            databaseHelper=new DatabaseHelper(context);
//                            databaseHelper.mashTable(DatabaseHelper.REGION_TBL);
//                            databaseHelper=new DatabaseHelper(context);

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            Region region = new Region();
                            MyApplication.regionList.clear();
                            MyApplication.sRegionList.clear();
                            MyApplication.sRegionIDList.clear();
                            for (int a = 0; a < jsonArray.length(); a++) {
                                jsonObject = jsonArray.getJSONObject(a);
                                region.setId(jsonObject.getString("region_id"));
                                region.setName(jsonObject.getString("region"));
                                region.setCountry_id(country_id);
                                // region.setCountry_id(country_id);

                                MyApplication.regionList.add(region);
                                MyApplication.sRegionList.add(region.getName());
                                MyApplication.sRegionIDList.add(region.getId());

//                                Log.d(TAG,"country_id "+country_id);
//                              if(country_id.trim().equalsIgnoreCase("b21"))
//                                    databaseHelper.addMessage(DatabaseHelper.REGION_TBL,region);
//
                            }
                            BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_SUCCESS, "success");
                        } else {
                            BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_FAIL, null);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "" + e);
                        BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_FAIL, null);
                    }
                } else {
                    BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_FAIL, null);
                }
            }
        }

        doFetchRegion doFetchRegion = new doFetchRegion();
        doFetchRegion.execute();
    }

    public void fetchDistricts(final String region_id) {
        final String URL = Keys.BASE_URL + "districts/req";

        Log.d(TAG, "load district start");

        class doFetchCountry extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);

                    Log.d(TAG, "region_id " + region_id);
                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("api_key", Keys.API_KEY)
                            .appendQueryParameter("region_id", "" + region_id);
                    //.appendQueryParameter("country_id",""+country_id);
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.d(TAG, "district JoinCommunityResponse " + response);

                if (null != response && !response.isEmpty()) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("data")) {
                            Log.d(TAG, "load district had data");
                            databaseHelper = new DatabaseHelper(context);

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            District district = new District();
                            MyApplication.districtList.clear();
                            MyApplication.sDistrictList.clear();
                            MyApplication.sDistrictIDList.clear();
                            for (int a = 0; a < jsonArray.length(); a++) {
                                jsonObject = jsonArray.getJSONObject(a);
                                district.setId(jsonObject.getString("district_id"));
                                district.setName(jsonObject.getString("district"));

                                MyApplication.districtList.add(district);
                                MyApplication.sDistrictList.add(district.getName());
                                MyApplication.sDistrictIDList.add(district.getId());
                            }
                            BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_SUCCESS, "success");
                        } else {
                            BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_FAIL, null);
                            Log.d(TAG, "load district has no data");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "" + e);
                        BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_FAIL, null);
                    }
                } else {
                    BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_FAIL, null);
                    Log.d(TAG, "load district empty");
                }
            }
        }

        doFetchCountry doFetchCountry = new doFetchCountry();
        doFetchCountry.execute();
    }

    public void fetchTowns(final String district_id) {
        final String TOWN_URL = Keys.BASE_URL + "towns";
        Log.d(TAG, "load town start");
        class doFetchTowns extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(TOWN_URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);

                    Log.d(TAG, "district_id " + district_id);
                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("api_key", Keys.API_KEY)
                            .appendQueryParameter("district_id", "" + district_id);
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }


            @Override
            protected void onPostExecute(final String response) {
                super.onPostExecute(response);
                Log.d(TAG, "district id " + district_id);
                Log.d(TAG, "town JoinCommunityResponse " + response);

                if (null != response && !response.isEmpty()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("data")) {
                            databaseHelper = new DatabaseHelper(context);
                            databaseHelper.createTable(DatabaseHelper.TOWN_TBL, null);
                            databaseHelper.mashTable(DatabaseHelper.TOWN_TBL);
                            databaseHelper = new DatabaseHelper(context);
                            databaseHelper.createTable(DatabaseHelper.TOWN_TBL, null);

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            ArrayList<Town> townList = new ArrayList<>();
                            Town town;
                            MyApplication.sTownList.clear();
                            MyApplication.sTownIDList.clear();
                            for (int a = 0; a < jsonArray.length(); a++) {
                                town = new Town();
                                jsonObject = jsonArray.getJSONObject(a);
                                town.setId(jsonObject.getString("town_id"));
                                town.setName(jsonObject.getString("town"));

                                townList.add(town);

                                MyApplication.sTownList.add(town.getName());
                                MyApplication.sTownIDList.add(town.getId());
                                //databaseHelper.addMessage(DatabaseHelper.TOWN_TBL,town);
                            }
                            boolean dbInsert = databaseHelper.addItem(DatabaseHelper.TOWN_TBL, townList);
                            if (dbInsert)
                                BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_SUCCESS, "success");
                            else
                                BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_FAIL, null);

                            Log.d(TAG, "load town has data: " + dbInsert);
                        } else {
                            BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_FAIL, null);
                            Log.d(TAG, "load town has no data");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "" + e);
                        BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_FAIL, null);
                    }
                } else {
                    BroadcastCall.publishDistrict(context, NetworkRequest.STATUS_FAIL, null);
                }
            }
        }

        doFetchTowns doFetchTowns = new doFetchTowns();
        doFetchTowns.execute();
    }

    public void fetchNeighbourhood(final String town_id) {
        final String TOWN_URL = Keys.BASE_URL + "neighbourhood";

        class doFetchNeigh extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(TOWN_URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("api_key", Keys.API_KEY)
                            .appendQueryParameter("town_id", "" + town_id);
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }


            @Override
            protected void onPostExecute(final String response) {
                super.onPostExecute(response);
                Log.d(TAG, "town id " + town_id);
                Log.d(TAG, "JoinCommunityResponse " + response);

                if (null != response && !response.isEmpty()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("data")) {
//                            databaseHelper=new DatabaseHelper(context);
//                            databaseHelper.mashTable(DatabaseHelper.NEIGHBOURHOOD_TBL);
//                            databaseHelper=new DatabaseHelper(context);

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            ArrayList<Neighbourhood> neighbourhoodList = new ArrayList<>();
                            Neighbourhood neighbourhood;
                            for (int a = 0; a < jsonArray.length(); a++) {
                                neighbourhood = new Neighbourhood();
                                jsonObject = jsonArray.getJSONObject(a);
                                neighbourhood.setTown_id(jsonObject.getString("town_id"));
                                neighbourhood.setName(jsonObject.getString("neighbourhood"));
                                neighbourhood.setId(jsonObject.getString("neighbourhood_id"));

                                neighbourhoodList.add(neighbourhood);
                                // databaseHelper.addMessage(DatabaseHelper.TOWN_TBL,town);
                            }
//                            boolean dbInsert=databaseHelper.addMessage(DatabaseHelper.NEIGHBOURHOOD_TBL,neighbourhoodList);
//                            if(dbInsert)
                            BroadcastCall.publishNeighbourhood(context, NetworkRequest.STATUS_SUCCESS, null);
                            // else
                            //     BroadcastCall.publishNeighbourhood(context,NetworkRequest.STATUS_FAIL,null);
                        } else {
                            BroadcastCall.publishNeighbourhood(context, NetworkRequest.STATUS_FAIL, null);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "" + e);
                        BroadcastCall.publishNeighbourhood(context, NetworkRequest.STATUS_FAIL, null);
                    }
                } else {
                    BroadcastCall.publishNeighbourhood(context, NetworkRequest.STATUS_FAIL, null);
                }
            }
        }

        doFetchNeigh doFetchNeigh = new doFetchNeigh();
        doFetchNeigh.execute();
    }

    public void sendLocsCast(final String longitude, final String latitude, final String accuracy) {
        final String LOCSCAST_URL = "http://hgtapi.silapha.com/ehandle/locscast";
        final User user = new User(context);
        class doLocsCast extends AsyncTask<String, Void, String> {


            @Override
            protected String doInBackground(String... params) {

                try {
                    URL url = new URL(LOCSCAST_URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);
                    //// TODO: 8/17/2017 remember to change the product code
                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("api_key", Keys.API_KEY)
                            .appendQueryParameter("product_code", Keys.HGT_APP_ID)
                            .appendQueryParameter("product_name", Keys.APP_NAME)
                            .appendQueryParameter("user_id", user.getHgt_user_id())
                            .appendQueryParameter("lon", "" + longitude)
                            .appendQueryParameter("lat", "" + latitude)
                            .appendQueryParameter("accuracy_m", "" + accuracy);

                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    //connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                //  Log.d(TAG,"JoinCommunityResponse "+JoinCommunityResponse);
            }
        }

        doLocsCast doLocsCast = new doLocsCast();
        doLocsCast.execute();
    }

    public void fetchForms(final String intentAction) {
        final String FORMS_URL = Keys.AGRO_BASE + "postFormsApi";

        class doFetchForms extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(FORMS_URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("api_key", Keys.API_KEY)
                            .appendQueryParameter("userid", (new User(context)).getId());
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }


            @Override
            protected void onPostExecute(final String response) {
                super.onPostExecute(response);
                Log.d(TAG, "fetchForms JoinCommunityResponse " + response);

                if (null != response && !response.isEmpty()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject jsonLocsData = jsonObject.optJSONObject("LocsData");
                        if (jsonLocsData != null)
                            databaseHelper.setup(jsonLocsData);

                        if (intentAction != null) {
                            if (intentAction.equalsIgnoreCase(BroadcastCall.ORGLIST)) {
                                BroadcastCall.publishOrgUpdate(context, NetworkRequest.STATUS_SUCCESS);
                            } else if (intentAction.equalsIgnoreCase(BroadcastCall.FORM_LIST)) {
                                BroadcastCall.publishFormUpdate(context, NetworkRequest.STATUS_SUCCESS);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "" + e);

                        if (intentAction != null) {
                            if (intentAction.equalsIgnoreCase(BroadcastCall.FORM_LIST))
                                BroadcastCall.publishFormUpdate(context, NetworkRequest.STATUS_FAIL);
                            else if (intentAction.equalsIgnoreCase(BroadcastCall.ORGLIST))
                                BroadcastCall.publishOrgUpdate(context, NetworkRequest.STATUS_SUCCESS);
                        }
                    }
                } else {
                    if (intentAction != null) {
                        if (intentAction.equalsIgnoreCase(BroadcastCall.FORM_LIST))
                            BroadcastCall.publishFormUpdate(context, NetworkRequest.STATUS_FAIL);
                        else if (intentAction.equalsIgnoreCase(BroadcastCall.ORGLIST))
                            BroadcastCall.publishOrgUpdate(context, NetworkRequest.STATUS_SUCCESS);
                    }
                }
            }
        }

        doFetchForms doFetchForms = new doFetchForms();
        doFetchForms.execute();
    }

    public void fetchForm(final String form_code, final boolean autoLoad) {
        // final String FORMS_URL= Keys.APPS+"postFormsCode";

        Log.d(TAG, "fetch by code, autoLoad: " + autoLoad);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Keys.FORMS_AGRO)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

          Call<ResponseBody> call = service.fetchFormByCode(createPartFromString(new User(context).getId()), createPartFromString(form_code), Keys.API_KEY);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                ResponseBody responseBody = response.body();
                String message = null;
                try {
                    if (responseBody != null)
                        message = responseBody.string();

                      Log.v(TAG, "JoinCommunityResponse: " + message + " body: " + responseBody
                            + " code: " + response.code() + " error: " + response.errorBody());

                    if (null != message && !message.isEmpty()) {
                        try {
                            JSONObject jsonObject = new JSONObject(message);
                            JSONObject jsonLocsData = jsonObject.optJSONObject("LocsData");

                              Log.e(TAG, "jsonLocsData: " + jsonLocsData);

                            if (jsonLocsData != null) {
                                databaseHelper.setup(jsonLocsData);
                                JSONArray orgArray = jsonLocsData.optJSONArray(DatabaseHelper.TBL_ORGANIZATION);

                                  Log.e(TAG, "orgArray: " + orgArray);

                                if (orgArray != null && orgArray.length() > 0) {
                                    JSONObject organizationsJSON = orgArray.getJSONObject(0);
                                    JSONObject orgDetails = organizationsJSON.optJSONObject(DatabaseHelper.DETAILS);
                                    String org_id = orgDetails.optString(DatabaseHelper.ITEM_ID);

                                    Log.e(TAG, "onResponse: Org_ID: " + org_id );

                                    JSONArray formArray = organizationsJSON.optJSONArray(DatabaseHelper.TBL_FORMS);
                                    if (formArray != null && formArray.length() > 0) {
                                        JSONObject formObject = formArray.optJSONObject(0);
                                        JSONObject formDetails = formObject.optJSONObject(DatabaseHelper.DETAILS);
                                        String form_id = formDetails.optString(DatabaseHelper.ITEM_ID);

                                          Log.e(TAG, "form_id: " + form_id + " org_id: " + org_id);

                                        if (form_id != null && !form_id.trim().isEmpty()
                                                && org_id != null && !org_id.trim().isEmpty()) {
                                            if (autoLoad) {
                                                (new SettingsModel(context)).setForm_id(form_id);
                                                (new SettingsModel(context)).setOrg_id(org_id);
                                                (new SettingsModel(context)).setTransaction_id(null);
                                                (CreateForm.getInstance(context)).setShouldRefreshForm(true);
                                            }

                                            Log.e(TAG, "settings form id: " + (new SettingsModel(context)).getForm_id());

                                            Form form = databaseHelper.getForm(form_id);
                                              Log.e(TAG, "form: " + form);
                                            if (form != null && form.getId() != null) {
                                                form.setAccess_code(form_code);
                                                  Log.e(TAG, "form_access_code: " + form.getAccess_code());
                                                databaseHelper.updateFormCode(form);
                                            }
//                                    Log.d(TAG,"form: "+form);
                                            BroadcastCall.publishFormUpdate(context, NetworkRequest.STATUS_SUCCESS);
                                        } else
                                            BroadcastCall.publishFormUpdate(context, NetworkRequest.STATUS_FAIL);
                                    } else
                                        BroadcastCall.publishFormUpdate(context, NetworkRequest.STATUS_FAIL);
                                } else
                                    BroadcastCall.publishFormUpdate(context, NetworkRequest.STATUS_FAIL);
                            } else
                                BroadcastCall.publishFormUpdate(context, NetworkRequest.STATUS_FAIL);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG, "" + e);
                            BroadcastCall.publishFormUpdate(context, NetworkRequest.STATUS_FAIL);
                        }
                    } else {
                        BroadcastCall.publishFormUpdate(context, NetworkRequest.STATUS_FAIL);
                    }

                    call.cancel();

                } catch (IOException e) {
                    e.printStackTrace();
                    call.cancel();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String message = t.getMessage();
                Log.d(TAG, "throwable: " + t.getMessage() + " class " + t.getClass() + " cause " + t.getCause());
                call.cancel();
                BroadcastCall.publishFormUpdate(context, NetworkRequest.STATUS_FAIL);
            }
        });
    }

    public void sendFeedback(final String feedback) {
        final String FORMS_URL = Keys.OLDHITAL_BASE + "feedback";

        class doSendfeedback extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(FORMS_URL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);

                    String user = SharePrefManager.getInstance(context).getUserDetails().get(0);

                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("api_key", Keys.API_KEY)
                            .appendQueryParameter("userid", (new User(context)).getId())
                            .appendQueryParameter("feedback", feedback)
                            .appendQueryParameter("form_id", (new SettingsModel(context)).getForm_id())
                            .appendQueryParameter("email", user);
                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }


            @Override
            protected void onPostExecute(final String response) {
                super.onPostExecute(response);
                Log.d(TAG, "JoinCommunityResponse " + response);
                if (response != null)
                    BroadcastCall.publishFeedback(context, Integer.parseInt(response));
                else
                    BroadcastCall.publishFeedback(context, STATUS_FAIL);
            }
        }

        doSendfeedback doSendfeedback = new doSendfeedback();
        doSendfeedback.execute();
    }

//    public void sendFAWMappingBulk(final JSONArray jsonArray, final String mapping_id, String type, final NetworkResponse netResponse, String mapping_name, double size) {
//        final User user = new User(context);
//
//        String u_email = SharePrefManager.getInstance(context).getUserDetails().get(0);
//        String ext_code = SharePrefManager.getInstance(context).getOrganisationDetails().get(0);
//          String label = SharePrefManager.getInstance(context).getFormMapping().get(0);
//          String comment = SharePrefManager.getInstance(context).getFormMapping().get(0);
//        Log.d(TAG, "mapping gps_payload: " + jsonArray);
//
//        Log.d(TAG, "mapping_id: " + mapping_id);
//        Log.d(TAG, "user_id: " + u_email);
//        Log.d(TAG, "type: " + type);
//        Log.d(TAG, "size: " + size);
//
//        Retrofit.Builder builder = new Retrofit.Builder()
////                .baseUrl(Keys.AGRO_BASE)
//                .baseUrl(Keys.FAW_API)
//                .addConverterFactory(GsonConverterFactory.create());
//
//        Retrofit retrofit = builder.build();
//
//        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);
//
//        Call<ResponseBody> call = service.sendFAWMappingBulk(
//                createPartFromString(type == null ? "line" : type),
//                createPartFromString(Keys.APP_ID),
//                createPartFromString(mapping_id),
//                createPartFromString(ext_code),
//                createPartFromString("" + u_email),
//                createPartFromString("" + mapping_name),
//                createPartFromString("" + comment),
//                size,
//                jsonArray,
//                Keys.API_KEY);
//
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//
//                ResponseBody responseBody = response.body();
//                String message = null;
//                try {
//                    if (responseBody != null)
//                        message = responseBody.string();
//
//                    Log.e(TAG, "JoinCommunityResponse: " + message + " body: " + responseBody
//                            + " code: " + response.code() + " error: " + response.errorBody());
//
//                    call.cancel();
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    call.cancel();
//                }
//
//                netResponse.onRespond(message);
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                String message = t.getMessage();
//                Log.d(TAG, "throwable: " + t.getMessage() + " class " + t.getClass() + " cause " + t.getCause());
//                Log.e(TAG, "throwable: " + t.getMessage() + " class " + t.getClass() + " cause " + t.getCause());
//                call.cancel();
//                netResponse.onRespond("error");
//            }
//        });
//
////        class doSendFAWMappingBulk extends AsyncTask<String, Void,String> {
////
////            @Override
////            protected String doInBackground(String... params) {
////
////                try {
////                    URL url = new URL(Keys.FAW_API+"process-mapping");
////                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
////                    connection.setRequestProperty("User-Agent", "");
////                    connection.setRequestMethod("POST");
////                    connection.setDoInput(true);
////                    connection.setConnectTimeout(10000);
////                    connection.setReadTimeout(10000);
////
////                    //// TODO: 8/17/2017 remember to change the product code
////                    Uri.Builder builder  = new Uri.Builder()
////                            .appendQueryParameter("mapping_id",""+mapping_id)
////                            .appendQueryParameter("user_id", (new User(context)).getId())
////                            .appendQueryParameter("gps_payload", ""+jsonArray);
////
////                    String query = builder.build().getEncodedQuery();
////
////                    OutputStream os = connection.getOutputStream();
////                    BufferedWriter writer = new BufferedWriter(
////                            new OutputStreamWriter(os, "UTF-8"));
////                    writer.write(query);
////                    writer.flush();
////                    writer.close();
////                    os.close();
////                    connection.connect();
////
////                    InputStream inputStream = connection.getInputStream();
////                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
////                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
////                    StringBuilder stringBuilder = new StringBuilder();
////                    String bufferedStrChunk = null;
////
////                    Log.d(TAG,"connection JoinCommunityResponse: "+connection.getResponseCode());
////
////                    switch (connection.getResponseCode()) {
////                        case HttpURLConnection.HTTP_OK:
////                            Log.d(TAG," **OK**");
////                            //connected = true;
////                            break; // fine, go on
////                        case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
////                            Log.d(TAG," **TIMEOUT**");
////                            onPostExecute(null);
////                            break;// retry
////                        case HttpURLConnection.HTTP_UNAVAILABLE:
////                            Log.d(TAG," **UNAVAILABLE**");
////                            onPostExecute(null);
////                            break;// retry, server is unstable
////                        default:
////                            Log.d(TAG," **NO CODE**");
////                            onPostExecute(null);
////                            break; // abort
////                    }
////
////                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
////                        stringBuilder.append(bufferedStrChunk);
////                    }
////                    return stringBuilder.toString();
////                }
////                catch (java.net.SocketTimeoutException e){
////                    Log.d(TAG,"timeout");
////                    onPostExecute("timeout");
////                }
////                catch  (IOException e) {
////                    e.printStackTrace();
////                    Log.d(TAG,"connection error");
////                    onPostExecute("error");
////                }
////
////                return null;
////            }
////
////            @Override
////            protected void onPostExecute(String JoinCommunityResponse) {
////                super.onPostExecute(JoinCommunityResponse);
////                Log.d(TAG,"JoinCommunityResponse: "+JoinCommunityResponse);
////                netResponse.onRespond(JoinCommunityResponse);
////            }
////        }
////
////        doSendFAWMappingBulk doSendFAWMappingBulk = new doSendFAWMappingBulk();
////        doSendFAWMappingBulk.execute();
//    }

    public void sendFAWMappingBulk(final JSONArray jsonArray, final String mapping_id, String type, final NetworkResponse netResponse){
        final User user=new User(context);
        Log.d(TAG,"mapping gps_payload: "+jsonArray);
        Log.d(TAG,"mapping_id: "+mapping_id);
        Log.d(TAG,"user_id: "+ (new User(context)).getId());
        Log.d(TAG,"type: "+type);

        Retrofit.Builder builder=new Retrofit.Builder()
                .baseUrl(Keys.FAW_API)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit=builder.build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

        Call<ResponseBody> call= service.sendFAWMappingBulk(
                createPartFromString(type==null? "line":type),
                createPartFromString(Keys.APP_ID),
                createPartFromString(mapping_id),
                createPartFromString(""+user.getId()),
                jsonArray,Keys.API_KEY);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                ResponseBody responseBody=response.body();
                String message=null;
                try {
                    if(responseBody!=null)
                        message=responseBody.string();

                    Log.d(TAG,"JoinCommunityResponse: "+message+" body: "+responseBody
                            +" code: "+response.code()+" error: "+response.errorBody());

                    call.cancel();

                } catch (IOException e) {
                    e.printStackTrace();
                    call.cancel();
                }

                netResponse.onRespond(message);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String message=t.getMessage();
                Log.d(TAG,"throwable: "+t.getMessage()+" class "+t.getClass()+" cause "+t.getCause());
                call.cancel();
                netResponse.onRespond("error");
            }
        });

//        class doSendFAWMappingBulk extends AsyncTask<String, Void,String> {
//
//            @Override
//            protected String doInBackground(String... params) {
//
//                try {
//                    URL url = new URL(Keys.FAW_API+"process-mapping");
//                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
//                    connection.setRequestProperty("User-Agent", "");
//                    connection.setRequestMethod("POST");
//                    connection.setDoInput(true);
//                    connection.setConnectTimeout(10000);
//                    connection.setReadTimeout(10000);
//
//                    //// TODO: 8/17/2017 remember to change the product code
//                    Uri.Builder builder  = new Uri.Builder()
//                            .appendQueryParameter("mapping_id",""+mapping_id)
//                            .appendQueryParameter("user_id", (new User(context)).getId())
//                            .appendQueryParameter("gps_payload", ""+jsonArray);
//
//                    String query = builder.build().getEncodedQuery();
//
//                    OutputStream os = connection.getOutputStream();
//                    BufferedWriter writer = new BufferedWriter(
//                            new OutputStreamWriter(os, "UTF-8"));
//                    writer.write(query);
//                    writer.flush();
//                    writer.close();
//                    os.close();
//                    connection.connect();
//
//                    InputStream inputStream = connection.getInputStream();
//                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String bufferedStrChunk = null;
//
//                    Log.d(TAG,"connection JoinCommunityResponse: "+connection.getResponseCode());
//
//                    switch (connection.getResponseCode()) {
//                        case HttpURLConnection.HTTP_OK:
//                            Log.d(TAG," **OK**");
//                            //connected = true;
//                            break; // fine, go on
//                        case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
//                            Log.d(TAG," **TIMEOUT**");
//                            onPostExecute(null);
//                            break;// retry
//                        case HttpURLConnection.HTTP_UNAVAILABLE:
//                            Log.d(TAG," **UNAVAILABLE**");
//                            onPostExecute(null);
//                            break;// retry, server is unstable
//                        default:
//                            Log.d(TAG," **NO CODE**");
//                            onPostExecute(null);
//                            break; // abort
//                    }
//
//                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(bufferedStrChunk);
//                    }
//                    return stringBuilder.toString();
//                }
//                catch (java.net.SocketTimeoutException e){
//                    Log.d(TAG,"timeout");
//                    onPostExecute("timeout");
//                }
//                catch  (IOException e) {
//                    e.printStackTrace();
//                    Log.d(TAG,"connection error");
//                    onPostExecute("error");
//                }
//
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String JoinCommunityResponse) {
//                super.onPostExecute(JoinCommunityResponse);
//                Log.d(TAG,"JoinCommunityResponse: "+JoinCommunityResponse);
//                netResponse.onRespond(JoinCommunityResponse);
//            }
//        }
//
//        doSendFAWMappingBulk doSendFAWMappingBulk = new doSendFAWMappingBulk();
//        doSendFAWMappingBulk.execute();
    }

    public void sendFAWMapping(final POI poi, final String mapping_id, final NetworkResponse netResponse) {
        final User user = new User(context);
        class doSendFAWMapping extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                try {
                    URL url = new URL(Keys.AGRO_BASE + "process-mapping");
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);
                    Log.d(TAG, "mapping_id: " + mapping_id
                            + " long: " + poi.getLon()
                            + " lat: " + poi.getLat() + "" +
                            " accur: " + poi.getAccuracy()
                            + " user_id: " + (new User(context)).getId());
                    //// TODO: 8/17/2017 remember to change the product code
                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("app_id", Keys.APP_ID)
                            .appendQueryParameter("api_key", Keys.API_KEY)
                            .appendQueryParameter("mapping_id", "" + mapping_id)
                            .appendQueryParameter("user_id", (new User(context)).getId())
                            .appendQueryParameter("longitude", poi.getLon())
                            .appendQueryParameter("latitude", poi.getLat())
                            .appendQueryParameter("accuracy", poi.getAccuracy());

                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, StandardCharsets.UTF_8));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                Log.e(TAG, "JoinCommunityResponse: " + response);
                netResponse.onRespond(response);
            }
        }

        doSendFAWMapping doSendFAWMapping = new doSendFAWMapping();
        doSendFAWMapping.execute();
    }

    public void fetchKnowledgeBase(final NetworkResponse netResponse) {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Keys.FAW_API)
                //.baseUrl(Keys.AGRO_BASE)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

        String language = (new SettingsModel(context)).getLang();
        Log.d(TAG, "fetch kb: language " + language);
        if (language != null) {
            if (language.equalsIgnoreCase(context.getResources().getString(R.string.swahili)))
                language = "english";
            else if (language.equalsIgnoreCase(context.getResources().getString(R.string.french)))
                language = "french";
            else if (language.equalsIgnoreCase("french"))
                language = "french";
            else
                language = "english";
        } else language = "english";

        Log.d(TAG, "fetch kb: lang " + language);
        Call<ResponseBody> call = service.fetchKB(language, "app", Keys.API_KEY);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> JoinCommunityResponse) {

                ResponseBody responseBody = JoinCommunityResponse.body();
                String date = null;
                try {
                    if (responseBody != null)
                        date = responseBody.string();

                    Log.e(TAG, "response: " + date + " body: " + responseBody
                            + " code: " + JoinCommunityResponse.code() + " error: " + JoinCommunityResponse.errorBody());

                    //  call.cancel();

                } catch (IOException e) {
                    e.printStackTrace();
                    netResponse.onRespond("error");
                }

                netResponse.onRespond(date);
                call.cancel();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String date = t.getMessage();
                netResponse.onRespond("error");
                call.cancel();
            }
        });
/*        class doKB extends AsyncTask<String, Void,String> {

            @Override
            protected String doInBackground(String... params) {

                try {
                    String platform="app";
                    Log.d(TAG,"lang: "+(new SettingsModel(context)).getLang()+" platform: "+platform);
                    String path=Keys.FAW_API
                            +"kb?lang="+(new SettingsModel(context)).getLang()+""
                            +"&platform="+platform+"";
                    Log.d(TAG,"kb path: "+path);
                    URL url = new URL(path);
                    HttpURLConnection connection  = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);
                    //// TODO: 8/17/2017 remember to change the product code
                    Uri.Builder builder  = new Uri.Builder()
                            .appendQueryParameter("user_id",""+(new User(context)).getHgt_user_id());

                    String query = builder.build().getEncodedQuery();

                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    return stringBuilder.toString();
                }catch  (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                netResponse.onRespond(response);
            }
        }

        doKB doKB = new doKB();
        doKB.execute();*/
    }

    public void sendMessage(Message message, final Message.MessageListener listener) {
        Log.d(TAG, "inside sendMessage: " + message.getText());
        MultipartBody.Part file = null;
        if (message.getImageUrl() != null)
            file = RetrofitHelper.prepareFilePart(context, "image", message.getImageUrl(), true);

        Log.d(TAG, "file: " + file);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Keys.MESSAGING)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        Log.d(TAG, "room_id: " + (new User(context)).getChatroomActive_id());
        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);
        final Call<ResponseBody> call = service.sendMessage(
                createPartFromString(Keys.APP_ID),
                createPartFromString("" + (new User(context)).getChatroomActive_id()),
                createPartFromString("" + message.getText()),
                createPartFromString(file == null ? "false" : "true"),
                createPartFromString((new User(context)).getId()),
                file, Keys.API_KEY);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                ResponseBody responseBody = response.body();
                String message = null;
                try {
                    if (responseBody != null)
                        message = responseBody.string();

                    Log.d(TAG, "sendMessage: " + message + " body: " + responseBody);

                } catch (IOException e) {
                    e.printStackTrace();
                    listener.status(null);
                }

                if (listener != null)
                    listener.status(message);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String message = t.getMessage();
                Log.d(TAG, "throwable: " + t);
                if (listener != null)
                    listener.status(null);
            }
        });
    }

    public void joinCommunity(User user, final JoinCommunityListener joinCommunityListener) {
        Log.d(TAG, "inside joinCommunity");

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Keys.MESSAGING)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

        String email = user.getEmail() == null ? "" : user.getEmail();
        String fullname = Chat.getAuthor(context).getName();
        String push_token = user.getPush_id() == null ? "" : user.getPush_id();

        Log.d(TAG, "JOINCOmmunity pushID: " + push_token);

        JSONArray rooms = new JSONArray();
        ArrayList<String> roomArray = user.getChatRoom_ids();

        for (String room : roomArray) {
            rooms.put(room);
        }
        Log.d(TAG, "rooms: " + rooms + " roomArraysize: " + roomArray.size());
        final Call<ResponseBody> call = service.joinCommunity(createPartFromString(email),
                createPartFromString(push_token),
                createPartFromString(fullname),
                rooms, Keys.API_KEY);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                ResponseBody responseBody = response.body();
                String data = null;
                try {
                    if (responseBody != null)
                        data = responseBody.string();

                    Log.d(TAG, "JoinCommunityResponse: " + data + " body: " + responseBody);
                } catch (IOException e) {
                    e.printStackTrace();
                    joinCommunityListener.JoinCommunityResponse(data, NetworkRequest.STATUS_FAIL);
                }

                if (data == null || data.trim().equalsIgnoreCase("null"))
                    joinCommunityListener.JoinCommunityResponse(null, NetworkRequest.STATUS_FAIL);
                else
                    joinCommunityListener.JoinCommunityResponse(data, NetworkRequest.STATUS_SUCCESS);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String date = t.getMessage();
                Log.d(TAG, "throwable: " + t);
                joinCommunityListener.JoinCommunityResponse(null, NetworkRequest.STATUS_FAIL);
            }
        });
    }

    public void getProducts(final String category, final ProductsListener listener) {
        Log.d(TAG, "getProducts");
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Keys.HITAL_BASE)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

        final Call<ResponseBody> call = service.getProducts(
                createPartFromString(Keys.APP_ID),
                createPartFromString("" + category), Keys.API_KEY);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                List<Product> productList = new ArrayList<>();
                if (response != null) {
                    ResponseBody responseBody = response.body();
                    String data = null;
                    try {
                        if (responseBody != null)
                            data = responseBody.string();

                        Log.d(TAG, "data: " + data);
                        if (null != data && !data.isEmpty()) {
                            try {
                                JSONObject jsonObject = new JSONObject(data);
                                JSONArray jsonArray = jsonObject.optJSONArray("items");
                                if (jsonArray != null) {
                                    for (int a = 0; a < jsonArray.length(); a++) {
                                        JSONObject object = jsonArray.getJSONObject(a);
                                        Product product = new Product();
                                        product.name = object.optString("Name");
                                        product.price = object.optLong("Price");
                                        product.description = object.optString("Description");
                                        product.image = object.optString("Image");
                                        product.id = object.optString("Key");
                                        product.max = object.optInt("Max");
                                        product.owner_name = object.optString("Org");
                                        product.category = category;


                                        productList.add(product);
                                    }
                                }
                                if (listener != null)
                                    listener.loaded(NetworkRequest.STATUS_SUCCESS, productList);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.d(TAG, "" + e);
                                if (listener != null)
                                    listener.loaded(NetworkRequest.STATUS_FAIL, productList);
                            }
                        } else {
                            if (listener != null)
                                listener.loaded(NetworkRequest.STATUS_FAIL, productList);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        if (listener != null)
                            listener.loaded(NetworkRequest.STATUS_FAIL, productList);
                    }
                } else {
                    if (listener != null) listener.loaded(NetworkRequest.STATUS_FAIL, null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String date = t.getMessage();
                Log.d(TAG, "throwable: " + t);
                if (listener != null) listener.loaded(NetworkRequest.STATUS_FAIL, null);
            }
        });

    }

    public void makePayment(final Pay pay, final PaymentListener listener) {
        Log.d(TAG, "makePayment");
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Keys.HITAL_BASE)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        LocsmmanProAPI service = retrofit.create(LocsmmanProAPI.class);

        final Call<ResponseBody> call = service.payment(
                pay.products,
                createPartFromString("" + pay.user.getId()),
                createPartFromString("" + pay.amountDue),
                createPartFromString("" + pay.cardNumber),
                createPartFromString("" + pay.cardName),
                createPartFromString("" + pay.cvv),
                createPartFromString("" + pay.yearExpiration),
                createPartFromString("" + pay.monthExpiration),
                createPartFromString("" + pay.cardType),
                Keys.API_KEY);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ResponseBody responseBody = response.body();
                String data = null;
                try {
                    if (responseBody != null)
                        data = responseBody.string();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (listener != null) listener.onRespond(NetworkRequest.STATUS_SUCCESS, data);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String date = t.getMessage();
                Log.d(TAG, "throwable: " + t);
                if (listener != null) listener.onRespond(NetworkRequest.STATUS_FAIL, null);
            }
        });
    }

    public class RegisterForPush extends AsyncTask<Void, Void, Exception> {
        protected Exception doInBackground(Void... params) {
            try {
                // Assign a unique token to this device
                String deviceToken = Pushy.register(context);

                // Log it for debugging purposes
                Log.e("MyApp", "Pushy device token: " + deviceToken);
                SharePrefManager.getInstance(context).savePushToken(deviceToken);

            } catch (Exception exc) {
                // Return exc to onPostExecute
                return exc;
            }

            // Success
            return null;
        }

        @Override
        protected void onPostExecute(Exception exc) {
            // Failed?
            if (exc != null) {
                // Show error as toast message
                Toast.makeText(context, exc.toString(), Toast.LENGTH_LONG).show();
            }

            // Succeeded, optionally do something to alert the user
            Log.e("Push:", "Push is: " + SharePrefManager.getInstance(context).getUserDetails().get(3));
        }
    }

    public static Retrofit getRetrofit(String url){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(provideOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

    private static OkHttpClient provideOkHttpClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder okhttpClientBuilder = new OkHttpClient.Builder();
        okhttpClientBuilder.connectTimeout(30, TimeUnit.SECONDS);
        okhttpClientBuilder.readTimeout(30, TimeUnit.SECONDS);
        okhttpClientBuilder.writeTimeout(30, TimeUnit.SECONDS);
        okhttpClientBuilder.addInterceptor(interceptor);

//        if (token != null){
//            okhttpClientBuilder.addInterceptor(tokenHeaderIntercept(token));
//        }

//        okhttpClientBuilder.addInterceptor(new TokenRenewInterceptor(getSession()));
//        okhttpClientBuilder.addInterceptor(new AuthorizationInterceptor(getApiInterface(), getSession()));
        return okhttpClientBuilder.build();
    }
}