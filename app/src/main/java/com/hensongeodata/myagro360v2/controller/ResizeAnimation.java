package com.hensongeodata.myagro360v2.controller;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by user1 on 6/19/2018.
 */

public class ResizeAnimation extends Animation {
    final int targetWidth;
    View view;
    int startWidth;

    public ResizeAnimation(View view, int targetWidth, int startWidth) {
        this.view = view;
        this.targetWidth = targetWidth;
        this.startWidth = startWidth;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        //int newWidth = (int) (startWidth + targetWidth * interpolatedTime);
        //to support decent animation, change new heigt as Nico S. recommended in comments
        int newWidth = (int) (startWidth+(targetWidth - startWidth) * interpolatedTime);
        view.getLayoutParams().width = newWidth;
        view.requestLayout();
    }

    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}
