package com.hensongeodata.myagro360v2.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.adapter.ChatbotAdapterRV;
import com.hensongeodata.myagro360v2.model.Answer;
import com.hensongeodata.myagro360v2.model.Archive;
import com.hensongeodata.myagro360v2.model.Author;
import com.hensongeodata.myagro360v2.model.CheckboxField;
import com.hensongeodata.myagro360v2.model.Country;
import com.hensongeodata.myagro360v2.model.DateField;
import com.hensongeodata.myagro360v2.model.District;
import com.hensongeodata.myagro360v2.model.DropdownField;
import com.hensongeodata.myagro360v2.model.EmailField;
import com.hensongeodata.myagro360v2.model.Field;
import com.hensongeodata.myagro360v2.model.FileField;
import com.hensongeodata.myagro360v2.model.Form;
import com.hensongeodata.myagro360v2.model.FormFarmMappingResponse;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.KnowledgeBase;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.model.Message;
import com.hensongeodata.myagro360v2.model.Neighbourhood;
import com.hensongeodata.myagro360v2.model.NumericField;
import com.hensongeodata.myagro360v2.model.Organization;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.model.POIGroup;
import com.hensongeodata.myagro360v2.model.PaymentReceipt;
import com.hensongeodata.myagro360v2.model.Product;
import com.hensongeodata.myagro360v2.model.Question;
import com.hensongeodata.myagro360v2.model.RadioField;
import com.hensongeodata.myagro360v2.model.Region;
import com.hensongeodata.myagro360v2.model.Section;
import com.hensongeodata.myagro360v2.model.ServiceClass;
import com.hensongeodata.myagro360v2.model.SettingsModel;
import com.hensongeodata.myagro360v2.model.TextAreaField;
import com.hensongeodata.myagro360v2.model.TextField;
import com.hensongeodata.myagro360v2.model.Town;
import com.hensongeodata.myagro360v2.model.TownField;
import com.hensongeodata.myagro360v2.model.Transaction;
import com.hensongeodata.myagro360v2.view.Chat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by user1 on 3/7/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper{
    private static String TAG=DatabaseHelper.class.getSimpleName();
    Context mContext;
    private static final int DATABASE_VERSION = 1;
    private static String DB_NAME="locsmman_faw"+ Keys.APP_NAME;
    private static String ACTION_INSERT="insert";
    private static String ACTION_UPDATE="update";
    public static String CLASS_TBL="tbl_class";
    public static String COUNTRY_TBL ="tbl_country";
    public static String REGION_TBL ="tbl_region";
    public static String DISTRICT_TBL ="tbl_district";
    public static String TOWN_TBL ="tbl_town";
    public static String NEIGHBOURHOOD_TBL ="tbl_neighbourhood";

    public static int SOURCE_CODE=100;//setup initiated after form was accessed by code
    public static int SOURCE_ORGANIZATION=200;// setup initiated after form was fetched via organization

    public static String TBL_ORGANIZATION="orgs_tbl";
    public static String TBL_FORMS="forms_tbl";
    public static String TBL_QUESTIONS ="fields_tbl";
    public static String TBL_POIS="pois_tbl";
    public static String TBL_POI_GROUP="pois_group_tbl";
    public static String TBL_OPTIONS="options_tbl";
    public static String TBL_ANSWERS="answers_tbl";
    public static String TBL_TRANSACTION="transaction_tbl";
    public static String TBL_SECTIONS ="sections_tbl";
    public static String TBL_DIMENSIONS ="dimensions_tbl";
    public static String TBL_SCOUT="scout_tbl";
    public static String TBL_SCOUT_DYNAMIC="scout_dynamic_tbl";
    public static String TBL_MAPPING="mapping_tbl";


    public static String DETAILS="details";
    public static int HISTORY_MAX=40;
    public static String STATUS_ARCHIVE="archived";
    public static String STATUS_ARCHIVED_NOT="not archived";

    //Form fields
    public static String ORG_ID="organization_id";
    public static String TITLE="title";
    public static String DESCRIPTION="description";
    public static String MAPPING="mapping";
    public static String STATUS="status";
    public static String FORM_NEW="new form";
    public static String FORM_OLD="old_form";
    public static String ACCESS_CODE="access_code";

    public static String STATUS_PUBLISHED="1";
    public static String STATUS_UNPUBLISHED="0";

    //Field fields
    public static String FORM_ID="form_id";
    public static String QUESTION="question";
    public static String TYPE="type";
    public static String HINT="hint";
    public static String REQUIRED="required";
    public static String SECTION_ID="section_id";


    //Coordinates fields
    public static String LONGITUDE="longitude";
    public static String LATITUDE="latitude";
    public static String ACCURACY="accuracy";
    public static String ALTITUDE="altitude";
    public static String TRANSACTION_ID="transaction_id";

    //Options fields
    public static String OPTION="option";

    //Answer fields
    public static String ANSWER="answer_val";
    public static String QUESTION_ID="question_id";


    //Transaction fields
    public static String USER_ID="user_id";
    public static String DATE="transaction_date";
    public static String TIME="transaction_time";
    public static String AREA="area";
    public static String DISTANCE="distance";
    public static String POI_GROUP_ID="poi_group_id";


    //ScanModel fields
    public static String SCOUT_ID="map_id";
    public static String MESSAGE ="message";
    public static String IMAGE_PATH="image_path";
    public static String PLANT_NUMBER="plant_number";
    public static String POINT_NUMBER="point_number";


    //General fields
    public static String ID="id";//autoincrement
    public static String ITEM_ID="item_id";//from server
    public static String NAME ="name";

    //neighbourhood table fields
    public static String DISTRICT_ID ="district_id";//as a secondary key to map each neighbourhoood to its town

    //neighbourhood table fields
    public static String TOWN_ID ="town_id";//as a secondary key to map each neighbourhoood to its town

    //region table fields
    public static String COUNTRY_ID ="country_id";//as a secondary key to map each region to its country


    //Knowledge base
    public static String TBL_KB="tbl_kb";
    public static String CONTENT="content";
    public static String URL="url";
    public static String LANG="language";
    public static String AUDIO="audio";

    //Providers pesticides
    public static String TBL_PROVIDERS="tbl_providers";
    public static String TBL_PESTICIDES="tbl_pesticides";
    public static String PHONE="phone";
    public static String LOCATION="location";
    public static String PROVIDER_ID="provider_id";

    //Messages
    public static String TBL_MESSAGES="tbl_messages";
    public static String AUTHOR_ID="author_id";
    public static String TBL_MESSAGE_GROUP="tbl_messages_group";
    public static String MSG_GROUP_ID ="group_id";

    //Authors
    public static String TBL_AUTHORS="tbl_authors";


    //Cart
    public static String TBL_PRODUCT="tbl_product";
    public static String QUANTITY="quantity";
    public static String PRICE="price";
    public static String OWNER_NAME="owner_name";
    public static String LOCATION_ID="location_id";
    public static String LOCATION_NAME="location_name";
    public static String MAX ="item_limit";

    //Image
    public static String TBL_IMAGE="tbl_image";

    //Receipt
    public static String TBL_RECEIPT="tbl_receipt";
    public static String AMOUNT="amount";
    public static String ORDER_CODE="order_code";


    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d(TAG,"onCreate");
        createTable(CLASS_TBL,sqLiteDatabase);
        createTable(COUNTRY_TBL,sqLiteDatabase);
        createTable(REGION_TBL,sqLiteDatabase);
        createTable(DISTRICT_TBL,sqLiteDatabase);
        createTable(TOWN_TBL,sqLiteDatabase);
        createTable(NEIGHBOURHOOD_TBL,sqLiteDatabase);
        createTable(TBL_TRANSACTION,sqLiteDatabase);
        createTable(TBL_ANSWERS,sqLiteDatabase);
        createTable(TBL_FORMS,sqLiteDatabase);
        createTable(TBL_ORGANIZATION,sqLiteDatabase);
        createTable(TBL_POIS,sqLiteDatabase);
        createTable(TBL_QUESTIONS,sqLiteDatabase);
        createTable(TBL_OPTIONS,sqLiteDatabase);
        createTable(TBL_SECTIONS,sqLiteDatabase);
        createTable(TBL_POI_GROUP,sqLiteDatabase);
        createTable(TBL_SCOUT,sqLiteDatabase);
        createTable(TBL_SCOUT_DYNAMIC,sqLiteDatabase);
        createTable(TBL_KB,sqLiteDatabase);
        createTable(TBL_PROVIDERS,sqLiteDatabase);
        createTable(TBL_PESTICIDES,sqLiteDatabase);
        createTable(TBL_MESSAGES,sqLiteDatabase);
        createTable(TBL_AUTHORS,sqLiteDatabase);
        createTable(TBL_MESSAGE_GROUP,sqLiteDatabase);
        createTable(TBL_PRODUCT,sqLiteDatabase);
        createTable(TBL_IMAGE,sqLiteDatabase);
        createTable(TBL_RECEIPT,sqLiteDatabase);
        createTable(TBL_MAPPING,sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
       Log.d(TAG,"on upgrade old: "+i+" new: "+i1);
     //  if(mContext!=null)
      //      Toast.makeText(mContext,"on upgrade old: "+i+" new: "+i1,Toast.LENGTH_SHORT).show();

        if(i1>i) {
            if(!isFieldExist(TBL_MESSAGES,MSG_GROUP_ID,sqLiteDatabase))
                sqLiteDatabase.execSQL("ALTER TABLE "+TBL_MESSAGES+" ADD COLUMN "+MSG_GROUP_ID+" VARCHAR");
            if(!isFieldExist(TBL_KB,AUDIO,sqLiteDatabase))
                sqLiteDatabase.execSQL("ALTER TABLE "+TBL_KB+" ADD COLUMN "+AUDIO+" VARCHAR");

            if(!isFieldExist(TBL_FORMS,MAPPING,sqLiteDatabase))
                sqLiteDatabase.execSQL("ALTER TABLE "+TBL_FORMS+" ADD COLUMN "+MAPPING+" VARCHAR");

            if(!isFieldExist(TBL_PRODUCT,MAX,sqLiteDatabase))
                sqLiteDatabase.execSQL("ALTER TABLE "+TBL_PRODUCT+" ADD COLUMN "+MAX+" INTEGER");

            createTable(TBL_PRODUCT,sqLiteDatabase);
            createTable(TBL_IMAGE,sqLiteDatabase);
            createTable(TBL_RECEIPT,sqLiteDatabase);
            createTable(TOWN_TBL,sqLiteDatabase);
        }
        }

    public boolean deleteDatabase(Context context){
        context.deleteDatabase(DB_NAME);
        return true;
    }

    public boolean createTable(String tableName,SQLiteDatabase db){
        Log.d(TAG,"create table: "+tableName);
        if(tableName!=null) {
            Log.d(TAG,"create tbl db: "+db+"tbl: "+tableName);
            if(db==null)
                db = this.getWritableDatabase();

            if(tableName.trim().equalsIgnoreCase(TOWN_TBL)) {
                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + NAME + " VARCHAR,"+
                        "" + DISTRICT_ID + " INTEGER);");
                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(NEIGHBOURHOOD_TBL)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + NAME + " VARCHAR," +
                        "" + TOWN_ID + " INTEGER);");

                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(DISTRICT_TBL)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + NAME + " VARCHAR);");

                return true;
            }

            else if(tableName.trim().equalsIgnoreCase(COUNTRY_TBL)){
                    Log.d(TAG,"creating country tbl");
                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + NAME + " VARCHAR);");
                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(REGION_TBL)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + NAME + " VARCHAR," +
                        "" + COUNTRY_ID + " INTEGER);");

                return true;
            }

            else if(tableName.trim().equalsIgnoreCase(CLASS_TBL)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + NAME + " VARCHAR," +
                        "" + DESCRIPTION + " VARCHAR);");

                return true;
            }

            else if(tableName.trim().equalsIgnoreCase(TBL_TRANSACTION)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + FORM_ID + " VARCHAR," +
                        "" + DATE + " VARCHAR," +
                        "" + TIME + " VARCHAR," +
                        "" + STATUS + " VARCHAR," +
                        "" + AREA + " VARCHAR," +
                        "" + DISTANCE + " VARCHAR," +
                        "" + POI_GROUP_ID + " VARCHAR," +
                        "" + USER_ID + " VARCHAR);");

                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_QUESTIONS)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + FORM_ID + " VARCHAR," +
                        "" + SECTION_ID+ " VARCHAR," +
                        "" + QUESTION + " VARCHAR," +
                        "" + TYPE + " VARCHAR," +
                        "" + DESCRIPTION + " VARCHAR," +
                        "" + HINT + " VARCHAR," +
                        "" + REQUIRED + " VARCHAR," +
                        "" + USER_ID + " VARCHAR);");

                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_ANSWERS)){
                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + QUESTION_ID + " VARCHAR," +
                        "" + ANSWER + " VARCHAR," +
                        "" + TRANSACTION_ID + " VARCHAR);");

                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_FORMS)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + ORG_ID + " VARCHAR," +
                        "" + TITLE + " VARCHAR," +
                        "" + MAPPING + " INTEGER DEFAULT 0," +
                        "" + DESCRIPTION + " VARCHAR," +
                        "" + DATE + " VARCHAR," +
                        "" + TIME + " VARCHAR," +
                        "" + ACCESS_CODE + " VARCHAR," +
                        "" + STATUS + " VARCHAR);");

                return true;
            }

            else if(tableName.trim().equalsIgnoreCase(TBL_ORGANIZATION)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + NAME + " VARCHAR," +
                        "" + DESCRIPTION + " VARCHAR);");

                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_OPTIONS)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + OPTION+ " VARCHAR," +
                        "" + TYPE+ " VARCHAR," +
                        "" + FORM_ID+ " VARCHAR," +
                        "" + QUESTION_ID + " VARCHAR);");

                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_SECTIONS)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + TITLE+ " VARCHAR," +
                        "" + TYPE+ " VARCHAR," +
                        "" + FORM_ID+ " VARCHAR," +
                        "" + DESCRIPTION + " VARCHAR);");

                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_POIS)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + LONGITUDE+ " VARCHAR," +
                        "" + LATITUDE+ " VARCHAR," +
                        "" + ALTITUDE+ " VARCHAR," +
                        "" + ACCURACY + " VARCHAR," +
                        "" + NAME + " VARCHAR," +
                        "" + TRANSACTION_ID + " VARCHAR," +
                        "" + DESCRIPTION + " VARCHAR);");

                return true;
            }

            else if(tableName.trim().equalsIgnoreCase(TBL_POI_GROUP)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID+ " VARCHAR," +
                        "" + NAME+ " VARCHAR," +
                        "" + STATUS + " VARCHAR);");

                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_SCOUT)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + SCOUT_ID + " VARCHAR," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + MESSAGE + " VARCHAR," +
                        "" + IMAGE_PATH + " VARCHAR," +
                        "" + STATUS + " INTEGER," +
                        "" + POINT_NUMBER + " INTEGER," +
                        "" + PLANT_NUMBER + " INTEGER," +
                        "" + TYPE + " INTEGER);");

                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_SCOUT_DYNAMIC)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + SCOUT_ID + " VARCHAR," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + MESSAGE + " VARCHAR," +
                        "" + IMAGE_PATH + " VARCHAR," +
                        "" + STATUS + " INTEGER," +
                        "" + POINT_NUMBER + " INTEGER," +
                        "" + PLANT_NUMBER + " INTEGER," +
                        "" + TYPE + " INTEGER);");

                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_KB)){
                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + TITLE + " INTEGER," +
                        "" + CONTENT + " VARCHAR," +
                        "" + URL + " VARCHAR," +
                        "" + AUDIO + " VARCHAR," +
                        "" + LANG + " VARCHAR," +
                        "" + TYPE + " INTEGER);");

                return true;
            }
//            else if(tableName.trim().equalsIgnoreCase(TBL_KB)){
//                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
//                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
//                        "" + TITLE + " VARCHAR," +
//                        "" + CONTENT + " VARCHAR," +
//                        "" + URL + " VARCHAR," +
//                        "" + LANG + " VARCHAR," +
//                        "" + TYPE + " INTEGER);");
//
//                return true;
//            }

            else if(tableName.trim().equalsIgnoreCase(TBL_PROVIDERS)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + TITLE + " VARCHAR," +
                        "" + PHONE + " VARCHAR," +
                        "" + LOCATION + " VARCHAR," +
                        "" + TYPE + " VARCHAR);");
                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_PESTICIDES)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + TITLE + " VARCHAR," +
                        "" + DESCRIPTION + " VARCHAR,"+
                        "" + PROVIDER_ID + " VARCHAR);");

                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_MESSAGES)){
                Log.d(TAG,"creating table: "+tableName);
                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + MESSAGE + " VARCHAR," +
                        "" + AUTHOR_ID + " VARCHAR,"+
                        "" + MSG_GROUP_ID + " VARCHAR,"+
                        "" + DATE + " INTEGER,"+
                        "" + STATUS + " VARCHAR,"+
                        "" + IMAGE_PATH + " VARCHAR);");


                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_AUTHORS)){

                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + NAME + " VARCHAR," +
                        "" + IMAGE_PATH + " VARCHAR);");

                return true;
            }

            else if(tableName.trim().equalsIgnoreCase(TBL_MESSAGE_GROUP)){
                Log.d(TAG,"creating table: "+tableName);
                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + NAME + " VARCHAR);");


                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_PRODUCT)){
                Log.d(TAG,"creating table: "+tableName);
                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + ORDER_CODE + " VARCHAR," +
                        "" + NAME + " VARCHAR," +
                        "" + DESCRIPTION + " VARCHAR," +
                        "" + PRICE + " VARCHAR," +
                        "" + OWNER_NAME + " VARCHAR," +
                        "" + MAX + " VARCHAR," +
                        "" + LOCATION_ID + " VARCHAR," +
                        "" + LOCATION_NAME + " VARCHAR," +
                        "" + QUANTITY + " INTEGER DEFAULT 1," +
                        "" + STATUS + " INTEGER);");


                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_IMAGE)){
                Log.d(TAG,"creating table: "+tableName);
                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + URL + " VARCHAR );");


                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_RECEIPT)){
                Log.d(TAG,"creating table: "+tableName);
                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + AMOUNT + " VARCHAR," +
                        "" + DATE + " VARCHAR," +
                        "" + STATUS + " VARCHAR );");

                return true;
            }
            else if(tableName.trim().equalsIgnoreCase(TBL_MAPPING)){
                Log.d(TAG,"creating table: "+tableName);
                db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName
                        + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "" + ITEM_ID + " VARCHAR," +
                        "" + NAME + " VARCHAR," +
                        "" + TYPE + " VARCHAR );");

                return true;
            }
        }

        return false;
    }


    public boolean isFieldExist(String tableName, String fieldName,SQLiteDatabase db)
    {
        boolean isExist = false;
        Cursor res = db.rawQuery("PRAGMA table_info(" + tableName + ")", null);

        if (res.moveToFirst()) {
            do {
                int value = res.getColumnIndex("name");
                if(value != -1 && res.getString(value).equals(fieldName))
                {
                    isExist = true;
                }
                // Add book to books

            } while (res.moveToNext());
        }

        return isExist;
    }

    public ArrayList<ArrayList<Object>> displayForm(){
       if(MyApplication.poiGroup.getPoiList()!=null)
                MyApplication.poiGroup=new POIGroup();

        String transaction_id=(new SettingsModel(mContext)).getTransaction_id();
        ArrayList<ArrayList<Object>> sections=new ArrayList<>();
        int tag=0;
        ArrayList<Object> fields;
        String form_id=null;
        String poi_group_id;
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }

        SQLiteDatabase db = this.getWritableDatabase();

        db.beginTransaction();

        try {
            Cursor pointerTransaction= db.rawQuery("SELECT * FROM " + TBL_TRANSACTION+" WHERE "+ID+"='"+transaction_id+"'", null);
            pointerTransaction.moveToFirst();
            if(pointerTransaction.getCount()>0) {
                form_id = pointerTransaction.getString(pointerTransaction.getColumnIndex(FORM_ID));
                poi_group_id=pointerTransaction.getString(pointerTransaction.getColumnIndex(POI_GROUP_ID));

                Cursor pointerSection = db.rawQuery("SELECT * FROM " + TBL_SECTIONS + " WHERE " + FORM_ID + "='" + form_id + "'", null);
                pointerSection.moveToFirst();
                //      Log.d(TAG,"form_id: "+form_id+" | db section pointer: "+pointerSection.getCount());

                fields = new ArrayList<>();
                while (!pointerSection.isAfterLast()) {
                    Section section = new Section();
                    String section_id = pointerSection.getString(pointerSection.getColumnIndex(ITEM_ID));
                    section.setId(section_id);
                  //  if(!pointerSection.isFirst())
                   //     section.setType(Section.SUB);
                   // else
                        section.setType(pointerSection.getString(pointerSection.getColumnIndex(TYPE)));

                    section.setTitle(pointerSection.getString(pointerSection.getColumnIndex(TITLE)));
                    section.setForm_id(pointerSection.getString(pointerSection.getColumnIndex(FORM_ID)));
                    section.setDesc(pointerSection.getString(pointerSection.getColumnIndex(DESCRIPTION)));

                    fields.add(section);//add section details to previewList

                    //      Log.d(TAG,"section_id: "+section_id);
                    Cursor pointerField = db.rawQuery("SELECT * FROM " + TBL_QUESTIONS + " WHERE " + SECTION_ID + "='" + section_id + "' AND " + FORM_ID + "='" + form_id + "'", null);
                    pointerField.moveToFirst();

                    while (!pointerField.isAfterLast()) {
                        Field field = new Field();
                        String question_id = pointerField.getString(pointerField.getColumnIndex(ITEM_ID));
                        field.setType(pointerField.getString(pointerField.getColumnIndex(TYPE)));
                        field.setQuestion(pointerField.getString(pointerField.getColumnIndex(QUESTION)));
                        Log.d(TAG,"question: "+field.getQuestion());
                        field.setDescription(pointerField.getString(pointerField.getColumnIndex(DESCRIPTION)));
                        String hint=pointerField.getString(pointerField.getColumnIndex(HINT));
                        if(hint==null||hint.trim().isEmpty()||hint.trim().equalsIgnoreCase("null"))
                            field.setHint("");
                        else
                            field.setHint(hint);

                        Log.d(TAG,"hint: "+hint);

                        String strRequired=pointerField.getString(pointerField.getColumnIndex(REQUIRED));
                          boolean required = strRequired != null && strRequired.toString().equalsIgnoreCase("1");
                        field.setRequired(required);
                        field.setId(question_id);
                        field.setTag(tag);
                        tag++;//increase tag value
                        field.setSection_id(pointerField.getString(pointerField.getColumnIndex(SECTION_ID)));

                        Cursor pointerOption = db.rawQuery("SELECT * FROM " + TBL_OPTIONS + " WHERE " + QUESTION_ID + "='" + question_id + "'", null);
                        pointerOption.moveToFirst();

                        ArrayList<String> options = new ArrayList<>();
                        while (!pointerOption.isAfterLast()) {
                            options.add(pointerOption.getString(pointerOption.getColumnIndex(OPTION)));
                            pointerOption.moveToNext();
                        }


                        Cursor pointerAnswers = db.rawQuery("SELECT * FROM " + TBL_ANSWERS + " WHERE " + QUESTION_ID + "='" + question_id
                                + "' AND "+TRANSACTION_ID+"='"+transaction_id+"'", null);
                        pointerAnswers.moveToFirst();

                        ArrayList<String> answers = new ArrayList<>();
                        while (!pointerAnswers.isAfterLast()) {
                            answers.add(pointerAnswers.getString(pointerAnswers.getColumnIndex(ANSWER)));
                            pointerAnswers.moveToNext();
                        }

                        field.setAnswers(answers);

                        Object mField=formatField(field,options);
                        if(mField!=null)
                            fields.add(mField);

                        pointerField.moveToNext();
                    }

                    pointerSection.moveToNext();

                    if (section.getType().equalsIgnoreCase(Section.MAIN) || pointerSection.isAfterLast()) {
                        sections.add(fields);//add a bunch of fields to a section
                        fields = new ArrayList<>();
                    }

                }

                Cursor pointerPOIGroup = db.rawQuery("SELECT * FROM " + TBL_POI_GROUP + " WHERE " + ITEM_ID + "='" + poi_group_id+"'", null);
                pointerPOIGroup.moveToFirst();
                if(pointerPOIGroup.getCount()>0) {
                    MyApplication.poiGroup.setName(pointerPOIGroup.getString(pointerPOIGroup.getColumnIndex(NAME)));
                    MyApplication.poiGroup.setId(pointerPOIGroup.getString(pointerPOIGroup.getColumnIndex(ITEM_ID)));
                }
                    MyApplication.poiGroup.setSelect(false);

                Cursor pointerPOI = db.rawQuery("SELECT * FROM " + TBL_POIS + " WHERE " + TRANSACTION_ID + "='" + transaction_id+"'", null);
                pointerPOI.moveToFirst();
                MyApplication.poiGroup.setPoiList(new ArrayList<POI>());
                while (!pointerPOI.isAfterLast()){
                    POI poi=new POI();
                    poi.setName(pointerPOI.getString(pointerPOI.getColumnIndex(NAME)));
                    poi.setAccuracy(pointerPOI.getString(pointerPOI.getColumnIndex(ACCURACY)));
                    poi.setLon(pointerPOI.getString(pointerPOI.getColumnIndex(LONGITUDE)));
                    poi.setLat(pointerPOI.getString(pointerPOI.getColumnIndex(LATITUDE)));
                    poi.setAlt(pointerPOI.getString(pointerPOI.getColumnIndex(ALTITUDE)));

                    MyApplication.poiGroup.getPoiList().add(poi);
                    pointerPOI.moveToNext();
                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        if(form_id!=null) {
            Form form = getForm(form_id);
            if (form != null) {
                (new SettingsModel(mContext)).setForm_id(form.getId());
                (new SettingsModel(mContext)).setOrg_id(form.getOrganization().getId());
            }
        }

        return sections;
    }

    private Object formatField(Field field, ArrayList options){
        Object mField=null;
    Log.d(TAG,field.getType()+" : "+field.getQuestion());
        if (field.getType().equalsIgnoreCase(Field.TEXT)) {
            TextField textField = new TextField();
            textField.addFields(field);
            mField=textField;
        }
        else if (field.getType().equalsIgnoreCase(Field.TEXTAREA)) {
            TextAreaField textField = new TextAreaField();
            textField.addFields(field);
            mField=textField;
        }
        else if (field.getType().equalsIgnoreCase(Field.CHECKBOX)) {
            CheckboxField checkboxField = new CheckboxField();
            checkboxField.addFields(field);
            checkboxField.setOptions(options);//options were retrieved from db earlier
            mField=checkboxField;
        }
        else if (field.getType().equalsIgnoreCase(Field.RADIO)) {
            RadioField radioField=new RadioField();
            radioField.addFields(field);
            radioField.setOptions(options);//options were retrieved from db earlier
            mField=radioField;
        }
        else if (field.getType().equalsIgnoreCase(FileField.FILE_IMAGE)
                ||field.getType().equalsIgnoreCase(FileField.FILE_VIDEO)
                ||field.getType().equalsIgnoreCase(FileField.FILE_MULTI_IMAGE)) {

                FileField fileField = new FileField();
                fileField.addFields(field);
                fileField.setType(field.getType());//set it to either file_image or file_video field type

                mField=fileField;
        }
        else if (field.getType().equalsIgnoreCase(Field.DROPDOWN)) {
            DropdownField dropdownField = new DropdownField();
            dropdownField.addFields(field);
            dropdownField.setOptions(options);//options were retrieved from db earlier
            mField=dropdownField;
        } else if (field.getType().equalsIgnoreCase(Field.EMAIL)) {
            EmailField emailField = new EmailField();
            emailField.addFields(field);
            mField=emailField;
        }
        else if (field.getType().equalsIgnoreCase(Field.DATE)) {
            DateField dateField = new DateField();
            dateField.addFields(field);
            mField=dateField;
        }
        else if (field.getType().equalsIgnoreCase(Field.NUMERIC)) {
            NumericField numericField= new NumericField();
            numericField.addFields(field);
            mField=numericField;
        }
        else if (field.getType().equalsIgnoreCase(Field.TOWN)) {
            TownField townField=new TownField();
            townField.addFields(field);
            mField=townField;
        }

        return mField;
    }

    public ArrayList<ArrayList<Object>> displayForm(String form_id){
        ArrayList<ArrayList<Object>> sections=new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        int tag=0;
        ArrayList<Object> fields;
        while (!db.isOpen()) {
              db = mContext.openOrCreateDatabase(DB_NAME, Context.MODE_PRIVATE, null);
        }

        db.beginTransaction();

            try {
                Cursor pointerSection= db.rawQuery("SELECT * FROM " + TBL_SECTIONS+" WHERE "+FORM_ID+"='"+form_id+"'", null);
                pointerSection.moveToFirst();
          //      Log.d(TAG,"form_id: "+form_id+" | db section pointer: "+pointerSection.getCount());

                fields=new ArrayList<>();
                while (!pointerSection.isAfterLast()){
                    Section section=new Section();
                    String section_id=pointerSection.getString(pointerSection.getColumnIndex(ITEM_ID));
                    section.setId(section_id);
                    section.setType(pointerSection.getString(pointerSection.getColumnIndex(TYPE)));
                    section.setTitle(pointerSection.getString(pointerSection.getColumnIndex(TITLE)));
                    section.setForm_id(pointerSection.getString(pointerSection.getColumnIndex(FORM_ID)));
                    section.setDesc(pointerSection.getString(pointerSection.getColumnIndex(DESCRIPTION)));

                    Log.d(TAG, "sectionType: "+section.getType());
                    fields.add(section);//add section details to previewList

              //      Log.d(TAG,"section_id: "+section_id);
                    Cursor pointerField= db.rawQuery("SELECT * FROM " + TBL_QUESTIONS+" WHERE "+SECTION_ID+"='"+section_id+"' AND "+FORM_ID+"='"+form_id+"'", null);
                    pointerField.moveToFirst();
                   Log.d(TAG,"field count: "+pointerField.getCount());
                    while (!pointerField.isAfterLast()){
                        Field field=new Field();
                        String question_id=pointerField.getString(pointerField.getColumnIndex(ITEM_ID));
                        field.setType(pointerField.getString(pointerField.getColumnIndex(TYPE)));
                        field.setQuestion(pointerField.getString(pointerField.getColumnIndex(QUESTION)));
                        Log.d(TAG,"question: "+field.getQuestion());
                        field.setDescription(pointerField.getString(pointerField.getColumnIndex(DESCRIPTION)));

                        String hint=pointerField.getString(pointerField.getColumnIndex(HINT));
                        if(hint==null||hint.trim().isEmpty()||hint.trim().equalsIgnoreCase("null"))
                            field.setHint("");
                        else
                            field.setHint(hint);

                        String strRequired=pointerField.getString(pointerField.getColumnIndex(REQUIRED));
                          boolean required = strRequired != null && strRequired.toString().equalsIgnoreCase("1");
                        field.setRequired(required);
                        field.setId(question_id);
                        field.setTag(tag);
                        tag++;//increase tag value
                        field.setSection_id(pointerField.getString(pointerField.getColumnIndex(SECTION_ID)));

                        Cursor pointerOption= db.rawQuery("SELECT * FROM " + TBL_OPTIONS+" WHERE "+QUESTION_ID+"='"+question_id+"'", null);
                        pointerOption.moveToFirst();

                        ArrayList<String>options=new ArrayList<>();
                        while (!pointerOption.isAfterLast()){
                            options.add(pointerOption.getString(pointerOption.getColumnIndex(OPTION)));
                            pointerOption.moveToNext();
                        }

                        //field.setRequired(true);
                        Object mField=formatField(field,options);
                        if(mField!=null)
                            fields.add(mField);

                        pointerField.moveToNext();
                    }

                    pointerSection.moveToNext();

                    if(section.getType().equalsIgnoreCase(Section.MAIN)||pointerSection.isAfterLast()){
                        sections.add(fields);//add a bunch of fields to a section
                        fields=new ArrayList<>();
                    }
                }

                //adding poi array to base object

                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }

        //Log.d(TAG,"sectionSize: "+sections.size());

        return sections;
    }

    /* Function to save user organizations, forms and form fields downloaded from the server*/
    public boolean setup(JSONObject jsonObject) throws JSONException{
        SQLiteDatabase db = this.getWritableDatabase();
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }

        JSONArray orgArray=jsonObject.getJSONArray(TBL_ORGANIZATION);
        ContentValues cv;
        //Add transaction to database
        Log.d(TAG,"setup orgArray: "+orgArray);

        for(int a=0;a<orgArray.length();a++){

            JSONObject organizationsJSON= orgArray.getJSONObject(a);
            JSONObject orgDetails=organizationsJSON.optJSONObject(DETAILS);
            cv=new ContentValues();
            String org_id = orgDetails.optString(ITEM_ID);
                cv.put(ITEM_ID, org_id);
                cv.put(NAME, orgDetails.optString(NAME));
                cv.put(DESCRIPTION, orgDetails.optString(DESCRIPTION));

            if(exists(TBL_ORGANIZATION,org_id))
                db.update(TBL_ORGANIZATION,cv,ITEM_ID+"='"+org_id+"'",null);
            else
                db.insert(TBL_ORGANIZATION, null, cv);


            JSONArray formArray=organizationsJSON.optJSONArray(TBL_FORMS);
            Log.d(TAG,"setup formArray: "+formArray);
            for(int b=0;b<formArray.length();b++){
                JSONObject formObject=formArray.optJSONObject(b);
                JSONObject formDetails=formObject.optJSONObject(DETAILS);
                cv=new ContentValues();
                String form_id=formDetails.optString(ITEM_ID);
                cv.put(ITEM_ID,form_id);
                cv.put(TITLE,formDetails.optString(TITLE));
                cv.put(MAPPING,formDetails.optInt(MAPPING));
                cv.put(DESCRIPTION, formDetails.optString(DESCRIPTION));
                cv.put(DATE,formDetails.optString(DATE));
                cv.put(TIME,formDetails.optString(TIME));
                cv.put(ORG_ID,formDetails.optString(ORG_ID));
                cv.put(STATUS,formDetails.optString(STATUS));
                    Log.d(TAG,"STATUS STATUS STATUS: "+formDetails.optString(STATUS));
                if(exists(TBL_FORMS,form_id))
                    removeForm(form_id);

                db.insert(TBL_FORMS,null,cv);

                JSONArray sectionArray=formObject.optJSONArray(TBL_SECTIONS);//nb. questions is same as fields
               // Log.d(TAG,"sectionArray: "+sectionArray);
                for(int c=0;c<sectionArray.length();c++){
                    JSONObject sectionObject=sectionArray.optJSONObject(c);
                    JSONObject sectionDetails=sectionObject.optJSONObject(DETAILS);
                    cv=new ContentValues();
                    String section_id=sectionDetails.optString(ITEM_ID);
                    cv.put(ITEM_ID,section_id);
                    cv.put(TITLE,sectionDetails.optString(TITLE));
                    cv.put(TYPE, sectionDetails.optString(TYPE));
                    cv.put(FORM_ID,form_id);
                    cv.put(DESCRIPTION, sectionDetails.optString(DESCRIPTION));

                    if(exists(TBL_SECTIONS,section_id))
                        db.update(TBL_SECTIONS,cv,ITEM_ID+"='"+section_id+"'",null);
                    else
                        db.insert(TBL_SECTIONS, null, cv);

                    JSONArray fieldArray=sectionObject.optJSONArray(TBL_QUESTIONS);//nb. questions is same as fields
                   // Log.d(TAG,"fieldArray: "+fieldArray+" form_id: "+form_id+" section_id: "+section_id);
                    for(int d=0;d<fieldArray.length();d++){
                        JSONObject fieldObject=fieldArray.optJSONObject(d);
                        cv=new ContentValues();
                        String field_id=fieldObject.optString(ITEM_ID);
                        cv.put(ITEM_ID,field_id);
                        cv.put(QUESTION,fieldObject.optString(QUESTION));
                        cv.put(TYPE, fieldObject.optString(TYPE));
                        cv.put(HINT,fieldObject.optString(HINT));
                        cv.put(DESCRIPTION, fieldObject.optString(DESCRIPTION));
                        cv.put(SECTION_ID,section_id);
                        cv.put(FORM_ID,form_id);
                        cv.put(REQUIRED,fieldObject.optString(REQUIRED));

                        Log.d(TAG,"jsonQuestion: "+fieldObject.optString(QUESTION));
                        if(exists(TBL_QUESTIONS,field_id))
                            db.update(TBL_QUESTIONS,cv,ITEM_ID+"='"+field_id+"'",null);
                        else
                            db.insert(TBL_QUESTIONS, null, cv);

                        //save field options too
                        JSONArray optionsArray=fieldObject.optJSONArray("options");
                        for(int e=0;e<optionsArray.length();e++){
                            JSONObject optionObject=optionsArray.optJSONObject(e);
                            String option_id=optionObject.optString(ID);
                            String option=optionObject.optString("value");
                            String type=optionObject.optString(TYPE);
                            cv=new ContentValues();
                            cv.put(ITEM_ID,option_id);
                            cv.put(OPTION,option);
                            cv.put(QUESTION_ID,field_id);
                            cv.put(FORM_ID,form_id);
                            cv.put(TYPE,type);

                            //check if option already exists
                            if(exists(TBL_OPTIONS,option_id))
                                db.update(TBL_OPTIONS, cv,ITEM_ID+"='"+option_id +"'",null);
                            else
                                db.insert(TBL_OPTIONS, null, cv);
                        }
                    }
                }
            }
        }

if(new SettingsModel(mContext).getForm_id()!=null&&new SettingsModel(mContext).getOrg_id()!=null){
    Form form =getForm(new SettingsModel(mContext).getForm_id());
    if(form==null){
        new SettingsModel(mContext).setForm_id(null);
        new SettingsModel(mContext).setOrg_id(null);
        new SettingsModel(mContext).setTransaction_id(null);
        CreateForm.getInstance(mContext).setShouldRefreshForm(true);
    }
}
        db.close();
        return true;
    }

public boolean removeForm(String form_id){
    Log.d(TAG,"delete form_id: "+form_id);
    if(form_id!=null&&!form_id.trim().isEmpty()) {
        delete(TBL_FORMS, "WHERE " + ITEM_ID + "='" + form_id + "'");
        delete(TBL_SECTIONS, "WHERE " + FORM_ID + "='" + form_id + "'");
        delete(TBL_QUESTIONS, "WHERE " + FORM_ID + "='" + form_id + "'");
        delete(TBL_OPTIONS, "WHERE " + FORM_ID + "='" + form_id + "'");
        return  true;
    }
    return false;
}

    public boolean removeForms(){
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor pointerForm = db.rawQuery("SELECT * FROM " + TBL_FORMS, null);
        pointerForm.moveToFirst();
        while (!pointerForm.isAfterLast()){
            String form_id=pointerForm.getString(pointerForm.getColumnIndex(ITEM_ID));
            removeForm(form_id);
            pointerForm.moveToNext();
        }

        return true;
    }

    public boolean removeItem(String tableName,int itemID){
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor pointer = db.rawQuery("SELECT * FROM "+tableName+" WHERE "+ID+"='"+itemID+"'", null);
        if(pointer.getCount()>0) {
            pointer.moveToFirst();
            db.execSQL("DELETE FROM "+tableName+" WHERE "+ID+"='"+itemID+"'");
            return true;
        }
        else
            return false;
    }

    public boolean addItem(String tableName, Object object){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        if(tableName.trim().equalsIgnoreCase(TOWN_TBL) && object instanceof ArrayList){
        //if town is provided
            db.beginTransaction();
            ArrayList<Town> townList= (ArrayList<Town>) object;
            try {
                for(int a=0;a<townList.size();a++) {
                    Town town = townList.get(a);
                    cv.put(ITEM_ID, town.getId());
                    cv.put(NAME, town.getName());
                    cv.put(DISTRICT_ID, town.getDistrict_id());

                    if (!exists(tableName, town.getId()))
                        db.insert(tableName, null, cv);
                }
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }

           db.close();
            return true;
        }
        else if(tableName.trim().equalsIgnoreCase(NEIGHBOURHOOD_TBL)&&object instanceof Neighbourhood){
        //if neighbourhood is provided
            Neighbourhood neighbourhood=(Neighbourhood) object;
            cv.put(ITEM_ID,neighbourhood.getId());
            cv.put(NAME,neighbourhood.getName());
            cv.put(TOWN_ID,neighbourhood.getTown_id());

            if(!exists(tableName,neighbourhood.getId()))
                db.insert(tableName, null, cv);

            db.close();
            return true;
        }

        else if(tableName.trim().equalsIgnoreCase(DISTRICT_TBL)&&object instanceof District){
            //if neighbourhood is provided
            District district=(District) object;
            cv.put(ITEM_ID,district.getId());
            cv.put(NAME,district.getName());

            if(!exists(tableName,district.getId()))
                db.insert(tableName, null, cv);

            db.close();
            return true;
        }

        else if(tableName.trim().equalsIgnoreCase(COUNTRY_TBL)&&object instanceof Country){
            //if neighbourhood is provided
            Country country= (Country) object;
            cv.put(ITEM_ID,country.getId());
            cv.put(NAME,country.getName());

            if(!exists(tableName,country.getId()))
                db.insert(tableName, null, cv);

            db.close();
            return true;
        }
        else if(tableName.trim().equalsIgnoreCase(REGION_TBL)&&object instanceof Region){
            //if neighbourhood is provided
            Region region= (Region) object;
            cv.put(ITEM_ID,region.getId());
            cv.put(NAME,region.getName());
            cv.put(COUNTRY_ID,region.getCountry_id());

            if(!exists(tableName,region.getId()))
                db.insert(tableName, null, cv);

            db.close();
            return true;
        }

        else if(tableName.trim().equalsIgnoreCase(CLASS_TBL)&&object instanceof ServiceClass){
            //if neighbourhood is provided
            ServiceClass serviceClass= (ServiceClass) object;
            cv.put(ITEM_ID,serviceClass.getId());
            cv.put(NAME,serviceClass.getName());
            cv.put(DESCRIPTION,serviceClass.getDescription());

            if(!exists(tableName,serviceClass.getId()))
                db.insert(tableName, null, cv);

            db.close();
            return true;
        }


        else if(tableName.trim().equalsIgnoreCase(TBL_TRANSACTION)&&object instanceof Transaction){
            //if neighbourhood is provided
            Transaction transaction= (Transaction) object;
            cv.put(ITEM_ID,transaction.getId());
            cv.put(FORM_ID,transaction.getForm_id());
            cv.put(FORM_ID,transaction.getForm_id());
            cv.put(TIME,transaction.getTime());
            cv.put(DATE,transaction.getDate());
            cv.put(USER_ID,transaction.getUser_id());

            if(!exists(tableName,transaction.getId()))
                db.insert(tableName, null, cv);

            db.close();
            return true;
        }


        else if(tableName.trim().equalsIgnoreCase(TBL_FORMS)&&object instanceof Form){
            //if neighbourhood is provided
            Form form= (Form) object;
            cv.put(ITEM_ID,form.getId());
            cv.put(ORG_ID,(form.getOrganization()).getId());
            cv.put(TITLE,form.getTitle());
            cv.put(DESCRIPTION,form.getDescription());
            cv.put(STATUS,form.getStatus());

            if(!exists(tableName,form.getId()))
                db.insert(tableName, null, cv);

            db.close();
            return true;
        }


        else if(tableName.trim().equalsIgnoreCase(TBL_QUESTIONS)&&object instanceof Question){
            //if neighbourhood is provided
            Question question= (Question) object;
            cv.put(ITEM_ID,question.getId());
            cv.put(FORM_ID,question.getForm_id());
            cv.put(QUESTION,question.getValue());
            cv.put(TYPE,question.getType());

            if(!exists(tableName,question.getId()))
                db.insert(tableName, null, cv);

            db.close();
            return true;
        }

        else if(tableName.trim().equalsIgnoreCase(TBL_ANSWERS)&&object instanceof Answer){
            //if neighbourhood is provided
            Answer answer= (Answer) object;
            cv.put(ITEM_ID,answer.getId());
            cv.put(QUESTION_ID,answer.getQuestion_id());
            cv.put(ANSWER,answer.getAnswer());
            cv.put(TRANSACTION_ID,answer.getTransaction_id());

            if(!exists(tableName,answer.getId()))
                db.insert(tableName, null, cv);

            db.close();
            return true;
        }


        else if(tableName.trim().equalsIgnoreCase(TBL_ORGANIZATION)&&object instanceof Organization){
            //if neighbourhood is provided
            Organization organization= (Organization) object;
            cv.put(ITEM_ID,organization.getId());
            cv.put(NAME,organization.getName());
            cv.put(DESCRIPTION,organization.getDescription());

            if(!exists(tableName,organization.getId()))
                db.insert(tableName, null, cv);

            db.close();
            return true;
        }

        else if(tableName.trim().equalsIgnoreCase(TBL_POIS)&&object instanceof POI){
            //if neighbourhood is provided
            POI poi= (POI) object;
            cv.put(LONGITUDE,poi.getLon());
            cv.put(LATITUDE,poi.getLat());
            cv.put(ACCURACY,poi.getAccuracy());
            cv.put(TRANSACTION_ID,poi.getTransaction_id());

            if(!exists(tableName,poi.getId()))
                db.insert(tableName, null, cv);

            db.close();
            return true;
        }

        db.close();
        return false;
    }

    public String saveArchive(JSONObject jsonObject) throws JSONException{

        Log.d(TAG,"archive JSON: "+jsonObject);
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        //Add transaction to database
        JSONObject transactionJSON= jsonObject.getJSONObject(TBL_TRANSACTION);

        cv.put(FORM_ID,transactionJSON.optString(FORM_ID));
        cv.put(USER_ID,transactionJSON.optString(USER_ID));
        cv.put(DATE, transactionJSON.optString(DATE));
        cv.put(TIME,transactionJSON.optString(TIME));
        cv.put(STATUS,STATUS_ARCHIVE);
        cv.put(POI_GROUP_ID,transactionJSON.optString(POI_GROUP_ID));

        db.insert(TBL_TRANSACTION, null, cv);

        //Get transaction ID from database
        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_TRANSACTION+" ORDER BY "+ID+" DESC LIMIT 1", null);
        pointer.moveToFirst();
        String transactionID=pointer.getString(pointer.getColumnIndex(ID));

        db.beginTransaction();
        Log.d(TAG,"transactionID: "+transactionID+" count: "+pointer.getCount());
        try {

            //Add answers to database
            JSONArray answerJSONArray=jsonObject.getJSONArray(TBL_ANSWERS);
            JSONObject answerObject;
            for(int a=0;a<answerJSONArray.length();a++){
                cv=new ContentValues();
                answerObject=answerJSONArray.getJSONObject(a);
                //Log.d(TAG,"answer object: "+answerObject);
                String answer=answerObject.optString(ANSWER);
                //Making sure answers inserted are not empty
                     Log.d(TAG,"answer: "+answer);
                    cv.put(QUESTION_ID, answerObject.optString(QUESTION_ID));
                    cv.put(ANSWER, answer);
                    cv.put(TRANSACTION_ID, transactionID);

                    db.insert(TBL_ANSWERS, null, cv);

            }

            //Add POIS to database
            JSONArray poiJSONArray=jsonObject.getJSONArray(TBL_POIS);
            JSONObject poiObject;
            Log.d(TAG,"poiJSON: "+poiJSONArray);
            for(int a=0;a<poiJSONArray.length();a++){
                cv=new ContentValues();
                poiObject=poiJSONArray.getJSONObject(a);

                cv.put(ACCURACY,poiObject.optString(ACCURACY));
                cv.put(LONGITUDE,poiObject.optString(LONGITUDE));
                cv.put(LATITUDE,poiObject.optString(LATITUDE));
                cv.put(TRANSACTION_ID,transactionID);
                cv.put(ALTITUDE,poiObject.optString(ALTITUDE));
                cv.put(NAME,poiObject.optString(NAME));

                db.insert(TBL_POIS, null, cv);

            }
            db.setTransactionSuccessful();

        } finally {
            db.endTransaction();
        }


            db.close();
        Log.d(TAG,"saved archive: "+transactionID);
            return transactionID;
    }

    public ArrayList<Organization> getOrganizations(){
        //if parent_id=-1 means all children should be retrieved
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }

        SQLiteDatabase db = this.getWritableDatabase();
            ArrayList<Organization> list=new ArrayList<>();
            Organization organization;
            Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_ORGANIZATION, null);

            pointer.moveToFirst();
            while (!pointer.isAfterLast()){
                organization=new Organization();
                organization.setId(pointer.getString(pointer.getColumnIndex(ITEM_ID)));
                organization.setName(pointer.getString(pointer.getColumnIndex(NAME)));
                organization.setDescription(pointer.getString(pointer.getColumnIndex(DESCRIPTION)));

                Cursor pointer2= db.rawQuery("SELECT * FROM " + TBL_FORMS+" " +
                        "WHERE "+ORG_ID+"='"+organization.getId()+"' AND "+STATUS+"="+STATUS_PUBLISHED, null);
                organization.setFormCount(String.valueOf(pointer2.getCount()));

                list.add(organization);
                pointer.moveToNext();
            }
        Collections.reverse(list);
            return list;
    }

    public ArrayList<Archive> getArchives(){
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Archive> list=new ArrayList<>();
        Archive archive;
        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_TRANSACTION+" WHERE "+STATUS+"='"+STATUS_ARCHIVE+"'", null);

        pointer.moveToFirst();
        while (!pointer.isAfterLast()){
            archive=new Archive();
            String form_id=pointer.getString(pointer.getColumnIndex(FORM_ID));
            String date=pointer.getString(pointer.getColumnIndex(DATE));
            String time=pointer.getString(pointer.getColumnIndex(TIME));
            String transaction_id= pointer.getString(pointer.getColumnIndex(ID));

            Cursor pointer2= db.rawQuery("SELECT * FROM " + TBL_FORMS+" " +
                    "WHERE "+ITEM_ID+"='"+form_id+"' AND "+STATUS+"="+STATUS_PUBLISHED, null);
            pointer2.moveToFirst();
            String form_title=pointer2.getString(pointer2.getColumnIndex(TITLE));
            String org_id=pointer2.getString(pointer2.getColumnIndex(ORG_ID));

            archive.setName(form_title);
            archive.setDate(date);
            archive.setTime(time);
            archive.setOrganization(getOrganization(org_id));
            archive.setTransaction_id(transaction_id);
            archive.setChecked(false);

            list.add(archive);
            pointer.moveToNext();
        }
        Collections.reverse(list);
        return list;
    }

    public ArrayList<Archive> getHistory(){
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Archive> list=new ArrayList<>();
        Archive archive;
        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_TRANSACTION+" LIMIT "+HISTORY_MAX, null);

        pointer.moveToFirst();
        while (!pointer.isAfterLast()){
            archive=new Archive();
            String form_id=pointer.getString(pointer.getColumnIndex(FORM_ID));
            String date=pointer.getString(pointer.getColumnIndex(DATE));
            String time=pointer.getString(pointer.getColumnIndex(TIME));
            String transaction_id= pointer.getString(pointer.getColumnIndex(ID));

            Cursor pointer2= db.rawQuery("SELECT * FROM " + TBL_FORMS+"" +
                    " WHERE "+ITEM_ID+"='"+form_id+"' AND "+STATUS+"="+STATUS_PUBLISHED, null);
            pointer2.moveToFirst();
            String form_title=pointer2.getString(pointer2.getColumnIndex(TITLE));
            String org_id=pointer2.getString(pointer2.getColumnIndex(ORG_ID));

            archive.setName(form_title);
            archive.setDate(date);
            archive.setTime(time);
            archive.setOrganization(getOrganization(org_id));
            archive.setTransaction_id(transaction_id);
            archive.setChecked(false);

            list.add(archive);
            pointer.moveToNext();
        }
        Collections.reverse(list);
        return list;
    }

    public boolean removeArchive(String transaction_id){
        boolean result=true;
        int tag=0;
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        try {
            //Cursor pointerTransaction= db.rawQuery("SELECT * FROM " + TBL_TRANSACTION+" WHERE "+ID+"="+transaction_id, null);
            //pointerTransaction.moveToFirst();
            //String form_id=pointerTransaction.getString(pointerTransaction.getColumnIndex(FORM_ID));

            ContentValues cv=new ContentValues();
            cv.put(STATUS,STATUS_ARCHIVED_NOT);
            db.update(TBL_TRANSACTION,cv,ID+"="+transaction_id,null);
            // TODO: 3/9/2018 remove answers based on archived removed

            //boolean success=
                   /* delete(TBL_TRANSACTION,"WHERE "+ID+"="+transaction_id);*/
            //if(delete==0)
            //    result=false;//no item was deleted hence removeArchive should return false
        //if(success)
           /*delete(TBL_ANSWERS,"WHERE "+TRANSACTION_ID+"='"+transaction_id+"'");*/
            //if(delete==0)
            //    result=false;//no item was deleted hence removeArchive should return false

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
          //  Log.d(TAG,"removed archive: "+result);
        //Log.d(TAG,"removed archive: "+transaction_id);
         return result;
    }

    public Organization getOrganization(String orgID){
        //if parent_id=-1 means all children should be retrieved
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_ORGANIZATION+" WHERE "+ITEM_ID+"='"+orgID+"'", null);

            pointer.moveToFirst();
        Organization organization=null;
if(pointer.getCount()>0) {
    organization = new Organization();
    organization.setId(pointer.getString(pointer.getColumnIndex(ITEM_ID)));
    organization.setName(pointer.getString(pointer.getColumnIndex(NAME)));
    organization.setDescription(pointer.getString(pointer.getColumnIndex(DESCRIPTION)));

    Cursor pointer2 = db.rawQuery("SELECT * FROM " + TBL_FORMS + " " +
            "WHERE " + ORG_ID + "='" + organization.getId() + "' AND "+STATUS+"="+STATUS_PUBLISHED, null);
    organization.setFormCount(String.valueOf(pointer2.getCount()));

    pointer.moveToNext();
}
        return organization;
    }

    public Form getForm(String formID){
        Form form=null;

//            while (!db.isOpen()) {
//                db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//            }
        SQLiteDatabase db = this.getWritableDatabase();
            Cursor pointer = db.rawQuery("SELECT * FROM " + TBL_FORMS + " " +
                    "WHERE " + ITEM_ID + "='" + formID + "' AND "+STATUS+"="+STATUS_PUBLISHED, null);
            pointer.moveToFirst();
            if(pointer.getCount()>0) {
                form = new Form();
                form.setId(pointer.getString(pointer.getColumnIndex(ITEM_ID)));
                form.setTitle(pointer.getString(pointer.getColumnIndex(TITLE)));
                form.setDescription(pointer.getString(pointer.getColumnIndex(DESCRIPTION)));
                form.mapping=pointer.getInt(pointer.getColumnIndex(MAPPING));

                String orgID = pointer.getString(pointer.getColumnIndex(ORG_ID));

                Cursor pointer2 = db.rawQuery("SELECT * FROM " + TBL_ORGANIZATION + " WHERE " + ITEM_ID + "='" + orgID + "'", null);
                pointer2.moveToFirst();

                Organization organization = new Organization();
                organization.setName(pointer2.getString(pointer2.getColumnIndex(NAME)));
                organization.setId(pointer2.getString(pointer2.getColumnIndex(ITEM_ID)));
                organization.setDescription(pointer2.getString(pointer2.getColumnIndex(DESCRIPTION)));

                form.setOrganization(organization);
            }

            Log.d(TAG,"form: "+form);
            return form;
        }

    public Form getFormByCode(String code){
        Form form=null;

//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor pointer = db.rawQuery("SELECT * FROM " + TBL_FORMS + " WHERE " + ACCESS_CODE + "='" + code + "'", null);
        pointer.moveToFirst();
        if(pointer.getCount()>0) {
            form = new Form();
            form.setId(pointer.getString(pointer.getColumnIndex(ITEM_ID)));
            form.setTitle(pointer.getString(pointer.getColumnIndex(TITLE)));
            form.setDescription(pointer.getString(pointer.getColumnIndex(DESCRIPTION)));

            String orgID = pointer.getString(pointer.getColumnIndex(ORG_ID));

            Cursor pointer2 = db.rawQuery("SELECT * FROM " + TBL_ORGANIZATION + " WHERE " + ITEM_ID + "='" + orgID + "'", null);
            pointer2.moveToFirst();

            Organization organization = new Organization();
            organization.setName(pointer2.getString(pointer2.getColumnIndex(NAME)));
            organization.setId(pointer2.getString(pointer2.getColumnIndex(ITEM_ID)));
            organization.setDescription(pointer2.getString(pointer2.getColumnIndex(DESCRIPTION)));

            form.setOrganization(organization);
        }

        Log.d(TAG,"form: "+form);
        return form;
    }

    public ArrayList getForms(String orgID){
        //if parent_id=-1 means all children should be retrieved
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Form> list=new ArrayList<>();
        Form form;
        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_FORMS+
                " WHERE "+ORG_ID+"='"+orgID+"' AND "+STATUS+"="+STATUS_PUBLISHED, null);

        pointer.moveToFirst();
        Log.d(TAG,"formList: "+pointer.getCount());

        while (!pointer.isAfterLast()){
            form=new Form();
            form.setId(pointer.getString(pointer.getColumnIndex(ITEM_ID)));
            form.setTitle(pointer.getString(pointer.getColumnIndex(TITLE)));
            form.setDescription(pointer.getString(pointer.getColumnIndex(DESCRIPTION)));
            form.mapping=pointer.getInt(pointer.getColumnIndex(MAPPING));

            list.add(form);
            pointer.moveToNext();
        }
        Collections.reverse(list);

        return list;
    }

    public JSONObject buildJSON(String transaction_id, boolean isFinalBuild) throws JSONException {
        JSONObject baseObject= new JSONObject();
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        try {

        Cursor pointerTransaction= db.rawQuery("SELECT * FROM " + TBL_TRANSACTION+" WHERE "+ID+"='"+transaction_id+"'", null);
        pointerTransaction.moveToFirst();

        JSONObject transactionObject=new JSONObject();
        transactionObject.put(DatabaseHelper.FORM_ID,pointerTransaction.getString(pointerTransaction.getColumnIndex(FORM_ID)));
        transactionObject.put(DatabaseHelper.USER_ID,pointerTransaction.getString(pointerTransaction.getColumnIndex(USER_ID)));
        transactionObject.put(DatabaseHelper.DATE,pointerTransaction.getString(pointerTransaction.getColumnIndex(DATE)));
        transactionObject.put(DatabaseHelper.TIME,pointerTransaction.getString(pointerTransaction.getColumnIndex(TIME)));

        baseObject.put(DatabaseHelper.TBL_TRANSACTION,transactionObject);//add transaction properties to base object


        Cursor pointerAnswers= db.rawQuery("SELECT * FROM " + TBL_ANSWERS+" WHERE "+TRANSACTION_ID+"='"+transaction_id+"'", null);
        pointerAnswers.moveToFirst();

        JSONArray answerArray=new JSONArray();
            JSONObject answerObject;
            while(!pointerAnswers.isAfterLast()) {
                String field_id = pointerAnswers.getString(pointerAnswers.getColumnIndex(QUESTION_ID));
                String answer = pointerAnswers.getString(pointerAnswers.getColumnIndex(ANSWER));
                String type = null;

                Cursor pointerField = db.rawQuery("SELECT " + TYPE + " FROM " + TBL_QUESTIONS + " WHERE " + ITEM_ID + "='" + field_id + "'", null);
                pointerField.moveToFirst();
                if (pointerField.getCount() > 0)
                    type = pointerField.getString(pointerField.getColumnIndex(TYPE));

                if (isFinalBuild&&(type!=null&& type.trim().equalsIgnoreCase(FileField.FILE_IMAGE)
                                ||type.trim().equalsIgnoreCase(FileField.FILE_VIDEO)
                                ||type.trim().equalsIgnoreCase(FileField.FILE_MULTI_IMAGE))) {
                    CreateForm.getInstance(mContext).
                            fillFilesToUpload(field_id, type, answer);
                } else {
                    answerObject = CreateForm.getInstance(mContext).fillJSON(field_id, answer, type);
                    answerArray.put(answerObject); //adding each answer model to answer array
                }
                pointerAnswers.moveToNext();
            }

            baseObject.put(DatabaseHelper.TBL_ANSWERS,answerArray);//add answer array to base object


        Cursor pointerPOI= db.rawQuery("SELECT * FROM " + TBL_POIS+" WHERE "+TRANSACTION_ID+"='"+transaction_id+"'", null);
        pointerPOI.moveToFirst();

        JSONObject poiObject;
        JSONArray poiArray=new JSONArray();
        ArrayList<POI> poiList=new ArrayList<>();
        POI poi;
        while(!pointerPOI.isAfterLast()) {
            String poiName=pointerPOI.getString(pointerPOI.getColumnIndex(NAME));
            String poiAccur=pointerPOI.getString(pointerPOI.getColumnIndex(ACCURACY));
            String poiLon=pointerPOI.getString(pointerPOI.getColumnIndex(LONGITUDE));
            String poiLat=pointerPOI.getString(pointerPOI.getColumnIndex(LATITUDE));
            String poiAlt=pointerPOI.getString(pointerPOI.getColumnIndex(ALTITUDE));

            poiObject=new JSONObject();
            poiObject.put(DatabaseHelper.NAME,poiName);
            poiObject.put(DatabaseHelper.ACCURACY,poiAccur);
            poiObject.put(DatabaseHelper.LONGITUDE,poiLon);
            poiObject.put(DatabaseHelper.LATITUDE,poiLat);
            poiObject.put(DatabaseHelper.ALTITUDE,poiAlt);


            poiArray.put(poiObject);
            poiList.add(new POI(poiName,poiLon,poiLat,poiAlt,poiAccur));
            pointerPOI.moveToNext();
        }

        baseObject.put(DatabaseHelper.TBL_POIS,poiArray);//adding poi array to base object

            int area=0;
            long distance=0;

            if(poiList.size()>0){
                area=MyApplication.getArea(poiList);
                distance=MyApplication.getPerimeter(poiList);
            }
            Log.d(TAG,"area: "+area+" distance: "+distance);
            JSONObject dimenObject=new JSONObject();
            dimenObject.put(AREA,area);
            dimenObject.put(DISTANCE,distance);

            baseObject.put(TBL_DIMENSIONS,dimenObject);
            db.setTransactionSuccessful();
    } finally {
        db.endTransaction();
    }


    return baseObject;
    }

    public ArrayList<Object> buildPreview(String transaction_id){

        ArrayList<Object> previewList=new ArrayList<>();
        Log.d(TAG,"transaction_id: "+transaction_id);
        if(transaction_id!=null) {
            int tag = 0;
//            while (!db.isOpen()) {
//                db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//            }
            SQLiteDatabase db = this.getWritableDatabase();
            db.beginTransaction();

            try {
                Cursor pointerTransaction = db.rawQuery("SELECT * FROM " + TBL_TRANSACTION + " WHERE " + ID + "=" + Integer.parseInt(transaction_id) + "", null);
                pointerTransaction.moveToFirst();

                Log.d(TAG, "transaction_count: " + pointerTransaction.getCount());
                if(pointerTransaction.getCount()>0){
                String form_id = pointerTransaction.getString(pointerTransaction.getColumnIndex(FORM_ID));

                Cursor pointerSection = db.rawQuery("SELECT * FROM " + TBL_SECTIONS + " WHERE " + FORM_ID + "='" + form_id + "'", null);
                pointerSection.moveToFirst();

                while (!pointerSection.isAfterLast()) {

                    Section section = new Section();
                    String section_id = pointerSection.getString(pointerSection.getColumnIndex(ITEM_ID));
                    section.setId(section_id);
                    section.setType(pointerSection.getString(pointerSection.getColumnIndex(TYPE)));
                    section.setTitle(pointerSection.getString(pointerSection.getColumnIndex(TITLE)));
                    section.setForm_id(pointerSection.getString(pointerSection.getColumnIndex(FORM_ID)));
                    section.setDesc(pointerSection.getString(pointerSection.getColumnIndex(DESCRIPTION)));

                    Log.d(TAG, "sectionType: " + section.getType());
                    previewList.add(section);//add section details to previewList

                    Cursor pointerField = db.rawQuery("SELECT * FROM " + TBL_QUESTIONS + " WHERE " + SECTION_ID + "='" + section_id + "' AND " + FORM_ID + "='" + form_id + "'", null);
                    pointerField.moveToFirst();

                    while (!pointerField.isAfterLast()) {
                        Field field = new Field();
                        String question_id = pointerField.getString(pointerField.getColumnIndex(ITEM_ID));
                        field.setType(pointerField.getString(pointerField.getColumnIndex(TYPE)));
                        field.setQuestion(pointerField.getString(pointerField.getColumnIndex(QUESTION)));
                        field.setDescription(pointerField.getString(pointerField.getColumnIndex(DESCRIPTION)));
                        field.setHint(pointerField.getString(pointerField.getColumnIndex(HINT)));
                        //field.setRequired();
                        field.setId(question_id);
                        field.setTag(tag);
                        tag++;//increase tag value
                        field.setSection_id(pointerField.getString(pointerField.getColumnIndex(SECTION_ID)));

                        Cursor pointerOption = db.rawQuery("SELECT * FROM " + TBL_OPTIONS + " WHERE " + QUESTION_ID + "='" + question_id + "'", null);
                        pointerOption.moveToFirst();

                        ArrayList<String> options = new ArrayList<>();
                        while (!pointerOption.isAfterLast()) {
                            options.add(pointerOption.getString(pointerOption.getColumnIndex(OPTION)));
                            pointerOption.moveToNext();
                        }

                        Cursor pointerAnswers = db.rawQuery("SELECT * FROM " + TBL_ANSWERS + " WHERE " + TRANSACTION_ID + "='" + transaction_id + "' AND " + QUESTION_ID + "='" + question_id + "'", null);
                        pointerAnswers.moveToFirst();
                        Log.d(TAG, "transaction_id: " + transaction_id + " question_id: " + question_id + " answers count: " + pointerAnswers.getCount());
                        ArrayList<String> answers = new ArrayList<>();
                        while (!pointerAnswers.isAfterLast()) {
                            String answer = pointerAnswers.getString(pointerAnswers.getColumnIndex(ANSWER));
                            if (answer != null && !answer.trim().isEmpty())
                                answers.add(answer);

                            pointerAnswers.moveToNext();
                        }

                        // answers.add(String.valueOf(field.getTag()));

                        field.setAnswers(answers);//add answers to field. NB some tvMessage types have only one answer
                        Object mField=formatField(field,options);
                        if(mField!=null)
                            previewList.add(mField);

                        pointerField.moveToNext();
                    }

                    pointerSection.moveToNext();
                }
                }

                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }

           // Log.d(TAG, "previewList: " + previewList.size());
        }
        return previewList;
    }

//    public ArrayList<Object> buildPreview2(String transaction_id){
//        ArrayList<Object> previewList= new ArrayList<>();
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.beginTransaction();
//
//        try {
//            Cursor pointerTransaction= db.rawQuery("SELECT * FROM " + TBL_TRANSACTION+" WHERE "+ID+"='"+transaction_id+"'", null);
//            pointerTransaction.moveToFirst();
//            String form_id=pointerTransaction.getString(pointerTransaction.getColumnIndex(FORM_ID));
//
//            Cursor pointerSection= db.rawQuery("SELECT * FROM " + TBL_SECTIONS+" WHERE "+FORM_ID+"='"+form_id+"'", null);
//            pointerSection.moveToFirst();
//
//            while (!pointerSection.isAfterLast()){
//                Section section=new Section();
//                String section_id=pointerSection.getString(pointerSection.getColumnIndex(ITEM_ID));
//                section.setId(section_id);
//                section.setType(pointerSection.getString(pointerSection.getColumnIndex(TYPE)));
//                section.setTitle(pointerSection.getString(pointerSection.getColumnIndex(TITLE)));
//                section.setForm_id(pointerSection.getString(pointerSection.getColumnIndex(FORM_ID)));
//                section.setDesc(pointerSection.getString(pointerSection.getColumnIndex(DESCRIPTION)));
//
//                previewList.add(section);//add section details to previewList
//
//                Cursor pointerField= db.rawQuery("SELECT * FROM " + TBL_QUESTIONS+" WHERE "+SECTION_ID+"='"+section_id+"'", null);
//                pointerField.moveToFirst();
//
//                while (!pointerField.isAfterLast()){
//                    Field field=new Field();
//                    String question_id=pointerField.getString(pointerSection.getColumnIndex(ITEM_ID));
//                    field.setType(pointerSection.getString(pointerSection.getColumnIndex(TYPE)));
//                    field.setDescription(pointerSection.getString(pointerSection.getColumnIndex(DESCRIPTION)));
//                    field.setHint(pointerSection.getString(pointerSection.getColumnIndex(HINT)));
//                    field.setId(pointerSection.getString(pointerSection.getColumnIndex(ITEM_ID)));
//                    field.setSection_id(pointerSection.getString(pointerSection.getColumnIndex(SECTION_ID)));
//
//                    Cursor pointerAnswer= db.rawQuery("SELECT * FROM " + TBL_ANSWERS+" WHERE "+QUESTION_ID+"='"+question_id+"' AND "+TRANSACTION_ID+"='"+transaction_id+"'", null);
//                    pointerAnswer.moveToFirst();
//                    ArrayList<String> answers=new ArrayList<>();
//                    while (!pointerAnswer.isAfterLast()){
//                        answers.add(pointerAnswer.getString(pointerAnswer.getColumnIndex(ANSWER)));
//                        pointerAnswer.moveToNext();
//                    }
//                    field.setAnswers(answers);//set all answers associated with this field
//                    previewList.add(field);//add field details to previewList
//                    pointerField.moveToNext();
//                }
//
//                pointerField.moveToNext();
//            }
//
//
//           //adding poi array to base object
//
//            db.setTransactionSuccessful();
//        } finally {
//            db.endTransaction();
//        }
//
//
//        return previewList;
//    }

    public ArrayList getAllItems(String tableName, String parent_id){
        //if parent_id=-1 means all children should be retrieved
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }
        SQLiteDatabase db = this.getWritableDatabase();
        if(tableName.trim().equalsIgnoreCase(TOWN_TBL)){
            ArrayList<Town> list=new ArrayList<>();
            Town town;
            Cursor pointer;
            if (parent_id!="-1")
                pointer= db.rawQuery("SELECT * FROM " + tableName+"WHERE "+TOWN_ID+"="+parent_id, null);
            else
                pointer= db.rawQuery("SELECT * FROM " + tableName, null);

            pointer.moveToFirst();
            while (!pointer.isAfterLast()){
                town=new Town();
                town.setId(pointer.getString(pointer.getColumnIndex(ITEM_ID)));
                town.setName(pointer.getString(pointer.getColumnIndex(NAME)));
                town.setDistrict_id(pointer.getString(pointer.getColumnIndex(DISTRICT_ID)));

                list.add(town);
                pointer.moveToNext();
            }
            return list;
        }
        else if(tableName.trim().equalsIgnoreCase(NEIGHBOURHOOD_TBL)){
            ArrayList<Neighbourhood> list=new ArrayList<>();
            Neighbourhood neighbourhood;
            Cursor pointer;
            if (parent_id!="-1")
                pointer= db.rawQuery("SELECT * FROM " + tableName+"WHERE "+TOWN_ID+"="+parent_id, null);
            else
                pointer= db.rawQuery("SELECT * FROM " + tableName, null);

            pointer.moveToFirst();

            while (!pointer.isAfterLast()){
                neighbourhood=new Neighbourhood();
                neighbourhood.setId(String.valueOf(pointer.getInt(pointer.getColumnIndex(ITEM_ID))));
                neighbourhood.setName(String.valueOf(pointer.getString(pointer.getColumnIndex(NAME))));
                neighbourhood.setTown_id(String.valueOf(pointer.getInt(pointer.getColumnIndex(TOWN_ID))));

                list.add(neighbourhood);
                pointer.moveToNext();
            }
            return list;
        }

        else if(tableName.trim().equalsIgnoreCase(DISTRICT_TBL)){
            ArrayList<District> list=new ArrayList<>();
            District district;

            Cursor pointer= db.rawQuery("SELECT * FROM " + tableName, null);
            pointer.moveToFirst();

            while (!pointer.isAfterLast()){
                district=new District();
                district.setId(pointer.getString(pointer.getColumnIndex(ITEM_ID)));
                district.setName(pointer.getString(pointer.getColumnIndex(NAME)));

                list.add(district);
                pointer.moveToNext();
            }
            return list;
        }

        else if(tableName.trim().equalsIgnoreCase(COUNTRY_TBL)){
            ArrayList<Country> list=new ArrayList<>();
            Country country;

            Cursor pointer= db.rawQuery("SELECT * FROM " + tableName, null);
            pointer.moveToFirst();

            while (!pointer.isAfterLast()){
                country=new Country();
                country.setId(pointer.getString(pointer.getColumnIndex(ITEM_ID)));
                country.setName(pointer.getString(pointer.getColumnIndex(NAME)));

                list.add(country);
                pointer.moveToNext();
            }
            return list;
        }

        else if(tableName.trim().equalsIgnoreCase(REGION_TBL)){
            ArrayList<Region> list=new ArrayList<>();
            Region region;
            Cursor pointer;
            if (parent_id!="-1")
                pointer= db.rawQuery("SELECT * FROM " + tableName+" WHERE "+COUNTRY_ID+"='"+parent_id+"'", null);
            else
                pointer= db.rawQuery("SELECT * FROM " + tableName, null);

            pointer.moveToFirst();
            while (!pointer.isAfterLast()){
                region=new Region();
                region.setId(pointer.getString(pointer.getColumnIndex(ITEM_ID)));
                region.setName(pointer.getString(pointer.getColumnIndex(NAME)));
                region.setCountry_id(pointer.getString(pointer.getColumnIndex(COUNTRY_ID)));

                list.add(region);
                pointer.moveToNext();
            }
            return list;
        }
        else if(tableName.trim().equalsIgnoreCase(CLASS_TBL)){

            ArrayList<ServiceClass> list=new ArrayList<>();
            ServiceClass serviceClass;

            Cursor pointer= db.rawQuery("SELECT * FROM " + tableName, null);
            pointer.moveToFirst();
            while (!pointer.isAfterLast()){
                serviceClass=new ServiceClass();
                serviceClass.setId(pointer.getString(pointer.getColumnIndex(ITEM_ID)));
                serviceClass.setName(pointer.getString(pointer.getColumnIndex(NAME)));
                serviceClass.setDescription(pointer.getString(pointer.getColumnIndex(DESCRIPTION)));

                list.add(serviceClass);
                pointer.moveToNext();
            }
            pointer.close();
            return list;
        }

        return null;
    }

    public Town getTown(String townName){
            ArrayList<Town> list=new ArrayList<>();
            Town town;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor pointer= db.rawQuery("SELECT * FROM "+TOWN_TBL+"WHERE "+NAME+"="+townName, null);
            pointer.moveToFirst();

                town=new Town();
                town.setId(pointer.getString(pointer.getColumnIndex(ITEM_ID)));
                town.setName(pointer.getString(pointer.getColumnIndex(NAME)));
                town.setDistrict_id(pointer.getString(pointer.getColumnIndex(DISTRICT_ID)));

                list.add(town);
                pointer.moveToNext();

        return town;
    }

    public boolean exists(String tableName, Object itemID){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor pointer = db.rawQuery("SELECT "+ITEM_ID+" FROM " + tableName+" WHERE "+ITEM_ID+"='"+itemID+"'", null);
        if(pointer.getCount()>0) {
            pointer.close();
            return true;
        }
        else {
            pointer.close();
            return false;
        }
    }

    public boolean removeAnswer(String tableName,String transaction_id,String field_id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+tableName+" WHERE "+TRANSACTION_ID+"='"+transaction_id+"' " +
                "AND "+QUESTION_ID+"='"+field_id+"'");
        return true;
    }

    public boolean deleteItem(String tableName,Object id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+tableName+" WHERE "+ITEM_ID+"='"+id+"'");
        db.close();
        return true;
    }

    public boolean delete(String tableName,String whereClause){
      //  if(db.isOpen())
     //           db.close();
       // if (!db.isOpen())
       //     db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+tableName+" "+whereClause);
      //  db.close();

        return true;
    }

    public int getSize(String tableName){
        SQLiteDatabase db = this.getWritableDatabase();
        return  (db.rawQuery("SELECT * FROM " + tableName, null)).getCount();
    }

    public boolean isEmpty(String tableName){
        SQLiteDatabase db = this.getWritableDatabase();
        return  ((db.rawQuery("SELECT * FROM " + tableName, null)).getCount()==0);
    }

    public boolean isEmpty(String tableName,boolean close){
        Log.d(TAG,"isEmpty");
        SQLiteDatabase db = this.getWritableDatabase();

      //  boolean empty=((db.rawQuery("SELECT * FROM " + tableName, null)).getCount()==0);
    //    if(close)
    //      db.close();
        return  true;
    }

    public void mashTable(String tableName){

        SQLiteDatabase db = this.getWritableDatabase();
        Log.d(TAG,"mashing table: "+tableName);
        db.execSQL("DROP TABLE "+tableName);
    }

    public void close(){
        SQLiteDatabase db = this.getWritableDatabase();
        if(db!=null)
        db.close();
    }

    public boolean savePOIGroup(POIGroup poiGroup){
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }
        SQLiteDatabase db = this.getWritableDatabase();
        boolean error=false;

        ContentValues cv=new ContentValues();
        cv.put(NAME,poiGroup.getName());
        cv.put(ITEM_ID,poiGroup.getId());
        cv.put(STATUS, String.valueOf(poiGroup.isSelect()));

        //Log.d(TAG,"name: "+poiGroup.getName()+" id: "+poiGroup.getId()+" status: "+poiGroup.isSelect());
        db.insert(TBL_POI_GROUP, null, cv);

        db.beginTransaction();
        try {

            List<POI> poiList=poiGroup.getPoiList();
            POI poi;
            //Log.d(TAG,"poiList: "+poiList);
            if(poiList!=null) {
                for (int a = 0; a < poiList.size(); a++) {
                    cv=new ContentValues();
                    poi=poiList.get(a);
                    cv.put(ACCURACY,poi.getAccuracy());
                    cv.put(LONGITUDE,poi.getLon());
                    cv.put(LATITUDE,poi.getLat());
                    cv.put(ITEM_ID,poiGroup.getId());
                    cv.put(ALTITUDE,poi.getAlt());
                    cv.put(NAME,poi.getName());

                    db.insert(TBL_POIS, null, cv);
                }
            }

            db.setTransactionSuccessful();

        } finally {
            db.endTransaction();
        }

           selectPOIGroup(poiGroup);

        db.close();
    return error;
    }

    public boolean updatePOIGroup(POIGroup poiGroup){
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }
        SQLiteDatabase db = this.getWritableDatabase();
        boolean error=false;
        String item_id=poiGroup.getId();

        ContentValues cv=new ContentValues();
        cv.put(NAME,poiGroup.getName());
        cv.put(ITEM_ID,item_id);
        cv.put(STATUS,  String.valueOf(poiGroup.isSelect()));

        delete(TBL_POIS,"WHERE "+ITEM_ID+"='"+item_id+"'");//removing all previous pois related to this group

        while (!db.isOpen()) {
              db = mContext.openOrCreateDatabase(DB_NAME, Context.MODE_PRIVATE, null);
        }

        db.update(TBL_POI_GROUP,cv,ITEM_ID+"='"+item_id+"'",null);

        db.beginTransaction();
        try {

            List<POI> poiList=poiGroup.getPoiList();
            POI poi;
            if(poiList!=null) {
                for (int a = 0; a < poiList.size(); a++) {
                    cv=new ContentValues();
                    poi=poiList.get(a);
                    cv.put(ACCURACY,poi.getAccuracy());
                    cv.put(LONGITUDE,poi.getLon());
                    cv.put(LATITUDE,poi.getLat());
                    cv.put(ITEM_ID,item_id);
                    cv.put(ALTITUDE,poi.getAlt());
                    cv.put(NAME,poi.getName());

                    db.insert(TBL_POIS, null, cv);
                }
            }

            db.setTransactionSuccessful();

        } finally {
            db.endTransaction();
        }

        db.close();
        return error;
    }

    public boolean selectPOIGroup(POIGroup poiGroup){
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }
        SQLiteDatabase db = this.getWritableDatabase();
        boolean error=false;
        String item_id=poiGroup.getId();

        ContentValues cv=new ContentValues();
        cv.put(STATUS,String.valueOf(false));

        db.update(TBL_POI_GROUP,cv,null,null);

        Cursor pointerGroup= db.rawQuery("SELECT * FROM " + TBL_POI_GROUP, null);
        pointerGroup.moveToFirst();
        /*while (!pointerGroup.isAfterLast()){
            Log.d(TAG,"b4 status: "+pointerGroup.getString(pointerGroup.getColumnIndex(STATUS)));
            pointerGroup.moveToNext();
        }*/

        cv.put(STATUS, String.valueOf(poiGroup.isSelect()));

        db.update(TBL_POI_GROUP,cv,ITEM_ID+"='"+item_id+"'",null);
        pointerGroup= db.rawQuery("SELECT * FROM " + TBL_POI_GROUP, null);
        pointerGroup.moveToFirst();

        /*while (!pointerGroup.isAfterLast()){
            Log.d(TAG,"after status: "+pointerGroup.getString(pointerGroup.getColumnIndex(STATUS)));
            pointerGroup.moveToNext();
        }*/
        db.close();
        return error;
    }

    public boolean removePOIGGroup(POIGroup poiGroup){
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }

        SQLiteDatabase db = this.getWritableDatabase();
        delete(DatabaseHelper.TBL_POI_GROUP,"WHERE "+DatabaseHelper.ITEM_ID+"='"+poiGroup.getId()+"'");
        db.close();

        return true;
    }
    public ArrayList<POIGroup> getPOIGroupList(){
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<POIGroup> groupList=new ArrayList<>();

        Cursor pointerGroup= db.rawQuery("SELECT * FROM " + TBL_POI_GROUP, null);
        if(pointerGroup.getCount()>0) {
            pointerGroup.moveToFirst();

            db.beginTransaction();
            try {
                while (!pointerGroup.isAfterLast()) {
                    POIGroup poiGroup=new POIGroup();
                    poiGroup.setPoiList(new ArrayList<POI>());

                    String item_id = pointerGroup.getString(pointerGroup.getColumnIndex(ITEM_ID));
                    poiGroup.setId(item_id);
                    poiGroup.setSelect(Boolean.parseBoolean(pointerGroup.getString(pointerGroup.getColumnIndex(STATUS))));
                    poiGroup.setName(pointerGroup.getString(pointerGroup.getColumnIndex(NAME)));

                    Log.d(TAG,"group name: "+poiGroup.getName());

                    Cursor pointerPOI = db.rawQuery("SELECT * FROM " + TBL_POIS + " WHERE " + ITEM_ID + "='" + item_id + "'", null);
                    pointerPOI.moveToFirst();

                    while (!pointerPOI.isAfterLast()) {
                        POI poi = new POI();
                        poi.setName(pointerPOI.getString(pointerPOI.getColumnIndex(NAME)));
                        poi.setAccuracy(pointerPOI.getString(pointerPOI.getColumnIndex(ACCURACY)));
                        poi.setLon(pointerPOI.getString(pointerPOI.getColumnIndex(LONGITUDE)));
                        poi.setLat(pointerPOI.getString(pointerPOI.getColumnIndex(LATITUDE)));
                        poi.setAlt(pointerPOI.getString(pointerPOI.getColumnIndex(ALTITUDE)));

                        Log.d(TAG,"groupList poi name: "+poi.getName());
                        poiGroup.getPoiList().add(poi);
                        pointerPOI.moveToNext();
                    }
                    groupList.add(poiGroup);
                    pointerGroup.moveToNext();
                }
                db.setTransactionSuccessful();

            } finally {
                db.endTransaction();
            }
          }
        db.close();

        return groupList;
    }

    public POIGroup getPOIGroup(){
        SQLiteDatabase db = this.getWritableDatabase();
        POIGroup poiGroup=new POIGroup();
        Cursor pointerGroup= db.rawQuery("SELECT * FROM " + TBL_POI_GROUP+" WHERE "+STATUS+"='true'", null);
        Log.d(TAG,"pointerGroupSize: "+pointerGroup.getCount());
        if(pointerGroup.getCount()>0) {
            pointerGroup.moveToFirst();
            db.beginTransaction();
            try {
                    String item_id = pointerGroup.getString(pointerGroup.getColumnIndex(ITEM_ID));
                    poiGroup.setId(item_id);
                    poiGroup.setSelect(Boolean.parseBoolean(pointerGroup.getString(pointerGroup.getColumnIndex(STATUS))));
                    poiGroup.setName(pointerGroup.getString(pointerGroup.getColumnIndex(NAME)));
                   Log.d(TAG,"the status: "+pointerGroup.getString(pointerGroup.getColumnIndex(STATUS)));

                Cursor pointerPOI = db.rawQuery("SELECT * FROM " + TBL_POIS + " WHERE " + ITEM_ID + "='" + item_id + "'", null);
                    pointerPOI.moveToFirst();

                    while (!pointerPOI.isAfterLast()) {
                        POI poi = new POI();
                        poi.setName(pointerPOI.getString(pointerPOI.getColumnIndex(NAME)));
                        poi.setAccuracy(pointerPOI.getString(pointerPOI.getColumnIndex(ACCURACY)));
                        poi.setLon(pointerPOI.getString(pointerPOI.getColumnIndex(LONGITUDE)));
                        poi.setLat(pointerPOI.getString(pointerPOI.getColumnIndex(LATITUDE)));
                        poi.setAlt(pointerPOI.getString(pointerPOI.getColumnIndex(ALTITUDE)));

                        Log.d(TAG,"seleted Group poi name: "+poi.getName());

                        poiGroup.getPoiList().add(poi);
                        pointerPOI.moveToNext();
                    }

                db.setTransactionSuccessful();

            } finally {
                db.endTransaction();
            }
        }

        db.close();
        return poiGroup;
    }

    public String createTransactionID(){
        String item_id=null;
     try {
        item_id= UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    } catch (Exception e) {
        e.printStackTrace();
    }
        Log.d(TAG,"uuid: "+item_id);
        return item_id;
    }

    public boolean updateFormCode(Form form){
        ContentValues cv=new ContentValues();
        cv.put(ACCESS_CODE,form.getAccess_code());
        SQLiteDatabase db = this.getWritableDatabase();
        if(exists(TBL_FORMS,form.getId())) {
            db.update(TBL_FORMS, cv, ITEM_ID + "='" + form.getId() + "'", null);
            return true;
        }

        return false;
    }

    public ArrayList<InstructionScout> getScoutList(int mode){

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<InstructionScout> list=new ArrayList<>();
        InstructionScout instructionScout;
        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_SCOUT+" " +
                "WHERE "+STATUS+"="+InstructionScout.STATE_UNPROCCESSED+" AND "+TYPE+"="+mode, null);

        pointer.moveToFirst();
        while (!pointer.isAfterLast()){
            instructionScout= getItem(pointer);

            list.add(instructionScout);
            pointer.moveToNext();
        }
        Collections.reverse(list);
        db.close();
        return list;
    }

    public ArrayList<InstructionScout> getScoutList(String section_id, int mode){

        SQLiteDatabase db = this.getWritableDatabase();

        ArrayList<InstructionScout> list=new ArrayList<>();
        InstructionScout instructionScout;
        Cursor pointer=null;
                if(mode== ChatbotAdapterRV.MODE_MAP) {
                pointer=db.rawQuery("SELECT * FROM " + TBL_SCOUT + " WHERE " +
                            "" + SCOUT_ID + "='" + section_id + "' AND " + TYPE + "=" + mode
                        +" AND "+STATUS+"="+InstructionScout.STATE_UNPROCCESSED, null);
                }
                else {
                    pointer=db.rawQuery("SELECT * FROM " + TBL_SCOUT + " WHERE " +
                            "" + SCOUT_ID + "='" + section_id + "' AND " + TYPE + "=" + mode, null);
                }
        pointer.moveToFirst();
        while (!pointer.isAfterLast()){
            instructionScout= getItem(pointer);

            list.add(instructionScout);
            pointer.moveToNext();
        }
        Collections.reverse(list);
        db.close();

        return list;
    }

    public ArrayList<InstructionScout> getScoutListAllStates(String section_id, int mode){

        SQLiteDatabase db = this.getWritableDatabase();

        ArrayList<InstructionScout> list=new ArrayList<>();
        InstructionScout instructionScout;
        Cursor pointer=null;
        if(mode== ChatbotAdapterRV.MODE_MAP) {
            pointer=db.rawQuery("SELECT * FROM " + TBL_SCOUT + " WHERE " +
                    "" + SCOUT_ID + "='" + section_id + "' AND " + TYPE + "=" + mode, null);
        }
        else {
            pointer=db.rawQuery("SELECT * FROM " + TBL_SCOUT + " WHERE " +
                    "" + SCOUT_ID + "='" + section_id + "' AND " + TYPE + "=" + mode, null);
        }
        pointer.moveToFirst();
        while (!pointer.isAfterLast()){
            instructionScout= getItem(pointer);

            list.add(instructionScout);
            pointer.moveToNext();
        }
        Collections.reverse(list);
        db.close();

        return list;
    }

    public ArrayList<String> getScoutIDList(int mode){

        SQLiteDatabase db = this.getWritableDatabase();

        ArrayList<String> list=new ArrayList<>();
        Cursor pointer= db.rawQuery("SELECT DISTINCT "+SCOUT_ID+" FROM " + TBL_SCOUT+" " +
                "WHERE "+TYPE+"="+mode, null);

        pointer.moveToFirst();
        while (!pointer.isAfterLast()){
            String header=pointer.getString(pointer.getColumnIndex(SCOUT_ID));
            if(mode==ChatbotAdapterRV.MODE_MAP){
               /* List<InstructionScout> allItems=getScoutListAllStates(header, ChatbotAdapterRV.MODE_MAP);
                if(allItems!=null&&allItems.size()>2)*/
                    list.add(header);
            }
            else {
                list.add(header);
            }
            pointer.moveToNext();
        }
        Log.d(TAG,"scout ids size: "+list.size());
        Collections.reverse(list);
        db.close();
        return list;
    }


    public ArrayList<String> getScoutIDList(String form_id,int mode){

        SQLiteDatabase db = this.getWritableDatabase();

        ArrayList<String> list=new ArrayList<>();

        Cursor pointer= db.rawQuery("SELECT DISTINCT "+SCOUT_ID+" FROM " + TBL_SCOUT+" " +
                "WHERE "+TYPE+"="+mode, null);

        pointer.moveToFirst();
        while (!pointer.isAfterLast()){
            String header=pointer.getString(pointer.getColumnIndex(SCOUT_ID));
            if(header!=null&&header.contains(form_id)) {
                List<InstructionScout> allItems=getScoutListAllStates(header, ChatbotAdapterRV.MODE_MAP);
                //if(allItems!=null&&allItems.size()>2)
                        list.add(header);
            }
            pointer.moveToNext();
        }
        Log.d(TAG,"scout ids size: "+list.size());
        Collections.reverse(list);
        db.close();
        return list;
    }

    public FormFarmMappingResponse buildFormMappingJSON(Context context, String form_id) throws JSONException {
        Log.d(TAG,"buildFormMappingJSON");
        Log.d(TAG,"form id: "+form_id);
        JSONArray baseArray=new JSONArray();
        ArrayList<String> headers=getScoutIDList(form_id,ChatbotAdapterRV.MODE_MAP);
        Log.d(TAG,"headers: "+(headers==null? "null":headers.size()));
        for(String header: headers){
            JSONArray pointsArray=new JSONArray();
            ArrayList<String>pois=new ArrayList<>();
            JSONObject mapObject=new JSONObject();
            List<InstructionScout> getAllItems=getScoutListAllStates(header, ChatbotAdapterRV.MODE_MAP);
            if(getAllItems!=null)
            Log.d(TAG,"AllItems: "+getAllItems.size());
            for (InstructionScout instructionScout : getAllItems) {
                if(instructionScout!=null) {
                    String message = instructionScout.getMessage();
                    Log.d(TAG,"Message: "+message);
                    if (message != null && !message.trim().isEmpty()) {
                        POI poi=CreateForm.getInstance(context).resolvePOI(message);
                        Log.d(TAG,"Poi: "+poi);
                        if(poi!=null) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("latitude", poi.getLat());
                            jsonObject.put("longitude", poi.getLon());
                            jsonObject.put("accuracy", poi.getAccuracy());

                            pointsArray.put(jsonObject);
                            pois.add(message);
                        }
                    }
                    mapObject.put("area",MyApplication.getArea(CreateForm.getInstance(context).buildPOI_Mapping(pois)));
                    mapObject.put("perimeter",MyApplication.getPerimeter(CreateForm.getInstance(context).buildPOI_Mapping(pois)));
                    mapObject.put("coordinates",pointsArray);
                }

            }

            baseArray.put(mapObject);
        }

        Log.d(TAG,"baseArray: "+baseArray);
        FormFarmMappingResponse response=new FormFarmMappingResponse();
        response.jsonArray=baseArray;
        response.headers=headers;

        return response;
    }

    public boolean removeScoutListItem(String section_id, int mode){
       Log.d(TAG,"removeScoutItem id: "+section_id);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor pointer=null;
        if(mode== ChatbotAdapterRV.MODE_MAP&&section_id!=null) {

            db.execSQL("DELETE FROM " + TBL_SCOUT + " WHERE " +
                    "" + SCOUT_ID + "='" + section_id + "' AND " + TYPE + "=" + mode);

            db.execSQL("DELETE FROM " + TBL_MAPPING + " WHERE " +
                    "" + ITEM_ID + "='" + section_id + "'");

        }

        db.close();

        return true;
    }


    public InstructionScout getInstructScout(String item_id, int mode){
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }

        SQLiteDatabase db = this.getWritableDatabase();

        InstructionScout instructionScout=null;
        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_SCOUT+" " +
                "WHERE "+ITEM_ID+"='"+item_id+"' AND "+TYPE+"="+mode, null);

        pointer.moveToFirst();
        while (!pointer.isAfterLast()){
            instructionScout= getItem(pointer);

            pointer.moveToNext();
        }
        db.close();
        return  instructionScout;
    }

    private InstructionScout getItem(Cursor pointer){

        InstructionScout instructionScout= new InstructionScout();

        if(pointer!=null) {
            instructionScout.setMessage(pointer.getString(pointer.getColumnIndex(MESSAGE)));
            instructionScout.setId(pointer.getString(pointer.getColumnIndex(ITEM_ID)));
            instructionScout.setStatus(pointer.getInt(pointer.getColumnIndex(STATUS)));
            instructionScout.setPlant_number(pointer.getInt(pointer.getColumnIndex(PLANT_NUMBER)));
            instructionScout.setPoint_number(pointer.getInt(pointer.getColumnIndex(POINT_NUMBER)));

            String image_path = pointer.getString(pointer.getColumnIndex(IMAGE_PATH));
            Image image = new Image();
            image.setUri(image_path != null && !image_path.trim().isEmpty() ?
                    Uri.parse(image_path) : null);
            instructionScout.setImage(image);

            instructionScout.setAction(pointer.getInt(pointer.getColumnIndex(TYPE)));
        }

        return instructionScout;
    }

    public boolean addScoutItem(String item_id, int state, String message,int mode){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(STATUS,state);
        cv.put(MESSAGE,message);
        cv.put(TYPE,mode);

        Log.d(TAG,"status: "+state+" date: "+message+" exists? "+exists(TBL_SCOUT,item_id));

        if(exists(TBL_SCOUT,item_id))
            db.update(TBL_SCOUT,cv,ITEM_ID+"='"+item_id+"'",null);
        else
            db.insert(TBL_SCOUT,null,cv);

        db.close();
        return true;
    }

    public boolean addScoutItems(ArrayList<InstructionScout> instructionScouts,int mode){

        Log.d(TAG,"save bulk addScoutItems");
        SQLiteDatabase db = this.getWritableDatabase();

        for(InstructionScout instructionScout:instructionScouts) {
            Log.d(TAG,"save bulk instructionScout: "+instructionScout);
            if(instructionScout!=null) {
                ContentValues cv = new ContentValues();
                cv.put(STATUS, instructionScout.getStatus());
                cv.put(MESSAGE, instructionScout.getMessage());
                cv.put(TYPE, mode);

                 Log.d(TAG,"status: "+instructionScout.getStatus()
                         +" date: "+instructionScout.getMessage()+" exists? "
                         +exists(TBL_SCOUT,instructionScout.getId()));

                Log.d(TAG,instructionScout.getScout_id()+" save bulk exists: "+exists(TBL_SCOUT, instructionScout.getId()));
                 if (exists(TBL_SCOUT, instructionScout.getId()))
                    db.update(TBL_SCOUT, cv, ITEM_ID + "='" + instructionScout.getId() + "'", null);
                else
                    db.insert(TBL_SCOUT, null, cv);
            }
        }
        db.close();
        return true;
    }

    public boolean removeMapping(String scout_id){
      /*  SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TBL_SCOUT+" WHERE "+SCOUT_ID+"='"+scout_id+"'");
        if(db.isOpen())
            db.close();
*/
        return true;
    }

    public boolean addScoutItem(InstructionScout instructionScout,int mode){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(ITEM_ID,instructionScout.getId());

        if(instructionScout.getImage()!=null&&instructionScout.getImage().getUri()!=null)
            cv.put(IMAGE_PATH,instructionScout.getImage().getUri().getPath());

        cv.put(STATUS,instructionScout.getStatus());
        cv.put(MESSAGE,instructionScout.getMessage());
        cv.put(SCOUT_ID,instructionScout.getScout_id());
        cv.put(TYPE,mode);
        cv.put(PLANT_NUMBER,instructionScout.getPlant_number());
        cv.put(POINT_NUMBER,instructionScout.getPoint_number());

        if(exists(TBL_SCOUT,instructionScout.getId()))
            db.update(TBL_SCOUT,cv,ITEM_ID+"='"+instructionScout.getId()+"'",null);
        else
            db.insert(TBL_SCOUT,null,cv);

        db.close();
        return true;
    }

    public boolean addMapping(MapModel map){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        Log.d(TAG,"addMapping id: "+map.id+" name: "+map.name+" type: "+map.type);
        cv.put(ITEM_ID,map.id);
        cv.put(NAME,map.name);
        cv.put(TYPE,map.type);

        if(exists(TBL_MAPPING,map.id))
            db.update(TBL_MAPPING,cv,ITEM_ID+"='"+map.id+"'",null);
        else
            db.insert(TBL_MAPPING,null,cv);

        db.close();
        return true;
    }

    public MapModel getMapping(String scout_id){

        Log.d(TAG,"getMapping scout_id: "+scout_id);
        MapModel mapModel=new MapModel();

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_MAPPING+" " +
                "WHERE "+ITEM_ID+"='"+scout_id+"'", null);

        Log.d(TAG,"getMapping pointer.size: "+pointer.getCount());
        if(pointer!=null&&pointer.getCount()>0) {
            pointer.moveToFirst();
            while (!pointer.isAfterLast()) {

                Log.d(TAG,"iteration " + pointer.getString(pointer.getColumnIndex(NAME)));
                mapModel.id = pointer.getString(pointer.getColumnIndex(ITEM_ID));
                mapModel.name = pointer.getString(pointer.getColumnIndex(NAME));
                mapModel.type = pointer.getString(pointer.getColumnIndex(TYPE));

                pointer.moveToNext();
            }
        }
        db.close();

        Log.d(TAG,"mapModel.id: "+mapModel.id+" mapModel.type: "+mapModel.type);

        return  mapModel;
    }


    public boolean addKB(KnowledgeBase knowledgeBase){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv=new ContentValues();

        cv.put(TITLE,knowledgeBase.getTitle());
        cv.put(CONTENT,knowledgeBase.getContent());
        cv.put(URL,knowledgeBase.getUrl());
        cv.put(AUDIO,knowledgeBase.getAudio());
        cv.put(TYPE,knowledgeBase.getMedia_type());
        cv.put(LANG,knowledgeBase.getLanguage());

        Log.d(TAG,"add audio: "+knowledgeBase.getAudio());

        db.insert(TBL_KB,null,cv);
        db.close();
        return true;
    }

    public ArrayList<KnowledgeBase> getKBs(String language){

        SQLiteDatabase db = this.getWritableDatabase();

        ArrayList<KnowledgeBase> list=new ArrayList<>();
        KnowledgeBase knowledgeBase;
        //String language=(new SettingsModel(mContext)).getLang();
        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_KB
                +" WHERE "+LANG+"='"+language+"'", null);

        Log.d(TAG,"lang: "+language+" count: "+pointer.getCount());
        pointer.moveToFirst();
        while (!pointer.isAfterLast()){
            knowledgeBase=new KnowledgeBase(
                    pointer.getInt(pointer.getColumnIndex(ID)),
                    pointer.getString(pointer.getColumnIndex(TITLE)),
                    pointer.getString(pointer.getColumnIndex(CONTENT)),
                    pointer.getString(pointer.getColumnIndex(URL)),
                    pointer.getString(pointer.getColumnIndex(AUDIO)),
                    pointer.getString(pointer.getColumnIndex(TYPE)),
                    language
            );

            //Log.d(TAG,"get content: "+knowledgeBase.getContent());
            list.add(knowledgeBase);
            pointer.moveToNext();
        }
        db.close();

        return list;
    }

    public boolean addMessage(Message message){
        Log.d(TAG,"add message: "+message.getId());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(ITEM_ID,message.getId());
        cv.put(MESSAGE,message.getText());
        cv.put(IMAGE_PATH,message.getImageUrl());
        cv.put(AUTHOR_ID,message.getUser().getId());
        cv.put(DATE,message.getCreatedAt().getTime());
        cv.put(STATUS,message.getStatus());
        cv.put(MSG_GROUP_ID,message.getGroup_id());
        //Log.d(TAG,"date: "+message.getCreatedAt().getTime());
        if(!exists(TBL_MESSAGES,message.getId()))
            db.insert(TBL_MESSAGES,null,cv);
        else
            db.update(TBL_MESSAGES,cv,ITEM_ID+"='"+message.getId()+"'",null);

        addAuthor(message.getUser(),db);

        db.close();
        return true;
    }

    public Message getMessage(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        Message message=new Message();
        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_MESSAGES+" WHERE "+ITEM_ID+"='"+id+"'", null);
        Log.d(TAG,"pointer size author: "+pointer.getCount());
        pointer.moveToFirst();
        if(pointer.getCount()>0){
            message.setText(pointer.getString(pointer.getColumnIndex(MESSAGE)));
            message.setId(pointer.getString(pointer.getColumnIndex(ITEM_ID)));
            Image image=new Image();
            image.setImage_url(pointer.getString(pointer.getColumnIndex(IMAGE_PATH)));
            message.setImage(image);
            message.setStatus(pointer.getString(pointer.getColumnIndex(STATUS)));
            message.setAuthor(getAuthor(pointer.getString(pointer.getColumnIndex(AUTHOR_ID)),db));
            message.setMillis(pointer.getLong(pointer.getColumnIndex(DATE)));
           // MessageContentType.Image image=new MessageContentType();
        }

        db.close();
        return message;
    }

    public ArrayList<Message> getMessages(Context context){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Message> messages=new ArrayList<>();
        Message message;
        int group_id= Chat.getActiveRoom_id(context);
        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_MESSAGES+" WHERE "+ MSG_GROUP_ID +"='"+group_id+"' ORDER BY "+ID+" DESC", null);
        pointer.moveToFirst();
        if(pointer.getCount()>0){
            while(!pointer.isAfterLast()) {
                message=new Message();
                message.setText(pointer.getString(pointer.getColumnIndex(MESSAGE)));
                message.setId(pointer.getString(pointer.getColumnIndex(ITEM_ID)));
                Image image=new Image();
                image.setImage_url(pointer.getString(pointer.getColumnIndex(IMAGE_PATH)));
                message.setImage(image);
                message.setStatus(pointer.getString(pointer.getColumnIndex(STATUS)));
                long millisecs=pointer.getLong(pointer.getColumnIndex(DATE));
                Log.d(TAG,"millisecs: "+millisecs);
                 message.setCreatedAt(millisecs!=0?new Date(millisecs):new Date());

                 message.setAuthor(getAuthor(pointer.getString(pointer.getColumnIndex(AUTHOR_ID)), db));
                message.setMillis(pointer.getLong(pointer.getColumnIndex(DATE)));

                messages.add(message);
                // MessageContentType.Image image=new MessageContentType();
                pointer.moveToNext();
            }
        }

        db.close();
        return messages;
    }

    public boolean updateMessage(Message message){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv=new ContentValues();

        cv.put(MESSAGE,message.getText());
        cv.put(IMAGE_PATH,message.getImageUrl());
        cv.put(AUTHOR_ID,message.getUser().getId());
        cv.put(DATE,message.getCreatedAt().getTime());
        cv.put(STATUS,message.getStatus());

        db.update(TBL_MESSAGES,cv,ITEM_ID+"='"+message.getId()+"'",null);

        db.close();
        return true;
    }

    public boolean addAuthor(Author author, SQLiteDatabase db){
        boolean dbControl=false;

        if(db==null) {
            db = this.getWritableDatabase();
            dbControl=true;
        }

        ContentValues cv=new ContentValues();

        cv.put(ITEM_ID,author.getId());
        cv.put(NAME,author.getName());
        cv.put(IMAGE_PATH,author.getAvatar());

        if(!exists(TBL_AUTHORS,author.getId()))
            db.insert(TBL_AUTHORS,null,cv);
        else
            db.update(TBL_AUTHORS,cv,ITEM_ID+"='"+author.getId()+"'",null);

        if(dbControl)
            db.close();

        return true;
    }

    public Author getAuthor(String id,SQLiteDatabase db){
        boolean dbControl=false;
        if(db==null){
            db = this.getWritableDatabase();
            dbControl=true ;
        }
        Author author=new Author();
        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_AUTHORS+" " +
                "WHERE "+ITEM_ID+"='"+id+"'", null);
        pointer.moveToFirst();
        Log.d(TAG,"pointer size author: "+pointer.getCount());
        if(pointer.getCount()>0) {
            author.setName(pointer.getString(pointer.getColumnIndex(NAME)));
            author.setId(pointer.getString(pointer.getColumnIndex(ITEM_ID)));
            author.setAvatar(pointer.getString(pointer.getColumnIndex(IMAGE_PATH)));
        }

        if(dbControl)
            db.close();

        return author;
    }


    public boolean addProduct(Product product){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv=new ContentValues();
        cv.put(ITEM_ID,product.id);
        cv.put(NAME,product.name);
        cv.put(DESCRIPTION,product.description);
        cv.put(PRICE,product.price);
        cv.put(QUANTITY,product.quantity);
        cv.put(STATUS,product.status);
        cv.put(ORDER_CODE,product.order_number);
        cv.put(OWNER_NAME,product.owner_name);
        cv.put(MAX,product.max);

        addPOI(product.poi);

        if(!db.isOpen())
            db = this.getWritableDatabase();

        if(getProduct(product.id,Product.STATUS_CART)==null) {
            if(!db.isOpen())
                db = this.getWritableDatabase();

            List<Image> images=new ArrayList<>();
            Image image = new Image();
            image.setId(product.id);
            image.setImage_url(product.image);
            images.add(image);
            addImages(images,db);
            db.insert(TBL_PRODUCT, null, cv);
        }
        else {
        Log.d(TAG,"product update");
            if(!db.isOpen())
                db = this.getWritableDatabase();

            db.update(TBL_PRODUCT, cv, ITEM_ID + "='" + product.id + "' AND "+ORDER_CODE+" is null", null);
        }
        db.close();
        return true;
    }

    public ArrayList<Product> getProducts(int status){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Product> products=new ArrayList<>();
        Product product;
        Cursor pointer;
        if(status==-1)
            pointer= db.rawQuery("SELECT * FROM " + TBL_PRODUCT+" ORDER BY "+ID+" DESC", null);
        else
            pointer= db.rawQuery("SELECT * FROM " + TBL_PRODUCT+" WHERE "+ STATUS +"="+status+" ORDER BY "+ID+" DESC", null);

        Log.d(TAG,"pointer size: "+pointer.getCount());
        pointer.moveToFirst();
        if(pointer.getCount()>0){
            while(!pointer.isAfterLast()) {
                product=new Product();
                product.id=pointer.getString(pointer.getColumnIndex(ITEM_ID));
                product.name=pointer.getString(pointer.getColumnIndex(NAME));
                product.description=pointer.getString(pointer.getColumnIndex(DESCRIPTION));
                product.price=pointer.getLong(pointer.getColumnIndex(PRICE));
                product.quantity=pointer.getInt(pointer.getColumnIndex(QUANTITY));
                product.owner_name=pointer.getString(pointer.getColumnIndex(OWNER_NAME));
                product.status=pointer.getInt(pointer.getColumnIndex(STATUS));
                product.max=pointer.getInt(pointer.getColumnIndex(MAX));

                product.images=getImages(product.id);
                if(product.images!=null&&product.images.size()>0)
                    product.image=product.images.get(0).getImage_url();

                product.poi=getPOI(product.id);
                products.add(product);
                pointer.moveToNext();
            }
        }

        Log.d(TAG,"pointer size: "+products.size());
        db.close();
        return products;
    }

    public ArrayList<Product> getProducts(String orderCode){
        ArrayList<Product> products=new ArrayList<>();
        if(orderCode!=null) {
            SQLiteDatabase db = this.getWritableDatabase();
            Product product;
            Cursor pointer;

            pointer = db.rawQuery("SELECT * FROM " + TBL_PRODUCT + " WHERE " + ORDER_CODE + "='" + orderCode + "' ORDER BY " + ID + " DESC", null);

            Log.d(TAG, "pointer size: " + pointer.getCount());
            pointer.moveToFirst();
            if (pointer.getCount() > 0) {
                while (!pointer.isAfterLast()) {
                    product = new Product();
                    product.id = pointer.getString(pointer.getColumnIndex(ITEM_ID));
                    product.name = pointer.getString(pointer.getColumnIndex(NAME));
                    product.description = pointer.getString(pointer.getColumnIndex(DESCRIPTION));
                    product.price = pointer.getLong(pointer.getColumnIndex(PRICE));
                    product.quantity = pointer.getInt(pointer.getColumnIndex(QUANTITY));
                    product.owner_name = pointer.getString(pointer.getColumnIndex(OWNER_NAME));
                    product.status = pointer.getInt(pointer.getColumnIndex(STATUS));
                    product.max=pointer.getInt(pointer.getColumnIndex(MAX));

                    product.images = getImages(product.id);
                    if (product.images != null && product.images.size() > 0)
                        product.image = product.images.get(0).getImage_url();

                    product.poi = getPOI(product.id);
                    products.add(product);
                    pointer.moveToNext();
                }
            }

            Log.d(TAG, "pointer size: " + products.size());
            db.close();
        }
        return products;
    }

    public Product getProduct(String  item_id){
        SQLiteDatabase db = this.getWritableDatabase();
        Product product=null;
        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_PRODUCT+" WHERE "+ ITEM_ID +"='"+item_id+"'", null);

        pointer.moveToFirst();
        if(pointer.getCount()>0){
            while(!pointer.isAfterLast()) {
                product=new Product();
                product.id=pointer.getString(pointer.getColumnIndex(ITEM_ID));
                product.name=pointer.getString(pointer.getColumnIndex(NAME));
                product.description=pointer.getString(pointer.getColumnIndex(DESCRIPTION));
                product.price=pointer.getLong(pointer.getColumnIndex(PRICE));
                product.quantity=pointer.getInt(pointer.getColumnIndex(QUANTITY));
                product.owner_name=pointer.getString(pointer.getColumnIndex(OWNER_NAME));
                product.status=pointer.getInt(pointer.getColumnIndex(STATUS));
                product.max=pointer.getInt(pointer.getColumnIndex(MAX));

                product.images=getImages(product.id);
                if(product.images!=null&&product.images.size()>0)
                    product.image=product.images.get(0).getImage_url();

                //product.images=getImages(product.id);
                product.poi=getPOI(product.id);

                pointer.moveToNext();
            }
        }

        db.close();
        return product;
    }

    public Product getProduct(String  item_id,int status){
        SQLiteDatabase db = this.getWritableDatabase();
        Product product=null;
        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_PRODUCT+" WHERE "+ ITEM_ID +"='"+item_id+"' AND "+STATUS+"="+status, null);
        pointer.moveToFirst();
        if(pointer.getCount()>0){
            while(!pointer.isAfterLast()) {
                product=new Product();
                product.id=pointer.getString(pointer.getColumnIndex(ITEM_ID));
                product.name=pointer.getString(pointer.getColumnIndex(NAME));
                product.description=pointer.getString(pointer.getColumnIndex(DESCRIPTION));
                product.price=pointer.getLong(pointer.getColumnIndex(PRICE));
                product.quantity=pointer.getInt(pointer.getColumnIndex(QUANTITY));
                product.owner_name=pointer.getString(pointer.getColumnIndex(OWNER_NAME));
                product.status=pointer.getInt(pointer.getColumnIndex(STATUS));
                product.max=pointer.getInt(pointer.getColumnIndex(MAX));

                product.images=getImages(product.id);
                if(product.images!=null&&product.images.size()>0)
                    product.image=product.images.get(0).getImage_url();

                //product.images=getImages(product.id);
                product.poi=getPOI(product.id);

                pointer.moveToNext();
            }
        }

        db.close();
        return product;
    }

    public boolean removeProduct(Product product){
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("DELETE FROM "+TBL_PRODUCT+" WHERE "+ITEM_ID+"='"+product.id+"' AND "+STATUS+"="+product.status);
            if(db.isOpen())
                db.close();

            return true;
    }

    public ArrayList<Image> getImages(String item_id){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Image> images=new ArrayList<>();
        Image image;
        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_IMAGE+" WHERE "+ ITEM_ID +"='"+item_id+"' ORDER BY "+ID+" DESC", null);

        pointer.moveToFirst();
        if(pointer.getCount()>0){
            while(!pointer.isAfterLast()) {
                image=new Image();
                image.setImage_url(pointer.getString(pointer.getColumnIndex(URL)));
                images.add(image);
                pointer.moveToNext();
            }
        }

        db.close();
        return images;
    }

    public boolean addImages(List<Image> images, SQLiteDatabase db){
        boolean dbControl=false;
        if(db==null) {
            db = this.getWritableDatabase();
            dbControl=true;
        }

        ContentValues cv=new ContentValues();
        for(int a=0;a<images.size();a++) {
         Image image=images.get(a);
            cv.put(ITEM_ID, image.getId());
            cv.put(URL, image.getImage_url());
            Log.d(TAG,"addImages image url: "+image.getImage_url());
            db.insert(TBL_IMAGE, null, cv);
        }
        if(dbControl)
            db.close();

        return true;
    }

    public boolean addReceipt(PaymentReceipt receipt){
        Log.d(TAG,"add message: "+receipt.id);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(ITEM_ID,receipt.order_number);
        cv.put(AMOUNT,receipt.amount_paid);
        cv.put(DATE,receipt.date);
        cv.put(STATUS,receipt.order_status);

        if(!exists(TBL_RECEIPT,receipt.order_number))
            db.insert(TBL_RECEIPT,null,cv);
        else
            db.update(TBL_RECEIPT,cv,ITEM_ID+"='"+receipt.order_number+"'",null);

        db.close();
        return true;
    }

    public ArrayList<PaymentReceipt> getReceipts(Context context){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<PaymentReceipt> receipts=new ArrayList<>();
        PaymentReceipt receipt;
        int group_id= Chat.getActiveRoom_id(context);
        Cursor pointer= db.rawQuery("SELECT * FROM " + TBL_RECEIPT+" ORDER BY "+ID+" DESC", null);
        pointer.moveToFirst();
        if(pointer.getCount()>0){
            while(!pointer.isAfterLast()) {
                receipt=new PaymentReceipt();
                receipt.order_number=pointer.getString(pointer.getColumnIndex(ITEM_ID));
                receipt.date=pointer.getString(pointer.getColumnIndex(DATE));
                receipt.amount_paid=pointer.getString(pointer.getColumnIndex(AMOUNT));
                receipt.order_status=pointer.getString(pointer.getColumnIndex(STATUS));
                receipt.id=pointer.getString(pointer.getColumnIndex(ITEM_ID));

                receipts.add(receipt);
                // MessageContentType.Image image=new MessageContentType();
                pointer.moveToNext();
            }
        }

        db.close();
        return receipts;
    }

    public POI getPOI(String item_id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor pointerPOI = db.rawQuery("SELECT * FROM " + TBL_POIS + " WHERE " + ITEM_ID + "='" + item_id + "'", null);
        pointerPOI.moveToFirst();
        POI poi = new POI();
        while (!pointerPOI.isAfterLast()) {
            poi.id_str=pointerPOI.getString(pointerPOI.getColumnIndex(ITEM_ID));
            poi.setName(pointerPOI.getString(pointerPOI.getColumnIndex(NAME)));
            poi.setAccuracy(pointerPOI.getString(pointerPOI.getColumnIndex(ACCURACY)));
            poi.setLon(pointerPOI.getString(pointerPOI.getColumnIndex(LONGITUDE)));
            poi.setLat(pointerPOI.getString(pointerPOI.getColumnIndex(LATITUDE)));
            poi.setAlt(pointerPOI.getString(pointerPOI.getColumnIndex(ALTITUDE)));

            pointerPOI.moveToNext();
        }
        db.close();
        return poi;
    }

    public boolean addPOI(POI poi){
        if(poi!=null) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(ITEM_ID, poi.getId());
            cv.put(ACCURACY, poi.getAccuracy());
            cv.put(LONGITUDE, poi.getLon());
            cv.put(LATITUDE, poi.getLat());
            cv.put(NAME, poi.getName());


            if (!exists(TBL_POIS, poi.id_str))
                db.insert(TBL_POIS, null, cv);
            else
                db.update(TBL_POIS, cv, ITEM_ID + "='" + poi.id_str + "'", null);

            db.close();
            return true;
        }
        else
            return false;
    }

    public boolean dropTable(String tableName){
//        while (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DB_NAME, mContext.MODE_PRIVATE, null);
//        }
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE "+tableName);

        db.close();
        return true;
    }
}
