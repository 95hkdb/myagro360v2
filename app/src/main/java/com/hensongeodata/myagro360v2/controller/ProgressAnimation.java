package com.hensongeodata.myagro360v2.controller;

import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ProgressBar;

import com.hensongeodata.myagro360v2.Interface.PercentageListener;

/**
 * Created by user1 on 6/19/2018.
 */

public class ProgressAnimation extends Animation {
    private ProgressBar progressBar;
    private float from;
    private float  to;
    private PercentageListener percentageListener;

    public ProgressAnimation(ProgressBar progressBar, float from, float to) {
        super();
        this.progressBar = progressBar;
        this.from = from;
        this.to = to;
    }

    public void setPercentageListener(PercentageListener percentageListener) {
        this.percentageListener = percentageListener;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        float value = from + (to - from) * interpolatedTime;
        progressBar.setProgress((int) value);
        percentageListener.onUpdate(((int)value)/100);
    }

}
