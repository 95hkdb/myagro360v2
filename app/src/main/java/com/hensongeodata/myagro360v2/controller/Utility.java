 package com.hensongeodata.myagro360v2.controller;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;

 public class Utility {
     public static final int MY_PERMISSIONS_RECORD_AUDIO = 124;
     public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
     public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 122;
     public static final int MY_PERMISSIONS_REQUEST_CAMERA = 121;
     public static final int MY_PERMISSIONS_CHECK_ALL = 125;
     private static ArrayList<String> permissions;

     @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
     public static boolean checkPermission(final Context context) {
         permissions=new ArrayList<>();
         int currentAPIVersion = Build.VERSION.SDK_INT;
         if (currentAPIVersion >= Build.VERSION_CODES.M) {

             //Check for write permissions
             if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                 if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                     permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                 } else {
                     permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                 }

             }

             //Check for read permissions
             if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                 if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                     permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                 } else {
                     permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                 }
             }
                 //Check for write permissions
                 if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                     if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.CAMERA)) {
                         permissions.add(Manifest.permission.CAMERA);
                     } else {
                         permissions.add(Manifest.permission.CAMERA);
                     }
                 }
             //Check for write permissions
             if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                 if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                     permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
                 } else {
                     permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
                 }
             }
             //Check for write permissions
             if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                 if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_FINE_LOCATION)) {
                     permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
                 } else {
                     permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
                 }
             }
             if(permissions.size()!=0){
                 String[]permissionRequest=new String[permissions.size()];
                 for(int a=0;a<permissions.size();a++){
                     permissionRequest[a]=permissions.get(a);
                 }

                 ActivityCompat.requestPermissions((Activity) context, permissionRequest, MY_PERMISSIONS_CHECK_ALL);
                 return false;
             }
                 else {
                     return true;
                 }

             } else {
                 return true;
             }
         }

     }
