package com.hensongeodata.myagro360v2.controller;

import android.content.Context;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatRadioButton;
import android.view.View;
import android.widget.EditText;

/**
 * Created by user1 on 5/8/2018.
 */

public class MyRadio extends AppCompatRadioButton {
   EditText editText;
    public MyRadio(Context context) {
        super(context);
        editText=new EditText(context);
        editText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    setChecked(true);
            }
        });
    }

    public MyRadio(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyRadio(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setEditText(EditText editText){
        this.editText=editText;
    }

    public String etGetText() {
        return editText.getText().toString();
    }

    public void etSetText(String text){
        editText.setText(text);
    }

    public void etSetHint(String hint){
        editText.setHint(hint);
    }

    public void etEnabled(boolean enable){
        editText.setEnabled(enable);
    }

    public void etRequestFocus(){
        editText.requestFocus();
    }

    public void etClear(){
        editText.setText(null);
    }
}
