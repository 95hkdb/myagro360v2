package com.hensongeodata.myagro360v2.contract;

import com.hensongeodata.myagro360v2.BaseView;
import com.hensongeodata.myagro360v2.Interface.BasePresenter;

public interface ContractPayment {

      interface View extends BaseView<Presenter> {
            void showError();

            void showSuccess();
    }

      interface Presenter extends BasePresenter {
            void initializePayment();

            void onError();

            void onSuccess();
    }
}
