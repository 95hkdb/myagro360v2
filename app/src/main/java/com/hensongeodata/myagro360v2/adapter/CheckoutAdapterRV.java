package com.hensongeodata.myagro360v2.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.Interface.CheckoutListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 8/2/2016.
 */
public class CheckoutAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=CheckoutAdapterRV.class.getSimpleName();
    private List<Product> productList;
    private List<Product> filteredList;
    private Context mContext;
    private ProductHolder productHolder;
    public static Product productSelected;
    public CheckoutListener checkoutListener;

    public CheckoutAdapterRV(Context context, List<Product> items, CheckoutListener listener) {
        mContext=context;
        this.productList =items;
        filteredList=items;
        this.checkoutListener=listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.checkout_item, parent, false);
            return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
           final Product product = filteredList.get(position);
            productHolder =(ProductHolder)holder;

        productHolder.name.setText(product.name);
        productHolder.price.setText("GHc "+product.price);
        Picasso.get().load(
                (product.image))
                .resize(800, 800)
                .placeholder(R.drawable.blurred_image)
                .into(productHolder.image);


        productHolder.quantity.setText("Qty "+product.quantity);
        productHolder.ibAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add(holder.getAdapterPosition());
            }
        });

        productHolder.ibSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subtract(holder.getAdapterPosition());
            }
        });

        productHolder.ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeItem(view,holder.getAdapterPosition());
            }
        });
    }

    private void updateQuantity(int position, int quantity){
        Product product=filteredList.get(position);
        if(product.quantity<quantity)
            add(position);
        else if(product.quantity>quantity)
            subtract(position);
    }

    private void add(int position){
        Product product=filteredList.get(position);
        checkoutListener.add(position);
        notifyDataSetChanged();
    }


    private void subtract(int position){
        Product product=filteredList.get(position);
        checkoutListener.subtract(position);
        notifyDataSetChanged();
    }

    private void removeItem(View v,final int position){
        final Product product= filteredList.get(position);
        PopupMenu popup = new PopupMenu(mContext, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if(menuItem.getItemId()==R.id.action_delete){
                   checkoutListener.remove(position);
                    notifyDataSetChanged();
                    MyApplication.showToast(mContext,mContext.getResources().getString(R.string.deleting), Gravity.CENTER);
                }
                return true;
            }
        });
        popup.inflate(R.menu.cart_item_menu);
        popup.show();
    }

    public void search(String keyword){
        filteredList=new ArrayList<>();
        if(keyword!=null&&!keyword.trim().isEmpty()) {
            keyword = keyword.toLowerCase();
            keyword=keyword.trim();
        }
        if(keyword==null||keyword.isEmpty()){
            //reset filtered list to original list
            filteredList= productList;
        }
        else {
            for(int a = 0; a< productList.size(); a++){
                if(productList.get(a).name!=null&& productList.get(a).description!=null) {
                    if (productList.get(a).name.toLowerCase().contains(keyword)
                            || productList.get(a).description.toLowerCase().contains(keyword)) {
                        filteredList.add(productList.get(a));
                    }
                }
            }
        }

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    private class ProductHolder extends RecyclerView.ViewHolder {
        public View container;
        public ImageView image;
        public TextView name;
        public TextView price;
        public TextView quantity;

        public ImageButton ibAdd;
        public ImageButton ibSub;
        public ImageButton ibMenu;
        //public StepperTouch stepperTouch;

        public ProductHolder(View itemView) {
            super(itemView);
            container=itemView.findViewById(R.id.v_container);
              image = itemView.findViewById(R.id.iv_image);
              name = itemView.findViewById(R.id.tv_name);
              price = itemView.findViewById(R.id.tv_price);
              quantity = itemView.findViewById(R.id.tv_quantity);
            //stepperTouch = (StepperTouch) itemView.findViewById(R.id.stepper_touch);
              ibAdd=itemView.findViewById(R.id.ib_add);
            ibSub=itemView.findViewById(R.id.ib_minus);
            ibMenu=itemView.findViewById(R.id.ib_menu);
        }
    }

}