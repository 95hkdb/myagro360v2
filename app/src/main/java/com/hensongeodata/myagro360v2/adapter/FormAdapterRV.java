package com.hensongeodata.myagro360v2.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.SwipeDismissTouchListener;
import com.hensongeodata.myagro360v2.model.Form;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.view.FormDetail;
import com.hensongeodata.myagro360v2.view.FormList;

/**
 * Created by user1 on 8/2/2016.
 */
public class FormAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=FormAdapterRV.class.getSimpleName();
    private List<Form> formList;
    private List<Form> filteredList;
    private Context mContext;
    private FormHolder formHolder;

    public FormAdapterRV(Context context, List<Form> items) {
        mContext=context;
        this.formList =items;
        filteredList=items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.form_item, parent, false);
            return new FormHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
           final Form form = filteredList.get(position);
            formHolder =(FormHolder)holder;

        formHolder.name.setText(form.getTitle());
        formHolder.date.setText(form.getDescription());

        addSwipeDismiss(formHolder.background,holder.getAdapterPosition());
        formHolder.background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadForm(holder.getAdapterPosition());
            }
        });

        formHolder.bgInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDetails(holder.getAdapterPosition());
            }
        });

        formHolder.ibInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDetails(holder.getAdapterPosition());
            }
        });
    }

    private void loadForm(final int position){
        Form form= filteredList.get(position);

        Intent intent=new Intent(BroadcastCall.FORM_LIST);
        intent.setAction(BroadcastCall.FORM_LIST);
        intent.putExtra("formID",form.getId());
        intent.putExtra("action",FormList.ACTION_SELECT);
        mContext.sendBroadcast(intent);
    }


    private void showDetails(int position){
        Form form= filteredList.get(position);

        Intent intent=new Intent(mContext, FormDetail.class);
        intent.putExtra("form_id",form.getId());
        mContext.startActivity(intent);
    }


    private void addSwipeDismiss(View view, final int position){
        // Create a generic swipe-to-dismiss touch listener.
        view.setOnTouchListener(new SwipeDismissTouchListener(
                view,
                null,
                new SwipeDismissTouchListener.DismissCallbacks() {
                    @Override
                    public boolean canDismiss(Object token) {
                        return true;
                    }

                    @Override
                    public void onDismiss(View view, Object token) {
                        showPop(view,position);
                    }
                }));
        //dismissableContainer.addView(view);
    }

    private void showPop(View v,int position){
        final Form form= filteredList.get(position);

        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.confirm_dialog);
        ((TextView)dialog.findViewById(R.id.tv_caption)).setText(form.getTitle());
          final EditText etPassword = dialog.findViewById(R.id.et_password);
          Button btnCancel = dialog.findViewById(R.id.btn_negative);
          Button btnDelete = dialog.findViewById(R.id.btn_positive);

        btnCancel.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_grey));
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnDelete.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_red));
        btnDelete.setTextColor(mContext.getResources().getColor(R.color.primaryWhite));
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password=etPassword.getText().toString().trim();
                if(password.isEmpty()){
                    Toast.makeText(mContext,"Password was empty ",Toast.LENGTH_SHORT).show();
                }
                else if(password.equals((new User(mContext)).getPassword())){
                    deleteForm(v,form);
                }
                else{
                    Toast.makeText(mContext,"Password was incorrect",Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }
    private void deleteForm(View v, Form form){

        if((new DatabaseHelper(mContext)).deleteItem(DatabaseHelper.TBL_FORMS,form.getId())){
            v.setVisibility(View.GONE);
            Toast.makeText(mContext,form.getTitle()+" Deleted successfully.",Toast.LENGTH_SHORT).show();
        }
        else {
            v.setVisibility(View.VISIBLE);
            Toast.makeText(mContext,form.getTitle()+" Failed to delete.",Toast.LENGTH_SHORT).show();
        }
    }


    public void search(String keyword){
        filteredList=new ArrayList<>();
        if(keyword!=null&&!keyword.trim().isEmpty()) {
            keyword = keyword.toLowerCase();
            keyword=keyword.trim();
        }
        if(keyword==null||keyword.isEmpty()){
            //reset filtered list to original list
            filteredList= formList;
        }
        else {
            for(int a = 0; a< formList.size(); a++){
                if(formList.get(a).getTitle()!=null&& formList.get(a).getDescription()!=null) {
                    if (formList.get(a).getTitle().toLowerCase().contains(keyword)
                            || formList.get(a).getDescription().toLowerCase().contains(keyword)) {
                        filteredList.add(formList.get(a));
                    }
                }
            }
        }

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    private class FormHolder extends RecyclerView.ViewHolder {
        int index;
        public View background;
        public View container;
        public View bgInfo;
        public TextView name;
        public TextView date;
        public ImageButton ibInfo;


        public FormHolder(View itemView) {
            super(itemView);
            container=itemView.findViewById(R.id.container);
            background=itemView.findViewById(R.id.background);
            bgInfo =itemView.findViewById(R.id.bg_info);
              name = itemView.findViewById(R.id.tv_title);
              date = itemView.findViewById(R.id.tv_date);
              ibInfo = itemView.findViewById(R.id.ib_info);
             }
    }

}