package com.hensongeodata.myagro360v2.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Field;
import com.hensongeodata.myagro360v2.model.Intro;
import com.hensongeodata.myagro360v2.model.PreviewState;
import com.hensongeodata.myagro360v2.view.OrgList;

/**
 * Created by user1 on 8/2/2016.
 */
public class IntroAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=IntroAdapterRV.class.getSimpleName();
    private ArrayList<Object> items;
    private ArrayList<PreviewState> state;
    private Context mContext;
    private final int BASE_ACTION =1, MAIN_CONTENT =3;

    public IntroAdapterRV(Context context, ArrayList<Object> items) {
        this.items = items;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        RecyclerView.ViewHolder viewHolder;
        View vBaseAction;
        switch (viewType) {
            case MAIN_CONTENT:
                View vMainContent= inflater.inflate(R.layout.intro_main_content,parent, false);
                viewHolder = new MainContentVH(vMainContent);
                break;
            case BASE_ACTION:
                vBaseAction = inflater.inflate(R.layout.intro_base_action,parent, false);
                viewHolder = new BaseActionVH(vBaseAction);
                break;
            default:
                vBaseAction = inflater.inflate(R.layout.intro_base_action,parent, false);
                viewHolder = new BaseActionVH(vBaseAction);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        BaseActionVH baseActionVH;
        switch (viewHolder.getItemViewType()) {
            case BASE_ACTION:
                baseActionVH = (BaseActionVH) viewHolder;
                configureBaseAction(baseActionVH, position);
                break;
            case MAIN_CONTENT:
                MainContentVH mainContentVH = (MainContentVH) viewHolder;
                configureMainContent(mainContentVH, position);
                break;
            default:
                baseActionVH = (BaseActionVH) viewHolder;
                configureBaseAction(baseActionVH, position);
                break;
        }

    }

    private void configureBaseAction(final BaseActionVH baseActionVH, final int position) {
        baseActionVH.background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, OrgList.class));
            }
        });
    }

    private void configureMainContent(final MainContentVH contentHolder, final int position) {
        Intro intro= (Intro) items.get(position);
        contentHolder.tvHeader.setText(intro.getCaption());
        contentHolder.tvMessage.setText(intro.getMessage());
    }


    //Since there could be several answers, we loop through to display each answer one after the other
    private LinearLayout setAnswersView(int position){
        LinearLayout baseView=new LinearLayout(mContext);
        baseView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        baseView.setOrientation(LinearLayout.VERTICAL);

        Field field= (Field) items.get(position);
        ArrayList<String> answers=field.getAnswers();
        for(int a=0;a<answers.size();a++) {
            View answerView = (LayoutInflater.from(mContext)).inflate(R.layout.preview_answer_item, null);
            ((TextView)answerView.findViewById(R.id.tv_answer)).setText(answers.get(a));
            baseView.addView(answerView);
        }
        return baseView;
        }

    @Override
    public int getItemViewType(int position) {
            if (position==0)
                return MAIN_CONTENT;
            else
                return BASE_ACTION;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private Context getContext() {
        return mContext;
    }

    static class BaseActionVH extends RecyclerView.ViewHolder {
        public View background;

        public BaseActionVH(View itemView) {
            super(itemView);

            background= itemView.findViewById(R.id.background);

        }
    }

    static class MainContentVH extends RecyclerView.ViewHolder {
        public TextView tvMessage;
        public ImageView ivIlustration;
        public TextView tvHeader;

        public MainContentVH(View itemView) {
            super(itemView);
              tvMessage = itemView.findViewById(R.id.tv_message);
              ivIlustration = itemView.findViewById(R.id.iv_illustration);
              tvHeader = itemView.findViewById(R.id.tv_caption);
        }
    }
}