package com.hensongeodata.myagro360v2.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.fragment.HistoryScanFragment;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by user1 on 8/2/2016.
 */
public class HistoryScanAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=HistoryScanAdapterRV.class.getSimpleName();
    private ArrayList<InstructionScout> items;
    private Context mContext;
    private final int SUMMARY = 0, ITEM =1;
    private int mode;

    public HistoryScanAdapterRV(Context context, ArrayList<InstructionScout> items) {
        this.items = items;
        mContext = context;
    }


    public void updateItem(int position,InstructionScout instructionScout){
        Log.d(TAG,position+" updateItem "+instructionScout);
        items.set(position,instructionScout);
        notifyItemChanged(position);
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        RecyclerView.ViewHolder viewHolder;
        View vInstruction;
        switch (viewType) {
            case SUMMARY:
                vInstruction = inflater.inflate(R.layout.summary_item, parent, false);
                viewHolder = new SummaryVH(vInstruction);
                break;
            case ITEM:
                View vTask = inflater.inflate(R.layout.history_scout_section_item,parent, false);
                viewHolder = new InformationVH(vTask);
                break;
            default:
                vInstruction = inflater.inflate(R.layout.history_scout_section_item, parent, false);
                viewHolder = new InformationVH(vInstruction);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        InformationVH informationVH;

        switch (viewHolder.getItemViewType()) {
            case SUMMARY:
                SummaryVH summaryVH = (SummaryVH) viewHolder;
                configureSummary(summaryVH, position);
                break;
            case ITEM:
                informationVH = (InformationVH) viewHolder;
                configureInformation(informationVH, position);
                break;
            default:
                informationVH = (InformationVH) viewHolder;
                configureInformation(informationVH, position);
                break;
        }

    }

    private void configureSummary(final SummaryVH summaryVH, final int position) {
        summaryVH.tvScanTotal.setText(""+HistoryScanFragment.scan_total);
        summaryVH.tvFawDetected.setText(""+HistoryScanFragment.scan_detected);
    }


    private void configureInformation(final InformationVH informationVH, final int position) {
            Context context=informationVH.image.getContext();
            InstructionScout info=items.get(position);

            boolean noInfo=info.getMessage()==null||info.getMessage().trim().isEmpty();
            informationVH.message.setText(noInfo?
            mContext.getResources().getString(R.string.ai_scout_analyse_image):
                    info.getMessage());

            boolean status=info.getStatus()==InstructionScout.STATE_PROCCESSED;
            Log.d(TAG,"noInfo: "+noInfo+" info: "+info.getMessage()+" status "+info.getStatus()+" processed: "+status);
            informationVH.vProgress.setVisibility(noInfo?View.VISIBLE:View.GONE);

            Image image=info.getImage();
        if(image!=null&&image.getUri()!=null) {
            String path=image.getUri().getPath();
            Log.d(TAG,"path: "+path);
            Picasso.get().load((new File(image.getUri().getPath())))
                    .resize(800,800)
                    .noFade()
                    .into(informationVH.image);
        }
    }

    @Override
    public int getItemViewType(int position) {

        InstructionScout item= items.get(position);

            if (position==0&&mode==ChatbotAdapterRV.MODE_SCOUT)
                return SUMMARY;
            else
                return ITEM;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class SummaryVH extends RecyclerView.ViewHolder {
        public TextView tvScanTotal;
        public TextView tvFawDetected;

        public SummaryVH(View itemView) {
            super(itemView);

              tvScanTotal = itemView.findViewById(R.id.tv_scan_total);
              tvFawDetected = itemView.findViewById(R.id.tv_scan_detected);
        }
    }

    static class InformationVH extends RecyclerView.ViewHolder {
        public TextView message;
        public ImageView image;
        public View background;
        public View vProgress;

        public InformationVH(View itemView) {
            super(itemView);

              message = itemView.findViewById(R.id.tv_message);
              image = itemView.findViewById(R.id.iv_image);
            background=itemView.findViewById(R.id.v_background);
            vProgress=itemView.findViewById(R.id.progressbar);
        }
    }
}