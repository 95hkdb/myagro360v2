package com.hensongeodata.myagro360v2.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.Interface.ControlListener;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.model.MapModel;
import com.hensongeodata.myagro360v2.model.SettingsModel;
import com.hensongeodata.myagro360v2.view.MainActivityAutomated;

import java.util.ArrayList;

/**
 * Created by user1 on 8/2/2016.
 */
public class ChatbotAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=ChatbotAdapterRV.class.getSimpleName();
    private ArrayList<InstructionScout> items;
    private Context mContext;
    private LocsmmanEngine locsmmanEngine;
    private final int INSTRUCTION = 0, TASK =1, INFORMATION=2;
    private int mode;
    public static int MODE_MAP=0, MODE_SCOUT=1;
    private boolean isFrench=false;
    private ControlListener controlListener;

    public ChatbotAdapterRV(){}
    public ChatbotAdapterRV(Context context, ArrayList<InstructionScout> items) {
        this.items = items;
        mContext = context;
        locsmmanEngine=new LocsmmanEngine(context);

        isFrench=(new SettingsModel(context)).getLang().equalsIgnoreCase(SettingsModel.LAN_FR);
    }

    public void  setControlListener(ControlListener listener){
        this.controlListener=listener;
    }

    public ChatbotAdapterRV(Context context) {
        items=new ArrayList<>();
        mContext=context;
        locsmmanEngine=new LocsmmanEngine(context);
        isFrench=(new SettingsModel(context)).getLang().equalsIgnoreCase(SettingsModel.LAN_FR);
    }

    public void addItem(InstructionScout instructionScout){
        Log.d(TAG,"add item");
        if(items!=null)
            items.add(instructionScout);

        notifyItemInserted(items.size()-1);
        locsmmanEngine.playVibrator();
        //notifyDataSetChanged();
    }

    public void updateItem(int position,InstructionScout instructionScout){
        items.set(position,instructionScout);
        //notifyDataSetChanged();
        notifyItemChanged(position);
        Log.d(TAG,"notifyDataSet");
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        RecyclerView.ViewHolder viewHolder;
        View vInstruction;
        View vTask;
        switch (viewType) {
            case INSTRUCTION:
                vInstruction = inflater.inflate(R.layout.instruction, parent, false);
                viewHolder = new InstructionVH(vInstruction);
                break;
            case INFORMATION:
                View vInformation = inflater.inflate(R.layout.information, parent, false);
                viewHolder = new InformationVH(vInformation);
                break;
            case TASK:
                if(getMode()==MODE_SCOUT)
                vTask = inflater.inflate(R.layout.instruction_scout_reverse,parent, false);
                else
                    vTask = inflater.inflate(R.layout.instruction_mapping_reverse,parent, false);
                viewHolder = new TaskVH(vTask);
                break;
            default:
                vInstruction = inflater.inflate(R.layout.instruction, parent, false);
                viewHolder = new InstructionVH(vInstruction);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        InstructionVH instructionVH;

        switch (viewHolder.getItemViewType()) {
            case INSTRUCTION:
                instructionVH = (InstructionVH) viewHolder;
                configureInstruction(instructionVH, position);
                break;
            case INFORMATION:
                InformationVH informationVH = (InformationVH) viewHolder;
                configureInformation(informationVH, position);
                break;
            case TASK:
                TaskVH taskVH = (TaskVH) viewHolder;
                configureTask(taskVH, position);
                break;
            default:
                instructionVH = (InstructionVH) viewHolder;
                configureInstruction(instructionVH, position);
                break;
        }

    }

    private void configureInstruction(final InstructionVH instructionVH, final int position) {
        InstructionScout instruction=items.get(position);
        instructionVH.message.setText(instruction.getMessage());
        //locsmmanEngine.showView(instructionVH.background,true);
    }


    private void configureInformation(final InformationVH informationVH, final int position) {

        InstructionScout info=items.get(position);

            informationVH.message.setText(info.getMessage());
            Image image=info.getImage();
            informationVH.image.setImageResource(image.getSrc());

        //locsmmanEngine.showView(informationVH.background,true);
    }

    private void configureTask(final TaskVH taskVH, final int position) {

        if(getMode()==MODE_SCOUT) {
            InstructionScout task = items.get(position);

            taskVH.message.setText(task.getMessage() == null || task.getMessage().trim().isEmpty() ?
                    (isFrench?mContext.getResources().getString(R.string.ai_scout_analyse_image_fr):
                            mContext.getResources().getString(R.string.ai_scout_analyse_image)): task.getMessage());

            taskVH.vScanning.setVisibility(task.getMessage() == null || task.getMessage().trim().isEmpty() ?
                    View.VISIBLE : View.GONE);

        }
        else if(getMode()==MODE_MAP){
            InstructionScout mapping = items.get(position);

            taskVH.message.setText(mapping.getMessage() == null || mapping.getMessage().trim().isEmpty() ?
                    (isFrench? mContext.getResources().getString(R.string.ai_map_processing_fr)
                            :mContext.getResources().getString(R.string.ai_map_processing)) :
                    (isFrench? mContext.getResources().getString(R.string.ai_map_attached_fr)
                            :mContext.getResources().getString(R.string.ai_map_attached)));

            taskVH.vScanning.setVisibility(mapping.getMessage() == null || mapping.getMessage().trim().isEmpty() ?
                    View.VISIBLE : View.GONE);
        }


        if(taskVH.ivLaunch!=null) {
            taskVH.ivLaunch.setVisibility(getMode() == MODE_MAP ? View.VISIBLE : View.GONE);

            taskVH.background.setOnClickListener(getMode()==MODE_SCOUT?null:new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openForms(taskVH.getAdapterPosition());
                }
            });
        }
    }


    private void openForms(int position){
        InstructionScout instructionScout=items.get(position);
        Log.d(TAG,"openForms scout_id: "+instructionScout.getScout_id());
        MapModel mapModel=new MapModel();
        mapModel.id=instructionScout.getScout_id()+" vxxx";
        mapModel.type=MapModel.TYPE_POINT;
        MainActivityAutomated.mapModel=mapModel;

        ArrayList<String>strPois =new ArrayList<>();

        String message = instructionScout.getMessage();
        if (message != null && !message.trim().isEmpty())
            strPois.add(message);

        MainActivityAutomated.pois=CreateForm.getInstance(mContext).buildPOI_Mapping(strPois);
        Intent intent=new Intent(mContext,MainActivityAutomated.class);
        mContext.startActivity(intent);
       // controlListener.pause();
    }


    @Override
    public int getItemViewType(int position) {

        InstructionScout item= items.get(position);

            if (item.getAction()==InstructionScout.ACTION_TASK)
                return INSTRUCTION;
            else if(item.getAction()==InstructionScout.ACTION_INSTRUCT)
                return INSTRUCTION;
            else if(item.getAction()==InstructionScout.ACTION_SCAN)
                return TASK;
            else if(item.getAction()==InstructionScout.ACTION_INFO)
                return INFORMATION;
            else
                return -1;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class InstructionVH extends RecyclerView.ViewHolder {
        public TextView message;
        public TextView description;
        public View background;

        public InstructionVH(View itemView) {
            super(itemView);

              message = itemView.findViewById(R.id.tv_message);
            //description= (TextView) itemView.findViewById(R.id.tv_description);
            background=itemView.findViewById(R.id.v_background);
        }
    }

    static class InformationVH extends RecyclerView.ViewHolder {
        public TextView message;
        public ImageView image;
        public View background;

        public InformationVH(View itemView) {
            super(itemView);

              message = itemView.findViewById(R.id.tv_message);
              image = itemView.findViewById(R.id.iv_image);
            background=itemView.findViewById(R.id.v_background);

        }
    }

    static class TaskVH extends RecyclerView.ViewHolder {
        public TextView message;
        public View vScanning;
        public View background;
        public ImageView ivLaunch;

        public TaskVH(View itemView) {
            super(itemView);
              message = itemView.findViewById(R.id.tv_message);
            vScanning= itemView.findViewById(R.id.v_scanning);
            background=itemView.findViewById(R.id.v_background);
            ivLaunch=itemView.findViewById(R.id.iv_launch);
        }
    }
}