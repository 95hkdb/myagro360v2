package com.hensongeodata.myagro360v2.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.marlonlom.utilities.timeago.TimeAgo;

import java.util.ArrayList;
import java.util.List;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.SwipeDismissTouchListener;
import com.hensongeodata.myagro360v2.model.Archive;
import com.hensongeodata.myagro360v2.model.Form;
import com.hensongeodata.myagro360v2.view.PreviewForm;

/**
 * Created by user1 on 8/2/2016.
 */
public class HistoryAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=HistoryAdapterRV.class.getSimpleName();
    private List<Archive> historyList;
    private List<Archive> filteredList;
    private Context mContext;
    private ArchiveHolder archiveHolder;

    public HistoryAdapterRV(Context context, List<Archive> items) {
        mContext=context;
        this.historyList =items;
        filteredList=items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.history_item, parent, false);
            return new ArchiveHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

          Archive archive = filteredList.get(position);
        archiveHolder= (ArchiveHolder) holder;
        archiveHolder.tvName.setText(archive.getName());
        long millis= MyApplication.getMillis(archive.getDate(),archive.getTime());
        archiveHolder.tvDate.setText(millis==-1? archive.getDate(): TimeAgo.using(millis));
        //archiveHolder.tvDate.setText(archive.getDate());
        archiveHolder.tvOrg.setText(archive.getOrganization().getName());

        archiveHolder.background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewForm(archiveHolder.getAdapterPosition());
            }
        });
    }

    private void previewForm(final int position){
          Archive archive = filteredList.get(position);
        // Log.d(TAG,"transaction_id: "+archive.getTransaction_id());
        Intent intent=new Intent(mContext, PreviewForm.class);
        intent.putExtra("source", PreviewForm.SOURCE_HISTORY);
        intent.putExtra("transaction_id",archive.getTransaction_id());
        mContext.startActivity(intent);
    }


    private void addSwipeDismiss(View view, final int position){
        // Create a generic swipe-to-dismiss touch listener.
        view.setOnTouchListener(new SwipeDismissTouchListener(
                view,
                null,
                new SwipeDismissTouchListener.DismissCallbacks() {
                    @Override
                    public boolean canDismiss(Object token) {
                        return true;
                    }

                    @Override
                    public void onDismiss(View view, Object token) {

                    }
                }));
        //dismissableContainer.addView(view);
    }

    private void deleteForm(View v, Form form){

        if((new DatabaseHelper(mContext)).deleteItem(DatabaseHelper.TBL_FORMS,form.getId())){
            v.setVisibility(View.GONE);
            Toast.makeText(mContext,form.getTitle()+" Deleted successfully.",Toast.LENGTH_SHORT).show();
        }
        else {
            v.setVisibility(View.VISIBLE);
            Toast.makeText(mContext,form.getTitle()+" Failed to delete.",Toast.LENGTH_SHORT).show();
        }
    }


    public void search(String keyword){
        filteredList=new ArrayList<>();
        if(keyword!=null&&!keyword.trim().isEmpty()) {
            keyword = keyword.toLowerCase();
            keyword=keyword.trim();
        }
        if(keyword==null||keyword.isEmpty()){
            //reset filtered list to original list
            filteredList= historyList;
        }
        else {
            for(int a = 1; a< historyList.size(); a++){
                  Archive archive = historyList.get(a);
                if(archive.getName()!=null&& archive.getOrganization()!=null) {
                    if (archive.getName().toLowerCase().contains(keyword)
                            || archive.getOrganization().getName().toLowerCase().contains(keyword)) {
                        filteredList.add(historyList.get(a));
                    }
                }
            }
        }

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    private class ArchiveHolder extends RecyclerView.ViewHolder {
        public View background;
        public View bgCheck;
        public TextView tvName;
        public TextView tvDate;
        public TextView tvOrg;

        public ArchiveHolder(View itemView) {
            super(itemView);
            background=itemView.findViewById(R.id.background);
              tvName = itemView.findViewById(R.id.tv_name);
              tvDate = itemView.findViewById(R.id.tv_date);
              tvOrg = itemView.findViewById(R.id.tv_organization);
            bgCheck=itemView.findViewById(R.id.bg_check);
        }
    }

}