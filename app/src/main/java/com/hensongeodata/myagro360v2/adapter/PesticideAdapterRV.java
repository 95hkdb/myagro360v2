package com.hensongeodata.myagro360v2.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Pesticide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 8/2/2016.
 */
public class PesticideAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=PesticideAdapterRV.class.getSimpleName();
    private List<Pesticide> pesticideList;
    private List<Pesticide> filteredList;
    private Context mContext;
    private PesticideHolder pesticideHolder;

    public PesticideAdapterRV(Context context, List<Pesticide> items) {
        mContext=context;
        this.pesticideList =items;
        filteredList=items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.pesticide_item, parent, false);
            return new PesticideHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

          Pesticide pesticide = filteredList.get(position);
        pesticideHolder = (PesticideHolder) holder;
        pesticideHolder.tvCaption.setText(pesticide.getTitle());
        pesticideHolder.tvIngredient.setText(pesticide.getActiveIngredients());
        pesticideHolder.tvDose.setText(pesticide.getDescription());

//        archiveHolder.background.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onAddMapButtonClicked(View v) {
//                previewForm(archiveHolder.getAdapterPosition());
//            }
//        });
    }

    public void search(String keyword){
        filteredList=new ArrayList<>();
        if(keyword!=null&&!keyword.trim().isEmpty()) {
            keyword = keyword.toLowerCase();
            keyword=keyword.trim();
        }
        if(keyword==null||keyword.isEmpty()){
            //reset filtered list to original list
            filteredList= pesticideList;
        }
        else {
            for(int a = 1; a< pesticideList.size(); a++){
                  Pesticide pesticide = pesticideList.get(a);
                if(pesticide.getTitle()!=null&& pesticide.getActiveIngredients()!=null) {
                    if (pesticide.getTitle().toLowerCase().contains(keyword)
                            || pesticide.getActiveIngredients().toLowerCase().contains(keyword)) {
                        filteredList.add(pesticideList.get(a));
                    }
                }
            }
        }

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    private class PesticideHolder extends RecyclerView.ViewHolder {
        public View background;
        public TextView tvCaption;
        public TextView tvIngredient;
        public TextView tvDose;

        public PesticideHolder(View itemView) {
            super(itemView);
            background=itemView.findViewById(R.id.background);
              tvCaption = itemView.findViewById(R.id.tv_caption);
              tvIngredient = itemView.findViewById(R.id.tv_ingredient);
              tvDose = itemView.findViewById(R.id.tv_dose);
        }
    }

}