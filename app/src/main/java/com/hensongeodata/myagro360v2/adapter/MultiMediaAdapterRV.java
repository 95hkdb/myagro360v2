package com.hensongeodata.myagro360v2.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.FileField;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.PreviewState;
import com.hensongeodata.myagro360v2.view.PreviewImageLarge;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user1 on 8/2/2016.
 */
public class MultiMediaAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=MultiMediaAdapterRV.class.getSimpleName();
    private ArrayList<Image> mediaList;
    private ArrayList<PreviewState> state;
    private Context mContext;
    private MyApplication myApplication;
    public static final int TYPE_IMAGE = 0, TYPE_VIDEO=1;
    private int mediaType;
    private int globalMediaIndex;

    public MultiMediaAdapterRV(Context context, ArrayList<Image> mediaList) {
        this.mediaList = mediaList;
        state=new ArrayList<>();
        for(int a = 0; a< mediaList.size(); a++)
                state.add(new PreviewState(false));//set all view states of preview items to false until they are viewed

        mContext = context;
        myApplication=new MyApplication();
    }

    public void setMediaType(String mediaType){
        if(mediaType.equalsIgnoreCase(FileField.FILE_IMAGE))
            this.mediaType=TYPE_IMAGE;
        else if(mediaType.equalsIgnoreCase(FileField.FILE_VIDEO))
            this.mediaType=TYPE_VIDEO;
    }

    public int getGlobalMediaIndex() {
        return globalMediaIndex;
    }

    public void setGlobalMediaIndex(int globalMediaIndex) {
        this.globalMediaIndex = globalMediaIndex;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        RecyclerView.ViewHolder viewHolder;
        View vImage;
        switch (viewType) {
            case TYPE_IMAGE:
                vImage = inflater.inflate(R.layout.multi_media_image_item, parent, false);
                viewHolder = new ImageVH(vImage);
                break;
            case TYPE_VIDEO:
                View vVideo = inflater.inflate(R.layout.multi_media_image_item,parent, false);
                viewHolder = new VideoVH(vVideo);
            default:
                vImage = inflater.inflate(R.layout.multi_media_image_item, parent, false);
                viewHolder = new ImageVH(vImage);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ImageVH imageVH;
        switch (viewHolder.getItemViewType()) {
            case TYPE_IMAGE:
                imageVH = (ImageVH) viewHolder;
                configureImage(imageVH, position);
                break;
            case TYPE_VIDEO:
                VideoVH videoVH = (VideoVH) viewHolder;
                configureVideo(videoVH, position);
                break;
            default:
                imageVH = (ImageVH) viewHolder;
                configureImage(imageVH, position);
                break;
        }

    }

    private void configureImage(final ImageVH imageHolder, final int position) {

        Image image= mediaList.get(position);

        imageHolder.ibDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeMedia(imageHolder.getAdapterPosition());
            }
        });
        imageHolder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG,"image clicked");
                previewImage(imageHolder.getAdapterPosition());
            }
        });

        imageHolder.ibView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                previewImage(imageHolder.getAdapterPosition());
            }
        });
        Log.d(TAG,"Image URI: "+image.getUri());
        Picasso.get().load(image.getUri()).resize(800, 800).centerInside().into(imageHolder.image);

    }
    private void configureVideo(final VideoVH videoHolder, final int position) {

        Image image= mediaList.get(position);

        videoHolder.ibDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeMedia(videoHolder.getAdapterPosition());
            }
        });

    }


    private void removeMedia(int position){
        if(mediaType==TYPE_IMAGE)
            MyApplication.imageMultiArrayList.get(globalMediaIndex).removeImage(position);

        notifyDataSetChanged();
    }

    private void previewImage(int position){
        Intent i=new Intent(mContext, PreviewImageLarge.class);
        i.putExtra("global_index",globalMediaIndex);
        i.putExtra("position",position);
        i.putExtra("multi_image",true);
        Uri uri=mediaList.get(position).getUri();
        if(uri!=null)
                i.putExtra("path",uri.getPath());
        else
            i.putExtra("path","");

        if(uri!=null)
              MyApplication.setImageModel(getImage(uri.getPath()));

        mContext.startActivity(i);
    }


    private Image getImage(String path){
        Image imageModel=new Image();
        imageModel.setImageBitmap(myApplication.bitmapCompressLarge(path));
          imageModel.setImageString(MyApplication.encodeBitmap(imageModel.getImageBitmap()));
        imageModel.setUri(Uri.parse(path));

        return imageModel;
    }

    @Override
    public int getItemViewType(int position) {
        return mediaType;
    }

    @Override
    public int getItemCount() {
        return mediaList.size();
    }

    static class ImageVH extends RecyclerView.ViewHolder {
        public ImageView image;
        public ImageButton ibDelete;
        public ImageButton ibView;

        public ImageVH(View itemView) {
            super(itemView);
              ibDelete = itemView.findViewById(R.id.ib_delete);
              image = itemView.findViewById(R.id.iv_image);
              ibView = itemView.findViewById(R.id.ib_action);
            ibView.setImageResource(R.drawable.ic_eye);
        }
    }


    static class VideoVH extends RecyclerView.ViewHolder {
        public ImageView image;
        public ImageButton ibDelete;
        public ImageButton ibPlay;

        public VideoVH(View itemView) {
            super(itemView);
              ibDelete = itemView.findViewById(R.id.ib_delete);
              image = itemView.findViewById(R.id.iv_image);
              ibPlay = itemView.findViewById(R.id.ib_action);
            ibPlay.setImageResource(R.drawable.ic_play_dark);
        }
    }

    private String fixPath(String path){
        if(path!=null){
            if(path.length()>1){
                if(path.charAt(0)=='/'||path.charAt(0)!='f'){
                    path="file://"+path;
                }
            }
        }

        return path;
    }
}