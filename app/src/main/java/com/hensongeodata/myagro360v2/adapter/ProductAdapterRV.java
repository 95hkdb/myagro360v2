package com.hensongeodata.myagro360v2.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Product;
import com.hensongeodata.myagro360v2.view.ProductPreview;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 8/2/2016.
 */
public class ProductAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=ProductAdapterRV.class.getSimpleName();
    private List<Product> productList;
    private List<Product> filteredList;
    private Context mContext;
    private ProductHolder productHolder;
    public static Product productSelected;

    public ProductAdapterRV(Context context, List<Product> items) {
        mContext=context;
        this.productList =items;
        filteredList=items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.product_item, parent, false);
            return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
           final Product product = filteredList.get(position);
            productHolder =(ProductHolder)holder;

        productHolder.name.setText(product.name);
        productHolder.description.setText(product.description);
        productHolder.price.setText("GHc "+product.price);
        if(product.owner_name==null||product.owner_name.trim().equalsIgnoreCase("null")){
            productHolder.owner_name.setVisibility(View.GONE);
        }
        else {
            productHolder.owner_name.setVisibility(View.VISIBLE);
            productHolder.owner_name.setText(product.owner_name);
        }
        Picasso.get().load(
                (product.image))
                .resize(800, 800)
                .placeholder(R.drawable.blurred_image)
                .into(productHolder.image);

        productHolder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewProduct(holder.getAdapterPosition());
            }
        });

    }

    private void previewProduct(final int position){
        productSelected= filteredList.get(position);
        Intent intent=new Intent(mContext,ProductPreview.class);
        mContext.startActivity(intent);
    }

    public void search(String keyword){
        filteredList=new ArrayList<>();
        if(keyword!=null&&!keyword.trim().isEmpty()) {
            keyword = keyword.toLowerCase();
            keyword=keyword.trim();
        }
        if(keyword==null||keyword.isEmpty()){
            //reset filtered list to original list
            filteredList= productList;
        }
        else {
            for(int a = 0; a< productList.size(); a++){
                if(productList.get(a).name!=null&& productList.get(a).description!=null&&productList.get(a).owner_name!=null) {
                    if (productList.get(a).name.toLowerCase().contains(keyword)
                            || productList.get(a).description.toLowerCase().contains(keyword)
                            ||productList.get(a).owner_name.toLowerCase().contains(keyword)) {
                        filteredList.add(productList.get(a));
                    }
                }
            }
        }

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    private class ProductHolder extends RecyclerView.ViewHolder {
        public View container;
        public ImageView image;
        public TextView name;
        public TextView price;
        public TextView description;
        public TextView owner_name;


        public ProductHolder(View itemView) {
            super(itemView);
            container=itemView.findViewById(R.id.v_container);
              image = itemView.findViewById(R.id.iv_image);
              name = itemView.findViewById(R.id.tv_name);
              description = itemView.findViewById(R.id.tv_description);
              price = itemView.findViewById(R.id.tv_price);
              owner_name = itemView.findViewById(R.id.tv_owner_name);
             }
    }

}