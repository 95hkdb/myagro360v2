package com.hensongeodata.myagro360v2.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.model.PaymentReceipt;
import com.hensongeodata.myagro360v2.model.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 8/2/2016.
 */
public class PaymentHistoryAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=PaymentHistoryAdapterRV.class.getSimpleName();
    private List<PaymentReceipt> receiptList;
    private List<PaymentReceipt> filteredList;
    private Context mContext;
    private ReceiptHolder receiptHolder;
    private DatabaseHelper databaseHelper;
    private LayoutInflater inflater;

    public PaymentHistoryAdapterRV(Context context, List<PaymentReceipt> items) {
        mContext=context;
        this.receiptList =items;
        filteredList=items;
        databaseHelper=new DatabaseHelper(context);
        inflater=LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.payment_history_item, parent, false);
            return new ReceiptHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
           final PaymentReceipt receipt = filteredList.get(position);
            receiptHolder =(ReceiptHolder)holder;

        receiptHolder.orderNumber.setText(mContext.getResources().getString(R.string.order_number)+" "+receipt.order_number);
        receiptHolder.purchaseDate.setText(""+receipt.date);
        receiptHolder.amountPaid.setText("GHc "+receipt.amount_paid);
        receiptHolder.status.setText(""+receipt.order_status);

        addProducts(receiptHolder.vProducts,receiptHolder.vHeader,receipt.order_number);
    }

    private void addProducts(LinearLayout container, View header, String order_code){
        List<Product> products=databaseHelper.getProducts(order_code);
        if(container!=null){
            container.removeAllViews();
         if(products!=null) {
             int index=0;
             for (Product product : products) {
                 if(index==0)
                     container.addView(header);

                 View v=inflater.inflate(R.layout.payment_history_item_item,null);
                 ((TextView)v.findViewById(R.id.tv_name)).setText(product.name);
                 ((TextView)v.findViewById(R.id.tv_quantity)).setText(""+product.quantity);
                 ((TextView)v.findViewById(R.id.tv_price)).setText("GHc "+(product.quantity*product.price));
                 container.addView(v);

                index++;
             }
         }
        }
    }

    public void search(String keyword){
        filteredList=new ArrayList<>();
        if(keyword!=null&&!keyword.trim().isEmpty()) {
            keyword = keyword.toLowerCase();
            keyword=keyword.trim();
        }
        if(keyword==null||keyword.isEmpty()){
            //reset filtered list to original list
            filteredList= receiptList;
        }
        else {
            for(int a = 0; a< receiptList.size(); a++){
                if(receiptList.get(a).date!=null&& receiptList.get(a).order_status!=null) {
                    if (receiptList.get(a).date.toLowerCase().contains(keyword)
                            || receiptList.get(a).order_status.toLowerCase().contains(keyword)) {
                        filteredList.add(receiptList.get(a));
                    }
                }
            }
        }

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    private class ReceiptHolder extends RecyclerView.ViewHolder {
        public View container;
        public TextView orderNumber;
        public TextView purchaseDate;
        public TextView amountPaid;
        public TextView status;
        public LinearLayout vProducts;
        public View vHeader;

        public ReceiptHolder(View itemView) {
            super(itemView);
            container=itemView.findViewById(R.id.v_container);
              orderNumber = itemView.findViewById(R.id.tv_order);
              purchaseDate = itemView.findViewById(R.id.tv_purchase_date);
              amountPaid = itemView.findViewById(R.id.tv_amount_paid);
              status = itemView.findViewById(R.id.tv_status_order);
            vHeader=itemView.findViewById(R.id.v_header);
            vProducts=itemView.findViewById(R.id.v_products);
        }
    }

}