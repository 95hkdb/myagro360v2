package com.hensongeodata.myagro360v2.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import com.hensongeodata.myagro360v2.Interface.ItemTouchHelperViewHolder;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.fragment.BSFragment;
import com.hensongeodata.myagro360v2.model.POIGroup;

/**
 * Created by user1 on 8/2/2016.
 */

public class POIGroupAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=POIGroupAdapterRV.class.getSimpleName();
    private ArrayList<POIGroup> poiGroupList;
    private ArrayList<POIGroup> filteredList;
    private Context mContext;
    private DatabaseHelper databaseHelper;
    private boolean error=false;
    private TextView tvError;

    public POIGroupAdapterRV(Context context,ArrayList<POIGroup> items) {
        mContext=context;
        this.poiGroupList =items;
        this.filteredList=items;
        databaseHelper=new DatabaseHelper(mContext);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.poi_group_item, parent, false);
            return new POIholder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        //Log.d(TAG,"bindview");

            Log.d(TAG,"POIGroupAdapt: "+position);
            POIGroup poiGroup = filteredList.get(position);
            POIholder poIholder = (POIholder) holder;
            poIholder.tvName.setText(poiGroup.getName());
            if(poiGroup.isSelect()){
                poIholder.indicator.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
            }else {
                poIholder.indicator.setBackgroundColor(mContext.getResources().getColor(R.color.light_gray));
            }

            poIholder.background.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                selectItem(holder.getAdapterPosition());
                }
            });

            poIholder.ibDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeItem(holder.getAdapterPosition());
                }
            });

            poIholder.ibEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showPop(holder.getAdapterPosition());
                }
            });
    }
    private void showPop(final int position){
        final POIGroup poiGroup= filteredList.get(position);

        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.confirm_dialog);
          tvError = dialog.findViewById(R.id.tv_message);
        ((TextView)dialog.findViewById(R.id.tv_caption)).setText("Edit Location name");
          final EditText etName = dialog.findViewById(R.id.et_password);
        etName.setText(poiGroup.getName());
        etName.addTextChangedListener(new MyPOIWatcher(BSFragment.ACTION_ADD_POI_GROUP));
          Button btnCancel = dialog.findViewById(R.id.btn_negative);
          Button btnSave = dialog.findViewById(R.id.btn_positive);


        btnCancel.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_grey));
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btnSave.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_green));
        btnSave.setTextColor(mContext.getResources().getColor(R.color.primaryWhite));
        btnSave.setText("Save");
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!error) {
                    String name = etName.getText().toString().trim();
                    if (name.isEmpty())
                        Toast.makeText(mContext, "Name can't be empty", Toast.LENGTH_SHORT).show();
                    else {
                        poiGroup.setName(name);
                        updateItem(position, poiGroup);
                    }
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }



    private void selectItem(int position){
        (poiGroupList.get(position)).setSelect(!poiGroupList.get(position).isSelect());

        for(int a=0;a<poiGroupList.size();a++)
            if(a!=position)
                    (poiGroupList.get(a)).setSelect(false);


        notifyDataSetChanged();

        databaseHelper.selectPOIGroup((poiGroupList.get(position)));//update selection in database too
    }

    private void removeItem(final int position){
        final POIGroup poiGroup=poiGroupList.get(position);


        new AlertDialog.Builder(mContext)
                .setMessage("Remove location "+poiGroup.getName()+"?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       databaseHelper.removePOIGGroup(poiGroup);
                        poiGroupList.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(mContext,"location "+poiGroup.getName()+" removed successfully.",Toast.LENGTH_SHORT).show();
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }
    private void updateItem(int position,POIGroup poiGroup){

        databaseHelper.updatePOIGroup(poiGroup);
        poiGroupList.set(position,poiGroup);

        notifyDataSetChanged();
        Toast.makeText(mContext,"Name changed to "+poiGroup.getName(),Toast.LENGTH_SHORT).show();
    }

    class MyPOIWatcher implements TextWatcher {
        String action;
        public MyPOIWatcher(String action){
            this.action=action;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            String name=charSequence.toString();
            Log.d(TAG,"action: "+action);
            if(action!=null&&tvError!=null){
                if(action.equalsIgnoreCase(BSFragment.ACTION_ADD_POI_GROUP)){
                    for (int a = 0; a < poiGroupList.size(); a++) {
                        if (poiGroupList.get(a).getName()!=null
                                &&poiGroupList.get(a).getName().equalsIgnoreCase(name)) {
                            error = true;
                            tvError.setText("Group name already exist.");
                            tvError.setVisibility(View.VISIBLE);
                            return;
                        }
                    }

                    if (name.length() >= BSFragment.MAX_POI_GROUP_CHARS) {
                        error = true;
                        tvError.setText("Group name should be max " +  BSFragment.MAX_POI_GROUP_CHARS + " characters.");
                        tvError.setVisibility(View.VISIBLE);
                        return;
                    }
                }


                tvError.setVisibility(View.GONE);
            }

            error=false;
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }


    public void search(String keyword){
        Log.d(TAG,"keyword: "+keyword);
        filteredList=new ArrayList<>();
        if(keyword!=null&&!keyword.trim().isEmpty()) {
            keyword = keyword.toLowerCase();
            keyword=keyword.trim();
        }
        if(keyword==null||keyword.isEmpty()){
            //reset filtered list to original list
            filteredList= poiGroupList;
            Log.d(TAG,"empty or null");
        }
        else {
            Log.d(TAG,"not empty "+poiGroupList.size());
            for(int a = 0; a< poiGroupList.size(); a++){
                Log.d(TAG,"loop "+a);
                if(poiGroupList.get(a).getName()!=null) {
                    if (poiGroupList.get(a).getName().toLowerCase().contains(keyword)) {
                        Log.d(TAG,"contains: "+poiGroupList.get(a).getName());
                        filteredList.add(poiGroupList.get(a));
                    }
                }
            }
        }

        notifyDataSetChanged();
        Log.d(TAG,"data set notified");
    }

    @Override
    public int getItemCount() {
        return poiGroupList.size();
    }

    private class POIholder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder {
        int index;
        public View background;
        public View indicator;
        public TextView tvName;
        public ImageView ivLocation;
        public ImageButton ibDelete;
        public ImageButton ibEdit;

        public POIholder(View itemView) {
            super(itemView);
            background=itemView.findViewById(R.id.background);
            indicator=itemView.findViewById(R.id.bg_indicator);
              ivLocation = itemView.findViewById(R.id.iv_location);
              tvName = itemView.findViewById(R.id.tv_name);
              ibDelete = itemView.findViewById(R.id.ib_delete);
              ibEdit = itemView.findViewById(R.id.ib_edit);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

}