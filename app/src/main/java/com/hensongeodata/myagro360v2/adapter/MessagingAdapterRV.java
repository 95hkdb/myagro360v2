package com.hensongeodata.myagro360v2.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.controller.NetworkRequest;
import com.hensongeodata.myagro360v2.controller.TouchImageView;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.Message;
import com.hensongeodata.myagro360v2.receiver.PushReceiver;
import com.hensongeodata.myagro360v2.view.Chat;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by user1 on 8/2/2016.
 */
public class MessagingAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=MessagingAdapterRV.class.getSimpleName();
    private ArrayList<Message> messages;
    private Context mContext;
    private LocsmmanEngine locsmmanEngine;
    private final int OUTGOING = 0, INCOMING =1, DATE_HEADER =2;
    private String user_id;
    private DatabaseHelper databaseHelper;
    private NetworkRequest networkRequest;
    private Dialog dialogImage;
    private TextView dialogTitle;
    private TouchImageView mImage;

    public MessagingAdapterRV(){}
    public MessagingAdapterRV(Context context, ArrayList<Message> messages,String user_id) {
        this.messages = messages;
        mContext = context;
        this.user_id=user_id;
        locsmmanEngine=new LocsmmanEngine(context);
        databaseHelper=new DatabaseHelper(context);
        networkRequest=new NetworkRequest(context);

        initImageDialog();
    }


    public void addMessage(Message message){
        Log.d(TAG,"add message");
        if(messages !=null)
            messages.add(message);

        notifyItemInserted(messages.size()-1);
        locsmmanEngine.playVibrator();
    }

    public void addToEnd(List<Message> messagesList, boolean reverse){
        if(messagesList!=null) {
            if(reverse)
                Collections.reverse(messagesList);

            messages.addAll(0, messagesList);
            notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        RecyclerView.ViewHolder viewHolder;
        View vInstruction;
        View vTask;
        switch (viewType) {
            case DATE_HEADER:
                vInstruction = inflater.inflate(R.layout.message_incoming, parent, false);
                viewHolder = new DateHeaderVH(vInstruction);
                break;
            case INCOMING:
                View vInformation = inflater.inflate(R.layout.message_incoming, parent, false);
                viewHolder = new IncomingVH(vInformation);
                break;
            case OUTGOING:
                    vTask = inflater.inflate(R.layout.message_outgoing,parent, false);
                viewHolder = new OutgoingVH(vTask);
                break;
            default:
                vInstruction = inflater.inflate(R.layout.message_incoming, parent, false);
                viewHolder = new DateHeaderVH(vInstruction);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        DateHeaderVH dateHeaderVH;

        switch (viewHolder.getItemViewType()) {
            case DATE_HEADER:
                dateHeaderVH = (DateHeaderVH) viewHolder;
                configureDateHeader(dateHeaderVH, position);
                break;
            case INCOMING:
                IncomingVH incomingVH = (IncomingVH) viewHolder;
                configureIncoming(incomingVH, position);
                break;
            case OUTGOING:
                OutgoingVH outgoingVH = (OutgoingVH) viewHolder;
                configureOutgoing(outgoingVH, position);
                break;
            default:
                dateHeaderVH = (DateHeaderVH) viewHolder;
                configureDateHeader(dateHeaderVH, position);
                break;
        }

    }

    private void configureDateHeader(final DateHeaderVH dateHeaderVH, final int position) {
        Message message = messages.get(position);
    }

    private void configureIncoming(final IncomingVH incomingVH, final int position) {

        final Message message = messages.get(position);
        if(position==(messages.size()-1))
                animate(incomingVH.background);

        if(message.getText()!=null&&!message.getText().trim().isEmpty()) {
            incomingVH.message.setVisibility(View.VISIBLE);
            String text=Chat.formatString(message.getText());
            boolean formatted=text!=null&&!text.equalsIgnoreCase(message.getText());
            incomingVH.message.setText("" + (formatted? Html.fromHtml(text):text));
            //incomingVH.message.setMovementMethod(LinkMovementMethod.getInstance());
            Linkify.addLinks(incomingVH.message, Linkify.ALL);
        }
        else {
            incomingVH.message.setVisibility(View.GONE);
        }
        incomingVH.name.setText(message.getUser().getName());
        incomingVH.name.setTextColor(mContext.getResources().getColor(randomColor()));
        incomingVH.status.setVisibility(View.GONE);
        incomingVH.background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteMessage(incomingVH.getAdapterPosition());
            }
        });

        //Log.d(TAG,"incoming message url: "+message.getImageUrl());
        if(message.getImageUrl()!=null&&!message.getImageUrl().trim().isEmpty()){
            if(message.getImageUrl().contains("http")){
            incomingVH.ivPlaceholder.setVisibility(View.GONE);
            incomingVH.imageView.setVisibility(View.VISIBLE);
            incomingVH.progressBar.setVisibility(View.GONE);

            Picasso.get().load(
                        (message.getImageUrl()))
                        .resize(800, 800)
                        .placeholder(R.drawable.blurred_image)
                    .centerCrop()
                    .into(incomingVH.imageView);

                incomingVH.imageView.setOnClickListener(new MyImageClickListener(message));
            }
            else {
                incomingVH.ivPlaceholder.setVisibility(View.VISIBLE);
                incomingVH.imageView.setVisibility(View.GONE);
                incomingVH.progressBar.setVisibility(View.VISIBLE);
                incomingVH.imageView.setOnClickListener(null);
            }

        }
        else {
            incomingVH.imageView.setVisibility(View.GONE);
            incomingVH.progressBar.setVisibility(View.GONE);
            incomingVH.ivPlaceholder.setVisibility(View.GONE);
            incomingVH.imageView.setOnClickListener(null);
        }

        incomingVH.time.setText(CreateForm.getInstance(mContext).getTime(message.getMillis()));
    }

    private void configureOutgoing(final OutgoingVH outgoingVH, final int position) {

        final Message message = messages.get(position);
        if(position==(messages.size()-1))
                animate(outgoingVH.background);

        if(message.getText()!=null&&!message.getText().trim().isEmpty()) {
            outgoingVH.message.setVisibility(View.VISIBLE);
            String text=Chat.formatString(message.getText());
            boolean formatted=text!=null&&!text.equalsIgnoreCase(message.getText());
           Log.d(TAG,"text: "+text+" formatted: "+formatted);
            outgoingVH.message.setText((formatted? Html.fromHtml(text):text));
            //outgoingVH.message.setMovementMethod(LinkMovementMethod.getInstance());
            Linkify.addLinks(outgoingVH.message, Linkify.ALL);
        }
        else {
            outgoingVH.message.setVisibility(View.GONE);
        }
        outgoingVH.name.setText(message.getUser().getName());
        outgoingVH.name.setTextColor(mContext.getResources().getColor(randomColor()));
        outgoingVH.status.setText(resolveStatus(message.getStatus()));
        outgoingVH.status.setTextColor(mContext.getResources().getColor(getStatusColor(message.getStatus())));
        outgoingVH.background.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                deleteMessage(outgoingVH.getAdapterPosition());
                return true;
            }
        });

        Log.d(TAG,"outgoing message url: "+message.getImageUrl());
        if(message.getImage()!=null&&message.getImageUrl()!=null&&!message.getImageUrl().trim().isEmpty()){

            if(message.getImageUrl().trim().contains("http")){
                Log.d(TAG,"outgoing show img");
                outgoingVH.ivPlaceholder.setVisibility(View.GONE);
                outgoingVH.imageView.setVisibility(View.VISIBLE);
                outgoingVH.progressBar.setVisibility(View.GONE);

                Picasso.get().load(
                        (message.getImageUrl()))
                        .resize(800, 800)
                        .centerCrop()
                        .placeholder(R.drawable.blurred_image)
                        .into(outgoingVH.imageView);

                outgoingVH.imageView.setOnClickListener(new MyImageClickListener(message));
            }
            else {
                Log.d(TAG,"outgoing show img progress");
                outgoingVH.ivPlaceholder.setVisibility(View.VISIBLE);
                outgoingVH.imageView.setVisibility(View.GONE);
                outgoingVH.progressBar.setVisibility(View.VISIBLE);

                outgoingVH.imageView.setOnClickListener(null);
            }

            // new MyPicasso(mContext, incomingVH.imageView, message.getImageUrl());

        }
        else {
            Log.d(TAG,"outgoing hide img");
            outgoingVH.imageView.setVisibility(View.GONE);
            outgoingVH.progressBar.setVisibility(View.GONE);
            outgoingVH.ivPlaceholder.setVisibility(View.GONE);
            outgoingVH.imageView.setOnClickListener(null);
        }

        outgoingVH.time.setText(CreateForm.getInstance(mContext).getTime(message.getMillis()));

        message.setListener(null);
        if(message.getStatus()!=null&&!message.getStatus().equalsIgnoreCase(Message.STATUS_DELIVERED)) {
            message.setListener(new Message.MessageListener() {
                @Override
                public void status(String result) {
                    Log.d(TAG,"status result: "+result);
                    if(outgoingVH!=null&&outgoingVH.status!=null) {
                        if (result != null) {
                            try {
                                JSONObject jsonObject=new JSONObject(result);
                                String error=jsonObject.getString("error");
                                String image_url=jsonObject.getString("image_url");

                                if(error!=null){
                                    if(error.trim().equalsIgnoreCase("1")){
                                        PushReceiver.playtone(mContext,R.raw.message_sent);
                                       message.setStatus(Message.STATUS_DELIVERED);
                                       if(image_url!=null&&!image_url.trim().isEmpty()&&!image_url.equalsIgnoreCase("null")){
                                           Log.d(TAG,"updating img url: "+image_url);
                                           Image image=new Image();
                                           image.setImage_url(image_url);
                                           message.setImage(image);
                                       }
                                    }
                                    else {
                                        message.setStatus(Message.STATUS_PENDING);
                                    }
                                    databaseHelper.updateMessage(message);
                                    outgoingVH.status.setText(resolveStatus(message.getStatus()));
                                    message.setListener(null);//remove listener

                                    notifyDataSetChanged();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {
                            Log.d(TAG,"retrying");
                            (new Handler()).postDelayed(new PersistentRunnable(message, message.getListener()), 60000);
                        }
                    }
                }
            });

            sendMessage(message,message.getListener());
        }
        else {

        }
    }

    private class PersistentRunnable implements Runnable{
        Message message;
        Message.MessageListener listener;
        PersistentRunnable(Message message,Message.MessageListener listener){
            this.message=message;
            this.listener=listener;
        }
        @Override
        public void run() {
            if(message!=null) {
             Log.d(TAG,"repeating message send "+message.getText()+" grp_id: "+message.getGroup_id());
                //sendMessage(message, message.getListener());
            }
            }
    }

    private int getStatusColor(String status){
        if(status!=null&&status.equalsIgnoreCase(Message.STATUS_DELIVERED))
            return R.color.colorAccentLight;
        else
            return R.color.disabledBlack;
    }

    private String resolveStatus(String status){
        if(status!=null){
            if (status.equalsIgnoreCase(Message.STATUS_DELIVERED))
                return mContext.getResources().getString(R.string.delivered);
            else if (status.equalsIgnoreCase(Message.STATUS_PENDING))
                return mContext.getResources().getString(R.string.pending);
            else
                return null;
        }
        else
            return null;
    }

    private void sendMessage(Message message, Message.MessageListener listener){
       Log.d(TAG,"send message id: "+message.getText());
        networkRequest.sendMessage(message,listener);
    }

    @Override
    public int getItemViewType(int position) {

        Message message= messages.get(position);
            if(message!=null&&message.getUser().getId()!=null) {
                if (message.getUser().getId().trim().equalsIgnoreCase(user_id))
                    return OUTGOING;
                else
                    return INCOMING;
            }

                return -1;
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    static class DateHeaderVH extends RecyclerView.ViewHolder {
        public TextView date;

        public DateHeaderVH(View itemView) {
            super(itemView);

              date = itemView.findViewById(R.id.tv_message);
        }
    }

    static class IncomingVH extends RecyclerView.ViewHolder {
        public TextView message;
        public TextView time;
        public TextView name;
        public TextView status;
        public ImageView imageView;
        public ImageView ivPlaceholder;
        public View background;
        public ProgressBar progressBar;
        public IncomingVH(View itemView) {
            super(itemView);
              message = itemView.findViewById(R.id.tv_message);
              name = itemView.findViewById(R.id.tv_name);
              status = itemView.findViewById(R.id.tv_status);
              time = itemView.findViewById(R.id.tv_time);
            imageView=itemView.findViewById(R.id.iv_image);
            background=itemView.findViewById(R.id.v_background);
            progressBar=itemView.findViewById(R.id.progressBar);
            ivPlaceholder=itemView.findViewById(R.id.iv_placeholder);
        }
    }

    static class OutgoingVH extends RecyclerView.ViewHolder {
        public TextView message;
        public TextView time;
        public TextView name;
        public TextView status;
        public ImageView imageView;
        public ImageView ivPlaceholder;
        public View background;
        public ProgressBar progressBar;
        public OutgoingVH(View itemView) {
            super(itemView);
              message = itemView.findViewById(R.id.tv_message);
              name = itemView.findViewById(R.id.tv_name);
              status = itemView.findViewById(R.id.tv_status);
              time = itemView.findViewById(R.id.tv_time);
            imageView=itemView.findViewById(R.id.iv_image);
            background=itemView.findViewById(R.id.v_background);
            progressBar=itemView.findViewById(R.id.progressBar);
            ivPlaceholder=itemView.findViewById(R.id.iv_placeholder);
        }
    }

    private void deleteMessage(final int position){
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.message_options);
        dialog.findViewById(R.id.tv_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Message message=messages.get(position);

                if(databaseHelper.deleteItem(DatabaseHelper.TBL_MESSAGES,message.getId())){
                    messages.remove(position);
                    MyApplication.showToast(mContext,mContext.getResources().getString(R.string.deleting), Gravity.CENTER);
                    notifyDataSetChanged();
                }

                dialog.cancel();
            }
        });

        dialog.show();
    }

    class MyImageClickListener implements View.OnClickListener{
        Message message;
        MyImageClickListener(Message message){
            this.message=message;
        }
        @Override
        public void onClick(View view) {
                showDialog(message);
        }
    }

    private void initImageDialog(){
        dialogImage = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialogImage.setContentView(R.layout.image_preview);
          mImage = dialogImage.findViewById(R.id.image);
        dialogImage.findViewById(R.id.ib_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogImage.cancel();
            }
        });

          dialogTitle = dialogImage.findViewById(R.id.tv_caption);

    }

    private void showDialog(Message message){

        if(message.getImage()!=null) {
         if(message.getImageUrl()!=null)
             Picasso.get().load(
                     (message.getImageUrl()))
                     .resize(800, 800)
                     .centerInside()
                     .placeholder(R.drawable.blurred_image)
                     .into(mImage);
         else
             return;

         if(message.getUser()!=null)
            dialogTitle.setText(message.getUser().getName());
        else
            dialogTitle.setText(mContext.getResources().getText(R.string.preview));

        dialogImage.show();
        }
    }

    private void animate(View v){
        /*YoYo.with(Techniques.FadeIn)
                .duration(200)
                .playOn(v);*/
    }

    private class MyPicassoTarget implements Target{
        ImageView imageView;
        Message message;
        MyPicassoTarget(Message message,ImageView imageView){
            this.imageView=imageView;
            this.message=message;
        }
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            Log.d(TAG,"bitmap target");
            imageView.setImageBitmap(bitmap);
            saveImageToDevice(message,bitmap);
        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
            imageView.setVisibility(View.GONE);
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            imageView.setVisibility(View.GONE);
        }
    }

    private void saveImageToDevice(Message message,Bitmap bitmap){
        ContextWrapper cw = new ContextWrapper(mContext);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,message.getMillis()+".jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

          Image image = new Image();
        image.setImage_url(mypath.getPath());
        message.setImage(image);
        message.setStatus(Message.STATUS_DELIVERED);
        databaseHelper.updateMessage(message);//updates message since instance of message already exists in db
    }

    class MyPicasso{
        Context context;
        ImageView imageView;
        String url;

        MyPicasso(Context context, ImageView imageView, String url){
            this.context=context;
            this.imageView=imageView;
            this.url=url;
            Picasso.get().load(
                    (this.url))
                    .resize(800, 800)
                    .into(this.imageView);

        }
    }


    private int randomColor(){
        int[]colors={R.color.amber,R.color.redlight,R.color.blue,R.color.colorPrimary};
        return colors[(new Random()).nextInt((colors.length-1))];
    }

    /*
        if(message.getImageUrl()!=null){
            Log.d(TAG,"message url: "+message.getImageUrl()+" message uri: "+message.getImage().getUri());
            outgoingVH.imageView.setVisibility(View.VISIBLE);
            outgoingVH.progressBar.setVisibility(View.VISIBLE);
            if(message.getImage().getUri()!=null){
                Picasso.with(outgoingVH.imageView.getContext()).load(message.getImage().getUri())
                        .resize(800, 800)
                        .noFade()
                        .into(outgoingVH.imageView);
            }
            else {
                Picasso.with(outgoingVH.imageView.getContext()).load(
                        (new File(message.getImageUrl())))
                        .resize(800, 800)
                        .noFade()
                        .into(outgoingVH.imageView);
            }

            outgoingVH.imageView.setOnClickListener(new MyImageClickListener(message));
        }
        else {
            outgoingVH.imageView.setVisibility(View.GONE);
            outgoingVH.imageView.setOnClickListener(null);
            outgoingVH.progressBar.setVisibility(View.GONE);
        }*/
}