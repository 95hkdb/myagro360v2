package com.hensongeodata.myagro360v2.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.hensongeodata.myagro360v2.Interface.HistoryTabListener;
import com.hensongeodata.myagro360v2.Interface.ItemTouchHelperViewHolder;
import com.hensongeodata.myagro360v2.Interface.Refresh;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.model.SharePrefManager;
import com.hensongeodata.myagro360v2.model.User;

import java.util.HashMap;
import java.util.List;

/**
 * Created by user1 on 8/2/2016.
 */

public class HistoryScanTabsRVCopy extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG = HistoryScanTabsRVCopy.class.getSimpleName();
    private List<HashMap<String,String>> items;
    private Context mContext;
    private HistoryTabListener listener;
    private Refresh refresh;
    private LocsmmanEngine locsmmanEngine;
    private int mode;

    User mUser;

    public HistoryScanTabsRVCopy(Context context, List<HashMap<String,String>> items, HistoryTabListener listener, Refresh refreshListener) {
        refresh=refreshListener;
        mContext=context;
        this.items =items;
        this.listener=listener;
        locsmmanEngine=new LocsmmanEngine(context);
    }

    public void updateItem(int position,HashMap<String,String> map){
        int index=0;
        for(HashMap<String, String> item: items) {
            item.put("focus", ""+0);
            items.set(index,item);
            index++;
        }

        items.set(position,map);
        notifyDataSetChanged();
        //Log.d(TAG,"notifyDataSet");
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.history_scan_tab_item, parent, false);
//            View view = inflater.inflate(R.layout.map_history, parent, false);
            return new TabHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        String fname = SharePrefManager.getInstance(mContext).getUserDetails().get(1);
        if (holder instanceof TabHolder) {
            HashMap<String,String> item = items.get(position);
            TabHolder tabHolder = (TabHolder) holder;
            tabHolder.tvTag.setText(""+item.get("focus"));
            int value=Integer.parseInt(item.get("id"));
//            String strVal=""+value;           // else if(value<1000)
//                strVal="0"+value;
            String strVal = "";

//            Scouting naming
            if (value < 10 && mode == ChatbotAdapterRV.MODE_SCOUT)
                strVal = fname + " ScanModel " + value;
            else if (value < 100 && mode == ChatbotAdapterRV.MODE_SCOUT)
                strVal = fname + " ScanModel " + value;
            else if (value < 1000 && mode == ChatbotAdapterRV.MODE_SCOUT)
                strVal = fname + " ScanModel " + value;
            else if (value < 10000 && mode == ChatbotAdapterRV.MODE_SCOUT)
                strVal = fname + " ScanModel " + value;

//            Mapping naming
            else if (value < 10 && mode == ChatbotAdapterRV.MODE_MAP)
                strVal = fname + " Map " + value;
            else if (value < 100 && mode == ChatbotAdapterRV.MODE_MAP)
                strVal = fname + " Map " + value;
            else if (value < 1000 && mode == ChatbotAdapterRV.MODE_MAP)
                strVal = fname + " Map " + value;
            else if (value < 10000 && mode == ChatbotAdapterRV.MODE_MAP)
                strVal = fname + " Map " + value;

//            tabHolder.tvName.setText((mode==ChatbotAdapterRV.MODE_SCOUT?
//                    mContext.getResources().getString(R.string._scout)+" ":
//                    mContext.getResources().getString(R.string._mapping)+" ")+strVal);

            tabHolder.tvName.setText(strVal);

            tabHolder.ibDelete.setVisibility(mode==ChatbotAdapterRV.MODE_MAP? View.VISIBLE:View.GONE);
            tabHolder.ibDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeMapping(holder.getAdapterPosition());
                }
            });

            Log.d(TAG,"focus: "+item.get("focus")+" position: "+position);
            tabHolder.vIndicator.setVisibility(item.get("focus").equalsIgnoreCase("1")?View.VISIBLE:View.INVISIBLE);

            tabHolder.background.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                 selectItem(holder.getAdapterPosition());
                }
            });

        }
    }


    private void removeMapping(final int position){
        HashMap<String,String> item = items.get(position);
        final String header=item.get("value");
        String tag=item.get("tag");
        new AlertDialog.Builder(mContext)
                .setTitle(tag)
                .setMessage("Do you want to remove this map?")
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if ((new DatabaseHelper(mContext)).removeScoutListItem(header, ChatbotAdapterRV.MODE_MAP)){
                            MyApplication.showToast(mContext, mContext.getResources().getString(R.string.success), Gravity.CENTER);
//                            items.remove(position);
//                            notifyDataSetChanged();

                            refresh.reload(null);
                        }
                        else
                            MyApplication.showToast(mContext,mContext.getResources().getString(R.string.failed), Gravity.CENTER);
                    }
                })
                .show();
    }


    private void selectItem(int position){
        HashMap<String, String> mapOld=items.get(position);
        //String value=items.get(position).get("value");
        listener.onSelected(mapOld.get("value"));
        //mapMask.put("value",value);
        HashMap<String,String> mapMask=new HashMap<>();
        mapMask.put("focus",""+1);
        mapMask.put("id",mapOld.get("id"));
        mapMask.put("value",mapOld.get("value"));

        updateItem(position,mapMask);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class TabHolder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder {
        int index;
        public View background;
        public TextView tvTag;
        public TextView tvName;
        public ImageButton ibDelete;
        public View vIndicator;

        public TabHolder(View itemView) {
            super(itemView);
            background=itemView.findViewById(R.id.background);
              tvTag = itemView.findViewById(R.id.tv_tag);
              tvName = itemView.findViewById(R.id.tv_name);
            ibDelete=itemView.findViewById(R.id.ib_delete);
            vIndicator=itemView.findViewById(R.id.v_indicator);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

}