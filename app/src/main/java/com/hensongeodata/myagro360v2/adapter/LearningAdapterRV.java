package com.hensongeodata.myagro360v2.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.KnowledgeBase;
import com.hensongeodata.myagro360v2.view.LearnContent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 8/2/2016.
 */
public class LearningAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
      private static String TAG = LearningAdapterRV.class.getSimpleName();
      private List<KnowledgeBase> kb_topic_list;
      private List<KnowledgeBase> filteredList;
      private Context mContext;
      private OrgHolder orgHolder;

      public LearningAdapterRV(Context context, List<KnowledgeBase> items) {
            this.mContext = context;
            this.kb_topic_list = items;
            this.filteredList = items;
      }

      @Override
      public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            Context context = parent.getContext();
            LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.kb_topic_item, parent, false);
            return new OrgHolder(view);
      }

      @Override
      public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
            final KnowledgeBase knowledgeBase = filteredList.get(position);
            orgHolder = (OrgHolder) holder;
            Log.d(TAG, "item" + position + ": " + knowledgeBase.getTitle());
            orgHolder.name.setText(knowledgeBase.getTitle());

            orgHolder.background.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                        showDetails(holder.getAdapterPosition());
                  }
            });
      }

      private void showDetails(int position) {
            Intent intent = new Intent(mContext, LearnContent.class);
            intent.putExtra("kb_id", position);
            mContext.startActivity(intent);
      }

      public void search(String keyword) {
            Log.d(TAG, "keyword: " + keyword);
            filteredList = new ArrayList<>();
            if (keyword != null && !keyword.trim().isEmpty()) {
                  keyword = keyword.toLowerCase();
                  keyword = keyword.trim();
            }
            if (keyword == null || keyword.isEmpty()) {
                  //reset filtered list to original list
                  filteredList = kb_topic_list;
            } else {
                  for (int a = 0; a < kb_topic_list.size(); a++) {
                        if (kb_topic_list.get(a).getTitle() != null && kb_topic_list.get(a).getTitle() != null) {
                              if (kb_topic_list.get(a).getTitle().toLowerCase().contains(keyword)
                                      || kb_topic_list.get(a).getTitle().toLowerCase().contains(keyword)) {
                                    filteredList.add(kb_topic_list.get(a));
                              }
                        }
                  }
            }
            Log.d(TAG, "filterList: " + filteredList.size());
            notifyDataSetChanged();
      }

      @Override
      public int getItemCount() {
            return filteredList.size();
      }

      class OrgHolder extends RecyclerView.ViewHolder {
            int index;
            public View background;
            public View container;
            public TextView name;

            public OrgHolder(View itemView) {
                  super(itemView);
                  container = itemView.findViewById(R.id.container);
                  background = itemView.findViewById(R.id.background);
                  name = itemView.findViewById(R.id.tv_name);
            }
      }

}