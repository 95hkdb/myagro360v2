package com.hensongeodata.myagro360v2.adapter;

import android.content.Context;
import android.os.Build;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.R;

import java.util.ArrayList;
import java.util.List;

public class GridCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> items = new ArrayList<>();

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, String obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public GridCategoryAdapter(Context context, List<String> items) {
        this.items = items;
        ctx = context;
    }

    public GridCategoryAdapter(Context context, List<String> items, OnItemClickListener listener) {
        this.items = items;
        ctx = context;
        this.mOnItemClickListener = listener;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public ImageView image;
        public TextView head;
        public TextView subtitle;
        public View lyt_parent;

        public OriginalViewHolder(View v) {
            super(v);
            image = v.findViewById(R.id.home_card_image);
            head = v.findViewById(R.id.head);
            subtitle = (TextView) v.findViewById(R.id.subtitle);
            lyt_parent = (View) v.findViewById(R.id.lyt_parent);
            cardView = v.findViewById(R.id.card_view);
        }

        public void bind(final String item , final OnItemClickListener listener ){

            lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                 int pos =   getAdapterPosition();
                    listener.onItemClick(v,item, pos);
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_item_cards, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;

            view.bind(items.get(position), mOnItemClickListener);

            String text = items.get(position);
            view.cardView.setCardBackgroundColor(ctx.getResources().getColor(R.color.colorGreen005));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                switch(text){
                    case "RECORD AN ACTIVITY":
                        view.image.setImageDrawable(ctx.getDrawable(R.drawable.record_white));
                        view.subtitle.setText("Undertake an activity on your farm.");
                        break;

                    case "SCAN FOR PESTS":
                        view.image.setImageDrawable(ctx.getDrawable(R.drawable.ic_scan_white));
                        view.subtitle.setText("Scan a strange insect on your farm.");
                        break;

                    case "MAP YOUR FARM":
                        view.image.setImageDrawable(ctx.getDrawable(R.drawable.ic_map_white));
                        view.subtitle.setText("Get to know your farm location and size.");
                        break;

                }
            }

            view.head.setText(text);
            view.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, items.get(position), position);
                        Log.i("GridCategoryAdapter", "onAddMapButtonClicked:  Item Clicked" );
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}