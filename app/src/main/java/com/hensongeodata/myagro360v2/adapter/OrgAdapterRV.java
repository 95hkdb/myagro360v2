package com.hensongeodata.myagro360v2.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.BroadcastCall;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.SwipeDismissTouchListener;
import com.hensongeodata.myagro360v2.model.Organization;
import com.hensongeodata.myagro360v2.model.User;
import com.hensongeodata.myagro360v2.view.OrgDetail;
import com.hensongeodata.myagro360v2.view.OrgList;

/**
 * Created by user1 on 8/2/2016.
 */
public class OrgAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=OrgAdapterRV.class.getSimpleName();
    private List<Organization> orgList;
    private List<Organization> filteredList;
    private Context mContext;
    private OrgHolder orgHolder;

    public OrgAdapterRV(Context context, List<Organization> items) {
        mContext=context;
        orgList =items;
        filteredList=items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.org_item, parent, false);
            return new OrgHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
           final Organization organization = filteredList.get(position);
            orgHolder =(OrgHolder)holder;
            Log.d(TAG,"item"+position+": "+organization.getName());
            orgHolder.name.setText(organization.getName());

            orgHolder.count.setText(organization.getFormCount()+(organization.getFormCount()!=null&&Integer.parseInt(organization.getFormCount())==1? " Form available ":" Forms available"));

        addSwipeDismiss(orgHolder.background,holder.getAdapterPosition());
            orgHolder.background.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showForms(holder.getAdapterPosition());
                }
            });

            orgHolder.bgInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDetails(holder.getAdapterPosition());
                }
            });

        orgHolder.ibInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDetails(holder.getAdapterPosition());
            }
        });
    }

    private void showDetails(int position){
        Organization organization= filteredList.get(position);

        Intent intent=new Intent(mContext, OrgDetail.class);
        intent.putExtra("org_id",organization.getId());
        mContext.startActivity(intent);
    }

    private void showForms(final int position){
        Organization organization= filteredList.get(position);

        Intent intent=new Intent(BroadcastCall.ORGLIST);
        intent.setAction(BroadcastCall.ORGLIST);
        intent.putExtra("orgID",organization.getId());
        intent.putExtra("action", OrgList.ACTION_SELECT);
        mContext.sendBroadcast(intent);
    }


    private void addSwipeDismiss(View view, final int position){
        // Create a generic swipe-to-dismiss touch listener.
        view.setOnTouchListener(new SwipeDismissTouchListener(
                view,
                null,
                new SwipeDismissTouchListener.DismissCallbacks() {
                    @Override
                    public boolean canDismiss(Object token) {
                        return true;
                    }

                    @Override
                    public void onDismiss(View view, Object token) {
                        showPop(view,position);
                    }
                }));
        //dismissableContainer.addView(view);
    }

    private void showPop(View v,int position){
        final Organization organization= filteredList.get(position);

        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.confirm_dialog);
        ((TextView)dialog.findViewById(R.id.tv_caption)).setText(organization.getName());
          final EditText etPassword = dialog.findViewById(R.id.et_password);
          Button btnCancel = dialog.findViewById(R.id.btn_negative);
          Button btnDelete = dialog.findViewById(R.id.btn_positive);


        btnCancel.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_grey));
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btnDelete.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_red));
        btnDelete.setTextColor(mContext.getResources().getColor(R.color.primaryWhite));
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password=etPassword.getText().toString().trim();
                if(password.isEmpty()){
                    Toast.makeText(mContext,"Password was empty ",Toast.LENGTH_SHORT).show();
                }
                else if(password.equals((new User(mContext)).getPassword())){
                    deleteOrg(v,organization);
                }
                else{
                    Toast.makeText(mContext,"Password was incorrect",Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }
    private void deleteOrg(View v,Organization organization){

        if((new DatabaseHelper(mContext)).deleteItem(DatabaseHelper.TBL_ORGANIZATION,organization.getId())){
        v.setVisibility(View.GONE);
        Toast.makeText(mContext,organization.getName()+" Deleted successfully.",Toast.LENGTH_SHORT).show();
            }
        else {
        v.setVisibility(View.VISIBLE);
        Toast.makeText(mContext,organization.getName()+" Failed to delete.",Toast.LENGTH_SHORT).show();
    }
    }

    public void search(String keyword){
        Log.d(TAG,"keyword: "+keyword);
        filteredList=new ArrayList<>();
        if(keyword!=null&&!keyword.trim().isEmpty()) {
            keyword = keyword.toLowerCase();
            keyword=keyword.trim();
        }
        if(keyword==null||keyword.isEmpty()){
            //reset filtered list to original list
            filteredList=orgList;
        }
        else {
            for(int a=0;a<orgList.size();a++){
                if(orgList.get(a).getName()!=null&&orgList.get(a).getDescription()!=null) {
                    if (orgList.get(a).getName().toLowerCase().contains(keyword)
                            || orgList.get(a).getDescription().toLowerCase().contains(keyword)) {
                        filteredList.add(orgList.get(a));
                    }
                }
            }
        }
        Log.d(TAG,"filterList: "+filteredList.size());
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    class OrgHolder extends RecyclerView.ViewHolder {
        int index;
        public View background;
        public View container;
        public View bgInfo;
        public TextView name;
        public TextView count;
        public ImageButton ibInfo;


        public OrgHolder(View itemView) {
            super(itemView);
            container=itemView.findViewById(R.id.container);
            background=itemView.findViewById(R.id.background);
            bgInfo =itemView.findViewById(R.id.bg_info);
              name = itemView.findViewById(R.id.tv_name);
              count = itemView.findViewById(R.id.tv_form_count);
              ibInfo = itemView.findViewById(R.id.ib_info);
        }
    }

}