package com.hensongeodata.myagro360v2.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.model.Image;
import com.hensongeodata.myagro360v2.model.InstructionScout;

import java.util.ArrayList;

/**
 * Created by user1 on 8/2/2016.
 */
public class ScoutAdapterRV extends ChatbotAdapterRV {
    private static String TAG=ScoutAdapterRV.class.getSimpleName();
    private ArrayList<InstructionScout> items;
    private Context mContext;
    private LocsmmanEngine locsmmanEngine;
    private final int INSTRUCTION = 0, TASK =1, INFORMATION=2;
    private int mode;
    public static int MODE_MAP=0, MODE_SCOUT=1;


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        RecyclerView.ViewHolder viewHolder;
        View vInstruction;
        View vTask;
        switch (viewType) {
            case INSTRUCTION:
                vInstruction = inflater.inflate(R.layout.instruction, parent, false);
                viewHolder = new InstructionVH(vInstruction);
                break;
            case INFORMATION:
                View vInformation = inflater.inflate(R.layout.information, parent, false);
                viewHolder = new InformationVH(vInformation);
                break;
            case TASK:
                if(getMode()==MODE_SCOUT)
                vTask = inflater.inflate(R.layout.instruction_scout_reverse,parent, false);
                else
                    vTask = inflater.inflate(R.layout.instruction_mapping_reverse,parent, false);
                viewHolder = new TaskVH(vTask);
                break;
            default:
                vInstruction = inflater.inflate(R.layout.instruction, parent, false);
                viewHolder = new InstructionVH(vInstruction);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        InstructionVH instructionVH;

        switch (viewHolder.getItemViewType()) {
            case INSTRUCTION:
                instructionVH = (InstructionVH) viewHolder;
                configureInstruction(instructionVH, position);
                break;
            case INFORMATION:
                InformationVH informationVH = (InformationVH) viewHolder;
                configureInformation(informationVH, position);
                break;
            case TASK:
                TaskVH taskVH = (TaskVH) viewHolder;
                configureTask(taskVH, position);
                break;
            default:
                instructionVH = (InstructionVH) viewHolder;
                configureInstruction(instructionVH, position);
                break;
        }

    }

    private void configureInstruction(final InstructionVH instructionVH, final int position) {
        InstructionScout instruction=items.get(position);
        instructionVH.message.setText(instruction.getMessage());
        //locsmmanEngine.showView(instructionVH.background,true);
    }


    private void configureInformation(final InformationVH informationVH, final int position) {

        InstructionScout info=items.get(position);

            informationVH.message.setText(info.getMessage());
            Image image=info.getImage();
            informationVH.image.setImageResource(image.getSrc());

        //locsmmanEngine.showView(informationVH.background,true);
    }

    private void configureTask(final TaskVH taskVH, final int position) {

        if(getMode()==MODE_SCOUT) {
            InstructionScout task = items.get(position);

            taskVH.message.setText(task.getMessage() == null || task.getMessage().trim().isEmpty() ?
                    "ANALYSING IMAGE" : task.getMessage());

            taskVH.vScanning.setVisibility(task.getMessage() == null || task.getMessage().trim().isEmpty() ?
                    View.VISIBLE : View.GONE);

            // taskVH.date.setText(task.getMessage());

            Log.d(TAG, "task: " + task.getId() + " date: " + task.getMessage());
        }
        else if(getMode()==MODE_MAP){

        }
       // locsmmanEngine.showView(taskVH.background,true);
    }


    @Override
    public int getItemViewType(int position) {

        InstructionScout item= items.get(position);

            if (item.getAction()==InstructionScout.ACTION_TASK)
                return INSTRUCTION;
            else if(item.getAction()==InstructionScout.ACTION_INSTRUCT)
                return INSTRUCTION;
            else if(item.getAction()==InstructionScout.ACTION_SCAN)
                return TASK;
            else if(item.getAction()==InstructionScout.ACTION_INFO)
                return INFORMATION;
            else
                return -1;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class InstructionVH extends RecyclerView.ViewHolder {
        public TextView message;
        public TextView description;
        public View background;

        public InstructionVH(View itemView) {
            super(itemView);

              message = itemView.findViewById(R.id.tv_message);
            //description= (TextView) itemView.findViewById(R.id.tv_description);
            background=itemView.findViewById(R.id.v_background);
        }
    }

    static class InformationVH extends RecyclerView.ViewHolder {
        public TextView message;
        public ImageView image;
        public View background;

        public InformationVH(View itemView) {
            super(itemView);

              message = itemView.findViewById(R.id.tv_message);
              image = itemView.findViewById(R.id.iv_image);
            background=itemView.findViewById(R.id.v_background);

        }
    }

    static class TaskVH extends RecyclerView.ViewHolder {
        public TextView message;
        public View vScanning;
        public View background;

        public TaskVH(View itemView) {
            super(itemView);
              message = itemView.findViewById(R.id.tv_message);
            vScanning= itemView.findViewById(R.id.v_scanning);
            background=itemView.findViewById(R.id.v_background);
        }
    }
}