package com.hensongeodata.myagro360v2.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;


import com.github.marlonlom.utilities.timeago.TimeAgo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Archive;
import com.hensongeodata.myagro360v2.view.PreviewForm;

/**
 * Created by user1 on 8/2/2016.
 */
public class ArchiveAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=ArchiveAdapterRV.class.getSimpleName();
    private List<Object> archiveList;
    private List<Object> filteredList;
    private List<Archive> pendingList;
    private Context mContext;
    private final int HEADER = 0, SECTION=1, SECTION_EXTENDED=2;

    public ArchiveAdapterRV(Context context, List<Object> items) {

        mContext=context;
        this.archiveList =items;
        //List<Object> filteredObject=new ArrayList<>();
        this.filteredList=items;
        pendingList=new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        RecyclerView.ViewHolder viewHolder;
        View vSection;
        switch (viewType) {
            case HEADER:
                View vHeaderMain = inflater.inflate(R.layout.top_action, parent, false);
                viewHolder = new HeadHolder(vHeaderMain);
                break;
            case SECTION:
                vSection = inflater.inflate(R.layout.archive_item,parent, false);
                viewHolder = new ArchiveHolder(vSection);
                break;
            case SECTION_EXTENDED:
                vSection = inflater.inflate(R.layout.archive_item_extended,parent, false);
                viewHolder = new ArchiveHolder(vSection);
                break;
            default:
                vSection = inflater.inflate(R.layout.archive_item,parent, false);
                viewHolder = new ArchiveHolder(vSection);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        ArchiveHolder archiveHolder;
        switch (holder.getItemViewType()) {
            case HEADER:
                HeadHolder headerMainVH = (HeadHolder) holder;
                configureHeader(headerMainVH, position);
                break;
            case SECTION:
                archiveHolder = (ArchiveHolder) holder;
                configureSection(archiveHolder, position);
                break;
            case SECTION_EXTENDED:
                archiveHolder = (ArchiveHolder) holder;
                configureSection(archiveHolder, position);
                break;
            default:
                archiveHolder = (ArchiveHolder) holder;
                configureSection(archiveHolder, position);
                break;
        }
    }


    private void configureHeader(final HeadHolder headerHolder, final int position) {

        final HashMap<String, Boolean> state= (HashMap<String, Boolean>) archiveList.get(position);

        headerHolder.tvTag.setText("Select all");

        headerHolder.cbCheck.setChecked(state.get("value"));
        headerHolder.cbCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG,"check clicked");
                updateCheckAll(!state.get("value"));
            }
        });
        headerHolder.background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateCheckAll(!headerHolder.cbCheck.isChecked());
            }
        });
    }

    private void configureSection(final ArchiveHolder archiveHolder, final int position) {

        Archive archive = (Archive) filteredList.get(position);

        archiveHolder.tvName.setText(archive.getName());
        long millis= MyApplication.getMillis(archive.getDate(),archive.getTime());
        archiveHolder.tvDate.setText(millis==-1? archive.getDate(): TimeAgo.using(millis));
        //archiveHolder.tvDate.setText(archive.getDate());
        archiveHolder.tvOrg.setText(archive.getOrganization().getName());

        archiveHolder.background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewForm(archiveHolder.getAdapterPosition());
            }
        });

        archiveHolder.bgCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleCheck(archiveHolder.getAdapterPosition());
            }
        });

        archiveHolder.cbCheck.setChecked(archive.isChecked());
        archiveHolder.cbCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleCheck(archiveHolder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemViewType(int position) {

        if (position==0) {
          return HEADER;
        }
        else if ((position+1)==filteredList.size())
            return SECTION_EXTENDED;
        else  {
            return SECTION;
        }
    }

    public void search(String keyword){
        filteredList=new ArrayList<>();
        if(keyword!=null&&!keyword.trim().isEmpty()) {
            keyword = keyword.toLowerCase();
            keyword=keyword.trim();
        }
        if(keyword==null||keyword.isEmpty()){
            //reset filtered list to original list
            filteredList= archiveList;
        }
        else {
            for(int a = 1; a< archiveList.size(); a++){
                Archive archive= (Archive) archiveList.get(a);
                if(archive.getName()!=null&& archive.getOrganization()!=null) {
                    if (archive.getName().toLowerCase().contains(keyword)
                            || archive.getOrganization().getName().toLowerCase().contains(keyword)) {
                        filteredList.add(archiveList.get(a));
                    }
                }
            }
        }

        notifyDataSetChanged();
    }

    private void toggleCheck(int position){
        //set check status to what is wasn't previously
            ((Archive)filteredList.get(position)).setChecked(!((Archive)filteredList.get(position)).isChecked());
        notifyDataSetChanged();
        Boolean isAllChecked=true;

        for(int a=1;a<filteredList.size();a++){
            if(!((Archive)filteredList.get(a)).isChecked()){
                isAllChecked=false;
                break;
            }
        }
        ((HashMap<String, Boolean>)archiveList.get(0)).put("value",isAllChecked);

        notifyDataSetChanged();

        }

    private void updateCheckAll(Boolean check){
        ((HashMap<String, Boolean>)archiveList.get(0)).put("value",check);
        for(int a=1;a<filteredList.size();a++){
            ((Archive)filteredList.get(a)).setChecked(check);
        }

        notifyDataSetChanged();
    }

    public List<Archive>getPendingList(){
        ArrayList<Archive> mList=new ArrayList<>();
        for(int a=1;a<filteredList.size();a++)
            mList.add((Archive) filteredList.get(a));
        return mList;
    }

    private void previewForm(final int position){
        Archive archive= (Archive) filteredList.get(position);
       // Log.d(TAG,"transaction_id: "+archive.getTransaction_id());
        Intent intent=new Intent(mContext, PreviewForm.class);
        intent.putExtra("source", PreviewForm.SOURCE_ARCHIVE);
        intent.putExtra("transaction_id",archive.getTransaction_id());
        mContext.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    private class ArchiveHolder extends RecyclerView.ViewHolder {
        public View background;
        public View bgCheck;
        public TextView tvName;
        public TextView tvDate;
        public TextView tvOrg;
        public CheckBox cbCheck;

        public ArchiveHolder(View itemView) {
            super(itemView);
            background=itemView.findViewById(R.id.background);
              tvName = itemView.findViewById(R.id.tv_name);
              tvDate = itemView.findViewById(R.id.tv_date);
              tvOrg = itemView.findViewById(R.id.tv_organization);
              cbCheck = itemView.findViewById(R.id.cb_check);
            bgCheck=itemView.findViewById(R.id.bg_check);
        }
    }

    private class HeadHolder extends RecyclerView.ViewHolder {
        public View background;
        public TextView tvTag;
        public CheckBox cbCheck;

        public HeadHolder(View itemView) {
            super(itemView);
            background=itemView.findViewById(R.id.bg_top_action);
              tvTag = itemView.findViewById(R.id.tv_tag);
              cbCheck = itemView.findViewById(R.id.cb_check);
        }
    }
}