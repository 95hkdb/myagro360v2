package com.hensongeodata.myagro360v2.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.Field;
import com.hensongeodata.myagro360v2.model.FileField;
import com.hensongeodata.myagro360v2.model.PreviewState;
import com.hensongeodata.myagro360v2.model.Section;
import com.hensongeodata.myagro360v2.view.CameraVideo;

/**
 * Created by user1 on 8/2/2016.
 */
public class PreviewAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=PreviewAdapterRV.class.getSimpleName();
    private ArrayList<Object> previewList;
    private ArrayList<PreviewState> state;
    private Context mContext;
    private final int HEADER_MAIN = 0, HEADER_SUB=1, QUESTION_ANSWER = 2, IMAGE=3, VIDEO=4, MULTI_IMAGE=5;

    public PreviewAdapterRV(Context context, ArrayList<Object>previewList) {
        this.previewList =previewList;
        state=new ArrayList<>();
        for(int a=0;a<previewList.size();a++)
                state.add(new PreviewState(false));//set all view states of preview items to false until they are viewed

        mContext = context;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        RecyclerView.ViewHolder viewHolder;
        View vQuestionAnswer;
        switch (viewType) {
            case HEADER_MAIN:
                View vHeaderMain = inflater.inflate(R.layout.preview_section, parent, false);
                viewHolder = new HeaderMainVH(vHeaderMain);
                break;
            case HEADER_SUB:
                View vHeaderSub = inflater.inflate(R.layout.preview_section,parent, false);
                viewHolder = new HeaderSubVH(vHeaderSub);
                break;
            case QUESTION_ANSWER:
                vQuestionAnswer = inflater.inflate(R.layout.preview_question_answer,parent, false);
                viewHolder = new QuestionAnswerVH(vQuestionAnswer);
                break;
            case IMAGE:
                View vImage = inflater.inflate(R.layout.preview_image,parent, false);
                viewHolder = new ImageVH(vImage);
                break;
            case MULTI_IMAGE:
                View vImageMulti = inflater.inflate(R.layout.preview_image,parent, false);
                viewHolder = new ImageMultiVH(vImageMulti);
                break;
            case VIDEO:
                View vVideo = inflater.inflate(R.layout.preview_image,parent, false);
                viewHolder = new VideoVH(vVideo);
                break;
            default:
                vQuestionAnswer = inflater.inflate(R.layout.preview_question_answer,parent, false);
                viewHolder = new QuestionAnswerVH(vQuestionAnswer);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        QuestionAnswerVH questionAnswerVH;
        switch (viewHolder.getItemViewType()) {
            case HEADER_MAIN:
                HeaderMainVH headerMainVH = (HeaderMainVH) viewHolder;
                configureHeaderMain(headerMainVH, position);
                break;
            case HEADER_SUB:
                HeaderSubVH headerSubVH = (HeaderSubVH) viewHolder;
                configureHeaderSub(headerSubVH, position);
                break;
            case QUESTION_ANSWER:
                questionAnswerVH = (QuestionAnswerVH) viewHolder;
                configureQuestionAnswer(questionAnswerVH, position);
                break;
            case IMAGE:
                ImageVH imageVH = (ImageVH) viewHolder;
                configureImage(imageVH, position);
                break;
            case MULTI_IMAGE:
                ImageMultiVH imageMultiVH = (ImageMultiVH) viewHolder;
                configureImageMulti(imageMultiVH, position);
                break;
            case VIDEO:
                VideoVH videoVH = (VideoVH) viewHolder;
                configureVideo(videoVH, position);
                break;
            default:
                questionAnswerVH = (QuestionAnswerVH) viewHolder;
                configureQuestionAnswer(questionAnswerVH, position);
                Log.d(TAG,"position: "+position+" viewType: "+viewHolder.getItemViewType());
                break;
        }

    }

    private void configureHeaderMain(final HeaderMainVH headerMainVH, final int position) {

        Section section= (Section) previewList.get(position);
        headerMainVH.title.setText(section.getTitle());
        headerMainVH.description.setText(section.getDesc());
    }

    private void configureHeaderSub(final HeaderSubVH headerSubVH, final int position) {

        Section section= (Section) previewList.get(position);
        headerSubVH.title.setText(section.getTitle());
        headerSubVH.description.setText(section.getDesc());
    }

    private void configureQuestionAnswer(final QuestionAnswerVH questionAnswerVH, final int position) {

        Field field= (Field) previewList.get(position);
        questionAnswerVH.question.setText(field.getQuestion());
        questionAnswerVH.answer.setVisibility(View.GONE);

        questionAnswerVH.answersContainer.removeAllViews();
        questionAnswerVH.tvPlaceholder.setVisibility(View.GONE);

        if(field.getAnswers().size()==0)
            questionAnswerVH.tvPlaceholder.setVisibility(View.VISIBLE);
        else
            questionAnswerVH.answersContainer.addView(setAnswersView(position));

        state.get(position).setViewed(true);
    }

    private void configureImage(final ImageVH imageHolder, final int position) {

        Field field= (Field) previewList.get(position);
        ArrayList<String> answers=field.getAnswers();

        imageHolder.question.setText(field.getQuestion());

        Log.d(TAG,"answersSize: "+answers.size());
        if(answers.size()>0){
            String path_image=answers.get(0);
            Log.d(TAG,"path: "+path_image);
            if(path_image!=null&&!path_image.equalsIgnoreCase("")) {
//                imageHolder.image.setVisibility(View.VISIBLE);
//                Picasso.with(mContext).load(path_image).resize(800, 800).centerInside().into(imageHolder.image);
                imageHolder.image.setVisibility(View.VISIBLE);
               imageHolder.image.setImageBitmap((new MyApplication()).bitmapCompressLarge(path_image));
                imageHolder.tvPlaceholder.setVisibility(View.GONE);
            }
            else {
                imageHolder.tvPlaceholder.setVisibility(View.VISIBLE);
                imageHolder.image.setVisibility(View.GONE);
            }
        }
        else {
            imageHolder.tvPlaceholder.setVisibility(View.VISIBLE);
            imageHolder.image.setVisibility(View.GONE);
        }

    }
    private void configureVideo(final VideoVH videoHolder, final int position) {
        Field field= (Field) previewList.get(position);
        ArrayList<String> answers=field.getAnswers();

        videoHolder.question.setText(field.getQuestion());

        Log.d(TAG,"answersSize: "+answers.size());
        if(answers.size()>0){
            String path_image=answers.get(0);
            if(path_image!=null&&!path_image.equalsIgnoreCase("")) {
                videoHolder.image.setVisibility(View.VISIBLE);
                try {
                    videoHolder.image.setImageBitmap(CameraVideo.retriveVideoFrameFromVideo(path_image));
                    videoHolder.tvPlaceholder.setVisibility(View.GONE);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }

            }
            else {
                videoHolder.tvPlaceholder.setVisibility(View.VISIBLE);
                videoHolder.image.setVisibility(View.GONE);
            }
        }
        else {
            videoHolder.tvPlaceholder.setVisibility(View.VISIBLE);
            videoHolder.image.setVisibility(View.GONE);
        }

    }
    private void configureImageMulti(final ImageMultiVH imageMultiHolder, final int position) {

        Field field= (Field) previewList.get(position);
        ArrayList<String> answers=field.getAnswers();

        imageMultiHolder.question.setText(field.getQuestion());

        Log.d(TAG,"answersSize: "+answers.size());
        if(answers.size()>0){
            String path_image=answers.get(0);
            Log.d(TAG,"path: "+path_image);
            if(path_image!=null&&!path_image.equalsIgnoreCase("")) {
                imageMultiHolder.image.setVisibility(View.VISIBLE);
                imageMultiHolder.image.setImageBitmap((new MyApplication()).bitmapCompressLarge(path_image));
                imageMultiHolder.tvPlaceholder.setVisibility(View.GONE);
            }
            else {
                imageMultiHolder.tvPlaceholder.setVisibility(View.VISIBLE);
                imageMultiHolder.image.setVisibility(View.GONE);
            }
        }
        else {
            imageMultiHolder.tvPlaceholder.setVisibility(View.VISIBLE);
            imageMultiHolder.image.setVisibility(View.GONE);
        }
    }


    //Since there could be several answers, we loop through to display each answer one after the other
    private LinearLayout setAnswersView(int position){
        LinearLayout baseView=new LinearLayout(mContext);
        baseView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        baseView.setOrientation(LinearLayout.VERTICAL);

        Field field= (Field) previewList.get(position);
        ArrayList<String> answers=field.getAnswers();
        for(int a=0;a<answers.size();a++) {
            View answerView = (LayoutInflater.from(mContext)).inflate(R.layout.preview_answer_item, null);
            ((TextView)answerView.findViewById(R.id.tv_answer)).setText(answers.get(a));
            baseView.addView(answerView);
        }
        return baseView;
        }

    @Override
    public int getItemViewType(int position) {

        Object item= previewList.get(position);
        if (item instanceof Section) {
            Log.d(TAG,"position: "+position+" type: section");
            if(((Section)item).getType()==Section.MAIN)
                return HEADER_MAIN;
            else
                return HEADER_SUB;
        }
        else if(item instanceof FileField){
            String file_type=((FileField)item).getType().trim();
            Log.d(TAG,"position: "+position+" type: filefield  type: "+file_type);
            if(file_type!=null&&file_type.equalsIgnoreCase(FileField.FILE_IMAGE.trim()))
                return IMAGE;
            else if(file_type!=null&&file_type.equalsIgnoreCase(FileField.FILE_MULTI_IMAGE.trim()))
                return MULTI_IMAGE;
            else if(file_type!=null&&file_type.equalsIgnoreCase(FileField.FILE_VIDEO.trim()))
                return VIDEO;
            else
                return -1;
        }
        else if (item instanceof Field) {
            Log.d(TAG,"position: "+position+" type: QandA");
            return QUESTION_ANSWER;
        }

        return -1;
    }

    @Override
    public int getItemCount() {
        return previewList.size();
    }

    private Context getContext() {
        return mContext;
    }

    static class HeaderMainVH extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView description;

        public HeaderMainVH(View itemView) {
            super(itemView);

              title = itemView.findViewById(R.id.tv_title);
              description = itemView.findViewById(R.id.tv_description);

        }
    }

    static class HeaderSubVH extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView description;

        public HeaderSubVH(View itemView) {
            super(itemView);
              title = itemView.findViewById(R.id.tv_title);
              description = itemView.findViewById(R.id.tv_description);
        }
    }

    static class QuestionAnswerVH extends RecyclerView.ViewHolder {
        public TextView question;
        public TextView answer;
        public LinearLayout answersContainer;
        public TextView tvPlaceholder;

        public QuestionAnswerVH(View itemView) {
            super(itemView);
              question = itemView.findViewById(R.id.tv_question);
              answer = itemView.findViewById(R.id.tv_answer);
              answersContainer = itemView.findViewById(R.id.answers);
              tvPlaceholder = itemView.findViewById(R.id.tv_placeholder);
        }
    }


    static class ImageVH extends RecyclerView.ViewHolder {
        public TextView question;
        public ImageView image;
        public TextView tvPlaceholder;

        public ImageVH(View itemView) {
            super(itemView);
              question = itemView.findViewById(R.id.tv_question);
              image = itemView.findViewById(R.id.iv_image);
              tvPlaceholder = itemView.findViewById(R.id.tv_placeholder);
        }
    }
    static class ImageMultiVH extends RecyclerView.ViewHolder {
        public TextView question;
        public ImageView image;
        public TextView tvPlaceholder;

        public ImageMultiVH(View itemView) {
            super(itemView);
              question = itemView.findViewById(R.id.tv_question);
              image = itemView.findViewById(R.id.iv_image);
              tvPlaceholder = itemView.findViewById(R.id.tv_placeholder);
        }
    }
    static class VideoVH extends RecyclerView.ViewHolder {
        public TextView question;
        public ImageView image;
        public TextView tvPlaceholder;

        public VideoVH(View itemView) {
            super(itemView);
              question = itemView.findViewById(R.id.tv_question);
              image = itemView.findViewById(R.id.iv_image);
              tvPlaceholder = itemView.findViewById(R.id.tv_placeholder);
        }
    }
}