package com.hensongeodata.myagro360v2.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.core.content.ContextCompat;
import androidx.core.view.MotionEventCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import com.hensongeodata.myagro360v2.Interface.ItemTouchHelperViewHolder;
import com.hensongeodata.myagro360v2.Interface.OnStartDragListener;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.view.PreviewPOI;

/**
 * Created by user1 on 8/2/2016.
 */

public class POIAdapterRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG=POIAdapterRV.class.getSimpleName();
    private List<POI> poiList;
    private Context mContext;
    private final OnStartDragListener mDragStartListener;

    public POIAdapterRV(Context context,  OnStartDragListener dragStartListener,List<POI> items) {
        mContext=context;
        mDragStartListener=dragStartListener;
        this.poiList =items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.poi_item, parent, false);
            return new POIholder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        //Log.d(TAG,"bindview");
        if (holder instanceof POIholder) {
            POI poi = poiList.get(position);
            POIholder poIholder = (POIholder) holder;
            Log.d(TAG,position+" name: "+poi.getName());
            poIholder.tvTag.setText(String.valueOf(position+1));
            poIholder.tvName.setText(poi.getName());

            poIholder.background.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPop(holder.getAdapterPosition());
                }
            });


            poIholder.handle.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                        mDragStartListener.onStartDrag(holder);
                    }
                    return false;
                }
            });

        }
    }

    private void showPop(final int position){
        POI poi=poiList.get(position);
        Intent i=new Intent(mContext, PreviewPOI.class);
        i.putExtra("position",position);
        mContext.startActivity(i);
    }

    public void addItem(POI poi){

        MyApplication.poiGroup.getPoiList().add(poi);
        poiList=MyApplication.poiGroup.getPoiList();

        notifyDataSetChanged();
    }

public List<POI> getList(){
    return poiList;
}
    private void removeItem(int position){
        MyApplication.poiGroup.getPoiList().remove(position);
        notifyDataSetChanged();
        Toast.makeText(mContext,"POI "+(position+1)+" removed successfully.",Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return poiList.size();
    }

    private class POIholder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder {
        int index;
        public View background;
        public View handle;
        public TextView tvTag;
        public TextView tvName;
        public TextView id;

        public POIholder(View itemView) {
            super(itemView);
            background=itemView.findViewById(R.id.background);
            handle=itemView.findViewById(R.id.bg_handle);
              tvTag = itemView.findViewById(R.id.tv_tag);
              tvName = itemView.findViewById(R.id.tv_name);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

}