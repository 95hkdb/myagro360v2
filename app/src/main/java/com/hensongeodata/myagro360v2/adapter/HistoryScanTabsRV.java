package com.hensongeodata.myagro360v2.adapter;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hensongeodata.myagro360v2.Interface.HistoryTabListener;
import com.hensongeodata.myagro360v2.Interface.ItemTouchHelperViewHolder;
import com.hensongeodata.myagro360v2.Interface.Refresh;
import com.hensongeodata.myagro360v2.MyApplication;
import com.hensongeodata.myagro360v2.R;
import com.hensongeodata.myagro360v2.controller.CreateForm;
import com.hensongeodata.myagro360v2.controller.DatabaseHelper;
import com.hensongeodata.myagro360v2.controller.LocsmmanEngine;
import com.hensongeodata.myagro360v2.helper.AppExecutors;
import com.hensongeodata.myagro360v2.model.InstructionScout;
import com.hensongeodata.myagro360v2.model.Keys;
import com.hensongeodata.myagro360v2.model.POI;
import com.hensongeodata.myagro360v2.model.User;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user1 on 8/2/2016.
 */

public class HistoryScanTabsRV extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG = HistoryScanTabsRV.class.getSimpleName();
    private List<HashMap<String,String>> items;
    private Context mContext;
    private HistoryTabListener listener;
    private Refresh refresh;
    private LocsmmanEngine locsmmanEngine;
    private int mode;
    ArrayList<String> strPois;
    protected DatabaseHelper databaseHelper;


    User mUser;

    public HistoryScanTabsRV(Context context, List<HashMap<String,String>> items,HistoryTabListener listener,Refresh refreshListener) {
        refresh=refreshListener;
        mContext=context;
        this.items =items;
        this.listener=listener;
        locsmmanEngine=new LocsmmanEngine(context);
        databaseHelper=new DatabaseHelper(mContext);

    }

    public void updateItem(int position,HashMap<String,String> map){
        int index=0;
        for(HashMap<String, String> item: items) {
            item.put("focus", ""+0);
            items.set(index,item);
            index++;
        }

        items.set(position,map);
        notifyDataSetChanged();
        //Log.d(TAG,"notifyDataSet");
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

//            View view = inflater.inflate(R.layout.history_scan_tab_item, parent, false);
            View view = inflater.inflate(R.layout.map_history, parent, false);
            return new TabHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        String fname = "";

        if (holder instanceof TabHolder) {
            HashMap<String,String> item = items.get(position);
            final TabHolder tabHolder = (TabHolder) holder;
            int value=Integer.parseInt(item.get("id"));
            final String sectionId = item.get("value");

            String strVal = "";

//            Scouting naming
            if (value < 10 && mode == ChatbotAdapterRV.MODE_SCOUT)
                strVal = fname + " ScanModel " + value;
            else if (value < 100 && mode == ChatbotAdapterRV.MODE_SCOUT)
                strVal = fname + " ScanModel " + value;
            else if (value < 1000 && mode == ChatbotAdapterRV.MODE_SCOUT)
                strVal = fname + " ScanModel " + value;
            else if (value < 10000 && mode == ChatbotAdapterRV.MODE_SCOUT)
                strVal = fname + " ScanModel " + value;

//            Mapping naming
            else if (value < 10 && mode == ChatbotAdapterRV.MODE_MAP)
                strVal = fname + " Map " + value;
            else if (value < 100 && mode == ChatbotAdapterRV.MODE_MAP)
                strVal = fname + " Map " + value;
            else if (value < 1000 && mode == ChatbotAdapterRV.MODE_MAP)
                strVal = fname + " Map " + value;
            else if (value < 10000 && mode == ChatbotAdapterRV.MODE_MAP)
                strVal = fname + " Map " + value;

            final String name = databaseHelper.getMapping(sectionId).getName();

            final String finalStrVal = strVal;

            if (name != null ){
                tabHolder.mapTitleTextView.setText(name);
                tabHolder.moreImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        refresh.moreClicked(name, sectionId);
                    }
                });

            }else{
                tabHolder.mapTitleTextView.setText(strVal);

                tabHolder.moreImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        refresh.moreClicked(finalStrVal, sectionId);
                    }
                });
            }

//            Log.i(TAG, "onBindViewHolder:  section id" + databaseHelper.getMapping(sectionId).getName());

            showPerimeter(tabHolder, sectionId);

            AppExecutors.getInstance().getNetworkIO().execute(new Runnable() {
                @Override
                public void run() {
                    loadMap(tabHolder, sectionId);
                }
            });



            tabHolder.moreImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    refresh.moreClicked(name, sectionId);
                }
            });

            tabHolder.shareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    refresh.shareButtonClicked(sectionId);
                }
            });
        }
    }

    private void loadMap(final TabHolder tabHolder, String sectionId) {
        tabHolder.webView.getSettings().setUseWideViewPort(true);
        tabHolder.webView.getSettings().setLoadWithOverviewMode(true);
        tabHolder.webView.getSettings().setJavaScriptEnabled(true);
        tabHolder.webView.getSettings().setBuiltInZoomControls(true);
        tabHolder.webView.getSettings().setDisplayZoomControls(false);

        tabHolder.webView.loadUrl(Keys.AGRO_BASE+"faw-api/get-map/?mapping_id="+sectionId);
        Log.e(TAG, "Section id: " + sectionId);

        tabHolder.webView.setWebViewClient(new WebViewClient() {

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
//                        tabHolder.progressBar.setVisibility(View.GONE);
//                        tabHolder.webView.setVisibility(View.VISIBLE);
                }

                @TargetApi(Build.VERSION_CODES.LOLLIPOP)


                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    view.loadUrl(request.getUrl().toString());
                    return false;
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                }

                @Override
                public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                    super.onReceivedError(view, request, error);
                    Log.d(TAG,"error");
                }

                @Override
                public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                    super.onReceivedSslError(view, handler, error);
                    Log.d(TAG,"ssl error");
                }

                @Override
                public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                    super.onReceivedHttpError(view, request, errorResponse);
                    Log.d(TAG,"http error");
                }
            });
    }

    private void removeMapping(final int position){
        HashMap<String,String> item = items.get(position);
        final String header=item.get("value");
        String tag=item.get("tag");

        new AlertDialog.Builder(mContext)
                .setTitle(tag)
                .setMessage("Do you want to remove this map?")
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if ((new DatabaseHelper(mContext)).removeScoutListItem(header, ChatbotAdapterRV.MODE_MAP)){
                            MyApplication.showToast(mContext, mContext.getResources().getString(R.string.success), Gravity.CENTER);
//                            items.remove(position);
//                            notifyDataSetChanged();

                            refresh.reload(null);
                        }
                        else
                            MyApplication.showToast(mContext,mContext.getResources().getString(R.string.failed), Gravity.CENTER);
                    }
                })
                .show();
    }

    private void selectItem(int position){
        HashMap<String, String> mapOld=items.get(position);
        //String value=items.get(position).get("value");
        listener.onSelected(mapOld.get("value"));
        //mapMask.put("value",value);
        HashMap<String,String> mapMask=new HashMap<>();
        mapMask.put("focus",""+1);
        mapMask.put("id",mapOld.get("id"));
        mapMask.put("value",mapOld.get("value"));

        updateItem(position,mapMask);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private class TabHolder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder {
        int index;
        public TextView mapTitleTextView;
        public WebView webView;
        public ProgressBar progressBar;
        public TextView mapSizeTextView;
        public ImageView moreImageView;
        public Button shareButton;

        public TabHolder(View itemView) {
            super(itemView);

            mapTitleTextView = itemView.findViewById(R.id.map_title);
            webView                  = itemView.findViewById(R.id.webview);
            progressBar           = itemView.findViewById(R.id.progress_bar);
            mapSizeTextView = itemView.findViewById(R.id.map_size);
            moreImageView   = itemView.findViewById(R.id.more);
            shareButton           = itemView.findViewById(R.id.share_map);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

    private double metersToAcres(long meterSquared){
        return Math.round((meterSquared*0.00024711) * 1000.0) / 1000.0;
    }

    private double metersToHectares(long mettersSquared){
        return mettersSquared*0.0001;
    }

    private double metersToKilometers(long metersSquared){
        return metersSquared*0.000001;
    }

    private double metersToKilo(long metersSquared){
        return metersSquared*0.001;
    }

    private void showPerimeter(TabHolder tabHolder, String section_id){

        List<InstructionScout> getAllItems=databaseHelper.getScoutListAllStates(section_id, ChatbotAdapterRV.MODE_MAP);
        strPois =new ArrayList<>();

        for (InstructionScout instructionScout : getAllItems) {
            if(instructionScout!=null) {
                String message = instructionScout.getMessage();
                if (message != null && !message.trim().isEmpty())
                    strPois.add(message);
            }
        }

        ArrayList<POI> pois= CreateForm.getInstance(mContext).buildPOI_Mapping(strPois);
        int area=MyApplication.getArea(pois);
        long perimeter=MyApplication.getPerimeter(pois);
        double inKilometers = metersToKilo(perimeter);
        double acreage         = metersToAcres(perimeter);
        tabHolder.mapSizeTextView.setText(String.format("%sm | %skm |  %s acres", String.valueOf(perimeter), inKilometers, acreage));
    }


}